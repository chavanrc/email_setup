<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard | 360&deg; Feedback Tool</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web-orange.css">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
        <link href="<?php echo base_url(); ?>assets/css/gallery-pop/gallery.css" rel="stylesheet">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <style>
            .page_spin1{
                position: absolute;
                top:0;
                left:0;
                background: rgba(0,0,0,0.1);
                width: 100%;
                height: 100%;
                display: none;
                z-index: 9999999;
            }
            .spin_icon1{
                width: 50px;
                height: 50px;
                //background: rgba(0,0,0,0.1);
                text-align: center;
                border-radius: 50%;
                
            }
            .spin_icon1 .fa{
                margin-top: 10px;
                font-size: 30px;
            }
            .spin_icon1 span{
                color:#999;
                font-size: 11px;
            }
        </style>
    </head>
    <body class="hold-transition skin-green sidebar-mini" style="background:#ecf0f5;">
        <div class="wrapper">
            <?php //include 'layout/header.php'; ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php //include 'layout/menu.php'; ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="margin-left: 0px;">
               
                <section class="content container-fluid">
                    <div class="row">
                        <div class="col-md-12 text-center">
                            
                            
                            <?php 
                            if(!empty($photo))
                            {
                                ?>
                                <div style="background: #fff; padding: 15px; border: solid 1px #ddd; margin-bottom: 15px; border-radius: 3px;">
                                    <div class="row preViewImg">
                                        <div id="pImg"></div>
                                    <?php
                                    foreach($photo as $ph_data)
                                    {
                                        ?>
                                            <img src="<?php echo base_url(); ?>assets/upload/<?php echo $ph_data->ti_img; ?>" data-darkbox="<?php echo base_url(); ?>assets/upload/<?php echo $ph_data->ti_img; ?>" style="height: 150px; float: left; margin: 7px; padding: 4px; border: solid 1px #ddd;"/>
                                        
                                        <?php
                                    }
                                    ?>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            
                            
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <?php //include 'layout/footer.php';  ?>
        </div>
        
        <div class="page_spin" id="page_spin">
            <br/>
            <div class="spin_icon">
                <i class="fa fa-spinner fa-spin"></i><br/>
                <span>One moment ...</span>
            </div>
        </div>
        <!-- ./wrapper -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/web.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/gallery-pop/gallery.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery.form.js"></script>
        <script type="text/javascript">
            document.getElementById("imgUpload").onchange = function () {
                document.getElementById("page_spin").style.display = 'block';
                document.getElementById("upload-form").submit();
            };
            
//            $(document).ready(function () {
//                $('#imgUpload').on('change', function () {
//                    $('.page_spin1').show();
//                    $("#upload-form")submit();
//                });
//            });
            
        </script>
        <?php include 'layout/script.php'; ?>
    </body>
</html>