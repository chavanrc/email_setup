<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Thank you | 360&deg; Feedback Tool</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web-orange.css">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-green layout-boxed sidebar-mini">
        <div class="wrapper">
            <?php include 'layout/header.php'; ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php include 'layout/menu.php'; ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1> Thank you </h1>

                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>home/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active">Success</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="post white-bg" style="padding:15px 15px 1px;">
                                <div class="user-block"> <img class="img-circle img-bordered-sm" src="<?php echo base_url(); ?>assets/images/emp_pic/<?php echo $emp[0]->emp_photo; ?>" alt="user image"> <span class="username"> <a href="#"><?php echo $emp[0]->emp_name; ?></a>  </span> <span class="description"><?php echo $emp[0]->emp_designation; ?></span> </div>
                            </div>
                        </div>

                        <div class="col-md-12">
                            <div class="box">

                                <div class="info-details">
                                    <p class="contact-icon text-center" aria-hidden="true" style="margin: 0 auto;"><i class="fa fa-check"></i></p>
                                    <h3>Thank You.. !!</h3>
                                    <h4>You completed 360&deg; Feedback for <span class="clr"> <?php echo $emp[0]->emp_name; ?>. </span>

                                    </h4>
                                    <br>
                                    <a href="<?php echo base_url(); ?>home/dashboard" class="btn btn-md btn-danger"><i class="fa fa-home"></i> Go to Home</a>
                                    <a href="<?php echo base_url(); ?>home/view_feedback/<?php echo $emp[0]->emp_id; ?>" class="btn btn-md btn-success">View Feedback &nbsp; <i class="fa fa-thumbs-up"></i></a>   

                                </div><!-- end info-details  -->

                            </div>

                        </div>

                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <?php include 'layout/footer.php'; ?>
        </div>
        <!-- ./wrapper -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/web.js"></script>
    </body>
</html>