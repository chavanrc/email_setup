<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>360 Degree Feedback Tool</title>
        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">

    </head>

    <body style="font-family: 'Open Sans', sans-serif;letter-spacing:0.6px;">
        <table style="width:60%; margin:0 auto; background:#585858; padding:10px;">
            <tr>
                <td style="text-align:center"><img src="<?php echo base_url(); ?>assets/images/news_logo.png"></td>
            </tr>
        </table>
        <table style="width:60%; margin:0 auto; background:#f7f7f7; padding:10px;">
            <tr>
                <td width="18%" style="width:15%; background: #0c98e8;padding: 7px;color: #FFF;text-align:center;font-size:14px;"><?php echo $val['name']; ?></td>
                <td width="79%" style="width:45%; background: #9ac9e4;padding: 7px;color: #FFF;text-align:right;font-size:14px;"><?php echo date('d-m-Y'); ?></td>
            </tr>
            <tr>
                <td style="width:15%; padding: 7px;display: table; margin: 0 auto;"><img src="<?php echo base_url(); ?>assets/images/emp_pic/<?php echo $val['photo']; ?>" width="70px"></td>
                <td style="width:45%; padding: 7px;color: #000; font-size:13px;">
                    <p>Dear <strong><?php echo $val['name']; ?></strong></p>
                    <p>This is a reminder for completing remaining 360 degree feedback for your co-workers in your organizations.</p>
                    <p>There are  pending people waiting your 360 degree feedback</p>
                </td>
            </tr>
            <?php
            $CI = & get_instance();
            $CI->load->model('home_model');
            $emp = $CI->home_model->get_pending_emp($val['eid']);
            if (!empty($emp)) {
                foreach ($emp as $em_data) {
                    ?>
                    <tr>
                        <td colspan="1"></td>
                        <td style="width:45%; padding: 7px;color: #000; font-size:13px;">
                            <img src="<?php echo base_url(); ?>assets/images/emp_pic/<?php echo $em_data->emp_photo; ?>" alt="" style="width:45px; height:45px; border-radius:50px; border:3px solid #FFF; float:left; margin-right:10px;">
                            <p style="font-size:18px; margin-top:14px; margin-bottom:0px"><?php echo $em_data->emp_name; ?></p>
                            <span style="font-size:13px; margin:0px; color:#8D8D8D"><?php echo $em_data->emp_designation; ?></span>
                    </tr>
                    <?php
                }
            }
            ?>
            <tr>
                <td colspan="1"></td>
                <td style="width:45%; padding: 7px;border: 1px solid #9ac9e4; font-size:13px; background:#FFF;">
                    <p><a href="#" style="text-decoration:none; margin-top:50px;">Click here to login to 360 degree feedback tool</a></p>
                </td>

            </tr>


        </table>
    </body>
</html>
