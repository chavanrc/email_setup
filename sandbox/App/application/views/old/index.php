<!doctype html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Step 2 Axis Way</title>
    <meta name="description" content="">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" type="image/x-icon" property="shortcut icon">
</head>
<body>

    <div class="container">

        <?php include 'layout/header.php'; ?>

        <div class="row ptb-30 content">
            <div class="col-md-12">
                <div class="main-head text-center">Dear Axis Bank Participant,</div>
                <div class="sub-head primary-clr">Greetings from Wagons Learning!</div>
                <p class="text-center">Request you all to take up the Post Training Test. We Look forward to your active participation in letting the organization measure the effectiveness of the training program.

                </p>
            </div>
        </div>

        <div class="row p-30">

            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul class="bread">
                        <li class="primary-clr"><strong>Register</strong></li> /
                        <li>Read Test Instructions</li> /
                        <li>Take Test</li>
                    </ul>

                </div>
            </div>
            <!-- Login Form-->
            <div class="col-md-5 col-xs-12">
                <?php
                if($msg==3)
                {
                    ?>
                <div class="alert alert-warning">You have already attempted the test previously. More than 1 attempt is not allowed. Please contact Wagons support to unlock</div>
                    <?php
                }
                if($msg==4)
                {
                    ?>
                <div class="alert alert-success">Assessment test already taken by you. You need not retry</div>
                    <?php
                }
                if($msg==0)
                {
                    ?>
                <div class="alert alert-danger">Email or password is invalid. Try Again</div>
                    <?php
                }
                ?>
                <div class="account-boxes search-form">
                    <div class="log-head col-md-12">Verify Your Information</div>
                    <form method="post" action="#" id="Login"  >
                        <div class="form-input form-group col-md-12"> <i class="fa fa-envelope" style="left: 22px;"></i>
                            <input type="text" class="form-control" id="email" name="email" placeholder="Email Id" >
                        </div>
                        <div class="form-input form-group col-md-12"> <i class="fa fa-lock" style="left: 23px;"></i>
                            <input type="password" class="form-control" id="pass" name="pass" placeholder="Password" >
                        </div>
                        <div class="form-group col-md-4 col-sm-12">
                            <button type="submit" name="submit" title="Login" class="btn btn-warning" style="padding: 6px 20px;
                                    font-size:15px;">LOGIN</button>
                        </div>
                        <div class="col-md-8 col-sm-12 ">
                            <button type="reset" name="submit" title="Login" class="btn btn-danger reset" style="padding: 6px 20px;
                                    font-size:15px;">RESET</button>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-md-7 col-xs-12 assistance">
                <div class="message-boxes search-form">
                    <div class="main-head text-center secondary-clr">For any assistance</div>
                    <div class="assist"> contact Wagons support team on <strong>020-6560 6055</strong></div>

                </div>
            </div>

        </div>

        <?php include 'layout/footer.php'; ?>

    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/validation/jquery-validate.bootstrap-tooltip.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#Login").validate({
                rules: {
                    pass: "required",
                    email: {
                        required: true,
                        email: true
                    },
                },
                tooltip_options: {
                    inst: {
                        trigger: 'focus',
                    },
                    example5: {
                        placement: 'right',
                        html: true
                    }
            },
            });
        });


    </script>
</body>
</html>