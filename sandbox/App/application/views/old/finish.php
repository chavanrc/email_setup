<!doctype html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Step 2 Axis Way | Test Completed</title>
    <meta name="description" content="">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/timer/jquery.countdownTimer.css">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" type="image/x-icon" property="shortcut icon">
</head>
<body>

    <div class="container">

        <?php include 'layout/header.php'; ?>

        <div class="row bread primary-bg">
            <div class="col-md-12 user">
                <div class="username"><i class="fa fa-user"></i> Hello <span class="white-clr"><?php echo $this->session->userdata('user_name'); ?></span></div>
                <span id="current_timer"></span>
            </div>

        </div>





        <div class="row ptb-30">
            <div class="col-md-12">

                <div class="col-md-12 attempt" style="padding:30px;">
                    <div class="success text-success"><strong>Success !</strong></div>
                    <h4>Dear <?php echo $this->session->userdata('user_name'); ?></h4>
                    <h5>You have finished Pre training assessment Test.</h5>
                    <h5>You answered all <?php echo $msg; ?> Questions. Your test statistics will be shared later.</h5>
                    <h5>Thank you for your patience, for any further queries kindly contact <strong>020-6560 6055</strong></h5>



                </div>

            </div>
        </div>

        <?php 
        $this->session->unset_userdata('user');
        $this->session->unset_userdata('user_name');
        $this->session->unset_userdata('user_id');
        $this->session->unset_userdata('finish');
        $this->session->unset_userdata('exam_time');
        include 'layout/footer.php'; ?>

    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/timer/jquery.countdownTimer.js"></script>

    <script>
        $(function () {
            $('#current_timer').countdowntimer({
                currentTime: true,
                size: "sm"
            });
        });
    </script>
</body>
</html>