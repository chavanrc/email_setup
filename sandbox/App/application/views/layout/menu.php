<aside class="main-sidebar">


    <section class="sidebar">

        <ul class="sidebar-menu" data-widget="tree">
            <li style="border-bottom: solid 1px #ddd;"><a href="<?php echo base_url(); ?>home/dashboard/<?php echo $userData['projectId']; ?>/<?php echo $userData['trainerId']; ?>"><i class="fa fa-home"></i><span> Home </span></a></li>
            <?php
            if (!empty($userData['projectId'])) {
                ?>
            <li style="border-bottom: solid 1px #ddd;"><a href="#" id="tCheckin" tid="<?php echo $userData['trainerId']; ?>" pid="<?php echo $userData['projectId']; ?>"><i class="fa fa-sign-in"></i><span> Check In </span></a></li>
                <li style="border-bottom: solid 1px #ddd;"><a href="#" id="preTrigger" tid="<?php echo $userData['trainerId']; ?>" pid="<?php echo $userData['projectId']; ?>"><i class="fa fa-flag"></i><span> Trigger Pre test Questions </span></a></li>
                <li style="border-bottom: solid 1px #ddd;"><a href="#" id="postTrigger" tid="<?php echo $userData['trainerId']; ?>" pid="<?php echo $userData['projectId']; ?>"><i class="fa fa-flag"></i><span> Trigger Post test Questions </span></a></li>
                <li style="border-bottom: solid 1px #ddd;"><a href="<?php echo base_url(); ?>home/participants_list/<?php echo $userData['projectId']; ?>"><i class="fa fa-bars"></i><span> Participants List </span></a></li>
                <li style="border-bottom: solid 1px #ddd;"><a href="#"><i class="fa fa-upload"></i><span> Upload Photos </span></a></li>
                <li style="border-bottom: solid 1px #ddd;"><a href="#"><i class="fa fa-sign-out"></i><span> Check Out </span></a></li>
                        <?php } ?>
        </ul>
        <!-- /.sidebar-menu -->
    </section>

    <!-- /.sidebar -->
</aside>
<div class="page_spin">
            <br/>
            <div class="spin_icon">
                <i class="fa fa-spinner fa-spin"></i><br/>
                <span>One moment ...</span>
            </div>
        </div>