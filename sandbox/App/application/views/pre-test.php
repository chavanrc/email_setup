<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard | 360&deg; Feedback Tool</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web-orange.css">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-green layout-boxed sidebar-mini" style="background: #ecf0f5;">
        <div class="wrapper">
            <?php //include 'layout/header.php'; ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php //include 'layout/menu.php'; ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="margin-left:0px;">

                <div style="background: #fff; padding: 15px; color: #429fff; font-size: 20px; box-shadow: 3px 0px 3px #9e9e9e;">
                   
                    &nbsp; &nbsp; &nbsp; Pre Assessment         
                </div>

                <!-- Main content -->
                <section class="content container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <form action="" method="POST" id="testForm">
                                <input type="hidden" name="pid" value="<?php echo $pid; ?>"/>
                                <input type="hidden" name="uid" value="<?php echo $uid; ?>"/>
                                <?php
                                if (!empty($question)) {
                                    $no = 0;
                                    foreach ($question as $qn_data) {
                                        $no++;
                                        ?>
                                        <div style="background: #fff; padding: 20px; border: solid 1px #ddd; margin-bottom: 15px; border-radius: 3px;">
                                            <b><?php echo $no; ?>.</b> <?php echo $qn_data->question_text; ?>

                                            <div style="margin-top: 10px;">
                                                <div style="padding: 7px;">
                                                    <label>
                                                        <input type="radio" name="qn_<?php echo $qn_data->question_id; ?>" value="<?php echo $qn_data->answer_1; ?>"> <?php echo $qn_data->answer_1; ?>
                                                    </label>
                                                </div>
                                                <div style="padding: 7px;">
                                                    <label>
                                                        <input type="radio" name="qn_<?php echo $qn_data->question_id; ?>" value="<?php echo $qn_data->answer_2; ?>"> <?php echo $qn_data->answer_2; ?>
                                                    </label>
                                                </div>
                                                <div style="padding: 7px;">
                                                    <label>
                                                        <input type="radio" name="qn_<?php echo $qn_data->question_id; ?>" value="<?php echo $qn_data->answer_3; ?>"> <?php echo $qn_data->answer_3; ?>
                                                    </label>
                                                </div>
                                                <div style="padding: 7px;">
                                                    <label>
                                                        <input type="radio" name="qn_<?php echo $qn_data->question_id; ?>" value="<?php echo $qn_data->answer_4; ?>"> <?php echo $qn_data->answer_4; ?>
                                                    </label>
                                                </div>
                                            </div>

                                        </div>

                                        <?php
                                    }
                                }
                                ?>
                                <div class="form-group text-center">
                                <button type="submit" style="width:100%; font-size: 22px;" class="btn btn-success hvr-curl-top-right"> Submit </button>
                            </div>
                            </form>
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <?php //include 'layout/footer.php';   ?>
        </div>
        <!-- ./wrapper -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/web.js"></script>
        <?php include 'layout/script.php'; ?>
    </body>
</html>