<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Login | 360&deg; Feedback Tool</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web-orange.css">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> 
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <style>
            .form-control{
                padding: 20px;
                font-size: 17px;
            }
            .btn{
                font-size: 19px;
            }
        </style>
    </head>

    <body style="background:#fff;">
        <div class="login" style="margin-top:20px;">
            <div class="container">
                <div class="row">
                    <div class="logo-wag">
                        <img src="<?php echo base_url(); ?>assets/images/logo2.png" class="img-responsive" alt="Wagons Learning" style="margin:0 auto;">

                    </div>
                    
                    <div style="background: #fff; padding: 15px;  margin-left:15px; margin-right:15px; border: solid 1px #ddd; border-radius: 3px;">
                                <div style="font-size: 18px;">
                                    <?php echo $score[0]->project_title; ?>
                                    
                                </div>
                                
                            </div>
                    
                    <div class="login-box" style="border-radius:5px; box-shadow: 0px 0px 3px #ddd;">
                        <div class="text-center" style="padding:10px; font-size: 18px;">Enter Your Details </div>
                        <form action="" method="post" name="Login" id="Login" class="log">
                            
                            <div class="form-group">
                                    <input type="text" class="form-control" id="fname" name="fname" placeholder="Full Name *"  required>
                            </div>
                            
                            <div class="form-group">
                                    <input type="text" class="form-control" id="org" name="org" placeholder="Organisation" >
                               
                            </div>
                            
                            <div class="form-group">
                                    <input type="text" class="form-control" id="city" name="city" placeholder="City *" >
                               
                            </div>
                            
                            <div class="form-group">
                                    <input type="text" class="form-control" id="address" name="address" placeholder="Address" >
                            </div>

                            <div class="form-group text-center">
                                <button type="submit" style="width:100%" class="btn btn-success hvr-curl-top-right"> Continue</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="page_spin">
            <br/>
            <div class="spin_icon">
                <i class="fa fa-spinner fa-spin"></i><br/>
                <span>One moment ...</span>
            </div>
        </div>


        <!-- ./wrapper -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/web.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery-validate.bootstrap-tooltip.js"></script>

        <script>
            $(document).ready(function () {
                function LoginForm() {
                    var email = $("#email").val();
                    var password = $("#password").val();


                    var dataString = "email=" + email + "&password=" + password;
                    $.ajax({
                        type: "POST",
                        url: "admin-login.php",
                        data: dataString,
                        success: function (result) {
                            if (result == "y")
                            {
                                //Moved to dashboard.....
                            }
                            else
                            {
                                $("#lgnErorMsg").html("<div class='alert alert-warning'>Invalid Username and Password!</div>");
                                $("#Login").find("input,textarea,select").val('')
                            }
                        }
                    });
                }
                $("#Login").validate({
                    rules: {
                        fname: "required",
                        city: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function(form) {
			    $('.page_spin').show();
			    form.submit();
			}
                });
            });

        </script>
    </body>
</html>