<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class participant extends CI_Controller {

    public function index($id = NULL) {
       $this->login();
    }
    
    public function login() {
        $this->load->model('home_model');
        if ($this->session->userdata('participant')) {
            redirect('participant/dashboard');
        }
        $data['msg'] = 4;
        if (isset($_POST['email'])) {
            $isUser = $this->home_model->participant_login($_POST);
          // return;
            if ($isUser == 2) {
                redirect('participant/complete_profile');
            } else if($isUser == 1){
                redirect('participant/dashboard');
            } else {
                $data['msg'] = $isUser;
            }
        }
        $this->load->view('participant-login', $data);
    }
    
    public function dashboard(){
    	
    	print_r($_GET);
        $this->session->set_userdata("participant",$_GET['uid']);
        $this->session->set_userdata("pMobile",$_GET['mobile']);
        $this->session->set_userdata("pName",$_GET['pName']);
        $this->session->set_userdata("pId",$_GET['pId']);
            
        $this->load->model('home_model');
        $data['msg'] = "";
         $data['score'] = $this->home_model->getParticipantDetails();
        $data['preAttd'] = $this->home_model->checkPreAttd();
        $data['postAttd'] = $this->home_model->checkPostAttd();
        if(!empty($data['score']))
        {
            $this->load->view('participant-dashboard', $data);
        }
    }
    
    public function pre_test($uid,$pid)
    {
        $this->load->model('home_model');
        $data['pid'] = $pid;
        $data['uid'] = $uid;
        $data['question'] = $this->home_model->getPretest_questions($pid);
        if(isset($_POST['uid']))
        {
            $this->home_model->save_pre_test();
             redirect('participant/dashboard');
        }
        $this->load->view('pre-test', $data);
    }
    
    public function post_test($uid,$pid)
    {
        $this->load->model('home_model');
        $data['pid'] = $pid;
        $data['uid'] = $uid;
        $data['question'] = $this->home_model->getPosttest_questions($pid);
        if(isset($_POST['uid']))
        {
            $this->home_model->save_post_test();
             redirect('participant/dashboard');
        }
        $this->load->view('post-test', $data);
    }
    
    public function feeback($uid,$pid)
    {
        echo "<h2>Give Your Feedback</h2>";
    }

    public function complete_profile()
    {
        $this->check_sess();
        $this->load->model('home_model');
        $data['msg'] = "";
        if(isset($_POST['fname']))
        {
            $this->home_model->save_participant();
            redirect('participant/dashboard');
        }
        $data['score'] = $this->home_model->getParticipantDetails();
        $this->load->view('add-profile', $data);
    }

    public function participants_list($id)
    {
        $this->load->model('home_model');
        $data['part'] = $this->home_model->getParticipants_list($id);
        $this->load->view('participants-list', $data);
    }
    
    public function upload_photo($id)
    {
        $this->load->model('home_model');
        $data['pid'] = $id;
        $data['tid'] = "";
        $data['msg'] = 0;
        if(isset($_POST['tid']))
        {
            $data['msg'] = $this->home_model->upload_photo();
        }
        $this->load->view('upload-photo', $data);
    }
    
    public function pre_questions($id)
    {
        $this->load->model('home_model');
        $data['question'] = $this->home_model->getPretest_questions($id);
        $this->load->view('pre-questions', $data);
    }
    
    public function post_questions($id)
    {
        $this->load->model('home_model');
        $data['question'] = $this->home_model->getPosttest_questions($id);
        $this->load->view('post-questions', $data);
    }

    public function send_email_reminder()
    {
        $this->load->model('home_model');
        $this->home_model->email_reminder();
//        foreach($result as $rs_data)
//            {
//                $data['name'] = $rs_data->emp_name;
//                $data['to'] = $rs_data->emp_email;
//                $data['photo'] = $rs_data->emp_photo;
//                $data['eid'] = $rs_data->emp_id;
//                $data1['val'] = $data;
//                
//                $this->load->view('email-temp', $data1);
//                //send_email($data);
//            }
    }


    public function check_sess() {
        if (!$this->session->userdata('participant')) {
            redirect('participant/index');
        }
    }
   

   
    public function logout() {
        $this->session->unset_userdata('user', '');
        $this->session->unset_userdata('user_name', '');
        redirect('home/index');
    }

    public function ajax_page() {
        $this->load->model('home_model');
        echo $this->home_model->$_POST['page']($_POST);
    }

}
