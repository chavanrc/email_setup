<?php
session_start();
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function index($id=NULL) {
        
        $this->login();
    }
    
    
    public function page_map($pr,$sc=NULL, $th=NULL){
        $this->$pr($sc, $th);
    }

    /* Admin Login */

    public function login() {
        $this->load->model('home_model');
        if (isset($_SESSION['admin'])) {
            redirect('admin/employee_list');
        }
        $data['msg'] = 2;
        if (isset($_POST['email'])) {
            $isUser = $this->home_model->checkUser($_POST);
            if ($isUser != 0) {
                redirect('admin/employee_list');
            } else {
                $data['msg'] = 'Invalid Credentials';
            }
        }
        $this->load->view('admin-login', $data);
    }
    
    

    public function logout() {
        $this->session->unset_userdata('admin','');
        redirect('admin/index');
    }
    
    public function employee_list()
    {
        $this->check_sess();
        $this->load->model('home_model');
	$data['emp'] = $this->home_model->get_employee_list();
        $this->load->view('employee-list',$data);
    }
    
    public function result($id)
    {
        $this->check_sess();
        $this->load->model('home_model');
        $data['question'] = $this->home_model->get_all_questions();
        $data['emp'] = $this->home_model->get_single_employee($id);
        $this->load->view('result', $data);
    }

    /* Admin Dashboard */
    
    public function check_sess()
    {
        if(!isset($_SESSION['admin']))
        {
            redirect('admin/login');
        }
    }

    public function dashboard() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['profile'] = $this->admin_model->admin_profile();
        $this->load->view('admin/profile',$data);
    }

    public function ajax_page() {
        $this->check_sess();
        $this->load->model('admin_model');
        echo $this->admin_model->$_POST['page']($_POST);
    }
  
}
