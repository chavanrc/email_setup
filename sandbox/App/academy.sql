-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 01, 2017 at 12:58 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `academy`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_user`
--

CREATE TABLE IF NOT EXISTS `admin_user` (
  `au_id` int(11) NOT NULL AUTO_INCREMENT,
  `au_type` varchar(255) NOT NULL,
  `au_email` varchar(255) NOT NULL,
  `au_password` varchar(255) NOT NULL,
  `au_name` varchar(255) NOT NULL,
  `au_contact` varchar(255) NOT NULL,
  `au_date` datetime NOT NULL,
  `au_last_login` datetime NOT NULL,
  `au_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`au_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `admin_user`
--

INSERT INTO `admin_user` (`au_id`, `au_type`, `au_email`, `au_password`, `au_name`, `au_contact`, `au_date`, `au_last_login`, `au_status`) VALUES
(1, 'Admin', 'admin@gmail.com', '123', 'Anand Academy', '9743285056', '2015-05-18 00:00:00', '2015-05-18 00:00:00', 1),
(2, 'Sub Admin', 'anil.webdreams@gmail.com', '123', 'Anil Kusanale', '9743285056', '2016-06-11 07:56:30', '2016-06-11 06:12:15', 1),
(3, 'Sub Admin', 'manish@webdreams.in', '123', 'Manish', '9743285056', '2016-06-11 07:56:36', '2016-06-11 06:12:52', 0),
(5, 'Sub Admin', 'madhu.click@gmail.com', '123', 'Madhukar', '9743285056', '2016-06-11 08:14:14', '2016-06-11 08:14:14', 1);

-- --------------------------------------------------------

--
-- Table structure for table `album`
--

CREATE TABLE IF NOT EXISTS `album` (
  `al_id` int(11) NOT NULL AUTO_INCREMENT,
  `al_title` varchar(255) NOT NULL,
  `al_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`al_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `album`
--

INSERT INTO `album` (`al_id`, `al_title`, `al_status`) VALUES
(9, 'Our Partners', 1);

-- --------------------------------------------------------

--
-- Table structure for table `batch`
--

CREATE TABLE IF NOT EXISTS `batch` (
  `bh_id` int(11) NOT NULL AUTO_INCREMENT,
  `bh_title` varchar(255) NOT NULL,
  `bh_date` datetime NOT NULL,
  `bh_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`bh_id`),
  KEY `bh_title` (`bh_title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `batch`
--

INSERT INTO `batch` (`bh_id`, `bh_title`, `bh_date`, `bh_status`) VALUES
(3, 'English Language', '2017-10-26 10:14:42', 0),
(4, 'Quantitative Aptitude', '2017-10-26 10:14:52', 0),
(5, 'Reasoning Ability', '2017-10-26 10:15:04', 0);

-- --------------------------------------------------------

--
-- Table structure for table `batch_user`
--

CREATE TABLE IF NOT EXISTS `batch_user` (
  `bu_id` int(11) NOT NULL AUTO_INCREMENT,
  `bh_title` varchar(255) NOT NULL,
  `u_email` varchar(255) NOT NULL,
  `bu_date` datetime NOT NULL,
  `bu_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`bu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `cadet`
--

CREATE TABLE IF NOT EXISTS `cadet` (
  `cd_id` int(11) NOT NULL AUTO_INCREMENT,
  `cd_name` varchar(255) NOT NULL,
  `cd_tsa_validity` varchar(255) NOT NULL,
  `cd_contact` varchar(255) NOT NULL,
  `cd_email` varchar(255) NOT NULL,
  `cd_address` varchar(1000) NOT NULL,
  `cd_password` varchar(255) NOT NULL,
  `cd_dob` datetime NOT NULL,
  `cd_batch` varchar(255) NOT NULL,
  `cd_computer` varchar(255) NOT NULL,
  `cd_date` datetime NOT NULL,
  `cd_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`cd_id`),
  KEY `cd_email` (`cd_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `cadet`
--

INSERT INTO `cadet` (`cd_id`, `cd_name`, `cd_tsa_validity`, `cd_contact`, `cd_email`, `cd_address`, `cd_password`, `cd_dob`, `cd_batch`, `cd_computer`, `cd_date`, `cd_status`) VALUES
(1, 'Anil Kusanale', '12-12-2016', '9743285056', 'anil.webdreams@gmail.com', 'Hubli, Karnataka', '1234', '2008-12-21 00:00:00', '54', '123', '2016-12-15 10:07:36', 0),
(3, 'Ravi', '12-12-2016', '9743285056', 'ravi.webdreams@gmail.com', 'Koppikar road, Hubli, Karnataka', '123', '1988-08-06 00:00:00', '54', '12', '2016-12-16 05:37:35', 0);

-- --------------------------------------------------------

--
-- Table structure for table `class_subjects`
--

CREATE TABLE IF NOT EXISTS `class_subjects` (
  `sb_id` int(11) NOT NULL AUTO_INCREMENT,
  `sb_title` varchar(255) NOT NULL,
  `sb_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`sb_id`),
  KEY `sb_title` (`sb_title`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `class_subjects`
--

INSERT INTO `class_subjects` (`sb_id`, `sb_title`, `sb_status`) VALUES
(7, 'IBPS', 0),
(8, 'IPS', 0);

-- --------------------------------------------------------

--
-- Table structure for table `employee`
--

CREATE TABLE IF NOT EXISTS `employee` (
  `em_id` int(11) NOT NULL AUTO_INCREMENT,
  `em_name` varchar(255) NOT NULL,
  `em_email` varchar(255) NOT NULL,
  `em_contact` varchar(255) NOT NULL,
  `em_password` varchar(255) NOT NULL,
  `em_dept` varchar(255) NOT NULL,
  `em_photo` varchar(2000) NOT NULL,
  `em_address` varchar(1000) NOT NULL,
  `em_date` datetime NOT NULL,
  `em_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`em_id`),
  KEY `em_email` (`em_email`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=1112 ;

--
-- Dumping data for table `employee`
--

INSERT INTO `employee` (`em_id`, `em_name`, `em_email`, `em_contact`, `em_password`, `em_dept`, `em_photo`, `em_address`, `em_date`, `em_status`) VALUES
(1111, 'ANil', 'anilk.acs@gmail.com', '9743285056', 'jywgb', 'IBPS', '31166_advertising.jpg', 'Gokul road hubli', '2017-10-26 08:44:52', 1);

-- --------------------------------------------------------

--
-- Table structure for table `exam`
--

CREATE TABLE IF NOT EXISTS `exam` (
  `ex_id` int(11) NOT NULL AUTO_INCREMENT,
  `ex_sub` varchar(255) NOT NULL,
  `ex_title` varchar(255) NOT NULL,
  `ex_date` datetime NOT NULL,
  `ex_marks` varchar(255) NOT NULL,
  `ex_min_marks` varchar(255) NOT NULL,
  `ex_time` varchar(255) NOT NULL,
  `ex_crt_date` datetime NOT NULL,
  `ex_status` tinyint(1) NOT NULL,
  `ex_result` tinyint(1) NOT NULL,
  PRIMARY KEY (`ex_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `exam`
--

INSERT INTO `exam` (`ex_id`, `ex_sub`, `ex_title`, `ex_date`, `ex_marks`, `ex_min_marks`, `ex_time`, `ex_crt_date`, `ex_status`, `ex_result`) VALUES
(5, 'IBPS', 'IBPS Mock Test For First Batch 2017', '2017-08-31 22:30:00', '100', '1', '90', '2017-10-27 07:57:15', 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `exam_batch`
--

CREATE TABLE IF NOT EXISTS `exam_batch` (
  `eb_id` int(11) NOT NULL AUTO_INCREMENT,
  `ex_title` varchar(255) NOT NULL,
  `bh_title` varchar(255) NOT NULL,
  `eb_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`eb_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `exam_questions`
--

CREATE TABLE IF NOT EXISTS `exam_questions` (
  `eq_id` int(11) NOT NULL AUTO_INCREMENT,
  `ex_id` int(11) NOT NULL,
  `eq_title` varchar(20000) NOT NULL,
  `eq_desc` varchar(5000) NOT NULL,
  `eq_section` varchar(255) NOT NULL,
  `eq_img` varchar(1000) NOT NULL,
  `eq_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`eq_id`),
  KEY `ex_id` (`ex_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `exam_questions`
--

INSERT INTO `exam_questions` (`eq_id`, `ex_id`, `eq_title`, `eq_desc`, `eq_section`, `eq_img`, `eq_status`) VALUES
(1, 5, 'This is first question', 'This is test passage', 'English Language', '22556_logo.png', 0),
(7, 5, 'Test question', '', 'Quantitative Aptitude', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `exam_result`
--

CREATE TABLE IF NOT EXISTS `exam_result` (
  `ea_id` int(11) NOT NULL AUTO_INCREMENT,
  `ex_id` int(11) NOT NULL,
  `cd_id` varchar(255) NOT NULL,
  `er_attd` varchar(255) NOT NULL,
  `er_right` varchar(255) NOT NULL,
  `er_wrong` varchar(255) NOT NULL,
  `er_marks` varchar(255) NOT NULL,
  `er_result` varchar(255) NOT NULL,
  `er_date` datetime NOT NULL,
  `er_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`ea_id`),
  KEY `ex_id` (`ex_id`),
  KEY `cd_id` (`cd_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `images`
--

CREATE TABLE IF NOT EXISTS `images` (
  `im_id` int(11) NOT NULL AUTO_INCREMENT,
  `al_id` int(11) NOT NULL,
  `im_title` varchar(500) NOT NULL,
  `im_img` varchar(500) NOT NULL,
  `im_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`im_id`),
  KEY `al_id` (`al_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `images`
--

INSERT INTO `images` (`im_id`, `al_id`, `im_title`, `im_img`, `im_status`) VALUES
(8, 9, 'Our Partners ', '23479_1.jpg', 1),
(9, 9, 'Our Partners ', '32548_2.jpg', 1),
(10, 9, 'Our Partners ', '19085_3.jpg', 1),
(11, 9, 'Our Partners ', '25666_4.jpg', 1),
(12, 9, 'Our Partners ', '31827_5.jpg', 1),
(13, 9, 'Our Partners ', '27024_6.jpg', 1),
(14, 9, 'Our Partners ', '649_7.jpg', 1);

-- --------------------------------------------------------

--
-- Table structure for table `imp_notice`
--

CREATE TABLE IF NOT EXISTS `imp_notice` (
  `in_id` int(11) NOT NULL AUTO_INCREMENT,
  `em_id` varchar(255) NOT NULL,
  `in_batch` varchar(255) NOT NULL,
  `in_title` varchar(1000) NOT NULL,
  `in_date` datetime NOT NULL,
  `in_up_date` datetime NOT NULL,
  `in_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`in_id`),
  KEY `em_id` (`em_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `imp_notice`
--

INSERT INTO `imp_notice` (`in_id`, `em_id`, `in_batch`, `in_title`, `in_date`, `in_up_date`, `in_status`) VALUES
(1, 'anilk.acs@gmail.com', '54', 'First Notice to all', '2016-12-03 00:00:00', '2016-12-03 11:00:18', 1),
(3, 'anilk.acs@gmail.com', '54', 'CPL GENTEST-5', '2016-12-21 00:00:00', '2016-12-06 07:39:47', 1);

-- --------------------------------------------------------

--
-- Table structure for table `key_answers`
--

CREATE TABLE IF NOT EXISTS `key_answers` (
  `ka_id` int(11) NOT NULL AUTO_INCREMENT,
  `eq_id` int(11) NOT NULL,
  `ka_title` varchar(255) NOT NULL,
  `ka_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`ka_id`),
  KEY `eq_id` (`eq_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=41 ;

--
-- Dumping data for table `key_answers`
--

INSERT INTO `key_answers` (`ka_id`, `eq_id`, `ka_title`, `ka_status`) VALUES
(33, 1, 'one', 0),
(34, 1, 'two', 1),
(35, 1, 'three', 0),
(36, 1, 'four', 0),
(37, 7, 'asd', 0),
(38, 7, 'sdf', 0),
(39, 7, 'dfg', 1),
(40, 7, 'fgh', 0);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `exam_questions`
--
ALTER TABLE `exam_questions`
  ADD CONSTRAINT `exam_questions_ibfk_1` FOREIGN KEY (`ex_id`) REFERENCES `exam` (`ex_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `exam_result`
--
ALTER TABLE `exam_result`
  ADD CONSTRAINT `exam_result_ibfk_1` FOREIGN KEY (`ex_id`) REFERENCES `exam` (`ex_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  ADD CONSTRAINT `exam_result_ibfk_2` FOREIGN KEY (`cd_id`) REFERENCES `cadet` (`cd_email`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `images`
--
ALTER TABLE `images`
  ADD CONSTRAINT `images_ibfk_1` FOREIGN KEY (`al_id`) REFERENCES `album` (`al_id`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `key_answers`
--
ALTER TABLE `key_answers`
  ADD CONSTRAINT `key_answers_ibfk_1` FOREIGN KEY (`eq_id`) REFERENCES `exam_questions` (`eq_id`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
