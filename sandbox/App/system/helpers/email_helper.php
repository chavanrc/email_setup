<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package		CodeIgniter
 * @author		ExpressionEngine Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc.
 * @license		http://codeigniter.com/user_guide/license.html
 * @link		http://codeigniter.com
 * @since		Version 1.0
 * @filesource
 */

// ------------------------------------------------------------------------

/**
 * CodeIgniter Email Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		ExpressionEngine Dev Team
 * @link		http://codeigniter.com/user_guide/helpers/email_helper.html
 */

// ------------------------------------------------------------------------

/**
 * Validate email address
 *
 * @access	public
 * @return	bool
 */
if ( ! function_exists('valid_email'))
{
	function valid_email($address)
	{
		return ( ! preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,6}$/ix", $address)) ? FALSE : TRUE;
	}
}

// ------------------------------------------------------------------------

/**
 * Send an email
 *
 * @access	public
 * @return	bool
 */
if ( ! function_exists('send_email'))
{
	
        function send_email($val)
        {
            $sm = & get_instance();
            $sm->load->library('email');
            $sm->email->from('info@wagonslearning.com', '360 Degree Feedback');
            $sm->email->reply_to('info@wagonslearning.com', '360 Degree Feedback');
            $sm->email->to($val['to']);
            $sm->email->set_mailtype("html");
            $sm->email->subject('Feedback Pending');
            $vm = & get_instance();
            $msg = $vm->load->view('email-temp',$val,TRUE);
            
            $sm->email->message($msg);
            $sm->email->send();
        }
        
        
}


/* End of file email_helper.php */
/* Location: ./system/helpers/email_helper.php */