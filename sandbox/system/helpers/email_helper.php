<?php

/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2016, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2016, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 * CodeIgniter Email Helpers
 *
 * @package		CodeIgniter
 * @subpackage	Helpers
 * @category	Helpers
 * @author		EllisLab Dev Team
 * @link		https://codeigniter.com/user_guide/helpers/email_helper.html
 */
// ------------------------------------------------------------------------

if (!function_exists('valid_email')) {

    /**
     * Validate email address
     *
     * @deprecated	3.0.0	Use PHP's filter_var() instead
     * @param	string	$email
     * @return	bool
     */
    function valid_email($email) {
        return (bool) filter_var($email, FILTER_VALIDATE_EMAIL);
    }

}

// ------------------------------------------------------------------------

if (!function_exists('send_email')) {

    function forgot_password_mail($email, $data) {
        $sm = & get_instance();
        $sm->load->library('email');
        $sm->email->from('contact@wagonslearning.com', 'Wagons');
        $sm->email->reply_to('contact@wagonslearning.com', 'Wagons');
        $sm->email->to($email);
        $sm->email->set_mailtype("html");
        $sm->email->subject('Wagons Password Reset Request');
        $vm = & get_instance();
        $detail['data'] = $data;
        $msg = $vm->load->view('email_temp/forgot-password', $detail, TRUE);
        $sm->email->message($msg);
        $sm->email->send();
    }
    
    
    function new_trainer_mail($email, $data1) {
        $sm = & get_instance();
        $sm->load->library('email');
        $sm->email->from('contact@wagonslearning.com', 'Wagons');
        $sm->email->reply_to('contact@wagonslearning.com', 'Wagons');
        $sm->email->to($email);
        $sm->email->set_mailtype("html");
        $sm->email->subject('New Trainer Created');
        $vm = & get_instance();
        $data['user'] = $data1;
        
        $msg = $vm->load->view('email_temp/new-trainer', $data, TRUE);
        $sm->email->message($msg);
        $sm->email->send();
    }
    
    function trainer_approval_mail($email, $data1) {
        $sm = & get_instance();
        $sm->load->library('email');
        $sm->email->from('contact@wagonslearning.com', 'Wagons');
        $sm->email->reply_to('contact@wagonslearning.com', 'Wagons');
        $sm->email->to($email);
        $sm->email->set_mailtype("html");
        $sm->email->subject('Trainer Approval');
        $vm = & get_instance();
        $data['user'] = $data1;
        
        $msg = $vm->load->view('email_temp/trainer-approval', $data, TRUE);
        $sm->email->message($msg);
        $sm->email->send();
    }
    
    function engagement_request_mail($email, $data) {
        $sm = & get_instance();
        $sm->load->library('email');
        $sm->email->from('contact@wagonslearning.com', 'Wagons');
        $sm->email->reply_to('contact@wagonslearning.com', 'Wagons');
        $sm->email->to($email);
        $sm->email->set_mailtype("html");
        $sm->email->subject('New Engagement Request');
        $vm = & get_instance();
        $detail['data'] = $data;
        $msg = $vm->load->view('email_temp/engagement-request', $detail, TRUE);
        $sm->email->message($msg);
        $sm->email->send();
    }
    
    function request_status_mail($email, $data) {
        $sm = & get_instance();
        $sm->load->library('email');
        $sm->email->from('contact@wagonslearning.com', 'Wagons');
        $sm->email->reply_to('contact@wagonslearning.com', 'Wagons');
        $sm->email->to($email);
        $sm->email->set_mailtype("html");
        $sm->email->subject('New Engagement Request');
        $vm = & get_instance();
        $detail['data'] = $data;
        $msg = $vm->load->view('email_temp/request-status', $detail, TRUE);
        $sm->email->message($msg);
        $sm->email->send();
    }
    
    function props_status_mail($email, $data) {
        $sm = & get_instance();
        $sm->load->library('email');
        $sm->email->from('contact@wagonslearning.com', 'Wagons');
        $sm->email->reply_to('contact@wagonslearning.com', 'Wagons');
        $sm->email->to($email);
        $sm->email->set_mailtype("html");
        $sm->email->subject('New Engagement Request');
        $vm = & get_instance();
        $detail['data'] = $data;
        $msg = $vm->load->view('email_temp/props-status', $detail, TRUE);
        $sm->email->message($msg);
        $sm->email->send();
    }
    
    function form_attach_mail($email, $data) {
        $sm = & get_instance();
        $sm->load->library('email');
        $sm->email->from('contact@wagonslearning.com', 'Wagons');
        $sm->email->reply_to('contact@wagonslearning.com', 'Wagons');
        $sm->email->to($email);
        $sm->email->set_mailtype("html");
        $sm->email->subject('New Engagement Request');
        $vm = & get_instance();
        $detail['data'] = $data;
        $msg = $vm->load->view('email_temp/forms-mail', $detail, TRUE);
        $sm->email->message($msg);
        $sm->email->send();
    }
    
    function bdm_review_mail($email, $data) {
        $sm = & get_instance();
        $sm->load->library('email');
        $sm->email->from('contact@wagonslearning.com', 'Wagons');
        $sm->email->reply_to('contact@wagonslearning.com', 'Wagons');
        $sm->email->to($email);
        $sm->email->set_mailtype("html");
        $sm->email->subject('New Engagement Request');
        $vm = & get_instance();
        $detail['data'] = $data;
        $msg = $vm->load->view('email_temp/request-status', $detail, TRUE);
        $sm->email->message($msg);
        $sm->email->send();
    }


    function send_email($to, $sub, $temp, $val, $type, $od_id = FALSE) {
        $sm = & get_instance();
        $sm->load->library('email');
        $sm->email->from('info@giftwithluv.com', 'Giftwithluv');
        $sm->email->reply_to('info@giftwithluv.com', 'Giftwithluv');
        $sm->email->to($to);
        $sm->email->set_mailtype("html");
        $sm->email->subject($sub);
        $vm = & get_instance();
        $data['pd'] = $val;
        $data['ff'] = $type;
        $data['saler'] = $to;
        $data['sub'] = $sub;
        $data['od_id'] = $od_id;
        $msg = $vm->load->view('email_temp/' . $temp, $data, TRUE);
        $sm->email->message($msg);
        $sm->email->send();
    }

}

function custome_encode($data) {
    $string = strrev($data);
    $result = '';
    $big = array('', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
    $small = array('', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        '~', '`', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '/', '-', '[', ']', '{', '}', ';', '"', ':', ',', '.', '/', '<', '>', '?','0','1','2','3','4','5','6','7','8','9');

    $script = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"), -100);

    for ($i = 0, $k = strlen($string); $i < $k; $i++) {
        $char = substr($string, $i, 1);
        $key = array_search($char, $small);
        $key1 = array_search($char, $big);
        if ($key) {
            $result .= substr($script, $key, 2) . $key;
        }
        if ($key1) {
            $result .= substr($script, $key1, 2) . $key1 . 'C';
        }
    }

    return $result . 'an';
}

function custome_decode($data) {
    
    $big = array('', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
    $small = array('', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        '~', '`', '!', '@', '#', '$', '%', '^', '&',  '*', '(', ')', '-', '_', '=', '+', '/', '-', '[', ']', '{', '}', ';', '"', ':', ',', '.', '/', '<', '>', '?','0','1','2','3','4','5','6','7','8','9');

    
    $result2 = '';
    for ($i = 0, $k = strlen($data); $i < $k; $i++) {
        $char = substr($data, $i, 1);
        if (!is_numeric($char) && !ctype_upper($char)) {
            $insert = '-';
        } else {
            $insert = $char;
        }

        $result2 .=$insert;
    }
    $result3 = explode('-', $result2);
    $data2 = '';
    foreach ($result3 as $rs) {
        if (!empty($rs)) {
            if (is_numeric($rs)) {
                $data2 .= $small[$rs];
            } else {
                $rs_data = explode('C', $rs);
                $data2 .= $big[$rs_data[0]];
            }
        }
    }
    
    return strrev($data2);
}
