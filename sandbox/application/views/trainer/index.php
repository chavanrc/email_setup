<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Trainer | Dashboard</title>
        <?php include 'css_files.php'; ?>
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Trainer Dashboard</h3>

                    </div>
                    <div class="page-title title-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-9">

                   




                    <div id='calendar'></div>
                </div>
            </div>
        </div>

        <?php
        include 'js_files.php';
        $program = $TR->trainer_model->get_progrom_date($this->session->userdata('t_code'));
        ?>

        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/moment.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/jquery.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.js'></script>
        
        <script type="text/javascript">

            $(document).ready(function () {

                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek,agendaDay,listWeek'
                    },
                    defaultDate: '<?php echo date('Y-m-d'); ?>',
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    events: [
<?php
if (!empty($program)) {
    foreach ($program as $pm_data) {
        ?>
                                {
                                    url: '<?php echo base_url(); ?>trainer/program_details/<?php echo $pm_data->project_id; ?>',
                                                        title: '<?php echo $pm_data->project_title; ?>',
                                                        start: '<?php echo $pm_data->training_date_from; ?>',
                                                        end: '<?php echo $pm_data->trainer_date_to; ?>',
                                                    },
        <?php
    }
}
?>
                                        ]
                                    });



                                });

        </script>

    </body>
</html>