<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Trainer | Program Details</title>
        <?php include 'css_files.php'; ?>
        <style>
            .panel-body{
                line-height:25px;
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>

            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3><?php echo $program[0]->project_title; ?></h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <?php
                        $request = $TR->trainer_model->check_request($program[0]->project_id, $this->session->userdata('t_code'));

                        if (empty($request)) {
                            ?>
                            <a href="#" class="btn btn-primary send-request-btn" pid="<?php echo $program[0]->project_id; ?>"><i class="fa fa-thumbs-up"></i> Interested</a>
                            <?php
                        } else {
                            if ($request[0]->status == '2') {
                                ?>
                                <span class="label label-success">Request Sent</span>
                                <?php
                            } else if ($request[0]->status != '2' && $request[0]->admin_approved == '0') {
                                ?>
                                <span class="label label-info">Review is Pending</span>
                                <?php
                            } else {
                                ?>
                                <span class="label label-warning">Rejected</span>
                                <?php
                            }
                        }
                        ?>
                        <a href="<?php echo base_url(); ?>trainer/new_programs" class="btn btn-danger"><i class="fa fa-backward"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-info"></i> Programs Details </h2>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">Program Title <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->project_title; ?></div>
                            <div class="clearfix"></div>
                            
                            <div class="col-md-4">Location <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->location_of_training; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Start Date <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->training_start_date; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Duration <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->training_duration; ?> Days</div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Skill Sets <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->trainer_skillsets; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Participants <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->no_of_participants; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Objectives <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->objective_of_training; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Props Required <span class="pull-right">:</span></div><div class="col-md-8"><?php if ($program[0]->props == 1) {
                            echo 'Yes';
                        } else {
                            echo 'No';
                        }; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Stay Arrangements <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->stay_arrangement; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Travel Arrangements <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->travel_arrangement; ?></div>
                            <div class="clearfix"></div>

                            <hr/>
                            <?php
                            $content = $this->projectmanager_model->get_project_content($program[0]->project_id);
                            if (!empty($content)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Content Title</th>
                                            <th>Uploaded By</th>
                                            <th>Uploaded Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($content as $cnt_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $cnt_data->filename; ?></td>
                                                <td><?php echo $cnt_data->name; ?> - <?php echo $cnt_data->user_type; ?></td>
                                                <td><?php echo date_formate_short($cnt_data->upload_date); ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $cnt_data->file_path; ?>" class="btn btn-primary"><i class="fa fa-download"></i> View</a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>

                        </div>


                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="request-wrap" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Send Request</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Your Comment</label>
                            <textarea class="form-control" id="comment"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary confirm-request">Confirm</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.assign-pm-btn').click(function (e) {
                    e.preventDefault();
                    $('#assign-pm-wrap').modal('show');
                    var cid = $(this).attr('cid');
                    $('#cid').val(cid);
                });


                $("#add-content-form").validate({
                    rules: {
                        title: "required",
                        content: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });
                
                $('.confirm-request').click(function(){
                    var pname = '<?php echo $program[0]->project_title; ?>';
                    var pid = '<?php echo $program[0]->project_id; ?>';
                    var code = "<?php echo $this->session->userdata('t_code'); ?>";
                    var comment = $('#comment').val();
                    $('.page_spin').show();
                    var dataString = "pname="+pname+"&pid=" + pid + "&code=" + code + "&comment="+comment+"&page=trainer_enagage_request";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>trainer/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                });

                $('.send-request-btn').click(function (e) {
                    $('#request-wrap').modal('show');
                   
                });


            });
        </script>

    </body>
</html>