<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function index() {
        $this->check_sess();
        $data['page_url'] = 'Dashboard';
        $this->load->view('admin/index', $data);
    }

    public function page_map($pr, $sc) {
        $this->$pr($sc);
    }

    public function check_sess() {
        if (!$this->session->userdata('admin')) {
            redirect('admin/login');
        }
    }

    public function login() {
        
        $this->load->view('admin/login');
    }

    public function logout() {
        $this->session->unset_userdata('admin', '');
        $this->session->unset_userdata('name', '');
        $this->session->unset_userdata('user_id', '');
        $this->session->unset_userdata('user_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function add_client() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Clients';
        $data['msg'] = 0;
        if (isset($_POST['fname'])) {
            $data['msg'] = $this->admin_model->save_client();
        }
        $data['pm'] = $this->admin_model->get_pm();
        $this->load->view('admin/add-clients', $data);
    }

    public function clients() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Clients';
        $data['client'] = $this->admin_model->get_client();
        $data['pm'] = $this->admin_model->get_pm();
        $this->load->view('admin/clients', $data);
    }

    public function upate_client($id = NULL) {
        $this->check_sess();
        $this->load->model('admin_model');
        if (!empty($id)) {
            $data['msg'] = 0;
            $data['page_url'] = 'Clients';
            if (isset($_POST['fname'])) {
                $data['msg'] = $this->admin_model->update_client();
            }
            $data['client'] = $this->admin_model->get_single_client($id);
            $this->load->view('admin/edit-client', $data);
        }
    }

    public function add_pm() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Clients';
        $data['msg'] = 'new';
        if (isset($_POST['fname'])) {
            $data['msg'] = $this->admin_model->save_pm();
        }
        $data['client'] = $this->admin_model->get_client_assign_pending();
        $this->load->view('admin/add-pm', $data);
    }

    public function project_manager() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Project Managers';
        $data['pm'] = $this->admin_model->get_pm();
        $data['client'] = $this->admin_model->get_client_assign_pending();
        $this->load->view('admin/project-manager', $data);
    }

    public function edit_pm($id) {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Clients';
        $data['msg'] = 'new';
        if (isset($_POST['fname'])) {
            $data['msg'] = $this->admin_model->update_pm();
        }
        $data['pm'] = $this->admin_model->get_single_pm($id);
        $this->load->view('admin/edit-pm', $data);
    }

    public function trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['trainer'] = $this->admin_model->get_trainer();
        $this->load->view('admin/trainers', $data);
    }

    public function add_trainer() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['msg'] = "new";
        if (isset($_POST['pname'])) {
            $data['msg'] = $this->admin_model->save_trainer();
        }
        $data['state'] = $this->admin_model->get_state();
        $this->load->view('admin/add-trainer', $data);
    }

    public function trainer_profile($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['trainer'] = $this->admin_model->get_single_trainer($id);
            $this->load->view('admin/trainer-profile', $data);
        }
    }

    public function update_trainer($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            if (isset($_POST['pname'])) {
                $data['msg'] = $this->admin_model->update_trainer();
            }
            $data['trainer'] = $this->admin_model->get_single_trainer($id);
            $data['state'] = $this->admin_model->get_state();
            $data['city'] = $this->admin_model->get_city_list($data['trainer'][0]->state);
            $this->load->view('admin/update-trainer', $data);
        }
    }
    
    public function trainer_experience($id)
    {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $data['exp'] = $this->admin_model->get_trainer_experience($id);
            $this->load->view('admin/trainer-experience', $data);
        }
    }
    
    public function trainer_program_engagment($id)
    {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $data['engage'] = $this->admin_model->get_trainer_engagement($id);
            $this->load->view('admin/trainer-program-engagement', $data);
        }
    }

    public function ajax_page() {
        $this->load->model('admin_model');
        echo $this->admin_model->$_POST['page']($_POST);
    }

}
