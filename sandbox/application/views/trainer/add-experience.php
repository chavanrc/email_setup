<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Trainer | Work Experience</title>
        <?php include 'css_files.php'; ?>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/multiselect/bootstrap-multiselect.css">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Update Training Experience</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>trainer/trainer_profile/<?php echo $trainer[0]->user_code; ?>" class="btn btn-danger"><i class="fa fa-backward"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <?php
                    if($msg==1)
                    {
                        ?>
                    <div class="alert alert-success col-md-6 col-md-offset-3">
                        Training Experience Updated Successfully.
                    </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-edit"></i> <?php echo $trainer[0]->name; ?> </h2>
                        </div>
                        <div class="panel-body">
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-work-form">
                                <input type="hidden" value="<?php echo $trainer[0]->user_code; ?>" name="user"/>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">

                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Area of Training </span>
                                        <select class="form-control"  id="training_area" name="training_area">
                                            <option value=""> - Select - </option>
                                           <?php
                                           $area = array('Assessment and Development',
                                               'Behavioural/Attitude Based',
                                               'Certified Coach/Consultant',
                                               'Knowledge Based',
                                               'Leadership Development',
                                               'Managerial Development',
                                               'Outbound Training',
                                               'Skill Based');
                                           foreach($area as $ar_data)
                                           {
                                               ?>
                                            <option value="<?php echo $ar_data; ?>"><?php echo $ar_data; ?></option>
                                                <?php
                                           }
                                           ?>
                                        </select>
                                    </div>

                                </div>
                                
                                
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Program </span>
                                        <select class="form-control"  id="program" name="program" >
                                           <option value=""> - Select - </option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Title</span>
                                        <input type="text" name="title" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-4 col-sm-3 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Company</span>
                                        <input type="text" name="company" class="form-control">
                                    </div>
                                </div>
                                <?php
                                $industry = array('Advertising','Event Management','Agriculture/Dairy','Architecture',
                                    'Automobiles',
                                    'Banking/Financial',
                                    'BPO',
                                    'Chemicals',
                                    'Construction',
                                    'Logistics',
                                    'Education/Training',
                                    'Export/Import',
                                    'Fertilizers',
                                    'FMCG',
                                    'Fresher/Trainee',
                                    'Gems and Jewellery',
                                    'Heavy Machinery',
                                    'Hotels/Restaurants',
                                    'Industrial Prods',
                                    'Energy',
                                    'Insurance',
                                    'IT-Hardware',
                                    'IT-Products',
                                    'Legal',
                                    'Media',
                                    'Healthcare',
                                    'NGO',
                                    'Automation',
                                    'Oil and Gas',
                                    'Paper',
                                    'Pharma',
                                    'Packaging',
                                    'Real Estate',
                                    'Recruitment Firm',
                                    'Retailing',
                                    'Security',
                                    'Electronics',
                                    'Shipping/Marine',
                                    'Telecom',
                                    'Textiles'
                                    );
                                ?>
                                <div class="form-group col-md-4 col-sm-3 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Industry </span>
                                        <select class="form-control"  id="industry" name="industry[]" multiple="multiple" style="width: 100%">
                                           <?php
                                           foreach($industry as $in_data)
                                           {
                                               ?>
                                            <option value="<?php echo $in_data; ?>"><?php echo $in_data; ?></option>
                                                <?php
                                           }
                                           ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script src="<?php echo base_url(); ?>assets/js/multiselect/bootstrap-multiselect.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#industry').multiselect({
                    nonSelectedText: '- Select Industry -'
                });
                
                $("#add-work-form").validate({
                    rules: {
                        training_area:"required",
                        title:"required",
                        program:"required",
                        company:"required",
                        industry:"required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });
                
                $('#training_area').change(function (e) {
                    var area = $(this).val();
                    if(area!='')
                    {
                        $('#program option').remove();
                        $('#program').append('<option value=""> - Select - </option>')
                    $('.page_spin').show();
                    var dataString = "area=" + area + "&page=get_program_title";
                    $.ajax({
                        type: "POST",
                        dataType : "JSON",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            $.each(data, function (i, item) {
                                $('#program').append("<option value=" +item.program +">" + item.program + "</option>");
                            });
                        }, //success fun end
                    });//ajax end
                    }
                });
                
            });
        </script>

    </body>
</html>