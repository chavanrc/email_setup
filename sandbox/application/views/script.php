<script type="text/javascript">
    $(document).ready(function () {
        $('#tCheckin').click(function (e) {
            e.preventDefault();
            var tid = $(this).attr('tid');
            var pid = $(this).attr('pid');
            alert(tid);
            $('.page_spin').show();
            var dataString = "tid="+tid+"&pid="+pid+"&page=trainer_checkin";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>home/ajax_page",
                data: dataString,
                success: function (data) {
                    $('.page_spin').hide();
                    window.location.href = "home/dashboard/<?php echo $userData['projectId']; ?>/<?php echo $userData['trainerId']; ?>";
                }, //success fun end
            }); //ajax end

        });
        
        $('#preTrigger').click(function (e) {
            e.preventDefault();
            var tid = $(this).attr('tid');
            var pid = $(this).attr('pid');
            $('.page_spin').show();
            var dataString = "tid="+tid+"&pid="+pid+"&page=trigger_preTest";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>home/ajax_page",
                data: dataString,
                success: function (data) {
                    $('.page_spin').hide();
                    window.location.href = "<?php echo base_url(); ?>home/pre_questions/<?php echo $userData['projectId']; ?>";
                }, //success fun end
            }); //ajax end

        });
        
        $('#postTrigger').click(function (e) {
            e.preventDefault();
            var tid = $(this).attr('tid');
            var pid = $(this).attr('pid');
            $('.page_spin').show();
            var dataString = "tid="+tid+"&pid="+pid+"&page=trigger_postTest";
            $.ajax({
                type: "POST",
                url: "<?php echo base_url(); ?>home/ajax_page",
                data: dataString,
                success: function (data) {
                    $('.page_spin').hide();
                    window.location.href = "<?php echo base_url(); ?>home/post_questions/<?php echo $userData['projectId']; ?>";
                }, //success fun end
            }); //ajax end

        });
        
    });
</script>