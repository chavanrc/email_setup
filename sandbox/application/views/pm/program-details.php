<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Project Manager | Program Details</title>
        <?php include 'css_files.php'; ?>
        <style>
            .panel-body{
                line-height:25px;
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Programs</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#"><i class="fa fa-info"></i> Details</a></li>
                            <li><a href="<?php echo base_url(); ?>projectmanager/engagement_requests/<?php echo $program[0]->project_id; ?>"><i class="fa fa-paper-plane"></i> Engagement Requests (
                                    <?php
                                    $engage_count = $CI->projectmanager_model->count_engagement_request($program[0]->project_id);
                                    echo $engage_count;
                                    ?>
                                    )</a></li>
                            

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
            
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3><?php echo $program[0]->project_title; ?></h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>projectmanager/upcoming_programs" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                
                
                
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-info"></i> Programs Details </h2>
                            <a href="<?php echo base_url(); ?>projectmanager/edit_program/<?php echo $program[0]->project_id; ?>" class="btn btn-sm btn-info pull-right" style="margin-top:-24px;"><i class="fa fa-edit"></i> Edit</a>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">Program Title <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->project_title; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Client <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->client_name; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Location <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->location_of_training; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Start Date <span class="pull-right">:</span></div><div class="col-md-8"><?php echo date_formate_short($program[0]->training_start_date); ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Duration <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->training_duration; ?> Days</div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Skill Sets <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->trainer_skillsets; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Participants <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->no_of_participants; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Objectives <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->objective_of_training; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Props Required <span class="pull-right">:</span></div><div class="col-md-8"><?php  if($program[0]->props==1){ echo 'Yes'; } else { echo 'No'; }; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Stay Arrangements <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->stay_arrangement; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Travel Arrangements <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->travel_arrangement; ?></div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                
                <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-graduation-cap"></i> Trainers </h2>
                            </div>
                            <div class="panel-body">
                                
                                <?php
                                $tn = $this->projectmanager_model->get_program_trainers($program[0]->project_id);
                                if (!empty($tn)) {
                                    ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Trainer</th>
                                                <th>Date From</th>
                                                <th>Date To</th>
                                                <th>Payment</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($tn as $tn_data) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $tn_data->name; ?></td>
                                                    <td><?php echo date_formate_short($tn_data->training_date_from); ?></td>
                                                    <td><?php echo date_formate_short($tn_data->trainer_date_to); ?></td>
                                                    <td><?php echo $tn_data->amount; ?></td>
                                                    <td></td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list-alt"></i> Contents </h2>
                            <a href="#" class="btn btn-sm btn-info pull-right upload-content-btn" style="margin-top:-24px;"><i class="fa fa-upload"></i> Upload</a>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 'Content') {
                                ?>
                                <div class="alert alert-success col-md-6 col-md-offset-3">
                                    Content Added Successfully.
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            $content = $this->projectmanager_model->get_project_content($program[0]->project_id, $program[0]->client_id);
                            if (!empty($content)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Content Title</th>
                                            <th>For</th>
                                            <th>Uploaded By</th>
                                            <th>Uploaded Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($content as $cnt_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $cnt_data->filename; ?></td>
                                                <td>
                                                    <?php
                                                    if ($cnt_data->project_id == '0') {
                                                        echo 'Client';
                                                    } else {
                                                        echo 'Program';
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php echo $cnt_data->name; ?> - <?php echo $cnt_data->user_type; ?></td>
                                                <td><?php echo date_formate_short($cnt_data->upload_date); ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $cnt_data->file_path; ?>" class="btn btn-primary"><i class="fa fa-download"></i> View</a>
                                                    <?php
                                                    if ($cnt_data->upload_by == $this->session->userdata('pm_code')) {
                                                        ?>
                                                        <a href="#" class="btn btn-sm btn-danger remove-content" rid="<?php echo $cnt_data->id; ?>"><i class="fa fa-trash"></i></a>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>


                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list-alt"></i> Forms </h2>
                            <!--<a href="#" class="btn btn-sm btn-info pull-right upload-form-btn" style="margin-top:-24px;"><i class="fa fa-upload"></i> Upload</a>-->
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 'Form') {
                                ?>
                                <div class="alert alert-success col-md-6 col-md-offset-3">
                                    Form Added Successfully.
                                </div>
                                <?php
                            }
                            ?>
                            <?php
                            $form = $this->projectmanager_model->get_project_form($program[0]->project_id);
                            if (!empty($form)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Form Type</th>
                                            <th>Uploaded By</th>
                                            <th>Program Date</th>
                                            <th>Uploaded Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($form as $fm_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $fm_data->form_type; ?></td>

                                                <td><?php echo $fm_data->name; ?> - <?php echo $fm_data->user_type; ?></td>
                                                <td><?php echo date_formate_short($fm_data->program_date); ?></td>
                                                <td><?php echo date_formate_short($fm_data->upload_date); ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>assets/upload/form/<?php echo $fm_data->form_file_name; ?>" class="btn btn-primary"><i class="fa fa-download"></i> View</a>
                                                    <?php
                                                    if ($fm_data->upload_by == $this->session->userdata('pm_code')) {
                                                        ?>
                                                        <a href="#" class="btn btn-sm btn-danger remove-form" rid="<?php echo $fm_data->id; ?>"><i class="fa fa-trash"></i></a>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <?php
                if($program[0]->props==1)
                {
                ?>

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-star"></i> Props </h2>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 'Props') {
                                ?>
                                <div class="alert alert-success col-md-6 col-md-offset-3">
                                    Props Updated Successfully.
                                </div>
                                <?php
                            }
                            ?>
                            <?php
                            $props = $this->projectmanager_model->get_project_props($program[0]->project_id);
                            if (!empty($props)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Quantity</th>
                                            <th>Uploaded By</th>
                                            <th>Requested Date</th>
                                            <th>Status</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($props as $p_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $p_data->prop_name; ?></td>
                                                <td><?php echo $p_data->quantity; ?></td>

                                                <td><?php echo $p_data->name; ?> - <?php echo $p_data->user_type; ?></td>
                                                <td><?php echo date_formate_short($p_data->requested_date); ?></td>
                                                <td><?php
                                                    if ($p_data->delivery_status == '0') {
                                                        echo 'Not Shipped';
                                                    }
                                                    if ($p_data->delivery_status == '2') {
                                                        echo 'Shipped';
                                                    }
                                                    if ($p_data->delivery_status == '1') {
                                                        echo 'Delivered';
                                                    }
                                                    ?></td>
                                                <td>
                                                    
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                
                <?php
                }
                if ($program[0]->stay_arrangement == 'Wagons' || $program[0]->stay_arrangement == 'Client') {
                    ?>
                    <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-bed"></i> Stay Arrangements </h2>
                            </div>
                            <div class="panel-body">
                                <?php
                                if ($msg == 'Stay') {
                                    ?>
                                    <div class="alert alert-success col-md-6 col-md-offset-3">
                                        Stay details Updated Successfully.
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php
                                $stay = $this->projectmanager_model->get_stay_details($program[0]->project_id);
                                if (!empty($stay)) {
                                    ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Trainer</th>
                                                <th>Hotel</th>
                                                <th>Check in</th>
                                                <th>Check out</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($stay as $st_data) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $st_data->name; ?></td>
                                                    <td><?php echo $st_data->hotel_name; ?></td>
                                                    <td><?php echo date_formate($st_data->checkin_time); ?></td>
                                                    <td><?php echo date_formate($st_data->checkout_time); ?></td>

                                                    <td>
                                                        
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                
                     <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-cab"></i> Travel Arrangements </h2>
                            </div>
                            <div class="panel-body">
                                <?php
                                if ($msg == 'Travel') {
                                    ?>
                                    <div class="alert alert-success col-md-6 col-md-offset-3">
                                        Travel details Updated Successfully.
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php
                                $travel = $this->projectmanager_model->get_travel_details($program[0]->project_id);
                                if (!empty($travel)) {
                                    ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Trainer</th>
                                                <th>Date</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Mode</th>
                                                <th>Ticket</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($travel as $tv_data) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $tv_data->name; ?></td>
                                                    <td><?php echo date_formate_short($tv_data->date); ?></td>
                                                    <td><?php echo $tv_data->from_loc; ?></td>
                                                    <td><?php echo $tv_data->to_loc; ?></td>
                                                    <td><?php echo $tv_data->mode; ?></td>
                                                    <td><a href="<?php echo base_url(); ?>assets/upload/travel/<?php echo $tv_data->screenshot; ?>" target="_blank">Ticket</a></td>

                                                    <td>
                                                        
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div>
        </div>

        <div class="modal fade" id="props_add_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Props Needed</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="props-add-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="props_user" value="<?php echo $this->session->userdata('pm_code'); ?>"/>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Props Name <sup>*</sup></span>
                                    <input type="text" name="pname"  class="form-control">
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Quantity <sup>*</sup></span>
                                    <input type="text" name="qnt"  class="form-control">
                                </div>
                            </div>


                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="upload_content_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Upload Content</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="add-content-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="client" value="<?php echo $program[0]->client_id; ?>"/>
                            <input type="hidden" name="content_user" value="<?php echo $this->session->userdata('pm_code'); ?>"/>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Content Title <sup>*</sup></span>
                                    <input type="text" name="title" placeholder="Content Title" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">

                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Upload Content <sup>*</sup></span>
                                    <input type="file" name="content" class="form-control">
                                </div>

                            </div>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Content For <sup>*</sup></span>
                                    <select class="form-control" name="ctype">
                                        <option value="project">Program</option>
                                        <option value="client">Client</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="upload_form_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Upload Forms</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="add-forms-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="form_user" value="<?php echo $this->session->userdata('pm_code'); ?>"/>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Form Type <sup>*</sup></span>
                                    <input type="text" name="ftype" placeholder="Form Type" class="form-control">
                                </div>
                            </div>
                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Date <sup>*</sup></span>
                                    <input type="date" name="fdate"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Upload Form <sup>*</sup></span>
                                    <input type="file" name="content" class="form-control">
                                </div>
                            </div>

                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.upload-form-btn').click(function (e) {
                    e.preventDefault();
                    $('#upload_form_wraper').modal('show');
                });

                $('.props-add-btn').click(function (e) {
                    e.preventDefault();
                    $('#props_add_wraper').modal('show');
                });

                $('.upload-content-btn').click(function (e) {
                    e.preventDefault();
                    $('#upload_content_wraper').modal('show');
                });


                $('.assign-pm-btn').click(function (e) {
                    e.preventDefault();
                    $('#assign-pm-wrap').modal('show');
                    var cid = $(this).attr('cid');
                    $('#cid').val(cid);
                });

                $("#props-add-form").validate({
                    rules: {
                        pname: "required",
                        qnt: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#add-content-form").validate({
                    rules: {
                        title: "required",
                        content: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#add-forms-form").validate({
                    rules: {
                        content: "required",
                        ftype: "required",
                        fdate: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });


                $('.prop-status').click(function (e) {
                    e.preventDefault();
                    var sid = $(this).attr('sid');
                    var status = $(this).attr('status');
                    $(this).remove();
                    $('.page_spin').show();
                    var dataString = "sid=" + sid + "&status=" + status + "&page=update_props_status";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            alert('Status updated');
                        }, //success fun end
                    });//ajax end
                });


                $('.remove-content').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove content ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_content";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.remove-form').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove form ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_form";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.remove-props').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove props ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_props";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

            });
        </script>

    </body>
</html>