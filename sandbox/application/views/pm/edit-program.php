<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Project Manager | Edit Program</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/datepicker/jquery.datetimepicker.css">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Program - Update</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>projectmanager/program_details/<?php echo $program[0]->project_id; ?>" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <?php
                if ($msg>0) {
                    ?>
                    <div class="alert alert-success col-md-4 col-md-offset-4">
                         Program updated Successfully.
                    </div>
                    
                
                    <?php
                }
                
                ?>

                

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-plus"></i> Update Program </h2>
                        </div>
                        <div class="panel-body">
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-program-form">
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Client <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="client">
                                            <option value=""> - Select -</option>
                                            <?php
                                            if(!empty($client))
                                            {
                                                foreach($client as $cl_data)
                                                {
                                                    ?>
                                                    <option value="<?php echo $cl_data->client_id; ?>" <?php if($program[0]->client_id==$cl_data->client_id){ echo 'selected'; } ?>> <?php echo $cl_data->client_name; ?> </option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Program Title <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="hidden" value="<?php echo $program[0]->project_id; ?>" name="pid">
                                        <input type="text" name="pname" value="<?php echo $program[0]->project_title; ?>" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Location <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="location" value="<?php echo $program[0]->location_of_training; ?>" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Objective <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea name="objective" class="form-control"><?php echo $program[0]->objective_of_training; ?></textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Start Date <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="date" id="sdate" name="sdate" value="<?php $sdate = explode(' ', $program[0]->training_start_date); echo $sdate[0]; ?>"  placeholder="yy-mm-dd" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                               
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Duration <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="duration" value="<?php echo $program[0]->training_duration; ?>" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Trainer Skill Sets <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea name="skill_set" class="form-control"><?php echo $program[0]->trainer_skillsets; ?></textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">No. of Participants <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="participants" value="<?php echo $program[0]->no_of_participants; ?>" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Participants Level <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="participant_level">
                                            <option value=""> - Select -</option>
                                            <option value="Beginners" <?php if($program[0]->participant_level=='Beginners'){ echo 'selected'; } ?>> Beginners </option>
                                            <option value="Juniors" <?php if($program[0]->participant_level=='Juniors'){ echo 'selected'; } ?>> Juniors </option>
                                            <option value="Intermediate" <?php if($program[0]->participant_level=='Intermediate'){ echo 'selected'; } ?>> Intermediate </option>
                                            <option value="Advanced" <?php if($program[0]->participant_level=='Advanced'){ echo 'selected'; } ?>> Advanced </option>
                                            <option value="Highly Experienced" <?php if($program[0]->participant_level=='Highly Experienced'){ echo 'selected'; } ?>> Highly Experienced </option>
                                            <option value="Above 40 years Staff" <?php if($program[0]->participant_level=='Above 40 years Staff'){ echo 'selected'; } ?>> Above 40 years Staff </option>
                                            <option value="Unskilled Labor" <?php if($program[0]->participant_level=='Unskilled Labor'){ echo 'selected'; } ?>> Unskilled Labor </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Props Required <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="radio" name="prop" <?php if($program[0]->props=='1'){ echo 'checked'; } ?> value="1"> Yes 
                                        <input type="radio" name="prop" <?php if($program[0]->props=='0'){ echo 'checked'; } ?> value="0" style="margin-left: 25px;"> No 
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Stay Arrangement <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="stay">
                                            <option value=""> - Select -</option>
                                            <option value="Wagons" <?php if($program[0]->stay_arrangement=='Wagons'){ echo 'selected'; } ?>> Wagons </option>
                                            <option value="Client" <?php if($program[0]->stay_arrangement=='Client'){ echo 'selected'; } ?>> Client </option>
                                            <option value="Trainer" <?php if($program[0]->stay_arrangement=='Trainer'){ echo 'selected'; } ?>> Trainer </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Travel Arrangement <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="travel">
                                            <option value=""> - Select -</option>
                                            <option value="Wagons" <?php if($program[0]->travel_arrangement=='Wagons'){ echo 'selected'; } ?>> Wagons </option>
                                            <option value="Client" <?php if($program[0]->travel_arrangement=='Client'){ echo 'selected'; } ?>> Client </option>
                                            <option value="Trainer" <?php if($program[0]->travel_arrangement=='Trainer'){ echo 'selected'; } ?>> Trainer </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php
                
                ?>
            </div>
        </div>

<?php include 'js_files.php'; ?>
        
        <script type="text/javascript">

            $(document).ready(function () {
                
                
                
                
                $("#add-program-form").validate({
                    rules: {
                        pname: "required",
                        client: "required",
                        location: "required",
                        sdate: "required",
                        duration: {
                            required: true,
                            number: true
                        },
                        skill_set: "required",
                        participants: {
                            required: true,
                            number: true
                        },
                        participant_level : "required",
                        stay : "required",
                        travel : "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                
            });

        </script>

    </body>
</html>