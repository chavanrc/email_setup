<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Update Client</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>admin/clients" class="btn btn-danger"><i class="fa fa-backward"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <?php
                if($msg>0)
                {
                ?>
                <div class="alert alert-success col-md-4 col-md-offset-4">
                    Client Updated Successfully.
                </div>
                <?php
                }
                ?>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-edit"></i> <?php echo $client[0]->client_name; ?> </h2>
                        </div>
                        <div class="panel-body">
                            <div class="alert alert-danger login_error" style="display: none;">Wrong Username & Password</div>
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-client-form">
                                <input type="hidden" name="cid" value="<?php echo $client[0]->client_id; ?>">
                                <input type="hidden" name="cimg" value="<?php echo $client[0]->logo; ?>">
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="fname" value="<?php echo $client[0]->client_name; ?>" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Industry <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="industry">
                                            <option value=""> - Select -</option>
                                            <option value="IT" <?php if($client[0]->industry){ echo 'selected'; } ?>> IT </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Address <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="address" value="<?php echo $client[0]->address; ?>" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Change Logo
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="logo" accept='image/*' class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Contact Person 1 <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-user"></i></span>
                                            <input type="text" name="name1" value="<?php echo $client[0]->person1; ?>" class="form-control" placeholder="Name">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                         <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-envelope"></i></span>
                                            <input type="text" name="email1" value="<?php echo $client[0]->email1; ?>" class="form-control" placeholder="Email id">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                         <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-phone"></i></span>
                                            <input type="text" name="mobile1" value="<?php echo $client[0]->phone1; ?>"  class="form-control" placeholder="Mobile Number" >
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Contact Person 2 
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-user"></i></span>
                                            <input type="text" name="name2" value="<?php echo $client[0]->person2; ?>" class="form-control" placeholder="Name" aria-describedby="sizing-addon1">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                         <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-envelope"></i></span>
                                            <input type="text" name="email2" value="<?php echo $client[0]->email2; ?>" class="form-control" placeholder="Email id" aria-describedby="sizing-addon1">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                         <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-phone"></i></span>
                                            <input type="text" name="mobile2" value="<?php echo $client[0]->phone2; ?>" class="form-control" placeholder="Mobile Number" aria-describedby="sizing-addon1">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Payment Credit Days <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="payment" value="<?php echo $client[0]->payment_credit_days; ?>" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">

            $(document).ready(function () {
                $("#add-client-form").validate({
                    rules: {
                        fname:"required",
                        industry:"required",
                        address:"required",
                        name1:"required",
                        email1:{
                            required: true,
                            email: true
                        },
                        mobile1:"required",
                        payment:"required",
                        
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });
            });

        </script>

    </body>
</html>