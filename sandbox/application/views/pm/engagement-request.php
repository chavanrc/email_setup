<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Project Manager | Engagement Requests</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Programs</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li ><a href="<?php echo base_url(); ?>projectmanager/program_details/<?php echo $pid; ?>"><i class="fa fa-info"></i> Details</a></li>
                            <li class="active"><a href="#" ><i class="fa fa-paper-plane"></i> Engagement Requests (
                                    <?php
                                    if(!empty($request))
                                    {
                                        echo count($request);
                                    }
                                    ?>
                                    )</a></li>
                            

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Engagement Requests</h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list"></i> Requests List </h2>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Trainer Comment</th>
                                        <th>BDM Comment</th>
                                        <th>client Comment</th>
                                        <th>Apply Date</th>
                                        <th>Status</th>
                                        <th>Status Date</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($request)) {
                                        $num = 0;
                                        foreach ($request as $pm_data) {
                                            $num++;
                                            ?>
                                            <tr>
                                                <td><?php echo $num; ?></td>
                                                <td><a href="<?php echo base_url(); ?>projectmanager/trainer_profile/<?php echo $pm_data->user_code; ?>"><?php echo $pm_data->name; ?></a></td>
                                                <td><?php echo $pm_data->trainer_comments; ?></td>
                                                <td><?php echo $pm_data->bdm_comments; ?></td>
                                                <td><?php echo $pm_data->client_comments; ?></td>
                                                <td><?php echo date_formate_short($pm_data->apply_date); ?></td>
                                                <td class="status_<?php echo $pm_data->id; ?>"><?php
                                                    if ($pm_data->status == '2') {
                                                        ?>
                                                        <span class="label label-warning">Pending</span>
                                                        <?php
                                                    }
                                                    if ($pm_data->status != '2' && $pm_data->admin_approved == '0') {
                                                        ?>
                                                        <span class="label label-primary">Review Pending</span>
                                                        <?php
                                                    }
                                                    if ($pm_data->status == '0' && $pm_data->admin_approved == '0') {
                                                        ?>
                                                        <span class="label label-danger">Rejected</span>
                                                        <?php
                                                    }
                                                    if ($pm_data->status == '1' && $pm_data->admin_approved == '1') {
                                                        ?>
                                                        <span class="label label-success">Approved</span>
                                                        <?php
                                                    }
                                                    ?></td>
                                                <td>
                                                    <?php
                                                    if ($pm_data->status == '1' && $pm_data->admin_approved == '0') {
                                                        echo date_formate_short($pm_data->engage_date);
                                                    }
                                                    if ($pm_data->status == '0' && $pm_data->admin_approved == '0') {
                                                        echo date_formate_short($pm_data->reject_date);
                                                    }
                                                    if ($pm_data->status == '1' && $pm_data->admin_approved == '1') {
                                                        echo date_formate_short($pm_data->approval_date);
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($pm_data->status == '2') {
                                                        ?>
                                                        <a href="#" class="btn btn-sm btn-info accept-btn" rid="<?php echo $pm_data->id; ?>">Accept</a>
                                                        <a href="#" class="btn btn-sm btn-danger reject-btn" uid="<?php echo $pm_data->user_code; ?>" rid="<?php echo $pm_data->id; ?>">Reject</a>
                                                        <?php
                                                    }
                                                    if ($pm_data->status != '2' && $pm_data->admin_approved == '0') {
                                                        if($pm_data->status !='3')
                                                        {
                                                        ?>
                                                        <a href="#" class="btn btn-sm btn-info bdm-btn" uid="<?php echo $pm_data->user_code; ?>" pid="<?php echo $pm_data->project_id; ?>" rid="<?php echo $pm_data->id; ?>">Send to BDM</a>
                                                        <?php
                                                        }
                                                        if($pm_data->status !='4')
                                                        {
                                                        ?>
                                                        <a href="#" class="btn btn-sm btn-primary client-btn" uid="<?php echo $pm_data->user_code; ?>" pid="<?php echo $pm_data->project_id; ?>" rid="<?php echo $pm_data->id; ?>">Send to Client</a>
                                                        <?php
                                                        }
                                                        ?>
                                                        <a href="#" class="btn btn-sm btn-success approved-btn" uid="<?php echo $pm_data->user_code; ?>" pid="<?php echo $pm_data->project_id; ?>" rid="<?php echo $pm_data->id; ?>">Approve</a>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="approve_form" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Assign Trainer</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="assign-trainer-form">

                        <div class="modal-body">
                            <input type="hidden" id="project" name="project" value=""/>
                            <input type="hidden" id="user" name="user" value=""/>
                            <input type="hidden" id="eid" name="eid" value=""/>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> From Date <sup>*</sup></span>
                                    <input type="date" id="from" name="from" class="form-control">
                                </div>
                            </div>
                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> To Date <sup>*</sup></span>
                                    <input type="date" id="to" name="to"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Payment <sup>*</sup></span>
                                    <input type="text" id="amt" name="amt"  class="form-control">
                                </div>
                            </div>

                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
        
        <div class="modal fade" id="request-wrap" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Status Update</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Your Comment</label>
                            <input type="hidden" name="rid" id="pid">
                            <input type="hidden" name="uid" id="uid">
                            <textarea class="form-control" id="comment"></textarea>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary confirm-request" style="display: none;">Confirm</button>
                        <button type="button" class="btn btn-primary confirm-reject" style="display: none;">Confirm</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
        <div class="modal fade" id="bdm-wrap" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">BDM Review</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="hidden" name="bdm_pid" id="bdm_pid">
                            <input type="hidden" name="bdm_rid" id="bdm_rid">
                            <input type="hidden" name="bdm_uid" id="bdm_uid">
                            <input type="text" class="form-control" name="bdm_name" id="bdm_name">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="bdm_email" id="bdm_email">
                            <span class="bdm-error" style="color:red;display: none;">Email id required</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary bdm-submit-btn">Submit</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
        <div class="modal fade" id="client-wrap" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Client Review</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label>Name</label>
                            <input type="hidden" name="client_pid" id="client_pid">
                            <input type="hidden" name="client_rid" id="client_rid">
                            <input type="hidden" name="client_uid" id="client_uid">
                            <input type="text" class="form-control" name="client_name" id="client_name">
                        </div>
                        <div class="form-group">
                            <label>Email</label>
                            <input type="email" class="form-control" name="client_email" id="client_email">
                            <span class="client-error" style="color:red;display: none;">Email id required</span>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary client-submit-btn">Submit</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {
                
                $('.bdm-btn').click(function(e){
                    e.preventDefault();
                    $('#bdm-wrap').modal('show');
                    var pid = $(this).attr('pid');
                    var rid = $(this).attr('rid');
                    var uid = $(this).attr('uid');
                    $('#bdm_pid').val(pid);
                    $('#bdm_rid').val(rid);
                    $('#bdm_uid').val(uid);
                });
                
                $('.client-btn').click(function(e){
                    e.preventDefault();
                    $('#client-wrap').modal('show');
                    var pid = $(this).attr('pid');
                    var rid = $(this).attr('rid');
                    var uid = $(this).attr('uid');
                    $('#client_pid').val(pid);
                    $('#client_rid').val(rid);
                    $('#client_uid').val(uid);
                });
                
                $('.accept-btn').click(function (e) {
                    var rid = $(this).attr('rid');
                    $('#pid').val(rid);
                    $('#request-wrap').modal('show');
                    $('.confirm-request').show();
                    $('.confirm-reject').hide();
                });
                
                $('.bdm-submit-btn').click(function (e) {
                    e.preventDefault();
                    var rid = $('#bdm_rid').val();
                    var pid = $('#bdm_pid').val();
                    var uid = $('#bdm_uid').val();
                    var email = $('#bdm_email').val();
                    var name = $('#bdm_name').val();
                    if(email=='')
                    {
                        $('.bdm-error').show();
                        return false;
                    }
                    $('.page_spin').show();
                    var dataString = "rid=" + rid + "&pid="+pid+"&uid="+uid+"&email="+email+"&name="+name+"&page=send_to_bdm";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            $('#bdm-wrap').modal('hide');
                            alert('Sent to BDM review');
                            
                        }, //success fun end
                    });//ajax end
                });
                
                $('.client-submit-btn').click(function (e) {
                    e.preventDefault();
                    var rid = $('#client_rid').val();
                    var pid = $('#client_pid').val();
                    var uid = $('#client_uid').val();
                    var email = $('#client_email').val();
                    var name = $('#client_name').val();
                    if(email=='')
                    {
                        $('.client-error').show();
                        return false;
                    }
                    $('.page_spin').show();
                    var dataString = "rid=" + rid + "&pid="+pid+"&uid="+uid+"&email="+email+"&name="+name+"&page=send_to_client";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            $('#client-wrap').modal('hide');
                            alert('Sent to Client review');
                            
                        }, //success fun end
                    });//ajax end
                });
                

                $('.confirm-request').click(function (e) {
                    e.preventDefault();
                    var rid = $('#pid').val();
                    var comment = $('#comment').val();
                    $('.page_spin').show();
                    var dataString = "rid=" + rid + "&comment="+comment+"&page=accept_request";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            alert('Status Updated');
                            $('.status_' + rid + '').children('.label-warning').remove();
                            $('.status_' + rid + '').siblings('td').children('.accept-btn').remove();
                            $('.status_' + rid + '').siblings('td').children('.reject-btn').remove();
                            $('.status_' + rid + '').append('<span class="label label-primary">Accepted</span>');
                        }, //success fun end
                    });//ajax end
                });
                
                $("#assign-trainer-form").validate({
                    rules: {
                        from: "required",
                        to: "required",
                        amt: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        assign_trainer();
                    }
                });
                
                function assign_trainer()
                {
                    $('#approve_form').modal('hide');
                    var from = $('#from').val();
                    var to = $('#to').val();
                    var amt = $('#amt').val();
                    var pid = $('#project').val();
                    var uid = $('#user').val();
                    var rid = $('#eid').val();
                    $('.page_spin').show();
                    var dataString = "rid=" + rid + "&pid="+pid+"&uid="+uid+"&from="+from+"&to="+to+"&amt="+amt+"&page=accept_request";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            alert('Status Updated');
                            $('.status_' + rid + '').children('.label-primary').remove();
                            $('.status_' + rid + '').siblings('td').children('.accept-btn').remove();
                            $('.status_' + rid + '').siblings('td').children('.reject-btn').remove();
                            $('.status_' + rid + '').append('<span class="label label-success">Admin Approved</span>');
                        }, //success fun end
                    });//ajax end
                }
                
                $('.approved-btn').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var pid = $(this).attr('pid');
                    var uid = $(this).attr('uid');
                    $('#approve_form').modal('show');
                    $('#user').val(uid);
                    $('#project').val(pid);
                    $('#eid').val(rid);
                    
                });
                
                $('.reject-btn').click(function (e) {
                    var rid = $(this).attr('rid');
                    var uid = $(this).attr('uid');
                    $('#pid').val(rid);
                    $('#uid').val(uid);
                    $('#request-wrap').modal('show');
                    $('.confirm-request').hide();
                    $('.confirm-reject').show();
                });
                
                $('.confirm-reject').click(function (e) {
                    e.preventDefault();
                    
                    var rid = $('#pid').val();
                    var uid = $('#uid').val();
                    var comment = $('#comment').val();
                    
                    $('.page_spin').show();
                    var dataString = "rid=" + rid + "&uid="+uid+"&comment="+comment+"&page=reject_request";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            alert('Status Updated');
                            $('.status_' + rid + '').children('.label-warning').remove();
                            $('.status_' + rid + '').siblings('td').children('.accept-btn').remove();
                            $('.status_' + rid + '').siblings('td').children('.reject-btn').remove();
                            $('.status_' + rid + '').append('<span class="label label-danger">Rejected</span>');
                        }, //success fun end
                    });//ajax end
                });



            });
        </script>

    </body>
</html>