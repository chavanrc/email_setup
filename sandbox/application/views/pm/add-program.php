<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Project Manager | Add Program</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/datepicker/jquery.datetimepicker.css">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Program - Create New</h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <?php
                if ($msg>0) {
                    ?>
                    <div class="alert alert-success col-md-4 col-md-offset-4">
                        Client Added Successfully.
                    </div>
                    
                
                    <?php
                }
                else
                {
                ?>

                

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-plus"></i> Create Program </h2>
                        </div>
                        <div class="panel-body">
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-program-form">
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Client <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="client">
                                            <option value=""> - Select -</option>
                                            <?php
                                            if(!empty($client))
                                            {
                                                foreach($client as $cl_data)
                                                {
                                                    ?>
                                                    <option value="<?php echo $cl_data->client_id; ?>"> <?php echo $cl_data->client_name; ?> </option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
								<div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Participant Type <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="participants_type">
                                            <option value=""> - Select -</option>
                                            		 <option value="Banking"> Banking </option>
                                                    <option value="Corporate"> Corporate </option>
                                                	 <option value="NGO"> NGO </option>
													  <option value="Govt"> Government </option>
                                            
                                        </select>
                                    </div>
                                </div>
								<div class="clearfix"></div>
								<div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Program Zone <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="zone">
                                            <option value=""> - Select -</option>
                                            		 <option value="North"> North </option>
                                                    <option value="East"> East </option>
                                                	 <option value="West"> West </option>
													  <option value="South"> South </option>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Program Title <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="pname" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Location <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="location" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Objective <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea name="objective" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Start Date <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="date" id="sdate" name="sdate"  placeholder="yy-mm-dd" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                               
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Duration in Days <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="duration" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Trainer Skill Sets <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea name="skill_set" class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">No. of Participants <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="participants" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Participants Level <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="participant_level">
                                            <option value=""> - Select -</option>
                                            <option value="Beginners"> Beginners </option>
                                            <option value="Juniors"> Juniors </option>
                                            <option value="Intermediate"> Intermediate </option>
                                            <option value="Advanced"> Advanced </option>
                                            <option value="Highly Experienced"> Highly Experienced </option>
                                            <option value="Above 40 years Staff"> Above 40 years Staff </option>
                                            <option value="Unskilled Labor"> Unskilled Labor </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Props Required <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="radio" name="prop" value="1"> Yes 
                                        <input type="radio" name="prop" checked value="0" style="margin-left: 25px;"> No 
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Stay Arrangement <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="stay">
                                            <option value=""> - Select -</option>
											<option value="None"> None </option>
                                            <option value="Wagons"> Wagons </option>
                                            <option value="Client"> Client </option>
                                            <option value="Trainer"> Trainer </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Travel Arrangement <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="travel">
                                            <option value=""> - Select -</option>
											<option value="None"> None </option>
                                            <option value="Wagons"> Wagons </option>
                                            <option value="Client"> Client </option>
                                            <option value="Trainer"> Trainer </option>
											
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php
                }
                ?>
            </div>
        </div>

<?php include 'js_files.php'; ?>
        
        <script type="text/javascript">

            $(document).ready(function () {
                
                
                
                
                $("#add-program-form").validate({
                    rules: {
                        pname: "required",
                        client: "required",
                        location: "required",
                        sdate: "required",
                        duration: {
                            required: true,
                            number: true
                        },
                        skill_set: "required",
                        participants: {
                            required: true,
                            number: true
                        },
                        participant_level : "required",
                        stay : "required",
                        travel : "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                
            });

        </script>

    </body>
</html>