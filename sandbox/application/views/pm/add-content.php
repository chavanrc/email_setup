<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Add Client</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Content - Add New Content</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>projectmanager/contents" class="btn btn-danger"><i class="fa fa-backward"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-plus"></i> Upload Content </h2>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 1) {
                                ?>
                                <div class="alert alert-success col-md-6 col-md-offset-3">
                                    Content Updated Successfully.
                                </div>
                                <?php
                            }
                            ?>
                            <div class="col-md-6">
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-content-form">
                                <input type="hidden" value="<?php echo $this->session->userdata('pm_code'); ?>" name="content_user"/>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1">Content Title <sup>*</sup></span>
                                        <input type="text" name="title" placeholder="Content Title" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Upload Content <sup>*</sup></span>
                                        <input type="file" name="content" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">

            $(document).ready(function () {
                $("#add-content-form").validate({
                    rules: {
                        title: "required",
                        content: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

            });

        </script>

    </body>
</html>