<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Project Manager | Clients</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Clients</h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list"></i> Client List </h2>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Industry</th>
                                        <th>Address</th>
                                        <th>Contact 1</th>
                                        <th>Payment Days</th>
                                        <th>Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($client)) {
                                        $num = 0;
                                        foreach ($client as $cl_data) {
                                            $num++;
                                            ?>
                                            <tr>
                                                <td><?php echo $num; ?></td>
                                                <td><?php echo $cl_data->client_name; ?>
                                                </td>
                                                <td><?php echo $cl_data->industry; ?></td>
                                                <td><?php echo $cl_data->address; ?></td>
                                                <td><?php echo $cl_data->person1; ?><br/>
                                                    <?php echo $cl_data->phone1; ?><br/>
                                                    <?php echo $cl_data->email1; ?>
                                                </td>
                                                
                                                <td><?php echo $cl_data->payment_credit_days; ?></td>
                                                <td><?php echo date_formate_short($cl_data->start_date); ?></td>
                                                
                                            </tr>
                                            <?php
                                        }
                                    }
                                    else
                                    {
                                        ?>
                                            <tr>
                                                <td colspan="7">No Record found</td>
                                            </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="assign-pm-wrap" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-arrow-right"></i> Assign Project Manager</h4>
                    </div>
                    <div class="modal-body">
                    <form action="" method="POST"  enctype="multipart/form-data" id="assign-pm-form">
                        <input type="hidden" value="" id="cid" name="cid"/>
                        <div class="form-group col-md-8 col-md-offset-2">
                                <select class="form-control" id="pm" name="pm">
                                    <option value=""> - Select Project Manager -</option>
                                    <?php
                                    foreach ($pm as $pm_data) {
                                        ?>
                                        <option value="<?php echo $pm_data->user_id; ?>"> <?php echo $pm_data->name; ?> </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group col-md-8 col-md-offset-2 text-center">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.assign-pm-btn').click(function(e){
                   e.preventDefault();
                   $('#assign-pm-wrap').modal('show');
                   var cid = $(this).attr('cid');
                   $('#cid').val(cid);
                });
                

                $("#assign-pm-form").validate({
                    rules: {
                        pm: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        assign_pm();
                    }
                });

                function assign_pm()
                {
                    var cid = $('#cid').val();
                    var pm = $('#pm').val();

                    $('.page_spin').show();
                    var dataString = "cid=" + cid + "&pm=" + pm + "&page=assign_pm";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            $('#assign-pm-wrap').modal('hide');

                        }, //success fun end
                    });//ajax end
                }
            });
        </script>

    </body>
</html>