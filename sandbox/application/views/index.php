<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>

<script type="text/javascript">
var ipad = true;               // Set to false to not redirect on iPad.
var other_tablets = true;  // Set to false to not redirect on other tablets.
var mobile_domain = 'kavaatavalves.cloudhostedresources.com';
document.write(unescape("%3Cscript src='" + location.protocol + "//s3.amazonaws.com/me.static/js/me.redirect.min.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
<title>Flanged End Ball Valve Manufacturers and suppliers, India</title>
<meta name="keywords" content="Kavaata Valves - Flanged End Ball Valves Manufacturers and suppliers in India" />
<meta name="description" content="Kavaata Flanged End Ball Valves are designed and manufactured as per BS 5351/ ISO 17292, API6D standards."  />
<meta name="copyright" content="http://www.kavaatavalves.com" />
<meta name="Charset" content="ISO-8859-1"/>
<meta name="Distribution" content="Global"/>
<meta name="Rating" content="General"/>
<meta name="googlebot" content="index, follow, noodp, noydir"/>
<meta name="msnbot" content="index, follow, noodp, noydir"/>
<meta name="slurp" content="index, follow, noodp, noydir"/>
<meta name="Revisit-after" content="7 Days"/> 
<link rel="stylesheet" href="css/style.css" type="text/css" />

<!--jQuery Mega Drop Down Menu CSS &amp; JS Links Start-->

<link href="css/dcmegamenu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type='text/javascript' src='js/jquery.hoverIntent.minified.js'></script>
<script type='text/javascript' src='js/jquery.dcmegamenu.1.3.3.js'></script>
<script src="SpryAssets/SpryAccordion.js" type="text/javascript"></script>
<script type="text/javascript">
$(document).ready(function($){
	$('#mega-menu-1').dcMegaMenu({
		rowItems: '3',
		speed: 0,
		effect: 'slide',
		event: 'click',
		fullWidth: true
	});
	$('#mega-menu-2').dcMegaMenu({
		rowItems: '1',
		speed: 'fast',
		effect: 'fade',
		event: 'click'
	});
	
});
</script>
 <link href="css/skins/black.css" rel="stylesheet" type="text/css" /> 
<!--jQuery Mega Drop Down Menu CSS &amp; JS Links End-->

<!--jQuery news scroll CSS &amp; JS Links Start--> 
<link href="css/global.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=IM+Fell+DW+Pica+SC' rel='stylesheet' type='text/css'>
<!--jQuery news scroll CSS &amp; JS Links End-->

<!--Zoom Script start -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.3.2/jquery.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/sexylightbox.v2.3.jquery.min.js"></script>
<link rel="stylesheet" href="css/sexylightbox.css" type="text/css" media="all" />
<script type="text/javascript">
$(document).ready(function(){
  SexyLightbox.initialize({color:'black', dir: 'images'});
});
</script>
<!--Zoom Script End -->

<link href="SpryAssets/SpryAccordion.css" rel="stylesheet" type="text/css" />
</head>

<body>
<?php include_once("analyticstracking.php") ?>
<div id="container-promot1" style="height:1245px;">
      
    <?php include ('includes/header-produts.php'); ?>
    <div style="clear:both; margin-top:5px; width:995px; margin-left:auto; margin-right:auto;">
       	<img src="flash/flanged-end-ball-valve.jpg" width="995" height="296" alt="Flanged End Ball Valve" title="Flanged End Ball Valve" />
    </div>
	    
    <div class="center">
      
      <div style="clear:both;">
   	  <div><h1 class="wel-text" title="Flanged End Ball Valve">Flanged End  <span style="color:#f97416;">Ball Valve</span></h1></div>
         <div class="pro1">
<a href="images/products-big/flange_big.jpg" rel="sexylightbox[group1]" title="Flanged End Ball Valve">
                <img src="images/products/flanged.png" width="284" height="178" alt=""  />
             <div style="margin:5px 0 0 10px;">
                 <div class="quality1">Flange End Two Way</div>
                  <a href="images/products-big/FLANGE-END-TWO-WAY.jpg" rel="sexylightbox[group1]" title="Flanged End Ball Valve">
                     <img src="images/products/FLANGE-END-TWO-WAY.jpg" width="200" height="148" alt=""  class="img-bdr1" />
                     <img src="images/zoom-icon.png" width="20" height="20" alt="zoom - Flanged End Ball Valve" title="zoom " />
                  </a>
          	     
             </div>
         </div>
               
      <div class="pro2">
            <p class="wel-text1" style="width:440px; text-align:justify;">
        	Kavaata Flanged End Ball Valves are designed and manufactured as per BS 5351/ ISO 17292, API6D standards. The material of construction generally offered are ASTM A216 Gr WCB (CS), ASTM A351 Gr CF8 (SS304) and ASTM A351 Gr CF8M (SS316). Valves in other M.O.C. like CF3, CF3M can be supplied against specific requirement. The Flanged End Ball Valves design is as per ANSI B16.5 standards and wall thickness as per ASME B16.34.
            </p>
            
            
     <div class="news-bg5">
   		 <div id="news-bg5" style="width:220px;">Features - Flanged End  Ball Valves</div> 
              <div id="news-bg3" style="padding:2px 0 0 0;">
            	 <ul id="ticker_01" class="ticker" style="width:200px; height:130px; margin:5px 0 0 25px;">
                  <li id="li-hgt">
                     <img src="images/li-style.png" width="8" height="9" alt="li - Flanged End Ball Valve " />&nbsp; Surface finish lessthan 0.1Âµ
                  </li>
                  <li id="li-hgt">
                     <img src="images/li-style.png" width="8" height="9" alt="li  " />&nbsp; Spericalness within 0.02mm
                  </li>
                  <li id="li-hgt">
                     <img src="images/li-style.png" width="8" height="9" alt="li " />&nbsp; Slot Centre Within 0.05mm
                  </li>
                   <li id="li-hgt">
                     <img src="images/li-style.png" width="8" height="9" alt="li " />&nbsp; End Radius Made On CNC 
                  </li>
                  <li id="li-hgt">
                     <img src="images/li-style.png" width="8" height="9" alt="li  " />&nbsp; Slot Width Within 0.05mm
                  </li>
                  <li id="li-hgt">
                     <img src="images/li-style.png" width="8" height="9" alt="li " />&nbsp; Ball Size Within 0.05mm 
                  </li>
                 </ul>
            </div>
      </div>
      
      <div id="broucher">
         
           <a href="pdf/flangeendballvalvecatalog.pdf" style="text-decoration:none;">
           <img src="images/brochure.jpg" width="148" height="38" alt="Broucher " style="padding:155px 0 0 10px;" />
           </a>
      </div>
            
        </div>
        
        <div class="right1" style="margin-top:-12px;">
            <p class="right2">
                <strong class="head1" style="color:#FFF;">PRODUCTS </strong><br />
               <ul class="pro-ul" style="list-style:url(images/ul-arrow.png)">
                	<li><a href="#" style="color:#F97416;" title="Flanged End Ball Valve">Flanged End Ball Valve</a></li>
                    <li><a href="forged-steel-ball-valves.php" title="Forged Steel Ball Valve">Forged Steel Ball Valve</a></li>
                    <li><a href="screwedendball.php" title="Screwed End Ball Valve">Screwed End Ball Valve</a></li>
              <li><a href="high-pressure-ball-valve.php" title="High Pressure Ball Valve">High Pressure Ball Valve</a></li>
                    <li><a href="three-way-ball-valves.php" title="Three Way Ball Valves">Three Way Ball Valves</a></li>
                    <li><a href="metal-seated-ball-valve.php" title="Metal Seated ball valves" >Metal Seated ball valves</a></li>
                     <li><a href="screwed_end_2_piece_ball_valve.php"  title="Screwed End 2 Piece Design Ball Valve">Screwed End 2 Pc  Ball Valve</a></li>
                     <li><a href="flanged_end_2_piece_ball_valve.php"  title="Flanged End 2 Pc  Ball Valve">Flanged End 2 Pc  Ball Valve</a></li>
                    <li><a href="special.php" title="Special Purpose Ball Valves">Special Purpose Ball Valves</a></li>
                    <!--<li><a href="repair-of-ball-valve.php">Repair of Ball Valves</a></li>-->
                    <li><a href="cutting.php">Cutting Tools</a></li>
                    <li><a href="automation.php">Automation of Valves</a></li>
               </ul>    	
            </p>
        </div>
        
        <div class="spry-panel"> 
          <div id="Accordion1" class="Accordion" tabindex="0">
            <div class="AccordionPanel">
              <div class="AccordionPanelTab">Technical Specifications</div>
              <div class="AccordionPanelContent" >
                <div class="spry-panel1"> 
                <a href="images/products/flangedenddiagram.jpg" rel="sexylightbox[group1]" title="Flanged End Ball Valve">
                <img src="images/products/digram1.png" width="250" height="182" alt="Flanged ball valves" title="Flanged ball valves" class="spry-img" />
                <img src="images/zoom-icon.png" width="20" height="20" alt="zoom - Flanged ball valve" />
                </a>
       	      	<div style="margin:0 0 5px 45px;">Click on the figure to zoom</div>
                </div>
                <div class="tech-table1">              
              <table height="190" width="460px;" border="0" cellpadding="0" cellspacing="0">
              <tr class="style1">
                <td width="74" height="16" align="center"  class="style11">SIZE</td>
                <td width="74" align="center"  class="style11">15 FB</td>
                <td width="74" align="center"  class="style11">20 FB</td>
                <td width="74" align="center"  class="style11">25 FB</td>
                <td width="74" align="center"  class="style11">40 FB</td>
                <td width="74" align="center"  class="style11">50 FB</td>
                <td width="74" align="center"  class="style11">80 FB</td>
                <td width="74" align="center"  class="style11">100 FB</td>
              </tr>
              <tr>
                <td width="74" align="center" class="tab_left">A</td>
                <td width="74" align="center" class="tab_content">108</td>
                <td width="74" align="center" class="tab_content">118</td>
                <td width="74" align="center" class="tab_content">127</td>
                <td width="74" align="center" class="tab_content">165</td>
                <td width="74" align="center" class="tab_content">178</td>
                <td width="74" align="center" class="tab_content">203</td>
                <td width="74" align="center" class="tab_content">229</td>
              </tr>
              <tr>
                <td width="74" align="center" class="tab_left"><strong>B</strong></td>
                <td width="74" align="center" class="tab_content">12.5</td>
                <td width="74" align="center" class="tab_content">20</td>
                <td width="74" align="center" class="tab_content">25</td>
                <td width="74" align="center" class="tab_content">38</td>
                <td width="74" align="center" class="tab_content">50</td>
                <td width="74" align="center" class="tab_content">75</td>
                <td width="74" align="center" class="tab_content">100</td>
              </tr>
              <tr>
                <td width="74" align="center" class="tab_left"><strong>C</strong></td>
                <td width="74" align="center" class="tab_content">80</td>
                <td width="74" align="center" class="tab_content">81</td>
                <td width="74" align="center" class="tab_content">98</td>
                <td width="74" align="center" class="tab_content">121</td>
                <td width="74" align="center" class="tab_content">132</td>
                <td width="74" align="center" class="tab_content">166</td>
                <td width="74" align="center" class="tab_content">190</td>
              </tr>
              <tr>
                <td width="74" align="center" class="tab_left"><strong>D</strong></td>
                <td width="74" align="center" class="tab_content">170</td>
                <td width="74" align="center" class="tab_content">170</td>
                <td width="74" align="center" class="tab_content">190</td>
                <td width="74" align="center" class="tab_content">250</td>
                <td width="74" align="center" class="tab_content">250</td>
                <td width="74" align="center" class="tab_content">300</td>
                <td width="74" align="center" class="tab_content">400</td>
              </tr>
            </table>
              
              </div>
               
              </div>
            </div>
            <div class="AccordionPanel">
              <div class="AccordionPanelTab">Technical Data</div>
              <div class="AccordionPanelContent">
              <div class="tech-table">
              <table width="500" border="0" cellpadding="0" cellspacing="0" bordercolor="#999999">
                <tr align="center">
                  <td width="117" class="style1">PART NAME</td>
                  <td colspan="3" class="style1">MATERIAL</td>
                </tr>
                <tr>
                  <td width="117" align="center" class="tab_left">Body/Adapters</td>
                  <td width="131" align="center" class="tab_content">ASTM<br />
                    A216<br />
                    Gr WCB</td>
                  <td width="129" align="center" class="tab_content">ASTM<br />
                    A351<br />
                    Gr CF8</td>
                  <td width="133" align="center" class="tab_content">ASTM<br />
                    A351<br />
                    Gr CF8M</td>
                </tr>
                <tr>
                  <td width="117" align="center" class="tab_left"><strong>Ball</strong></td>
                  <td colspan="3" align="center" class="tab_content">SS304/SS316/SS410</td>
                </tr>
                <tr>
                  <td width="117" align="center" class="tab_left"><strong>Stem</strong></td>
                  <td colspan="3" align="center" class="tab_content">SS304/SS316</td>
                </tr>
                <tr>
                  <td width="117" align="center" class="tab_left"><strong>Nuts</strong></td>
                  <td colspan="3" align="center" class="tab_content">SS304</td>
                </tr>
                <tr>
                  <td width="117" align="center" class="tab_left"><strong>Studs</strong></td>
                  <td colspan="3" align="center" class="tab_content">CS or SS304</td>
                </tr>
                <tr>
                  <td width="117" align="center" class="tab_left"><strong>Nuts</strong></td>
                  <td colspan="3" align="center" class="tab_content">CS or SS304</td>
                </tr>
                <tr>
                  <td width="117" align="center" class="tab_left"><strong>Seat</strong></td>
                  <td colspan="3" align="center" class="tab_content">PTFE / GFT</td>
                </tr>
                <tr>
                  <td width="117" align="center" class="tab_left"><strong>Seal</strong></td>
                  <td colspan="3" align="center" class="tab_content">CFT</td>
                </tr>
                <tr>
                </tr>
              </table>
              <div style="margin:8px 0;">
              <table border="0" align="center" cellpadding="0" cellspacing="0" bordercolor="#999999">
            <tr class="style1">
              <td width="130" align="center" bgcolor="#015CAB" class="style1">Test Pressure</td>
              <td width="222" align="center" bgcolor="#015CAB" class="style1">Hydrostatic</td>
              <td width="172" align="center" bgcolor="#015CAB" class="style1">Pneumatic</td>
            </tr>
            <tr>
              <td width="130" align="center" class="tab_left"><strong>SHELL</strong></td>
              <td width="222" align="center" class="tab_content">30 kg/sq-cm</td>
              <td width="172" align="center" class="tab_content">----</td>
            </tr>
            <tr>
              <td width="130" align="center" class="tab_left"><strong>SEAT</strong></td>
              <td width="222" align="center" class="tab_content">20 kg/sq-cm</td>
              <td width="172" align="center" class="tab_content">80 psi</td>
            </tr>
          </table>
              </div>
              </div>
              <!--<div class="tech-table1">
              
              </div>-->
              <div class="spry-img1"><img src="images/products/flangedimg.jpg" width="250" height="159" alt="Flanged ball valves" title="Flanged ball valves" /></div>
              </div>
            </div>
            <div class="AccordionPanel">
              <div class="AccordionPanelTab">Valve Standards</div>
              <div class="AccordionPanelContent" >
                  <div class="tech-table2">
              <table height="180" border="0" cellpadding="0" cellspacing="0" bordercolor="#999999">
                <tr>
                  <td height="19" colspan="2" align="center" bgcolor="#015CAB" class="style1">VALVE STANDARDS</td>
                </tr>
                <tr>
                  <td width="240" align="center" class="tab_left"><strong>Pressure Rating</strong></td>
                  <td width="240" align="center" class="tab_content">ASA 150#</td>
                </tr>
                <tr>
                  <td width="240" align="center" class="tab_left"><strong>Design</strong></td>
                  <td width="240" align="center" class="tab_content">BS5351/ISO17292</td>
                </tr>
                <tr>
                  <td width="240" align="center" class="tab_left"><strong>Face to Face</strong></td>
                  <td width="240" align="center" class="tab_content">ASME B 16.10</td>
                </tr>
                <tr>
                  <td width="240" align="center" class="tab_left"><strong>Flange Dimension</strong></td>
                  <td width="240" align="center" class="tab_content">ASME B 16.5</td>
                </tr>
                <tr>
                  <td width="240" height="45" align="center" class="tab_left"><strong>Test Pressure</strong></td>
                  <td width="240" align="center" class="tab_content">BS 6755</td>
                </tr>
              </table>
              </div>
              <div class="tech-table3">
              	<p>
                All the features of balls mentioned above contribute to the key attributes of Kavaata Ball Valves viz : <br />
                <ul id="ul-style">
                	<li><strong>Higher Seat Life</strong></li>
                    <li><strong>Bubble Tight Shut off</strong></li>
                    <li><strong>Low Torque</strong></li>
                </ul>
                <strong>Note :</strong> Due to continuous improvements, the specifications are subject to change without notice
               </p>
              </div>
              </div>
            </div>
            
            
            
          </div>
        </div>
        
      </div>
    </div>
    
    <?php include ('includes/footer.php'); ?>

</div>
<script>
function tick(){
		$('#ticker_01 li:first').slideUp( function () { $(this).appendTo($('#ticker_01')).slideDown(); });
	}
	setInterval(function(){ tick () }, 6000);
var Accordion1 = new Spry.Widget.Accordion("Accordion1");
</script>
</body>
</html>