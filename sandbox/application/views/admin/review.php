<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Review</title>
        <?php include 'css_files.php'; ?>
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    </head>
    <body>

        <div class="col-md-8 col-md-offset-2">
            <div class="brand text-center" style="padding:10px; background: #172d44;">
                <img src="<?php echo base_url(); ?>assets/images/wagons-logo.png"/>
            </div>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3><?php echo $type; ?> Review</h3>
                        <?php
                        if($msg==1)
                        {
                            ?>
                        <div class="alert alert-success">
                            Review added successfully.
                        </div>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="page-title title-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12" style="background: #fff; padding: 15px;">
                    <div class="col-md-6">
                    Project : <?php echo $program[0]->project_title; ?> <br/>
                    Client Name : <?php echo $program[0]->client_name; ?> <br/>
                    </div>
                    <div class="col-md-6">
                    Trainer : <?php echo $program[0]->name; ?> <br/>
                    Company : <?php echo $program[0]->company_name; ?> <br/>
                    </div>
                    <div class="clearfix"></div>
                    <div class="page-title">
                        <h4>Your Comment</h4>
                        <form action="" method="POST" id="review-form">
                            <div class="form-group">
                                <input type="hidden" value="<?php echo $id; ?>" name="id"/>
                                <input type="hidden" value="<?php echo $type; ?>" name="user_type"/>
                                <textarea class="form-control" name="comment" id="comment" rows="4"></textarea>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <?php
        include 'js_files.php';
        ?>
       
        <script type="text/javascript">

            $(document).ready(function () {
                $("#review-form").validate({
                    rules: {
                        comment: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

            });

        </script>

    </body>
</html>