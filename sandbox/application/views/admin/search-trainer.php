<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Trainers</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Trainers</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>admin/trainers" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-search"></i> Search Trainer </h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-6 col-md-offset-3 alert alert-danger" id="search_error" style="display: none;">
                                Please use at least one parameter to search.
                            </div>
                             <form action="" method="POST"  enctype="multipart/form-data" id="search-trainer-form">
                                
                                
                                <?php
                                $graducation = array('B.A','B.Arch','BCA','B.B.A','B.Com','B.Ed','BDS','BHM','B.Pharma',
                                    'B.Sc','B.Tech/B.E.','LLB','MBBS','Diploma','BVSC','Other');
                                $pg = array('CA','CS','ICWA (CMA)','M.A','M.Arch','M.Com','M.Ed','M.Pharma','M.Sc','M.Tech',
                                    'MBA/PGDM','MCA','MS','PG Diploma','MVSC','MCM','Other');
                                $doct = array('Ph.D/Doctorate','MPHIL','Other');
                                ?>
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> State</span>
                                        <select class="form-control" id="state"  name="state">
                                            <option value="">- Select -</option>
                                           <?php
                                           foreach($state as $st_data)
                                           {
                                               ?>
                                            <option value="<?php echo $st_data->st_name; ?>"><?php echo  $st_data->st_name; ?></option>
                                                <?php
                                           }
                                           ?>
                                            
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> City</span>
                                        <select class="form-control" id="city" name="city">
                                            <option value=""> - Select - </option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <?php
                                    
                                    $industry = array('Advertising','Event Management','Agriculture/Dairy','Architecture',
                                    'Automobiles',
                                    'Banking/Financial',
                                    'BPO',
                                    'Chemicals',
                                    'Construction',
                                    'Logistics',
                                    'Education/Training',
                                    'Export/Import',
                                    'Fertilizers',
                                    'FMCG',
                                    'Fresher/Trainee',
                                    'Gems and Jewellery',
                                    'Heavy Machinery',
                                    'Hotels/Restaurants',
                                    'Industrial Prods',
                                    'Energy',
                                    'Insurance',
                                    'IT-Hardware',
                                    'IT-Products',
                                    'Legal',
                                    'Media',
                                    'Healthcare',
                                    'NGO',
                                    'Automation',
                                    'Oil and Gas',
                                    'Paper',
                                    'Pharma',
                                    'Packaging',
                                    'Real Estate',
                                    'Recruitment Firm',
                                    'Retailing',
                                    'Security',
                                    'Electronics',
                                    'Shipping/Marine',
                                    'Telecom',
                                    'Textiles'
                                    );
                                    ?>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Industry</span>
                                        <select class="form-control" id="industry" name="industry">
                                            <option value=""> - Select -</option>
                                            <?php
                                            foreach($industry as $in_data)
                                            {
                                                ?>
                                                <option value="<?php echo $in_data; ?>"> <?php echo $in_data; ?> </option>
                                                <?php
                                            }
                                            ?>
                                            
                                        </select>
                                    </div>
                                </div>
                                 
                                 <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Age</span>
                                        <select class="form-control" id="age" name="age">
                                            <option value=""> - Select - </option>
                                            <option value="18-30">18-30 Years</option>
                                            <option value="31-40">31-40 Years</option>
                                            <option value="41-50">41-50 Years</option>
                                            <option value="51-60">51-60 Years</option>
                                            <option value="61-99">More Than 60 Years</option>
                                        </select>
                                    </div>
                                </div>
                                 
                                 <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Experience</span>
                                        <select class="form-control" id="experience" name="experience">
                                            <option value=""> - Select - </option>
                                            <option value="0-1">Less Than 1 Year</option>
                                            <option value="1-3">1-3 Years</option>
                                            <option value="3-5">3-5 Years</option>
                                            <option value="5-10">5-10 Years</option>
                                            <option value="10-20">10-20 Years</option>
                                            <option value="21-99">More Than 20 Years</option>
                                        </select>
                                    </div>
                                </div>
                                 
                                 <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                   
                                        <input type="text" name="tname" id="tname" class="form-control" placeholder="Trainer Name">
                                   
                                </div>
                                 
                                 <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                     <label>Keyword/Phrase</label>
                                     <textarea class="form-control" id="keyword" name="keyword"></textarea>
                                </div>
                                 <div class="col-md-6 col-sm-6 col-xs-12">
                                   <div style="margin-top:50px;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="button" class="btn btn-primary search-btn">Search</button>
                                   </div>
                                </div>
                                
                                <div class="clearfix"></div>
                                
                            </form>
                            <div class="trainer-result" style="display: none;">
                                <hr/>
                                <h4>Search Result</h4>
                            <table class="table table-bordered" id="search-result" >
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th >Name</th>
                                        <th >Company</th>
                                        <th >City</th>
                                        <th >Age</th>
                                        <th >Training Exp.</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    
                                </tbody>
                            </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php include 'js_files.php'; ?>
        
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function () {
                
                $('#state').change(function () {
                    var state = $(this).val();
                    if (state != '')
                    {
                        $('.page_spin').show();
                        var dataString = "state=" + state + "&page=get_city";
                        $.ajax({
                            type: "POST",
                            dataType: "JSON",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('#city option').remove();
                                $('#city').append('<option value=""> - Select - </option>');
                                $('.page_spin').hide();
                                
                                $.each(data, function (i, item) {

                                    $('#city').append('<option value="' + item.city + '">' + item.city + '</option>');
                                });
                            }, //success fun end
                        }); //ajax end 
                    }
                });
                
                $('.form-control').focus(function(){
                    $('#search_error').hide();
                });
                
                $('.search-btn').click(function(){
                    var state = $('#state').val();
                    var city = $('#city').val();
                    var industry = $('#industry').val();
                    var age = $('#age').val();
                    var exp = $('#experience').val();
                    var name = $('#tname').val();
                    var keyword = $('#keyword').val();
                    
                    if(state=='' && city=='' && industry=='' && age=='' && exp=='' && name=='' && keyword=='')
                    {
                        $('#search_error').show();
                        return false;
                    }
                    
                    $('.page_spin').show();
                        var dataString = "state=" + state + "&city="+city+"&industry="+industry+"&age="+age+"&exp="+exp+"&name="+name+"&keyword="+keyword+"&page=search_trainer";
                        $.ajax({
                            type: "POST",
                            dataType: "JSON",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.trainer-result').show();
                                $('.result-row').remove();
                                $('.page_spin').hide();
                                if(data.length<1)
                                {
                                    $('#search-result').append('<tr class="result-row" ><td colspan="6">No Result found</td></tr>');
                                }
                                else
                                {
                                $.each(data, function (i, item) {
                                    $('#search-result').append('<tr class="result-row"><td></td><td><a href="<?php echo base_url(); ?>admin/trainer_profile/'+item.user_code+'">'+item.name+'</a></td><td>'+item.company_name+'</td><td>'+item.city+'</td><td>'+item.age+'</td><td>'+item.total_experiance_training+'</td></tr>');
                                });
                                }
                                
                                
                            }, //success fun end
                        }); //ajax end 
                    
                });
                
            });
        </script>

    </body>
</html>