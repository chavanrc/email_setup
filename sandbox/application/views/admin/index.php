<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Dashboard</title>
        <?php include 'css_files.php'; ?>
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Admin Dashboard</h3>
                    </div>
                    <div class="page-title title-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-8">
                    <div id='calendar'></div>
                </div>
            </div>
        </div>

        <?php
        include 'js_files.php';
        $program = $CI->admin_model->get_progrom_date();
        ?>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/moment.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/jquery.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.js'></script>
        <script type="text/javascript">

            $(document).ready(function () {

                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek'
                    },
                    defaultDate: '<?php echo date('Y-m-d'); ?>',
                    editable: false,
                    eventLimit: false, // allow "more" link when too many events
                    events: [
                        <?php
                        if(!empty($program))
                        {
                            foreach($program as $pm_data)
                            {
                                $date = strtotime("+".$pm_data->training_duration." days", strtotime($pm_data->training_start_date));
                                $end_date =  date("Y-m-d", $date);
                                
                                $period = new DatePeriod(new DateTime($pm_data->training_start_date), new DateInterval('P1D'), new DateTime($end_date.' +1 day'));
                                foreach ($period as $date) {
                                   
                                ?>
                                                {
                                                    url: '<?php echo base_url(); ?>admin/program_details/<?php echo $pm_data->project_id; ?>',
                                                    title : '<?php echo $pm_data->project_title; if($pm_data->trainer_engage_flag=='0') { echo ' - New'; } ?>',
                                                    start : '<?php echo $date->format("Y-m-d"); ?>',
                                                },
                                <?php
                                }
                            }
                        }
                        ?>
                    ]
                });

            });

        </script>

    </body>
</html>