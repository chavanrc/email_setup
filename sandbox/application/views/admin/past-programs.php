<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Past Programs</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Past Programs</h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list"></i> Programs List </h2>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>PM</th>
                                        <th>Client</th>
                                        <th>Title</th>
                                        <th>Location</th>
                                        <th>Start Date</th>
                                        <th>Status</th>
                                        <th>Create Date</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($program)) {
                                        $num = 0;
                                        foreach ($program as $pm_data) {
                                            $num++;
                                            $pm = $CI->admin_model->get_single_pm($pm_data->user_code);
                                            ?>
                                            <tr>
                                                <td><?php echo $num; ?></td>
                                                <td><?php echo $pm[0]->name; ?></td>
                                                <td><?php echo $pm_data->client_name; ?></td>
                                                <td><?php echo $pm_data->project_title; ?></td>
                                                <td><?php echo $pm_data->location_of_training; ?></td>
                                                <td><?php echo date_formate_short($pm_data->training_start_date); ?></td>
                                                <td><?php 
                                                if($pm_data->is_active==0)
                                                {
                                                    ?>
                                                    <span class="label label-warning">In Active</span>
                                                    <?php
                                                }
                                                else
                                                {
                                                    ?>
                                                    <span class="label label-success">Active</span>
                                                    <?php
                                                }
                                                ?></td>
                                                <td><?php echo date_formate_short($pm_data->create_date); ?></td>
                                                <td><a href="<?php echo base_url(); ?>admin/program_details/<?php echo $pm_data->project_id; ?>" class="btn btn-sm btn-info">View</a></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    else
                                    {
                                        ?>
                                            <tr>
                                                <td colspan="9">No Record found</td>
                                            </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {
                
            });
        </script>

    </body>
</html>