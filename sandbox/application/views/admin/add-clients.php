<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Add Client</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Clients - Add New Client</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>admin/clients" class="btn btn-danger"><i class="fa fa-backward"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <?php
                if ($msg>0) {
                    ?>
                    <div class="alert alert-success col-md-4 col-md-offset-4">
                        Client Added Successfully.
                    </div>
                    
                    <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h3 class="panel-title" ><i class="fa fa-arrow-right"></i> Assign Project Manager </h3>
                        </div>
                        <div class="panel-body">
                            <form action="" method="POST"  enctype="multipart/form-data" id="assign-pm-form">
                                <input type="hidden" value="<?php echo $msg; ?>" id="cid" name="cid"/>
                                <div class="form-group">
                                    <div class="col-md-6 col-md-offset-3">
                                        <select class="form-control" id="pm" name="pm">
                                            <option value=""> - Select Project Manager -</option>
                                            <?php
                                            foreach ($pm as $pm_data) {
                                                ?>
                                                <option value="<?php echo $pm_data->user_code; ?>"> <?php echo $pm_data->name; ?> </option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <a href="<?php echo base_url(); ?>admin/clients" class="btn btn-warning">Assign Later</a>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
                    <?php
                }
                else
                {
                ?>

                

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-plus"></i> Create Client </h2>
                        </div>
                        <div class="panel-body">
                            <div class="alert alert-danger login_error" style="display: none;">Wrong Username & Password</div>
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-client-form">
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="fname" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Industry <span class="required">*</span>
                                    </div>
                                    <?php
                                    
                                    $industry = array('Advertising','Event Management','Agriculture/Dairy','Architecture',
                                    'Automobiles',
                                    'Banking/Financial',
                                    'BPO',
                                    'Chemicals',
                                    'Construction',
                                    'Logistics',
                                    'Education/Training',
                                    'Export/Import',
                                    'Fertilizers',
                                    'FMCG',
                                    'Fresher/Trainee',
                                    'Gems and Jewellery',
                                    'Heavy Machinery',
                                    'Hotels/Restaurants',
                                    'Industrial Prods',
                                    'Energy',
                                    'Insurance',
                                    'IT-Hardware',
                                    'IT-Products',
                                    'Legal',
                                    'Media',
                                    'Healthcare',
                                    'NGO',
                                    'Automation',
                                    'Oil and Gas',
                                    'Paper',
                                    'Pharma',
                                    'Packaging',
                                    'Real Estate',
                                    'Recruitment Firm',
                                    'Retailing',
                                    'Security',
                                    'Electronics',
                                    'Shipping/Marine',
                                    'Telecom',
                                    'Textiles'
                                    );
                                    ?>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="industry">
                                            <option value=""> - Select -</option>
                                            <?php
                                            foreach($industry as $in_data)
                                            {
                                                ?>
                                                <option value="<?php echo $in_data; ?>"> <?php echo $in_data; ?> </option>
                                                <?php
                                            }
                                            ?>
                                            
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Address <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="address" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Logo
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="logo" accept='image/*' class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Contact Person 1 <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-user"></i></span>
                                            <input type="text" name="name1" class="form-control" placeholder="Name">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-envelope"></i></span>
                                            <input type="text" name="email1" class="form-control" placeholder="Email id">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-phone"></i></span>
                                            <input type="text" name="mobile1"  class="form-control" placeholder="Mobile Number" >
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Contact Person 2 
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-user"></i></span>
                                            <input type="text" name="name2" class="form-control" placeholder="Name" aria-describedby="sizing-addon1">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-envelope"></i></span>
                                            <input type="text" name="email2" class="form-control" placeholder="Email id" aria-describedby="sizing-addon1">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-phone"></i></span>
                                            <input type="text" name="mobile2" class="form-control" placeholder="Mobile Number" aria-describedby="sizing-addon1">
                                        </div>
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Payment Credit Days <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="payment" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php
                }
                ?>
            </div>
        </div>

<?php include 'js_files.php'; ?>
        <script type="text/javascript">

            $(document).ready(function () {
                $("#add-client-form").validate({
                    rules: {
                        fname: "required",
                        industry: "required",
                        address: "required",
                        name1: "required",
                        email1: {
                            required: true,
                            email: true
                        },
                        mobile1: "required",
                        payment: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#assign-pm-form").validate({
                    rules: {
                        pm: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        assign_pm();
                    }
                });

                function assign_pm()
                {
                    var cid = $('#cid').val();
                    var pm = $('#pm').val();

                    $('.page_spin').show();
                    var dataString = "cid=" + cid + "&pm=" + pm + "&page=assign_pm";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.href = "<?php echo base_url(); ?>admin/clients";

                        }, //success fun end
                    });//ajax end
                }
            });

        </script>

    </body>
</html>