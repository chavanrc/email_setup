<?php
$CI = & get_instance();
$CI->load->model('home_model');
?>
<div class="topbar">
    <div class="container">
        <div class="row">
            <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="topbar-text">
                    <i class="fa fa-envelope"></i><a href="mailto:info@medico.com" style="color:#FFF;"> info@medico.com </a> &nbsp; | &nbsp; <i class="fa fa-phone"></i> +91 98451-20333 / +91 98451-20333
                </div>
            </div>
            <div class="col-md-6 col-sm-6 col-xs-12 social-network ">
                <span class="social"> <i class="fa fa-facebook fb"></i></span>
                <span class="social"><i class="fa fa-twitter tw"></i> </span>
                <span class="social"><i class="fa fa-google go"></i> </span>
                <span class="social"><i class="fa fa-linkedin link"></i></span>
            </div>
        </div>
    </div>
</div>

<div class="header">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-md-3 col-sm-6">
                <a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>assets/images/logo.png" class="img-responsive gift-logo" alt="Medico Basket Logo "></a>
            </div>
            <div class="col-lg-4 col-md-4 col-sm-6">
                <div class="form-group has-danger has-feedback serach-store">
                    <form action="<?php echo base_url(); ?>home/search_books" method="GET" id="search_form">
                    <div class="input-group">
                        <input type="text" class="form-control" name="tags" id="tags" placeholder="Search Book name or Authour" autocomplete="off" style="height: 40px;">
                        <span class="input-group-addon search-btn" style="height: 40px; cursor: pointer;"><i class="fa fa-search"></i> </span>
                    </div>
                    </form>
                </div>
            </div>
            <div class="col-lg-4 col-md-5 col-sm-12 header-end">
                <?php
                if($this->session->userdata('user'))
                {
                    ?>
                <a href="<?php echo base_url(); ?>home/account" class="btn add-cart"><i class="fa fa-user"></i> <?php echo $this->session->userdata('u_name'); ?></a>
                <a href="<?php echo base_url(); ?>home/logout" class="btn add-cart" style="color:#E5696B;"><i class="fa fa-sign-out"></i> Logout</a>
                    <?php
                }
                else
                {
                ?>
                <a href="<?php echo base_url(); ?>home/register" class="btn btn-default add-cart hvr-wobble-horizontal"><i class="fa fa-sign-in"></i> SIGNIN</a>
                <a href="<?php echo base_url(); ?>home/register" class="btn btn-default add-cart hvr-wobble-horizontal"><i class="fa fa-user"></i> CREATE ACCOUNT</a>
                <?php
                }
                ?>
                <a href="<?php echo base_url(); ?>home/cart" class="btn btn-default add-cart hvr-wobble-horizontal">
                    <i class="fa fa-cart-plus"></i> CART
                    <?php
                    $cart_count = 0;
                    $cart_array = array();
                    if($this->session->userdata('cart'))
                    {
                        $cart_count = count($this->session->userdata('cart'));
                        $cart_array = $this->session->userdata('cart');
                    }
                    ?>
                    <span class="cart-item cart-count"><?php echo $cart_count; ?></span>
                </a>
            </div>
        </div>
    </div>
</div>

<div id="car_modal" class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Cart</h4>
      </div>
      <div class="modal-body">
          You have <span class="cart-count"></span> items in your cart.<br/>
          <a href="<?php echo base_url(); ?>home/cart" class="btn btn-default">Go to Cart</a>
      </div>
      
    </div>
  </div>
</div>
