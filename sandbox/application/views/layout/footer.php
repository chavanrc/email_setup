 <div class="footer hidden-xs">
 	<div class="container">
    	<div class="row">

            
            <div class="col-md-3">
            	<div class="occasion"> ABOUT US </div>
                <p style="color:#A4A4A4;">
                	Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                    <a href="#" class="btn btn-default cart-button" style="margin-top:10px;"> Read More </a>
                </p>
            </div>
            
            <div class="col-md-3">
            	<div class="occasion"> USEFUL LINKS </div>
    			  <ul class="occasion-ul">
                	<li><a href="javascript:void(0);"><i class="fa fa-caret-right"></i> Services </a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-caret-right"></i> Science </a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-caret-right"></i> Biology </a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-caret-right"></i> Books </a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-caret-right"></i> About Us </a></li>
                    <li><a href="javascript:void(0);"><i class="fa fa-caret-right"></i> Contact Us </a></li>
                 </ul>
            </div>
            
            <div class="col-md-3">
            	<div class="occasion"> WE ACCEPT </div>
                <ul class="occasion-ul">
                	<img style="margin-bottom: 10px;" src="http://www.giftwithluv.com/assets/images/payment/american.png" alt="">
                    <img style="margin-bottom: 10px;" src="http://www.giftwithluv.com/assets/images/payment/discover.png" alt="">
                    <img style="margin-bottom: 10px;" src="http://www.giftwithluv.com/assets/images/payment/master.png" alt="">
                    <img style="margin-bottom: 10px;" src="http://www.giftwithluv.com/assets/images/payment/moestro.png" alt="">
                    <img style="margin-bottom: 10px;" src="http://www.giftwithluv.com/assets/images/payment/net-banking.png" alt="">
                    <img style="margin-bottom: 10px;" src="http://www.giftwithluv.com/assets/images/payment/paypa.png" alt="">
                    <img style="margin-bottom: 10px;" src="http://www.giftwithluv.com/assets/images/payment/visa.png" alt="">
                    <p style="color:#A4A4A4;">
                	Lorem Ipsum is simply dummy text of the printing and typesetting industry,
                    Lorem Ipsum has been the industry's standard dummy.
               		</p>
                 </ul>
            </div>
            
            <div class="col-md-3">
            	<div class="occasion"> CONTACT US </div>
                <p style="color:#A4A4A4;">
                	<i class="fa fa-map-marker" aria-hidden="true"></i> &nbsp; 
                     Lorem Ipsum is simply dummy text of the printing and typesetting industry. <br /><br />
                     
                     <i class="fa fa-phone" aria-hidden="true"></i> &nbsp;
                     +91 98450-98450<br /><br />
                     
                     <i class="fa fa-envelope-o" aria-hidden="true"></i> &nbsp;
                     info@medico.com <br /><br /><br /><br />
                </p>
            </div>
            
       
  
        <div class="col-md-6 hidden-xs policy">
        	<a href="index.php"> Home </a>
            <a href="terms-conditions.php"> Terms &amp; Conditions </a>
            <a href="privacy.php"> Privacy Policy </a>
            <a href="faq.php"> FAQ </a>
        </div>
     	
         <div class="col-md-6 social-network policy">
        	<span class="social"> <i class="fa fa-facebook fb"></i></span>
            <span class="social"><i class="fa fa-twitter tw"></i> </span>
            <span class="social"><i class="fa fa-google go"></i> </span>
            <span class="social"><i class="fa fa-linkedin link"></i></span>
        </div>  
     
       
         </div>
    </div>
</div>
<div class="small-footer">
	<div class="container">
    	<div class="row">
        	<div class="col-md-6">
            	<div class="copyright">© 2016 Medicao Basket All Rights Reserved.</div>
                
            </div>
            <div class="col-md-6">
            	<div class="designed">Designed &amp; Developed By : <a href="http://webdreams.in" target="_blank" rel="nofollow">WebDreams India </a></div>
                
            </div>
         </div>
     </div>
  </div>