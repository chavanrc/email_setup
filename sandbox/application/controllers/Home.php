<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->output->set_header('Access-Control-Allow-Origin: *');
    }

    public function index() {
        $this->load->model('home_model');
        $this->load->view('index');
    }
    
    public function page_map($pr, $sc = NULL) {
        $this->$pr($sc);
    }
    
    public function upload_photo($id)
    {
        $this->load->model('home_model');
        $data['pid'] = $id;
        $data['tid'] = "";
        $data['msg'] = 0;
        if(isset($_POST['tid']))
        {
            $data['msg'] = $this->home_model->upload_photo();
        }
        $data['photo'] = $this->home_model->getPhotos($id);
        $this->load->view('upload-photo', $data);
    }

   	public function pre_questions($id)
    {
        $this->load->model('home_model');
        $data['dTime'] = $this->home_model->get_Time($id);
        $data['question'] = $this->home_model->getPretest_questions($id);
        $this->load->view('pre-questions', $data);
    }
    
    public function post_questions($id)
    {
        $this->load->model('home_model');
        $data['dTime'] = $this->home_model->get_Time($id);
        $data['question'] = $this->home_model->getPosttest_questions($id);
        $this->load->view('post-questions', $data);
    }

}
