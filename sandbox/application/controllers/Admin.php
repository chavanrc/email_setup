<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function index() {
        $this->check_sess();
        $data['page_url'] = 'Dashboard';
        $this->load->view('admin/index', $data);
    }

    public function page_map($pr, $sc) {
        $this->$pr($sc);
    }

    public function check_sess() {
        if (!$this->session->userdata('admin')) {
            redirect('admin/login');
        }
    }

    public function kslu_result() {
        $query = $this->db->query("SELECT * FROM temp_result2");
        $result = $query->result();
        foreach ($result as $rs_data) {
            $data = array('sem_code' => $rs_data->sem,
                'reg_no' => $rs_data->reg,
                'name' => $rs_data->name,
                'father_name' => $rs_data->father,
                'mother_name' => $rs_data->mother,
                'subject_1' => $rs_data->sub1,
                'subject_2' => $rs_data->sub2,
                'subject_3' => $rs_data->sub3,
                'subject_4' => $rs_data->sub4,
                'subject_5' => $rs_data->sub5,
                'total' => $rs_data->total,
                'result' => $rs_data->result,
                'college_code' => $rs_data->collage,
                'is_active' => '1',
                'year' => '2016',
            );

            $this->db->insert('kslu_result_master_102', $data);
        }
    }

    public function kslu_sem_result() {
        $query = $this->db->query("SELECT * FROM temp_result2 WHERE reg >'43613101099'");
        $result = $query->result();
        foreach ($result as $rs_data) {
            $data = array('sem_code' => $rs_data->sem,
                'reg_no' => $rs_data->reg,
                'name' => $rs_data->name,
                'father_name' => $rs_data->father,
                'mother_name' => $rs_data->mother,
                'subject_1' => $rs_data->sub1,
                'subject_2' => $rs_data->sub2,
                'subject_3' => $rs_data->sub3,
                'subject_4' => $rs_data->sub4,
                'subject_5' => $rs_data->sub5,
                'total' => $rs_data->total,
                'result' => $rs_data->result,
                'college_code' => $rs_data->collage,
                'is_active' => '1',
                'year' => '2016',
            );

            $this->db->insert('kslu_result_master_102', $data);
        }
    }

    public function kslu_sem3_result() {
        return;
        $query = $this->db->query("SELECT * FROM table_105");
        $result = $query->result();
        foreach ($result as $rs_data) {
            $data = array('sem_code' => $rs_data->sem,
                'reg_no' => $rs_data->reg,
                'name' => $rs_data->name,
                'father_name' => $rs_data->father,
                'mother_name' => $rs_data->mother,
                'subject_1' => $rs_data->sub1,
                'subject_2' => $rs_data->sub2,
                'subject_3' => $rs_data->sub3,
                'subject_4' => $rs_data->sub4,
                'subject_5' => $rs_data->sub5,
                'subject_6' => $rs_data->sub6,
//                'subject_7' => $rs_data->sub7,
//                'subject_8' => $rs_data->sub3,
//                'subject_9' => $rs_data->sub4,
//                'subject_6_ia' => $rs_data->sem_ia,
//                'subject_6_agg' => $rs_data->sem_ag,
//                'subject_10' => $rs_data->sub5,
//                'subject_11' => $rs_data->sub11,
//                'subject_12' => $rs_data->sub12,
                'total' => $rs_data->total,
//                'total_1' => $rs_data->total1,
//                'total_2' => $rs_data->total2,
//                'total_3' => $rs_data->total3,
//                'total_4' => $rs_data->total4,
//                'total_5' => $rs_data->total5,
//                'total_6' => $rs_data->total6,
//                'total_7' => $rs_data->total7,
//                'total_8' => $rs_data->total8,
//                'total_9' => $rs_data->total9,
//                'grand_total' => $rs_data->grand_total,
                'result' => $rs_data->result,
                'college_code' => $rs_data->collage,
                'is_active' => '1',
                'year' => 'June 2017',
            );

            $this->db->insert('kslu_result_master_105', $data);
        }
    }

    public function login() {

        $this->load->view('admin/login');
    }

    public function logout() {
        $this->session->unset_userdata('admin', '');
        $this->session->unset_userdata('name', '');
        $this->session->unset_userdata('user_id', '');
        $this->session->unset_userdata('user_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }
    
    public function pm_logout() {
        $this->session->unset_userdata('pm', '');
        $this->session->unset_userdata('pm_name', '');
        $this->session->unset_userdata('pm_id', '');
        $this->session->unset_userdata('pm_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }
    
    public function trainer_logout() {
        $this->session->unset_userdata('trainer', '');
        $this->session->unset_userdata('t_name', '');
        $this->session->unset_userdata('t_id', '');
        $this->session->unset_userdata('t_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function add_client() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Clients';
        $data['msg'] = 0;
        if (isset($_POST['fname'])) {
            $data['msg'] = $this->admin_model->save_client();
        }
        $data['pm'] = $this->admin_model->get_pm();
        $this->load->view('admin/add-clients', $data);
    }

    public function update_password($code=NULL,$id=NULL) {
        if(!empty($code) && !empty($id))
        {
            $this->load->model('admin_model');
           // $link = $this->admin_model->forgot_password('anilk.acs@gmail.com');
            $data['email'] = custome_decode($id);
            $data['user_code'] = $code;
            $this->load->view('admin/update-password',$data);
        }
    }
    
    public function email_temp()
    {
        $this->load->view('email_temp/forgot-password');
    }

    public function clients() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Clients';
        $data['client'] = $this->admin_model->get_client();
        $data['pm'] = $this->admin_model->get_pm();
        $this->load->view('admin/clients', $data);
    }

    public function upate_client($id = NULL) {
        $this->check_sess();
        $this->load->model('admin_model');
        if (!empty($id)) {
            $data['msg'] = 0;
            $data['page_url'] = 'Clients';
            if (isset($_POST['fname'])) {
                $data['msg'] = $this->admin_model->update_client();
            }
            $data['client'] = $this->admin_model->get_single_client($id);
            $this->load->view('admin/edit-client', $data);
        }
    }

    public function add_pm() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Clients';
        $data['msg'] = 'new';
        if (isset($_POST['fname'])) {
            $data['msg'] = $this->admin_model->save_pm();
        }
        $data['client'] = $this->admin_model->get_client_assign_pending();
        $this->load->view('admin/add-pm', $data);
    }
    
    public function profile()
    {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Dashboard';
        $data['trainer'] = $this->admin_model->get_single_trainer($this->session->userdata('user_code'));
        $this->load->view('admin/profile', $data);
    }

    public function project_manager() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Project Managers';
        $data['pm'] = $this->admin_model->get_pm();
        $data['client'] = $this->admin_model->get_client_assign_pending();
        $this->load->view('admin/project-manager', $data);
    }

    public function edit_pm($id) {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Clients';
        $data['msg'] = 'new';
        if (isset($_POST['fname'])) {
            $data['msg'] = $this->admin_model->update_pm();
        }
        $data['pm'] = $this->admin_model->get_single_pm($id);
        $this->load->view('admin/edit-pm', $data);
    }

    public function add_content() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Content';
        $data['msg'] = 'new';
        if (isset($_POST['content_user'])) {
            $data['msg'] = $this->admin_model->save_content();
        }
        $this->load->view('admin/add-content', $data);
    }

    public function contents() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Content';
        $data['msg'] = 'new';
        $data['content'] = $this->admin_model->get_all_content();
        $this->load->view('admin/content', $data);
    }
    
    public function payments() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Payments';
        $data['msg'] = 'new';
        $data['payment'] = $this->admin_model->get_all_payment();
        $this->load->view('admin/payment', $data);
    }

    public function trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['trainer'] = $this->admin_model->get_trainer();
        $this->load->view('admin/trainers', $data);
    }

    public function inactive_trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['trainer'] = $this->admin_model->get_inactive_trainer();
        $this->load->view('admin/inactive_trainers', $data);
    }

    public function pending_trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['trainer'] = $this->admin_model->get_pending_trainer();
        $this->load->view('admin/pending_trainers', $data);
    }

    public function search_trainer() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['state'] = $this->admin_model->get_state();
        $this->load->view('admin/search-trainer', $data);
    }

    public function add_trainer() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['msg'] = "new";
        if (isset($_POST['pname'])) {
            $data['msg'] = $this->admin_model->save_trainer();
        }
        $data['state'] = $this->admin_model->get_state();
        $this->load->view('admin/add-trainer', $data);
    }

    public function trainer_profile($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['trainer'] = $this->admin_model->get_single_trainer($id);
            $this->load->view('admin/trainer-profile', $data);
        }
    }

    public function trainer_calendar($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $this->load->view('admin/trainer-calendar', $data);
        }
    }

    public function update_trainer($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            if (isset($_POST['pname'])) {
                $data['msg'] = $this->admin_model->update_trainer();
            }
            $data['trainer'] = $this->admin_model->get_single_trainer($id);
            $data['state'] = $this->admin_model->get_state();
            $data['city'] = $this->admin_model->get_city_list($data['trainer'][0]->state);
            $this->load->view('admin/update-trainer', $data);
        }
    }

    public function trainer_experience($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $data['exp'] = $this->admin_model->get_trainer_experience($id);
            $this->load->view('admin/trainer-experience', $data);
        }
    }

    public function trainer_program_engagment($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $data['engage'] = $this->admin_model->get_trainer_engagement($id);
            $this->load->view('admin/trainer-program-engagement', $data);
        }
    }

    public function upcoming_programs($id) {
        $this->load->model('admin_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 0;
        $data['program'] = $this->admin_model->get_basic_programs();
        $this->load->view('admin/upcoming-programs', $data);
    }

    public function past_programs($id) {
        $this->load->model('admin_model');
        $data['page_url'] = 'Past Programs';
        $data['msg'] = 0;
        $data['program'] = $this->admin_model->get_basic_past_programs();
        $this->load->view('admin/past-programs', $data);
    }
    
    public function review($id,$u)
    {
        $id = explode('-', custome_decode($id));
        $data['id'] = $id[0];
        $data['pid'] = $id[1];
        $data['type'] = custome_decode($u);
        $this->load->model('admin_model');
        $data['msg'] = 0;
        if(isset($_POST['comment']))
        {
            $data['msg'] = $this->admin_model->save_review();
        }
        $data['program'] = $this->admin_model->get_request_details($data['id']);
        $this->load->view('admin/review',$data);
    }

    public function program_details($id) {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 'new';
        if (isset($_POST['content_user'])) {
            $data['msg'] = $this->projectmanager_model->save_content();
        }
        if (isset($_POST['form_user'])) {
            $data['msg'] = $this->projectmanager_model->save_form();
        }
        if (isset($_POST['props_user'])) {
            $data['msg'] = $this->projectmanager_model->save_props();
        }
        if (isset($_POST['stay_user'])) {
            $data['msg'] = $this->projectmanager_model->save_stay();
        }
        if (isset($_POST['travel_user'])) {
            $data['msg'] = $this->projectmanager_model->save_travel();
        }
        $data['program'] = $this->projectmanager_model->get_single_program($id);

        $this->load->view('admin/program-details', $data);
    }

    public function engagement_requests($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        if (!empty($id)) {
            $this->load->model('projectmanager_model');
            $data['request'] = $this->projectmanager_model->get_requests($id);
        }
        $data['pid'] = $id;
        $this->load->view('admin/engagement-request', $data);
    }

    public function ajax_page() {
        $this->load->model('admin_model');
        echo $this->admin_model->$_POST['page']($_POST);
    }

}
