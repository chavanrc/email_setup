<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class projectmanager extends CI_Controller {

    public function index() {
        $this->check_sess();
        $data['page_url'] = 'Dashboard';
        $this->load->view('pm/index', $data);
    }

    public function page_map($pr, $sc) {
        $this->$pr($sc);
    }

    public function check_sess() {
        if (!$this->session->userdata('pm')) {
            redirect('projectmanager/login');
        }
    }

    public function login() {
        if ($this->session->userdata('pm')) {
            redirect('projectmanager');
        }
        $this->load->view('pm/login');
    }
    
    public function profile() {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Dashborad';
        $data['trainer'] = $this->projectmanager_model->get_single_pm($this->session->userdata('pm_code'));
        $this->load->view('pm/profile', $data);
    }

    public function clients() {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Clients';
        $data['client'] = $this->projectmanager_model->get_client($this->session->userdata('pm_code'));
        $this->load->view('pm/clients', $data);
    }

    public function add_program() {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Create Program';
        $data['msg'] = 0;
        if (isset($_POST['client'])) {
            $data['msg'] = $this->projectmanager_model->save_program();
            redirect('projectmanager/program_details/' . $data['msg']);
        }
        $data['client'] = $this->projectmanager_model->get_client($this->session->userdata('pm_code'));
        $this->load->view('pm/add-program', $data);
    }

    public function upcoming_programs($id) {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 0;
        $data['program'] = $this->projectmanager_model->get_basic_programs($this->session->userdata('pm_code'));
        $this->load->view('pm/upcoming-programs', $data);
    }
    
    public function past_programs($id) {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Past Programs';
        $data['msg'] = 0;
        $data['program'] = $this->projectmanager_model->get_past_programs($this->session->userdata('pm_code'));
        $this->load->view('pm/past-programs', $data);
    }

    public function program_details($id) {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 'new';
        if (isset($_POST['content_user'])) {
            $data['msg'] = $this->projectmanager_model->save_content();
        }
        if (isset($_POST['form_user'])) {
            $data['msg'] = $this->projectmanager_model->save_form();
        }
        if (isset($_POST['props_user'])) {
            $data['msg'] = $this->projectmanager_model->save_props();
        }
        $data['program'] = $this->projectmanager_model->get_single_program($id);

        $this->load->view('pm/program-details', $data);
    }

    public function engagement_requests($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        if (!empty($id)) {
            $this->load->model('projectmanager_model');
            $data['request'] = $this->projectmanager_model->get_requests($id);
        }
        $data['pid'] = $id;
        $this->load->view('pm/engagement-request', $data);
    }

    public function trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['trainer'] = $this->admin_model->get_trainer();
        $this->load->view('pm/trainers', $data);
    }

    public function inactive_trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['trainer'] = $this->admin_model->get_inactive_trainer();
        $this->load->view('pm/inactive_trainers', $data);
    }

    public function pending_trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['trainer'] = $this->admin_model->get_pending_trainer();
        $this->load->view('pm/pending_trainers', $data);
    }

    public function trainer_profile($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['trainer'] = $this->admin_model->get_single_trainer($id);
            $this->load->view('pm/trainer-profile', $data);
        }
    }

    public function trainer_engagment_request($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $data['engage'] = $this->admin_model->get_trainer_engagement($id);
            $this->load->view('pm/trainer-program-engagement', $data);
        }
    }

    public function edit_program($id) {
        if (!empty($id)) {
            $this->check_sess();
            $data['msg'] = "new";
            $data['page_url'] = 'Upcoming Programs';
            $this->load->model('projectmanager_model');
            if (isset($_POST['client'])) {
                $data['msg'] = $this->projectmanager_model->update_program();
            }
            $data['program'] = $this->projectmanager_model->get_single_program($id);
            $data['client'] = $this->projectmanager_model->get_client($this->session->userdata('pm_code'));
            $this->load->view('pm/edit-program', $data);
        }
    }
    
    public function add_content() {
        $this->check_sess();
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Content';
        $data['msg'] = 'new';
        if(isset($_POST['content_user']))
        {
            $data['msg'] = $this->projectmanager_model->save_single_content();
        }
        $this->load->view('pm/add-content', $data);
    }
    
    public function contents() {
        $this->check_sess();
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Content';
        $data['msg'] = 'new';
        $data['content'] = $this->projectmanager_model->get_all_content();
        $this->load->view('pm/content', $data);
    }
    
    public function payments() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Payments';
        $data['msg'] = 'new';
        $data['payment'] = $this->admin_model->get_all_payment();
        $this->load->view('pm/payment', $data);
    }

    public function logout() {
        $this->session->unset_userdata('pm', '');
        $this->session->unset_userdata('pm_name', '');
        $this->session->unset_userdata('pm_id', '');
        $this->session->unset_userdata('pm_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function ajax_page() {
        $this->load->model('projectmanager_model');
        echo $this->projectmanager_model->$_POST['page']($_POST);
    }

}
