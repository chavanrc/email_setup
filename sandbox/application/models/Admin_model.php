<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_model extends CI_Model {

    public $cdate = null;
    public $full_date = null;

    public function __construct() {
        $datestring = "%Y-%m-%d";
        $datestring1 = "%Y-%m-%d %h:%i:%s";
        $time = time();
        $this->cdate = mdate($datestring, $time);
        $this->full_date = mdate($datestring1, $time);
    }

    public function image_resize($file_name, $path, $newpath, $newWidth) {
        $img_size = getimagesize($path . $file_name);
        $newHeight = round(($newWidth / $img_size[0]) * $img_size[1]);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $file_name;
        $config['new_image'] = $newpath . $file_name;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $newWidth;
        $config['height'] = $newHeight;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    /* Checks User exists or not */

    public function admin_login($val) {
        $this->db->where('email', $val['username']);
        $this->db->where('password', md5($val['password']));
       // $this->db->where('password', $val['password']);
        $query = $this->db->get('application_users');
        if ($query->num_rows()) {
            $result = $query->result();
            if ($result[0]->user_type == 'admin') {
                $this->session->set_userdata('admin', $result[0]->email);
                $this->session->set_userdata('name', $result[0]->name);
                $this->session->set_userdata('user_id', $result[0]->user_id);
                $this->session->set_userdata('user_code', $result[0]->user_code);
                $this->session->set_userdata('user_photo', $result[0]->user_code);
                $this->session->set_userdata('type', $result[0]->user_type);
            }
            if ($result[0]->user_type == 'project manager') {
                $this->session->set_userdata('pm', $result[0]->email);
                $this->session->set_userdata('pm_name', $result[0]->name);
                $this->session->set_userdata('pm_id', $result[0]->user_id);
                $this->session->set_userdata('pm_code', $result[0]->user_code);
                $this->session->set_userdata('pm_photo', $result[0]->user_photo);
                $this->session->set_userdata('type', $result[0]->user_type);
            }
            if ($result[0]->user_type == 'trainer') {
                $this->session->set_userdata('trainer', $result[0]->email);
                $this->session->set_userdata('t_name', $result[0]->name);
                $this->session->set_userdata('t_id', $result[0]->user_id);
                $this->session->set_userdata('t_code', $result[0]->user_code);
                $this->session->set_userdata('t_photo', $result[0]->user_photo);
                $this->session->set_userdata('type', $result[0]->user_type);
            }
            return $result[0]->user_type;
        } else {
            return 0;
        }
    }

    public function get_state() {
        $query = $this->db->query("SELECT * FROM state");
        return $query->result();
    }

    public function get_city_list($id) {
        $query = $this->db->query("SELECT * FROM cities WHERE state='" . $id . "'");
        return $query->result();
    }

    public function get_city($val) {
        $query = $this->db->query("SELECT * FROM cities WHERE state='" . $val['state'] . "'");
        $result = $query->result();
        return json_encode($result);
    }

    public function save_client() {
        $logo = 'default.png';

        $path = './assets/upload/client/';
        $img_name = $_FILES['logo']['name'];
        $tmp_name = $_FILES['logo']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $this->image_resize($file_name, $path, $path, '250');
                $logo = $file_name;
            }
        }

        $data = array('client_name' => $_POST['fname'],
            'industry' => $_POST['industry'],
            'logo' => $logo,
            'address' => $_POST['address'],
            'person1' => $_POST['name1'],
            'email1' => $_POST['email1'],
            'phone1' => $_POST['mobile1'],
            'phone2' => $_POST['mobile2'],
            'email2' => $_POST['email2'],
            'person2' => $_POST['name2'],
            'payment_credit_days' => $_POST['payment'],
            'create_date' => $this->cdate
        );

        $this->db->insert('clients', $data);

        return $this->db->insert_id();
    }

    public function get_client() {
        $query = $this->db->query("SELECT * FROM clients");
        return $query->result();
    }

    public function get_single_client($id) {
        $query = $this->db->query("SELECT * FROM clients WHERE 	client_id='" . $id . "'");
        return $query->result();
    }

    public function update_client() {
        $logo = $_POST['cimg'];

        $path = './assets/upload/client/';
        $img_name = $_FILES['logo']['name'];
        $tmp_name = $_FILES['logo']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $this->image_resize($file_name, $path, $path, '250');
                $logo = $file_name;
            }
        }

        $data = array('client_name' => $_POST['fname'],
            'industry' => $_POST['industry'],
            'logo' => $logo,
            'address' => $_POST['address'],
            'person1' => $_POST['name1'],
            'email1' => $_POST['email1'],
            'phone1' => $_POST['mobile1'],
            'phone2' => $_POST['mobile2'],
            'email2' => $_POST['email2'],
            'person2' => $_POST['name2'],
            'payment_credit_days' => $_POST['payment'],
            'create_date' => $this->cdate
        );

        $this->db->where('client_id', $_POST['cid']);
        $this->db->update('clients', $data);

        return 1;
    }

    public function save_pm() {
        $check = $this->db->query("SELECT email FROM application_users WHERE email='" . $_POST['email'] . "'");
        if ($check->num_rows()) {
            return 'error';
        }
        $photo = 'no-image.jpg';
        $user_code = md5(uniqid(rand()));

        $path = './assets/upload/pm/';
        $img_name = $_FILES['photo']['name'];
        $tmp_name = $_FILES['photo']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $this->image_resize($file_name, $path, $path, '250');
                $photo = $file_name;
            }
        }

        $data = array('password' => md5($_POST['password']),
            'user_code' => $user_code,
            'name' => $_POST['fname'],
            'user_type' => 'project manager',
            'email' => $_POST['email'],
            'contact_number' => $_POST['mobile'],
            'user_photo' => $photo,
            'is_active' => '1',
            'admin_approved' => '1',
            'create_date' => $this->cdate);

        $this->db->insert('application_users', $data);

        return $user_code;
    }

    public function get_pm() {
        $query = $this->db->query("SELECT user_id,user_code,name,user_type,email,contact_number,user_photo,is_active,admin_approved,create_date FROM application_users WHERE user_type='project manager'");
        return $query->result();
    }

    public function get_single_pm($id) {
        $query = $this->db->query("SELECT user_id,name,user_type,email,contact_number,user_photo,is_active,admin_approved,create_date FROM application_users WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function update_pm() {
        if ($_POST['c_email'] != $_POST['email']) {
            $check = $this->db->query("SELECT email FROM application_users WHERE email='" . $_POST['email'] . "'");
            if ($check->num_rows()) {
                return 'error';
            }
        }
        $photo = $_POST['cimg'];

        $path = './assets/upload/pm/';
        $img_name = $_FILES['photo']['name'];
        $tmp_name = $_FILES['photo']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $this->image_resize($file_name, $path, $path, '250');
                $photo = $file_name;
            }
        }

        $data = array(
            'name' => $_POST['fname'],
            'user_type' => 'project manager',
            'email' => $_POST['email'],
            'contact_number' => $_POST['mobile'],
            'user_photo' => $photo,
            'is_active' => '1',
            'admin_approved' => '1');

        $this->db->where('user_id', $_POST['uid']);
        $this->db->update('application_users', $data);

        return 1;
    }

    public function assign_pm($val) {
        $data = array('client_id' => $val['cid'],
            'pm_id' => $val['pm'],
            'start_date' => $this->cdate,
            'status' => 1);

        $this->db->insert('client_projectmanagers', $data);
        $this->db->query("UPDATE clients SET status='1' WHERE client_id='" . $val['cid'] . "'");
    }

    public function check_client_assign($id) {
        $query = $this->db->query("SELECT pm.user_id,pm.name FROM client_projectmanagers cp, application_users pm WHERE cp.client_id='" . $id . "' AND pm.user_code=cp.pm_id");
        return $query->result();
    }

    public function get_pm_client_list($val) {
        $query = $this->db->query("SELECT c.client_name FROM client_projectmanagers cp, clients c WHERE cp.pm_id='" . $val['pm'] . "' AND c.client_id=cp.client_id");
        $result = $query->result();
        echo json_encode($result);
    }

    public function get_client_assign_pending() {
        $query = $this->db->query("SELECT client_id,client_name FROM clients WHERE status='0'");
        return $query->result();
    }

    public function save_trainer() {

        $check = $this->db->query("SELECT email FROM application_users WHERE email='" . $_POST['email'] . "'");
        if ($check->num_rows()) {
            return 'error';
        }
        $photo = 'no-image.jpg';
        $user_code = md5(uniqid(rand()));

        $password = md5('123');

        $data = array('user_code' => $user_code,
            'user_type' => 'trainer',
            'password' => $password,
            'name' => $_POST['pname'],
            'address' => $_POST['address1'],
            'city' => $_POST['city'],
            'state' => $_POST['state'],
            'country' => $_POST['country'],
            'contact_number' => $_POST['mobile'],
            'contact_number_landline' => $_POST['landline'],
            'user_dob' => $_POST['dob'],
            'user_gender' => $_POST['gender'],
            'email' => $_POST['email'],
            'skypeID' => $_POST['skype'],
            'create_date' => $this->cdate,
            'is_active' => '1',
            'admin_approved' => '1',
            'user_photo' => $photo,
        );

        $this->db->insert('application_users', $data);
        $msg = array('email' => $_POST['email'], 'password' => $password, 'name'=>$_POST['pname']);
        new_trainer_mail($_POST['email'], $msg);
        return 1;
    }

    public function update_trainer() {
        if ($_POST['c_email'] != $_POST['email']) {
            $check = $this->db->query("SELECT email FROM application_users WHERE email='" . $_POST['email'] . "'");
            if ($check->num_rows()) {
                return 'error';
            }
        }

        $photo = $_POST['cimg'];
        $cv = $_POST['c_cv'];

        $path = './assets/upload/trainer/';
        $img_name = $_FILES['photo']['name'];
        $tmp_name = $_FILES['photo']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $this->image_resize($file_name, $path, $path, '250');
                $photo = $file_name;
            }
        }

        $img_name = $_FILES['cv']['name'];
        $tmp_name = $_FILES['cv']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $cv = $file_name;
            }
        }

        $data = array(
            'name' => $_POST['pname'],
            'address' => $_POST['address1'],
            'city' => $_POST['city'],
            'state' => $_POST['state'],
            'country' => $_POST['country'],
            'contact_number' => $_POST['mobile'],
            'contact_number_landline' => $_POST['landline'],
            'user_dob' => $_POST['dob'],
            'user_gender' => $_POST['gender'],
            'email' => $_POST['email'],
            'skypeID' => $_POST['skype'],
            'create_date' => $this->cdate,
            'user_photo' => $photo,
            'user_cv' => $cv,
            'is_active' => '1',
            'freelancer' => $_POST['occupation'],
            'trainer_area' => implode(',', $_POST['specification']),
            'industry' => implode(',', $_POST['industry']),
            'expected_fee_per_day' => $_POST['fees'],
            'admin_approved' => '1',
        );

        $this->db->where('user_code', $_POST['uid']);
        $this->db->update('application_users', $data);

        return 1;
    }

    public function get_trainer() {
        $query = $this->db->query("SELECT user_id,user_code,name,trainer_area,email,contact_number,state,city,create_date,is_active,admin_approved,user_photo FROM application_users WHERE user_type='trainer' AND is_active='1' ORDER BY create_date DESC");
        return $query->result();
    }

    public function get_inactive_trainer() {
        $query = $this->db->query("SELECT user_id,user_code,name,trainer_area,email,contact_number,state,city,create_date,is_active,admin_approved,user_photo FROM application_users WHERE user_type='trainer' AND is_active='0' AND admin_approved='1' ORDER BY create_date DESC");
        return $query->result();
    }

    public function get_pending_trainer() {
        $query = $this->db->query("SELECT user_id,user_code,name,trainer_area,email,contact_number,state,city,create_date,is_active,admin_approved,user_photo FROM application_users WHERE user_type='trainer' AND admin_approved='0' ORDER BY create_date DESC");
        return $query->result();
    }

    public function get_program_title($val) {
        $query = $this->db->query("SELECT program FROM program_titles WHERE area='" . $val['area'] . "'");
        $result = $query->result();
        echo json_encode($result);
    }

    public function get_single_trainer($id) {
        $query = $this->db->query("SELECT * FROM application_users WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function get_trainer_workExperiance($id) {
        $query = $this->db->query("SELECT * FROM trainer_work_experiance WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function trainer_education($id) {
        $query = $this->db->query("SELECT * FROM trainer_education WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function trainer_certifications($id) {
        $query = $this->db->query("SELECT * FROM trainer_certifications WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function trainer_make_approve($val) {
        $this->db->query("UPDATE application_users SET is_active='1', admin_approved='1' WHERE user_code='" . $val['tid'] . "'");
        $query = $this->db->query("SELECT email, name application_users WHERE user_code='" . $val['tid'] . "'");
        $result = $query->result();
        $data = array('name'=>$result[0]->name,'email'=>$result[0]->email);
        trainer_approval_mail($result[0]->email, $data);
    }

    public function get_basic_trainer($id) {
        $query = $this->db->query("SELECT name,email,contact_number,user_code FROM application_users WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function get_trainer_engagement($id) {
        $query = $this->db->query("SELECT eg.*, p.project_title,p.location_of_training FROM training_engagements eg, training_projects p WHERE eg.user_code='" . $id . "' AND p.project_id=eg.project_id");
        return $query->result();
    }

    public function add_trainer_work() {
        $data = array('user_code' => $_POST['user'],
            'work_designation' => $_POST['designation'],
            'work_function' => '',
            'work_industry' => implode('/', $_POST['industry']),
            'work_period_from' => $_POST['pfrom'],
            'work_orgn' => $_POST['org'],
            'up_date' => $this->cdate,
            'work_period_to' => $_POST['pto']);

        $this->db->insert('trainer_work_experiance', $data);

        return 1;
    }

    public function update_trainer_work() {
        $data = array(
            'work_designation' => $_POST['designation'],
            'work_function' => '',
            'work_industry' => implode('/', $_POST['industry']),
            'work_period_from' => $_POST['pfrom'],
            'work_orgn' => $_POST['org'],
            'up_date' => $this->cdate,
            'work_period_to' => $_POST['pto']);

        $this->db->where('id', $_POST['id']);
        $this->db->update('trainer_work_experiance', $data);

        return 1;
    }

    public function add_trainer_experience() {
        $data = array('user_code' => $_POST['user'],
            'area_of_training' => $_POST['training_area'],
            'program_title' => $_POST['program'],
            'company' => $_POST['company'],
            'industry' => implode('/', $_POST['industry']),
            'up_date' => $this->cdate);

        $this->db->insert('trainer_training_experiance', $data);

        return 1;
    }

    public function get_trainer_experience($id) {
        $query = $this->db->query("SELECT * FROM trainer_training_experiance WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function get_single_trainer_work($code, $id) {
        $query = $this->db->query("SELECT * FROM trainer_work_experiance WHERE user_code='" . $code . "' AND id='" . $id . "'");
        return $query->result();
    }

    public function add_trainer_education() {
        $edu = explode(',', $_POST['qualificatioin']);
        $data = array('user_code' => $_POST['user'],
            'qualification' => $edu[0],
            'discipline' => $edu[1],
            'university' => $_POST['university'],
            'year_of_passing' => $_POST['year'],
            'up_date' => $this->cdate);

        $this->db->insert('trainer_education', $data);

        return 1;
    }

    public function add_trainer_certification() {
        $data = array('user_code' => $_POST['user'],
            'certification_in' => $_POST['title'],
            'certification_level' => $_POST['level'],
            'awarded_by' => $_POST['award'],
            'valid_upto' => $_POST['valid'],
            'up_date' => $this->cdate);

        $this->db->insert('trainer_certifications', $data);

        return 1;
    }

    public function get_single_program($id) {
        $query = $this->db->query("SELECT * FROM training_projects p, clients c WHERE p.project_id='" . $id . "' AND c.client_id=p.client_id");
        return $query->result();
    }

    public function get_basic_programs() {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT p.project_id,p.user_code,p.project_title,p.location_of_training,p.training_start_date,p.training_end_date,p.is_active,p.trainer_engage_flag,p.create_date,c.client_id,c.client_name FROM training_projects p, clients c WHERE p.training_start_date>='" . $today . "' AND c.client_id=p.client_id");
        return $query->result();
    }

    public function get_basic_past_programs() {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT p.project_id,p.user_code,p.project_title,p.location_of_training,p.training_start_date,p.training_end_date,p.is_active,p.create_date,c.client_id,c.client_name FROM training_projects p, clients c WHERE p.training_start_date<'" . $today . "' AND c.client_id=p.client_id");
        return $query->result();
    }

    public function accept_request($val) {
        $this->db->query("UPDATE training_engagements SET admin_approved='1', approval_date='" . $this->cdate . "' WHERE id='" . $val['rid'] . "'");
        $this->db->query("UPDATE training_projects SET trainer_engage_flag='1' WHERE project_id='" . $val['pid'] . "'");


        $data = array('project_id' => $val['pid'],
            'trainer_id' => $val['uid'],
            'training_date_from' => $val['from'],
            'trainer_date_to' => $val['to'],
            'amount' => $val['amt']);

        $this->db->insert('program_trainers', $data);
		
		 $data = array('project_id' => $val['pid'],
            'trainer_id' => $val['uid'],
            'program_date' => $val['from']);

        $this->db->insert('trainer_assigned', $data);

        $trainer = $this->get_basic_trainer($val['uid']);
        $program = $this->get_basic_program_detail($val['pid']);
        $msg = array('pname' => $program[0]->projet_title, 'email' => $trainer[0]->email, 'name' => $trainer[0]->name, 'status' => 'Approved');

        request_status_mail($trainer[0]->email, $msg);
    }

    public function get_basic_program_detail($id) {
        $query = $this->db->query("SELECT p.project_id,p.project_title,u.email,u.name FROM training_projects p, application_users u WHERE p.project_id='" . $id . "' AND u.user_code=p.user_code");
        return $query->result();
    }

    public function reject_request($val) {
        $this->db->query("UPDATE training_engagements SET status='0', reject_date='" . $this->cdate . "' WHERE id='" . $val['rid'] . "'");
    }

    public function get_assigned_trainers($id) {
        $query = $this->db->query("SELECT u.user_code,u.name FROM program_trainers p, application_users u WHERE p.project_id='" . $id . "' AND u.user_code=p.trainer_id");
        return $query->result();
    }

    public function get_progrom_date() {
        $month = date('m');
        $query = $this->db->query("SELECT project_id,project_title,training_start_date,training_duration,trainer_engage_flag FROM training_projects WHERE MONTH(training_start_date)='" . $month . "'");
        return $query->result();
    }

    public function save_trainer_calendar() {
        $data = array('trainer_id' => $_POST['date_user'],
            'tc_title' => $_POST['title'],
            'from_date' => $_POST['from'],
            'to_date' => $_POST['to']);

        $this->db->insert('trainer_calendar', $data);

        return 'Calendar';
    }

    public function get_trainer_calendor($id) {
        $query = $this->db->query("SELECT * FROM trainer_calendar WHERE trainer_id='" . $id . "'");
        return $query->result();
    }

    public function remove_work_experience($val) {
        $this->db->query("DELETE FROM trainer_work_experiance WHERE id='" . $val['rid'] . "'");
    }

    public function remove_training_experience($val) {
        $this->db->query("DELETE FROM trainer_training_experiance WHERE id='" . $val['rid'] . "'");
    }

    public function remove_education($val) {
        $this->db->query("DELETE FROM trainer_education WHERE id='" . $val['rid'] . "'");
    }

    public function remove_certification($val) {
        $this->db->query("DELETE FROM trainer_certifications WHERE id='" . $val['rid'] . "'");
    }

    public function search_trainer($val) {
        $filter = '';

        if ($val['state'] != '') {
            $filter .= " AND state='" . $val['state'] . "' ";
        }
        if ($val['city'] != '') {
            $filter .= " AND city='" . $val['city'] . "' ";
        }
        if ($val['industry'] != '') {
            $filter .= " AND industry LIKE'%" . $val['industry'] . "%' ";
        }
        if ($val['age'] != '') {
            $age_data = explode('-', $val['age']);
            $filter .= " AND (date_format(now(),'%Y') - date_format(user_dob,'%Y')) BETWEEN '" . $age_data[0] . "' and '" . $age_data[1] . "' ";
        }
        if ($val['exp']) {
            $exp = explode('-', $val['exp']);
            $filter .= " AND total_experiance_training BETWEEN '" . $exp[0] . "' and '" . $exp[1] . "' ";
        }
        if ($val['name'] != '') {
            $filter .= " AND name LIKE'%" . $val['name'] . "%' ";
        }
        if ($val['keyword'] != '') {
            $filter .= " AND cv_content LIKE'%" . $val['keyword'] . "%' ";
        }

        $this->session->set_userdata('search_filter', $filter);

        $query = $this->db->query("SELECT name,company_name,city,total_experiance_training,date_format(now(),'%Y') - date_format(user_dob,'%Y') as age, user_code FROM application_users WHERE user_type='trainer' AND is_active='1' " . $filter . " ORDER BY name ");
        $result = $query->result();
        echo json_encode($result);
    }

    public function save_content() {
        $path = './assets/upload/content/';
        $img_name = $_FILES['content']['name'];
        $tmp_name = $_FILES['content']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $project = '0';
        $client = '0';



        $data = array('filename' => $_POST['title'],
            'file_path' => $file,
            'project_id' => $project,
            'client_id' => $client,
            'upload_by' => $_POST['content_user'],
            'upload_date' => $this->cdate);

        $this->db->insert('training_content', $data);

        return 1;
    }

    public function get_all_content() {
        $query = $this->db->query("SELECT c.*, u.name,u.user_type,u.user_code FROM training_content c,application_users u WHERE u.user_code=c.upload_by ORDER BY c.id");
        return $query->result();
    }

    public function remove_content($val) {
        $this->db->query("DELETE FROM training_content WHERE id='" . $val['rid'] . "'");
    }

    public function forgot_password($val) {
        $query = $this->db->query("SELECT user_code,email FROM application_users WHERE email='" . $val['email'] . "'");

        if ($query->num_rows()) {
            $result = $query->result();
            $link = 'admin/update_password/' . $result[0]->user_code . '/' . custome_encode($result[0]->email);
            forgot_password_mail($val['email'], $link);
            return 1;
        }
        return 0;
    }

    public function update_password($val) {
        $query = $this->db->query("SELECT user_code,email FROM application_users WHERE email='" . $val['username'] . "' AND user_code='" . $val['user_code'] . "'");
        //return $this->db->last_query();
        if ($query->num_rows()) {
            $pass = md5($val['password']);
            $this->db->query("UPDATE application_users SET password='" . $pass . "' WHERE user_code='" . $val['user_code'] . "'");
            return 1;
        }
        return 0;
    }
    
    public function get_all_payment()
    {
        $query = $this->db->query("SELECT pt.*,p.project_id,p.project_title,u.user_code,u.name FROM program_trainers pt INNER JOIN training_projects p ON pt.project_id=p.project_id INNER JOIN application_users u ON  pt.trainer_id=u.user_code WHERE pt.trainer_invoice_flag='1'");
        return $query->result();
    }
    
    public function update_payment($val)
    {
        $data = array('trainer_paid_status'=>'1',
            'trainer_payment_date'=>$this->cdate);
        $this->db->where('id',$val['pid']);
        $this->db->update('program_trainers',$data);
    }
    
    public function change_password($val)
    {
        $old = md5($val['old_pass']);
        $check = $this->db->query("SELECT user_code FROM application_users WHERE password='".$old."' AND user_code='".$val['uid']."'");
        if($check->num_rows())
        {
            $new = md5($val['new_pass']);
            $check = $this->db->query("UPDATE  application_users SET password='".$new."' WHERE  user_code='".$val['uid']."'");
            return 1;
        }
        
        return 0;
    }
    
    public function get_request_details($id)
    {
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training,c.client_name,c.email1,c.phone1,u.email,u.name,u.company_name,u.city FROM training_engagements te INNER JOIN application_users u ON te.user_code=u.user_code, training_projects p , clients c  WHERE te.id='".$id."' AND p.project_id=te.project_id AND c.client_id=p.client_id");
        return $query->result();
    }
    
    public function save_review()
    {
        if($_POST['user_type']=='BDM')
        {
        $this->db->query("UPDATE training_engagements SET bdm_comments='".$_POST['comment']."',status='3' WHERE id='".$_POST['id']."'");
        }
        if($_POST['user_type']=='Client')
        {
        $this->db->query("UPDATE training_engagements SET client_comments='".$_POST['comment']."',status='4' WHERE id='".$_POST['id']."'");
        }
        return 1;
    }
    
    
    public function getCheckin($pid,$tid)
    {
        $query = $this->db->query("SELECT * FROM trainer_assigned WHERE project_id='".$pid."' AND trainer_id='".$tid."'");
        //return $this->db->last_query();
        return $query->result();
    }
    
    

}
