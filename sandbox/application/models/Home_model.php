<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class home_model extends CI_Model {

    public $cdate = null;
    public $full_date = null;

    public function __construct() {
        $datestring = "%Y-%m-%d";
        $datestring1 = "%Y-%m-%d %h:%i:%s";
        $time = time();
        $this->cdate = mdate($datestring, $time);
        $this->full_date = mdate($datestring1, $time);
    }

    public function trainer_login($pid, $tid) {
        $query = $this->db->query("SELECT user_code,name,email,user_type FROM application_users WHERE user_code='" . $tid . "'");
        if ($query->num_rows()) {
            $result = $query->result();
            $tid = $result[0]->user_code;
            $name = $result[0]->name;
            $email = $result[0]->email;
            $usertype = $result[0]->user_type;

            $projectId = "";
            $projectName = "";
            $city = "";

            $today = date('Y-m-d');
            $query1 = $this->db->query("SELECT p.project_id, p.project_title, p.location_of_training FROM trainer_assigned t, training_projects p WHERE t.trainer_id='" . $tid . "' AND t.program_date='" . $today . "' AND p.project_id=t.project_id");


            $result1 = $query1->result();
            if (!empty($result1)) {
                $projectId = $result1[0]->project_id;
                $projectName = $result1[0]->project_title;
                $city = $result1[0]->location_of_training;
            }

            $data = array('trainerId' => $tid, 'trainerName' => $name, 'trainerEmail' => $email, 'projectId' => $projectId, 'projectName' => $projectName, 'city' => $city, 'usertype' => $usertype);
            $this->session->set_userdata('user', $data);
        }
    }

    public function get_trainerCheckin($tid, $pid) {
        $query = $this->db->query("SELECT trainer_checkin_time FROM trainer_assigned WHERE trainer_id='" . $tid . "' AND project_id='" . $pid . "' AND trainer_checkin_status='1'");
        return $query->result();
    }

    public function trainer_checkin($val) {
        $this->db->query("UPDATE trainer_assigned SET trainer_checkin_status='1', trainer_checkin_time='" . $this->full_date . "' WHERE trainer_id='" . $val['tid'] . "' AND project_id='" . $val['pid'] . "' AND trainer_checkin_status!='1'");
    }

    public function trigger_preTest($val) {
        $this->db->query("UPDATE trainer_assigned SET pre_test_trigger='1' WHERE trainer_id='" . $val['tid'] . "' AND project_id='" . $val['pid'] . "' AND trainer_checkin_status='1'");
    }
    
    public function trigger_postTest($val) {
        $this->db->query("UPDATE trainer_assigned SET post_test_trigger='1' WHERE trainer_id='" . $val['tid'] . "' AND project_id='" . $val['pid'] . "' AND trainer_checkin_status='1'");
    }

    public function getPretest_questions($pid) {
        $check = $this->db->query("SELECT pre_test_trigger FROM trainer_assigned WHERE project_id='" . $pid . "' AND pre_test_trigger='1'");
        if ($check->num_rows()) {
            
            $query = $this->db->query("SELECT participants_type FROM training_projects WHERE project_id='" . $pid . "'");
            $result = $query->result();
            if (!empty($result)) {
                
                $query2 = $this->db->query("SELECT * FROM assessment_questions WHERE participant_type='" . $result[0]->participants_type . "' AND assessment_type='pre'");
                //return $this->db->last_query();
                return $query2->result();
            }
        }
    }
    
    

    public function getPosttest_questions($pid) {
        $check = $this->db->query("SELECT pre_test_trigger FROM trainer_assigned WHERE project_id='" . $pid . "' AND pre_test_trigger='1'");
        if ($check->num_rows()) {
            $query = $this->db->query("SELECT participants_type FROM training_projects WHERE project_id='" . $pid . "'");
            $result = $query->result();
            if (!empty($result)) {
                $query2 = $this->db->query("SELECT * FROM assessment_questions WHERE participant_type='" . $result[0]->participants_type . "' AND assessment_type='post'");
                return $query2->result();
            }
        }
    }
    
    public function getParticipants_list($id)
    {
        $query = $this->db->query("SELECT * FROM participants WHERE project_id='".$id."' ORDER BY attendance_flag DESC");
        return $query->result();
    }
    
    public function upload_photo()
    {
    ini_set('memory_limit', '240M');
        $img = "";
        $path = './App/assets/upload/';
        $img_name = $_FILES['img']['name'];
        $tmp_name = $_FILES['img']['tmp_name'];
        if(!empty($img_name))
        {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $img = $file_name;
                
                
	$SourceFile = $path.$file_name;
        $WaterMarkText = date('Y-m-d H:s a');
        $DestinationFile = $path.$file_name;
        list($width, $height) = getimagesize($SourceFile);
        $image_p = imagecreatetruecolor($width, $height);
        $image = imagecreatefromjpeg($SourceFile);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);
        $black = imagecolorallocate($image_p, 255, 193, 7);
        $font = './assets/fonts/Heebo-Bold.ttf';
        $font_size = 60;
        imagettftext($image_p, $font_size, 0, 50, 100, $black, $font, $WaterMarkText);
        if ($DestinationFile <> '') {
            imagejpeg($image_p, $DestinationFile, 50);
        } else {
            header('Content-Type: image/jpeg');
            imagejpeg($image_p, null, 50);
        };
        imagedestroy($image);
        imagedestroy($image_p);
                
            }
        }
        
        $data = array('project_id'=>$_POST['pid'],
            'trainer_id'=>$_POST['tid'],
            'ti_img'=>$img,
            'ti_date'=> $this->full_date);
        
        $this->db->insert('trainer_img', $data);
        return 1;
    }
    
    public function participant_login()
    {
        $today = date('Y-m-d');
        $this->db->where("phone_number", $_POST['email']);
        $this->db->where("app_password", $_POST['password']);
        //$query = $this->db->query("SELECT * FROM participants p, trainer_assigned t WHERE p.phone_number='".$_POST['email']."' AND p.app_password='".$_POST['password']."' AND  t.project_id=p.project_id AND t.program_date='".$today."'");
        $query = $this->db->get('participants');
        
        if($query->num_rows())
        {
        
            $result = $query->result();
            $query1 = $this->db->query("SELECT p.p_id,p.name,p.phone_number, t.project_id FROM participants p, trainer_assigned t WHERE p.phone_number='".$_POST['email']."' AND  t.project_id=p.project_id AND t.program_date='".$today."'");
            $result1 = $query1->result();
            //return $this->db->last_query();
            
            if(empty($result1))
            {
            	return 3;
            }
            
            $this->session->set_userdata("participant",$result1[0]->p_id);
            $this->session->set_userdata("pMobile",$result1[0]->phone_number);
            $this->session->set_userdata("pName",$result1[0]->name);
            $this->session->set_userdata("pId",$result1[0]->project_id);
            
            if(empty($result1[0]->name))
            {
                return 2;
            }
            return 1;
        }
        return 0;
    }
    
    public function save_participant()
    {
        $data = array('name'=>$_POST['fname'],
            'organization'=>$_POST['org'],
            'location'=>$_POST['city'],
            'vpa_address'=>$_POST['address'],
            'attendance_flag'=>1,
            'login_time'=>$this->full_date);
        
        $this->session->set_userdata("pName",$_POST['fname']);
        $this->db->where('p_id', $this->session->userdata('participant'));
        $this->db->update('participants',$data);
    }
    
    public function getParticipantDetails()
    {
        $uid = $this->session->userdata('participant');
        $pid = $this->session->userdata('pId');
        $query = $this->db->query("SELECT p.*, a.pre_test_trigger, a.post_test_trigger, tp.project_id, tp.project_title FROM participants p, trainer_assigned a, training_projects tp WHERE p.p_id='".$uid."' AND p.project_id='".$pid."' AND a.project_id=p.project_id AND tp.project_id=a.project_id");
        //echo $this->db->last_q
        return $query->result();
    }
    
    public function save_pre_test()
    {
        $query = $this->db->query("SELECT participants_type FROM training_projects WHERE project_id='" . $_POST['pid'] . "'");
            $result = $query->result();
            if (!empty($result)) {
                
                $query2 = $this->db->query("SELECT * FROM assessment_questions WHERE participant_type='" . $result[0]->participants_type . "' AND assessment_type='pre'");
                $question = $query2->result();
                if(!empty($question))
                {
                    $totalScore = 0;
                    foreach($question as $qn_data)
                    {
                        $pid = $_POST['pid'];
                        $uid = $_POST['uid'];
                        $qid = $qn_data->question_id;
                        $ans = $qn_data->correct_ans;
                        $rAns = "";
                        $score = 0;
                        if(isset($_POST['qn_'.$qid]))
                        {
                           $rAns = $_POST['qn_'.$qid];
                           if($rAns==$ans)
                           {
                               $score = 1;
                           }
                        }
                        $totalScore +=$score;
                        $data = array('project_id'=>$pid,
                            'participant_id'=>$uid,
                            'question_id'=>$qid,
                            'answer'=>$rAns,
                            'score'=>$score,
                            'question_type'=>'pre');
                        
                        $this->db->insert('assessment_answers',$data);
                    }
                    
                    $this->db->query("UPDATE participants SET pre_score='".$totalScore."' WHERE p_id='".$uid."'");
                }
                
            }
    }
    
    public function save_post_test()
    {
        $query = $this->db->query("SELECT participants_type FROM training_projects WHERE project_id='" . $_POST['pid'] . "'");
            $result = $query->result();
            if (!empty($result)) {
                
                $query2 = $this->db->query("SELECT * FROM assessment_questions WHERE participant_type='" . $result[0]->participants_type . "' AND assessment_type='post'");
                $question = $query2->result();
                if(!empty($question))
                {
                    $totalScore = 0;
                    foreach($question as $qn_data)
                    {
                        $pid = $_POST['pid'];
                        $uid = $_POST['uid'];
                        $qid = $qn_data->question_id;
                        $ans = $qn_data->correct_ans;
                        $rAns = "";
                        $score = 0;
                        if(isset($_POST['qn_'.$qid]))
                        {
                           $rAns = $_POST['qn_'.$qid];
                           if($rAns==$ans)
                           {
                               $score = 1;
                           }
                        }
                        $totalScore +=$score;
                        $data = array('project_id'=>$pid,
                            'participant_id'=>$uid,
                            'question_id'=>$qid,
                            'answer'=>$rAns,
                            'score'=>$score,
                            'question_type'=>'post');
                        
                        $this->db->insert('assessment_answers',$data);
                    }
                    
                    $this->db->query("UPDATE participants SET post_score='".$totalScore."' WHERE p_id='".$uid."'");
                }
                
            }
    }
    
    
    public function checkPreAttd()
    {
        $pid = $this->session->userdata('pId');
        $uid = $this->session->userdata('participant');
        
        $query = $this->db->query("SELECT project_id FROM assessment_answers WHERE project_id='".$pid."' AND participant_id='".$uid."' AND question_type='pre'");
        return $query->result();
    }
    
    public function checkPostAttd()
    {
        $pid = $this->session->userdata('pId');
        $uid = $this->session->userdata('participant');
        
        $query = $this->db->query("SELECT project_id FROM assessment_answers WHERE project_id='".$pid."' AND participant_id='".$uid."' AND question_type='post'");
        return $query->result();
    }
    
    public function getPhotos($id)
    {
        $query = $this->db->query("SELECT * FROM trainer_img WHERE project_id='".$id."'");
        return $query->result();
    }
    
    public function get_Time($id)
    {
        $query = $this->db->query("SELECT pre_test_trigger_time, post_test_trigger_time FROM trainer_assigned WHERE project_id='".$id."'");
        return $query->result();
    }
    
    public function gett_TotalAattd($id)
    {
        $query = $this->db->query("SELECT COUNT(p_id) as tPart FROM participants WHERE project_id='".$id."' AND attendance_flag='1'");
        return $query->result();
    }

}
