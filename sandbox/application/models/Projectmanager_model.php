<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class projectmanager_model extends CI_Model {

    public $cdate = null;
    public $full_date = null;

    public function __construct() {
        $datestring = "%Y-%m-%d";
        $datestring1 = "%Y-%m-%d %h:%i:%s";
        $time = time();
        $this->cdate = mdate($datestring, $time);
        $this->full_date = mdate($datestring1, $time);
    }

    public function image_resize($file_name, $path, $newpath, $newWidth) {
        $img_size = getimagesize($path . $file_name);
        $newHeight = round(($newWidth / $img_size[0]) * $img_size[1]);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $file_name;
        $config['new_image'] = $newpath . $file_name;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $newWidth;
        $config['height'] = $newHeight;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    /* Checks User exists or not */

    public function pm_login($val) {
        $this->db->where('email', $val['username']);
        $this->db->where('password', md5($val['password']));
        $this->db->where('user_type', 'project manager');
        $query = $this->db->get('application_users');
        if ($query->num_rows()) {
            $result = $query->result();
            $this->session->set_userdata('pm', $result[0]->email);
            $this->session->set_userdata('pm_name', $result[0]->name);
            $this->session->set_userdata('pm_id', $result[0]->user_id);
            $this->session->set_userdata('pm_code', $result[0]->user_code);
            $this->session->set_userdata('pm_photo', $result[0]->user_photo);
            $this->session->set_userdata('type', $result[0]->user_type);
            return 1;
        } else {
            return 0;
        }
    }
    
    public function get_single_pm($id)
    {
//        $this->db->where('user_code', $id);
//        $query = $this->db->get('application_users');
        $query = $this->db->query("SELECT * FROM application_users WHERE user_code='".$id."'");
        return $query->result();
    }

    public function get_client($id) {
        $query = $this->db->query("SELECT * FROM clients c, client_projectmanagers cp WHERE cp.pm_id='" . $id . "' AND c.client_id=cp.client_id");

        return $query->result();
    }

    public function get_single_client($id) {
        $query = $this->db->query("SELECT * FROM clients WHERE 	client_id='" . $id . "'");
        return $query->result();
    }

    public function save_program() {
        $data = array('user_code' => $this->session->userdata('pm_code'),
            'client_id' => $_POST['client'],
            'industry' => $_POST['client'],
            'project_title' => $_POST['pname'],
            'location_of_training' => $_POST['location'],
            'objective_of_training' => $_POST['objective'],
            'training_start_date' => $_POST['sdate'],
            'training_duration' => $_POST['duration'],
            'trainer_skillsets' => $_POST['skill_set'],
            'no_of_participants' => $_POST['participants'],
            'is_active' => '0',
            'create_date' => $this->cdate,
            'participant_level' => $_POST['participant_level'],
            'mail_flag' => '0',
            'trainer_engage_flag' => '0',
            'props' => $_POST['prop'],
            'props_delivery_status' => '0',
            'stay_arrangement' => $_POST['stay'],
            'travel_arrangement' => $_POST['travel'],
			'zone' => $_POST['zone'],
			'participants_type' => $_POST['participants_type'],
        );

        $this->db->insert('training_projects', $data);
        $this->session->set_flashdata('new_program', 'true');
        return $this->db->insert_id();
    }
    
    public function update_program() {
        $data = array('user_code' => $this->session->userdata('pm_code'),
            'client_id' => $_POST['client'],
            'industry' => $_POST['client'],
            'project_title' => $_POST['pname'],
            'location_of_training' => $_POST['location'],
            'objective_of_training' => $_POST['objective'],
            'training_start_date' => $_POST['sdate'],
            'training_duration' => $_POST['duration'],
            'trainer_skillsets' => $_POST['skill_set'],
            'no_of_participants' => $_POST['participants'],
            'is_active' => '0',
            'create_date' => $this->cdate,
            'participant_level' => $_POST['participant_level'],
            'mail_flag' => '0',
            'trainer_engage_flag' => '0',
            'props' => $_POST['prop'],
            'props_delivery_status' => '0',
            'stay_arrangement' => $_POST['stay'],
            'travel_arrangement' => $_POST['travel'],
        );
        $this->db->where('project_id', $_POST['pid']);
        $this->db->update('training_projects', $data);
        return $_POST['pid'];
    }

    public function get_single_program($id) {
        $query = $this->db->query("SELECT * FROM training_projects p, clients c WHERE p.project_id='" . $id . "' AND c.client_id=p.client_id");
        return $query->result();
    }

    public function get_basic_programs($id) {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,p.training_end_date,p.trainer_engage_flag,p.is_active,p.create_date,c.client_id,c.client_name FROM training_projects p, clients c WHERE p.user_code='" . $id . "' AND p.training_start_date>='" . $today . "' AND c.client_id=p.client_id");
        return $query->result();
    }
    
    public function count_engagement_request($id)
    {
        $query = $this->db->query("SELECT COUNT(project_id) as total FROM training_engagements WHERE project_id='".$id."'");
        $result = $query->result();
        return $result[0]->total;
    }

    public function get_past_programs($id) {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,p.training_end_date,p.is_active,p.create_date,c.client_id,c.client_name FROM training_projects p, clients c WHERE p.user_code='" . $id . "' AND p.training_start_date<'" . $today . "' AND c.client_id=p.client_id");
        return $query->result();
    }

    public function save_content() {
        $path = './assets/upload/content/';
        $img_name = $_FILES['content']['name'];
        $tmp_name = $_FILES['content']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $project = $_POST['project'];
        $client = $_POST['client'];

        if ($_POST['ctype'] == 'project') {
            $client = '0';
        }
        if ($_POST['ctype'] == 'client') {
            $project = '0';
        }

        $data = array('filename' => $_POST['title'],
            'file_path' => $file,
            'project_id' => $project,
            'client_id' => $client,
            'upload_by' => $_POST['content_user'],
            'upload_date' => $this->cdate);

        $this->db->insert('training_content', $data);

        return 'Content';
    }

    public function get_project_content($id, $cid=NULL) {
        $query = $this->db->query("SELECT tc.*, u.name, u.user_type, u.user_code FROM training_content tc, application_users u  WHERE tc.project_id='" . $id . "'  AND u.user_code=tc.upload_by");
        return $query->result();
    }

    public function save_form() {
        $path = './assets/upload/form/';
        $img_name = $_FILES['content']['name'];
        $tmp_name = $_FILES['content']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = content_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $data = array(
            'form_file_name' => $file,
            'project_id' => $_POST['project'],
            'form_type' => $_POST['ftype'],
            'program_date' => $_POST['fdate'],
            'upload_by' => $_POST['form_user'],
            'upload_date' => $this->cdate);

        $this->db->insert('program_forms', $data);
        
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,c.client_name,u.email FROM training_projects p INNER JOIN application_users u ON p.user_code=u.user_code, clients c  WHERE p.project_id='".$_POST['project']."' AND c.client_id=p.client_id");
        $project = $query->result();
        
        
        
        $email = $project[0]->email;
        
        $data = array('project'=>$project[0]->project_title,
            'start_date'=>$project[0]->training_start_date,
            'company'=>$project[0]->client_name,
            'type'=>$_POST['ftype'],
            'link'=>$file);
        
        form_attach_mail($email, $data);

        return 'Form';
    }

    public function get_project_form($id) {
        $query = $this->db->query("SELECT tf.*, u.name, u.user_type, u.user_code FROM program_forms tf, application_users u  WHERE tf.project_id='" . $id . "'  AND u.user_code=tf.upload_by");
        return $query->result();
    }
    
    public function save_images() {
        $photo = "";

        $path = './assets/upload/images/';
        $img_name = $_FILES['img']['name'];
        $tmp_name = $_FILES['img']['tmp_name'];
        $img_size = count($img_name);
        for ($i = 0; $i < $img_size; $i++) {
            if ($file_name = image_upload($path, $img_name[$i], $tmp_name[$i])) {
                $photo = $file_name;

                $data = array(
                    'project_id' => $_POST['project'],
                    'pi_img' => $photo,
                    'upload_by' => $_POST['img_user'],
                    'pi_date' => $this->cdate);

                $this->db->insert('program_img', $data);
            }
        }

        return 'Image';
    }
    
    public function get_project_images($id)
    {
        $query = $this->db->query("SELECT * FROM trainer_img WHERE project_id='".$id."'");
        return $query->result();
    }

    public function save_props() {
        $data = array(
            'prop_name' => $_POST['pname'],
            'project_id' => $_POST['project'],
            'quantity' => $_POST['qnt'],
            'requested_by' => $_POST['props_user'],
            'requested_date' => $this->cdate);

        $this->db->insert('program_props', $data);
        return 'Props';
    }

    public function get_project_props($id) {
        $query = $this->db->query("SELECT p.*, u.name, u.user_type, u.user_code FROM program_props p, application_users u  WHERE p.project_id='" . $id . "'  AND u.user_code=p.requested_by");
        return $query->result();
    }

    public function get_requests($id, $code = NULL) {
        $query = $this->db->query("SELECT r.*, u.name, u.user_code, u.email, u.contact_number FROM training_engagements r, application_users u WHERE r.project_id='" . $id . "' AND u.user_code=r.user_code");
        return $query->result();
    }

    public function accept_request($val) {
        $this->db->query("UPDATE training_engagements SET status='1', engage_date='" . $this->cdate . "', admin_comments='".$val['comment']."' WHERE id='" . $val['rid'] . "'");
    }

    public function reject_request($val) {
        $this->db->query("UPDATE training_engagements SET status='0', reject_date='" . $this->cdate . "', admin_comments='".$val['comment']."' WHERE id='" . $val['rid'] . "'");
        $trainer = $this->get_basic_trainer($val['uid']);
        $program = $this->get_basic_program_detail($val['rid']);
        $msg = array('pname'=>$program[0]->projet_title,'email'=>$trainer[0]->email,'name'=>$trainer[0]->name,'status'=>'Rejected');
        
        request_status_mail($trainer[0]->email, $msg);
    }
    
    public function get_basic_program_detail($id)
    {
        $query = $this->db->query("SELECT p.project_id,p.project_title,u.email,u.name FROM training_projects p, application_users u WHERE p.project_id='".$id."' AND u.user_code=p.user_code");
        return $query->result();
    }
    
    public function get_basic_trainer($id) {
        $query = $this->db->query("SELECT name,email,company_name,city,contact_number,user_code FROM application_users WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function remove_content($val) {
        $this->db->query("DELETE FROM training_content WHERE id='" . $val['rid'] . "'");
    }

    public function remove_form($val) {
        $this->db->query("DELETE FROM program_forms WHERE id='" . $val['rid'] . "'");
    }

    public function program_props($val) {
        $this->db->query("DELETE FROM program_props WHERE id='" . $val['rid'] . "'");
    }
    
    public function remove_stay($val) {
        $this->db->query("DELETE FROM program_stay WHERE id='" . $val['rid'] . "'");
    }
    
    public function remove_travel($val) {
        $this->db->query("DELETE FROM program_travel WHERE id='" . $val['rid'] . "'");
    }

    public function update_props_status($val) {
        $this->db->query("UPDATE program_props SET delivery_status='" . $val['status'] . "' WHERE id='" . $val['sid'] . "'");
        
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,c.client_name FROM training_projects p, clients c  WHERE p.project_id='".$val['pid']."' AND c.client_id=p.client_id");
        $project = $query->result();
        
        $query2 = $this->db->query("SELECT u.name,u.email FROM program_trainers p, application_users u WHERE p.project_id='".$val['pid']."' AND u.user_code=p.trainer_id");
        $result = $query2->result();
        $email = '';
        foreach($result as $da)
        {
           $email .=$da->email.',';
        }
        
        $email = rtrim($email,',');
        
        $data = array('project'=>$project[0]->project_title,
            'start_date'=>$project[0]->training_start_date,
            'company'=>$project[0]->client_name,
            'status'=>'Props');
        
        props_status_mail($email, $data);
        
    }

    public function save_stay() {
        foreach ($_POST['trainer'] as $tid) {

            $data = array('project_id' => $_POST['project'],
                'trainer_id' => $tid,
                'hotel_name' => $_POST['hotel'],
                'checkin_time' => $_POST['checkin'],
                'checkout_time' => $_POST['checkout'],
                'entered_by' => $_POST['stay_user']);

            $this->db->insert('program_stay', $data);
        }
        
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,c.client_name FROM training_projects p, clients c  WHERE p.project_id='".$_POST['project']."' AND c.client_id=p.client_id");
        $project = $query->result();
        
        $query2 = $this->db->query("SELECT u.name,u.email FROM program_trainers p, application_users u WHERE p.project_id='".$_POST['project']."' AND u.user_code=p.trainer_id");
        $result = $query2->result();
        $email = '';
        foreach($result as $da)
        {
           $email .=$da->email.',';
        }
        
        $email = rtrim($email,',');
        
        $data = array('project'=>$project[0]->project_title,
            'start_date'=>$project[0]->training_start_date,
            'company'=>$project[0]->client_name,
            'status'=>'Stay');
        
        props_status_mail($email, $data);

        return 'Stay';
    }

    public function get_stay_details($id) {
        $query = $this->db->query("SELECT s.*,u.name,u.email,u.user_code FROM program_stay s, application_users u WHERE s.project_id='" . $id . "' AND u.user_code=s.trainer_id");
        return $query->result();
    }

    public function save_travel() {
        $file = '';
        $path = './assets/upload/travel/';
        $img_name = $_FILES['screen']['name'];
        $tmp_name = $_FILES['screen']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        foreach ($_POST['trainer'] as $tid) {
            
            $data = array('project_id' => $_POST['project'],
                'trainer_id' => $tid,
                'date' => $_POST['date'],
                'from_loc' => $_POST['from'],
                'to_loc' => $_POST['to'],
                'mode' => $_POST['mode'],
                'screenshot' => $file,
                'entered_by' => $_POST['travel_user']);

            $this->db->insert('program_travel', $data);
        }
        
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,c.client_name FROM training_projects p, clients c  WHERE p.project_id='".$_POST['project']."' AND c.client_id=p.client_id");
        $project = $query->result();
        
        $query2 = $this->db->query("SELECT u.name,u.email FROM program_trainers p, application_users u WHERE p.project_id='".$_POST['project']."' AND u.user_code=p.trainer_id");
        $result = $query2->result();
        $email = '';
        foreach($result as $da)
        {
           $email .=$da->email.',';
        }
        
        $email = rtrim($email,',');
        
        $data = array('project'=>$project[0]->project_title,
            'start_date'=>$project[0]->training_start_date,
            'company'=>$project[0]->client_name,
            'status'=>'Travel');
        
        props_status_mail($email, $data);

        return 'Travel';
    }
    
    public function get_travel_details($id) {
        $query = $this->db->query("SELECT t.*,u.name,u.email,u.user_code FROM program_travel t, application_users u WHERE t.project_id='" . $id . "' AND u.user_code=t.trainer_id");
        return $query->result();
    }
    
    public function get_program_trainers($id)
    {
        $query = $this->db->query("SELECT t.*,u.name,u.email,u.user_code FROM program_trainers t, application_users u WHERE t.project_id='".$id."' AND u.user_code=t.trainer_id");
        return $query->result();
    }
    
    public function get_progrom_date($id)
    {
        $month = date('m');
        $query = $this->db->query("SELECT project_id,project_title,training_start_date,training_duration,trainer_engage_flag FROM training_projects WHERE user_code='".$id."' AND MONTH(training_start_date)='".$month."'");
        return $query->result();
    }
    
    public function get_all_content()
    {
        $query = $this->db->query("SELECT c.*, u.name,u.user_type,u.user_code FROM training_content c,application_users u WHERE  u.user_code=c.upload_by ORDER BY c.id");
        return $query->result();
    }
    
    public function save_single_content() {
        $path = './assets/upload/content/';
        $img_name = $_FILES['content']['name'];
        $tmp_name = $_FILES['content']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $project = '0';
        $client = '0';

        $data = array('filename' => $_POST['title'],
            'file_path' => $file,
            'project_id' => $project,
            'client_id' => $client,
            'upload_by' => $_POST['content_user'],
            'upload_date' => $this->cdate);

        $this->db->insert('training_content', $data);

        return 1;
    }
    
    public function send_to_bdm($val)
    {
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training,c.client_name,c.email1,c.phone1,u.email FROM training_projects p INNER JOIN application_users u ON p.user_code=u.user_code, clients c  WHERE p.project_id='".$val['pid']."' AND c.client_id=p.client_id");
       // return $this->db->last_query();
        $details = $query->result();
        $trainer = $this->get_basic_trainer($val['uid']);
        $msg = array('project'=>$details[0]->project_title,
            'project_date'=>$details[0]->training_start_date,
            'company'=>$details[0]->client_name,
            'city'=>$details[0]->location_of_training,
            'email'=>$details[0]->email1,
            'name'=>$trainer[0]->name,
            'tcom'=>$trainer[0]->company_name,
            'tcity'=>$trainer[0]->city,
            'contact'=>$trainer[0]->contact_number,
            'temail'=>$trainer[0]->email,
            'bdm'=>$val['name'],
            'link'=>'admin/review/'.  custome_encode($val['rid'].'-'.$val['pid']).'/'. custome_encode('BDM'),
            );
        echo $msg['link'];
        engagement_request_mail($val['email'], $msg);
    }
    
    public function send_to_client($val)
    {
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training,c.client_name,c.email1,c.phone1,u.email FROM training_projects p INNER JOIN application_users u ON p.user_code=u.user_code, clients c  WHERE p.project_id='".$val['pid']."' AND c.client_id=p.client_id");
       // return $this->db->last_query();
        $details = $query->result();
        $trainer = $this->get_basic_trainer($val['uid']);
        $msg = array('project'=>$details[0]->project_title,
            'project_date'=>$details[0]->training_start_date,
            'company'=>$details[0]->client_name,
            'city'=>$details[0]->location_of_training,
            'email'=>$details[0]->email1,
            'name'=>$trainer[0]->name,
            'tcom'=>$trainer[0]->company_name,
            'tcity'=>$trainer[0]->city,
            'contact'=>$trainer[0]->contact_number,
            'temail'=>$trainer[0]->email,
            'bdm'=>$val['name'],
            'link'=>'admin/review/'.  custome_encode($val['rid'].'-'.$val['pid']).'/'. custome_encode('Client'),
            );
       // echo $msg['link'];
        engagement_request_mail($val['email'], $msg);
    }

}
