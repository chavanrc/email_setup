<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class trainer_model extends CI_Model {

    public $cdate = null;
    public $full_date = null;

    public function __construct() {
        $datestring = "%Y-%m-%d";
        $datestring1 = "%Y-%m-%d %h:%i:%s";
        $time = time();
        $this->cdate = mdate($datestring, $time);
        $this->full_date = mdate($datestring1, $time);
    }

    public function image_resize($file_name, $path, $newpath, $newWidth) {
        $img_size = getimagesize($path . $file_name);
        $newHeight = round(($newWidth / $img_size[0]) * $img_size[1]);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $file_name;
        $config['new_image'] = $newpath . $file_name;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $newWidth;
        $config['height'] = $newHeight;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    public function get_new_program()
    {
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,p.training_end_date,p.training_duration,p.is_active,p.create_date,c.client_id,c.client_name FROM training_projects p, clients c WHERE trainer_engage_flag='0' AND c.client_id=p.client_id");
        return $query->result();
    }
    
    public function check_request($id,$code)
    {
        $query = $this->db->query("SELECT project_id,status,admin_approved,reject_date FROM training_engagements WHERE user_code='".$code."' AND project_id='".$id."'");
        return $query->result();
    }
    
    public function trainer_enagage_request($val)
    {
        $data = array('project_id'=>$val['pid'],
            'user_code'=>$val['code'],
            'status'=>'2',
            'trainer_comments'=>$val['comment'],
            'apply_date'=>$this->cdate,
            'admin_approved'=>'0');
        
        $this->db->insert('training_engagements',$data);
        
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training,c.client_name,c.email1,c.phone1,u.email FROM training_projects p INNER JOIN application_users u ON p.user_code=u.user_code, clients c  WHERE p.project_id='".$val['pid']."' AND c.client_id=p.client_id");
        $details = $query->result();
        $trainer = $this->get_basic_trainer($val['code']);
        $msg = array('project'=>$details[0]->project_title,
            'project_date'=>$details[0]->training_start_date,
            'company'=>$details[0]->client_name,
            'city'=>$details[0]->location_of_training,
            'email'=>$details[0]->email1,
            'name'=>$trainer[0]->name,
            'tcom'=>$trainer[0]->company_name,
            'tcity'=>$trainer[0]->city,
            'contact'=>$trainer[0]->contact_number,
            'temail'=>$trainer[0]->email
            );
        
        engagement_request_mail($details[0]->email, $msg);
        
    }
    
    public function get_basic_trainer($id)
    {
        $query = $this->db->query("SELECT name,email,company_name,city,contact_number FROM application_users WHERE user_code='".$id."'");
        return $query->result();
    }

    public function get_basic_program_detail($id)
    {
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,u.email,u.name FROM training_projects p, application_users u WHERE p.project_id='".$id."' AND u.user_code=p.user_code");
        return $query->result();
    }

    public function get_upcoming_programs($id)
    {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT p.project_id,p.client_id,p.project_title,p.location_of_training,p.training_start_date,p.training_duration,c.client_name FROM program_trainers pt, training_projects p, clients c WHERE pt.trainer_id='".$id."' AND p.project_id=pt.project_id AND p.training_start_date>='".$today."' AND c.client_id=p.client_id");
        return $query->result();
    }
    
    public function get_past_programs($id)
    {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT p.project_id,p.client_id,p.project_title,p.location_of_training,p.training_start_date,p.training_duration,c.client_name FROM training_engagements e, training_projects p, clients c WHERE e.user_code='".$id."' AND e.status='1' AND e.admin_approved='1' AND p.project_id=e.project_id AND p.training_start_date<'" . $today . "' AND c.client_id=p.client_id");
        return $query->result();
    }
    
    public function get_trainer_program_list($id)
    {
        $query = $this->db->query("SELECT pt.id,p.project_id,p.project_title FROM program_trainers pt, training_projects p WHERE pt.trainer_id='".$id."' AND p.project_id=pt.project_id");
        return $query->result();
    }
    
   

    public function get_progrom_date($id)
    {
        $month = date('m');
        $query = $this->db->query("SELECT p.project_id,p.project_title,t.training_date_from,t.trainer_date_to FROM training_projects p, program_trainers t WHERE t.trainer_id='".$id."' AND MONTH(t.training_date_from)='".$month."' AND p.project_id=t.project_id");
        return $query->result();
    }
    
    public function get_all_content($id)
    {
        $query = $this->db->query("SELECT c.*, u.name,u.user_type,u.user_code FROM training_content c,application_users u WHERE  u.user_code=c.upload_by AND (u.user_code='".$id."' || u.user_type='admin' || u.user_type='project manager') ORDER BY c.id");
        return $query->result();
    }
    
    public function save_content() {
        $path = './assets/upload/content/';
        $img_name = $_FILES['content']['name'];
        $tmp_name = $_FILES['content']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $project = '0';
        $client = '0';

        $data = array('filename' => $_POST['title'],
            'file_path' => $file,
            'project_id' => $project,
            'client_id' => $client,
            'upload_by' => $_POST['content_user'],
            'upload_date' => $this->cdate);

        $this->db->insert('training_content', $data);

        return 1;
    }
    
    public function  save_payment()
    {
        $path = './assets/upload/content/';
        $img_name = $_FILES['content']['name'];
        $tmp_name = $_FILES['content']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }
        
        $data = array('trainer_invoice_flag'=>1,
            'trainer_invoice_file'=>$file,
            'trainer_invoice_date'=>$this->cdate);
        
        $this->db->where('id',$_POST['pid']);
        $this->db->update('program_trainers',$data);
        
        return 1;
    }
    
    public function get_payment_list($id)
    {
        $query = $this->db->query("SELECT pt.*,p.project_id,p.project_title FROM program_trainers pt, training_projects p WHERE pt.trainer_id='".$id."' AND pt.trainer_invoice_flag='1' AND p.project_id=pt.project_id");
        return $query->result();
    }

}
