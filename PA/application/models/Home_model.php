<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Home_model extends CI_Model {

    public $cdate = null;
    public $full_date = null;

    public function __construct() {
        $datestring = "%Y-%m-%d";
        $datestring1 = "%Y-%m-%d %h:%i:%s";
        $time = time();
        $this->cdate = mdate($datestring, $time);
        $this->full_date = mdate($datestring1, $time);
    }

    public function image_resize($file_name, $path, $newpath, $newWidth) {
        $img_size = getimagesize($path . $file_name);
        $newHeight = round(($newWidth / $img_size[0]) * $img_size[1]);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $file_name;
        $config['new_image'] = $newpath . $file_name;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $newWidth;
        $config['height'] = $newHeight;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    // ** product module 


    public function getProject() {
        $today = date('Y-m-d');
        $query1 = $this->db->query("SELECT p.project_id, p.project_title, t.trainer_id FROM trainer_assigned t, training_projects p WHERE  t.program_date='" . $today . "' AND p.project_id=t.project_id AND p.client_id='4'");
        return $query1->result();
    }

    public function participants_signup() {

        $today = date('Y-m-d H:i:s');

        $check = $this->db->query("SELECT phone_number FROM participants WHERE phone_number='" . $_POST['mobile'] . "' AND project_id='" . $_POST['project'] . "'");

        if (!$check->num_rows()) {
            $data = array('attendance_flag' => '1',
                'name' => $_POST['name'],
                'phone_number' => $_POST['mobile'],
                'organization' => $_POST['org'],
                'location' => $_POST['city'],
                'project_id' => $_POST['project'],
                'email' => $_POST['email'],
                'login_time' => $today,
                'vpa_address' => $_POST['vpa']);

            $this->db->insert('participants', $data);
            $pid = $this->db->insert_id();

            $query = $this->db->query("SELECT project_title FROM training_projects WHERE project_id='" . $_POST['project'] . "'");
            $result = $query->result();
            $pName = $result[0]->project_title;

            $query1 = $this->db->query("SELECT u.name FROM trainer_assigned t, application_users u WHERE t.project_id='" . $_POST['project'] . "' AND u.user_code=t.trainer_id");
            $result1 = $query1->result();
            $tName = $result1[0]->name;

           // $this->session->set_userdata('uid', $pid);
           //$this->session->set_userdata('uname', $_POST['name']);
           // $this->session->set_userdata('mobile', $_POST['mobile']);
           // $this->session->set_userdata('pname', $pName);
           // $this->session->set_userdata('pid', $_POST['project']);
           // $this->session->set_userdata('tname', $tName);
           // $this->session->set_userdata('logout', 0);
            
            $_SESSION['uid'] = $pid;
            $_SESSION['uname'] = $_POST['name'];
            $_SESSION['mobile'] = $_POST['mobile'];
            $_SESSION['pname'] = $pName;
            $_SESSION['tname'] = $tName;
            $_SESSION['logout'] = 0;
            $_SESSION['pid'] = $_POST['project'];

            return 1;
        } else {
            return 0;
        }
    }

    public function participants_details() {
        $uid = $_SESSION['uid'];
        $pid = $_SESSION['pid'];
        $data[] = array();
        $preAttd = 0;
        $postAttd = 0;
        $feedback = 0;
        $query = $this->db->query("SELECT p.*, a.pre_test_trigger, a.post_test_trigger FROM participants p, trainer_assigned a  WHERE p.p_id='" . $uid . "' AND p.project_id='" . $pid . "' AND a.project_id=p.project_id ");
        $result = $query->result();

        $query1 = $this->db->query("SELECT project_id FROM assessment_answers WHERE project_id='" . $pid . "' AND participant_id='" . $uid . "' AND question_type='pre'");
        $result1 = $query1->result();
        if (!empty($result1)) {
            $preAttd = 1;
        }

        $query2 = $this->db->query("SELECT project_id FROM assessment_answers WHERE project_id='" . $pid . "' AND participant_id='" . $uid . "' AND question_type='post'");
        $result2 = $query2->result();
        if (!empty($result2)) {
            $postAttd = 1;
        }

        $query3 = $this->db->query("SELECT id FROM feedback_answers WHERE project_id='" . $pid . "' AND p_id='" . $uid . "'");
        $result3 = $query3->result();
        if (!empty($result3)) {
            $feedback = 1;
        }

        if (!empty($result)) {
            $data[0] = array('preScore' => $result[0]->pre_score,
                'postScore' => $result[0]->post_score,
                'preTrigger' => $result[0]->pre_test_trigger,
                'postTrigger' => $result[0]->post_test_trigger,
                'vpa' => $result[0]->vpa_address,
                'preAttd' => $preAttd,
                'postAttd' => $postAttd,
                'feedback' => $feedback);

            return $data;
        }
        return $data;
    }

    public function preTest_questions() {
        $data[] = array();
        $pId = $_SESSION['pid'];
        $check = $this->db->query("SELECT pre_test_trigger FROM trainer_assigned WHERE project_id='" . $pId . "' AND pre_test_trigger='1'");
        if ($check->num_rows()) {
            $query = $this->db->query("SELECT participants_type FROM training_projects WHERE project_id='" . $pId . "'");
            $result = $query->result();
            if (!empty($result)) {
                $query2 = $this->db->query("SELECT * FROM assessment_questions WHERE participant_type='" . $result[0]->participants_type . "' AND assessment_type='pre'");

                $question = $query2->result();
                if (!empty($question)) {
                    $no = 0;
                    foreach ($question as $qn_data) {
                        $data1[$no] = array(
                            'questionId' => $qn_data->question_id,
                            'question' => $qn_data->question_text,
                            'option_1' => $qn_data->answer_1,
                            'option_2' => $qn_data->answer_2,
                            'option_3' => $qn_data->answer_3,
                            'option_4' => $qn_data->answer_4);
                        $no++;
                    }
                    $data[0] = array('status' => 1, 'qBody' => $data1);
                } else {
                    $data[0] = array('status' => 0);
                }
            } else {
                $data[0] = array('status' => 0);
            }
        } else {
            $data[0] = array('status' => 0);
        }


        return $data;
    }

    public function postTest_questions() {
        $data[] = array();
        $pId = $_SESSION['pid'];
        $check = $this->db->query("SELECT pre_test_trigger FROM trainer_assigned WHERE project_id='" . $pId . "' AND post_test_trigger='1'");
        if ($check->num_rows()) {
            $query = $this->db->query("SELECT participants_type FROM training_projects WHERE project_id='" . $pId . "'");
            $result = $query->result();
            if (!empty($result)) {
                $query2 = $this->db->query("SELECT * FROM assessment_questions WHERE participant_type='" . $result[0]->participants_type . "' AND assessment_type='post'");
                $question = $query2->result();
                if (!empty($question)) {
                    $no = 0;
                    foreach ($question as $qn_data) {
                        $data1[$no] = array(
                            'questionId' => $qn_data->question_id,
                            'question' => $qn_data->question_text,
                            'option_1' => $qn_data->answer_1,
                            'option_2' => $qn_data->answer_2,
                            'option_3' => $qn_data->answer_3,
                            'option_4' => $qn_data->answer_4);
                        $no++;
                    }
                    $data[0] = array('status' => 1, 'qBody' => $data1);
                } else {
                    $data[0] = array('status' => 0);
                }
            } else {
                $data[0] = array('status' => 0);
            }
        } else {
            $data[0] = array('status' => 0);
        }


        return $data;
    }

    public function save_pre_test() {
        $pid = $_SESSION['pid'];
        $uid = $_SESSION['uid'];
        //$data = json_decode($_POST['ans']);
        // print_r($data);
        //echo $data->{1};
        //return;
        $query = $this->db->query("SELECT participants_type FROM training_projects WHERE project_id='" . $pid . "'");
        $result = $query->result();
        if (!empty($result)) {

            $query2 = $this->db->query("SELECT * FROM assessment_questions WHERE participant_type='" . $result[0]->participants_type . "' AND assessment_type='pre'");
            $question = $query2->result();
            if (!empty($question)) {
                $totalScore = 0;
                foreach ($question as $qn_data) {
                    $qid = $qn_data->question_id;
                    $ans = $qn_data->correct_ans;
                    $rAns = "";
                    $score = 0;
                    // if(array_key_exists($qid,$data))
                    // {
                    $rAns = $_POST['ans_' . $qid];
                    if ($rAns == $ans) {
                        $score = 1;
                    }
                    // }
                    $totalScore +=$score;
                    $data1 = array('project_id' => $pid,
                        'participant_id' => $uid,
                        'question_id' => $qid,
                        'answer' => $rAns,
                        'score' => $score,
                        'question_type' => 'pre');

                    $this->db->insert('assessment_answers', $data1);
                    echo $this->db->last_query();
                }

                $this->db->query("UPDATE participants SET pre_score='" . $totalScore . "' WHERE p_id='" . $uid . "'");
            }
        }
    }

    public function save_post_test() {
        $pid = $_SESSION['pid'];
        $uid = $_SESSION['uid'];
        //$data = json_decode($_POST['ans']);
        // print_r($data);
        //echo $data->{1};
        //return;
        $query = $this->db->query("SELECT participants_type FROM training_projects WHERE project_id='" . $pid . "'");
        $result = $query->result();
        if (!empty($result)) {

            $query2 = $this->db->query("SELECT * FROM assessment_questions WHERE participant_type='" . $result[0]->participants_type . "' AND assessment_type='post'");
            $question = $query2->result();
            if (!empty($question)) {
                $totalScore = 0;
                foreach ($question as $qn_data) {
                    $qid = $qn_data->question_id;
                    $ans = $qn_data->correct_ans;
                    $rAns = "";
                    $score = 0;
                    // if(array_key_exists($qid,$data))
                    // {
                    $rAns = $_POST['ans_' . $qid];
                    if ($rAns == $ans) {
                        $score = 1;
                    }
                    // }
                    $totalScore +=$score;
                    $data1 = array('project_id' => $pid,
                        'participant_id' => $uid,
                        'question_id' => $qid,
                        'answer' => $rAns,
                        'score' => $score,
                        'question_type' => 'post');

                    $this->db->insert('assessment_answers', $data1);
                    echo $this->db->last_query();
                }

                $this->db->query("UPDATE participants SET post_score='" . $totalScore . "' WHERE p_id='" . $uid . "'");
            }
        }
    }

    public function getFeedback() {
        $query = $this->db->query("SELECT * FROM feedback_questions");
        $result = $query->result();
        $data = array();
        if (!empty($result)) {
            
            $no = 0;
            foreach ($result as $rs_data) {
                $data[$no] = array('qid' => $rs_data->id, 'qTitle' => $rs_data->question_text);
                $no++;
            }
            
        }
        return $data;
    }

    public function saveFeedback() {
        $pid = $_SESSION['pid'];
        $uid = $_SESSION['uid'];
        //$data = json_decode($_POST['ans']);

        $query = $this->db->query("SELECT * FROM feedback_questions");
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $data1 = array('p_id' => $uid,
                    'project_id' => $pid,
                    'question_id' => $rs_data->id,
                    'answer' => $_POST['ans_' . $rs_data->id]);

                $this->db->insert('feedback_answers', $data1);
            }
        }
    }

    public function bhim() {
        $uid = $_SESSION['uid'];
        $this->db->query("UPDATE participants SET bhim_download = bhim_download+1 WHERE p_id='" . $uid . "'");
        //echo $this->db->last_query();
    }
    
    public function update_vpa($val){
        $uid = $_SESSION['uid'];
        $this->db->query("UPDATE participants SET vpa_address = '".$val['vpa']."' WHERE p_id='" . $uid . "'");
    }

}
