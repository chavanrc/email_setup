<?php
session_start();
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {

    public function index() {
        $this->load->model('home_model');
        $data['msg'] = 2;
        if (isset($_SESSION['uid'])) {
                redirect('home/dashboard');
        }
        $data['project'] = $this->home_model->getProject();
        if (isset($_POST['name'])) {
            $data['msg'] = $this->home_model->participants_signup();
            if($data['msg']==1){
                redirect('home/dashboard');
            }
        }
        $this->load->view('admin/login', $data);
    }

    public function dashboard() {
        $this->check_sess();
        $this->load->model('home_model');
        $data['msg'] = 3;
        $data['prj'] = $this->home_model->participants_details();
        if(!empty($data['prj'])){
            $this->load->view('admin/dashboard', $data);
        }
    }
    
    public function preTest(){
        $this->check_sess();
        $this->load->model('home_model');
        $data['msg'] = 3;
        if(isset($_POST['pid'])){
            $this->home_model->save_pre_test();
            redirect('home/dashboard');
        }
        $data['pre'] = $this->home_model->preTest_questions();
        $this->load->view('admin/preTest', $data);
    }
    
    public function postTest(){
        $this->check_sess();
        $this->load->model('home_model');
        $data['msg'] = 3;
        if(isset($_POST['pid'])){
            $this->home_model->save_post_test();
            redirect('home/dashboard');
        }
        $data['pre'] = $this->home_model->postTest_questions();
        $this->load->view('admin/postTest', $data);
    }
    
    public function feedback(){
        $this->check_sess();
        $this->load->model('home_model');
        $data['msg'] = 3;
        if(isset($_POST['pid'])){
            $this->home_model->saveFeedback();
            redirect('home/dashboard');
        }
        $data['pre'] = $this->home_model->getFeedback();
        $this->load->view('admin/feedback', $data);
    }

    public function check_sess() {
        if (!isset($_SESSION['uid'])) {
            redirect('home/index');
        }
    }
    
    public function ajax_page() {
        $this->load->model('home_model');
        echo $this->home_model->$_POST['page']($_POST);
    }

}
