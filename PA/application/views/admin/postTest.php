<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php include 'css-files.php'; ?>
        <style>
            .form-control{
                height: 38px;
                font-size: 15px;
            }
            .topHeader{
                padding: 5px;
                background: #3aaaf9;
                color:#fff;
            }
            .boxWrap{
                padding: 10px;
                margin: 7px;
                background: #fff;
                border: solid 1px #e8e8e8;
                border-radius: 2px;
            }
            .iconWrap{
                width: 30px;
                float: left;
                margin-right: 10px;
            }
            .iconWrap img{
                width: 30px;
            }
            .bhimWrap{
                width: 80px;
                float: left;
                margin-right: 10px; 
            }
            .bhimWrap img{
                width: 80px;
            }
        </style>
    </head>
    <body style="background:#f9f9f9;">
        <div class="container">
            <div class="row">
                <div class="topHeader">
                    <img src="<?php echo base_url(); ?>assets/images/wagon-wheel.png" style="width:40px; margin-right: 10px; float: left;"/>
                    <div style="padding:0px; ">
                        <span style="font-size: 17px;"><?php echo $_SESSION['uname']; ?></span><br/>
                        <span><?php echo $_SESSION['mobile']; ?></span>
                    </div>
                </div>
                <div style="font-size:20px;padding: 8px;">
                    Post Assessment Survey
                </div>
                <form action="" method="POST" id="formSurvey">
                    <input type="hidden" value="<?php echo $_SESSION['pid']; ?>" name="pid"/>
                    <?php
                    if (!empty($pre[0]['qBody'])) {
                        $no = 0;
                        foreach ($pre[0]['qBody'] as $pr_data) {
                            $no++;
                            ?>

                            <div class="wrap<?php echo $no; ?>" <?php if ($no > 1) { ?> style="display:none;" <?php } ?>>
                                <div class="error<?php echo $no; ?>" style="padding:5px; padding-left: 20px; color: red;"></div>
                                <div class="boxWrap">
                                    <div style="font-size:20px;">
                                        <?php echo $no; ?>. <?php echo $pr_data['question']; ?>
                                    </div>
                                    <?php if (!empty($pr_data['option_1'])) { ?>
                                        <div style="font-size:18px; margin-top: 15px; padding-top:5px;">
                                            <label style="font-weight:normal;">
                                                <input type="radio" class="ans_<?php echo $pr_data['questionId']; ?>" name="ans_<?php echo $pr_data['questionId']; ?>" value="<?php echo $pr_data['option_1']; ?>"/>
                                                <?php echo $pr_data['option_1']; ?>
                                            </label>
                                        </div>
                                        <?php
                                    }
                                    if (!empty($pr_data['option_2'])) {
                                        ?>
                                        <div style="font-size:18px; padding-top:5px;">
                                            <label style="font-weight:normal;">
                                                <input type="radio" class="ans_<?php echo $pr_data['questionId']; ?>" name="ans_<?php echo $pr_data['questionId']; ?>" value="<?php echo $pr_data['option_2']; ?>"/>
                                                <?php echo $pr_data['option_2']; ?>
                                            </label>
                                        </div>
                                        <?php
                                    }
                                    if (!empty($pr_data['option_3'])) {
                                        ?>
                                        <div style="font-size:18px; padding-top:5px;">
                                            <label style="font-weight:normal;">
                                                <input type="radio" class="ans_<?php echo $pr_data['questionId']; ?>" name="ans_<?php echo $pr_data['questionId']; ?>" value="<?php echo $pr_data['option_3']; ?>"/>
                                                <?php echo $pr_data['option_3']; ?>
                                            </label>
                                        </div>
                                        <?php
                                    }
                                    if (!empty($pr_data['option_4'])) {
                                        ?>
                                        <div style="font-size:18px; padding-top:5px;">
                                            <label style="font-weight:normal;">
                                                <input type="radio" class="ans_<?php echo $pr_data['questionId']; ?>" name="ans_<?php echo $pr_data['questionId']; ?>" value="<?php echo $pr_data['option_4']; ?>"/>
                                                <?php echo $pr_data['option_4']; ?>
                                            </label>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div style="position:fixed; bottom: 0px; width: 100%;">
                                    <?php if ($no < count($pre[0]['qBody'])) {
                                        ?>
                                        <button type="button" class="btn btn-block btn-success next-btn" qid="<?php echo $pr_data['questionId']; ?>" no="<?php echo $no; ?>" style="font-size: 22px; padding: 10px;">Next</button>
                                        <?php
                                    } else {
                                        ?>
                                        <button type="button" class="btn btn-block btn-warning next-btn" qid="<?php echo $pr_data['questionId']; ?>" no="<?php echo $no; ?>" style="font-size: 22px; padding: 10px;">Submit</button>
                                    <?php }
                                    ?>

                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </form>
            </div>
        </div>
        <!-- ./wrapper -->
        <?php include 'js-files.php'; ?>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery-validate.bootstrap-tooltip.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.next-btn').click(function () {
                    var no = $(this).attr('no');
                    var qid = $(this).attr('qid');
                    var nxt = parseInt(no) + 1;
                    var total = '<?php echo count($pre[0]['qBody']); ?>';
                    var totalQn = parseInt(total);

                    var ans = $('.ans_' + qid + ':checked').val();
                    if (ans == null) {
                        $('.error' + no + '').html("Select one option");
                        return false;
                    }
                    if(totalQn==no){
                        $('#formSurvey').submit();
                        return false;
                    }
                    if (nxt <= totalQn) {
                        $('.wrap' + nxt + '').show();
                        $('.wrap' + no + '').hide();
                    }
                });

            });

        </script>
    </body>
</html>