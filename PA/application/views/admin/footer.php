 <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
     Developed &amp; Maintaned By: <a href="http://www.webdreams.in/" rel="nofollow" target="_blank">WebDreams India</a>
    </div>
    <!-- Default to the left -->
    <strong>© <span id="copyright">
          <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>
          </span> <span class="primary-clr">Aluprints</span></strong> . All Rights Reserved.
  </footer>