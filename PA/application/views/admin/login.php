<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Admin Login</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php include 'css-files.php'; ?>
        <style>
            .form-control{
                height: 38px;
                font-size: 15px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row" style="min-height: 550px;">
                <div class="col-md-4 col-md-offset-4" style="margin-top: 10%;">
                    <div class="text-center">
                        <img src="<?php echo base_url(); ?>assets/images/logo.png" style="width:200px;"/><br/>
                        <h4>Participant Signup</h4>
                    </div>
                    <div class="login-panel panel panel-default">
                        
                        <div class="panel-body regWrap" style="background: #f9f9f9;">
                            <form id="user-signup" action="" method="POST">
                                <fieldset>
                                    <?php if ($msg==0) { ?>
                                        <div class="alert alert-danger" style="margin-top:10px;">
                                            This mobile number already exist
                                        </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <select class="form-control" name="project">
                                            <option value=""> - Select Project -</option>
                                            <?php
                                            if (!empty($project)) {
                                                foreach ($project as $pd_data) {
                                                    ?>
                                                    <option value="<?php echo $pd_data->project_id; ?>"><?php echo $pd_data->project_id; ?> - <?php echo $pd_data->project_title; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>

                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Full Name" name="name" type="text">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Mobile Number" name="mobile" type="text">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Email id" name="email" type="text">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="City" name="city" type="text">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Organisation" name="org" type="text">
                                    </div>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Virtual Private Address" name="vpa" type="text">
                                        Leave Blank if you don't know
                                    </div>
                                    <button type="submit" class="btn btn-success btn-block" name="login" style="font-size:18px;">SUBMIT</button>
<!--                                    <div style="padding:15px" class="text-center">
                                        OR
                                    </div>
                                    <div class="text-center">
                                        <a href="#" class="btn loginBtn" style="font-size: 18px;">Login</a>
                                    </div>-->
                                    
                                </fieldset>
                            </form>
                        </div>
                        
                        <div class="panel-body loginWrap" style="background: #f9f9f9; display: none;">
                            <form id="user-login" action="" method="POST">
                                <fieldset>
                                    <?php if ($msg==3) { ?>
                                        <div class="alert alert-danger" style="margin-top:10px;">
                                            Mobile Number is Wrong
                                        </div>
                                    <?php } ?>
                                    <div class="form-group">
                                        <input class="form-control" placeholder="Mobile Number" name="lmobile" type="text">
                                    </div>
                                    <button type="submit" class="btn btn-success btn-block" name="login" style="font-size:18px;">LOGIN</button>
                                    <div style="padding:15px" class="text-center">
                                        OR
                                    </div>
                                    <div class="text-center">
                                        <a href="#" class="btn signupBtn" style="font-size: 18px;">Signup</a>
                                    </div>
                                    
                                </fieldset>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- ./wrapper -->
        <?php include 'js-files.php'; ?>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery-validate.bootstrap-tooltip.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $("#user-signup").validate({
                    rules: {
                        project: "required",
                        name: "required",
                        email: "required",
                        mobile: "required",
                        city: "required",
                        org: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                });
                
                $("#user-login").validate({
                    rules: {
                        lmobile: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                });
                
                $('.loginBtn').click(function(e){
                   e.preventDefault();
                   $('.regWrap').hide();
                   $('.loginWrap').show();
                });
                
                $('.signupBtn').click(function(e){
                   e.preventDefault();
                   $('.regWrap').show();
                   $('.loginWrap').hide();
                });
                
            });

        </script>
    </body>
</html>