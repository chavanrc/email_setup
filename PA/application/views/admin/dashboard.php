<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?php include 'css-files.php'; ?>
        <style>
            .form-control{
                height: 38px;
                font-size: 15px;
            }
            .topHeader{
                padding: 5px;
                background: #3aaaf9;
                color:#fff;
            }
            .boxWrap{
                padding: 10px;
                margin: 7px;
                background: #fff;
                border: solid 1px #e8e8e8;
                border-radius: 2px;
            }
            .iconWrap{
                width: 30px;
                float: left;
                margin-right: 10px;
            }
            .iconWrap img{
                width: 30px;
            }
            .bhimWrap{
                width: 80px;
                float: left;
                margin-right: 10px; 
            }
            .bhimWrap img{
                width: 80px;
            }
        </style>
    </head>
    <body style="background:#f9f9f9;">
        <div class="container">
            <div class="row">
                <div class="topHeader">
                    <img src="<?php echo base_url(); ?>assets/images/wagon-wheel.png" style="width:40px; margin-right: 10px; float: left;"/>
                    <div style="padding:0px; ">
                        <span style="font-size: 17px;"><?php echo $_SESSION['uname']; ?></span><br/>
                        <span><?php echo $_SESSION['mobile']; ?></span>
                    </div>
                </div>
                <div class="boxWrap">
                    <span style="font-size: 19px;"><?php echo $_SESSION['pname']; ?></span><br/>
                    <span style="color: #929292;"><?php echo date('d M Y'); ?></span><br/>
                    <span style="color: #929292;">Trainer : <?php echo $_SESSION['tname']; ?></span><br/>
                </div>
                <div class="boxWrap preSurvey" style="margin-top:20px;" tStatus="<?php
                if ($prj[0]['preTrigger'] == 1) {
                    echo 1;
                } else {
                    echo 0;
                }
                ?>" aStatus="<?php
                     if ($prj[0]['preAttd'] == 1) {
                         echo 1;
                     } else {
                         echo 0;
                     }
                     ?>">
                    <div class="iconWrap" style="height:50px;">
                        <img src="<?php echo base_url(); ?>assets/images/assement.png"/>
                    </div>
                    <span style="font-size: 21px; color: #8ed421;">Pre Assessment Survey</span><br/>
                    <?php
                    if ($prj[0]['preAttd'] == 1) {
                        ?>
                        <span style="color: #f18f12;">Taken</span><br/>
                        <?php
                    } else {
                        ?>
                        <span style="color: #f18f12;">Take Survey</span><br/>
                        <?php
                    }
                    ?>

                    <div class="clearfix"></div>
                </div>
                <div class="boxWrap postSurvey" tStatus="<?php
                if ($prj[0]['postTrigger'] == 1) {
                    echo 1;
                } else {
                    echo 0;
                }
                ?>" aStatus="<?php
                     if ($prj[0]['postAttd'] == 1) {
                         echo 1;
                     } else {
                         echo 0;
                     }
                     ?>">
                    <div class="iconWrap" style="height:50px;">
                        <img src="<?php echo base_url(); ?>assets/images/assement.png"/>
                    </div>
                    <span style="font-size: 21px; color: #8ed421;">Post Assessment Survey</span><br/>
                    <?php
                    if ($prj[0]['postAttd'] == 1) {
                        ?>
                        <span style="color: #f18f12;">Taken</span><br/>
                        <?php
                    } else {
                        ?>
                        <span style="color: #f18f12;">Take Survey</span><br/>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                </div>
                <div class="boxWrap feedback" status="<?php
                if ($prj[0]['feedback'] == 1) {
                    echo 1;
                } else {
                    echo 0;
                }
                ?>">
                    <div class="iconWrap">
                        <img src="<?php echo base_url(); ?>assets/images/feedback.png"/>
                    </div>
                    <span style="font-size: 21px; color: #f18f12;">Feedback</span><br/>
                    <div class="clearfix"></div>
                </div>
                <div class="boxWrap update-vpa-btn">
                    <div class="iconWrap" style="height:50px;">
                        <img src="<?php echo base_url(); ?>assets/images/vpa.png"/>
                    </div>
                    <span style="font-size: 21px; color: #8ed421;">VPA</span><br/>
                    <span>
                        <?php
                        if (!empty($prj[0]['vpa'])) {
                            echo $prj[0]['vpa'];
                        } else {
                            echo "Enter VPA";
                        }
                        ?>
                    </span><br/>
                    <div class="clearfix"></div>
                </div>
                <div class="boxWrap bhimDownload">
                    <div class="bhimWrap">
                        <img src="<?php echo base_url(); ?>assets/images/bhim.png"/>
                    </div>
                    <span style="font-size: 21px; color: #f18f12;">Download</span><br/>
                    <div class="clearfix"></div>
                </div>
                

            </div>
        </div>

        <div class="modal fade" id="vpaWrap" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Update VPA</h4>
                    </div>
                    <div class="modal-body">
                        <input class="form-control vpaText" type="text" value="<?php echo $prj[0]['vpa']; ?>">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary vpa-btn">Submit</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <!-- ./wrapper -->
        <div class="page_spin">
            <br/>
            <div class="spin_icon">
                <i class="fa fa-spinner fa-spin"></i><br/>
                <span>One moment ...</span>
            </div>
        </div>
<?php include 'js-files.php'; ?>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery-validate.bootstrap-tooltip.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.update-vpa-btn').click(function () {
                    $('#vpaWrap').modal('show');
                });

                $('.loginBtn').click(function (e) {
                    e.preventDefault();
                    $('.regWrap').hide();
                    $('.loginWrap').show();
                });

                $('.signupBtn').click(function (e) {
                    e.preventDefault();
                    $('.regWrap').show();
                    $('.loginWrap').hide();
                });

                $('.preSurvey').click(function () {

                    var tStatus = $(this).attr('tStatus');
                    var aStatus = $(this).attr('aStatus');
                    if (tStatus == '0') {
                        alert("Pre Survey is not started");
                        return false;
                    }
                    if (aStatus == '1') {
                        alert("Survey Taken");
                        return false;
                    }

                    window.location.href = "<?php echo base_url(); ?>home/preTest";
                });

                $('.postSurvey').click(function () {

                    var tStatus = $(this).attr('tStatus');
                    var aStatus = $(this).attr('aStatus');
                    if (tStatus == '0') {
                        alert("Post Survey is not started");
                        return false;
                    }
                    if (aStatus == '1') {
                        alert("Survey Taken");
                        return false;
                    }

                    window.location.href = "<?php echo base_url(); ?>home/postTest";
                });

                $('.feedback').click(function () {

                    var status = $(this).attr('status');
                    if (status == '1') {
                        alert("Feedback Sent");
                        return false;
                    }

                    window.location.href = "<?php echo base_url(); ?>home/feedback";
                });

                $('.bhimDownload').click(function () {
                    var dataString = "page=bhim";
                    $('.page_spin').show();
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>home/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.href = "https://play.google.com/store/apps/details?id=in.org.npci.upiapp";
                        }, //success fun end

                    });//ajax end
                });

                $('.vpa-btn').click(function () {
                    var vpa = $('.vpaText').val();
                    var dataString = "vpa="+vpa+"&page=update_vpa";
                    if (vpa != "") {
                        $('.page_spin').show();
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>home/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.href = "<?php echo base_url(); ?>home/dashboard";
                            }, //success fun end

                        });//ajax end
                    }
                });

            });

        </script>
    </body>
</html>