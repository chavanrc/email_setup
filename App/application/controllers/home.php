<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class home extends CI_Controller {

    public function index($id = NULL) {
        $this->load->model('home_model');
        $data['msg'] = 2;
        
        $this->load->view('index', $data);
    }
    
    public function dashboard($pid,$tid){
        $this->load->model('home_model');
        $data['msg'] = "";
        $this->home_model->trainer_login($pid,$tid);
        if($this->session->userdata('user'))
        {
            $data['checkIn'] = $this->home_model->get_trainerCheckin($tid,$pid);
            $this->load->view('dashboard', $data);
        }
    }
    
    public function participants_list($id)
    {
        $this->load->model('home_model');
        $data['part'] = $this->home_model->getParticipants_list($id);
        $data['attd'] = $this->home_model->gett_TotalAattd($id);
        $this->load->view('participants-list', $data);
    }
    
    public function upload_photo($id)
    {

		

        $this->load->model('home_model');
        $data['pid'] = $id;
        $data['tid'] = "";
        $data['msg'] = 0;
        if(isset($_POST['tid']))
        {
            $data['msg'] = $this->home_model->upload_photo();
        }
        $data['photo'] = $this->home_model->getPhotos($id);
        $this->load->view('upload-photo', $data);
    }
    
    public function get_photo($id)
    {
        $this->load->model('home_model');
        $data['pid'] = $id;
        $data['tid'] = "";
        $data['msg'] = 0;
        if(isset($_POST['tid']))
        {
            $data['msg'] = $this->home_model->upload_photo();
        }
        $data['photo'] = $this->home_model->getPhotos($id);
        $this->load->view('get-photo', $data);
    }
    
    public function upload_photo1($id)
    {
        $this->load->model('home_model');
        
        if(isset($_POST['tid']))
        {
            $data['msg'] = $this->home_model->upload_photo();
        }
    }
    
    public function pre_questions($id)
    {
        $this->load->model('home_model');
        $data['dTime'] = $this->home_model->get_Time($id);
        $data['question'] = $this->home_model->getPretest_questions($id);
        $this->load->view('pre-questions', $data);
    }
    
    public function post_questions($id)
    {
        $this->load->model('home_model');
        $data['dTime'] = $this->home_model->get_Time($id);
        $data['question'] = $this->home_model->getPosttest_questions($id);
        $this->load->view('post-questions', $data);
    }

    public function send_email_reminder()
    {
        $this->load->model('home_model');
        $this->home_model->email_reminder();
//        foreach($result as $rs_data)
//            {
//                $data['name'] = $rs_data->emp_name;
//                $data['to'] = $rs_data->emp_email;
//                $data['photo'] = $rs_data->emp_photo;
//                $data['eid'] = $rs_data->emp_id;
//                $data1['val'] = $data;
//                
//                $this->load->view('email-temp', $data1);
//                //send_email($data);
//            }
    }


    public function check_sess() {
        if (!$this->session->userdata('user')) {
            redirect('home/index');
        }
    }
   

   
    public function logout() {
        $this->session->unset_userdata('user', '');
        $this->session->unset_userdata('user_name', '');
        redirect('home/index');
    }

    public function ajax_page() {
        $this->load->model('home_model');
        echo $this->home_model->$_POST['page']($_POST);
    }

}
