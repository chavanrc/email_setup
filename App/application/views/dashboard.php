<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard | 360&deg; Feedback Tool</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web-orange.css">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-green layout-boxed sidebar-mini" >
        <div class="wrapper">
            <?php include 'layout/header.php'; ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php include 'layout/menu.php'; ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Welcome
                        <span class="pull-right" style="font-size: 13px;">
                            <?php echo date('M d, Y'); ?>
                        </span>
                    </h1>
                </section>
                <!-- Main content -->
                <section class="content container-fluid">
                    <div class="row">
                        <div class="col-md-8">
                            <div style="background: #fff; padding: 15px; border: solid 1px #ddd; border-radius: 3px;">
                                <?php
                                if(!empty($userData['projectId']))
                                {
                                ?>
                                <div style="font-size: 13px; color: #9e9e9e;">
                                    Project Details
                                </div>
                                <div style="font-size: 18px;">
                                    <?php echo $userData['projectName']; ?>
                                </div>
                                <div style="font-size: 15px; color: #0a5784;">
                                    <i class="fa fa-map-marker"></i> <?php echo $userData['city']; ?><br/>
                                    <?php if(!empty($checkIn))
                                    {
                                        ?>
                                    <i class="fa fa-sign-in"></i> <?php echo date_formate($checkIn[0]->trainer_checkin_time); ?>
                                        <?php
                                    }
                                        ?>
                                </div>
                                <?php
                                }
                                else{
                                    ?>
                                <h4>Today you don't have any programs</h4>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <?php //include 'layout/footer.php'; ?>
        </div>
        <!-- ./wrapper -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/web.js"></script>
        <?php include 'layout/script.php'; ?>
    </body>
</html>