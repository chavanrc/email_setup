<!doctype html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Step 2 Axis Way | Instructions</title>
    <meta name="description" content="">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/timer/jquery.countdownTimer.css">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" type="image/x-icon" property="shortcut icon">
</head>
<body>

    <div class="container">

        <?php include 'layout/header.php'; ?>

        <div class="row bread primary-bg">
            <div class="col-md-12 user">
                <div class="username"><i class="fa fa-user"></i> Hello <span class="white-clr">Sampath Shetty</span></div>
                <span id="current_timer"></span>
            </div>
        </div>



        <div class="row ptb-15">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul class="bread">
                        <li><a href="#">Register</a></li> /
                        <li class="primary-clr"><strong>Read Test Instructions</strong></li> /
                        <li>Take Test</li>
                    </ul>

                </div>
            </div>
            <div class="col-md-12">
                <div class="main-head2 secondary-clr">Instructions</div>
                <p><i class="fa fa-plus-square primary-clr"></i> Shuffle all Questions & Answer/options also </p>
                <p><i class="fa fa-plus-square primary-clr"></i> Add Count down Timer at right hand side corner. Timer: 50 Minutes</p>
                <p><i class="fa fa-plus-square primary-clr"></i> After completion of exam time, Test should be automatically submitted with selected answer choice.</p>
                <p><i class="fa fa-plus-square primary-clr"></i> Only one attempt for each participant, if any technical issue comes we will give permission for re-attempt of exam from backend.</p>
                <p><i class="fa fa-plus-square primary-clr"></i> No Test Screen Minimize option.</p>
                <p><i class="fa fa-plus-square primary-clr"></i> After clicking on Start Test Button, Test shall start in New Window and not in same window.</p>
                <p><i class="fa fa-plus-square primary-clr"></i> In backend, report generation in charts and a sheet mentioning Name, Email ID, Employee Code, Questions attempted out of total, Total Test Score, Percentage.</p>
                <p><i class="fa fa-plus-square primary-clr"></i> All questions have diff weightage and the marks should appear next to the question. The score should also be calculated according to the marks against the questions.</p>
                <p><i class="fa fa-plus-square primary-clr"></i> Participants should not be able to refresh/F5 or reset the test.</p>
                <p><i class="fa fa-plus-square primary-clr"></i>  Add Wagons Logo(.jpg & .png) whichever you need use it & STEP2AXISWAY. (Find Attachment)</p>
                <div class="main-head2 secondary-clr">Test Instructions For Learners</div>
                <p><i class="fa fa-plus-square primary-clr"></i>All questions are compulsory. </p>
                <p><i class="fa fa-plus-square primary-clr"></i>The question paper consists of 50 questions. </p>
                <p><i class="fa fa-plus-square primary-clr"></i> There is no negative marking. </p>
                <p><i class="fa fa-plus-square primary-clr"></i> The weightage for each question is mentioned next to each question.</p>
                <p><i class="fa fa-plus-square primary-clr"></i> Use of calculator is not allowed</p>
                <p><i class="fa fa-plus-square primary-clr"></i> Maximum Marks: (Vini Will Provide)</p>
            </div>
            <div class="col-md-12">
                <a href="#" class="btn btn-warning test-start">BEGIN TEST</a>
            </div>
        </div>

        <?php include 'layout/footer.php'; ?>

    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/timer/jquery.countdownTimer.js"></script> 
    <script>
        $(document).ready(function(){
            $('.test-start').click(function(e){
                e.preventDefault();
                var win_width = $(window).width();
                window.open('<?php echo base_url(); ?>home/exam_page','Step 2 Axis Way','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width='+win_width+',height=800');
            });
        });
    </script>
    <script>
        $(function () {
            $('#current_timer').countdowntimer({
                currentTime: true,
                size: "sm"
            });
        });
    </script>
</body>
</html>