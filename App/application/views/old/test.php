<!doctype html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Step 2 Axis Way | Test Begins</title>
    <meta name="description" content="">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/main.css">
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/timer/jquery.countdownTimer.css">
    <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico" type="image/x-icon" property="shortcut icon">
    <style>
        .page_spin{
            position: fixed;
            top:0;
            left:0;
            //background: rgba(0,0,0,0.1);
            width: 100%;
            height: 100%;
            display: none;
            z-index: 9999999;
        }
        .spin_icon{
            width: 100px;
            height: 100px;
            padding: 10px;
            background: rgba(0,0,0,0.1);
            text-align: center;
            border-radius: 50%;
            margin-top: 10%;
            margin-left: 47%;
        }
        .spin_icon .fa{
            margin-top: 15px;
            font-size: 30px;
        }
        .spin_icon span{
            color:#999;
            font-size: 11px;
        }
    </style>
</head>
<body>

    <div class="container">

        <?php include 'layout/header.php'; ?>

        <div class="row bread primary-bg">
            <div class="col-md-12 user">
                <div class="username"><i class="fa fa-user"></i> Hello <span class="white-clr"><?php echo $this->session->userdata('user_name'); ?></span></div>
                <span id="current_timer"></span>

            </div>

        </div>
        <div class="row pt-15">
            <div class="col-md-12">
                <div class="breadcrumb">
                    <ul class="bread">
                        <li><a href="#">Register</a></li> /
                        <li><a href="#"> Read Test Instructions </a></li> /
                        <li class="primary-clr"><strong>Take Test</strong></li>
                    </ul>

                </div>
            </div>
        </div>

        <?php
        if ($this->session->userdata('finish') == '0') {
            ?>

            <div class="row" style="background: #f7f7f7;padding-top: 15px;">
                <div class="col-md-12">
                    <div class="wealth">Wealth Management &amp; Personal Effectiveness Program - Pre Training Assessment Test </div>
                </div>
            </div>

            <div class="row text-center" style="background: #f7f7f7; padding-bottom: 20px;">
                <div class="col-md-12">
                    <div class="main-head3 primary-clr">Welcome to the Pre Training Assessment Test  for STEP 2 Axis Way</div>
                    <p class="note"><i class="fa fa-warning secondary-clr"></i> <strong>Note:</strong> Do not refresh page or press back doing so will wipe all answered questions and data. </p>

                </div>
            </div>
            <div class="row" style="background: #dcdcdc;">
                <div class="col-md-12 text-center">
                    <span class="timer">Time Left : </span> <span id="more_options" style="color:#f79226; font-size: 25px;"></span>
                </div>
            </div>
            <?php
            $start_time = time();
            if ($this->session->userdata('exam_time')) {
                $ex_time = $start_time - $this->session->userdata('exam_time');
            } else {
                $this->session->set_userdata('exam_time', $start_time);
                $ex_time = 0;
            }
            $r_time = (50 * 60) - $ex_time;
            if (!empty($questions)) {
                $no = 0;
                $cQuestionNo = 1;
                $atd_array = array();
                if (!empty($attd)) {
                    $no = count($attd);
                    $cQuestionNo = count($attd) + 1;
                    $ex_time = $attd[0]->time_laps;
                    $newTime = $start_time - $ex_time;
                    $this->session->set_userdata('exam_time', $newTime);
                    $r_time = (50 * 60) - $ex_time;

                    foreach ($attd as $at_data) {
                        array_push($atd_array, $at_data->question_id);
                    }
                }
                $total = count($questions);

                foreach ($questions as $qn_data) {
                    if (!in_array($qn_data['qid'], $atd_array)) {
                        $no++;
                        ?>
                        <div class="row qn_wrap qn_wrap_<?php echo $no; ?>" style="display: none; padding-top: 20px;padding-bottom: 20px;background: #fdfdfd;">
                            <div class="col-md-10 col-md-offset-1">
                                <div class="questions">
                                    <div class="q-text"> Question <span class="primary-clr">  <strong><?php echo $no; ?>/<?php echo count($questions); ?></strong></span></div>

                                    <div class="marks">Marks : <span class="primary-clr"> <strong><?php echo $qn_data['score']; ?> Marks</strong></span></div>

                                </div>
                                <div class="col-md-12 attempt">
                                    <div class="que">
                                        <?php echo $no; ?>. <?php echo $qn_data['title']; ?>;
                                    </div>
                                    <?php
                                    $data = array('a', 'b', 'c', 'd');
                                    $no1 = 0;
                                    $status = "";
                                    foreach ($qn_data['ans'] as $an_data) {
                                        if ($an_data->an_status == '1') {
                                            $status = $an_data->an_id;
                                        }
                                        ?>
                                        <div class="radio radio-primary">
                                            <input type="hidden" name="title" id="ansTitle<?php echo $an_data->an_id; ?>" value="<?php echo $an_data->an_title; ?>"/>
                                            <input type="radio" class="radio-btn" name="ans_option<?php echo $qn_data['qid']; ?>" qid="<?php echo $qn_data['qid']; ?>" id="radio<?php echo $an_data->an_id; ?>" value="<?php echo $an_data->an_id; ?>">
                                            <label for="radio<?php echo $an_data->an_id; ?>">
                                                <?php echo $data[$no1]; ?>. <?php echo $an_data->an_title; ?>  
                                            </label>
                                        </div>
                                        <?php
                                        $no1++;
                                    }
                                    ?>
                                    <input type="hidden" name="status" id="status<?php echo $qn_data['qid']; ?>" value="<?php echo $status; ?>"/>
                                    <input type="hidden" name="score" id="score<?php echo $qn_data['qid']; ?>" value="<?php echo $qn_data['score']; ?>"/>
                                    <div class="ptb-15" style="text-align:left">
                                        <?php
                                        if ($no != $cQuestionNo) {
                                            ?>
                                            <button type="submit" class="btn btn-warning prev-btn" qid="<?php echo $qn_data['qid']; ?>" qn_no="<?php echo $no; ?>"><i class="fa fa-arrow-left"></i> Previous Question</button>
                                            <?php
                                        }
                                        if ($no == $total) {
                                            ?>
                                            <button type="submit" class="btn btn-success next-btn" qid="<?php echo $qn_data['qid']; ?>" qn_no="<?php echo $no; ?>" style="margin-left:20px;">Submit Test</button>
                                            <?php
                                        } else {
                                            ?>
                                            <button type="submit" class="btn btn-info next-btn" qid="<?php echo $qn_data['qid']; ?>" qn_no="<?php echo $no; ?>" style="margin-left:20px;">Next Question <i class="fa fa-arrow-right"></i></button>
                                            <?php
                                        }
                                        ?>
                                        <span class="error_wrap error_<?php echo $no; ?>" style="margin-left: 30px; display: none; color:#de0c20;">Answer to this question</span>
                                    </div> 
                                </div>

                            </div>
                        </div>
                <?php
            }
        }
    }
} else {
    ?>
            <div class="alert alert-info">
                Assessment test already taken by you. You need not retry
            </div>
    <?php
}
?>



<?php include 'layout/footer.php'; ?>

    </div>

    <div class="page_spin">
        <br/>
        <div class="spin_icon">
            <i class="fa fa-spinner fa-spin"></i><br/>
            <span>One moment ...</span>
        </div>
    </div>


    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script> 
    <script src="<?php echo base_url(); ?>assets/js/timer/jquery.countdownTimer.js"></script>
    <script type="text/javascript">

        //window.open('http://localhost/Exam/','liveMatches','directories=no,titlebar=no,toolbar=no,location=no,status=no,menubar=no,scrollbars=no,resizable=no,width=720,height=800');
        $(document).ready(function () {

//            $("body").on("contextmenu", function (e) {
//                return false;
//            });
//            document.onkeydown = function (e) {
//                return false;
//            }
            var cQuestion = '0';
            $('.radio-btn').change(function () {
                cQuestion = $(this).attr('qid');
            });

            function complete_exam()
            {
                $('.page_spin').show();
                var qid = cQuestion;
                var ans = $("input[name='ans_option" + qid + "']:checked").val();
                var status = $('#status' + qid + '').val();
                var answered = $('#ansTitle' + ans + '').val();
                var score = $('#score' + qid + '').val();
                var dataString = "qid=" + qid + "&status=" + status + "&ans=" + answered + "&score=" + score + "&ref=" + ans + "&page=complete_status";
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>home/ajax_page",
                    data: dataString,
                    success: function (data) {
                        $('.page_spin').hide();
                        var replay = $.trim(data);
                        window.location = "<?php echo base_url(); ?>home/exam_complete/" + replay;
                    }, //success fun end
                }); //ajax end
            }


            var countdown = <?php echo $r_time; ?> * 1000;
            var timerId = setInterval(function () {
                countdown -= 1000;
                var min = Math.floor(countdown / (60 * 1000));
                var sec = Math.floor((countdown - (min * 60 * 1000)) / 1000); //correct
                if (countdown <= 0) {
                    $('.time_out').hide();
                    $('.go_back').show();
                    clearInterval(timerId);
                    complete_exam();
                } else {
                    $("#more_options").html(min + " : " + sec);
                }

            }, 1000);

            var total = '<?php echo $total; ?>';
            var start_point = '<?php echo $cQuestionNo; ?>';
            $('.qn_wrap_' + start_point + '').show();

            $('.prev-btn').click(function () {
                var cq = $(this).attr('qn_no');
                var qid = $(this).attr('qid');
                $('.qn_wrap').hide();
                cq = parseInt(cq) - 1;
                $('.qn_wrap_' + cq + '').show();
                if(cQuestion!='0')
                {
                    var ans = $("input[name='ans_option" + qid + "']:checked").val();
                    $('.page_spin').show();
                    var status = $('#status' + qid + '').val();
                    var answered = $('#ansTitle' + ans + '').val();
                    var score = $('#score' + qid + '').val();
                    var dataString = "qid=" + qid + "&status=" + status + "&ans=" + answered + "&score=" + score + "&ref=" + ans + "&page=save_answer";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>home/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            cQuestion = '0';
                        }, //success fun end
                    }); //ajax end
                }
            });

            $('.next-btn').click(function () {
                var cq = $(this).attr('qn_no');
                var qid = $(this).attr('qid');
                var ans = $("input[name='ans_option" + qid + "']:checked").val();
                if (ans == null)
                {
                    $('.error_' + cq + '').show();
                    return false;
                }
                if (cq == total)
                {
                    complete_exam();
                    return false;
                }
                $('.page_spin').show();
                var status = $('#status' + qid + '').val();
                var answered = $('#ansTitle' + ans + '').val();
                var score = $('#score' + qid + '').val();
                var dataString = "qid=" + qid + "&status=" + status + "&ans=" + answered + "&score=" + score + "&ref=" + ans + "&page=save_answer";
                $.ajax({
                    type: "POST",
                    url: "<?php echo base_url(); ?>home/ajax_page",
                    data: dataString,
                    success: function (data) {
                        $('.page_spin').hide();
                        $('.qn_wrap').hide();
                        cq = parseInt(cq) + 1;
                        $('.qn_wrap_' + cq + '').show();
                        cQuestion = '0';
                    }, //success fun end
                }); //ajax end


            });
        });
//        $(function () {
//            $('#more_options').countdowntimer({
//                minutes: 50,
//                size: "lg",
//                tickInterval: 1,
//                timeSeparator: ":"
//            });
//        });
    </script>
    <script>
        $(function () {
            $('#current_timer').countdowntimer({
                currentTime: true,
                size: "sm"
            });
        });
    </script>
</body>
</body>
</html>