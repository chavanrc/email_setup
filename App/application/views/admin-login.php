<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Login | 360&deg; Feedback Tool</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web-orange.css">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> 
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>

    <body style="background:url(<?php echo base_url(); ?>assets/images/bg2.jpg);">
        <div class="login">
            <div class="container">
                <div class="row">
                    <div class="logo-wag">
                        <img src="<?php echo base_url(); ?>assets/images/log-logo.png" class="img-responsive" alt="Wagons Learning" style="margin:0 auto;">

                    </div>
                    <div class="login-box">
                        <div class="head text-center">Admin Login</div>
                        <form action="" method="post" name="Login" id="Login" class="log">
                            <div id="lgnErorMsg">
                                <?php
                                if ($msg == 0) {
                                    ?>
                                    <div class='alert alert-warning'>Invalid Username and Password!</div>
                                <?php } ?>
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-md"> <span class="input-group-addon" style="background-color: #0d71ab;color:#FFF;"><i class="fa fa-user"></i></span>
                                    <input type="text" class="form-control " id="email" name="email" placeholder="Email Id *"  required>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group input-group-md"> <span class="input-group-addon" style="background-color: #0d71ab;color:#FFF;"><i class="fa fa-phone"></i></span>
                                    <input type="password" class="form-control " id="password" name="password" placeholder="Password *"  required>
                                </div>
                            </div>

                            <div class="form-group ">
                                <button type="submit" style="width:100%" class="btn btn-success hvr-curl-top-right"> Login</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>


        <!-- ./wrapper -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/web.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery-validate.bootstrap-tooltip.js"></script>

        <script>
            $(document).ready(function () {
                function LoginForm() {
                    var email = $("#email").val();
                    var password = $("#password").val();


                    var dataString = "email=" + email + "&password=" + password;
                    $.ajax({
                        type: "POST",
                        url: "admin-login.php",
                        data: dataString,
                        success: function (result) {
                            if (result == "y")
                            {
                                //Moved to dashboard.....
                            }
                            else
                            {
                                $("#lgnErorMsg").html("<div class='alert alert-warning'>Invalid Username and Password!</div>");
                                $("#Login").find("input,textarea,select").val('')
                            }
                        }
                    });
                }
                $("#Login").validate({
                    rules: {
                        password: "required",
                        email: {
                            required: true,
                            email: true
                        },
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    messages: {
                        password: "Please enter password",
                        email: {
                            required: "Please enter email id"

                        },
                    }
                });
            });

        </script>
    </body>
</html>