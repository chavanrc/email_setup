<aside class="main-sidebar">

    
    <section class="sidebar">

      <!-- Sidebar Menu -->
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header"><?php echo $_SESSION['admin']; ?></li>
        <!-- Optionally, you can add icons to the links -->
        <!--<li><a href="javascript:void(0);"><i class="fa fa-id-card-o"></i><span> 01234567890123456</span></a></li>-->
         <li><a href="<?php echo base_url(); ?>admin/employee_list"><i class="fa fa-bookmark-o"></i><span>Employee List</span></a></li>
        <li><a href="<?php echo base_url(); ?>admin/logout"><i class="fa fa-sign-out"></i><span> Logout </span></a></li>
        
      </ul>
      <!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
  </aside>