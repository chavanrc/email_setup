<!-- Main Footer -->
<footer class="main-footer">
  <!-- To the right -->
  
  <div class="pull-right hidden-xs"> Developed &amp; Maintaned By: <a href="http://www.wagonslearning.com/" rel="nofollow" target="_blank" class="clr">Wagons Learning</a> </div>
  <!-- Default to the left -->
  <strong>© <span id="copyright">
  <script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>
  </span> <span class="primary-clr">360&deg; Feedback Tool</span></strong> . All Rights Reserved.
  </footer>
  <div class="control-sidebar-bg"></div>
