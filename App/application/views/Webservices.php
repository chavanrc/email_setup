<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Webservices extends CI_Controller {

    public $cdate = null;
    public $full_date = null;

    function __construct() {
        parent::__construct();
        $this->output->set_header('Access-Control-Allow-Origin: *');
        $datestring = "%Y-%m-%d";
        $datestring1 = "%Y-%m-%d %h:%i:%s";
        $time = time();
        $this->cdate = mdate($datestring, $time);
        $this->full_date = mdate($datestring1, $time);
    }

    public function index() {
        $this->load->model('home_model');
        $this->load->view('index');
    }

    public function page_map($pr, $sc = NULL) {
        $this->$pr($sc);
    }

    public function about() {
        $this->load->view('about');
    }

    public function trainer_login() {

        if (isset($_POST['mobile'])) {
            $data[] = array();
            $mobile = $_POST['mobile'];
            $pass = $_POST['password'];

            $this->db->where('contact_number', $mobile);
            $this->db->where('app_password', $pass);

            $query = $this->db->get('application_users');
            if ($query->num_rows()) {
                $result = $query->result();
                $tid = $result[0]->user_code;
                $name = $result[0]->name;
                $email = $result[0]->email;
                $usertype = $result[0]->user_type;
                $projectId = "";
                $projectName = "";
                $city = "";

                $today = date('Y-m-d');

                $query1 = $this->db->query("SELECT p.project_id, p.project_title, p.location_of_training FROM trainer_assigned t, training_projects p WHERE t.trainer_id='" . $tid . "' AND t.program_date='" . $today . "' AND p.project_id=t.project_id");


                $result1 = $query1->result();
                if (!empty($result1)) {
                    $projectId = $result1[0]->project_id;
                    $projectName = $result1[0]->project_title;
                    $city = $result1[0]->location_of_training;
                }

                $data[0] = array('status' => '1', 'trainerId' => $tid, 'trainerName' => $name, 'trainerEmail' => $email, 'projectId' => $projectId, 'projectName' => $projectName, 'city' => $city, 'usertype' => $usertype);
            } else {
                $data[0] = array('status' => '0');
            }

            echo json_encode($data);
        }
    }

    public function trainer_checkin() {
    
        if (isset($_POST['trainerId'])) {
            $tId = $_POST['trainerId'];
            $pId = $_POST['projectId'];
            $tName = $_POST['trainerName'];
            $inTime = $_POST['checkinTime'];
            $lat = $_POST['lat'];
            $lng = $_POST['lng'];

            $data = array(
                'trainer_checkin_time' => $inTime,
                'trainer_lat' => $lat,
                'trainer_long' => $lng,
                'trainer_checkin_status' => 1);

            $this->db->where('trainer_id', $tId);
            $this->db->where('project_id', $pId);
            $this->db->update('trainer_assigned', $data);

            echo 1;
        } else {
            echo 0;
        }
    }

    public function participants_login() {
        if (isset($_POST['mobile'])) {
            $mobile = $_POST['mobile'];
            $pass = $_POST['password'];
            $data[] = array();
            $this->db->where('phone_number', $mobile);
            $this->db->where('app_password', $pass);

            $query = $this->db->get('participants');
            if ($query->num_rows()) {
                $result = $query->result();
                $pId = $result[0]->project_id;
                $prId = $result[0]->p_id;

                $query1 = $this->db->query("SELECT p.project_id, p.project_title FROM  training_projects p WHERE  p.project_id='" . $pId . "'");
                $pName = "";
                if ($query1->num_rows()) {
                    $result1 = $query1->result();
                    $pName = $result1[0]->project_title;
                }

                $data[0] = array('status' => 1, 'participantId' => $prId, 'projectId' => $pId, 'projectName' => $pName);
            } else {
                $data[0] = array('status' => 0);
            }

            echo json_encode($data);
        }
    }

    public function update_Pdetails() {
        if (isset($_POST['participantId'])) {
            $data = array('attendance_flag' => '1',
                'name' => $_POST['name'],
                'organization' => $_POST['organization'],
                'location' => $_POST['location'],
                'vpa_address' => $_POST['address']);

            $this->db->where('p_id', $_POST['participantId']);
            $this->db->update('participants', $data);
            echo 1;
        } else {
            echo 0;
        }
    }

    public function preTest_triger() {
        if (isset($_POST['trainerId'])) {
            $tId = $_POST['trainerId'];
            $pId = $_POST['projectId'];

            $data = array('pre_test_trigger' => 1,
                'pre_test_trigger_time' => $this->full_date);

            $this->db->where('trainer_id', $tId);
            $this->db->where('project_id', $pId);
            $this->db->update('trainer_assigned', $data);
            echo 1;
        } else {
            echo 0;
        }
    }

    public function postTest_triger() {
        if (isset($_POST['trainerId'])) {
            $tId = $_POST['trainerId'];
            $pId = $_POST['projectId'];

            $data = array('post_test_trigger' => 1,
                'post_test_trigger_time' => $this->full_date);

            $this->db->where('trainer_id', $tId);
            $this->db->where('project_id', $pId);
            $this->db->update('trainer_assigned', $data);
            echo 1;
        } else {
            echo 0;
        }
    }

    public function preTest_questions() {
        $data[] = array();
        if (isset($_POST['projectId'])) {
            $pId = $_POST['projectId'];
            $check = $this->db->query("SELECT pre_test_trigger FROM trainer_assigned WHERE project_id='" . $pId . "' AND pre_test_trigger='1'");
            if ($check->num_rows()) {
                $query = $this->db->query("SELECT participants_type FROM training_projects WHERE project_id='" . $pId . "'");
                $result = $query->result();
                if (!empty($result)) {
                    $query2 = $this->db->query("SELECT * FROM assessment_questions WHERE participant_type='" . $result[0]->participants_type . "' AND assessment_type='pre'");
                    
                    $question = $query2->result();
                    if (!empty($question)) {
                        $no = 0;
                        foreach ($question as $qn_data) {
                            $data[$no] = array(
                                'questionId' => $qn_data->question_id,
                                'question' => $qn_data->question_text,
                                'option_1' => $qn_data->answer_1,
                                'option_2' => $qn_data->answer_2,
                                'option_3' => $qn_data->answer_3,
                                'option_4' => $qn_data->answer_4);
                            $no++;
                        }
                        $data[0] = array('status' => 1,'qBody'=>$data);
                    } else {
                        $data[0] = array('status' => 0);
                    }
                } else {
                    $data[0] = array('status' => 0);
                }
            } else {
                $data[0] = array('status' => 0);
            }
        } else {
            $data[0] = array('status' => 0);
        }

        echo json_encode($data);
    }

    public function postTest_questions() {
        $data[] = array();
        if (isset($_POST['projectId'])) {
            $pId = $_POST['projectId'];
            $check = $this->db->query("SELECT pre_test_trigger FROM trainer_assigned WHERE project_id='" . $pId . "' AND post_test_trigger='1'");
            if ($check->num_rows()) {
                $query = $this->db->query("SELECT participants_type FROM training_projects WHERE project_id='" . $pId . "'");
                $result = $query->result();
                if (!empty($result)) {
                    $query2 = $this->db->query("SELECT * FROM assessment_questions WHERE participant_type='" . $result[0]->participants_type . "' AND assessment_type='post'");
                    $question = $query2->result();
                    if (!empty($question)) {
                        $no = 0;
                        foreach ($question as $qn_data) {
                            $data1[$no] = array(
                                'questionId' => $qn_data->question_id,
                                'question' => $qn_data->question_text,
                                'option_1' => $qn_data->answer_1,
                                'option_2' => $qn_data->answer_2,
                                'option_3' => $qn_data->answer_3,
                                'option_4' => $qn_data->answer_4);
                            $no++;
                        }
                        $data[0] = array('status' => 1,'qBody'=>$data1);
                    } else {
                        $data[0] = array('status' => 0);
                    }
                } else {
                    $data[0] = array('status' => 0);
                }
            } else {
                $data[0] = array('status' => 0);
            }
        } else {
            $data[0] = array('status' => 0);
        }
        
        echo json_encode($data);
    }
    
    public function updateImages()
    {
        if (isset($_POST['projectId'])) {
            $pId = $_POST['projectId'];
            $tId = $_POST['trainerId'];
            $img = $_POST['trainigImg'];
            
            $data = array('project_id'=>$pId,
                'trainer_id'=>$tId,
                'ti_img'=>$img,
                'ti_date'=>$this->full_date);
            
            $this->db->insert('trainer_img', $data);
            echo 1;
        }
        else {
            echo 0;
        }
    }
    
    public function lastCheckin()
    {
        if(isset($_POST['trainerId']))
        {
            $data[] = array();
            $tId = $_POST['trainerId'];
            $pId = $_POST['projectId'];
            
            $query = $this->db->query("SELECT trainer_checkin_time,trainer_checkin_status FROM trainer_assigned WHERE project_id='".$pId."' AND trainer_id='".$tId."' AND trainer_checkin_status='1'");
            if($query->num_rows())
            {
                $result = $query->result();
                $data[0] = array('status'=>1,'checkIn'=>$result[0]->trainer_checkin_time);
            }
            else{
                $data[0] = array('status'=>0);
            }
            
        }
    }

}
