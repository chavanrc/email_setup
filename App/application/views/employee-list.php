<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard | 360&deg; Feedback Tool</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web-orange.css">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-green layout-boxed sidebar-mini" >
        <div class="wrapper">
            <?php include 'layout/header.php'; ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php include 'layout/admin-menu.php';
             $CI = & get_instance();
             $CI->load->model('home_model');
            ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Admin | Employee List </h1>

                    <ol class="breadcrumb">
                        <li><a href=""><i class="fa fa-dashboard"></i> Employee List</a></li>
                        <li class="active">Here</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="nav-tabs-custom">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th></th>
                                            <th>Code</th>
                                            <th>Name</th>
                                            <th>Email</th>
                                            <th>Designation</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php if(!empty($emp)){
                                            $no = 0;
                                            foreach($emp as $em_data)
                                            {
                                                $no++;
                                                ?>
                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td><img class="img-circle img-bordered-sm" src="<?php echo base_url(); ?>assets/images/emp_pic/<?php echo $em_data->emp_photo; ?>" alt="user image" height="50px" width="50px"> </td>
                                            <td><?php echo $em_data->emp_code; ?></td>
                                            <td><?php echo $em_data->emp_name; ?></td>
                                            <td><?php echo $em_data->emp_email; ?></td>
                                            <td><?php echo $em_data->emp_designation; ?></td>
                                            <td>
                                                <span class="label label-primary" style="font-size: 15px;"><i class="fa fa-hand-o-up"></i>
                                                &nbsp; &nbsp; <?php echo $CI->home_model->review_count($em_data->emp_id); ?>
                                                </span>
                                            </td>
                                            <td style="width:100px;">
                                                <a href="<?php echo base_url(); ?>admin/result/<?php echo $em_data->emp_id; ?>" class="btn-sm btn-success"><i class="fa fa-eye"></i> Result</a>
                                            </td>
                                        </tr>
                                                <?php
                                            }
                                            
                                        } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
<?php include 'layout/footer.php'; ?>
        </div>
        <!-- ./wrapper -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/web.js"></script>
    </body>
</html>