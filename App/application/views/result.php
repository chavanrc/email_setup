<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard | 360&deg; Feedback Tool</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web-orange.css">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-green layout-boxed sidebar-mini" >
        <div class="wrapper">
            <?php include 'layout/header.php'; ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php
            include 'layout/admin-menu.php';
            $CI = & get_instance();
            $CI->load->model('home_model');
            ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Admin | Feedback Result </h1>

                    <ol class="breadcrumb">
                        <li><a href=""><i class="fa fa-dashboard"></i>Feedback Result</a></li>
                        <li class="active">Here</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="post white-bg" style="padding:15px 15px 1px;">
                                <div class="user-block"> <img class="img-circle img-bordered-sm" src="<?php echo base_url(); ?>assets/images/emp_pic/<?php echo $emp[0]->emp_photo; ?>" alt="user image"> <span class="username"> <a href="#"><?php echo $emp[0]->emp_name; ?></a>  </span> <span class="description"><?php echo $emp[0]->emp_designation; ?></span> </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="nav-tabs-custom">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th></th>
                                            <th>Question</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($question)) {
                                            $no = 0;
                                            foreach ($question as $qn_data) {
                                                $no++;
                                                ?>
                                        <tr >
                                                    <td><?php echo $no; ?></td>
                                                    <td><div style="font-size:18px;"><?php echo $qn_data->question_text; ?> </div> <br/>
                                                    <?php
                                                        $rv = $CI->home_model->qn_review_count($emp[0]->emp_id, $qn_data->question_id);
                                                        $rr = $CI->home_model->get_review_count($emp[0]->emp_id, $qn_data->question_id);
                                                        $sa = $ag = $ne = $da = $sd = 0;
                                                        if ($rr['Strongly Agree'] != 0) {
                                                            $sa = round(($rr['Strongly Agree'] / $rv) * 100);
                                                            ?>
                                                            <div class="bg-green text-center" style="height: 20px; margin:0px; float:left; padding:0px;  width: <?php echo $sa; ?>%;"><?php echo $sa; ?>%</div>
                                                            <?php
                                                        }
                                                        if ($rr['Agree'] != 0) {
                                                            $ag = round(($rr['Agree'] / $rv) * 100);
                                                            ?>
                                                            <div class="bg-blue text-center" style="height: 20px; margin:0px; float:left; padding:0px;  width: <?php echo $ag; ?>%;"><?php echo $ag; ?>%</div>
                                                            <?php
                                                        }
                                                        if ($rr['Neutral'] != 0) {
                                                            $ne = round(($rr['Neutral'] / $rv) * 100);
                                                            ?>
                                                            <div class="bg-purple text-center" style="height: 20px; margin:0px; float:left; padding:0px;  width: <?php echo $ne; ?>%;"><?php echo $ne; ?>%</div>
                                                            <?php
                                                        }
                                                        if ($rr['Disagree'] != 0) {
                                                            $da = round(($rr['Disagree'] / $rv) * 100);
                                                            ?>
                                                            <div class="bg-yellow text-center" style="height: 20px; float:left; margin:0px; padding:0px;  width: <?php echo $da; ?>%;"><?php echo $da; ?>%</div>
                                                            <?php
                                                        }
                                                        if ($rr['Strongly Disagree'] != 0) {
                                                            $sd = round(($rr['Strongly Disagree'] / $rv) * 100);
                                                            ?>
                                                            <div class="bg-red text-center" style="height: 20px; float:left; margin:0px; padding:0px; width: <?php echo $sd; ?>%;"><?php echo $sd; ?>%</div>
                                                            <?php
                                                        }
                                                        ?>
                                                            <div class="clearfix"></div>
                                                            <div>
                                                                <br/>
                                                                <span class="bg-green" style="height: 13px; width: 15px; display: inline-block;"></span> Strongly Agree (<i class="fa fa-user"></i> <?php echo $rr['Strongly Agree']; ?>)
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="bg-blue" style="height: 13px; width: 15px; display: inline-block;"></span> Agree  (<i class="fa fa-user"></i> <?php echo $rr['Agree']; ?>)
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="bg-purple" style="height: 13px; width: 15px; display: inline-block;"></span> Neutral (<i class="fa fa-user"></i> <?php echo $rr['Neutral']; ?>)
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="bg-yellow" style="height: 13px; width: 15px; display: inline-block;"></span> Disagree  (<i class="fa fa-user"></i> <?php echo $rr['Disagree']; ?>)
                                                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="bg-red" style="height: 13px; width: 15px; display: inline-block;"></span> Strongly Disagree (<i class="fa fa-user"></i> <?php echo $rr['Strongly Disagree']; ?>)
                                                            </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <?php include 'layout/footer.php'; ?>
        </div>
        <!-- ./wrapper -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/web.js"></script>
    </body>
</html>