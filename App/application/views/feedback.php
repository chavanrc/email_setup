<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Feedback | 360&deg; Feedback Tool</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web-orange.css">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <style>
            .page_spin{
                position: fixed;
                top:0;
                left:0;
                //background: rgba(0,0,0,0.1);
                width: 100%;
                height: 100%;
                display: none;
                z-index: 9999999;
            }
            .spin_icon{
                width: 100px;
                height: 100px;
                padding: 10px;
                background: rgba(0,0,0,0.1);
                text-align: center;
                border-radius: 50%;
                margin-top: 10%;
                margin-left: 47%;
            }
            .spin_icon .fa{
                margin-top: 15px;
                font-size: 30px;
            }
            .spin_icon span{
                color:#999;
                font-size: 11px;
            }
        </style>
    </head>
    <body class="hold-transition skin-green layout-boxed sidebar-mini">
        <div class="wrapper">
            <?php include 'layout/header.php'; ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php include 'layout/menu.php'; ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1> Feedback </h1>

                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>home/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
                        <li class="active">Feedback</li>
                    </ol>
                </section>
                <!-- Main content -->
                <section class="content container-fluid">
                    <div class="row">
                        <?php
                        if ($questions == 0) {
                            ?>
                            <div class="alert alert-danger">
                                <strong>Oops !! </strong> Something went wrong.
                            </div>
                            <?php
                        } else {
                            ?>

                            <div class="col-md-12">
                                <div class="post white-bg" style="padding:15px 15px 1px;">
                                    <div class="user-block"> <img class="img-circle img-bordered-sm" src="<?php echo base_url(); ?>assets/images/emp_pic/<?php echo $emp[0]->emp_photo; ?>" alt="user image"> <span class="username"> <a href="#"><?php echo $emp[0]->emp_name; ?></a>  </span> <span class="description"><?php echo $emp[0]->emp_designation; ?></span> </div>
                                </div>
                            </div>
                            <?php
                            if ($questions == 2) {
                                ?>
                                <div class="alert alert-danger">
                                    Your feedback is already taken for this employee.
                                </div>
                                <?php
                            } else {
                                ?>
                                <div class="col-md-12">
                                    <div class="box">

                                        <div class="info-details">


                                            <?php
                                            if (!empty($questions)) {

                                                $atd_array = array();
                                                $att_count = 0;
                                                if (!empty($attd)) {
                                                    $att_count = count($attd);
                                                    foreach ($attd as $at_data) {
                                                        array_push($atd_array, $at_data->question_id);
                                                    }
                                                }

                                                $no = 0;
                                                $total_qn = count($questions) - $att_count;
                                                ?>
                                                <p class="contact-icon text-center" aria-hidden="true" style="margin: 0 auto;"><span class="count-down">1</span>/<?php echo $total_qn; ?></p>
                                                <h3>Questions</h3>
                                                <?php
                                                foreach ($questions as $qn_data) {
                                                    if (!in_array($qn_data->question_id, $atd_array)) {
                                                        $no++;
                                                        ?>
                                                        <div class="qn_wrap_<?php echo $no; ?>" style="display:none;">
                                                            <h4><span class="clr"><?php echo $no; ?>.</span> 
                                                                <?php echo $qn_data->question_text; ?>
                                                            </h4>
                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" class="radio-btn" name="ans_option<?php echo $qn_data->question_id; ?>"  value="Strongly Agree">
                                                                     Strongly Agree <!--<i class="fa fa-thumbs-up"> </i> <i class="fa fa-thumbs-up"></i>-->
                                                                </label>
                                                            </div>

                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" class="radio-btn" name="ans_option<?php echo $qn_data->question_id; ?>"  value="Agree">
                                                                    Agree <!--<i class="fa fa-thumbs-up"> </i>-->
                                                                </label>
                                                            </div>

                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" class="radio-btn" name="ans_option<?php echo $qn_data->question_id; ?>"  value="Neutral">
                                                                    Neutral <!--<i class="fa fa-meh-o"> </i>-->
                                                                </label>
                                                            </div>

                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" class="radio-btn" name="ans_option<?php echo $qn_data->question_id; ?>"  value="Disagree" >
                                                                    Disagree <!--<i class="fa fa-thumbs-down"> </i> -->
                                                                </label>
                                                            </div>

                                                            <div class="radio">
                                                                <label>
                                                                    <input type="radio" class="radio-btn" name="ans_option<?php echo $qn_data->question_id; ?>"  value="Strongly Disagree">
                                                                    Strongly Disagree <!--<i class="fa fa-thumbs-down"></i> <i class="fa fa-thumbs-down"></i>-->
                                                                </label>
                                                            </div>
                                                            <span class="error_wrap error_<?php echo $no; ?>" style="margin-left: 30px; display: none; color:#de0c20;">Answer to this question</span>
                                                            <br>
                                                            <?php
                                                            if ($no > 1) {
                                                                ?>
                                                                <a href="#" class="btn btn-md btn-danger prev-btn" qid="<?php echo $qn_data->question_id; ?>" qno="<?php echo $no; ?>"><i class="fa fa-arrow-circle-left"></i> Previous</a>
                                                                <?php
                                                            }
                                                            if ($no != $total_qn) {
                                                                ?>
                                                                <a href="#" class="btn btn-md btn-success next-btn" qid="<?php echo $qn_data->question_id; ?>" qno="<?php echo $no; ?>">Next &nbsp; <i class="fa fa-arrow-circle-right"></i></a>
                                                                <?php
                                                            } else {
                                                                ?>
                                                                <a href="#" class="btn btn-md btn-success finish-btn" qid="<?php echo $qn_data->question_id; ?>" qno="<?php echo $no; ?>">Finish</a>
                                                                <?php
                                                            }
                                                            ?>

                                                        </div>
                                                        <?php
                                                    }
                                                }
                                            }
                                            ?>


                                        </div><!-- end info-details  -->

                                    </div>

                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <?php include 'layout/footer.php'; ?>
        </div>

        <div class="page_spin">
            <br/>
            <div class="spin_icon">
                <i class="fa fa-spinner fa-spin"></i><br/>
                <span>One moment ...</span>
            </div>
        </div>
        <!-- ./wrapper -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/web.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.qn_wrap_1').show();

                var cQuestion = '0';
                $('.radio-btn').change(function () {
                    cQuestion = $(this).attr('qid');
                });

                $('.next-btn').click(function (e) {
                    e.preventDefault();
                    var qno = $(this).attr('qno');
                    var nxt = parseInt(qno) + 1;
                    var qid = $(this).attr('qid');
                    var ans = $("input[name='ans_option" + qid + "']:checked").val();
                    if (ans == null)
                    {
                        $('.error_' + qno + '').show();
                        return false;
                    }
                    if (cQuestion != '0') {
                        $('.page_spin').hide();
                        var eid = "<?php echo $emp[0]->emp_id; ?>";
                        var dataString = "qid=" + qid + "&ans=" + ans + "&eid=" + eid + "&page=save_answer";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>home/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                $('.count-down').html(nxt);
                                $('.qn_wrap_' + qno + '').hide();
                                $('.qn_wrap_' + nxt + '').show();
                                $('.error_' + qno + '').hide();
                                cQuestion = '0';
                            }, //success fun end
                        }); //ajax end
                    }
                    else {
                        $('.count-down').html(nxt);
                        $('.qn_wrap_' + qno + '').hide();
                        $('.qn_wrap_' + nxt + '').show();
                        $('.error_' + qno + '').hide();
                    }

                });

                $('.prev-btn').click(function (e) {
                    e.preventDefault();
                    var qno = $(this).attr('qno');
                    var prv = parseInt(qno) - 1;
                    var qid = $(this).attr('qid');
                    var ans = $("input[name='ans_option" + qid + "']:checked").val();
                    var eid = "<?php echo $emp[0]->emp_id; ?>";
                    if (cQuestion != '0') {
                        $('.page_spin').show();
                        var dataString = "qid=" + qid + "&ans=" + ans + "&eid=" + eid + "&page=save_answer";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>home/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                $('.count-down').html(prv);
                                $('.qn_wrap_' + qno + '').hide();
                                $('.qn_wrap_' + prv + '').show();
                                cQuestion = '0';
                            }, //success fun end
                        }); //ajax end
                    }
                    else {
                        $('.count-down').html(prv);
                        $('.qn_wrap_' + qno + '').hide();
                        $('.qn_wrap_' + prv + '').show();
                    }

                });
                
                $('.finish-btn').click(function (e) {
                    e.preventDefault();
                    var qno = $(this).attr('qno');
                    var nxt = parseInt(qno) + 1;
                    var qid = $(this).attr('qid');
                    var ans = $("input[name='ans_option" + qid + "']:checked").val();
                    if (ans == null)
                    {
                        $('.error_' + qno + '').show();
                        return false;
                    }
                    if (cQuestion != '0') {
                        $('.page_spin').hide();
                        var eid = "<?php echo $emp[0]->emp_id; ?>";
                        var mid = "<?php echo $emp[0]->mapping_id; ?>";
                        var dataString = "qid=" + qid + "&ans=" + ans + "&eid=" + eid + "&mid="+mid+"&page=finish_exam";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>home/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.href = "<?php echo base_url(); ?>home/exam_complete/<?php echo $emp[0]->emp_id; ?>";
                            }, //success fun end
                        }); //ajax end
                    }

                });

            });
        </script>
    </body>
</html>