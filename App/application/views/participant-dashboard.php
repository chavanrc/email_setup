<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard | 360&deg; Feedback Tool</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web-orange.css">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-green layout-boxed sidebar-mini" style="background: #ecf0f5;">
        <div class="wrapper">
            <?php //include 'layout/header.php'; ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php //include 'layout/menu.php'; ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="margin-left:0px;">
                    <div style="background: #fff; padding: 15px; text-align: center; box-shadow: 3px 0px 3px #9e9e9e; border: solid 1px #ddd;">
                        <img src="<?php echo base_url(); ?>assets/images/logo2.png" style="height: 30px;" />
                    </div>
               
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Dashboard
                        <span class="pull-right" style="font-size: 13px;">
                            <?php echo date('M d, Y'); ?>
                        </span>
                    </h1>
                </section>
                <!-- Main content -->
                <section class="content container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="background: #fff; padding: 15px; box-shadow: 0px 0px 3px #ddd; border: solid 1px #ddd; border-radius: 3px;">
                                <div style="font-size: 18px; margin-bottom: 10px;">
                                    <?php echo $score[0]->project_title; ?>
                                </div>
                                <div style="font-size: 20px;">
                                    <?php echo $this->session->userdata('pName'); ?>
                                </div>
                                <div style="font-size: 15px; color: #9e9e9e;">
                                    <?php echo $this->session->userdata('pMobile'); ?>
                                </div>
                            </div>
                            
                            <a <?php if($score[0]->pre_test_trigger=='1' && empty($preAttd)){ ?> href="<?php echo base_url(); ?>participant/pre_test/<?php echo $score[0]->project_id; ?>" <?php } ?> style="background: #fff; box-shadow: 0px 0px 3px #9e9e9e; display: block;  padding: 15px; margin-top: 20px; border: solid 1px #429fff; border-radius: 3px;">
                                
                                <div style="font-size: 22px; color: #429fff;">
                                    Pre Assessment
                                </div>
                                <div style="font-size: 19px; color: #000;">
                                    Score : <?php if(empty($preAttd)){ ?>NA<?php } else { echo $score[0]->pre_score; } ?>
                                </div>
                            </a>
                            
                            <a <?php if($score[0]->post_test_trigger=='1' && empty($postAttd)){ ?> href="<?php echo base_url(); ?>participant/post_test/<?php echo $score[0]->project_id; ?>" <?php } ?> style="background: #fff; box-shadow: 0px 0px 3px #9e9e9e; display: block;  padding: 15px; margin-top: 20px; border: solid 1px #8fc312; border-radius: 3px;">
                                
                                <div style="font-size: 22px; color: #8fc312;">
                                    Post Assessment
                                </div>
                                <div style="font-size: 19px; color: #000;">
                                    Score : <?php if(empty($postAttd)){ ?>NA<?php } else { echo $score[0]->post_score; } ?>
                                </div>
                            </a>
                            
                            <a href="#" style="background: #fff; display: block; box-shadow: 0px 0px 3px #9e9e9e;  padding: 15px; margin-top: 20px; border: solid 1px #ff8b14; border-radius: 3px;">
                                <div style="font-size: 22px; text-align: center; color: #000;">
                                    Feedback
                                </div>
                            </a>
                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <?php //include 'layout/footer.php';  ?>
        </div>
        <!-- ./wrapper -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/web.js"></script>
        <?php include 'layout/script.php'; ?>
    </body>
</html>