<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Dashboard | 360&deg; Feedback Tool</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web-orange.css">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
          <![endif]-->
        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
    </head>
    <body class="hold-transition skin-green layout-boxed sidebar-mini" >
        <div class="wrapper">
            <?php //include 'layout/header.php'; ?>
            <!-- Left side column. contains the logo and sidebar -->
            <?php //include 'layout/menu.php'; ?>
            <!-- Content Wrapper. Contains page content -->
            <div class="content-wrapper" style="margin-left:0px;">
                <!-- Content Header (Page header) -->
                <section class="content-header">
                    <h1>Participants List
                        <span class="pull-right" style="font-size: 13px; margin-top: 10px;">
                            <?php echo date('M d, Y'); ?>
                        </span>
                    </h1>
                    <?php
                    $tP = 0;
                    $tA = 0;
                    if(!empty($part))
                    {
                        $tP = count($part);
                        if(!empty($attd))
                        {
                            $tA = $attd[0]->tPart;
                        }
                    }
                    ?>
                    <span style="margin-left: 5px;">Attendance : <?php echo $tA; ?>/<?php echo $tP; ?></span>
                </section>
                <!-- Main content -->
                <section class="content container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <?php
                            if (!empty($part)) {
                                foreach ($part as $pr_data) {
                                    ?>
                                    <div style="background: #fff; padding: 15px; border: solid 1px #ddd; margin-bottom: 15px; border-radius: 3px;">

                                        <div style="font-size: 18px;">
                                            <i class="fa fa-user"></i> &nbsp; <?php echo $pr_data->name; ?>
                                            <?php
                                            if($pr_data->attendance_flag=='1')
                                            {
                                            	?>
                                            	<i class="fa fa-check pull-right" style="font-size:20px; color:#45af19"></i>
                                          	<?php
                                            }
                                            else {
                                            ?>
                                            	<i class="fa fa-ban pull-right" style="font-size:20px; color:red"></i>
                                            <?php
                                            }
                                             ?>
                                        </div>
                                        <div style="font-size: 17px; color: #9e9e9e;">
                                            <i class="fa fa-phone"></i> &nbsp; <?php echo $pr_data->phone_number; ?>
                                        </div>
                                        <div class="row" style="margin-top: 15px;">
                                            <div class="col-xs-6">
                                                <div class="text-center" style="padding:5px; border:solid 1px #0a5784; border-radius: 3px;">
                                                    <span style="font-size: 17px;">Pre Test Score</span><br/>
                                                    <span style="font-size: 22px; color:#0a5784; "><?php echo $pr_data->pre_score; ?></span>
                                                </div>
                                            </div>
                                            <div class="col-xs-6">
                                                <div class="text-center" style="padding:5px; border:solid 1px #45af19; border-radius: 3px;">
                                                    <span style="font-size: 17px;">Post Test Score</span><br/>
                                                    <span style="font-size: 22px; color:#45af19;"><?php echo $pr_data->post_score; ?></span>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </div>

                                    </div>
                                    <?php
                                }
                            }
                            ?>

                        </div>
                    </div>
                </section>
                <!-- /.content -->
            </div>
            <!-- /.content-wrapper -->
            <?php //include 'layout/footer.php';  ?>
        </div>
        <!-- ./wrapper -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/web.js"></script>
        <?php include 'layout/script.php'; ?>
    </body>
</html>