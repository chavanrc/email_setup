<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Login | 360&deg; Feedback Tool</title>
        <!-- Tell the browser to be responsive to screen width -->
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/font-awesome/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/ionicons.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web.css">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/web-orange.css">
        <link rel="shortcut icon" href="<?php echo base_url(); ?>assets/images/favicon.ico">
        <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script> 
        <![endif]-->

        <!-- Google Font -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
        <style>
            .form-control{
                padding: 20px;
                font-size: 17px;
            }
            .btn{
                font-size: 19px;
            }
        </style>
    </head>

    <body style="background:#fff;">
        <div class="login" style="margin-top:70px;">
            <div class="container">
                <div class="row">
                    <div class="logo-wag">
                        <img src="<?php echo base_url(); ?>assets/images/logo2.png" class="img-responsive" alt="Wagons Learning" style="margin:0 auto;">

                    </div>
                    <div class="login-box" style="border-radius:5px;">
                        
                        <form action="" method="post" name="Login" id="Login" class="log">
                            <div id="lgnErorMsg">
                                <?php
                                if ($msg == 0) {
                                    ?>
                                    <div class='alert alert-warning'>Invalid Username and Password!</div>
                                <?php } 
                                if ($msg == 3) {
                                    ?>
                                    <div class='alert alert-warning'>No Training Program Shcheduled for Today</div>
                                <?php }
                                 ?>
                            </div>
                            <div class="form-group">
                                    <input type="text" class="form-control " id="email" name="email" placeholder="Mobile Number *"  required>
                               
                            </div>
                            <div class="form-group">
                                    <input type="password" class="form-control " id="password" name="password" placeholder="Password *"  required>
                              
                            </div>

                            <div class="form-group text-center">
                                <button type="submit" style="width:100%" class="btn btn-success hvr-curl-top-right"> Login</button>
                            </div>

                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="page_spin">
            <br/>
            <div class="spin_icon">
                <i class="fa fa-spinner fa-spin"></i><br/>
                <span>One moment ...</span>
            </div>
        </div>


        <!-- ./wrapper -->
        <script src="<?php echo base_url(); ?>assets/js/jquery.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/web.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery-validate.bootstrap-tooltip.js"></script>

        <script>
            $(document).ready(function () {
            
            	
            
                function LoginForm() {
                    var email = $("#email").val();
                    var password = $("#password").val();


                    var dataString = "email=" + email + "&password=" + password;
                    $.ajax({
                        type: "POST",
                        url: "admin-login.php",
                        data: dataString,
                        success: function (result) {
                            if (result == "y")
                            {
                                //Moved to dashboard.....
                            }
                            else
                            {
                                $("#lgnErorMsg").html("<div class='alert alert-warning'>Invalid Username and Password!</div>");
                                $("#Login").find("input,textarea,select").val('')
                            }
                        }
                    });
                }
                $("#Login").validate({
                    rules: {
                        password: "required",
                        email: {
                            required: true,
                            number: true
                        },
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    messages: {
                        password: "Please enter password",
                        email: {
                            required: "Please enter mobile number"
                        },
                    },
                    submitHandler: function(form) {
			    $('.page_spin').show();
			    form.submit();
			}
                });
            });

        </script>
    </body>
</html>