<!DOCTYPE html>
<html>
  <head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <title>Wagons Learning</title>
    <style>
      /* Always set the map height explicitly to define the size of the div
       * element that contains the map. */
      #map {
        height: 250px;
        width:300px;
      }
      /* Optional: Makes the sample page fill the window. */
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      .route_wrap{
          position: absolute;
          background: #fff;
          border-radius: 3px;
          border: solid 1px #ddd;
          width:94%;
          top:50px;
          margin-left: 3%;
          display: none;
      }
      .route_img{
          position: absolute;
          top:8px;
          left:45%;
          cursor: pointer;
      }
      .route_select{
          padding: 10px;
          border:none;
          border-right: solid 1px #ddd;
          font-size: 15px;
          width: 90%;
      }
      .close_icon{
          width: 17px;
          float: right;
          margin-top: 10px;
          margin-right: 10px;
          cursor: pointer;
      }
    </style>
  </head>
  <body>
      
      
         
    <div id="map">
    workin
    </div>
    
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.2/jquery.min.js"></script>
    <script type="text/javascript">
    
        var wH = screen.height;
        var wW = screen.width;
        var aH = wH-100;
        
        document.getElementById('map').style.width = wW+'px';
       // document.getElementById('map').style.height = aH+'px';
    
      function initMap() {
          <?php
          $dseLat = '';
          $dseLng = '';
          //if(!empty($loc))
          //{
              $dseLat = $_GET['lat'];
              $dseLng = $_GET['lng'];
          //}
          ?>
        var myLatLng = {lat: <?php echo $dseLat; ?>, lng: <?php echo $dseLng; ?>};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 15,
          center: myLatLng
        });
        
                
                var rLatLgn = {lat : <?php echo $dseLat; ?>, lng : <?php echo $dseLng; ?>};
                 var marker = new google.maps.Marker({
                    position: rLatLgn,
                    map: map
                  });
                
          var markerArray = [];
          var directionsService = new google.maps.DirectionsService;
          var directionsDisplay = new google.maps.DirectionsRenderer({map: map});
          var stepDisplay = new google.maps.InfoWindow;
          
            
            var onChangeHandler = function() {
                var dest = document.getElementById('route_select').value;
                if(dest!="")
                {
                calculateAndDisplayRoute(
                directionsDisplay, directionsService, markerArray, stepDisplay, map);
                }
            };
            //document.getElementById('route_select').addEventListener('change', onChangeHandler);
        
      }
      
      
      
      function calculateAndDisplayRoute(directionsDisplay, directionsService,
          markerArray, stepDisplay, map, dest) {
        // First, remove any existing markers from the map.
        for (var i = 0; i < markerArray.length; i++) {
          markerArray[i].setMap(null);
        }

        // Retrieve the start and end locations and create a DirectionsRequest using
        // WALKING directions.
        directionsService.route({
          origin: '<?php echo $dseLat; ?>, <?php echo $dseLng; ?>',
          destination: document.getElementById('route_select').value,
          travelMode: 'DRIVING'
        }, function(response, status) {
          if (status === 'OK') {
            directionsDisplay.setDirections(response);
          } else {
            window.alert('Directions request failed due to ' + status);
          }
        });
      }
      
     
      
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAb3SIOMI8cUfJrClS0FFSzfVkA00rAh44&callback=initMap">
    </script>
    
  </body>
</html>