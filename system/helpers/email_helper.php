<?php

defined('BASEPATH') OR exit('No direct script access allowed');
if (!function_exists('valid_email')) {

    function valid_email($email) {
        return (bool) filter_var($email, FILTER_VALIDATE_EMAIL);
    }

}
if (!function_exists('send_email')) {

    function new_program_trainer($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/new-program-trainer', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function mail_to_pm($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/pm-template', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }
    
    function mail_to_trainer($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/trainer-template', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }
    
    function testMail($email) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = "$vm->load->view('email_temp/trainer-template', $detail, TRUE)";

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function mail_to_admin($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/admin-template', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Email Template';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function mail_to_ts($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/ts-template', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function mail_to_as($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/as-template', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function send_feedback_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/send-feedback-mail', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function notification_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/notification-mail', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM | Notification';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function new_program_task($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/admin-template', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = $data['sub'];

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function edit_request_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/edit-request', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM | Invoice Edit Request';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function program_edit_request($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/program-edit-request', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM | Program Edit Request';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function edit_requestAccept_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/accept-edit-request', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM | Invoice Edit Request';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function forgot_password_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/forgot-password', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM | Password Reset Request';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function new_trainer_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['user'] = $data;
        $body = $vm->load->view('email_temp/new-trainer', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM | New Trainer Registration';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function new_user_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['user'] = $data;
        $body = $vm->load->view('email_temp/new-user', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM | New User Account Created';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function trainer_approval_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['user'] = $data;
        $body = $vm->load->view('email_temp/trainer-approval', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM | Wagons Trainer Account Approved';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function engagement_request_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/engagement-request', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM | New Engagement Request';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function request_status_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/request-status', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM | Training Program Engagement Success';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function project_mail_pm($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/project-mail-pm', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM | Training Program Update';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function project_mail_support($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/project-mail-support', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM | Training Program Update';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function trainer_invoice_paid($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/trainer_invoice_paid', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM | Training Program Update';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function project_mail_admin($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/project_mail_admin', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM | Trainer Invoice Approved';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function reject_status_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/reject-status', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM | Training Program Engagement Rejected';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function props_status_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/props-status', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Important Training Program Update!';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function form_attach_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/form-mail', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Training Program Forms Updated';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function bdm_review_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/bdm_review', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Trainer Engagement Request- Review Required';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function content_setting_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/new-program-content', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Content Setting Update!';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function handbook_required_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/handbook-required', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Participant Handbook Required';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function content_map_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/content-update', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Program content is mapped';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function content_complete_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/content-complete', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Program content status';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function content_password_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/content-password', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Program content status';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }
    
    function tds_certificate_mail($email, $data) {
        include('Mail.php');
        $vm = & get_instance();
        $detail['data'] = $data;
        $body = $vm->load->view('email_temp/tds-certificate', $detail, TRUE);

        $host = "a2plvcpnl337183.prod.iad2.secureserver.net";
        $username = "crm@wagonslearning.in";
        $password = "Crm@3210!";
        $port = "25";

        $from = "Wagons Learning <crm@wagonslearning.in>";
        $to = $email;
        $subject = 'Wagons CRM | TDS Certificate';

        $headers = array('From' => $from,
            'To' => $to,
            'Subject' => $subject,
            'Content-type' => "text/html; charset=iso-8859-1\r\n\r\n");
        $smtp = Mail::factory('smtp', array('host' => $host,
                    'auth' => true,
                    'username' => $username,
                    'password' => $password, 'port' => $port));

        $smtp->send($to, $headers, $body);
    }

    function send_email($to, $sub, $temp, $val, $type, $od_id = FALSE) {
        $sm = & get_instance();
        $sm->load->library('email');
        $sm->email->from('info@giftwithluv.com', 'Giftwithluv');
        $sm->email->reply_to('info@giftwithluv.com', 'Giftwithluv');
        $sm->email->to($to);
        $sm->email->set_mailtype("html");
        $sm->email->subject($sub);
        $vm = & get_instance();
        $data['pd'] = $val;
        $data['ff'] = $type;
        $data['saler'] = $to;
        $data['sub'] = $sub;
        $data['od_id'] = $od_id;
        $msg = $vm->load->view('email_temp/' . $temp, $data, TRUE);
        $sm->email->message($msg);
        $sm->email->send();
    }

}

function custome_encode($data) {
    $string = strrev($data);
    $result = '';
    $big = array('', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
    $small = array('', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        '~', '`', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '/', '-', '[', ']', '{', '}', ';', '"', ':', ',', '.', '/', '<', '>', '?', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');

    $script = substr(str_shuffle("abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz"), -100);

    for ($i = 0, $k = strlen($string); $i < $k; $i++) {
        $char = substr($string, $i, 1);
        $key = array_search($char, $small);
        $key1 = array_search($char, $big);
        if ($key) {
            $result .= substr($script, $key, 2) . $key;
        }
        if ($key1) {
            $result .= substr($script, $key1, 2) . $key1 . 'C';
        }
    }

    return $result . 'an';
}

function custome_decode($data) {

    $big = array('', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z');
    $small = array('', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        '~', '`', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '/', '-', '[', ']', '{', '}', ';', '"', ':', ',', '.', '/', '<', '>', '?', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9');


    $result2 = '';
    for ($i = 0, $k = strlen($data); $i < $k; $i++) {
        $char = substr($data, $i, 1);
        if (!is_numeric($char) && !ctype_upper($char)) {
            $insert = '-';
        } else {
            $insert = $char;
        }

        $result2 .=$insert;
    }
    $result3 = explode('-', $result2);
    $data2 = '';
    foreach ($result3 as $rs) {
        if (!empty($rs)) {
            if (is_numeric($rs)) {
                $data2 .= $small[$rs];
            } else {
                $rs_data = explode('C', $rs);
                $data2 .= $big[$rs_data[0]];
            }
        }
    }

    return strrev($data2);
}
