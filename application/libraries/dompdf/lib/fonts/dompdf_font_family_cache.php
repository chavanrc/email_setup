<?php return array (
  'sans-serif' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Helvetica',
    'bold' => DOMPDF_FONT_DIR . 'Helvetica-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Helvetica-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Helvetica-BoldOblique',
  ),
  'times' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'times-roman' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'courier' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'helvetica' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Helvetica',
    'bold' => DOMPDF_FONT_DIR . 'Helvetica-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Helvetica-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Helvetica-BoldOblique',
  ),
  'zapfdingbats' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'bold' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'italic' => DOMPDF_FONT_DIR . 'ZapfDingbats',
    'bold_italic' => DOMPDF_FONT_DIR . 'ZapfDingbats',
  ),
  'symbol' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Symbol',
    'bold' => DOMPDF_FONT_DIR . 'Symbol',
    'italic' => DOMPDF_FONT_DIR . 'Symbol',
    'bold_italic' => DOMPDF_FONT_DIR . 'Symbol',
  ),
  'serif' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Times-Roman',
    'bold' => DOMPDF_FONT_DIR . 'Times-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Times-Italic',
    'bold_italic' => DOMPDF_FONT_DIR . 'Times-BoldItalic',
  ),
  'monospace' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'fixed' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'Courier',
    'bold' => DOMPDF_FONT_DIR . 'Courier-Bold',
    'italic' => DOMPDF_FONT_DIR . 'Courier-Oblique',
    'bold_italic' => DOMPDF_FONT_DIR . 'Courier-BoldOblique',
  ),
  'dejavu sans' => 
  array (
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSans-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSans-BoldOblique',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSans-Oblique',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSans',
  ),
  'dejavu sans light' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSans-ExtraLight',
  ),
  'dejavu sans condensed' => 
  array (
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed-BoldOblique',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed-Oblique',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSansCondensed',
  ),
  'dejavu sans mono' => 
  array (
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSansMono-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSansMono-BoldOblique',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSansMono-Oblique',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSansMono',
  ),
  'dejavu serif' => 
  array (
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSerif-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSerif-BoldItalic',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSerif-Italic',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSerif',
  ),
  'dejavu serif condensed' => 
  array (
    'bold' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed-Bold',
    'bold_italic' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed-BoldItalic',
    'italic' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed-Italic',
    'normal' => DOMPDF_FONT_DIR . 'DejaVuSerifCondensed',
  ),
  'glyphicons halflings' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '37dd70a5186d88595e6cfd5d5657449f',
  ),
  'fontawesome' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '3db0af1ee22739bfb0478ddc83e8fbb1',
  ),
  'roboto' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '720228ebe98581cf3dcb9c52d14cb500',
  ),
  'max-thin' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '7465ff071c80db2ad42cd4d19f1b51da',
  ),
  'max-regular' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '96a2f08c7071d98979a53a1794e01659',
  ),
  'max-medium' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '24822655507eabae90edde753f89bc37',
  ),
  'max-bold' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'fa9509150bb01ea96109bf2275fe0266',
  ),
  'flaticon' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '062760c0bda9978c471edfe6d09dbfbc',
  ),
  'enquiry-text' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '9b5b531743a216bc62acc45bc1d38429',
  ),
  'open sans' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '07ffedf4983a20afacaf57bfba0ebf45',
  ),
  'ionicons' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'ccbced35d9219f2521ae93a3b5db89e4',
  ),
  'source sans pro' => 
  array (
    'italic' => DOMPDF_FONT_DIR . '539f543ec00a8811978f7965dc895ef7',
    'normal' => DOMPDF_FONT_DIR . '514379ccad74755f7dcf15cb110075fe',
  ),
  'trebuchet ms regular' => 
  array (
    'normal' => DOMPDF_FONT_DIR . 'a2ed64df91ae48c6f0d16895cb40a3f6',
  ),
  'trebuchet ms bold' => 
  array (
    'normal' => DOMPDF_FONT_DIR . '4ea8fb402ec78d821531f015833848c6',
  ),
) ?>