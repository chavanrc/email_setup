<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
        <style>
            td, th
            {
                font-size:12px;
            }
            .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
                border-top:solid 1px #000 !important;
                padding: 5px !important;
            }
            .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
                border: 1px solid #000 !important;
            }
            @media print {
                .in-print{
                    margin-top:100px;
                }
                .panel{
                    border:0px;
                }
                .panel-body{
                    padding:0px;
                }
                .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
                    padding: 5px;
                }
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12 no-print">
                    <div class="row">
                        <div class="col-md-4 page-title">
                            <h3 style="display:inline-block; margin-right: 20px;">Monthly Invoices</h3>
                            <?php
                            if ($invoice[0]->di_img != '') {
                                $cimg = explode(',', $invoice[0]->di_img);
                                foreach($cimg as $cimgdata){
                                ?>
                            <a href="<?php echo base_url(); ?>assets/upload/client/<?php echo $cimgdata; ?>" target="_blank" class="btn-sm btn-warning" style="margin-right:3px;"><i class="fa fa-file-image-o" aria-hidden="true"></i> Invoice Copy</a>
                                <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="col-md-8" style="padding-top: 10px;">
                            <a href="<?php echo base_url(); ?>account/drona_invoice<?php if(isset($_GET['page'])){ echo '?page='.$_GET['page']; } ?>" class="btn btn-danger pull-right" style="margin-left:10px;">Back</a>
                            <?php
                            if ($invoice[0]->di_status == 0) {
                                } else {
                                ?>
                                <a href="#" onclick="window.print();" class="btn btn-primary pull-right">Print / Download</a>
                            <?php }
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <?php
                    if (!empty($erequest)) {
                        ?>
                        <div class="no-print">
                            <strong>Invoice Revision</strong>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Request Send to</th>
                                        <th>Description</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Update Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($erequest as $er_data) {
                                        ?>
                                        <tr>
                                            <td><?php echo $er_data->er_email; ?></td>
                                            <td><?php echo $er_data->er_desc; ?></td>
                                            <td><?php echo $er_data->er_date; ?></td>
                                            <td>
                                                <?php
                                                if ($er_data->er_status == '0') {
                                                    echo 'Pending';
                                                }
                                                if ($er_data->er_status == '1') {
                                                    echo 'Approved';
                                                }
                                                if ($er_data->er_status == '2') {
                                                    echo 'Rejected';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($er_data->er_status != '0') {
                                                    echo date_formate($er_data->er_status_date);
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <?php
                    }
                    ?>

                </div>
                <?php
                $inAmt = 0;
                $debitAmt = 0;
                $inGST = 0;
                $debitGST = 0;
                $inTotal = 0;
                $debitTotal = 0;
                if ($invoice[0]->di_onlyDebit == 0) {
                    ?>
                    <div class="col-md-10 content-page" style="margin-top: 10px;">
                        <div class="panel panel-default in-print">
                            <div class="panel-body">
                                <div>
                                    <?php if (!empty($invoice[0]->di_type)) { ?>
                                        <table class="table table-bordered" style="margin-bottom: 0px;">
                                            <tr>
                                                <td style="text-align: center; font-weight: bold;">
                                                    <?php echo $cm[0]->company_name; ?><br/>
                                                    <?php echo $cm[0]->address; ?> <br/>
                                                    Maharashtra, India | Contact No : 8149006055
                                                </td>
                                                <td align="center">Kofax 2.0 <br/> NA</td>
                                            </tr>
                                        </table>
                                    <?php } ?>
                                    <table class="table table-bordered" style="margin-bottom: 0px;">
                                        <tr>
                                            <td colspan="4" style="text-align: center; text-transform: uppercase;"><strong>Tax Invoice For <?php
                                                    if (!empty($invoice[0]->di_type)) {
                                                        echo $invoice[0]->di_type;
                                                    } else {
                                                        echo $invoice[0]->di_project;
                                                    }
                                                    ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">To,</td>
                                            <td style="width:150px;">Invoice Number</td>
                                            <td><?php echo $invoice[0]->di_number; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" rowspan="<?php
                                            if (!empty($invoice[0]->di_type)) {
                                                echo '6';
                                            } else {
                                                echo '4';
                                            }
                                            ?>">
                                                    <?php echo $invoice[0]->di_address; ?>
                                            </td>
                                            <td>Invoice Date</td>
                                            <td><?php echo date_formate_short($invoice[0]->di_date); ?></td>
                                        </tr>

                                        <?php
                                        if (!empty($invoice[0]->di_type)) {
                                            ?>
                                            <tr>
                                                <td>Vendor Code</td>
                                                <td><?php echo $invoice[0]->di_vendor_code; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Location</td>
                                                <td><?php echo $invoice[0]->di_location; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Department</td>
                                                <td><?php echo $invoice[0]->di_department; ?></td>
                                            </tr>
                                            <tr>
                                                <td>TML Contact e-mail</td>
                                                <td><?php echo $invoice[0]->di_email; ?></td>
                                            </tr>
                                            <tr>
                                                <td>PO Number</td>
                                                <td><?php echo $invoice[0]->di_poNumber; ?></td>

                                            </tr>
                                            <?php
                                        } else {
                                            ?>
                                            <tr>
                                                <td>Ref. No.</td>
                                                <td>
                                                    <?php
                                                    if (!empty($invoice[0]->di_debitNumber)) {
                                                        echo $invoice[0]->di_debitNumber;
                                                    } else {
                                                        echo 'NA';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Name</td>
                                                <td><?php echo $invoice[0]->di_name; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><?php echo $invoice[0]->di_email; ?></td>
                                            </tr>    
                                            <?php
                                        }
                                        ?>
                                        <tr>
                                            <td>State Code</td>
                                            <td><?php echo $invoice[0]->di_state_code; ?></td>
                                            <?php if (!empty($invoice[0]->di_type)) { ?>
                                                <td>SES Number</td>
                                                <td><?php echo $invoice[0]->di_sesNumber; ?></td>
                                            <?php } else {
                                                ?>
                                                <td>PO Number</td>
                                                <td><?php echo $invoice[0]->di_poNumber; ?></td>
                                            <?php }
                                            ?>
                                        </tr>
                                        <tr>
                                            <td>GSTIN No</td>
                                            <td><?php echo $invoice[0]->di_gst; ?></td>
                                            <td>SAC Code</td>
                                            <td>999293</td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered" style="margin-bottom: 0px;">
                                        <tr>
                                            <td style="text-align: center;">Sl. No.</td>
                                            <td style="text-align: center;">Description</td>
                                            <td style="text-align: center;">Month</td>
                                            <td style="text-align: center;"><?php
                                                if (!empty($invoice[0]->di_type)) {
                                                    echo 'Type of Project';
                                                } else {
                                                    echo 'Location';
                                                }
                                                ?></td>
                                            <td style="text-align: center; width: 130px;">Amount</td>
                                            <?php
                                            if ($invoice[0]->di_status == 0) {
                                                ?>
                                                <td></td>
                                                <?php
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                        $total = 0;
                                        if (!empty($exp)) {
                                            $no = 0;
                                            $inCount = 0;
                                            foreach ($exp as $inData) {
                                                $inCount++;
                                            }
                                            foreach ($exp as $exData) {
                                                if ($exData->dd_status == 1) {
                                                    $no++;
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center;"><?php echo $no; ?></td>
                                                        <td style="text-align: center;"><?php echo $exData->dd_desc; ?></td>
                                                        <?php if ($no == 1) { ?>
                                                            <td valign="middle" style="text-align: center; vertical-align: middle;" rowspan="<?php echo $inCount; ?>">
                                                                <?php
                                                                if($invoice[0]->di_month!='NA'){
                                                                $dt = DateTime::createFromFormat('!m', $invoice[0]->di_month);
                                                                echo $dt->format('F') . '-' . $invoice[0]->di_year;
                                                                } else {
                                                                    echo "NA";
                                                                }
                                                                ?>
                                                            </td>
                                                            <td valign="middle" style="text-align: center; vertical-align: middle;" rowspan="<?php echo $inCount; ?>">
                                                                <?php
                                                                if (!empty($invoice[0]->di_type)) {
                                                                    echo $invoice[0]->di_type;
                                                                } else {
                                                                    echo $invoice[0]->di_location;
                                                                }
                                                                ?>
                                                            </td>
                                                        <?php } ?>
                                                        <td  style="text-align: right;"><?php echo number_format($exData->dd_amount, 2); ?></td>
                                                        <?php
                                                        if ($invoice[0]->di_status == 0) {
                                                            ?>
                                                            <td>
                                                                <a href="#" class="remove-exp" eid="<?php echo $exData->dd_id; ?>">
                                                                    <i class="fa fa-remove"></i>
                                                                </a>
                                                            </td>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tr>
                                                    <?php
                                                    $total +=$exData->dd_amount;
                                                }
                                            }
                                        }
                                        ?>
                                    </table>
                                    <?php
                                    $cmState = $cm[0]->gst_state_code;
                                    $cState = $invoice[0]->di_state_code;
                                    $cgst = 0;
                                    $sgst = 0;
                                    $gTotal = 0;
                                    ?>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td rowspan="2" align="center">Rupees In Words</td>
                                            <td rowspan="2" align="center">
                                                <?php
                                                if ($cState != $cmState) {
                                                    $gstamt = round((18 / 100) * $total,2);
                                                } else {
                                                    $cgst = round((9 / 100) * $total,2);
                                                    $sgst = round((9 / 100) * $total,2);
                                                    $gstamt = $cgst + $sgst;
                                                }
                                                $gTotal = $total + $gstamt;
                                                $inAmt = $total;
                                                $inGST = $gstamt;
                                                $inTotal = round($gTotal);
                                                echo 'Rupees '.getIndianCurrency(round($gTotal)).' Only';
                                                ?>
                                            </td>
                                            <td style="font-weight: 600;">Gross Total</td>
                                            <td style="text-align: right; width: 130px; font-weight: 600;"><?php echo number_format($total, 2); ?></td>
                                        </tr>
                                        <tr>
                                            <td>CGST On Professional Fees @ 9%</td>
                                            <td style="text-align: right;">
                                                <?php
                                                if ($cState == $cmState) {
                                                    
                                                    echo number_format($cgst, 2);
                                                } else {
                                                    echo '-';
                                                }
                                                ?>
                                            </td>
                                            
                                        </tr>
                                        <tr>
                                            
                                            <td>PAN No.</td>
                                            <td><?php echo $cm[0]->pan_no; ?></td>
                                            <td>SGST On Professional Fees @ 9%</td>
                                            <td style="text-align: right; width: 130px;">
                                                <?php
                                                if ($cState == $cmState) {
                                                    
                                                    echo number_format($sgst, 2);
                                                } else {
                                                    echo '-';
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>GSTIN No</td>
                                            <td><?php echo $cm[0]->gstn; ?></td>
                                            
                                            <td>IGST On Professional Fees @ 18%</td>
                                            <td style="text-align: right; width: 130px;">
                                                <?php
                                                if ($cState != $cmState) {
                                                    echo number_format($gstamt, 2);
                                                } else {
                                                    echo '-';
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr >
                                            <td>TAN No</td>
                                            <td><?php echo $cm[0]->tan_no; ?></td>
                                            <td style="font-weight: 600;">Total</td>
                                            <td style="text-align: right; font-weight: 600;"><?php echo number_format(round($gTotal), 2); ?></td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>Beneficiary Name</td>
                                            <td><?php echo $cm[0]->company_name; ?></td>
                                            <td rowspan="6" style="width:250px; text-align: center;">
                                                For <?php echo $cm[0]->company_name; ?><br/><br/><br/>
                                                <br/><br/><br/><br/>
                                                Authorised Signatory
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Account Number</td>
                                            <td><?php echo $cm[0]->account_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td>IFSC Code</td>
                                            <td><?php echo $cm[0]->ifsc_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Bank & Branch</td>
                                            <td><?php echo $cm[0]->bank_branch; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align: center;">
                                                <strong>
                                                    Thanking you and assuring our best service at all times.
                                                </strong>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <?php
                }
                if (!empty($invoice[0]->di_debitNumber) || $invoice[0]->di_onlyDebit == 1) {
                    ?>
                    <div class="col-md-10 content-page" style="margin-top: 10px; page-break-before: always;">
                        <div class="panel panel-default in-print">
                            <div class="panel-body">
                                <div>
                                    <?php if (!empty($invoice[0]->di_type)) { ?>
                                        <table class="table table-bordered" style="margin-bottom: 0px;">
                                            <tr>
                                                <td style="text-align: center; font-weight: bold;">
                                                    <?php echo $cm[0]->company_name; ?><br/>
                                                    <?php echo $cm[0]->address; ?><br/>
                                                    Maharashtra, India | Contact No : 8149006055
                                                </td>
                                                <td align="center">Kofax 2.0 <br/> NA</td>
                                            </tr>
                                        </table>
                                    <?php } ?>
                                    <table class="table table-bordered" style="margin-bottom: 0px;">
                                        <tr>
                                            <td colspan="4" style="text-align: center; text-transform: uppercase;"><strong>Tax Invoice For <?php
                                                    if (!empty($invoice[0]->di_type)) {
                                                        echo $invoice[0]->di_type;
                                                    } else {
                                                        echo $invoice[0]->di_project;
                                                    }
                                                    ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">To,</td>
                                            <td style="width:150px;">Invoice Number</td>
                                            <td><?php echo $invoice[0]->di_debitNumber; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" rowspan="<?php
                                            if (!empty($invoice[0]->di_type)) {
                                                echo '6';
                                            } else {
                                                echo '4';
                                            }
                                            ?>">
                                                    <?php echo $invoice[0]->di_address; ?>
                                            </td>
                                            <td>Invoice Date</td>
                                            <td><?php echo date_formate_short($invoice[0]->di_date); ?></td>
                                        </tr>
                                        <?php
                                        if (!empty($invoice[0]->di_type)) {
                                            ?>
                                            <tr>
                                                <td>Vendor Code</td>
                                                <td><?php echo $invoice[0]->di_vendor_code; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Location</td>
                                                <td><?php echo $invoice[0]->di_location; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Department</td>
                                                <td><?php echo $invoice[0]->di_department; ?></td>
                                            </tr>
                                            <tr>
                                                <td>TML Contact e-mail</td>
                                                <td><?php echo $invoice[0]->di_email; ?></td>
                                            </tr>
                                            <tr>
                                                <td>SES Number</td>
                                                <td><?php echo $invoice[0]->di_sesNumber; ?></td>
                                            </tr>
                                            <?php
                                        } else {
                                            ?>
                                            <tr>
                                                <td>Ref. No.</td>
                                                <td><?php 
                                                if(!empty($invoice[0]->di_number)){
                                                echo $invoice[0]->di_number; 
                                                } else {
                                                     echo 'NA';
                                                }
                                                ?></td>
                                            </tr>
                                            <tr>
                                                <td>Name</td>
                                                <td><?php echo $invoice[0]->di_name; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><?php echo $invoice[0]->di_email; ?></td>
                                            </tr>    
                                            <?php
                                        }
                                        ?>
                                        <tr>
                                            <td>State Code</td>
                                            <td><?php echo $invoice[0]->di_state_code; ?></td>
                                            <td>PO Number</td>
                                            <td><?php echo $invoice[0]->di_poNumber; ?></td>
                                        </tr>
                                        <tr>
                                            <td>GSTIN No</td>
                                            <td><?php echo $invoice[0]->di_gst; ?></td>
                                            <td>SAC Code</td>
                                            <td>999293</td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered" style="margin-bottom: 0px;">
                                        <tr>
                                            <td style="text-align: center;">Sl. No.</td>
                                            <td style="text-align: center;">Description</td>
                                            <td style="text-align: center;">Month</td>
                                            <td style="text-align: center;"><?php
                                                if (!empty($invoice[0]->di_type)) {
                                                    echo 'Type of Project';
                                                } else {
                                                    echo 'Location';
                                                }
                                                ?></td>
                                            <td style="text-align: center; width: 130px;">Amount</td>
                                            <?php
                                            if ($invoice[0]->di_status == 0) {
                                                ?>
                                                <td></td>
                                                <?php
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                        $total = 0;
                                        if (!empty($exp)) {
                                            $no = 0;
                                            $debitCount = 0;
                                            foreach ($exp as $exCount) {
                                                if ($exCount->dd_status == 0) {
                                                    $debitCount++;
                                                }
                                            }
                                            foreach ($exp as $exData) {
                                                if ($exData->dd_status == 0) {
                                                    $no++;
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center;"><?php echo $no; ?></td>
                                                        <td style="text-align: center;"><?php echo $exData->dd_desc; ?></td>
                                                        <?php if ($no == 1) { ?>
                                                            <td valign="middle" style="text-align: center; vertical-align: middle;" rowspan="<?php echo $debitCount; ?>">
                                                                <?php
                                                                if($invoice[0]->di_month!='NA'){
                                                                $dt = DateTime::createFromFormat('!m', $invoice[0]->di_month);
                                                                echo $dt->format('F') . '-' . $invoice[0]->di_year;
                                                                } else {
                                                                    echo 'NA';
                                                                }
                                                                ?>
                                                            </td>
                                                            <td valign="middle" style="text-align: center; vertical-align: middle;" rowspan="<?php echo $debitCount; ?>">
                                                                <?php
                                                                if (!empty($invoice[0]->di_type)) {
                                                                    echo $invoice[0]->di_type;
                                                                } else {
                                                                    echo $invoice[0]->di_location;
                                                                }
                                                                ?>
                                                            </td>
                                                        <?php } ?>
                                                        <td style="text-align: right;"><?php echo number_format($exData->dd_amount, 2); ?></td>
                                                        <?php
                                                        if ($invoice[0]->di_status == 0) {
                                                            ?>
                                                            <td>
                                                                <a href="#" class="remove-exp" eid="<?php echo $exData->dd_id; ?>">
                                                                    <i class="fa fa-remove"></i>
                                                                </a>
                                                            </td>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tr>
                                                    <?php
                                                    $total +=$exData->dd_amount;
                                                }
                                            }
                                        }
                                        ?>
                                    </table>
                                    <?php
                                    $cmState = $cm[0]->gst_state_code;
                                    $cState = $invoice[0]->di_state_code;
                                    $cgst = 0;
                                    $sgst = 0;
                                    $gTotal = 0;
                                    ?>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td rowspan="2" align="center">Rupees In Words</td>
                                            <td rowspan="2" align="center">

                                                <?php
                                                if ($cState != $cmState) {
                                                    $gstamt = round((18 / 100) * $total,2);
                                                } else {
                                                    $cgst = round((9 / 100) * $total,2);
                                                    $sgst = round((9 / 100) * $total,2);
                                                    $gstamt = $cgst + $sgst;
                                                }
                                                $gTotal = $total + $gstamt;
                                                $debitAmt = $total;
                                                $debitGST = $gstamt;
                                                $debitTotal = round($gTotal);
                                                echo 'Rupees '.getIndianCurrency(round($gTotal)).' Only';
                                                ?>

                                            </td>
                                            
                                            <td style="font-weight: 600;">Gross Total</td>
                                            <td style="text-align: right; width: 130px; font-weight: 600;"><?php echo number_format($total, 2); ?></td>
                                        </tr>
                                        <tr>
                                            
                                            <td>CGST On Professional Fees @ 9%</td>
                                            <td style="text-align: right;">
                                                <?php
                                                if ($cState == $cmState) {
                                                    
                                                    echo number_format($cgst, 2);
                                                } else {
                                                    echo '-';
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>PAN No.</td>
                                            <td><?php echo $cm[0]->pan_no; ?></td>
                                            
                                            <td>SGST On Professional Fees @ 9%</td>
                                            <td style="text-align: right; width: 130px;">
                                                <?php
                                                if ($cState == $cmState) {
                                                    
                                                    echo number_format($sgst, 2);
                                                } else {
                                                    echo '-';
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>GSTIN No</td>
                                            <td><?php echo $cm[0]->gstn; ?></td>
                                            <td>IGST On Professional Fees @ 18%</td>
                                            <td style="text-align: right; width: 130px;">
                                                <?php
                                                if ($cState != $cmState) {
                                                    echo number_format($gstamt, 2);
                                                } else {
                                                    echo '-';
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr style="font-weight: 600;">
                                            <td>TAN No</td>
                                            <td><?php echo $cm[0]->tan_no; ?></td>
                                            <td>Total</td>
                                            <td style="text-align: right;"><?php echo number_format(round($gTotal), 2); ?></td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>Beneficiary Name</td>
                                            <td><?php echo $cm[0]->company_name; ?></td>
                                            <td rowspan="6" style="width:250px; text-align: center;">
                                                For <?php echo $cm[0]->company_name; ?><br/><br/><br/>
                                                <br/><br/><br/><br/>
                                                Authorised Signatory
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Account Number</td>
                                            <td><?php echo $cm[0]->account_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td>IFSC Code</td>
                                            <td><?php echo $cm[0]->ifsc_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Bank & Branch</td>
                                            <td><?php echo $cm[0]->bank_branch; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align: center;">
                                                <strong>
                                                    Thanking you and assuring our best service at all times.
                                                </strong>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                }
                if (!empty($payment)) {
                    ?>
                    <div class="col-md-10 content-page">
                        <div class="panel panel-default no-print">
                            <div class="panel-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <td colspan="3"><h4>Payment Details</h4></td>
                                    </tr>
                                    <tr>
                                        <td>Amount</td>
                                        <td>Date</td>
                                        <td>Payment id</td>
                                    </tr>
                                    <tr>
                                        <td><?php echo number_format($payment[0]->cp_amt, 2); ?></td>
                                        <td><?php echo date_formate_short($payment[0]->cp_date); ?></td>
                                        <td><?php echo $payment[0]->cp_pay_id; ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                }
                ?>
            </div>
        </div>


        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/additional-methods.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>

    </body>
</html>