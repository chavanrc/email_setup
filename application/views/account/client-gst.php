<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Clients</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>GST Numbers for Client</h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" style="display: inline-block;"><i class="fa fa-list"></i> GST List </h2>
                            <a href="<?php echo base_url(); ?>account/clients" class="btn-sm btn-danger pull-right">Back</a>
                            
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 1) {
                                ?>
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="alert alert-success">
                                        GST imported successfully
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <table class="table table-bordered" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Client</th>
                                        <th>Address</th>
                                        <th>State</th>
                                        <th>State Code</th>
                                        <th>GST Number</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($gst)) {
                                        $num = 0;
                                        foreach ($gst as $gs_data) {
                                            $num++;
                                            ?>
                                            <tr>
                                                <td><?php echo $num; ?></td>
                                                <td><?php echo $gs_data->client_name; ?></td>
                                                <td><?php echo $gs_data->client_address; ?></td>
                                                <td><?php echo $gs_data->state; ?></td>
                                                <td><?php echo $gs_data->state_code; ?></td>
                                                <td><?php echo $gs_data->gst_number; ?></td>
                                                <td style="width:90px;">
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="7">No Record found</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php include 'js_files.php'; ?>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
<?php if (!empty($gst)) { ?>
                    $('#data-list').DataTable({
                        "pageLength": 25
                    });
<?php } ?>
                $('.import-btn').click(function (e) {
                    e.preventDefault();
                    $('#import-gst-wraper').modal('show');
                });
                
                $('.add-btn').click(function (e) {
                    e.preventDefault();
                    $('#add-gst-wraper').modal('show');
                });

                $('.delete-btn').click(function (e) {
                    e.preventDefault();
                    var cid = $(this).attr('cid');
                    var ele = $(this).parent('td').parent('tr');
                    var f = confirm("Are you sure want to delete ?");
                    if (f == true) {
                        $('.page_spin').show();
                        var dataString = "cid=" + cid + "&page=remove_client_gst";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                alert('GST removed');
                                ele.hide();
                            }, //success fun end
                        });//ajax end
                    }
                });
                
                $("#add-gst-form").validate({
                    rules: {
                        ccid: "required",
                        gstaddress: "required",
                        state: "required",
                        statecode: "required",
                        gst: "required"
                        
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
//                    submitHandler: function (form) {
//                        assign_pm();
//                    }
                });

                $("#assign-pm-form").validate({
                    rules: {
                        cid: "required",
                        gstFile: {
                            required: true,
                            extension: "xls"
                        },
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
//                    submitHandler: function (form) {
//                        assign_pm();
//                    }
                });

            });
        </script>

    </body>
</html>