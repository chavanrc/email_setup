<table border="1px" style="text-align: center;">
    <thead>
        <tr>
            <th>Company</th>
            <th>Name Of the Client</th>
            <th>Address</th>
            <th>GSTN</th>
            <th>Name Of the Program</th>
            <th>Invoice Number</th>
            <th>Invoice Type</th>
            <th>Month</th>
            <th>Program Start Date</th>
            <th>Program End Date</th>
            <th>Location</th>
            <th>State Code</th>
            <th>Invoice Date</th>
            <th>Invoice Amount</th>
            <th>No. Of. Days</th>
            <th>CGST</th>
            <th>SGST</th>
            <th>IGST</th>
            <th>Total Amount</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($invoice)) {
            $no = 0;
            foreach ($invoice as $in_data) {
                ?>
                <tr>
                    <td><?php echo $in_data->company; ?></td>
                    <td><?php echo $in_data->clientname; ?></td>
                    <td><?php 
                    $add = preg_replace("<br/>", " ", $in_data->gst_address);
                    echo $add;
                    ?></td>
                    <td><?php echo $in_data->gst; ?></td>
                    <td><?php echo $in_data->project; ?></td>
                    <td><?php
                        if ($in_data->type == 'drona') {
                            if ($in_data->debitid != '') {
                                echo $in_data->debitid;
                            } else {
                                echo $in_data->number;
                            }
                        } else {
                            if ($in_data->debitid == 0) {
                                echo $in_data->number;
                            } else {
                                echo $in_data->nofrmt . 'D/' . str_pad($in_data->debitid, 3, "0", STR_PAD_LEFT);
                            }
                        }
                        ?></td>
                    <td>
                        <?php
                        if ($in_data->type == 'drona') {
                            if ($in_data->debitid == '') {
                                echo 'Drona Professional Fees';
                            } else {
                                echo 'Drona Debit Note';
                            }
                        } else {
                            if ($in_data->debitid == 0) {
                                echo 'Professional Fees';
                            } else {
                                echo 'Debit Note';
                            }
                        }
                        ?>

                    </td>
                    <td>
                        <?php echo $in_data->month . ' - ' . $in_data->year; ?>
                    </td>
                    <td><?php
                        if ($in_data->type == 'Program') {
                            echo date_formate_short($in_data->sdate);
                        }
                        ?>
                    </td>
                    <td><?php
                        if ($in_data->type == 'Program') {
                            echo date_formate_short($in_data->edate);
                        }
                        ?>
                    </td>
                    <td><?php echo $in_data->location; ?></td>
                    <td style="text-align: center;"><?php echo $in_data->statecode; ?></td>
                    <td><?php echo date_formate_short($in_data->date); ?></td>
                    <td><?php
                        if ($in_data->type == 'drona') {
                            if ($in_data->debitid == '') {
                                echo $in_data->amt;
                            } else {
                                echo $in_data->damt;
                            }
                        } else {
                            if ($in_data->debitid == 0) {
                                echo $in_data->amt;
                            } else {
                                echo $in_data->damt;
                            }
                        }
                        ?></td>
                    <td style="text-align: center;"><?php echo $in_data->duration; ?></td>
                    <td>

                        <?php
                        $gst = '';
                        $igst = '';

                        if ($in_data->type == 'drona') {
                            if ($in_data->debitid == '') {
                                if ($in_data->statecode == $in_data->gststatecode) {
                                    if ($in_data->gstamt != '0') {
                                        $gst = $in_data->gstamt / 2;
                                    }
                                } else {
                                    if ($in_data->gstamt != '0') {
                                        $igst = $in_data->gstamt;
                                    }
                                }
                            } else {
                                if ($in_data->statecode == $in_data->gststatecode) {
                                    if ($in_data->dgstamt != '0') {
                                        $gst = $in_data->dgstamt / 2;
                                    }
                                } else {
                                    if ($in_data->dgstamt != '0') {
                                        $igst = $in_data->dgstamt;
                                    }
                                }
                            }
                        } else {
                            if ($in_data->debitid == 0) {
                                if ($in_data->statecode == $in_data->gststatecode) {
                                    if ($in_data->gstamt != '0') {
                                        $gst = $in_data->gstamt / 2;
                                    }
                                } else {
                                    if ($in_data->gstamt != '0') {
                                        $igst = $in_data->gstamt;
                                    }
                                }
                            } else {
                                if ($in_data->statecode == $in_data->gststatecode) {
                                    if ($in_data->dgstamt != '0') {
                                        $gst = $in_data->dgstamt / 2;
                                    }
                                } else {
                                    if ($in_data->dgstamt != '0') {
                                        $igst = $in_data->dgstamt;
                                    }
                                }
                            }
                        }
                        echo $gst;
                        ?>

                    </td>
                    <td><?php echo $gst; ?></td>
                    <td><?php echo $igst; ?></td>
                    <td><?php
                        if ($in_data->type == 'drona') {
                            if ($in_data->debitid == '') {
                                echo round($in_data->amt + $in_data->gstamt);
                            } else {
                                echo round($in_data->damt + $in_data->dgstamt);
                            }
                        } else {
                            if ($in_data->debitid == 0) {
                                echo $in_data->amt + $in_data->gstamt;
                            } else {
                                echo $in_data->damt + $in_data->dgstamt;
                            }
                        }
                        ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>