<div class="top_nav no-print">
    <span><strong style="color:#FF0000; font-size:18px; margin-left:20px;">CRM SANDBOX</strong></span>
    <div class="nav_menu nav_menu_right">
        <ul class="nav-menu-list">
            <li class="nav-menu-list-item" style="padding-right: 40px;">
                <a href="#" data-toggle="dropdown" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">

                    <img src="<?php echo base_url(); ?>assets/images/lathesh.jpg" class="img-circle" width="30px;">

                    <?php echo $this->session->userdata('name'); ?>
                    <span class="fa fa-angle-down"></span>
                </a>
                <ul class="dropdown-menu" aria-labelledby="dLabel">
                    <li><a href="<?php echo base_url(); ?>account/profile">Profile <i class="fa fa-user pull-right"></i></a></li>
                    <li><a href="<?php echo base_url(); ?>account/logout">Logout <i class="fa fa-sign-out pull-right"></i></a></li>
                </ul>
            </li>

        </ul>
    </div>
    <div class="clearfix"></div>
</div>