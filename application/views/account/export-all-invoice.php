<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3 page-title">
                            <h3>Monthly Invoices</h3>
                        </div>
                        <div class="col-md-9">
                            <?php
                            $page = 1;
                            $next = 2;
                            $totalPage = 0;
                            if (isset($_GET['page'])) {
                                $page = $_GET['page'];
                            }
                            if (!empty($count)) {
                                $totalPage = ceil($count[0]->total / 20);
                                if ($count[0]->total > 20) {
                                    ?>
                                    <h4 class="pull-right" style="display: inline-block; margin-top: 10px;"><?php echo (($page - 1) * 20) + 1; ?> - <?php
                                        if ($page < $totalPage) {
                                            echo $page * 20;
                                        } else {
                                            echo $count[0]->total;
                                        }
                                        ?> of <?php echo $count[0]->total; ?> </h4>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page" style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table class="table table-bordered">

                                <form action="<?php echo base_url(); ?>account/export_all_invoice" method="POST">
                                    <tr>
                                        <td>
                                            <select class="form-control" id="pclient" name="pclient">
                                                <option value="All"> - All Client -</option>
                                                <?php
                                                if (!empty($client)) {
                                                    foreach ($client as $cl_data) {
                                                        ?>
                                                        <option value="<?php echo $cl_data->client_id; ?>" <?php
                                                        if (isset($_POST['pclient'])) {
                                                            if ($_POST['pclient'] == $cl_data->client_id) {
                                                                echo 'selected';
                                                            }
                                                        }
                                                        ?> > <?php echo $cl_data->client_name; ?> </option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control" name="month[]" multiple="multiple">                                                
                                                <option value="All"> All </option>                                                
                                                <option value="NA"> NA </option>                                             
                                                <option value="01"> Jan </option>                                             
                                                <option value="02"> Feb </option>                                                
                                                <option value="03"> Mar </option>                                                
                                                <option value="04"> Apr </option>                                                
                                                <option value="05"> May </option>                                                
                                                <option value="06"> Jun </option>                                                
                                                <option value="07"> Jul </option>                                                
                                                <option value="08"> Aug </option>                                                
                                                <option value="09"> Sep </option>                                                
                                                <option value="10"> Oct </option>                                                
                                                <option value="11"> Nov </option>                                                
                                                <option value="12"> Dec </option>                                                                                            
                                            </select>
                                        </td>
                                        <td>
                                            <select class="form-control" name="year">                                                
                                                <option value="All"> All </option>                                                
                                                <option value="2019"> 2019 </option>                                                
                                                <option value="2020"> 2020 </option>                                                
                                                <option value="2021"> 2021 </option>                                                
                                                <option value="2022"> 2022 </option>                                                                                                                              
                                                <option value="2023"> 2023 </option>                                                                                                                              
                                                <option value="2024"> 2024 </option> 
                                            </select>
                                        </td>

                                        <td>
                                            <button class="btn-sm btn-primary">Export</button>
                                        </td>
                                    </tr>
                                </form>

                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/additional-methods.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {

                $("#creatinForm").validate({
                    rules: {
                        company: "required",
                        client: "required",
                        gst: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });


            });
        </script>

    </body>
</html>