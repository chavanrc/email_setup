<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3 page-title">
                            <h3>Client Invoices</h3>
                        </div>
                        <div class="col-md-9">
                            <?php
                            $page = 1;
                            $next = 2;
                            $totalPage = 0;
                            if (isset($_GET['page'])) {
                                $page = $_GET['page'];
                            }
                            if (!empty($count)) {
                                $totalPage = ceil($count[0]->total / 20);
                                if ($count[0]->total > 20) {
                                    ?>
                                    <h4 class="pull-right" style="display: inline-block; margin-top: 10px;"><?php echo (($page - 1) * 20) + 1; ?> - <?php
                                        if ($page < $totalPage) {
                                            echo $page * 20;
                                        } else {
                                            echo $count[0]->total;
                                        }
                                        ?> of <?php echo $count[0]->total; ?> </h4>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page" style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" style="display: inline-block;"><i class="fa fa-list"></i> Client Invoices List </h2>
                            
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" style="font-size: 12px;">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Client</th>
                                        <th style="width:75px;">Invoice No.</th>
                                        <th>Date</th>
                                        <th>Program</th>
                                        <th>Status</th>
                                        <th>Amount</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <form action="<?php echo base_url(); ?>account/client_invoice" method="POST">
                                    <tr>
                                        <td></td>
                                        <td>
                                            <select class="form-control" id="pclient" name="pclient">
                                                <option value="All"> - All Client -</option>
                                                <?php
                                                if (!empty($client)) {
                                                    foreach ($client as $cl_data) {
                                                        ?>
                                                        <option value="<?php echo $cl_data->client_id; ?>" <?php
                                                        if (isset($_POST['pclient'])) {
                                                            if ($_POST['pclient'] == $cl_data->client_id) {
                                                                echo 'selected';
                                                            }
                                                        }
                                                        ?> > <?php echo $cl_data->client_name; ?> </option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                            </select>
                                        </td>
                                        <td colspan="2">
                                            <input type="text" class="form-control" placeholder="Invoice No" name="invoice"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" placeholder="Program" name="title"/>
                                        </td>

                                        <td colspan="2">
                                            <select class="form-control" name="status" style="display: inline-block; width: 130px;">
                                                <option value="All"> All </option>
                                                <option value="0"> Pending </option>
                                                <option value="1"> Confirmed </option>
                                            </select>
                                        </td>
                                        <td>
                                            <button class="btn-sm btn-primary"><i class="fa fa-search"></i></button>
                                        </td>
                                    </tr>
                                </form>
                                <?php
                                if (!empty($invoice)) {
                                    $no = 0;
                                    if (isset($_GET['page'])) {
                                        $no = 20 * ($_GET['page'] - 1);
                                    }
                                    foreach ($invoice as $in_data) {
                                        $no++;
                                        ?>
                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo $in_data->client_name; ?></td>
                                            <td><?php echo $in_data->ci_number; ?> <br/>
                                                <?php
                                                if ($in_data->ci_debit_id != '0') {
                                                    echo $in_data->ci_no_frmt;
                                                    ?>D/<?php
                                                    echo str_pad($in_data->ci_debit_id, 3, "0", STR_PAD_LEFT);
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo date_formate_short($in_data->ci_date); ?></td>
                                            <td><?php
                                                if ($in_data->ci_type == 'Consolidated') {
                                                    echo 'Multiple';
                                                } else {
                                                    echo $in_data->project_title;
                                                }
                                                ?></td>
                                            <td>
                                                <?php
                                                if ($in_data->ci_status == '0') {
                                                    echo 'Confirmation Pending';
                                                } else {
                                                    echo 'Confirmed';
                                                }
                                                $duration = $in_data->training_duration - 1;
                                                $cr = $in_data->payment_credit_days + $duration;
                                                if ($in_data->ci_type == 'Consolidated') {
                                                    $dueDate = date('d-m-Y', strtotime($in_data->ci_date . ' + ' . $cr . ' days'));
                                                } else {
                                                    $dueDate = date('d-m-Y', strtotime($in_data->training_start_date . ' + ' . $cr . ' days'));
                                                }
                                                ?>
                                                <br/>
                                                <strong>Due Date :</strong> <?php echo $dueDate; ?>
                                            </td>
                                            <td>
                                                <?php echo round($in_data->ci_amt + $in_data->ci_gstamt); ?><br/>
                                                <strong>Status : </strong><?php
                                                if ($in_data->client_payment_receieved == 'Y') {
                                                    echo 'Paid';
                                                } else {
                                                    echo 'Pending';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url(); ?>account/client_invoice_details/<?php echo $in_data->ci_id; ?>?page=<?php echo $page; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                            <div class="text-right" style="font-size:16px; padding: 10px;">
                                <?php
                                if ($page > 1) {
                                    $next = $page + 1;
                                    $prev = $page - 1;
                                    ?>
                                    <a href="<?php echo base_url(); ?>account/client_invoice/?page=1"><i class="fa fa-backward" style="font-size:12px;"></i> First</a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>account/client_invoice/?page=<?php echo $prev; ?>"><i class="fa fa-caret-left"></i> Prev</a>
                                    <?php
                                }
                                if ($page < $totalPage) {
                                    ?>
                                    &nbsp;&nbsp;<a href="<?php echo base_url(); ?>account/client_invoice/?page=<?php echo $next; ?>">Next <i class="fa fa-caret-right"></i> </a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>account/client_invoice/?page=<?php echo $totalPage; ?>">Last <i class="fa fa-forward" style="font-size:12px;"></i> </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/additional-methods.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {

               
                $('#client').change(function () {
                    var client = $(this).val();
                    if (client != '') {
                        $('.page_spin').show();
                        var dataString = "cid=" + client + "&page=get_client_location";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                $('.load-gst').html(data);
                            }, //success fun end
                        });//ajax end
                    }
                });


                

            });
        </script>

    </body>
</html>