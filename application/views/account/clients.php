<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Clients</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
        <link href='https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' />
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Clients</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>account/client_gst" class="btn btn-success"> GST Data</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" style="display:inline-block;"><i class="fa fa-list"></i> Client List </h2>
                            <select class="form-control pull-right" id="cType" style="width:250px;">
                                <option value="">All</option>
                                <option value="1">Wagons Management Consulting</option>
                                <option value="2">Wagons Learning Pvt. Ltd. </option>
                            </select>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Logo</th>
                                        <th>Industry</th>
                                        <th style="width:100px !important;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($client)) {
                                        $num = 0;
                                        foreach ($client as $cl_data) {
                                            $num++;
                                            ?>
                                            <tr class="crow company<?php echo $cl_data->company_id; ?>">
                                                <td><?php echo $num; ?></td>
                                                <td><?php echo $cl_data->client_name; ?><br/>
                                                    <?php
                                                    $check = $CI->admin_model->check_client_assign($cl_data->client_id);
                                                    if (!empty($check)) {
                                                        ?>
                                                        <span class="label label-success">PM</span> : <?php echo $check[0]->name; ?><br/>
                                                        <span class="label label-success">BM</span> : <?php echo $check[0]->bname; ?>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if (!empty($cl_data->logo)) {
                                                        ?>
                                                        <img src="<?php echo base_url(); ?>assets/upload/client/<?php echo $cl_data->logo; ?>" width="50px"/>
                                                    <?php } ?>
                                                </td>
                                                <td><?php echo $cl_data->industry; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>account/client_gst/<?php echo $cl_data->client_id; ?>" class="btn-sm btn-primary">GST</a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="9">No Record found</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                
            });
        </script>

    </body>
</html>