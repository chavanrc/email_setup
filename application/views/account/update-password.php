<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Wagons | Update Password</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <div class="page_spin">
            <br/>
            <div class="spin_icon">
                <i class="fa fa-spinner fa-spin"></i><br/>
                <span>One moment ...</span>
            </div>
        </div>
        <div class="col-md-4 col-md-offset-4" style="margin-top: 7%;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title" style="font-size:23px;">Update Password <i class="fa fa-shield"></i></h2>
                </div>
                <div class="panel-body">
                    <div class="alert alert-danger login_error" style="display: none;">Wrong Username & Password</div>
                    <div class="alert alert-success login_success" style="display: none;">Your Password updated successfully <br/> <a href="<?php echo base_url(); ?>admin/login" class="btn btn-sm btn-primary">Login</a></div>
                    <form action="" method="POST" id="admin_login_form">
                        <div class="form-group">
                            <input type="hidden" id="user_code" name="user_code" value="<?php echo $user_code; ?>"/>
                            <input type="text" name="username" id="username" readonly="readonly" class="form-control" value="<?php echo $email; ?>"/>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password"/>
                        </div>
                        <div class="form-group">
                             <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-send"></i> Submit</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {

                $('.forgot-btn').click(function (e) {
                    e.preventDefault();
                    $('#ForgotPass-wrap').modal('show');
                });

                $('#username').focus(function () {
                    $('.login_error').hide();
                });

                $('#password').focus(function () {
                    $('.login_error').hide();
                });

                $("#admin_login_form").validate({
                    rules: {
                        username: "required",
                        password: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                        example5: {
                            placement: 'right',
                            html: true
                        }
                    },
                    submitHandler: function (form) {
                        admin_login();
                    }


                });

                $("#forgot-password-form").validate({
                    rules: {
                        reg_email: {
                            required: true,
                            email: true
                        },
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });
                

                function admin_login()
                {
                    var username = $('#username').val();
                    var password = $('#password').val();
                    var user_code = $('#user_code').val();

                    $('.page_spin').show();
                    var dataString = "user_code="+user_code+"&username=" + username + "&password=" + password + "&page=update_password";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            var replay = $.trim(data);
                            if (replay == '1')
                            {
                                $('.login_success').show();
                            }
                            
                            else
                            {
                                $('.login_error').show();
                            }

                        }, //success fun end
                    });//ajax end
                }



            });
        </script>
    </body>
</html>