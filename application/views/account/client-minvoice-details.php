<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
        <style>
            .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
                border-top:solid 1px #000 !important;
            }
            .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
                border: 1px solid #000 !important;
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                        <div class="page-title col-md-4 no-print">
                            <?php
                        if ($invoice[0]->ci_img != '') {
                            $ciImg = explode(',', $invoice[0]->ci_img);
                            foreach($ciImg as $ciimgdata){
                            ?>
                        <a href="<?php echo base_url(); ?>assets/upload/client/<?php echo $ciimgdata; ?>" target="_blank" class="btn btn-warning" style="margin-right:5px;"><i class="fa fa-file-image-o" aria-hidden="true"></i> Invoice Copy</a>
                            <?php
                            }
                        }
                        ?>
                        </div>
                        
                    <div class="page-title title-right col-md-8 no-print">
                        <?php
                        if ($invoice[0]->ci_status == '0') {
                            
                        } else {
                            ?>
                            <a href="#" onclick="window.print();" class="create-invoice-btn btn btn-primary">Print / Download</a>
                            <?php
                        }
                        ?>

                        <a href="<?php echo base_url(); ?>account/client_invoice<?php if(isset($_GET['page'])){ echo '?page='.$_GET['page']; } ?>" class="create-invoice-btn btn btn-danger">Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-10 content-page">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <tr style="font-size:20px;">
                                    <td colspan="3" style="text-align: center;"><h4 style="font-size:20px;">Tax Invoice</h4>
                                    <?php if($invoice[0]->include_gst=='N'){
                                        ?>
                                        <h4 style="font-size:15px;">Export Of Goods Or Services Without Payment Of Integrated Tax</h4>
                                        <?php
                                    } ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="5" style="width:500px;">To, <br/>
                                        <?php echo $client[0]->client_name; ?><br/>
                                        <?php
                                        if (!empty($invoice)) {
                                            echo $invoice[0]->ci_gst_address;
                                        }
                                        ?>
                                    </td>
                                    <td>Invoice Number</td>
                                    <td><?php echo $invoice[0]->ci_number; ?></td>
                                </tr>
                                <tr>
                                    <td>Invoice Date</td>
                                    <td><?php echo date_formate_short($invoice[0]->ci_date); ?></td>
                                </tr>
                                <tr>
                                    <td>Ref. No</td>
                                    <td><?php
                                        if (!empty($invoice[0]->ci_refNumber)) {
                                            echo $invoice[0]->ci_refNumber;
                                        } else {
                                            echo 'NA';
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>Name</td>
                                    <td><?php echo $client[0]->person1; ?></td>
                                </tr>
                                <tr>

                                    <td>Email id</td>
                                    <td><?php echo $client[0]->email1; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>State Code :  </strong><?php
                                        if (!empty($invoice)) {
                                            echo $invoice[0]->ci_state_code;
                                        }
                                        ?> 
                                    </td>
                                    <td>PO. No</td>
                                    <td><?php
                                        if (!empty($invoice[0]->ci_poNumber)) {
                                            echo $invoice[0]->ci_poNumber;
                                        } else {
                                            echo 'NA';
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>GST NO: </strong> <?php
                                        if (!empty($invoice)) {
                                            echo $invoice[0]->ci_gst;
                                        }
                                        ?>
                                    </td>
                                    <td>SAC Code</td>
                                    <td>999293</td>
                                </tr>
                            </table>
                            <?php
                            $total = 0;
                            $no = 0;
                            ?>
                            <table class="table table-bordered text-center">
                                <tr style="font-weight: bold;">
                                    <td>Sl. No</td>
                                    <td>Description</td>
                                    <td>Name of the Program</td>
                                    <td>Month of the Program</td>
                                    <td>Training Location</td>
                                    <td>No. of Days</td>
                                    <td>Amount</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td><?php echo $invoice[0]->ci_desc; ?></td>
                                    <td><?php echo $invoice[0]->ci_project; ?></td>
                                    <td><?php
                                        $dt = DateTime::createFromFormat('!m', $invoice[0]->ci_month);
                                        echo $dt->format('F').'-'.$invoice[0]->ci_year;
                                        ?>
                                    </td>
                                    <td>As per Voucher</td>
                                    <td><?php echo $invoice[0]->ci_days; ?></td>
                                    <td class="text-right"><?php $total = $invoice[0]->ci_charge; echo number_format($total, 2) ?></td>
                                </tr>
                                <tr style="font-size:18px;">
                                    <td colspan="6" class="text-right">Gross Total</td>
                                    <td style="width:100px; text-align:right;"><?php echo number_format($total, 2); ?></td>
                                </tr>

                            </table>
                            <?php
                            $updAmount = 0;
                            $upAmount = $total;
                            $company = $CI->admin_model->get_single_company($invoice[0]->ci_company);
                            $cmState = $company[0]->gst_state_code;
                            $cState = $company[0]->gst_state_code;
                            $cgst = 0;
                            $sgst = 0;
                            if (!empty($invoice)) {
                                $cState = $invoice[0]->ci_state_code;
                            }
                            ?>
                            <table class="table table-bordered">
                                <tr>
                                    <td>PAN No.</td>
                                    <td><?php echo $company[0]->pan_no; ?></td>
                                    <td class="text-right">CGST On Professional Fees @9%</td>
                                    <td style="width:100px; text-align:right;">
                                        <?php
                                        if ($cState == $cmState && $invoice[0]->include_gst == 'Y') {
                                            $cgst = round((9 / 100) * $total, 2);
                                            echo number_format($cgst, 2);
                                        } else {
                                            echo '-';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>GSTIN</td>
                                    <td><?php echo $company[0]->gstn; ?></td>
                                    <td class="text-right">SGST On Professional Fees @9%</td>
                                    <td style="text-align:right;"><?php
                                        if ($cState == $cmState && $invoice[0]->include_gst == 'Y') {
                                            $sgst = round((9 / 100) * $total, 2);
                                            echo number_format($sgst, 2);
                                        } else {
                                            echo '-';
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>TAN No</td>
                                    <td><?php echo $company[0]->tan_no; ?></td>
                                    <td class="text-right">IGST On Professional Fees @18%</td>
                                    <td style="text-align:right;">
                                        <?php
                                        if ($cState != $cmState && $invoice[0]->include_gst == 'Y') {
                                            $gstamt = round((18 / 100) * $total,2);
                                            echo number_format($gstamt, 2);
                                        } else {
                                            $gstamt = $cgst + $sgst;
                                            echo '-';
                                        }
                                        $updGst = 0;
                                        $upGst = $gstamt;
                                        $gTotal = round($total + $gstamt);
                                        ?>
                                    </td>
                                </tr>
                                <tr style="font-size:20px;">
                                    <td colspan="2"></td>
                                    <td class="text-right">Total</td>
                                    <td style="text-align:right;">
                                        <?php
                                        echo number_format($gTotal,2);
                                        
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <strong>In Words: 
                                            <?php
                                            //$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                            echo "Rs. " . getIndianCurrency($gTotal) . " Only.";
                                            ?>
                                        </strong>
                                    </td>
                                </tr>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td>Beneficiary</td>
                                    <td><?php echo $company[0]->company_name; ?></td>
                                    <td rowspan="5" style="text-align: center;">For, <?php echo $company[0]->company_name; ?>
                                        <br/><br/><br/><br/><br/><br/><br/><br/>
                                        Authorized Signatory
                                    </td>
                                </tr>
                                <tr>
                                    <td>Account No</td>
                                    <td><?php echo $company[0]->account_no; ?></td>
                                </tr><tr>
                                    <td>IFSC Code</td>
                                    <td><?php echo $company[0]->ifsc_no; ?></td>
                                </tr>
                                <tr>
                                    <td>Bank & Branch</td>
                                    <td><?php echo $company[0]->bank_branch; ?></td>
                                </tr>
                                <tr>
                                    <td class="text-center" colspan="2">
                                        <strong>
                                            * Thanking you and assuring our best services at all
                                        </strong>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?php
                    if (!empty($payment)) {
                        ?>
                        <div class="panel panel-default no-print">
                            <div class="panel-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <td colspan="3"><h4>Payment Details</h4></td>
                                    </tr>
                                    <tr>
                                        <td>Amount</td>
                                        <td>Date</td>
                                        <td>Payment id</td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $payment[0]->cp_amt; ?></td>
                                        <td><?php echo date_formate_short($payment[0]->cp_date); ?></td>
                                        <td><?php echo $payment[0]->cp_pay_id; ?></td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                </div>
            </div>
        </div>
        

        <?php include 'js_files.php'; ?>
       
        <script type="text/javascript">
            $(document).ready(function () {

            });
        </script>

    </body>
</html>