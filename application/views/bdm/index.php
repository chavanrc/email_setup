<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Trainer | Dashboard</title>
        <?php include 'css_files.php'; ?>

    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px; padding-top: 20px;">

                <div class="col-md-6">
                    <div class="box box-success">
                        <div class="box-header with-border text-center">
                            <a href="<?php echo base_url(); ?>bdm/client_programs">
                                <h4 style="display: inline-block;">Upcoming Programs</h4>
                            </a>
                        </div>
                        <div class="box-body text-center">
                            <span style="font-size:25px">
                                <a href="<?php echo base_url(); ?>bdm/client_programs">
                                    <?php
                                    $upTotal = $this->business_model->getBDMTotalUpcoming($this->session->userdata('t_code'));
                                    echo $upTotal[0]->total;
                                    ?>
                                </a>
                            </span>
                        </div>
                    </div>
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h4 style="display: inline-block;">Top Clients</h4>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Client</th>
                                        <th>No. Programs</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $up = $this->business_model->getBDMUpcomingProgram($this->session->userdata('t_code'));
                                    if (!empty($up)) {
                                        foreach ($up as $updata) {
                                            ?>
                                            <tr>
                                                <td><?php echo $updata->client; ?></td>
                                                <td><?php echo $updata->total; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>bdm/client_programs/<?php echo $updata->client_id; ?>" class="btn-sm btn-primary"><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <tr>
                                        <td colspan="3">
                                            <select class="form-control" id="clientOption">
                                                <option value=""> - Select Client - </option>
                                                <?php
                                                if (!empty($client)) {
                                                    foreach ($client as $cdata) {
                                                        ?>
                                                        <option value="<?php echo $cdata->client_id; ?>"><?php echo $cdata->client_name; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h4 style="display: inline-block;">Pending Contents</h4>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Client</th>
                                        <th>No. Modules</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $cn = $this->business_model->getBDMContentPending($this->session->userdata('t_code'));
                                    if (!empty($cn)) {
                                        foreach ($cn as $updata) {
                                            ?>
                                            <tr>
                                                <td><?php echo $updata->client; ?></td>
                                                <td><?php echo $updata->total; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>bdm/pending_content/<?php echo $updata->client_id; ?>" class="btn-sm btn-primary"><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-success">
                        <div class="box-header with-border text-center">
                            <a href="<?php echo base_url(); ?>bdm/past_programs">
                                <h4 style="display: inline-block;">Past Programs</h4>
                            </a>
                        </div>
                        <div class="box-body text-center">
                            <span style="font-size:25px">
                                <a href="<?php echo base_url(); ?>bdm/past_programs">
                                    <?php
                                    $pastTotal = $this->business_model->getBDMTotalPast($this->session->userdata('t_code'));
                                    echo $pastTotal[0]->total;
                                    ?>
                                </a>
                            </span>
                        </div>
                    </div>


                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h4 style="display: inline-block;">Client Invoice</h4>
                            <a href="<?php echo base_url(); ?>bdm/client_invoice" class="btn-sm btn-primary pull-right">Client Invoice</a>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Amount</th>
                                        <th>GST</th>
                                        <th>Total Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $amt = $this->business_model->getBDMInvoiceTotal($this->session->userdata('t_code'));
                                    if (!empty($amt)) {
                                        ?>
                                        <tr>
                                            <td>Professional Fees</td>
                                            <td>
                                                <?php echo number_format(round($amt[0]->amt)); ?>
                                            </td>
                                            <td>
                                                <?php echo number_format(round($amt[0]->gst)); ?>
                                            </td>
                                            <td>
                                                <?php echo $ptotal = number_format(round($amt[0]->amt) + round($amt[0]->gst)); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Debit Note</td>
                                            <td>
                                                <?php echo number_format(round($amt[0]->damt)); ?>
                                            </td>
                                            <td>
                                                <?php echo number_format(round($amt[0]->dgst)); ?>
                                            </td>
                                            <td>
                                                <?php echo $dtotal = number_format(round($amt[0]->damt) + round($amt[0]->dgst)); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Total</td>
                                            <td><?php
                                                $tamt = round($amt[0]->damt) + round($amt[0]->amt);
                                                echo number_format($tamt);
                                                ?></td>
                                            <td><?php
                                                $tgst = round($amt[0]->dgst) + round($amt[0]->gst);
                                                echo number_format($tgst);
                                                ?></td>
                                            <td><?php echo number_format($tamt + $tgst); ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h4 style="display: inline-block;">Trainer Invoice</h4>
                            <a href="<?php echo base_url(); ?>bdm/trainer_invoices" class="btn-sm btn-primary pull-right">Trainer Invoice</a>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Professional Fees</td>
                                        <td>
                                            <?php
                                            $amt = $this->business_model->getBDMTrainerAmount($this->session->userdata('t_code'));
                                            echo number_format(round($amt[0]->amt));
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Debit Note</td>
                                        <td>
                                            <?php
                                            $damt = $this->business_model->getBDMTrainerDebit($this->session->userdata('t_code'));
                                            echo number_format($damt[0]->amt);
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td><?php echo number_format(round($amt[0]->amt) + round($damt[0]->amt)); ?></td>
                                    </tr>

                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <?php
        include 'js_files.php';
        ?>

        <script type="text/javascript">
            $(document).ready(function () {
                $('#clientOption').change(function () {
                    var cid = $(this).val();
                    window.location.href = "<?php echo base_url(); ?>bdm/client_programs/" + cid;
                });
            });
        </script>

    </body>
</html>