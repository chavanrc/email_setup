<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Dashboard</title>
        <?php include 'css_files.php'; ?>
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/demo-topbar.css' rel='stylesheet' />
    </head>
    <body>

        <?php include 'admin_sidemenu.php'; ?>
        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Programs</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#"><i class="fa fa-info"></i> Details</a></li>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3><?php echo $program[0]->project_title; ?></h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>cm/" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a>

                    </div>
                    <div class="clearfix"></div>
                </div>



                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-info"></i> Programs Details </h2>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">Program Title <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->project_title; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Client <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->client_name; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Location <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->location_of_training; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Start Date <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->training_start_date; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Duration <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->training_duration; ?> Days</div>
                            <div class="clearfix"></div>
                            <?php
                            $cmodule = $this->projectmanager_model->get_projectModule($program[0]->project_id, $program[0]->client_id);
                            if(!empty($cmodule)){
                                ?>
                            <div class="col-md-4">Content Module   <span class="pull-right">:</span></div><div class="col-md-8"> <?php echo $cmodule[0]->module; ?> </div>
                            <div class="clearfix"></div>
                                <?php
                            }
                            ?>
                            
                            <div class="col-md-4">Venue  <span class="pull-right">:</span></div><div class="col-md-8"> <?php if (!empty($program[0]->venue)) { ?><?php echo $program[0]->venue; ?> <?php } else { ?><strong>Not Updated</strong> <?php } ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">SPOC <span class="pull-right">:</span></div><div class="col-md-8"><?php if (!empty($program[0]->spoc)) { ?><?php echo $program[0]->spoc; ?><?php } else { ?><strong>Not Updated</strong> <?php } ?></div>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                </div>

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-graduation-cap"></i> Trainers </h2>
                        </div>
                        <div class="panel-body">
                            

                            <?php
                            $tn = $this->projectmanager_model->get_program_trainers($program[0]->project_id);

                            if (!empty($tn)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Trainer</th>
                                            <th>Date From</th>
                                            <th>Date To</th>
                                            <th>Contact</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($tn as $tn_data) {
                                            $ttt = $this->projectmanager_model->get_ttt($tn_data->trainer_id, $program[0]->project_id);
                                            ?>
                                            <tr>
                                                <td><?php echo $tn_data->name; ?></td>
                                                <td><?php echo date_formate_short($tn_data->training_date_from); ?></td>
                                                <td><?php echo date_formate_short($tn_data->trainer_date_to); ?></td>
                                                <td><?php echo $tn_data->email; ?> <br/>
                                                    <?php echo $tn_data->contact_number; ?>
                                                </td>

                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                                if (!empty($ttt)) {
                                    ?>
                                    <table class="table table-bordered">
                                        <tr>

                                            <td style="width:180px;"><strong>TTT Schedule</strong></td>
                                            <td style="width:300px;">
                                                <?php
                                                foreach ($ttt as $tt_data) {
                                                    ?>

                                                    <?php
                                                    if ($tt_data->ttt_status == '1') {
                                                        ?>
                                                        <span style="margin-right:35px"><?php echo date_formate_short($tt_data->ttt_date); ?> &nbsp;&nbsp; <?php echo $tt_data->ttt_time; ?>
                                                            <i class="fa fa-check" aria-hidden="true" style="color:green;"></i>
                                                        </span>
                                                        <?php
                                                    }
                                                }
                                                ?> 
                                            </td>
                                            <td>
                                                <?php
                                                if($tn[0]->ttt_status==0){
                                                ?>
                                                <a href="#" class="btn btn-primary updateBtn" tid="<?php echo $tt_data->trainer_id; ?>" pid="<?php echo $tt_data->project_id; ?>">Update Status</a>
                                                <?php } else {
                                                    ?>
                                                    <strong><i class="fa fa-check" aria-hidden="true" style="color:green;"></i> TTT is Conducted with Trainer!!</strong>
                                                    <?php
                                                } ?>
                                            </td>

                                        </tr>
                                    </table>
                                    <?php
                                }
                            } else {
                                ?>
                            <div>
                                Trainer is not yet assigned.
                            </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list-alt"></i> Contents </h2>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 'Content') {
                                ?>
                                <div class="alert alert-success col-md-6 col-md-offset-3">
                                    Content Added Successfully.
                                </div>
                                <?php
                            }
                            ?>
                            <?php
                            $content = $this->projectmanager_model->get_module_content($program[0]->project_id, $program[0]->client_id);
                            if (!empty($content)) {
                                ?>
                                <h5>Below is list of files required by trainer to conduct the program.</h5>
                                <div class="text-right">
                                    <strong>Status : </strong> <?php
                                    if (!empty($content)) {
                                        if ($content[0]->content_status == '1') {
                                            echo 'Update Closed by CM';
                                        } else {
                                            echo 'Opened by CM for Update';
                                        }
                                    }
                                    ?>
                                    <span style="margin-left:50px; display: inline-block;"><strong>Last Updated :</strong> <?php
                                        if ($content[0]->content_status_date != '0000-00-00 00:00:00') {
                                            echo date_formate($content[0]->content_status_date);
                                        }
                                        ?></span>
                                </div>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Module</th>
                                            <th>Content Title</th>
                                            <th>Note</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $checkHandbook = $this->projectmanager_model->checkHandbookStatus($program[0]->project_id);
                                        if (!empty($content)) {
                                            foreach ($content as $cn_data) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $cn_data->module_name; ?></td>
                                                        <td><?php echo $cn_data->content_title; ?></td>
                                                        <td><?php echo $cn_data->content_notes; ?></td>
                                                        <td>
                                                            <a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $cn_data->content_file; ?>" download="<?php echo $cn_data->content_file; ?>" class="btn-sm btn-primary"><i class="fa fa-download"></i></a>
                                                            <?php
                                                            if($cn_data->content_handbook==1 && $checkHandbook==0){
                                                                ?>
                                                            <a href="#" class="btn-sm btn-success handbookStatus" pid="<?php echo $program[0]->project_id; ?>">Update Status</a>
                                                                <?php
                                                            }
                                                            if($cn_data->content_handbook==1 && $checkHandbook==1){
                                                                echo 'Status updated';
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>

                                <div class="text-center">
                                    <?php
                                    if (!empty($content)) {
                                        $check = $this->projectmanager_model->check_content_send($program[0]->project_id);
                                        if (empty($check)) {
                                            if ($program[0]->diff <= 3) {
                                                ?>
                                                <a href="#" class="btn btn-primary confirm-content-btn" pid="<?php echo $program[0]->project_id; ?>">Confirm & Send to Trainer</a>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <span style="display:inline-block; padding: 5px 10px; border:solid 1px #ddd; border-radius: 2px;" class="btn-sm btn-warning">
                                                Content was sent to trainer on : <?php echo $check[0]->trainer_pass_date; ?>
                                            </span>

                                            <?php
                                            if ($check[0]->content_download_date != '0000-00-00 00:00:00') {
                                                ?>
                                                <span style="margin-left: 50px; display:inline-block; padding: 5px 10px; border:solid 1px #ddd; border-radius: 2px;">
                                                    Trainer Download Date  : <?php echo date_formate($check[0]->content_download_date); ?>
                                                </span>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                                <?php
                            } else {
                                ?>
                                <div>
                                    Content is not yet updated
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="modal fade" id="tttWrap" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <form action="" method="POST">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Update TTT Status</h4>
                        </div>
                        <div class="modal-body">
                            <input type="hidden" name="pid" id="pid"/>
                            <input type="hidden" name="tid" id="tid"/>
                            <textarea class="form-control" name="desc"></textarea>
                        </div>
                        <div class="modal-footer">
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">

            $(document).ready(function () {
                $('.updateBtn').click(function (e) {
                    e.preventDefault();
                    $('#tttWrap').modal('show');
                    var tid = $(this).attr('tid');
                    var pid = $(this).attr('pid');
                    $('#pid').val(pid);
                    $('#tid').val(tid);
                });
                
                $('.handbookStatus').click(function (e) {
                    e.preventDefault();
                    var pid = $(this).attr('pid');
                    $('.page_spin').show();
                    var dataString = "pid=" + pid + "&page=updateHandbookStatus";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>cm/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end 
                });
            });

        </script>
    </body>
</html>