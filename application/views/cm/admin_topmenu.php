<div class="top_nav">
		<span><strong style="color:#FF0000; font-size:18px; margin-left:20px;">CRM SANDBOX</strong></span>
                <div class="nav_menu nav_menu_right">
                    <ul class="nav-menu-list">
                        <li class="nav-menu-list-item" style="padding-right: 40px;">
                            <a href="#" data-toggle="dropdown" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <img src="<?php echo base_url(); ?>assets/upload/cm/<?php echo $this->session->userdata('t_photo'); ?>" class="img-circle" width="30px;">
                                <?php echo $this->session->userdata('t_name'); ?>
                                <span class="fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                <li><a href="<?php echo base_url(); ?>cm/profile"><i class="fa fa-user"></i> Profile </a></li>
                                <li><a href="<?php echo base_url(); ?>admin/bdm_logout"><i class="fa fa-sign-out"></i> Logout </a></li>
                            </ul>
                        </li>

                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>