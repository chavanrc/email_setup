<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Content Manager | Dashboard</title>
        <?php include 'css_files.php'; ?>
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
        <style>
            .margin10{
                margin-top: 10px;
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Search Content</h3>
                    </div>
                    <div class="page-title title-right">
                        <a href="<?php echo base_url(); ?>cm/add_content" class="btn btn-primary pull-right">Add Content</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>


                <div class="col-md-12">
                    <div class="box box-primary">
                        <!--                        <div class="box-header with-border text-center">
                                                    <h4 style="display: inline-block;">Modules</h4>
                                                </div>-->
                        <div class="box-body" style="min-height: 250px;">
                            <form action="" method="POST" id="search-form">
                                <div class="row">
                                    <div class="col-md-6">
                                        <select class="form-control" id="client" name="client">
                                            <option value=""> - Select Client -</option>
                                            <?php
                                            $clients = $CM->cm_model->get_clients();
                                            if (!empty($clients)) {
                                                foreach ($clients as $cl_data) {
                                                    ?>
                                                    <option value="<?php echo $cl_data->client_id; ?>"><?php echo $cl_data->client_name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group load-module">
                                            <select class="form-control" id="modl" name="modl">
                                                <option value=""> - All Module -</option>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="text" class="form-control" id="title" placeholder="Content title" name="title"/>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <select class="form-control" id="ftype" name="ftype">
                                                <option value=""> - All File Type -</option>
                                                <?php
                                                $ftype = $CI->cm_model->get_file_type();
                                                if (!empty($ftype)) {
                                                    foreach ($ftype as $ft_data) {
                                                        ?>
                                                        <option value="<?php echo $ft_data->file_type; ?>"><?php echo $ft_data->file_type; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                    </div>
                                    <div class="col-md-6">
                                        <button type="submit" class="btn btn-primary">Search</button>
                                    </div>

                                </div>
                            </form>


                            <div class="load-content margin10">


                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>

            <?php
            include 'js_files.php';
            //$program = $TR->trainer_model->get_progrom_date($this->session->userdata('t_code'));
            ?>


            <script type="text/javascript">
                $(document).ready(function () {

                    $('#client').change(function (e) {
                        var cid = $(this).val();
                        $('.page_spin').show();
                        var dataString = "cid=" + cid + "&page=load_module";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>cm/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                $('.load-module').html(data);

                            }, //success fun end
                        });//ajax end 
                    });

//                    function checkTitle() {
//                        var title = $('#title').val();
//                        var client = $('#client').val();
//                        if (title == '' && client == '') {
//                            return false;
//                        }
//                    }

                    function search_content() {
                        var title = $('#title').val();
                        var ftype = $('#ftype').val();
                        var modl = $('#modl').val();
                        var client = $('#client').val();
                        $('.page_spin').show();
                        var dataString = "title=" + title + "&ftype=" + ftype + "&client=" + client + "&modl=" + modl + "&page=search_content";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>cm/search_load",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                $('.load-content').html(data);
                                $('[data-toggle="tooltip"]').tooltip();
                            }, //success fun end
                        });//ajax end 
                    }

                    $.validator.addMethod("checkTitle", function (value, element) {
                        var title = $('#title').val();
                        var client = $('#client').val();
                        var modl = $('#modl').val();
                        var ftype = $('#ftype').val();
                        if (title == '' && client == '' && modl=='' && ftype=='') {
                            return false;
                        } else {
                            return true;
                        }
                    }, 'Select any one option OR enter search text');

                    $("#search-form").validate({
                        rules: {
                            title: {
                                checkTitle: true
                            },
                        },
                        tooltip_options: {
                            inst: {
                                trigger: 'focus',
                            },
                        },
                        submitHandler: function (form) {
                            search_content();
                        }
                    });
                    
                    $(document).on('click', '.remove-content', function (e) {
                        e.preventDefault();
                        var cid = $(this).attr('cid');
                        var file = $(this).attr('file');
                        var ele = $(this);
                        var f = confirm("Are you sure want to remove content ?");
                        if (f == true) {
                            $('.page_spin').show();
                            var dataString = "cid=" + cid + "&file=" + file + "&page=remove_content";
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>cm/ajax_page",
                                data: dataString,
                                success: function (data) {
                                    $('.page_spin').hide();

                                    ele.parent("td").parent("tr").remove();
                                }, //success fun end
                            });//ajax end
                        }
                    });

                });
            </script>

    </body>
</html>