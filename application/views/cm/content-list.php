<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Content Manager | Dashboard</title>
        <?php include 'css_files.php'; ?>
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
        <style>
            .margin10{
                margin-top: 10px;
            }
            .in-complete-btn{
                display: none;
            }
            .complete-btn{
                display: none;
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Content Database</h3>
                    </div>
                    <div class="page-title title-right">

                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>


                <div class="col-md-12">
                    <div class="box box-primary">
                        <!--                        <div class="box-header with-border text-center">
                                                    <h4 style="display: inline-block;">Modules</h4>
                                                </div>-->
                        <div class="box-body" style="min-height: 250px;">
                            <div class="row">
                                <div class="col-md-5">
                                    <select class="form-control" id="client" name="client">
                                        <option value=""> - Select Client -</option>
                                        <?php
                                        $clients = $CM->cm_model->get_clients();
                                        $cid = '';
                                        if (isset($_GET['client'])) {
                                            $cid = $_GET['client'];
                                        }
                                        if (!empty($clients)) {
                                            foreach ($clients as $cl_data) {
                                                ?>
                                                <option value="<?php echo $cl_data->client_id; ?>" <?php
                                                if ($cl_data->client_id == $cid) {
                                                    echo 'selected';
                                                }
                                                ?> ><?php echo $cl_data->client_name; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                    </select>
                                </div>

                                <div class="col-md-5">
                                    <div class="form-group load-module">
                                        <select class="form-control" id="modl" name="modl">
                                            <option value=""> - All Module -</option>
                                            <?php
                                            if (isset($_GET['client'])) {
                                                $mid = 0;
                                                if (isset($_GET['modl'])) {
                                                    $mid = $_GET['modl'];
                                                }
                                                $modl = $CI->cm_model->get_client_module($_GET['client']);
                                                if (!empty($modl)) {
                                                    foreach ($modl as $md_data) {
                                                        ?>
                                                        <option value="<?php echo $md_data->cm_id; ?>" <?php
                                                        if ($md_data->cm_id == $mid) {
                                                            echo 'selected';
                                                        }
                                                        ?> ><?php echo $md_data->module_name; ?></option>
                                                                <?php
                                                            }
                                                        }
                                                    }
                                                    ?>
                                        </select>
                                    </div>
                                </div>

                                <div class="col-md-2">
                                    <a href="#" class="btn btn-primary pull-right add-content-btn" style="display:none;">Add Content</a>
                                </div>
                            </div>

                            <?php
                            if (isset($_GET['add'])) {
                                ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="alert alert-success">Content added successfully <a href="add_content?client=<?php echo $cid; ?>&modl=<?php echo $mid; ?>">Add More</a></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                                <?php
                            }
                            if (isset($_GET['up'])) {
                                ?>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="alert alert-success">Content updated successfully <a href="add_content?client=<?php echo $cid; ?>&modl=<?php echo $mid; ?>">Add More</a></div>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                                <?php
                            }
                            
                            ?>

                            <div class="load-content margin10">


                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>

            <?php
            include 'js_files.php';
            //$program = $TR->trainer_model->get_progrom_date($this->session->userdata('t_code'));
            ?>


            <script type="text/javascript">
                $(document).ready(function () {

                    function get_modl(cid) {
                        $('.page_spin').show();
                        $('.add-content-btn').hide();
                        var dataString = "cid=" + cid + "&page=load_module";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>cm/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                $('.load-module').html(data);
                                var length = $('#modl > option').length;
                                if(length==1){
                                    $('.load-content').html('There is no module found for this client. Go to <a href="<?php echo base_url(); ?>cm/client_modules">Content modules </a> to add modules');
                                } else {
                                    $('.load-content').html('');
                                }
                            }, //success fun end
                        });//ajax end
                    }

                    function get_content(cid, mid) {
                        $('.page_spin').show();
                        var dataString = "cid=" + cid + "&modl=" + mid;
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>cm/load_content",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                $('.load-content').html(data);
                                $('.add-content-btn').show();
                                $('[data-toggle="tooltip"]').tooltip();
                            }, //success fun end
                        });//ajax end 
                    }

                    $('#client').change(function (e) {
                        var cid = $(this).val();
                        get_modl(cid);
                    });


                    $(document).on('change', '#modl', function () {
                        var cid = $('#client').val();
                        var modl = $('#modl').val();
                        if (modl == "") {
                            return false;
                        }
                        get_content(cid, modl);

                    });

<?php if (isset($_GET['client']) && isset($_GET['modl'])) {
    ?>
                        get_content(<?php echo $_GET['client']; ?>,<?php echo $_GET['modl']; ?>);
<?php }
?>

                    $(document).on('click', '.remove-content', function (e) {
                        e.preventDefault();
                        var cid = $(this).attr('cid');
                        var file = $(this).attr('file');
                        var ele = $(this);
                        var f = confirm("Are you sure want to remove content ?");
                        if (f == true) {
                            $('.page_spin').show();
                            var dataString = "cid=" + cid + "&file=" + file + "&page=remove_content";
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>cm/ajax_page",
                                data: dataString,
                                success: function (data) {
                                    $('.page_spin').hide();

                                    ele.parent("td").parent("tr").remove();
                                }, //success fun end
                            });//ajax end
                        }
                    });

                    $(document).on('click', '.complete-btn', function (e) {
                        e.preventDefault();
                        var cid = $(this).attr('cid');
                        var ele = $(this);
                        var status = '1';
                        var f = confirm("Are you sure want to close content for update ?");
                        if (f == true) {
                            $('.page_spin').show();
                            var dataString = "cid=" + cid + "&status="+status+"&page=complete_content";
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>cm/ajax_page",
                                data: dataString,
                                success: function (data) {
                                    $('.page_spin').hide();
                                    ele.hide();
                                    alert('Status updated');
                                    window.location.reload();
                                }, //success fun end
                            });//ajax end
                        }
                    });
                    
                    $(document).on('click', '.in-complete-btn', function (e) {
                        e.preventDefault();
                        var cid = $(this).attr('cid');
                        var ele = $(this);
                        var status = '2';
                        var f = confirm("Are you sure want to open content for update ?");
                        if (f == true) {
                            $('.page_spin').show();
                            var dataString = "cid=" + cid + "&status="+status+"&page=complete_content";
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>cm/ajax_page",
                                data: dataString,
                                success: function (data) {
                                    $('.page_spin').hide();
                                    ele.hide();
                                    alert('Status updated');
                                    window.location.reload();
                                }, //success fun end
                            });//ajax end
                        }
                    });

                    $("#add-content-form").validate({
                        rules: {
                            client: "required",
                            modl: "required",
                            title: "required",
                            doc: {
                                required: true,
                                extension: "xls|csv|doc|docx|odt|pdf|xls|xlsx|ppt|pptx|txt"
                            }
                        },
                        tooltip_options: {
                            inst: {
                                trigger: 'focus',
                            },
                        }
                    });

                    $('.add-content-btn').click(function (e) {
                        e.preventDefault();
                        var cid = $('#client').val();
                        var modl = $('#modl').val();
                        if (modl != "" && cid != "") {
                            window.location.href = "<?php echo base_url(); ?>cm/add_content?client=" + cid + "&modl=" + modl;
                        } else {
                            alert('Select module to add content');
                        }
                    });
                    
                    $(document).on('change','.showBtn',function(){
                       var sval = $(this).val();
                       if(sval=='1'){
                           $('.tbox').show();
                       } else {
                           $('.tbox').hide();
                       }
                    });

                });
            </script>

    </body>
</html>