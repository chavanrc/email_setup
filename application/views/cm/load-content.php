<?php
if (!empty($modl)) {
    foreach ($modl as $md_data) {
        $tc = 0;
        $tempContent = array();
        $content = $this->cm_model->get_content($md_data->cm_id);
        if (!empty($content)) {
            $tc = count($content);
            foreach($content as $tc_data){
                if(array_key_exists($tc_data->content_type, $tempContent)){
                    $tempContent[$tc_data->content_type][] = $tc_data;
                } else {
                    $tempContent[$tc_data->content_type] = array();
                    $tempContent[$tc_data->content_type][] = $tc_data;
                }
            }
        }
        ?>
        <div class="panel-group" style="margin-bottom: 10px; background: #fff;" id="accordion<?php echo $md_data->cm_id; ?>" role="tablist" aria-multiselectable="true">
            <div class="panel panel-default">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title" style="display:inline-block;">

                        <?php echo $md_data->module_name; ?> (<?php echo $tc; ?>)

                    </h4>



                    <?php
                    ?>
                    <a href="#" class="btn-sm btn-warning pull-right in-complete-btn" <?php if ($md_data->content_status == '1') { ?> style="display:inline-block;" <?php } ?> cid="<?php echo $md_data->cm_id; ?>">Open Content for Updated</a>
                    <a href="#" class="btn-sm btn-success pull-right complete-btn" <?php if ($md_data->content_status == '2') { ?> style="display:inline-block;" <?php } ?> cid="<?php echo $md_data->cm_id; ?>">Close Content for Updated</a>
                    <?php
                    if ($md_data->content_status == '2') {
                        ?>
                        <span class="pull-right" style="margin-right:30px; font-size:13px;"> Content Opened for Update on <?php echo date_formate($md_data->content_status_date); ?> </span>
                        <?php
                    }
                    if ($md_data->content_status == '1') {
                        ?>
                        <span class="pull-right" style="margin-right:30px; font-size:13px;"> Content Closed for Update on <?php echo date_formate($md_data->content_status_date); ?> </span>
                        <?php
                    }
                    ?>



                    <div class="clearfix"></div>
                </div>
                <div id="collapse<?php echo $md_data->cm_id; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <div>
                            <label class="pull-right" style="margin-right:40px;">
                                <input type="radio" name="showBtn" class="showBtn"  value="0"> Show for Trainer
                            </label>
                            <label class="pull-right" style="margin-right:20px;">
                                <input type="radio" name="showBtn" class="showBtn" checked value="1"> Show All
                            </label>
                            <div class="clearfix"></div>
                        </div>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Note</th>
                                    <th>For Trainer</th>
                                    <th>Last Update</th>
                                    <th style="width:125px;"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if (!empty($tempContent)) {
                                    foreach ($tempContent as $key=>$tp_data) {
                                        
                                        ?>
                                <tr style="background:#f1f1f1;">
                                    <td colspan="5"><strong><?php  echo strtoupper($key); ?></strong></td>
                                </tr>
                                        <?php
                                        foreach($tp_data as $cn_data){
                                            
                                        ?>
                                        <tr <?php if ($cn_data->for_trainer == 0) { ?> class="tbox" <?php } ?> >
                                            <td><?php echo $cn_data->content_title; ?></td>
                                            <td><?php echo $cn_data->content_notes; ?></td>
                                            <td style="text-align:center;"><?php
                                                if ($cn_data->for_trainer == 1) {
                                                    ?>
                                                    <i class="fa fa-check text-success" style="font-size:18px;"></i>
                                                    <?php
                                                }
                                                ?></td>
                                            <td><?php echo date_formate_short($cn_data->last_update); ?></td>
                                            <td>
                                                <?php
                                                $fSize = filesize("./assets/upload/content/" . $cn_data->content_file);
                                                $fSize = round($fSize / 1024, 2);
                                                ?>
                                                <a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $cn_data->content_file; ?>" download="<?php echo $cn_data->content_file; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $cn_data->content_file; ?> <?php echo $fSize; ?> KB" class="btn-sm btn-primary"><i class="fa fa-download"></i></a>
                                                <a href="<?php echo base_url(); ?>cm/edit_content/<?php echo $cn_data->content_id; ?>" class="btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                                <a href="#" class="btn-sm btn-danger remove-content" file="<?php echo $cn_data->content_file; ?>" cid="<?php echo $cn_data->content_id; ?>"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                        }
                                    }
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
} else {
    echo 'No content found';
}
?>
