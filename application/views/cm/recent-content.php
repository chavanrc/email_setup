<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Content Manager | Dashboard</title>
        <?php include 'css_files.php'; ?>
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
        <style>
            .margin10{
                margin-top: 10px;
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Recently Added Content</h3>
                    </div>
                    <div class="page-title title-right">
                        <a href="<?php echo base_url(); ?>cm/add_content" class="btn btn-primary pull-right">Add Content</a>
                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>


                <div class="col-md-12">
                    <div class="box box-primary">
                        <div class="box-body" style="min-height: 250px;">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th style="width:300px;">Client & Module</th>
                                        <th>Title</th>
                                        <th>Note</th>
                                        <th>For Trainer</th>
                                        <th>Last Update</th>
                                        <th style="width:125px;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($content)) {
                                        foreach ($content as $cn_data) {
                                            ?>
                                            <tr>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>cm/content_database?client=<?php echo $cn_data->client_id; ?>"><?php echo $cn_data->client_name; ?></a> <br/> 
                                                    <a href="<?php echo base_url(); ?>cm/content_database?client=<?php echo $cn_data->client_id; ?>&modl=<?php echo $cn_data->cm_id; ?>"><?php echo $cn_data->module_name; ?></a>
                                                </td>
                                                <td><?php echo $cn_data->content_title; ?></td>
                                                <td><?php echo $cn_data->content_notes; ?></td>
                                                <td style="text-align:center;"><?php
                                                    if ($cn_data->for_trainer == 1) {
                                                        ?>
                                                        <i class="fa fa-check text-success" style="font-size:18px;"></i>
                                                        <?php
                                                    }
                                                    ?></td>
                                                <td><?php echo date_formate_short($cn_data->last_update); ?></td>
                                                <td>
                                                    <?php 
                                                    $fSize = filesize("./assets/upload/content/".$cn_data->content_file);
                                                    $fSize = round($fSize/1024,2);
                                                    ?>
                                                    <a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $cn_data->content_file; ?>" download="<?php echo $cn_data->content_file; ?>" class="btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="<?php echo $cn_data->content_file; ?> <?php echo $fSize; ?> KB" ><i class="fa fa-download"></i></a>
                                                    <a href="<?php echo base_url(); ?>cm/edit_content/<?php echo $cn_data->content_id; ?>" class="btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                                                    <a href="#" class="btn-sm btn-danger remove-content" file="<?php echo $cn_data->content_file; ?>" cid="<?php echo $cn_data->content_id; ?>"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>

            <?php
            include 'js_files.php';
            //$program = $TR->trainer_model->get_progrom_date($this->session->userdata('t_code'));
            ?>


            <script type="text/javascript">
                $(document).ready(function () {
                    
                    $('[data-toggle="tooltip"]').tooltip();

                    $(document).on('click', '.remove-content', function (e) {
                        e.preventDefault();
                        var cid = $(this).attr('cid');
                        var file = $(this).attr('file');
                        var ele = $(this);
                        var f = confirm("Are you sure want to remove content ?");
                        if (f == true) {
                            $('.page_spin').show();
                            var dataString = "cid=" + cid + "&file=" + file + "&page=remove_content";
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>cm/ajax_page",
                                data: dataString,
                                success: function (data) {
                                    $('.page_spin').hide();

                                    ele.parent("td").parent("tr").remove();
                                }, //success fun end
                            });//ajax end
                        }
                    });

                    $(document).on('click', '.complete-btn', function (e) {
                        e.preventDefault();
                        var cid = $(this).attr('cid');
                        var ele = $(this);
                        var f = confirm("Are you sure want to complete content udpate ?");
                        if (f == true) {
                            $('.page_spin').show();
                            var dataString = "cid=" + cid + "&page=complete_content";
                            $.ajax({
                                type: "POST",
                                url: "<?php echo base_url(); ?>cm/ajax_page",
                                data: dataString,
                                success: function (data) {
                                    $('.page_spin').hide();
                                    ele.remove();
                                }, //success fun end
                            });//ajax end
                        }
                    });

                    $("#add-content-form").validate({
                        rules: {
                            client: "required",
                            modl: "required",
                            title: "required",
                            doc: {
                                required: true,
                                extension: "xls|csv|doc|docx|odt|pdf|xls|xlsx|ppt|pptx|txt"
                            }
                        },
                        tooltip_options: {
                            inst: {
                                trigger: 'focus',
                            },
                        }
                    });

                });
            </script>

    </body>
</html>