<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Content Manager | Dashboard</title>
        <?php include 'css_files.php'; ?>
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
        <style>
            .margin10{
                margin-top: 10px;
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Add Content</h3>
                    </div>
                    <div class="page-title title-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>


                <div class="col-md-6">
                    <?php
                    if ($msg == 2) {
                        ?>
                        <div class="alert alert-danger">Error !! file not exist in server.</div>
                        <?php
                    }
                    ?>
                    <div class="box box-primary">
                        <!--                        <div class="box-header with-border text-center">
                                                    <h4 style="display: inline-block;">Modules</h4>
                                                </div>-->
                        <div class="box-body">
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-content-form">
                                <select class="form-control" id="client" name="client">
                                    <option value=""> - Select Client -</option>
                                    <?php
                                    $cid = '';
                                    if (isset($_GET['client'])) {
                                        $cid = $_GET['client'];
                                    }
                                    $clients = $CM->cm_model->get_clients();
                                    if (!empty($clients)) {
                                        foreach ($clients as $cl_data) {
                                            ?>
                                            <option value="<?php echo $cl_data->client_id; ?>" <?php
                                            if ($cl_data->client_id == $cid) {
                                                echo 'selected';
                                            }
                                            ?> ><?php echo $cl_data->client_name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                </select>
                                <div class="form-group margin10 load-module">
                                    <select class="form-control" id="modl" name="modl">
                                        <option value=""> - Select Module -</option>
                                        <?php
                                        if (isset($_GET['client'])) {
                                            $mid = 0;
                                            if (isset($_GET['modl'])) {
                                                $mid = $_GET['modl'];
                                            }
                                            $modl = $CI->cm_model->get_client_module($_GET['client']);
                                            if (!empty($modl)) {
                                                foreach ($modl as $md_data) {
                                                    ?>
                                                    <option value="<?php echo $md_data->cm_id; ?>" <?php
                                                    if ($md_data->cm_id == $mid) {
                                                        echo 'selected';
                                                    }
                                                    ?> ><?php echo $md_data->module_name; ?></option>
                                                            <?php
                                                        }
                                                    }
                                                }
                                                ?>
                                    </select>
                                </div>

                                <div class="form-group margin10">
                                    <input type="text" class="form-control" placeholder="Title" name="title"/>
                                </div>

                                <div class="form-group margin10">
                                    <textarea class="form-control" placeholder="Note" name="note"></textarea>
                                </div>
                                
                                <div class="form-group margin10">
                                    <label style="display:inline-block;">
                                        <input type="radio" name="upType" class="upType" checked value="1"/> Upload Now
                                    </label>

                                    <label style="display:inline-block; margin-left: 20px;">
                                        <input type="radio" name="upType" class="upType" value="0"/> Pick it from Server
                                    </label>
                                </div>

                                <div class="form-group margin10 docType">
                                    <input type="file" class="form-control" name="doc[]" multiple="multiple"/>
                                </div>
                                
                                <div class="form-group margin10 nameType" style="display:none;">
                                    <input type="text" class="form-control" placeholder="File Name" name="fName"/>
                                </div>

                                <div class="form-group margin10">
                                    Available for Trainer : <label style="display:inline-block;">
                                        <input type="radio" name="trainerStatus" value="1"/> Yes
                                    </label>

                                    <label style="display:inline-block; margin-left: 20px;">
                                        <input type="radio" name="trainerStatus" checked value="0"/> No
                                    </label>
                                </div>
                                
                                <div class="form-group margin10">
                                    <label style="display:inline-block;">
                                        <input type="checkbox" name="handbook" value="1"/> Participants Handbook
                                    </label>

                                </div>

                                <div class="form-group margin10 text-right">
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>

        <?php
        include 'js_files.php';
//$program = $TR->trainer_model->get_progrom_date($this->session->userdata('t_code'));
        ?>


        <script type="text/javascript">
            $(document).ready(function () {
                
                $('.upType').change(function(e){
                   var uptype = $(this).val();
                   if(uptype==1){
                       $('.docType').show();
                       $('.nameType').hide();
                   } else {
                       $('.docType').hide();
                       $('.nameType').show();
                   }
                });

                $('#client').change(function (e) {
                    var cid = $(this).val();
                    $('.page_spin').show();
                    var dataString = "cid=" + cid + "&page=load_module";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>cm/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            $('.load-module').html(data);

                        }, //success fun end
                    });//ajax end 
                });

                $("#add-content-form").validate({
                    rules: {
                        client: "required",
                        modl: "required",
                        title: "required",
                        doc: {
                            required: true,
                            extension: "xls|csv|doc|docx|odt|pdf|xls|xlsx|ppt|pptx|txt|mp4|mp3|wav|mov|wmv|flv|jpg|png|jpeg|gif"
                        }
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        $('.page_spin').show();
                        form.submit();
                    }
                });

            });
        </script>

    </body>
</html>