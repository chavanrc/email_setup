<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Content Manager | Dashboard</title>
        <?php include 'css_files.php'; ?>
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
        <style>
            .margin10{
                margin-top: 10px;
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Edit Content</h3>
                    </div>
                    <div class="page-title title-right">
                        <a href="<?php echo base_url(); ?>cm/content_database" class="btn btn-danger pull-right">Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>


                <div class="col-md-6">
                    <?php
                    if ($msg == 2) {
                        ?>
                        <div class="alert alert-danger">Error !! file not exist in server.</div>
                        <?php
                    }
                    ?>
                    <div class="box box-primary">
                        <!--                        <div class="box-header with-border text-center">
                                                    <h4 style="display: inline-block;">Modules</h4>
                                                </div>-->
                        <div class="box-body">
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-content-form">
                                <select class="form-control" id="client" name="client">
                                    <option value=""> - Select Client -</option>
                                    <?php
                                    $clients = $CM->cm_model->get_clients();
                                    if (!empty($clients)) {
                                        foreach ($clients as $cl_data) {
                                            ?>
                                            <option value="<?php echo $cl_data->client_id; ?>" <?php
                                            if ($content[0]->client_id == $cl_data->client_id) {
                                                echo 'selected';
                                            }
                                            ?> ><?php echo $cl_data->client_name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                </select>
                                <div class="form-group margin10 load-module">
                                    <select class="form-control" id="modl" name="modl">
                                        <option value=""> - Select Module -</option>
                                        <?php
                                        $modle = $CM->cm_model->get_client_module($content[0]->client_id);
                                        ?>
                                        <?php
                                        if (!empty($modle)) {
                                            foreach ($modle as $rs_data) {
                                                ?>
                                                <option value="<?php echo $rs_data->cm_id; ?>" <?php
                                            if ($content[0]->module_id == $rs_data->cm_id) {
                                                echo 'selected';
                                            }
                                            ?> ><?php echo $rs_data->module_name; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="form-group margin10">
                                    <input type="text" class="form-control" value="<?php echo $content[0]->content_title; ?>" placeholder="Title" name="title"/>
                                </div>

                                <div class="form-group margin10">
                                    <textarea class="form-control" placeholder="Note" name="note"><?php echo $content[0]->content_notes; ?></textarea>
                                </div>
                                
                                <div class="form-group margin10">
                                    <label style="display:inline-block;">
                                        <input type="radio" name="upType" class="upType" <?php if($content[0]->file_upType==1){ ?> checked <?php } ?> value="1"/> Upload Now
                                    </label>

                                    <label style="display:inline-block; margin-left: 20px;">
                                        <input type="radio" name="upType" class="upType" <?php if($content[0]->file_upType==0){ ?> checked <?php } ?> value="0"/> Pick it from Server
                                    </label>
                                </div>

                                <div class="form-group margin10 docType" <?php if($content[0]->file_upType==0){ ?> style="display: none;" <?php } ?>>
                                    <input type="file" class="form-control" name="doc"/>
                                </div>
                                
                                <div class="form-group margin10 nameType" <?php if($content[0]->file_upType==1){ ?> style="display: none;" <?php } ?>>
                                    <input type="text" class="form-control" placeholder="File Name" value="<?php echo $content[0]->content_file; ?>" name="fName"/>
                                </div>

                                <div class="form-group margin10">
                                    Available for Trainer : <label style="display:inline-block;">
                                        <input type="radio" name="trainerStatus" <?php if($content[0]->for_trainer==1){ echo 'checked'; } ?> value="1"/> Yes
                                    </label>

                                    <label style="display:inline-block; margin-left: 20px;">
                                        <input type="radio" name="trainerStatus" <?php if($content[0]->for_trainer==0){ echo 'checked'; } ?> value="0"/> No
                                    </label>
                                </div>

                                <div class="form-group margin10 text-right">
                                    <input type="hidden" name="cid" value="<?php echo $content[0]->content_id; ?>"/>
                                    <input type="hidden" name="cdoc" value="<?php echo $content[0]->content_file; ?>"/>
                                    <button class="btn btn-primary" type="submit">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

                <div class="clearfix"></div>
            </div>
        </div>

        <?php
        include 'js_files.php';
        //$program = $TR->trainer_model->get_progrom_date($this->session->userdata('t_code'));
        ?>


        <script type="text/javascript">
            $(document).ready(function () {
                
                $('.upType').change(function(e){
                   var uptype = $(this).val();
                   if(uptype==1){
                       $('.docType').show();
                       $('.nameType').hide();
                   } else {
                       $('.docType').hide();
                       $('.nameType').show();
                   }
                });

                $('#client').change(function (e) {
                    var cid = $(this).val();
                    $('.page_spin').show();
                    var dataString = "cid=" + cid + "&page=load_module";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>cm/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            $('.load-module').html(data);

                        }, //success fun end
                    });//ajax end 
                });

                $("#add-content-form").validate({
                    rules: {
                        client: "required",
                        modl: "required",
                        title: "required",
                        doc: {
                            extension: "xls|csv|doc|docx|odt|pdf|xls|xlsx|ppt|pptx|txt|mp4|mp3|wav|mov|wmv|flv"
                        }
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        $('.page_spin').show();
                        form.submit();
                    }
                });

            });
        </script>

    </body>
</html>