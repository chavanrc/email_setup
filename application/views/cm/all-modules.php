<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Content Manager | Dashboard</title>
        <?php include 'css_files.php'; ?>
        <link href='https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' />
        <style>
            .margin10{
                margin-top: 10px;
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3 style="display:inline-block;">Content modules</h3>


                        <div class="clearfix"></div>
                    </div>
                    <div class="page-title title-right">

                        <div class="clearfix"></div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-6">
                    <select class="form-control pull-right" id="client" name="client">
                        <option value=""> - Select Client -</option>
                        <?php
                        $clients = $CM->cm_model->get_clients();
                        if (!empty($clients)) {
                            foreach ($clients as $cl_data) {
                                ?>
                                <option value="<?php echo $cl_data->client_id; ?>" <?php
                                if ($cid == $cl_data->client_id) {
                                    echo 'selected';
                                }
                                ?> ><?php echo $cl_data->client_name; ?></option>
                                        <?php
                                    }
                                }
                                ?>
                    </select>
                </div>
                <div class="col-md-6">
                    <a href="" class="btn btn-primary pull-right add-module-btn" style="<?php if ($cid == '') { ?>display:none; <?php } ?>">Add Module</a>
                </div>
                <div class="clearfix"></div>
                <?php
                if ($msg == '1') {
                    ?>
                    <div class="col-md-6 col-md-offset-3" style="margin-top:15px;">
                        <div class="alert alert-success">New Module added</div>
                    </div>
                    <?php
                }
                if ($msg == '2') {
                    ?>
                    <div class="col-md-6 col-md-offset-3" style="margin-top:15px;">
                        <div class="alert alert-success">Module updated</div>
                    </div>
                    <?php
                }
                ?>
                <div class="clearfix"></div>

                <div class="col-md-12" style="margin-top:15px;">
                    <div class="box box-primary">
                        <div class="box-body">
                            <table class="table table-bordered table-striped" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Module</th>
                                        <th>Status</th>
                                        <th>Status Last updated</th>
                                        <th style="width:170px;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($modl)) {
                                        $no = 0;
                                        foreach ($modl as $cn_data) {
                                            $no++;
                                            ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $cn_data->module_name; ?></td>
                                                <td>
                                                    <?php
                                                    if ($cn_data->content_status == '1') {
                                                        echo 'Closed for update';
                                                    }
                                                    if ($cn_data->content_status == '2') {
                                                        echo 'Open for update';
                                                    }
                                                    if ($cn_data->content_status == '0') {
                                                        echo 'No files';
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($cn_data->content_status != '0' && $cn_data->content_status_date != '0000-00-00 00:00:00') {
                                                        echo date_formate($cn_data->content_status_date);
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <a href="#" class="btn-sm btn-primary edit-btn" ctitle="<?php echo $cn_data->module_name; ?>" cmid="<?php echo $cn_data->cm_id; ?>"><i class="fa fa-pencil"></i></a>
                                                    <a href="<?php echo base_url(); ?>cm/content_database?client=<?php echo $cn_data->client_id; ?>&modl=<?php echo $cn_data->cm_id; ?>" class="btn-sm btn-info">View Content</a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="5">No records found</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                </div>
            </div>

            <div class="modal fade" id="add-module-wraper" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Add new content module</h4>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST"  enctype="multipart/form-data" id="assign-pm-form">
                                <input type="hidden" value="<?php echo $cid; ?>" id="cid" name="cid"/>

                                <div class="form-group col-md-8 col-md-offset-2">
                                    <input type="text" class="form-control" placeholder="Module Title" name="modl">
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8 col-md-offset-2 text-center">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            
            <div class="modal fade" id="edit-module-wraper" data-backdrop="static" data-keyboard="false">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Edit content module</h4>
                        </div>
                        <div class="modal-body">
                            <form action="" method="POST"  enctype="multipart/form-data" id="assign-pm-form">
                                <input type="hidden" value="" id="cmid" name="cmid"/>

                                <div class="form-group col-md-8 col-md-offset-2">
                                    <input type="text" class="form-control" placeholder="Module Title" id="cmodl" name="modl">
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-8 col-md-offset-2 text-center">
                                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </div>
                                <div class="clearfix"></div>
                            </form>
                        </div>
                    </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->

            <?php
            include 'js_files.php';
            //$program = $TR->trainer_model->get_progrom_date($this->session->userdata('t_code'));
            ?>

            <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
            <script type="text/javascript">
                $(document).ready(function () {

                    //$('#data-list').DataTable();

                    //$('[data-toggle="tooltip"]').tooltip();
                    
                    $('.edit-btn').click(function (e) {
                        e.preventDefault();
                        var title = $(this).attr('ctitle');
                        var cid = $(this).attr('cmid');
                        $('#cmid').val(cid);
                        $('#cmodl').val(title);
                        $('#edit-module-wraper').modal('show');
                    });

                    $('.add-module-btn').click(function (e) {
                        e.preventDefault();
                        $('#add-module-wraper').modal('show');
                    });

                    $('#client').change(function (e) {
                        var cid = $(this).val();
                        window.location.href = "<?php echo base_url(); ?>cm/client_modules/" + cid;
                    });

                    $("#assign-pm-form").validate({
                        rules: {
                            modl: "required"
                        },
                        tooltip_options: {
                            inst: {
                                trigger: 'focus',
                            },
                        }
                    });

                });
            </script>

    </body>
</html>