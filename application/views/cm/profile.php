<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Project Manager | Profile</title>
        <?php include 'css_files.php'; ?>
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
        <style>
            .panel-body i{
                display: inline-block;
                width: 20px;
            }

            .top-label{
                padding:10px; font-size: 12px; border:solid 1px #73879C;
                margin-left: -10px;
                color:#fff;
            }

        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>

            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3> <?php echo $cm[0]->name; ?></h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" style="display:inline-block"><i class="fa fa-info"></i> Details </h2>
                                <a href="#" class="btn-sm btn-primary editBtn pull-right"><i class="fa fa-edit"></i> Edit</a>
                            </div>
                            <div class="panel-body" style="line-height: 25px; min-height: 200px;">
                                <img src="<?php echo base_url(); ?>assets/upload/trainer/<?php echo $cm[0]->user_photo; ?>" style="width:150px;padding: 5px; border: solid 1px #ddd; position: absolute; right: 20px;">
                                <i class="fa fa-user"></i> <?php echo $cm[0]->name; ?><br/>
                                <i class="fa fa-envelope"></i> <?php echo $cm[0]->email; ?><br/>
                                <i class="fa fa-mobile-phone"></i> <?php echo $cm[0]->contact_number; ?><br/>

                            </div>
                        </div>

                    </div>

                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-shield"></i> Change Password </h2>
                            </div>
                            <div class="panel-body" style="line-height: 25px;">
                                <form action="" id="password-form" method="POST">
                                    <div class="alert alert-danger pass-error" style="display:none;">
                                        Current Password is wrong.
                                    </div>
                                    <div class="alert alert-success pass-success" style="display:none;">
                                        Password updated successfully.
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"> Current Password <sup>*</sup></span>
                                            <input type="password" id="old_password" name="old_password" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"> New Password <sup>*</sup></span>
                                            <input type="password" id="new_password" name="new_password" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group text-right">
                                        <button class="btn btn-info" type="submit"><i class="fa fa-send"></i> Update</button>
                                    </div>
                                </form>
                            </div>
                        </div>

                    </div>



                    <div class="clearfix"></div>


                </div>
            </div>
        </div>
        
        <div class="modal fade" id="editForm" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Update Profile</h4>
                    </div>
                    <div class="modal-body">
                        <form action="<?php echo base_url(); ?>admin/edit_user" enctype="multipart/form-data" id="edit-form" method="POST">
                            <input type="hidden" value="<?php echo $cm[0]->user_id; ?>" name="uid"/>
                            <input type="hidden" value="<?php echo $cm[0]->user_photo; ?>" name="cimg"/>
                            <input type="hidden" value="cm/profile" name="callback"/>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Name <sup>*</sup></span>
                                    <input type="text" id="fname" name="fname" value="<?php echo $cm[0]->name; ?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Contact No <sup>*</sup></span>
                                    <input type="number" id="contact" name="contact" value="<?php echo $cm[0]->contact_number; ?>" class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <div>
                                    <span>Photo</span>
                                    <input type="file" id="contact" name="photo" accept="image/*" class="form-control">
                                </div>
                            </div>
                            <div class="form-group text-right">
                                <button class="btn btn-info" type="submit">Update</button>
                            </div>
                        </form>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <?php include 'js_files.php'; ?>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/moment.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/jquery.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.js'></script>
        <script type="text/javascript">
            $(document).ready(function () {
                
                $('.editBtn').click(function(e){
                   e.preventDefault();
                   $('#editForm').modal('show');
                });
                
                $("#edit-form").validate({
                    rules: {
                        fname: "required",
                        contact: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        $('.page_spin').show();
                        form.submit();
                    }
                });
                
                $("#password-form").validate({
                    rules: {
                        old_password: "required",
                        new_password: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        change_password();
                    }
                });
                
                $('#old_password, #new_password').focus(function(){
                   $('.pass-error').hide(); 
                   $('.pass-success').hide(); 
                });
                
                function change_password()
                {
                    var old_pass = $('#old_password').val();
                    var new_pass = $('#new_password').val();
                    var uid = '<?php echo $this->session->userdata('t_code'); ?>';
                    
                    $('.page_spin').show();
                    var dataString = "old_pass=" + old_pass + "&new_pass=" + new_pass + "&uid="+uid+"&page=change_password";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            var replay = $.trim(data);
                            if(replay=='1')
                            {
                                $('.pass-success').show();
                            }
                            else{
                                $('.pass-error').show();
                            }

                        }, //success fun end
                    });//ajax end
                }
                
            });
        </script>

    </body>
</html>