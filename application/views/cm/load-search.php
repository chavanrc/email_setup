<table class="table table-bordered">
    <thead>
        <tr>
            <th>Client & Module</th>
            <th>Title</th>
            <th>Notes</th>
            <th>For Trainer</th>
            <th>Last updated</th>
            <th style="width:125px;"></th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($search)) {
            foreach ($search as $sr_data) {
                ?>
                <tr>
                    <td>
                        <a href="<?php echo base_url(); ?>cm/content_database?client=<?php echo $sr_data->client_id; ?>"><?php echo $sr_data->client_name; ?></a> <br/> 
                        <a href="<?php echo base_url(); ?>cm/content_database?client=<?php echo $sr_data->client_id; ?>&modl=<?php echo $sr_data->cm_id; ?>"><?php echo $sr_data->module_name; ?></a>
                    </td>
                    <td><?php echo $sr_data->content_title; ?></td>
                    <td><?php echo $sr_data->content_notes; ?></td>
                    <td style="text-align:center;"><?php
                        if ($sr_data->for_trainer == 1) {
                            ?>
                            <i class="fa fa-check text-success" style="font-size:18px;"></i>
                            <?php
                        }
                        ?></td>
                    <td><?php echo date_formate_short($sr_data->last_update); ?></td>
                    <td>
                        <?php
                        $fSize = filesize("./assets/upload/content/" . $sr_data->content_file);
                        $fSize = round($fSize / 1024, 2);
                        ?>
                        <a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $sr_data->content_file; ?>" download="<?php echo $sr_data->content_file; ?>" data-toggle="tooltip" data-placement="top" title="<?php echo $sr_data->content_file; ?> <?php echo $fSize; ?> KB" class="btn-sm btn-primary"><i class="fa fa-download"></i></a>
                        <a href="<?php echo base_url(); ?>cm/edit_content/<?php echo $sr_data->content_id; ?>" class="btn-sm btn-info"><i class="fa fa-pencil"></i></a>
                        <a href="#" class="btn-sm btn-danger remove-content" file="<?php echo $sr_data->content_file; ?>" cid="<?php echo $sr_data->content_id; ?>"><i class="fa fa-trash"></i></a>
                    </td>
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="4">
                    No content found
                </td>
            </tr>
            <?php
        }
        ?>
    </tbody>
</table>