<?php
$CI = & get_instance();
$CI->load->model('admin_model');

$CM = & get_instance();
$CM->load->model('cm_model');
?>
<div class="nav-side-menu">
    <div class="brand"><img src="<?php echo base_url(); ?>assets/images/wagons-logo.png"/></div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

    <div class="menu-list">

        <ul id="menu-content" class="menu-content collapse out">
            <li <?php if ($page_url == 'Dashboard') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>cm/">
                    <i class="fa fa-dashboard"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>cm/content_database">
                    Content Database
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>cm/recent_content">
                    Recently Added
                </a>
            </li>
            <li>
                <a href="<?php echo base_url(); ?>cm/search_content">
                    Search Content
                </a>
            </li>

            <li>
                <a href="<?php echo base_url(); ?>cm/add_content">
                    Add Content
                </a>
            </li>
            
            <li>
                <a href="<?php echo base_url(); ?>cm/client_modules">
                    Content Module
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="page_spin">
    <br/>
    <div class="spin_icon">
        <i class="fa fa-spinner fa-spin"></i><br/>
        <span>One moment ...</span>
    </div>
</div>
