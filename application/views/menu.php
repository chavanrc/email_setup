<div id="nav-top-menu" class="nav-top-menu">
    <div class="container">
        <div class="row">
            <div class="col-sm-3" id="box-vertical-megamenus">
                <div class="box-vertical-megamenus">
                    <h4 class="title">
                        <span class="title-menu">Categories</span>
                        <span class="btn-open-mobile pull-right home-page"><i class="fa fa-bars"></i></span>
                    </h4>
                    <div class="vertical-menu-content is-home">
                        <ul class="vertical-menu-list">

                            <?php
                            $side_no = 0;
                            foreach ($cat as $nc_data) {
                                $side_no++;
                                if($side_no>5)
                                {
                                ?>
                                <li>
                                    <a class="parent" href="#"> <?php echo $nc_data->c_title; ?> </a>
                                    <?php
                                    $sub_cat = $CI->home_model->sub_category($nc_data->c_id);
                                    if (!empty($sub_cat)) {
                                        ?>
                                        <div class="vertical-dropdown-menu">
                                            <div class="vertical-groups col-sm-12">
                                                <div class="mega-group col-sm-12">

                                                    <ul>
                                                        <?php
                                                        foreach($sub_cat as $scn_data)
                                                        {
                                                            ?>
                                                            <li><a href="<?php echo base_url(); ?>products/category/<?php echo $scn_data->c_id; ?>?name=<?php echo $nc_data->c_title; ?>"><?php echo $scn_data->sc_title; ?></a></li>
                                                            <?php
                                                        }
                                                        ?>
                                                        
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>

                                        <?php
                                    }
                                    ?>
                                </li>
                                <?php
                                }
                            }
                            ?>









                        </ul>



                    </div>
                </div>
            </div>
            <div id="main-menu" class="col-sm-9 main-menu">
                <nav class="navbar navbar-default">
                    <div class="container-fluid">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                                <i class="fa fa-bars"></i>
                            </button>
                            <a class="navbar-brand" href="#">MENU</a>
                        </div>
                        <div id="navbar" class="navbar-collapse collapse">
                            <ul class="nav navbar-nav">
                                <li class="active"><a href="<?php echo base_url(); ?>">Home</a></li>
                                <?php
                                $cat_no = 0;
                                foreach ($cat as $c_data) {
                                    $cat_no++;
                                    if($cat_no<6)
                                    {
                                    ?>
                                    <li class="dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><?php echo $c_data->c_title; ?></a>
                                        <?php
                                        $sub = $CI->home_model->sub_category($c_data->c_id);
                                        if (!empty($sub)) {
                                            ?>
                                            <ul class="mega_dropdown dropdown-menu">

                                                <li class="block-container col-sm-4">
                                                    <ul class="block">
                                                        <?php
                                                        foreach ($sub as $sc_data) {
                                                            ?>
                                                            <li class="link_container">
                                                                <a href="<?php echo base_url(); ?>products/category/<?php echo $sc_data->c_id; ?>?name=<?php echo $c_data->c_title; ?>"><?php echo $sc_data->sc_title; ?></a>
                                                            </li>
                                                            <?php
                                                        }
                                                        ?>

                                                    </ul>
                                                </li>
                                            </ul>
                                            <?php
                                        }
                                        ?>

                                    </li>
                                    <?php
                                    }
                                }
                                ?>
                                <li><a href="<?php echo base_url(); ?>home/vendors">Vendors</a></li>
                            </ul>
                        </div><!--/.nav-collapse -->
                    </div>
                </nav>
            </div>
        </div>
        <!-- userinfo on top-->
        <div id="form-search-opntop">
        </div>
        <!-- userinfo on top-->
        <div id="user-info-opntop">
        </div>
        <!-- CART ICON ON MMENU -->
        <div id="shopping-cart-box-ontop">
            <i class="fa fa-shopping-cart"></i>
            <div class="shopping-cart-box-ontop-content"></div>
        </div>
    </div>
</div>
</div>

<div class="cart_load">
    <div class="cart_load_box">
        <br/>
        <div class="cart_qnt_load cart_count">
            <?php echo $cart_no; ?>
        </div>
    </div>
</div>