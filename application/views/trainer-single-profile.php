<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Trainer Profile</title>
        <?php include 'css_files.php'; ?>
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
        <style>
            .panel-body i{
                display: inline-block;
                width: 20px;
            }

            .top-label{
                padding:10px; font-size: 12px; border:solid 1px #73879C;
                margin-left: -10px;
                color:#fff;
            }

        </style>
    </head>
    <body>
        <?php
        $CI = & get_instance();
        $CI->load->model('admin_model');
        ?>

        <div class="col-md-8 col-md-offset-2">
            <nav class="navbar navbar-default" style="background:#929292;">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <img src="http://localhost/Wagons/assets/images/wagons-logo1.png">
                        <span style="font-size:24px; margin-left: 50px; color: #fff; display: inline-block; margin-top: 7px;">Trainer</span>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    
                </div><!-- /.container-fluid -->
            </nav>

            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>  <?php echo $trainer[0]->name; ?></h3>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-info"></i> Basic Details </h2>
                            </div>
                            <div class="panel-body" style="line-height: 25px;">
                                <img src="<?php echo base_url(); ?>assets/upload/trainer/<?php echo $trainer[0]->user_photo; ?>" style="width:150px;padding: 5px; border: solid 1px #ddd; position: absolute; right: 20px;">
                                <i class="fa fa-user"></i> <?php echo $trainer[0]->name; ?><br/>
                                <i class="glyphicon glyphicon-blackboard"></i> <?php echo $trainer[0]->trainer_area; ?><br/>
                                <i class="fa fa-envelope"></i> <?php echo $trainer[0]->email; ?><br/>
                                <i class="fa fa-mobile-phone"></i> <?php echo $trainer[0]->contact_number; ?><br/>
                                <i class="fa fa-birthday-cake"></i> <?php echo $trainer[0]->user_dob; ?><br/>
                                <i class="fa fa-venus-mars"></i> <?php echo $trainer[0]->user_gender; ?><br/>
                                <i class="fa fa-map-marker"></i> <?php echo $trainer[0]->country; ?><br/>
                                <i class="fa fa-map-pin"></i> <?php echo $trainer[0]->state; ?> , <?php echo $trainer[0]->city; ?><br/>
                                <i class="fa fa-map-signs"></i> <?php echo $trainer[0]->address; ?><br/>
                                <i class="fa fa-phone"></i> <?php echo $trainer[0]->contact_number_landline; ?><br/>
                                <i class="fa fa-skype"></i> <?php echo $trainer[0]->skypeID; ?><br/>
                                <i class="fa fa-credit-card"></i> <?php echo $trainer[0]->pan_no; ?><br/>
                                Industry :  <?php echo $trainer[0]->industry; ?><br/>
                                Freelancer :  <?php
                                if ($trainer[0]->freelancer == 'Y') {
                                    echo 'Yes';
                                }
                                if ($trainer[0]->freelancer == 'N') {
                                    echo 'No';
                                }
                                ?><br/>
                                Expected Fees P/D :  <?php echo $trainer[0]->expected_fee_per_day; ?><br/>
                            </div>
                        </div>
                        <?php
                        if (!empty($trainer[0]->user_cv)) {
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 class="panel-title" ><i class="fa fa-info"></i> Trainer CV / Profile </h2>
                                </div>
                                <div class="panel-body" style="line-height: 25px;">
                                    <a href="<?php echo base_url(); ?><?php echo $trainer[0]->user_cv; ?>" class="btn btn-info btn-block">Download <i class="fa fa-download"></i></a>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>

                    <!--                    <div class="col-md-6">
                                            <div class="panel panel-default">
                                                <div class="panel-heading">
                                                    <h2 class="panel-title" ><i class="fa fa-calendar"></i> Trainer Calendar </h2>
                                                </div>
                                                <div class="panel-body" style="line-height: 25px;">
                                                    <div id='calendar'></div>
                                                </div>
                                            </div>
                    
                    
                                        </div>-->
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-info"></i> Training Experience </h2>
                        </div>
                        <div class="panel-body" style="line-height: 25px;">
                            <?php
                            if (!empty($exp)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Program</th>
                                            <th>Training Area</th>

                                            <th>Industry</th>
                                            <th>Company</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($exp as $ex_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $ex_data->program_title; ?></td>
                                                <td><?php echo $ex_data->area_of_training; ?></td>

                                                <td><?php echo $ex_data->industry; ?></td>
                                                <td><?php echo $ex_data->company; ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-info"></i> Work Experience </h2>
                        </div>
                        <div class="panel-body" style="line-height: 25px;">
                            <?php
                            $work = $CI->admin_model->get_trainer_workExperiance($trainer[0]->user_code);
                            if (!empty($work)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Designation</th>
                                            <th>Function</th>
                                            <th>Industry</th>
                                            <th>Company</th>
                                            <th>Period</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($work as $wk_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $wk_data->work_designation; ?></td>
                                                <td><?php echo $wk_data->work_function; ?></td>
                                                <td><?php echo $wk_data->work_industry; ?></td>
                                                <td><?php echo $wk_data->work_orgn; ?></td>
                                                <td><?php echo $wk_data->work_period_from; ?> - <?php echo $wk_data->work_period_to; ?> </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-graduation-cap"></i> Education </h2>
                        </div>
                        <div class="panel-body" style="line-height: 25px;">
                            <?php
                            $education = $CI->admin_model->trainer_education($trainer[0]->user_code);
                            if (!empty($education)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Qualification</th>
                                            <th>Discipline</th>
                                            <th>University</th>
                                            <th>Year of Passing</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($education as $ed_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $ed_data->qualification; ?></td>
                                                <td><?php echo $ed_data->discipline; ?></td>
                                                <td><?php echo $ed_data->university; ?></td>
                                                <td><?php echo $ed_data->year_of_passing; ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-graduation-cap"></i> Certifications </h2>
                        </div>
                        <div class="panel-body" style="line-height: 25px;">
                            <?php
                            $cert = $CI->admin_model->trainer_certifications($trainer[0]->user_code);
                            if (!empty($cert)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Certification</th>
                                            <th>Level</th>
                                            <th>Awarded By</th>
                                            <th>Validity</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($cert as $cr_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $cr_data->certification_in; ?></td>
                                                <td><?php echo $cr_data->certification_level; ?></td>
                                                <td><?php echo $cr_data->awarded_by; ?></td>
                                                <td><?php echo $cr_data->valid_upto; ?></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-bank"></i> Bank Details </h2>
                        </div>
                        <div class="panel-body" style="line-height: 25px;">
                            <?php
                            $bank = $CI->admin_model->trainer_bankDetails($trainer[0]->user_code);
                            if (!empty($bank)) {
                                ?>
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td style="width:40%">Bank Name</td>
                                            <td><?php echo $bank[0]->bank_name; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Account Holder Name</td>
                                            <td><?php echo $bank[0]->account_name; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Account Number</td>
                                            <td><?php echo $bank[0]->bank_account; ?></td>
                                        </tr>
                                        <tr>
                                            <td>IFSC Code</td>
                                            <td><?php echo $bank[0]->bank_ifsc; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Branch Name</td>
                                            <td><?php echo $bank[0]->bank_branch; ?></td>
                                        </tr>
                                        <tr>
                                            <td>PAN No.</td>
                                            <td><?php echo $bank[0]->pan_no; ?><br/>
                                                <?php if ($bank[0]->pan_image != "") { ?>
                                                    <img src="/assets/upload/pan/<?php echo $bank[0]->pan_image; ?>" width="250">
                                                <?php } else { ?>
                                                    PAN not Uploaded
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>


                </div>
            </div>
        </div>


        <?php include 'js_files.php'; ?>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/moment.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/jquery.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.js'></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.assign-pm-btn').click(function (e) {
                    e.preventDefault();
                    $('#assign-pm-wrap').modal('show');
                    var cid = $(this).attr('cid');
                    $('#cid').val(cid);
                });





                $("#assign-pm-form").validate({
                    rules: {
                        pm: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        assign_pm();
                    }
                });

                $('.approved-btn').click(function (e) {
                    var tid = $(this).attr('tid');

                    $('.page_spin').show();
                    var dataString = "tid=" + tid + "&page=trainer_make_approve";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                });


            });
        </script>

    </body>
</html>