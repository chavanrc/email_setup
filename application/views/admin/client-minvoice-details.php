<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
        <style>
            .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
                border-top:solid 1px #000 !important;
            }
            .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
                border: 1px solid #000 !important;
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title col-md-4 no-print">
                        <?php
                        if ($invoice[0]->ci_img != '') {
                            $ciImg = explode(',', $invoice[0]->ci_img);
                            foreach ($ciImg as $ciimgdata) {
                                ?>
                                <a href="<?php echo base_url(); ?>assets/upload/client/<?php echo $ciimgdata; ?>" target="_blank" class="btn btn-warning" style="margin-right:5px;"><i class="fa fa-file-image-o" aria-hidden="true"></i> Invoice Copy</a>
                                <?php
                            }
                        }
                        ?>
                    </div>

                    <div class="page-title title-right col-md-8 no-print">
                        <?php
                        if ($invoice[0]->ci_status == '0') {
                            ?>
                            <a href="<?php echo base_url(); ?>admin/expense/<?php echo $invoice[0]->ci_id; ?>" class="btn btn-success">Edit Details</a>
                            <a href="#" class="btn btn-primary confirm-btn" pid="<?php echo $invoice[0]->ci_project; ?>" inid="<?php echo $invoice[0]->ci_id; ?>">Confirm</a>
                            <?php
                        } else {
                            ?>
                            <a href="#"  class="btn btn-success edit-request-btn">Request to Edit</a>
                            <a href="#"  class="btn btn-success upload-btn">Upload Scan Copy</a>

                            <a href="#"  class="make-payment-btn btn btn-primary">Update Payment</a>

                            <a href="#" onclick="window.print();" class="create-invoice-btn btn btn-primary">Print / Download</a>
                            <?php
                        }
                        ?>

                        <a href="<?php echo base_url(); ?>admin/client_invoice<?php
                        if (isset($_GET['page'])) {
                            echo '?page=' . $_GET['page'];
                        }
                        ?>" class="create-invoice-btn btn btn-danger">Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-10 content-page">
                    <?php
                    if (!empty($erequest)) {
                        ?>
                    <div class="no-print">
                        <strong>Invoice Revision Request</strong>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Status Date</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($erequest as $er_data) {
                                    ?>
                                    <tr>
                                        <td><?php echo $er_data->er_desc; ?></td>
                                        <td><?php echo date_formate($er_data->er_date); ?></td>
                                        <td>
                                    <?php 
                                    if($er_data->er_status=='0'){
                                        echo 'Pending';
                                    }
                                    if($er_data->er_status=='1'){
                                        echo 'Approved';
                                    }
                                    if($er_data->er_status=='2'){
                                        echo 'Rejected';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if($er_data->er_status!='0'){
                                        echo date_formate($er_data->er_status_date);
                                    }
                                    ?>
                                </td>
                                        <td>
                                            <?php if($er_data->er_status=='0'){ ?>
                                            <a href="#" cid="<?php echo $er_data->ci_id; ?>" eid="<?php echo $er_data->er_id; ?>" class="btn-sm btn-success accept-btn">Approve</a>
                                            <a href="#" cid="<?php echo $er_data->ci_id; ?>" eid="<?php echo $er_data->er_id; ?>" class="btn-sm btn-warning reject-btn">Reject</a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                        <?php
                    }
                    ?>
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <tr style="font-size:20px;">
                                    <td colspan="3" style="text-align: center;"><h4 style="font-size:20px;">Tax Invoice</h4>
                                        <?php if ($invoice[0]->include_gst == 'N') {
                                            ?>
                                            <h4 style="font-size:15px;">Export Of Goods Or Services Without Payment Of Integrated Tax</h4>
                                        <?php }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td rowspan="5" style="width:500px;">To, <br/>
                                        <?php echo $client[0]->client_name; ?><br/>
                                        <?php
                                        if (!empty($invoice)) {
                                            echo $invoice[0]->ci_gst_address;
                                        }
                                        ?>
                                    </td>
                                    <td>Invoice Number</td>
                                    <td><?php echo $invoice[0]->ci_number; ?></td>
                                </tr>
                                <tr>
                                    <td>Invoice Date</td>
                                    <td><?php echo date_formate_short($invoice[0]->ci_date); ?></td>
                                </tr>
                                <tr>
                                    <td>Ref. No</td>
                                    <td><?php
                                        if (!empty($invoice[0]->ci_refNumber)) {
                                            echo $invoice[0]->ci_refNumber;
                                        } else {
                                            echo 'NA';
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>Name</td>
                                    <td><?php echo $client[0]->person1; ?></td>
                                </tr>
                                <tr>

                                    <td>Email id</td>
                                    <td><?php echo $client[0]->email1; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>State Code :  </strong><?php
                                        if (!empty($invoice)) {
                                            echo $invoice[0]->ci_state_code;
                                        }
                                        ?> 
                                    </td>
                                    <td>PO. No</td>
                                    <td><?php
                                        if (!empty($invoice[0]->ci_poNumber)) {
                                            echo $invoice[0]->ci_poNumber;
                                        } else {
                                            echo 'NA';
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>GST NO: </strong> <?php
                                        if (!empty($invoice)) {
                                            echo $invoice[0]->ci_gst;
                                        }
                                        ?>
                                    </td>
                                    <td>SAC Code</td>
                                    <td><?php echo $invoice[0]->ci_sac; ?></td>
                                </tr>
                            </table>
                            <?php
                            $total = 0;
                            $no = 0;
                            ?>
                            <table class="table table-bordered text-center">
                                <tr style="font-weight: bold;">
                                    <td>Sl. No</td>
                                    <td>Description</td>
                                    <td>Name of the Program</td>
                                    <td>Month of the Program</td>
                                    <td>Training Location</td>
                                    <td>No. of Days</td>
                                    <td>Amount</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td><?php echo $invoice[0]->ci_desc; ?></td>
                                    <td><?php echo $invoice[0]->ci_project; ?></td>
                                    <td><?php
                                        $dt = DateTime::createFromFormat('!m', $invoice[0]->ci_month);
                                        echo $dt->format('F') . '-' . $invoice[0]->ci_year;
                                        ?>
                                    </td>
                                    <td>As per Voucher</td>
                                    <td><?php echo $invoice[0]->ci_days; ?></td>
                                    <td class="text-right"><?php
                                        $total = $invoice[0]->ci_charge;
                                        echo number_format($total, 2)
                                        ?></td>
                                </tr>
                                <tr style="font-size:18px;">
                                    <td colspan="6" class="text-right">Gross Total</td>
                                    <td style="width:100px; text-align:right;"><?php echo number_format($total, 2); ?></td>
                                </tr>

                            </table>
                            <?php
                            $updAmount = 0;
                            $upAmount = $total;
                            $company = $CI->admin_model->get_single_company($invoice[0]->ci_company);
                            $cmState = $company[0]->gst_state_code;
                            $cState = $company[0]->gst_state_code;
                            $cgst = 0;
                            $sgst = 0;
                            if (!empty($invoice)) {
                                $cState = $invoice[0]->ci_state_code;
                            }
                            ?>
                            <table class="table table-bordered">
                                <tr>
                                    <td>PAN No.</td>
                                    <td><?php echo $company[0]->pan_no; ?></td>
                                    <td class="text-right">CGST On Professional Fees @9%</td>
                                    <td style="width:100px; text-align:right;">
                                        <?php
                                        if ($cState == $cmState && $invoice[0]->include_gst == 'Y') {
                                            $cgst = round((9 / 100) * $total, 2);
                                            echo number_format($cgst, 2);
                                        } else {
                                            echo '-';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>GSTIN</td>
                                    <td><?php echo $company[0]->gstn; ?></td>
                                    <td class="text-right">SGST On Professional Fees @9%</td>
                                    <td style="text-align:right;"><?php
                                        if ($cState == $cmState && $invoice[0]->include_gst == 'Y') {
                                            $sgst = round((9 / 100) * $total, 2);
                                            echo number_format($sgst, 2);
                                        } else {
                                            echo '-';
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>TAN No</td>
                                    <td><?php echo $company[0]->tan_no; ?></td>
                                    <td class="text-right">IGST On Professional Fees @18%</td>
                                    <td style="text-align:right;">
                                        <?php
                                        if ($cState != $cmState && $invoice[0]->include_gst == 'Y') {
                                            $gstamt = round((18 / 100) * $total, 2);
                                            echo number_format($gstamt, 2);
                                        } else {
                                            $gstamt = $cgst + $sgst;
                                            echo '-';
                                        }
                                        $updGst = 0;
                                        $upGst = $gstamt;
                                        $gTotal = round($total + $gstamt);
                                        ?>
                                    </td>
                                </tr>
                                <tr style="font-size:20px;">
                                    <td colspan="2"></td>
                                    <td class="text-right">Total</td>
                                    <td style="text-align:right;">
                                        <?php
                                        echo number_format($gTotal, 2);
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <strong>In Words: 
                                            <?php
                                            //$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                            echo "Rs. " . getIndianCurrency($gTotal) . " Only.";
                                            ?>
                                        </strong>
                                    </td>
                                </tr>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td>Beneficiary</td>
                                    <td><?php echo $company[0]->company_name; ?></td>
                                    <td rowspan="5" style="text-align: center;">For, <?php echo $company[0]->company_name; ?>
                                        <br/><br/><br/><br/><br/><br/><br/><br/>
                                        Authorized Signatory
                                    </td>
                                </tr>
                                <tr>
                                    <td>Account No</td>
                                    <td><?php echo $company[0]->account_no; ?></td>
                                </tr><tr>
                                    <td>IFSC Code</td>
                                    <td><?php echo $company[0]->ifsc_no; ?></td>
                                </tr>
                                <tr>
                                    <td>Bank & Branch</td>
                                    <td><?php echo $company[0]->bank_branch; ?></td>
                                </tr>
                                <tr>
                                    <td class="text-center" colspan="2">
                                        <strong>
                                            * Thanking you and assuring our best services at all
                                        </strong>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?php
                    if (!empty($payment)) {
                        ?>
                        <div class="panel panel-default no-print">
                            <div class="panel-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <td colspan="5"><h4>Payment Details</h4></td>
                                    </tr>
                                    <tr>
                                        <td>Amount</td>
                                        <td>Date</td>
                                        <td>Payment id</td>
                                        <td>Payment Type</td>
                                        <td>Payment Advice</td>
                                        <td></td>
                                    </tr>
                                    <?php foreach ($payment as $pdata) {
                                        ?>
                                        <tr>
                                            <td><?php echo $pdata->cp_amt; ?></td>
                                            <td><?php echo date_formate_short($pdata->cp_date); ?></td>
                                            <td><?php echo $pdata->cp_pay_id; ?></td>
                                            <td><?php
                                                if ($pdata->cp_type == '1') {
                                                    echo 'Professional Fees';
                                                } else {
                                                    echo 'Debit Note';
                                                }
                                                ?></td>
                                            <td>
                                                <?php if ($pdata->cp_type == '1') { ?>
                                                    <input type="checkbox" cid="<?php echo $invoice[0]->ci_id; ?>" <?php
                                                    if ($invoice[0]->ci_pay_advice == '1') {
                                                        echo 'checked';
                                                    }
                                                    ?> id="payad" name="payad" value="1"/>
                                                       <?php } ?>
                                            </td>
                                            <td>
                                                <a href="#" class="btn-sm btn-danger remove-pay" rid="<?php echo $pdata->cp_id; ?>">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                        <?php }
                                    ?>
                                </table>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                </div>
            </div>
        </div>

        <div class="modal fade" id="pay_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Update Payment</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="pay-form">

                        <div class="modal-body">
                            <input type="hidden" name="cid" value="<?php echo $invoice[0]->ci_id; ?>"/>
                            <input type="hidden" name="intype" value="2"/>
                            <input type="hidden" name="month" value="<?php echo $invoice[0]->ci_month; ?>"/>
                            <input type="hidden" name="year" value="<?php echo $invoice[0]->ci_year; ?>"/>
                            <input type="hidden" name="client" value="<?php echo $invoice[0]->ci_client; ?>"/>
                            <input type="hidden" name="payType" value="1"/>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Amount <sup>*</sup></span>
                                    <input type="text" name="amt"  class="form-control" placeholder="Total Amount">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Payment Date <sup>*</sup></span>
                                    <input type="date" name="date"  class="form-control" placeholder="Payment Date">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Payment Notes </span>
                                    <input type="text" name="paymentid"  class="form-control" placeholder="Payment Id">
                                </div>
                            </div>

                            <div class="form-group  col-md-12">
                                <label><input type="checkbox" name="payadvice" value="1"> Payment Advice Pending</label>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="upload_img" class="btn btn-info" >Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="request_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Request</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="edit-request-form">

                        <div class="modal-body">
                            <input type="hidden" name="cid" value="<?php echo $invoice[0]->ci_id; ?>"/>
                            <input type="hidden" name="inumber" value="<?php echo $invoice[0]->ci_number; ?>"/>
                            <input type="hidden" name="client" value="<?php echo $client[0]->client_name; ?>"/>
                            <input type="hidden" name="project" value="Monthly Invoice"/>
                            <input type="hidden" name="pdate" value="<?php
                            $dt = DateTime::createFromFormat('!m', $invoice[0]->ci_month);
                            echo $dt->format('F') . '-' . $invoice[0]->ci_year;
                            ?>"/>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Email id <sup>*</sup></span>
                                    <input type="text" name="email"  class="form-control" placeholder="Email id">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Edit Reason </span>
                                    <input type="text" name="reason"  class="form-control" placeholder="Edit Reason">
                                </div>
                            </div>

                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="upload_img" class="btn btn-info" >Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="upload_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Upload Scan copy of invoice</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="upload-form">

                        <div class="modal-body">
                            <input type="hidden" name="upCid" value="<?php echo $invoice[0]->ci_id; ?>"/>
                            <input type="hidden" name="upPid" value=""/>

                            <div class="form-group col-md-12">
                                <div>
                                    <span> Scan Copy </span>
                                    <input type="file" name="img[]" multiple="multiple" accept="image/*,application/pdf"/>
                                </div>
                            </div>

                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="upload_img" class="btn btn-info" >Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <?php include 'js_files.php'; ?>

        <script type="text/javascript">
            $(document).ready(function () {
                
                $('.accept-btn').click(function (e) {
                    e.preventDefault();
                    var eid = $(this).attr('eid');
                    var cid = $(this).attr('cid');
                    var project = 'Monthly Invoice';
                    var client = '<?php echo $client[0]->client_name; ?>';
                    var pdate = '';
                    $('.page_spin').show();
                    var dataString = "cid="+cid+"&eid=" + eid + "&project="+project+"&client="+client+"&pdate="+pdate+"&status=1&page=accept_edit_request";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                });
                
                $('.reject-btn').click(function (e) {
                    e.preventDefault();
                    var eid = $(this).attr('eid');
                    var cid = $(this).attr('cid');
                    $('.page_spin').show();
                    var dataString = "cid="+cid+"&eid=" + eid + "&status=2&page=accept_edit_request";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                });

                $("#pay-form").validate({
                    rules: {
                        amt: "required",
                        date: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#edit-request-form").validate({
                    rules: {
                        email: "required",
                        reason: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#upload-form").validate({
                    rules: {
                        'img[]': "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $('.make-payment-btn').click(function (e) {
                    $('#pay_wraper').modal('show');
                });


                $('.edit-request-btn').click(function (e) {
                    e.preventDefault();
                    $('#request_wraper').modal('show');
                });

                $('.upload-btn').click(function (e) {
                    e.preventDefault();
                    $('#upload_wraper').modal('show');
                });
                
                $('#payad').change(function (e) {
                    var cid = $(this).attr('cid');
                    var status = '0';
                    if ($(this).prop("checked") == true) {
                         status = '1';
                    }
                    $('.page_spin').show();
                    var dataString = "cid=" + cid + "&status="+status+"&page=updatePayAdvice";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                        }, //success fun end
                    });//ajax end
                });

                $('.remove-pay').click(function(e){
                   e.preventDefault();
                   var rid = $(this).attr('rid');
                   var f = confirm("Are you sure want to remove ?");
                   if(f==true){
                       var ele = $(this).parent('td').parent('tr');
                       $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_client_payment";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                ele.remove();
                            }, //success fun end
                        });//ajax end
                   }
                });

                $('.confirm-btn').click(function (e) {
                    e.preventDefault();
                    var cid = $(this).attr('inid');
                    var cm = '<?php echo $invoice[0]->ci_company; ?>';
                    var amt = '<?php echo $upAmount; ?>';
                    var gst = '<?php echo $upGst; ?>';
                    var f = confirm("Are you sure want confirm ?");
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "did=0&pid=''&cm=" + cm + "&debit=Y&exp=0&cid=" + cid + "&amt=" + amt + "&damt=0&gst=" + gst + "&dgst=0&page=confirm_client_invoice";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

            });
        </script>

    </body>
</html>