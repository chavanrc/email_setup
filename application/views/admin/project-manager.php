<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Project Managers</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Functional Users </h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>admin/add_pm" class="btn btn-primary"><i class="fa fa-user-plus"></i> Create New User</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list"></i> Users List </h2>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>User Type</th>
                                        <th>Name</th>
                                        <th>Email</th>
                                        <th>Contact</th>
                                        <th>Photo</th>
                                        <th>Status</th>
                                        <th>Date</th>
                                        <th style="width:100px;"></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($pm)) {
                                        $no = 0;
                                        foreach ($pm as $pm_data) {
											switch ($pm_data->user_type)
											{
											case "project manager" :
												$folder="pm";
												break;
											case "admin support" :
												$folder="support";
												break;
											case "training support" : 
												$folder="training";
												break;
											case "content manager" :
												$folder="cm";
												break;
											case "business manager" :
												$folder="bm";
												break;
											case "drona" :
												$folder="drona";
												break;
												
											}
                                            $no++;
                                            ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo ucfirst($pm_data->user_type); ?></td>
                                                <td><?php echo $pm_data->name; ?></td>
                                                <td><?php echo $pm_data->email; ?></td>
                                                <td><?php echo $pm_data->contact_number; ?></td>
                                                <td align="center">
                                                    <img src="<?php echo base_url(); ?>assets/upload/<?php echo $folder; ?>/<?php echo $pm_data->user_photo; ?>" width="60px">
                                                </td>
                                                <td align="center"><?php
                                                    if ($pm_data->is_active == '1') {
                                                        ?>
                                                        <span class="label label-success">Active</span>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <span class="label label-warning">In Active</span>
                                                        <?php
                                                    }
                                                    ?></td>

                                                <td><?php echo date_formate_short($pm_data->create_date); ?></td>
                                                <td>
                                                    <?php if ($pm_data->user_type == "project manager" || $pm_data->user_type == "business manager") { ?>
                                                        <a href="#" class="btn btn-sm btn-success assign-client-btn" pm="<?php echo $pm_data->user_code; ?>" style="margin-bottom: 5px; display: inline-block;"><i class="fa fa-sliders"></i></a>
                                                    <?php } ?>
                                                    <a href="<?php echo base_url(); ?>admin/edit_pm/<?php echo $pm_data->user_code; ?>" class="btn btn-sm btn-info" style="margin-bottom: 5px; display: inline-block;"><i class="fa fa-edit"></i></a>
                                                    <a href="#" class="btn btn-sm btn-danger remove-user" uid="<?php echo $pm_data->user_id; ?>" style="margin-bottom: 5px; display: inline-block;"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="9">No Record found</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="modal fade" id="assign-pm-wrap" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <div class="col-md-5 text-center" style="border-right: solid 2px #ddd;">
                            <h4 class="modal-title">Existing Client List</h4>
                        </div>
                        <div class="col-md-6 text-center">
                            <h4 class="modal-title"><i class="fa fa-arrow-right"></i> Assign New Client</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-body">
                        <div class="col-md-5" style="border-right: solid 2px #ddd;">

                            <ul class="client_list">

                            </ul>
                        </div>
                        <div class="col-md-6">
                            <form action="" method="POST"  enctype="multipart/form-data" id="assign-pm-form">
                                <input type="hidden" value="" id="pm" name="pm"/>
                                <div class="form-group">
                                    <select class="form-control" id="cid" name="cid">
                                        <option value=""> - Select Client -</option>
                                        <?php
                                        foreach ($client as $cl_data) {
                                            ?>
                                            <option value="<?php echo $cl_data->client_id; ?>"> <?php echo $cl_data->client_name; ?> </option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                </div>
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </form>
                        </div>
                        <div class="clearfix"></div>


                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            
            $('#data-list').DataTable();

            $(document).ready(function () {
                $('.assign-client-btn').click(function (e) {
                    e.preventDefault();
                    $('#assign-pm-wrap').modal('show');
                    var pm = $(this).attr('pm');
                    $('#pm').val(pm);
                    $('.client_list li').remove();
                    $('.page_spin').show();
                    var dataString = "pm=" + pm + "&page=get_pm_client_list";
                    $.ajax({
                        type: "POST",
                        dataType: "JSON",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            $.each(data, function (i, item) {
                                $('.client_list').append("<li>" + item.client_name + "</li>");
                            });

                        }, //success fun end
                    });//ajax end
                });


                $("#assign-pm-form").validate({
                    rules: {
                        cid: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        assign_pm();
                    }
                });

                function assign_pm()
                {
                    var cid = $('#cid').val();
                    var pm = $('#pm').val();

                    $('.page_spin').show();
                    var dataString = "cid=" + cid + "&pm=" + pm + "&page=assign_pm";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            $('#assign-pm-wrap').modal('hide');

                        }, //success fun end
                    });//ajax end
                }

                $('.remove-user').click(function (e) {
                    e.preventDefault();
                    var uid = $(this).attr('uid');
                    var ele = $(this);
                    var f = confirm("Are you sure want to remove user ?");
                    if (f == true) {
                        $('.page_spin').show();
                        var dataString = "uid=" + uid + "&page=remove_user";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                ele.parent('td').parent('tr').remove();
                            }, //success fun end
                        });//ajax end
                    }
                });

            });

        </script>

    </body>
</html>