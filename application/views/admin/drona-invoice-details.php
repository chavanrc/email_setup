<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
        <style>
            td, th
            {
                font-size:12px;
            }
            .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
                border-top:solid 1px #000 !important;
                padding: 5px !important;
            }
            .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
                border: 1px solid #000 !important;
            }
            @media print {
                .in-print{
                    margin-top:100px;
                }
                .panel{
                    border:0px;
                }
                .panel-body{
                    padding:0px;
                }
                .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td {
                    padding: 5px;
                }
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12 no-print">
                    <div class="row">
                        <div class="col-md-4 page-title">
                            <h3 style="display:inline-block; margin-right: 20px;">Monthly Invoices</h3>
                            <?php
                            if ($invoice[0]->di_img != '') {
                                $cimg = explode(',', $invoice[0]->di_img);
                                foreach ($cimg as $cimgdata) {
                                    ?>
                                    <a href="<?php echo base_url(); ?>assets/upload/client/<?php echo $cimgdata; ?>" target="_blank" class="btn-sm btn-warning" style="margin-right:3px;"><i class="fa fa-file-image-o" aria-hidden="true"></i> Invoice Copy</a>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="col-md-8" style="padding-top: 10px;">
                            <a href="<?php echo base_url(); ?>admin/drona_invoice<?php
                            if (isset($_GET['page'])) {
                                echo '?page=' . $_GET['page'];
                            }
                            ?>" class="btn btn-danger pull-right" style="margin-left:10px;">Back</a>
                            <a href="#" cid="<?php echo $invoice[0]->di_id; ?>"class="cancel-invoice-btn pull-right btn btn-warning" style="margin-left:10px;">Cancel</a>
                               <?php
                               if ($invoice[0]->di_status == 0) {
                                   if ($this->session->userdata('type') == 'admin') {
                                       ?>
                                    <a href="#" class="btn btn-success pull-right confirm-btn" style="margin-left:10px;">Confirm</a>
                                <?php } ?>
                                <a href="#" class="btn btn-primary pull-right add-exp-btn" style="margin-left:10px;">Add Expense</a>
                                <a href="#" class="btn btn-warning pull-right edit-btn">Edit</a>
                            <?php } else if ($invoice[0]->di_status == '2') {
                            ?>
                            <p style="color:#ED1122;"><strong>Invoice Cancelled</strong></p>
                            <?php
                        } else {
                                ?>
                                <a href="#"  class="btn btn-success pull-right edit-request-btn" style="margin-left:10px;">Request to Edit</a>
                                <?php if ($this->session->userdata('type') == 'admin') { ?>
                                    <a href="#"  class="btn btn-success pull-right upload-btn" style="margin-left:10px;">Upload Scan Copy</a>
                                    <?php
                                }
                                if ($this->session->userdata('type') == 'admin') {
                                    ?>
                                    <a href="#"  class="make-payment-btn btn btn-primary pull-right" style="margin-left:10px;">Update Payment</a>
                                <?php } ?>
                                <a href="#" onclick="window.print();" class="btn btn-primary pull-right">Print / Download</a>
                                
                            <?php }
                            ?>
                            
                        </div>
                    </div>
                    <div class="clearfix"></div>

                    <?php
                    if (!empty($erequest)) {
                        ?>
                        <div class="no-print">
                            <strong>Invoice Revision</strong>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Request Send to</th>
                                        <th>Description</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Update Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($erequest as $er_data) {
                                        ?>
                                        <tr>
                                            <td><?php echo $er_data->er_email; ?></td>
                                            <td><?php echo $er_data->er_desc; ?></td>
                                            <td><?php echo $er_data->er_date; ?></td>
                                            <td>
                                                <?php
                                                if ($er_data->er_status == '0') {
                                                    echo 'Pending';
                                                }
                                                if ($er_data->er_status == '1') {
                                                    echo 'Approved';
                                                }
                                                if ($er_data->er_status == '2') {
                                                    echo 'Rejected';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($er_data->er_status != '0') {
                                                    echo date_formate($er_data->er_status_date);
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <?php
                    }
                    ?>

                </div>
                <?php
                $inAmt = 0;
                $debitAmt = 0;
                $inGST = 0;
                $debitGST = 0;
                $inTotal = 0;
                $debitTotal = 0;
                if ($invoice[0]->di_onlyDebit == 0) {
                    ?>
                    <div class="col-md-10 content-page" style="margin-top: 10px;">
                        <div class="panel panel-default in-print">
                            <div class="panel-body">
                                <div>
                                    <?php if (!empty($invoice[0]->di_type)) { ?>
                                        <table class="table table-bordered" style="margin-bottom: 0px;">
                                            <tr>
                                                <td style="text-align: center; font-weight: bold;">
                                                    <?php echo $cm[0]->company_name; ?><br/>
                                                    <?php echo $cm[0]->address; ?> <br/>
                                                    Maharashtra, India | Contact No : 8149006055
                                                </td>
                                                <td align="center">Kofax 2.0 <br/> NA</td>
                                            </tr>
                                        </table>
                                    <?php } ?>
                                    <table class="table table-bordered" style="margin-bottom: 0px;">
                                        <tr>
                                            <td colspan="4" style="text-align: center; text-transform: uppercase;"><strong>Tax Invoice For <?php
                                                    if (!empty($invoice[0]->di_type)) {
                                                        echo $invoice[0]->di_type;
                                                    } else {
                                                        echo $invoice[0]->di_project;
                                                    }
                                                    ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">To,</td>
                                            <td style="width:150px;">Invoice Number</td>
                                            <td><?php echo $invoice[0]->di_number; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" rowspan="<?php
                                            if (!empty($invoice[0]->di_type)) {
                                                echo '6';
                                            } else {
                                                echo '4';
                                            }
                                            ?>">
                                                    <?php echo $invoice[0]->di_address; ?>
                                            </td>
                                            <td>Invoice Date</td>
                                            <td><?php echo date_formate_short($invoice[0]->di_date); ?></td>
                                        </tr>

                                        <?php
                                        if (!empty($invoice[0]->di_type)) {
                                            ?>
                                            <tr>
                                                <td>Vendor Code</td>
                                                <td><?php echo $invoice[0]->di_vendor_code; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Location</td>
                                                <td><?php echo $invoice[0]->di_location; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Department</td>
                                                <td><?php echo $invoice[0]->di_department; ?></td>
                                            </tr>
                                            <tr>
                                                <td>TML Contact e-mail</td>
                                                <td><?php echo $invoice[0]->di_email; ?></td>
                                            </tr>
                                            <tr>
                                                <td>PO Number</td>
                                                <td><?php echo $invoice[0]->di_poNumber; ?></td>

                                            </tr>
                                            <?php
                                        } else {
                                            ?>
                                            <tr>
                                                <td>Ref. No.</td>
                                                <td>
                                                    <?php
                                                    if (!empty($invoice[0]->di_debitNumber)) {
                                                        echo $invoice[0]->di_debitNumber;
                                                    } else {
                                                        echo 'NA';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>Name</td>
                                                <td><?php echo $invoice[0]->di_name; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><?php echo $invoice[0]->di_email; ?></td>
                                            </tr>    
                                            <?php
                                        }
                                        ?>
                                        <tr>
                                            <td>State Code</td>
                                            <td><?php echo $invoice[0]->di_state_code; ?></td>
                                            <?php if (!empty($invoice[0]->di_type)) { ?>
                                                <td>SES Number</td>
                                                <td><?php echo $invoice[0]->di_sesNumber; ?></td>
                                            <?php } else {
                                                ?>
                                                <td>PO Number</td>
                                                <td><?php echo $invoice[0]->di_poNumber; ?></td>
                                            <?php }
                                            ?>
                                        </tr>
                                        <tr>
                                            <td>GSTIN No</td>
                                            <td><?php echo $invoice[0]->di_gst; ?></td>
                                            <td>SAC Code</td>
                                            <td><?php echo $invoice[0]->di_sac; ?></td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered" style="margin-bottom: 0px;">
                                        <tr>
                                            <td style="text-align: center;">Sl. No.</td>
                                            <td style="text-align: center;">Description</td>
                                            <td style="text-align: center;">Month</td>
                                            <td style="text-align: center;"><?php
                                                if (!empty($invoice[0]->di_type)) {
                                                    echo 'Type of Project';
                                                } else {
                                                    echo 'Location';
                                                }
                                                ?></td>
                                            <td style="text-align: center; width: 130px;">Amount</td>
                                            <?php
                                            if ($invoice[0]->di_status == 0) {
                                                ?>
                                                <td></td>
                                                <?php
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                        $total = 0;
                                        if (!empty($exp)) {
                                            $no = 0;
                                            $inCount = 0;
                                            foreach ($exp as $inData) {
                                                $inCount++;
                                            }
                                            foreach ($exp as $exData) {
                                                if ($exData->dd_status == 1) {
                                                    $no++;
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center;"><?php echo $no; ?></td>
                                                        <td style="text-align: center;"><?php echo $exData->dd_desc; ?></td>
                                                        <?php if ($no == 1) { ?>
                                                            <td valign="middle" style="text-align: center; vertical-align: middle;" rowspan="<?php echo $inCount; ?>">
                                                                <?php
                                                                if ($invoice[0]->di_month != 'NA') {
                                                                    $dt = DateTime::createFromFormat('!m', $invoice[0]->di_month);
                                                                    echo $dt->format('F') . '-' . $invoice[0]->di_year;
                                                                } else {
                                                                    echo "NA";
                                                                }
                                                                ?>
                                                            </td>
                                                            <td valign="middle" style="text-align: center; vertical-align: middle;" rowspan="<?php echo $inCount; ?>">
                                                                <?php
                                                                if (!empty($invoice[0]->di_type)) {
                                                                    echo $invoice[0]->di_type;
                                                                } else {
                                                                    echo $invoice[0]->di_location;
                                                                }
                                                                ?>
                                                            </td>
                                                        <?php } ?>
                                                        <td  style="text-align: right;"><?php echo number_format($exData->dd_amount, 2); ?></td>
                                                        <?php
                                                        if ($invoice[0]->di_status == 0) {
                                                            ?>
                                                            <td>
                                                                <a href="#" class="remove-exp" eid="<?php echo $exData->dd_id; ?>">
                                                                    <i class="fa fa-remove"></i>
                                                                </a>
                                                            </td>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tr>
                                                    <?php
                                                    $total +=$exData->dd_amount;
                                                }
                                            }
                                        }
                                        ?>
                                    </table>
                                    <?php
                                    $cmState = $cm[0]->gst_state_code;
                                    $cState = $invoice[0]->di_state_code;
                                    $cgst = 0;
                                    $sgst = 0;
                                    $gTotal = 0;
                                    ?>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td rowspan="2" align="center">Rupees In Words</td>
                                            <td rowspan="2" align="center">
                                                <?php
                                                if ($cState != $cmState) {
                                                    $gstamt = round((18 / 100) * $total, 2);
                                                } else {
                                                    $cgst = round((9 / 100) * $total, 2);
                                                    $sgst = round((9 / 100) * $total, 2);
                                                    $gstamt = $cgst + $sgst;
                                                }
                                                $gTotal = $total + $gstamt;
                                                $inAmt = $total;
                                                $inGST = $gstamt;
                                                $inTotal = round($gTotal);
                                                echo 'Rupees ' . getIndianCurrency(round($gTotal)) . ' Only';
                                                ?>
                                            </td>
                                            <td style="font-weight: 600;">Gross Total</td>
                                            <td style="text-align: right; width: 130px; font-weight: 600;"><?php echo number_format($total, 2); ?></td>
                                        </tr>
                                        <tr>
                                            <td>CGST On Professional Fees @ 9%</td>
                                            <td style="text-align: right;">
                                                <?php
                                                if ($cState == $cmState) {

                                                    echo number_format($cgst, 2);
                                                } else {
                                                    echo '-';
                                                }
                                                ?>
                                            </td>

                                        </tr>
                                        <tr>

                                            <td>PAN No.</td>
                                            <td><?php echo $cm[0]->pan_no; ?></td>
                                            <td>SGST On Professional Fees @ 9%</td>
                                            <td style="text-align: right; width: 130px;">
                                                <?php
                                                if ($cState == $cmState) {

                                                    echo number_format($sgst, 2);
                                                } else {
                                                    echo '-';
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>GSTIN No</td>
                                            <td><?php echo $cm[0]->gstn; ?></td>

                                            <td>IGST On Professional Fees @ 18%</td>
                                            <td style="text-align: right; width: 130px;">
                                                <?php
                                                if ($cState != $cmState) {
                                                    echo number_format($gstamt, 2);
                                                } else {
                                                    echo '-';
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr >
                                            <td>TAN No</td>
                                            <td><?php echo $cm[0]->tan_no; ?></td>
                                            <td style="font-weight: 600;">Total</td>
                                            <td style="text-align: right; font-weight: 600;"><?php echo number_format(round($gTotal), 2); ?></td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>Beneficiary Name</td>
                                            <td><?php echo $cm[0]->company_name; ?></td>
                                            <td rowspan="6" style="width:250px; text-align: center;">
                                                For <?php echo $cm[0]->company_name; ?><br/><br/><br/>
                                                <br/><br/><br/><br/>
                                                Authorised Signatory
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Account Number</td>
                                            <td><?php echo $cm[0]->account_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td>IFSC Code</td>
                                            <td><?php echo $cm[0]->ifsc_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Bank & Branch</td>
                                            <td><?php echo $cm[0]->bank_branch; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align: center;">
                                                <strong>
                                                    Thanking you and assuring our best service at all times.
                                                </strong>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="clearfix"></div>
                    <?php
                }
                if (!empty($invoice[0]->di_debitNumber) || $invoice[0]->di_onlyDebit == 1) {
                    ?>
                    <div class="col-md-10 content-page" style="margin-top: 10px; page-break-before: always;">
                        <div class="panel panel-default in-print">
                            <div class="panel-body">
                                <div>
                                    <?php if (!empty($invoice[0]->di_type)) { ?>
                                        <table class="table table-bordered" style="margin-bottom: 0px;">
                                            <tr>
                                                <td style="text-align: center; font-weight: bold;">
                                                    <?php echo $cm[0]->company_name; ?><br/>
                                                    <?php echo $cm[0]->address; ?><br/>
                                                    Maharashtra, India | Contact No : 8149006055
                                                </td>
                                                <td align="center">Kofax 2.0 <br/> NA</td>
                                            </tr>
                                        </table>
                                    <?php } ?>
                                    <table class="table table-bordered" style="margin-bottom: 0px;">
                                        <tr>
                                            <td colspan="4" style="text-align: center; text-transform: uppercase;"><strong>Tax Invoice For <?php
                                                    if (!empty($invoice[0]->di_type)) {
                                                        echo $invoice[0]->di_type;
                                                    } else {
                                                        echo $invoice[0]->di_project;
                                                    }
                                                    ?></strong></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2">To,</td>
                                            <td style="width:150px;">Invoice Number</td>
                                            <td><?php echo $invoice[0]->di_debitNumber; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" rowspan="<?php
                                            if (!empty($invoice[0]->di_type)) {
                                                echo '6';
                                            } else {
                                                echo '4';
                                            }
                                            ?>">
                                                    <?php echo $invoice[0]->di_address; ?>
                                            </td>
                                            <td>Invoice Date</td>
                                            <td><?php echo date_formate_short($invoice[0]->di_date); ?></td>
                                        </tr>
                                        <?php
                                        if (!empty($invoice[0]->di_type)) {
                                            ?>
                                            <tr>
                                                <td>Vendor Code</td>
                                                <td><?php echo $invoice[0]->di_vendor_code; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Location</td>
                                                <td><?php echo $invoice[0]->di_location; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Department</td>
                                                <td><?php echo $invoice[0]->di_department; ?></td>
                                            </tr>
                                            <tr>
                                                <td>TML Contact e-mail</td>
                                                <td><?php echo $invoice[0]->di_email; ?></td>
                                            </tr>
                                            <tr>
                                                <td>SES Number</td>
                                                <td><?php echo $invoice[0]->di_sesNumber; ?></td>
                                            </tr>
                                            <?php
                                        } else {
                                            ?>
                                            <tr>
                                                <td>Ref. No.</td>
                                                <td><?php
                                                    if (!empty($invoice[0]->di_number)) {
                                                        echo $invoice[0]->di_number;
                                                    } else {
                                                        echo 'NA';
                                                    }
                                                    ?></td>
                                            </tr>
                                            <tr>
                                                <td>Name</td>
                                                <td><?php echo $invoice[0]->di_name; ?></td>
                                            </tr>
                                            <tr>
                                                <td>Email</td>
                                                <td><?php echo $invoice[0]->di_email; ?></td>
                                            </tr>    
                                            <?php
                                        }
                                        ?>
                                        <tr>
                                            <td>State Code</td>
                                            <td><?php echo $invoice[0]->di_state_code; ?></td>
                                            <td>PO Number</td>
                                            <td><?php echo $invoice[0]->di_poNumber; ?></td>
                                        </tr>
                                        <tr>
                                            <td>GSTIN No</td>
                                            <td><?php echo $invoice[0]->di_gst; ?></td>
                                            <td>SAC Code</td>
                                            <td><?php echo $invoice[0]->di_sac; ?></td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered" style="margin-bottom: 0px;">
                                        <tr>
                                            <td style="text-align: center;">Sl. No.</td>
                                            <td style="text-align: center;">Description</td>
                                            <td style="text-align: center;">Month</td>
                                            <td style="text-align: center;"><?php
                                                if (!empty($invoice[0]->di_type)) {
                                                    echo 'Type of Project';
                                                } else {
                                                    echo 'Location';
                                                }
                                                ?></td>
                                            <td style="text-align: center; width: 130px;">Amount</td>
                                            <?php
                                            if ($invoice[0]->di_status == 0) {
                                                ?>
                                                <td></td>
                                                <?php
                                            }
                                            ?>
                                        </tr>
                                        <?php
                                        $total = 0;
                                        if (!empty($exp)) {
                                            $no = 0;
                                            $debitCount = 0;
                                            foreach ($exp as $exCount) {
                                                if ($exCount->dd_status == 0) {
                                                    $debitCount++;
                                                }
                                            }
                                            foreach ($exp as $exData) {
                                                if ($exData->dd_status == 0) {
                                                    $no++;
                                                    ?>
                                                    <tr>
                                                        <td style="text-align: center;"><?php echo $no; ?></td>
                                                        <td style="text-align: center;"><?php echo $exData->dd_desc; ?></td>
                                                        <?php if ($no == 1) { ?>
                                                            <td valign="middle" style="text-align: center; vertical-align: middle;" rowspan="<?php echo $debitCount; ?>">
                                                                <?php
                                                                if ($invoice[0]->di_month != 'NA') {
                                                                    $dt = DateTime::createFromFormat('!m', $invoice[0]->di_month);
                                                                    echo $dt->format('F') . '-' . $invoice[0]->di_year;
                                                                } else {
                                                                    echo 'NA';
                                                                }
                                                                ?>
                                                            </td>
                                                            <td valign="middle" style="text-align: center; vertical-align: middle;" rowspan="<?php echo $debitCount; ?>">
                                                                <?php
                                                                if (!empty($invoice[0]->di_type)) {
                                                                    echo $invoice[0]->di_type;
                                                                } else {
                                                                    echo $invoice[0]->di_location;
                                                                }
                                                                ?>
                                                            </td>
                                                        <?php } ?>
                                                        <td style="text-align: right;"><?php echo number_format($exData->dd_amount, 2); ?></td>
                                                        <?php
                                                        if ($invoice[0]->di_status == 0) {
                                                            ?>
                                                            <td>
                                                                <a href="#" class="remove-exp" eid="<?php echo $exData->dd_id; ?>">
                                                                    <i class="fa fa-remove"></i>
                                                                </a>
                                                            </td>
                                                            <?php
                                                        }
                                                        ?>
                                                    </tr>
                                                    <?php
                                                    $total +=$exData->dd_amount;
                                                }
                                            }
                                        }
                                        ?>
                                    </table>
                                    <?php
                                    $cmState = $cm[0]->gst_state_code;
                                    $cState = $invoice[0]->di_state_code;
                                    $cgst = 0;
                                    $sgst = 0;
                                    $gTotal = 0;
                                    ?>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td rowspan="2" align="center">Rupees In Words</td>
                                            <td rowspan="2" align="center">

                                                <?php
                                                if ($cState != $cmState) {
                                                    $gstamt = round((18 / 100) * $total, 2);
                                                } else {
                                                    $cgst = round((9 / 100) * $total, 2);
                                                    $sgst = round((9 / 100) * $total, 2);
                                                    $gstamt = $cgst + $sgst;
                                                }
                                                $gTotal = $total + $gstamt;
                                                $debitAmt = $total;
                                                $debitGST = $gstamt;
                                                $debitTotal = round($gTotal);
                                                echo 'Rupees ' . getIndianCurrency(round($gTotal)) . ' Only';
                                                ?>

                                            </td>

                                            <td style="font-weight: 600;">Gross Total</td>
                                            <td style="text-align: right; width: 130px; font-weight: 600;"><?php echo number_format($total, 2); ?></td>
                                        </tr>
                                        <tr>

                                            <td>CGST On Professional Fees @ 9%</td>
                                            <td style="text-align: right;">
                                                <?php
                                                if ($cState == $cmState) {

                                                    echo number_format($cgst, 2);
                                                } else {
                                                    echo '-';
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>PAN No.</td>
                                            <td><?php echo $cm[0]->pan_no; ?></td>

                                            <td>SGST On Professional Fees @ 9%</td>
                                            <td style="text-align: right; width: 130px;">
                                                <?php
                                                if ($cState == $cmState) {

                                                    echo number_format($sgst, 2);
                                                } else {
                                                    echo '-';
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>GSTIN No</td>
                                            <td><?php echo $cm[0]->gstn; ?></td>
                                            <td>IGST On Professional Fees @ 18%</td>
                                            <td style="text-align: right; width: 130px;">
                                                <?php
                                                if ($cState != $cmState) {
                                                    echo number_format($gstamt, 2);
                                                } else {
                                                    echo '-';
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr style="font-weight: 600;">
                                            <td>TAN No</td>
                                            <td><?php echo $cm[0]->tan_no; ?></td>
                                            <td>Total</td>
                                            <td style="text-align: right;"><?php echo number_format(round($gTotal), 2); ?></td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>Beneficiary Name</td>
                                            <td><?php echo $cm[0]->company_name; ?></td>
                                            <td rowspan="6" style="width:250px; text-align: center;">
                                                For <?php echo $cm[0]->company_name; ?><br/><br/><br/>
                                                <br/><br/><br/><br/>
                                                Authorised Signatory
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Account Number</td>
                                            <td><?php echo $cm[0]->account_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td>IFSC Code</td>
                                            <td><?php echo $cm[0]->ifsc_no; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Bank & Branch</td>
                                            <td><?php echo $cm[0]->bank_branch; ?></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" style="text-align: center;">
                                                <strong>
                                                    Thanking you and assuring our best service at all times.
                                                </strong>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                }
                if (!empty($payment)) {
                    ?>
                    <div class="col-md-10 content-page">
                        <div class="panel panel-default no-print">
                            <div class="panel-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <td colspan="5"><h4>Payment Details</h4></td>
                                    </tr>
                                    <tr>
                                        <td>Amount</td>
                                        <td>Date</td>
                                        <td>Payment id</td>
                                        <td>Payment type</td>
                                        <td></td>
                                    </tr>
                                    <?php foreach ($payment as $pdata) {
                                        ?>
                                        <tr>
                                            <td><?php echo $pdata->cp_amt; ?></td>
                                            <td><?php echo date_formate_short($pdata->cp_date); ?></td>
                                            <td><?php echo $pdata->cp_pay_id; ?></td>
                                            <td><?php
                                                if ($pdata->cp_type == '1') {
                                                    echo 'Professional Fees';
                                                } else {
                                                    echo 'Debit Note';
                                                }
                                                ?></td>
                                            <td>
                                                <a href="#" class="btn-sm btn-danger remove-pay" rid="<?php echo $pdata->cp_id; ?>">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php }
                                    ?>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    <?php
                }
                ?>
            </div>
        </div>

        <div class="modal fade" id="clientWraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Select Client to generate new invoice</h4>
                    </div>
                    <form action="" method="POST" id="creatinForm">
                        <input type="hidden" name="inid" value="<?php echo $invoice[0]->di_id; ?>"/>
                        <input type="hidden" name="onlydebit" value="<?php echo $invoice[0]->di_onlyDebit; ?>"/>
                        <div class="modal-body">

                            <div class="form-group col-md-12">



                                <div class="input-group">                                            
                                    <select class="form-control" name="year" required>                                                
                                        <option value="2019" <?php
                                        if ($invoice[0]->di_year == '2019') {
                                            echo 'selected';
                                        }
                                        ?>> 2019 </option>                                                
                                        <option value="2020" <?php
                                        if ($invoice[0]->di_year == '2020') {
                                            echo 'selected';
                                        }
                                        ?> > 2020 </option>                                                
                                        <option value="2021" <?php
                                        if ($invoice[0]->di_year == '2021') {
                                            echo 'selected';
                                        }
                                        ?> > 2021 </option>                                                
                                        <option value="2022" <?php
                                        if ($invoice[0]->di_year == '2022') {
                                            echo 'selected';
                                        }
                                        ?> > 2022 </option>                                                                                                                              
                                        <option value="2023" <?php
                                        if ($invoice[0]->di_year == '2023') {
                                            echo 'selected';
                                        }
                                        ?> > 2023 </option>                                                                                                                              
                                        <option value="2024" <?php
                                        if ($invoice[0]->di_year == '2024') {
                                            echo 'selected';
                                        }
                                        ?> > 2024 </option> 
                                    </select> 
                                    <div class="input-group-addon" style="width:100px; padding: 0px; border:0;">
                                        <select class="form-control" name="month" required>  
                                            <option value="NA" <?php
                                            if ($invoice[0]->di_month == 'NA') {
                                                echo 'selected';
                                            }
                                            ?> > NA </option>
                                            <option value="01" <?php
                                            if ($invoice[0]->di_month == '01') {
                                                echo 'selected';
                                            }
                                            ?> > Jan </option>                                                
                                            <option value="02" <?php
                                            if ($invoice[0]->di_month == '02') {
                                                echo 'selected';
                                            }
                                            ?> > Feb </option>                                                
                                            <option value="03" <?php
                                            if ($invoice[0]->di_month == '03') {
                                                echo 'selected';
                                            }
                                            ?> > Mar </option>                                                
                                            <option value="04" <?php
                                            if ($invoice[0]->di_month == '04') {
                                                echo 'selected';
                                            }
                                            ?> > Apr </option>                                                
                                            <option value="05" <?php
                                            if ($invoice[0]->di_month == '05') {
                                                echo 'selected';
                                            }
                                            ?> > May </option>                                                
                                            <option value="06" <?php
                                            if ($invoice[0]->di_month == '06') {
                                                echo 'selected';
                                            }
                                            ?> > Jun </option>                                                
                                            <option value="07" <?php
                                            if ($invoice[0]->di_month == '07') {
                                                echo 'selected';
                                            }
                                            ?> > Jul </option>                                                
                                            <option value="08" <?php
                                            if ($invoice[0]->di_month == '08') {
                                                echo 'selected';
                                            }
                                            ?> > Aug </option>                                                
                                            <option value="09" <?php
                                            if ($invoice[0]->di_month == '09') {
                                                echo 'selected';
                                            }
                                            ?> > Sep </option>                                                
                                            <option value="10" <?php
                                            if ($invoice[0]->di_month == '10') {
                                                echo 'selected';
                                            }
                                            ?> > Oct </option>                                                
                                            <option value="11" <?php
                                            if ($invoice[0]->di_month == '11') {
                                                echo 'selected';
                                            }
                                            ?> > Nov </option>                                                
                                            <option value="12" <?php
                                            if ($invoice[0]->di_month == '12') {
                                                echo 'selected';
                                            }
                                            ?> > Dec </option>                                                                                            
                                        </select> 
                                    </div>

                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Invoice Date </span>
                                    <?php
                                    $edate = explode(' ', $invoice[0]->di_date);
                                    ?>
                                    <input type="date" class="form-control" name="idate" value="<?php echo $edate[0]; ?>"/>
                                </div>
								<div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Invoice No. </span>
                                    <input type="text" class="form-control" name="inumber" value="<?php echo $invoice[0]->di_number; ?>" placeholder="Invoice Number"/>
                                </div>
                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Clients <sup>*</sup></span>
                                    <select class="form-control" name="client" id="client">
                                        <option value=""> - select - </option>
                                        <?php
                                        if (!empty($client)) {
                                            foreach ($client as $cl_data) {
                                                ?>
                                                <option value="<?php echo $cl_data->client_id; ?>" <?php
                                                if ($invoice[0]->di_client == $cl_data->client_id) {
                                                    echo 'selected';
                                                }
                                                ?> ><?php echo $cl_data->client_name; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                    </select>
                                </div>

                                <div class="input-group load-gst" style="margin-top: 10px;">
                                    <input type="hidden" name="cgst" value="<?php echo $invoice[0]->di_state; ?>"/>
                                    <span class="input-group-addon" id="sizing-addon1">GST Location <sup>*</sup></span>
                                    <select class="form-control" name="gst" required>
                                        <option value=""> - select - </option>
                                        <?php
                                        if (!empty($location)) {
                                            foreach ($location as $r_data) {
                                                ?>
                                                <option value="<?php echo $r_data->id; ?>" <?php
                                                if ($invoice[0]->di_state == $r_data->state) {
                                                    echo 'selected';
                                                }
                                                ?> ><?php echo $r_data->state; ?></option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                    </select>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Drona Type</span>
                                    <select class="form-control" name="drona">
                                        <option value="" <?php
                                        if ($invoice[0]->di_type == "") {
                                            echo 'selected';
                                        }
                                        ?> > - select - </option>
                                        <option value="Service Drona" <?php
                                        if ($invoice[0]->di_type == "Service Drona") {
                                            echo 'selected';
                                        }
                                        ?> >Service Drona</option>
                                        <option value="Sales Drona" <?php
                                        if ($invoice[0]->di_type == "Sales Drona") {
                                            echo 'selected';
                                        }
                                        ?> >Sales Drona </option>
                                        <option value="Neev Drona" <?php
                                        if ($invoice[0]->di_type == "Neev Drona") {
                                            echo 'selected';
                                        }
                                        ?> >Neev Drona</option>
                                        <option value="Project Vijay" <?php
                                        if ($invoice[0]->di_type == "Project Vijay") {
                                            echo 'selected';
                                        }
                                        ?> >Project Vijay</option>

                                    </select>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Vendor Code </span>
                                    <input type="text" class="form-control" name="vcode" value="<?php echo $invoice[0]->di_vendor_code; ?>" placeholder="Vendor Code"/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Location </span>
                                    <input type="text" class="form-control" name="location" value="<?php echo $invoice[0]->di_location; ?>" placeholder="Location"/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Project </span>
                                    <input type="text" class="form-control" name="project" value="<?php echo $invoice[0]->di_project; ?>" placeholder="Project"/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Department </span>
                                    <input type="text" class="form-control" name="department" value="<?php echo $invoice[0]->di_department; ?>" placeholder="Department"/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Name </span>
                                    <input type="text" class="form-control" name="name" value="<?php echo $invoice[0]->di_name; ?>" placeholder="Name"/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Email </span>
                                    <input type="text" class="form-control" name="email" value="<?php echo $invoice[0]->di_email; ?>" placeholder="Email"/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">PO. No. </span>
                                    <input type="text" class="form-control" name="poNum" value="<?php echo $invoice[0]->di_poNumber; ?>" placeholder="PO. No."/>
                                </div>
                                
                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">SAC Code </span>
                                    <input type="text" class="form-control" name="sac" value="<?php echo $invoice[0]->di_sac; ?>" placeholder="SAC Code"/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">SES No. </span>
                                    <input type="text" class="form-control" name="sesNum" value="<?php echo $invoice[0]->di_sesNumber; ?>" placeholder="SES No."/>
                                </div>

                            </div>



                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="expWraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add Expense</h4>
                    </div>
                    <form action="" method="POST" id="expForm">
                        <input type="hidden" name="debit_number" value="<?php echo $invoice[0]->di_debitNumber; ?>"/>
                        <input type="hidden" name="in_number" value="<?php echo $invoice[0]->di_number; ?>"/>
                        <input type="hidden" name="in_id" value="<?php echo $invoice[0]->di_id; ?>"/>
                        <input type="hidden" name="company" value="<?php echo $invoice[0]->di_company; ?>"/>
                        <div class="modal-body">

                            <div class="form-group col-md-12">


                                <div class="input-group" style="margin-top: 10px;">
                                    <span class="input-group-addon" id="sizing-addon1">Description </span>
                                    <input type="text" class="form-control" name="desc" placeholder="Description"/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">
                                    <span class="input-group-addon" id="sizing-addon1">Amount </span>
                                    <input type="text" class="form-control" name="amount" placeholder="Amount"/>
                                </div>

                                <div  style="margin-top: 10px;">
                                    <?php if ($invoice[0]->di_onlyDebit == 0) { ?>
                                        <label><input type="radio" checked name="exptype" value="1"/> Invoice</label>
                                    <?php } ?>
                                    <label><input type="radio" <?php
                                        if ($invoice[0]->di_onlyDebit == 1) {
                                            echo 'checked';
                                        }
                                        ?> name="exptype" value="0"/> Debit Note</label>
                                </div>

                            </div>



                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="pay_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Update Payment</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="pay-form">

                        <div class="modal-body">
                            <input type="hidden" name="intype" value="3"/>
                            <input type="hidden" name="cid" value="<?php echo $invoice[0]->di_id; ?>"/>

                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Payment Type <sup>*</sup></span>
                                    <select class="form-control" name="payType">
                                        <option value="1">Professional Fees</option>
                                        <option value="0">Debit Note</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Amount <sup>*</sup></span>
                                    <input type="text" name="amt" value="<?php echo $inTotal + $debitTotal; ?>" class="form-control" placeholder="Total Amount">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Payment Date <sup>*</sup></span>
                                    <input type="date" name="date"  class="form-control" placeholder="Payment Date">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Payment Notes </span>
                                    <input type="text" name="paymentid"  class="form-control" placeholder="Payment Id">
                                </div>
                            </div>

                            <div class="form-group  col-md-12">
                                <label><input type="checkbox" name="payadvice" value="1"> Payment Advice Pending</label>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="upload_img" class="btn btn-info" >Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="request_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Request</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="edit-request-form">

                        <div class="modal-body">
                            <input type="hidden" name="intype" value="Drona"/>
                            <input type="hidden" name="cid" value="<?php echo $invoice[0]->di_id; ?>"/>
                            <input type="hidden" name="inumber" value="<?php
                            if ($invoice[0]->di_onlyDebit == '0') {
                                echo $invoice[0]->di_number;
                            } else {
                                echo $invoice[0]->di_debitNumber;
                            }
                            ?>"/>
                            <input type="hidden" name="client" value="<?php echo $invoice[0]->client_name; ?>"/>
                            <input type="hidden" name="project" value="Drona Invoice <?php
                            if (!empty($invoice[0]->di_type)) {
                                echo $invoice[0]->di_type;
                            } else {
                                echo $invoice[0]->di_project;
                            }
                            ?> "/>
                            <input type="hidden" name="pdate" value="<?php
                            if ($invoice[0]->di_month != 'NA') {
                                $dt = DateTime::createFromFormat('!m', $invoice[0]->di_month);
                                echo $dt->format('F') . '-' . $invoice[0]->di_year;
                            } else {
                                echo 'NA';
                            }
                            ?>"/>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Send Request To  <sup>*</sup></span>

                                    <select  name="email"  class="form-control">
                                        <option value="uday@wagonslearning.com">Uday Shetty</option>
                                        <option value="raviraj@wagonslearning.com">Raviraj Poojary</option>
                                        <option value="sourabh.shah@wagonslearning.com">Sourabh Shah</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Edit Reason </span>
                                    <input type="text" name="reason"  class="form-control" placeholder="Edit Reason">
                                </div>
                            </div>

                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" >Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="upload_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Upload Scan copy of invoice</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="upload-form">

                        <div class="modal-body">
                            <input type="hidden" name="upCid" value="<?php echo $invoice[0]->di_id; ?>"/>
                            <input type="hidden" name="upPid" value=""/>

                            <div class="form-group col-md-12">
                                <div>
                                    <span> Scan Copy </span>
                                    <input type="file" name="img[]" multiple="multiple" accept="image/*,application/pdf"/>
                                </div>
                            </div>

                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="upload_img" class="btn btn-info" >Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/additional-methods.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
                                    $(document).ready(function () {
                                        
                                        $('.cancel-invoice-btn').click(function(e){
                                            e.preventDefault();
                                            var cid = $(this).attr('cid');
                                            var f = confirm("Are you sure want to cancel invoice ?");
                                            if(f==true){
                                                 $('.page_spin').show();
                                                var dataString = "cid=" + cid + "&page=cancelDInvoice";
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>admin/ajax_page",
                                                    data: dataString,
                                                    success: function (data) {
                                                        $('.page_spin').hide();
                                                        window.location.reload();
                                                    }, //success fun end
                                                });//ajax end
                                            }
                                        });

                                        $("#pay-form").validate({
                                            rules: {
                                                amt: "required",
                                                date: "required",
                                            },
                                            tooltip_options: {
                                                inst: {
                                                    trigger: 'focus',
                                                },
                                            }
                                        });

                                        $("#edit-request-form").validate({
                                            rules: {
                                                email: "required",
                                                reason: "required",
                                            },
                                            tooltip_options: {
                                                inst: {
                                                    trigger: 'focus',
                                                },
                                            }
                                        });

                                        $("#upload-form").validate({
                                            rules: {
                                                'img[]': "required",
                                            },
                                            tooltip_options: {
                                                inst: {
                                                    trigger: 'focus',
                                                },
                                            }
                                        });

                                        $('.make-payment-btn').click(function (e) {
                                            $('#pay_wraper').modal('show');
                                        });


                                        $('.edit-request-btn').click(function (e) {
                                            e.preventDefault();
                                            $('#request_wraper').modal('show');
                                        });

                                        $('.upload-btn').click(function (e) {
                                            e.preventDefault();
                                            $('#upload_wraper').modal('show');
                                        });

                                        $('.add-exp-btn').click(function (e) {
                                            e.preventDefault();
                                            $('#expWraper').modal('show');
                                        });

                                        $("#expForm").validate({
                                            rules: {
                                                desc: "required",
                                                amount: {
                                                    required: true,
                                                    number: true
                                                }
                                            },
                                            tooltip_options: {
                                                inst: {
                                                    trigger: 'focus',
                                                },
                                            }
                                        });

                                        $("#creatinForm").validate({
                                            rules: {
                                                company: "required",
                                                client: "required",
                                                gst: "required"
                                            },
                                            tooltip_options: {
                                                inst: {
                                                    trigger: 'focus',
                                                },
                                            }
                                        });

                                        $('#data-list').DataTable();

                                        $('.edit-btn').click(function (e) {
                                            e.preventDefault();
                                            $('#clientWraper').modal('show');
                                        });

                                        $('#payad').change(function (e) {
                                            var cid = $(this).attr('cid');
                                            var status = '0';
                                            if ($(this).prop("checked") == true) {
                                                status = '1';
                                            }
                                            $('.page_spin').show();
                                            var dataString = "cid=" + cid + "&status=" + status + "&page=updatePayAdvice";
                                            $.ajax({
                                                type: "POST",
                                                url: "<?php echo base_url(); ?>admin/ajax_page",
                                                data: dataString,
                                                success: function (data) {
                                                    $('.page_spin').hide();
                                                }, //success fun end
                                            });//ajax end
                                        });

                                        $('#client').change(function () {
                                            var client = $(this).val();
                                            if (client != '') {
                                                $('.page_spin').show();
                                                var dataString = "cid=" + client + "&page=get_client_location";
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>admin/ajax_page",
                                                    data: dataString,
                                                    success: function (data) {
                                                        $('.page_spin').hide();
                                                        $('.load-gst').html(data);
                                                    }, //success fun end
                                                });//ajax end
                                            }
                                        });

                                        $('.remove-pay').click(function (e) {
                                            e.preventDefault();
                                            var rid = $(this).attr('rid');
                                            var f = confirm("Are you sure want to remove ?");
                                            if (f == true) {
                                                var ele = $(this).parent('td').parent('tr');
                                                $('.page_spin').show();
                                                var dataString = "rid=" + rid + "&page=remove_client_payment";
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>admin/ajax_page",
                                                    data: dataString,
                                                    success: function (data) {
                                                        $('.page_spin').hide();
                                                        ele.remove();
                                                    }, //success fun end
                                                });//ajax end
                                            }
                                        });

                                        $('.remove-exp').click(function (e) {
                                            e.preventDefault();
                                            var eid = $(this).attr('eid');
                                            var f = confirm("Are you sure want to remove expense ?");
                                            if (f == true) {
                                                $('.page_spin').show();
                                                var dataString = "eid=" + eid + "&page=remove_drona_expense";
                                                $.ajax({
                                                    type: "POST",
                                                    url: "<?php echo base_url(); ?>admin/ajax_page",
                                                    data: dataString,
                                                    success: function (data) {
                                                        $('.page_spin').hide();
                                                        window.location.href = "<?php echo base_url(); ?>admin/drona_invoice_details/<?php echo $invoice[0]->di_id; ?>";
                                                                            }, //success fun end
                                                                        });//ajax end
                                                                    }
                                                                });

                                                                $('.confirm-btn').click(function (e) {
                                                                    e.preventDefault();
                                                                    var did = '<?php echo $invoice[0]->di_id; ?>';
                                                                    var inamt = '<?php echo $inTotal; ?>';
                                                                    var insub = '<?php echo $inAmt; ?>';
                                                                    var ingst = '<?php echo $inGST; ?>';
                                                                    var debitamt = '<?php echo $debitTotal; ?>';
                                                                    var debitsub = '<?php echo $debitAmt; ?>';
                                                                    var debitgst = '<?php echo $debitGST; ?>';
                                                                    var f = confirm("Are you sure want to confirm ?");
                                                                    if (f == true) {
                                                                        $('.page_spin').show();
                                                                        var dataString = "insub=" + insub + "&ingst=" + ingst + "&debitsub=" + debitsub + "&debitgst=" + debitgst + "&did=" + did + "&inamt=" + inamt + "&debitamt=" + debitamt + "&page=confirm_drona_invoice";
                                                                        $.ajax({
                                                                            type: "POST",
                                                                            url: "<?php echo base_url(); ?>admin/ajax_page",
                                                                            data: dataString,
                                                                            success: function (data) {
                                                                                $('.page_spin').hide();
                                                                                window.location.href = "<?php echo base_url(); ?>admin/drona_invoice_details/<?php echo $invoice[0]->di_id; ?>";
                                                                                                    }, //success fun end
                                                                                                });//ajax end
                                                                                            }
                                                                                        });

                                                                                    });
        </script>

    </body>
</html>