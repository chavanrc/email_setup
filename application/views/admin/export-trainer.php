<table border="1px">
    <thead>
        <tr>
            <th>#</th>
            <th >Name</th>
            <th >Company</th>
            <th >Address</th>
            <th >City</th>
            <th >State</th>
            <th >Email</th>
            <th >Contact</th>
            <th >Freelancer</th>
            <th >Industry</th>
            <th >Trainer Area</th>
            <th >Total Experience</th>
            <th >Expected Fees</th>
            <th >Certifications</th>
            <th >DOB</th>
            <th >Gender</th>
            <th >Admin Comments</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($trainer)) {
            $num = 0;
            foreach ($trainer as $tr_data) {
                $num++;
                ?>
                <tr>
                    <td><?php echo $num; ?></td>
                    <td><?php echo $tr_data->name; ?></td>
                    <td><?php echo $tr_data->company_name; ?></td>
                    <td><?php echo $tr_data->address; ?></td>
                    <td><?php echo $tr_data->city; ?></td>
                    <td><?php echo $tr_data->state; ?></td>
                    <td><?php echo $tr_data->email; ?></td>
                    <td><?php echo $tr_data->contact_number; ?></td>
                    <td><?php echo $tr_data->freelancer; ?></td>
                    <td><?php echo $tr_data->industry; ?></td>
                    <td><?php echo $tr_data->trainer_area; ?></td>
                    <td><?php echo $tr_data->total_experiance; ?></td>
                    <td><?php echo $tr_data->expected_fee_per_day; ?></td>
                    <td><?php echo $tr_data->trainer_certifications; ?></td>
                    <td><?php echo $tr_data->user_dob; ?></td>
                    <td><?php echo $tr_data->user_gender; ?></td>
                    <td><?php echo $tr_data->admin_comments; ?></td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>