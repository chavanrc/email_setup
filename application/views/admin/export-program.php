<table border="1px">
    <thead>
        <tr style="background: #337ab7; color: #fff;">
            <th></th>
            <th>Company Name</th>
            <th >Client Name</th>
            <th >PM</th>
            <th >Program Name</th>
            <th >Program Prof. Fees</th>
            <th >Duration</th>
            <th >Venue</th>
            <th >City</th>
            <th >State</th>
            <th >Program Start Date</th>
            <th >Program End Date</th>
            <th >Trainer Name</th>
            <th >Trainer Prof. Fees</th>
            <th >GST</th>
            <th >Trainer Expenses Reimbursed</th>
            <th >Trainer Location</th>
            <th >Trainer Payment Due</th>
            <th >Trainer Payment Status</th>
            <th >Trainer Travel</th>
            <th >Travel Amount</th>
            <th >Trainer Stay</th>
            <th >Stay Amount</th>
            <th >Trainer  Invoice</th>
            <th >Invoice Print</th>
            <th >Client Invoice</th>
            <th >Invoice Date</th>
            <th >Invoice Number</th>
            <th >Debit Note Number</th>
            <th >Prof. Fees</th>
            <th >GST On Prof. Fees</th>
            <th >Gross Prof. Fees</th>
            <th >Debit Note Amount</th>
            <th >GST On Debit Note</th>
            <th >Gross Debit Note</th>
            <th >Total Amount</th>
            <th >Payment Status</th>
            <th >Debit Payment Status</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($programs)) {
            $instatus = 0;
            $dateStatus = "";
            $num = 0;
            foreach ($programs as $tr_data) {
                $num++;
                $invoice = "";
                $dueDate = date('Y-m-d', strtotime($tr_data->trainer_date_to . ' + ' . $tr_data->trainer_credit . ' days')) . " 00:00";
                $pdate = explode('-', $tr_data->training_start_date);
                $matchDate = $pdate[0] . "-" . $pdate[1];
                if ($tr_data->invoice_type == 'Consolidated') {
                    if ($instatus != $tr_data->client_id || $dateStatus != $matchDate) {
                        $dateStatus = $matchDate;
                        $invoice = $this->admin_model->get_consInvoiceExport($tr_data->client_id, $pdate[0], $pdate[1]);
                        if (!empty($invoice)) {
                            $instatus = $tr_data->client_id;
                        }
                    }
                } else {
                    $invoice = $this->admin_model->get_singleclient_invoice($tr_data->project_id);
                }
                ?>
                <tr>
                    <td><?php echo $pdate[0] . "-" . $pdate[1]; ?></td>
                    <td><?php echo $tr_data->company_name; ?></td>
                    <td><?php echo $tr_data->client_name; ?></td>
                    <td><?php echo $tr_data->pname; ?></td>
                    <td><?php echo $tr_data->project_title; ?></td>
                    <td><?php
                        if ($tr_data->client_charges == '0') {
                            echo 'NA';
                        } else {
                            echo $tr_data->client_charges * $tr_data->training_duration;
                        }
                        ?></td>
                    <td><?php echo $tr_data->training_duration; ?> Days</td>
                    <td><?php echo $tr_data->venue; ?></td>
                    <td><?php echo $tr_data->location_of_training; ?></td>
                    <td><?php echo $tr_data->program_state; ?></td>
                    <td><?php echo $tr_data->training_start_date; ?></td>
                    <td><?php echo $tr_data->training_end_date; ?></td>
                    <td><?php echo $tr_data->name; ?></td>
                    <td><?php
                        if ($tr_data->amount == '0') {
                            echo 'NA';
                        } else {
                            echo $tr_data->amount * $tr_data->training_duration;
                        }
                        ?></td>
                    <td>0</td>
                    <td><?php
                        if ($tr_data->amount == '0') {
                            echo 'NA';
                        } else {
                            echo $tr_data->trainer_debit;
                        }
                        ?></td>
                    <td><?php echo $tr_data->city; ?></td>
                    <td><?php
                        if ($tr_data->amount == '0') {
                            echo 'NA';
                        } else {
                            echo $dueDate;
                        }
                        ?></td>
                    <td><?php
                        if ($tr_data->amount == '0') {
                            echo 'NA';
                        } else {
                            if ($tr_data->trainer_paid_status == 0) {
                                echo 'Pending';
                            } else {
                                echo 'Paid';
                            }
                        }
                        ?></td>
                    <td><?php
                        if ($tr_data->travel_arrangement == 'None') {
                            echo 'NA';
                        } else {
                            echo $tr_data->travel_arrangement;
                        }
                        ?></td>
                    <td><?php echo $tr_data->travelAmount; ?></td>
                    <td><?php
                        if ($tr_data->stay_arrangement == 'None') {
                            echo 'NA';
                        } else {
                            echo $tr_data->stay_arrangement;
                        }
                        ?></td>
                    <td><?php echo $tr_data->stayAmount; ?></td>
                    <td>
                        <?php
                        if ($tr_data->trainer_invoice_flag == 0) {
                            echo 'Pending';
                        } else if ($tr_data->trainer_invoice_action == 0 || $tr_data->trainer_invoice_action == '') {
                            echo 'PM Approval Pending';
                        } else if($tr_data->trainer_invoice_action == 2){
                            echo 'PM Approved';
                        } else if($tr_data->trainer_invoice_action == 1) {
                            echo 'Support Approved';
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($tr_data->invoice_print == 0) {
                            echo 'Not';
                        } else {
                            echo 'Taken';
                        }
                        ?>
                    </td>
                    <?php if (!empty($invoice)) { ?>
                        <td><?php
                            if ($invoice[0]->ci_status == '1') {
                                echo 'Generated';
                            } else {
                                echo 'Inprocess';
                            }
                            ?></td>
                        <td><?php
//                            if ($tr_data->invoice_type != 'Consolidated') {
                            echo $invoice[0]->ci_date;
//                            } else {
//                                echo 'Consolidated';
//                            }
                            ?></td>
                        <td><?php echo $invoice[0]->ci_number; ?></td>
                        <td><?php
                            if ($invoice[0]->ci_debit_id != '0') {
                                echo $invoice[0]->ci_no_frmt . 'D/' . $invoice[0]->ci_debit_id;
                            }
                            ?></td>
                        <td><?php echo $invoice[0]->ci_amt; ?></td>
                        <td><?php echo $invoice[0]->ci_gstamt; ?></td>
                        <td><?php echo $total = $invoice[0]->ci_gstamt + $invoice[0]->ci_amt; ?></td>
                        <td><?php echo $invoice[0]->ci_damt; ?></td>
                        <td><?php echo $invoice[0]->ci_dgstamt; ?></td>
                        <td><?php echo $dtotal = $invoice[0]->ci_dgstamt + $invoice[0]->ci_damt; ?></td>
                        <td><?php echo $dtotal + $total; ?></td>
                        <td><?php
                            if ($tr_data->client_payment_receieved == 'Y') {
                                echo 'Paid';
                            } else {
                                echo 'Pending';
                            }
                            ?></td>
                        <td><?php
                            if ($invoice[0]->ci_debit_id != '0') {
                                if ($tr_data->client_debit_received == 'Y') {
                                    echo 'Paid';
                                } else {
                                    echo 'Pending';
                                }
                            } else {
                                echo 'NA';
                            }
                            ?></td>
                    <?php } else {
                        ?>
                        <td><?php
                            if ($tr_data->client_charges == '0') {
                                echo 'NA';
                            } else {
                                if ($instatus == $tr_data->client_id) {
                                    echo 'Generated';
                                } else {
                                    echo 'Pending';
                                }
                            }
                            ?></td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                        <td>NA</td>
                    <?php }
                    ?>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>