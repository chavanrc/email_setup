<?php
$CI = & get_instance();
$CI->load->model('admin_model');
?>
<div class="nav-side-menu no-print">
    <div class="brand">
        <img src="<?php echo base_url(); ?>assets/images/wagons-logo.png"/>
    </div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

    <div class="menu-list">

        <ul id="menu-content" class="menu-content collapse out">
            <?php if ($this->session->userdata('type') == 'admin') { ?>
                <li <?php if ($page_url == 'Dashboard') { ?> class="active" <?php } ?>>
                    <a href="<?php echo base_url(); ?>admin/">
                        <i class="fa fa-dashboard"></i> Dashboard
                    </a>
                </li>

                <li <?php if ($page_url == 'Clients') { ?> class="active" <?php } ?>>
                    <a href="<?php echo base_url(); ?>admin/clients">
                        <i class="fa fa-users "></i> Clients
                    </a>
                </li>

                <li <?php if ($page_url == 'Project Managers') { ?> class="active" <?php } ?>>
                    <a href="<?php echo base_url(); ?>admin/project_manager">
                        <i class="fa fa-user "></i> Users
                    </a>
                </li>
                <li <?php if ($page_url == 'Trainers') { ?> class="active" <?php } ?>>
                    <a href="<?php echo base_url(); ?>admin/trainers">
                        <i class="fa fa-graduation-cap"></i> Trainers
                    </a>
                </li>
                <li <?php if ($page_url == 'Upcoming Programs') { ?> class="active" <?php } ?>>
                    <a href="<?php echo base_url(); ?>admin/upcoming_programs">
                        <i class="fa fa-calendar-plus-o"></i> Upcoming Programs
                    </a>
                </li>
                <li <?php if ($page_url == 'Past Programs') { ?> class="active" <?php } ?>>
                    <a href="<?php echo base_url(); ?>admin/past_programs">
                        <i class="fa fa-calendar-check-o"></i> Past Programs
                    </a>
                </li>
                <li <?php if ($page_url == 'Trainer Invoices') { ?> class="active" <?php } ?>>
                    <a href="<?php echo base_url(); ?>admin/trainer_invoices">
                        <i class="fa fa-file-text-o"></i> Trainer Invoices
                    </a>
                </li>
                <li <?php if ($page_url == 'Payments') { ?> class="active" <?php } ?>>
                    <a href="<?php echo base_url(); ?>admin/client_invoice">
                        <i class="fa fa-money"></i> Client Invoices
                    </a>
                </li>

                <li <?php if ($page_url == 'Drona') { ?> class="active" <?php } ?>>
                    <a href="<?php echo base_url(); ?>admin/drona_invoice">
                        <i class="fa fa-money"></i> Monthly Invoices
                    </a>
                </li>
                <li <?php if ($page_url == 'Payments') { ?> class="active" <?php } ?>>
                    <a href="<?php echo base_url(); ?>admin/export_all_invoice">
                        <i class="fa fa-money"></i> Export Invoices
                    </a>
                </li>
            <?php } if ($this->session->userdata('type') == 'drona') {
                ?>
                <li <?php if ($page_url == 'Drona') { ?> class="active" <?php } ?>>
                    <a href="<?php echo base_url(); ?>admin/drona_invoice">
                        <i class="fa fa-money"></i> Monthly Invoices
                    </a>
                </li>
            <?php }
            ?>
        </ul>
    </div>
</div>

<div class="page_spin">
    <br/>
    <div class="spin_icon">
        <i class="fa fa-spinner fa-spin"></i><br/>
        <span>One moment ...</span>
    </div>
</div>
