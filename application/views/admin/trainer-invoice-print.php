<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3> Trainer Invoice</h3>
                    </div>
                    
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="col-md-6 content-page" style="font-size: 12px;">
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <h3>Invoice</h3>
                                <?php
                                if (!empty($fees)) {
                                    if (empty($fees[0]->trainer_invoice_file)) {
                                        ?>
                                        <table class="table table-bordered">
                                            <tr style="background:#656565; color: #fff; font-size: 15px;">
                                                <td>Name & Address of Trainer</td>
                                                <td>Invoice No</td>
                                                <td>Date</td>
                                            </tr>
                                            <tr>
                                                <td rowspan="3">
                                                    <?php echo $tDetails[0]->name; ?> <br/>
                                                    <?php echo $tDetails[0]->address; ?> <br/> <br/>
                                                    <?php
                                                    if(!empty($fees[0]->trainer_invoice_gst)){
                                                        ?>
                                                        <strong>GSTIN : <?php echo $fees[0]->trainer_invoice_gst; ?> </strong>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                                <td>T/19-20/<?php echo $fees[0]->id; ?></td>
                                                <td>
                                                    <?php
                                                    if ($fees[0]->trainer_invoice_flag == 1) {
                                                        echo date_formate_short($fees[0]->trainer_invoice_date);
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr style="background:#656565; color: #fff; font-size: 15px;">
                                                <td colspan="2" class="text-center">Terms</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-center"><?php
                                                    $startD = explode(' ', $fees[0]->training_date_from);
                                                    $endD = explode(' ', $fees[0]->trainer_date_to);
                                                    $start = strtotime($startD[0]);
                                                    $end = strtotime($endD[0]);

                                                    $days_between = ceil(abs($end - $start) / 86400);
                                                    $edays = 1;
                                                    if ($fees[0]->training_date_from != $fees[0]->trainer_date_to) {
                                                        $edays = 2;
                                                    }

                                                    echo $tDetails[0]->trainer_credit;
                                                    ?> Days</td>
                                            </tr>
                                            <tr style="background:#656565; color: #fff; font-size: 15px;">
                                                <td> Client </td>
                                                <td colspan="2">Bill To</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Name : <?php echo $pDetails[0]->client_name; ?><br/>
                                                    Program : <?php echo $pDetails[0]->project_title; ?><br/>
                                                    Date : <?php echo date_formate_short($pDetails[0]->training_start_date) ; ?>
													<?php if($pDetails[0]->training_duration > 1) { 
													 echo "to ".date_formate_short($pDetails[0]->training_end_date) ; 
													} ?>
													<br/>
                                                    Location : <?php echo $pDetails[0]->location_of_training; ?><br/>
                                                </td>
                                                <td colspan="2">
                                                    <?php if ($pDetails[0]->company_id == 1) { ?>
                                                    ATTN: Name / Dept : Accounts 	<br/>	
                                                    Wagons Management Consulting.	<br/>	
                                                    A/7-8 , Srushti Apartment Opp Corporation Bank	<br/>	
                                                    Baner Road, Pune - 411007<br/><br/>
                                                    <strong>GSTIN : 27AABFW3629E1Z8 </strong>
                                                <?php } if ($pDetails[0]->company_id == 2) { ?>
                                                    ATTN: Name / Dept : Accounts 	<br/>	
                                                    Wagons Learning Pvt Ltd.	<br/>	
                                                    A/7-8 , Srushti Apartment Opp Corporation Bank	<br/>	
                                                    Baner Road, Pune - 411007<br/><br/>
                                                    <strong>GSTIN : 27AABCW4538P1ZP </strong>
                                                <?php } ?>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="table table-bordered">
                                            <tr style="background:#656565; color: #fff; font-size: 15px;">
                                                <td>Sl. No.</td>
                                                <td colspan="2">Description</td>
                                                <td>Rate</td>
                                                <td>Amount</td>
                                            </tr>
                                            <?php
                                            $no = 1;
                                            if (!empty($fees)) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $no; ?></td>
                                                    <td colspan="2"><?php
                                                        echo 'Professional Fees: ';
                                                         if ($pDetails[0]->half_day == '1') 
														echo "(Half Day Program)"; 
													 else 
													     echo $pDetails[0]->training_duration . " Day(s)";
                                                        ?></td>
                                                    <td align="right"><?php echo $fees[0]->amount; ?></td>
                                                    <td align="right"><?php echo number_format(round($fees[0]->amount * $pDetails[0]->training_duration, 2),2); ?></td>
                                                </tr>
                                                <?php
                                            }
                                            
                                            $professional = round($fees[0]->amount * $pDetails[0]->training_duration, 2);
                                            
                                            $gst = 0;
                                            if (!empty($fees[0]->trainer_invoice_gst)) {
                                                ?>
                                                <tr>
                                                    <td></td>
                                                    <td colspan="2"></td>
                                                    <td>CGST @ 9%</td>
                                                    <td>
                                                        <?php
                                                        if ($fees[0]->trainer_invoice_code == '27') {
                                                            $cgst = round(0.09 * $professional, 2);
															echo number_format($cgst,2);
                                                            $gst = $cgst;
                                                        } else {
                                                            echo '-';
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td colspan="2"></td>
                                                    <td>SGST @ 9%</td>
                                                    <td><?php
                                                        if ($fees[0]->trainer_invoice_code == '27') {
                                                            $sgst = round(0.09 * $professional, 2);
															echo number_format($sgst,2);
                                                            $gst = $gst + $sgst;
                                                        } else {
                                                            echo '-';
                                                        }
                                                        ?></td>
                                                </tr>
                                                <tr>
                                                    <td></td>
                                                    <td colspan="2"></td>
                                                    <td>IGST @ 18%</td>
                                                    <td align="right">
                                                        <?php
                                                        if ($fees[0]->trainer_invoice_code != '27') {
                                                             $igst = round(0.18 * $professional, 2);
															 echo number_format($igst,2);
                                                            $gst = $igst;
                                                        } else {
                                                            echo '-';
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                            <tr style="font-size: 18px;">
                                                <td colspan="4" class="text-right">Total</td>
                                                <td align="right">
                                                    <?php
                                                     $itotal = round($professional + $gst);
													 echo number_format($itotal,2);
                                                    $fTotal = $itotal;
                                                    ?>/-
                                                </td>
                                            </tr>
                                            
                                            <tr><td colspan="5"><strong>In Words: 
                                                        <?php
                                                        $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                                        echo "Rs. " . ucfirst($f->format($itotal)) . " Only.";
                                                        ?>
                                                    </strong></td></tr>
                                        </table>
                                        <?php
                                    } 
                                }
                                ?>
                                <table class="table table-bordered">
                                    <tr style="background:#656565; color: #fff; font-size: 15px;">
                                        <td colspan="2">
                                            Bank Details
                                        </td>
                                    </tr>
                                    <?php
                                    if (!empty($bank)) {
                                        ?>
                                        <tr>
                                            <td>Bank Name</td>
                                            <td><?php echo $bank[0]->bank_name; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Account Number</td>
                                            <td><?php echo $bank[0]->bank_account; ?></td>
                                        </tr>
                                        <tr>
                                            <td>IFSC Code</td>
                                            <td><?php echo $bank[0]->bank_ifsc; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Beneficiary Name</td>
                                            <td><?php echo $bank[0]->account_name; ?></td>
                                        </tr>
                                        <tr>
                                            <td>PAN No.</td>
                                            <td><?php echo $bank[0]->pan_no; ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </table>
                                
                            </div>
                        </div>
                    </div>
                    <?php if (!empty($debit)) { ?>
                        <div style="page-break-after: always;"></div>
                        <div class="col-md-6 content-page" style="font-size: 12px;">
                            <div class="panel panel-default">

                                <div class="panel-body">
                                    <div>
                                        <h3 style="display:inline-block;">Debit Note</h3>

                                        <div class="clearfix"></div>
                                    </div>
                                    <?php if (!empty($fees)) { ?>
                                        <table class="table table-bordered">
                                            <tr style="background:#656565; color: #fff; font-size: 15px;">
                                                <td>Name & Address of Trainer</td>
                                                <td>Invoice No</td>
                                                <td>Date</td>
                                            </tr>
                                            <tr>
                                                <td rowspan="3">
                                                    <?php echo $tDetails[0]->name; ?> <br/>
                                                    <?php echo $tDetails[0]->address; ?>
                                                </td>
                                                <td>T/19-20/D/<?php echo $fees[0]->id; ?></td>
                                                <td>
                                                    <?php
                                                    if ($fees[0]->trainer_invoice_flag == 1) {
                                                        echo date_formate_short($fees[0]->trainer_invoice_date);
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr style="background:#656565; color: #fff; font-size: 15px;">
                                                <td colspan="2" class="text-center">Terms</td>
                                            </tr>
                                            <tr>
                                                <td colspan="2" class="text-center"><?php
                                                    $startD = explode(' ', $fees[0]->training_date_from);
                                                    $endD = explode(' ', $fees[0]->trainer_date_to);
                                                    $start = strtotime($startD[0]);
                                                    $end = strtotime($endD[0]);

                                                    $days_between = ceil(abs($end - $start) / 86400);
                                                    $edays = 1;
                                                    if ($fees[0]->training_date_from != $fees[0]->trainer_date_to) {
                                                        $edays = 2;
                                                    }
                                                    //echo $days_between + $edays;
                                                    echo $tDetails[0]->trainer_credit;
                                                    ?> Days</td>
                                            </tr>
                                            <tr style="background:#656565; color: #fff; font-size: 15px;">
                                                <td> Client </td>
                                                <td colspan="2">Bill To</td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    Name : <?php echo $pDetails[0]->client_name; ?><br/>
                                                    Program : <?php echo $pDetails[0]->project_title; ?><br/>
                                                    Date : <?php echo $pDetails[0]->training_start_date; ?><br/>
                                                    Location : <?php echo $pDetails[0]->location_of_training; ?><br/>
                                                </td>
                                                <td colspan="2">
                                                    <?php if ($pDetails[0]->company_id == 1) { ?>
                                                    ATTN: Name / Dept : Accounts 	<br/>	
                                                    Wagons Management Consulting.	<br/>	
                                                    A/7-8 , Srushti Apartment Opp Corporation Bank	<br/>	
                                                    Baner Road, Pune - 411007<br/><br/>
                                                  
                                                <?php } if ($pDetails[0]->company_id == 2) { ?>
                                                    ATTN: Name / Dept : Accounts 	<br/>	
                                                    Wagons Learning Pvt Ltd.	<br/>	
                                                    A/7-8 , Srushti Apartment Opp Corporation Bank	<br/>	
                                                    Baner Road, Pune - 411007<br/><br/>
                                                  
                                                <?php } ?>
                                                </td>
                                            </tr>
                                        </table>
                                        <table class="table table-bordered">
                                            <tr style="background:#656565; color: #fff; font-size: 15px;">
                                                <td>Sl. No.</td>
                                                <td colspan="2">Description</td>

                                                <td>Amount</td>
                                            </tr>
                                            <?php
                                            $no = 0;

                                            $subTotal = 0;
                                            if (!empty($debit)) {
                                                foreach ($debit as $d_data) {
                                                    $no++;
                                                    ?>
                                                    <tr <?php if ($d_data->debit_status == '0') { ?> class="no-print" <?php } ?> >
                                                        <td><?php echo $no; ?></td>
                                                        <td><?php echo $d_data->debit_title; ?> <br/>
                                                            <?php echo $d_data->debit_desc; ?>
                                                        </td>
                                                        <td>
                                                            
                                                        </td>

                                                        <td align="right">
                                                            <?php
                                                            echo number_format($d_data->debit_amt,2);
                                                            if ($d_data->debit_status == '1') {
                                                                $subTotal = $subTotal + $d_data->debit_amt;
                                                            } 
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            $total = $subTotal;
                                            ?>
                                            <tr style="font-size: 18px;">
                                                <td colspan="3" class="text-right">Total</td>
                                                <td align="right">
                                                    <?php echo number_format($total,2); ?>/-
                                                </td>
                                            </tr>
                                            <tr><td colspan="4"><strong>In Words: 
                                                        <?php
                                                        //$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                                        echo "Rs. " . ucfirst($f->format($total)) . " Only.";
                                                        ?>
                                                    </strong></td></tr>
                                        </table>
                                    <?php } ?>
									
									
                                </div>
								
                            </div>
							
                        </div>
                    <?php } ?>
                    <div class="clearfix"></div>

                    <?php if (!empty($payment)) { ?>

                        <div class="col-md-12 content-page">
                            <div class="panel panel-default">
                                <div class="panel-body">
                                    <div>
                                        <h3 style="display:inline-block;">Payment Details</h3>
                                        <div class="clearfix"></div>
                                    </div>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Amount Paid</th>
                                                <th>Date</th>
                                                <th>Payment Notes</th>
                                                <th>Payment Advice</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?php echo $payment[0]->tp_amt; ?></td>
                                                <td><?php echo $payment[0]->tp_pay_date; ?></td>
                                                <td><?php echo $payment[0]->tp_pay_id; ?></td>
                                                <td><?php
                                                    if (!empty($payment[0]->tp_advice)) {
                                                        ?>
                                                        <a download="advice" href="<?php echo base_url(); ?>assets/upload/content/<?php echo $payment[0]->tp_advice; ?>" class="btn btn-primary">Download</a>
                                                        <?php
                                                    }
                                                    ?></td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    <?php } ?>

                </div>
            </div>
        </div>

        
        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {

                window.print();

            });
        </script>

    </body>
</html>