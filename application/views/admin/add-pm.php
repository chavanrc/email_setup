<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Add Project Manager</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>New User</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>admin/project_manager" class="btn btn-danger"><i class="fa fa-backward"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <?php
                if ($msg != 'error' && $msg != 'new') {
                    ?>
                    <div class="alert alert-success col-md-4 col-md-offset-4">
                        User Added Successfully.
                    </div>


                    <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title" ><i class="fa fa-arrow-right"></i> Assign Clients </h3>
                            </div>
                            <div class="panel-body">
                                <form action="" method="POST"  enctype="multipart/form-data" id="assign-pm-form">
                                    <input type="hidden" value="<?php echo $msg; ?>" id="pm" name="pm"/>
                                    <div class="form-group">
                                        <div class="col-md-6 col-md-offset-3">
                                            <select class="form-control" id="cid" name="cid">
                                                <option value=""> - Select Client -</option>
                                                <?php
                                                foreach ($client as $cl_data) {
                                                    ?>
                                                    <option value="<?php echo $cl_data->client_id; ?>"> <?php echo $cl_data->client_name; ?> </option>
                                                    <?php
                                                }
                                                ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                            <a href="<?php echo base_url(); ?>admin/project_manager" class="btn btn-warning">Assign Later</a>
                                            <button type="reset" class="btn btn-default">Reset</button>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>


                    <?php
                }
                else
                {
                ?>

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-plus"></i> Create User </h2>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 'error') {
                                ?>
                                <div class="alert alert-danger col-md-4 col-md-offset-4">
                                    This email id already exist.
                                </div>
                                <?php
                            }
                            ?>
                            <div class="clearfix"></div>
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-pm-form">
				<div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">User <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="user_type">
                                            <option value=""> - Select -</option>
                                            <option value="project manager">Project Manager</option>
					   <option value="business manager">BDM</option>
					   <option value="content manager">Content Manager</option>
					   <option value="admin support">Admin Support</option>
                        <option value="training support">Training Support</option>                     
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-user"></i></span>
                                            <input type="text" name="fname" class="form-control" placeholder="Full Name">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Email id <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-envelope"></i></span>
                                            <input type="text" name="email" class="form-control" placeholder="Email id">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Contact Number <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-phone"></i></span>
                                            <input type="text" name="mobile" class="form-control" placeholder="Mobile Number">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Password <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-key"></i></span>
                                            <input type="password" name="password" class="form-control" placeholder="Password">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Photo
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="photo" accept='image/*' class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php
                }
                ?>
            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">

            $(document).ready(function () {
                $("#add-pm-form").validate({
                    rules: {
                        fname: "required",
                        password: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        mobile: {
                            required: true,
                            number: true
                        },
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#assign-pm-form").validate({
                    rules: {
                        cid: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        assign_pm();
                    }
                });

                function assign_pm()
                {
                    var cid = $('#cid').val();
                    var pm = $('#pm').val();

                    $('.page_spin').show();
                    var dataString = "cid=" + cid + "&pm=" + pm + "&page=assign_pm";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.href = "<?php echo base_url(); ?>admin/project_manager";

                        }, //success fun end
                    });//ajax end
                }

            });

        </script>

    </body>
</html>