<table border="1">
    <thead>
        <tr>
            <th>Client</th>
            <th>Invoice No.</th>
            <th>Month</th>
            <th>Invoice Date</th>
            <th>Program</th>
            <th>Amount</th>
            <th>GST</th>
            <th>Grand Total</th>
            <th>Payment Status</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($invoice)) {
            foreach ($invoice as $indata) {
                ?>
                <tr>
                    <td><?php echo $indata->client_name; ?></td>
                    <td>
                        <?php
                        if ($indata->di_onlyDebit == '0') {
                            echo $indata->di_number;
                        } else {
                            echo $indata->di_debitNumber;
                        }
                        ?>
                    </td>
                    <td>
                        <?php
                        if ($indata->di_month != 'NA') {
                            $dt = DateTime::createFromFormat('!m', $indata->di_month);
                            echo $dt->format('F') . '-' . $indata->di_year;
                        } else {
                            echo 'NA';
                        }
                        ?>
                    </td>
                    <td><?php echo date_formate_short($indata->di_date); ?></td>
                    <td>
                        <?php
                        if (!empty($indata->di_type)) {
                            echo $indata->di_type;
                        } else {
                            echo $indata->di_project;
                        }
                        ?>
                    </td>

                    <?php
                    if (!empty($indata->di_number)) {
                        ?>
                        <td><?php echo $indata->di_subtotal; ?></td>
                        <td><?php echo $indata->di_gstamt; ?></td>
                        <td><?php echo $indata->di_amount; ?></td>
                        <?php
                    } else {
                        ?>
                        <td><?php echo $indata->di_debit_subtotal; ?></td>
                        <td><?php echo $indata->di_debit_gstamt; ?></td>
                        <td><?php echo $indata->di_debit_amount; ?></td>
                        <?php
                    }
                    ?>

                    <td>
                        <?php
                        if ($indata->di_status == 0) {
                            echo 'Not Confirmed';
                        }
                        if ($indata->di_status == 1) {
                            if (!empty($indata->di_number) && $indata->di_pay_status == '0') {
                                echo 'Payment Pending';
                            } else if (!empty($indata->di_debitNumber) && $indata->di_debit_status == '0') {
                                echo 'Debit Pay Pending';
                            } else {
                                echo 'Paid';
                            }
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>