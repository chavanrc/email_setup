<!DOCTYPE html>
<html lang="en">    
    <head>        
        <meta charset="utf-8">        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">        
        <meta name="viewport" content="width=device-width, initial-scale=1">        
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->        <title>Admin | Payments</title>        <?php include 'css_files.php'; ?>        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">    </head>    <body>        <?php include 'admin_sidemenu.php'; ?>        <div class="right-side">            <?php include 'admin_topmenu.php'; ?>            <div class="row" style="margin: 0px;">                
                <div class="col-md-12">                    
                    <div class="page-title title-left">                        <h3>Invoice</h3>                    </div>                    
                    <div class="page-title title-right text-right">                        
                        <a href="#" class="create-invoice-btn btn btn-danger">Back</a>                    </div>                    
                    <div class="clearfix"></div>                
                </div>                
                <div class="col-md-12 content-page">                    
                    <div class="panel panel-default">                        
                        <div class="panel-body">                            
                            <div class="col-md-6">                                
                                <?php if ($client[0]->invoice_type == 'Program') { ?>                                    
                                    <form action="" method="POST" id="pCreateinvoice">                                        
                                        <input type="hidden" name="cid" value="<?php echo $client[0]->client_id; ?>"/>                                        
                                        <input type="hidden" name="cm" value="<?php echo $_POST['company']; ?>"/>                                        
                                        <input type="hidden" name="gst" value="<?php echo $_POST['gst']; ?>"/>                                        
                                        <input type="hidden" name="poNum" value="<?php echo $_POST['poNum']; ?>"/>                                        
                                        <input type="hidden" name="refNum" value="<?php echo $_POST['refNum']; ?>"/>                                        
                                        <div class="input-group">                                            
                                            <span class="input-group-addon" id="sizing-addon1">Project <sup>*</sup></span>                                            
                                            <select class="form-control" name="project" required>                                                
                                                <option value=""> - select - </option>                                                
                                                <?php
                                                $project = $CI->admin_model->get_client_basicProject($client[0]->client_id);
                                                if (!empty($project)) {
                                                    foreach ($project as $p_data) {
                                                        ?>                                                        
                                                        <option value="<?php echo $p_data->project_id; ?>"><?php echo $p_data->project_title; ?></option>                                                        
                                                        <?php
                                                    }
                                                }
                                                ?>                                            
                                            </select>                                        
                                        </div>                                        
                                        <div class="text-right" style="margin-top:15px;">                                            
                                            <button type="submit" class="btn btn-primary">Submit</button>                                        
                                        </div>                                    </form>                                
                                <?php } else { ?>                                    
                                    <form action="" method="POST" id="mCreateinvoice">                                        
                                        <input type="hidden" name="cid" value="<?php echo $client[0]->client_id; ?>"/>                                        
                                        <input type="hidden" name="gst" value="<?php echo $_POST['gst']; ?>"/>                                        
                                        <input type="hidden" name="cm" value="<?php echo $client[0]->company_id; ?>"/>                                        
                                        <input type="hidden" name="poNum" value="<?php echo $_POST['poNum']; ?>"/>                                        
                                        <input type="hidden" name="refNum" value="<?php echo $_POST['refNum']; ?>"/>                                        
                                        <div class="input-group">                                            
                                            <span class="input-group-addon" id="sizing-addon1">From Date <sup>*</sup></span>                                            
<!--                                            <select class="form-control" name="year" required>                                                
                                                <option value="2022"> 2022 </option>                                                
                                                <option value="2021"> 2021 </option>                                                
                                                <option value="2020"> 2020 </option>                                                
                                                <option value="2019"> 2019 </option>                                                
                                                <option value="2018"> 2018 </option>                                                                                            
                                            </select>                                        -->
                                            <input type="date" name="fdate" class="form-control" required/>
                                        </div>                                        
                                        <div class="input-group" style="margin-top:15px;">                                            
                                            <span class="input-group-addon" id="sizing-addon1">To Date <sup>*</sup></span>                                            
<!--                                            <select class="form-control" name="month" required>                                                
                                                <option value="01"> Jan </option>                                                
                                                <option value="02"> Feb </option>                                                
                                                <option value="03"> Mar </option>                                                
                                                <option value="04"> Apr </option>                                                
                                                <option value="05"> May </option>                                                
                                                <option value="06"> Jun </option>                                                
                                                <option value="07"> Jul </option>                                                
                                                <option value="08"> Aug </option>                                                
                                                <option value="09"> Sep </option>                                                
                                                <option value="10"> Oct </option>                                                
                                                <option value="11"> Nov </option>                                                
                                                <option value="12"> Dec </option>                                                                                            
                                            </select>                                        -->
                                            <input type="date" name="tdate" class="form-control" required/>
                                        </div>                                        
                                        <div class="text-right" style="margin-top:15px;">                                            
                                            <button type="submit" class="btn btn-primary">Submit</button>                                        
                                        </div>                                    
                                    </form>                                    

                                <?php }
                                ?>                            
                            </div>                        
                        </div>                    
                    </div>                
                </div>            
            </div>        
        </div>        
        <?php include 'js_files.php'; ?>        
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>        
        <script type="text/javascript">
            $(document).ready(function () {
            });
        </script>    
    </body>
</html>