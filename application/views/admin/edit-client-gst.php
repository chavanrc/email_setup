<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Clients</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>GST Numbers for Client</h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" style="display: inline-block;"><i class="fa fa-list"></i>CLient GST  </h2>
                            <a href="<?php echo base_url(); ?>admin/client_gst" class="btn-sm btn-danger pull-right">Back</a>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 1) {
                                ?>
                                <div class="col-md-6 col-md-offset-3">
                                    <div class="alert alert-success">
                                        GST updated successfully
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                            <div class="col-md-6">
                                <form action="" method="POST"  enctype="multipart/form-data" id="gst-form">
                                    <input type="hidden" value="<?php echo $gst[0]->id; ?>" name="gid"/>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"> State </span>
                                            <input type="text" name="state" value="<?php echo $gst[0]->state; ?>" class="form-control" placeholder="State">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"> State Code</span>
                                            <input type="text" name="statecode" value="<?php echo $gst[0]->state_code; ?>"  class="form-control" placeholder="State Code">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"> GST Number </span>
                                            <input type="text" name="gst" value="<?php echo $gst[0]->gst_number; ?>" class="form-control" placeholder="GST Number">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div>
                                            <span> Address </span>
                                            <textarea name="address" class="form-control"><?php echo $gst[0]->client_address; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="import-gst-wraper" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" style="display: inline-block;"><i class="fa fa-arrow-right"></i> Import Client GST</h4>
                        <a href="<?php echo base_url(); ?>assets/Wagons_clients_GST.xls" class="btn-sm btn-warning pull-right" style="margin-right: 20px;">Sample File</a>
                    </div>
                    <div class="modal-body">
                        <form action="" method="POST"  enctype="multipart/form-data" id="assign-pm-form">
                            <?php if (!empty($cid)) { ?>
                                <input type="hidden" value="<?php echo $cid; ?>" id="cid" name="cid"/>
                            <?php } else {
                                ?>
                                <select class="form-control" name="cid" style="margin-bottom: 10px;">
                                    <option value=""> - select client - </option>
                                    <?php
                                    foreach ($client as $cdata) {
                                        ?>
                                        <option value="<?php echo $cdata->client_id; ?>"> <?php echo $cdata->client_name; ?> </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <?php }
                            ?>
                            <div class="form-group">
                                <input type="file" name="gstFile" class="form-control"/>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-8 col-md-offset-2 text-center">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/additional-methods.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {

                // $('#data-list').DataTable();

                $('.import-btn').click(function (e) {
                    e.preventDefault();
                    $('#import-gst-wraper').modal('show');
                });


                $("#assign-pm-form").validate({
                    rules: {
                        cid: "required",
                        gstFile: {
                            required: true,
                            extension: "xls"
                        },
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
//                    submitHandler: function (form) {
//                        assign_pm();
//                    }
                });

            });
        </script>

    </body>
</html>