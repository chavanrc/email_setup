<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
        <style>
            td, th
            {
                font-size:12px;
            }
            .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
                border-top:solid 1px #000 !important;
            }
            .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
                border: 1px solid #000 !important;
            }
            @media print {
                .in-print{
                    margin-top:100px;
                }
            }
        </style>

    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">

                    <div class="page-title col-md-4 no-print">
                        <?php
                        if ($invoice[0]->ci_img != '') {
                            $ciImg = explode(',', $invoice[0]->ci_img);
                            foreach ($ciImg as $ciimgdata) {
                                ?>
                                <a href="<?php echo base_url(); ?>assets/upload/client/<?php echo $ciimgdata; ?>" target="_blank" class="btn btn-warning" style="margin-right:5px;"><i class="fa fa-file-image-o" aria-hidden="true"></i> Invoice Copy</a>
                                <?php
                            }
                        }
                        ?>
                    </div>

                    <div class="page-title col-md-8 text-right no-print">
                        <?php
                        if ($invoice[0]->ci_status == '0') {
                            ?>
                            <a href="<?php echo base_url(); ?>admin/expense/<?php echo $invoice[0]->ci_id; ?>" class="btn btn-success">Edit Details</a>
                            <a href="#" class="btn btn-primary confirm-btn" pid="<?php echo $invoice[0]->ci_project; ?>" inid="<?php echo $invoice[0]->ci_id; ?>">Confirm</a>
                        <?php } else if ($invoice[0]->ci_status == '2') {
                            ?>
                            <p style="color:#ED1122;"><strong>Invoice Cancelled</strong></p>
                            <?php
                        } else {
                            ?>
                            <a href="#"  class="btn btn-success edit-request-btn">Request to Edit</a>
                            <a href="#"  class="btn btn-success upload-btn">Upload Scan Copy</a>

                            <a href="#"  class="make-payment-btn btn btn-primary">Update Payment</a>

                            <a href="#" onclick="window.print();" class="create-invoice-btn btn btn-primary">Print / Download</a>
                            <?php
                        }
                        ?>
                        <a href="#" cid="<?php echo $invoice[0]->ci_id; ?>" pid="<?php echo $invoice[0]->ci_project; ?>" class="cancel-invoice-btn btn btn-warning">Cancel</a>
                        <a href="<?php echo base_url(); ?>admin/client_invoice<?php
                        if (isset($_GET['page'])) {
                            echo '?page=' . $_GET['page'];
                        }
                        ?>" class="create-invoice-btn btn btn-danger">Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-10 content-page">
                    <?php
                    $upAmount = 0;
                    $upGst = 0;
                    if (!empty($erequest)) {
                        ?>
                        <div class="no-print">
                            <strong>Invoice Revision</strong>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Request Send to</th>
                                        <th>Description</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th>Update Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach ($erequest as $er_data) {
                                        ?>
                                        <tr>
                                            <td><?php echo $er_data->er_email; ?></td>
                                            <td><?php echo $er_data->er_desc; ?></td>
                                            <td><?php echo $er_data->er_date; ?></td>
                                            <td>
                                                <?php
                                                if ($er_data->er_status == '0') {
                                                    echo 'Pending';
                                                }
                                                if ($er_data->er_status == '1') {
                                                    echo 'Approved';
                                                }
                                                if ($er_data->er_status == '2') {
                                                    echo 'Rejected';
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($er_data->er_status != '0') {
                                                    echo date_formate($er_data->er_status_date);
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                        <?php
                    }
                    if ($invoice[0]->ci_number != 'NA') {
                        ?>

                        <div class="panel panel-default in-print">
                            <div class="panel-body" >
                                <table class="table table-bordered" >
                                    <tr style="font-size:15px;">
                                        <td colspan="3" style="text-align: center;"><h4 style="font-size:15px;">Tax Invoice <?php if ($invoice[0]->ci_status == '2') { ?><span style="color:#ED1122;"><strong>(Invoice Cancelled)</strong></span><?php } ?></h4>
                                            <?php if ($invoice[0]->include_gst == 'N') {
                                                ?>
                                                <h4 style="font-size:14px;">Supply Meant For Export/Supply To SEZ Unit Or SEZ Developer For Authorized Operations<br/>
                                                    Under Bond Or Letter Of Undertaking Without Payment Of Integrated Tax 
                                                </h4>
                                            <?php }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="5" style="width:350px;">To, <br/>
                                            <?php echo $client[0]->client_name; ?><?php /* for Deusche bank case */ if ($client[0]->client_id == 38) echo ", " . $invoice[0]->ci_state; ?><br/>
                                            <?php
                                            if (!empty($invoice)) {
                                                echo $invoice[0]->ci_gst_address;
                                            }
                                            ?>
                                        </td>


                                        <td>Invoice Number</td>
                                        <td><?php echo $invoice[0]->ci_number; ?><?php //echo $invoice[0]->ci_id;                                               ?></td>
                                    </tr>
                                    <tr>
                                        <td>Invoice Date</td>
                                        <td><?php echo date_formate_short($invoice[0]->ci_date); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Ref. No</td>
                                        <td><?php
                                            if ($client[0]->debit_note == 'Yes' && $invoice[0]->ci_debit_id != '0') {
                                                echo $invoice[0]->ci_no_frmt;
                                                ?>D/<?php
                                                echo str_pad($invoice[0]->ci_debit_id, 3, "0", STR_PAD_LEFT);
                                            } else {
                                                echo 'NA';
                                            }
                                            ?></td>
                                    </tr>
                                    <tr>

                                        <td>Name</td>
                                        <td><?php echo $invoice[0]->ci_name; ?><?php //echo $invoice[0]->ci_id;             ?></td>
                                    </tr>
                                    <tr>

                                        <td>Email id</td>
                                        <td><?php echo $invoice[0]->ci_email; ?></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>State Code :  </strong><?php
                                            if (!empty($invoice)) {
                                                echo $invoice[0]->ci_state_code;
                                            }
                                            if ($invoice[0]->ci_client == 38) {
                                                ?>
                                                <span style="display:inline-block; text-transform: capitalize;" class="pull-right">
                                                    <strong>Place of Supply :  </strong>
                                                    <?php
                                                    if (!empty($invoice)) {
                                                        echo $invoice[0]->ci_state;
                                                    }
                                                    ?>
                                                </span>
                                                <?php
                                            }
                                            ?>

                                        </td>
                                        <?php
                                        if (strpos($invoice[0]->ci_poNumber, "|") !== false) {
                                            $vc = explode("|", $invoice[0]->ci_poNumber);
                                            $vc_part1 = $vc[0];
                                            $vc_part2 = $vc[1];
                                        } else {
                                            $vc_part1 = "PO. No.";
                                            $vc_part2 = $invoice[0]->ci_poNumber;
                                        }
                                        ?>
                                        <td><?php echo $vc_part1; ?></td>
                                        <td><?php
                                            if (empty($vc_part2)) {
                                                echo 'NA';
                                            } else {
                                                echo $vc_part2;
                                            }
                                            ?></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>GST NO: </strong> <?php
                                            if (!empty($invoice)) {
                                                echo $invoice[0]->ci_gst;
                                            }
                                            ?>
                                        </td>
                                        <td>SAC Code</td>
                                        <td>
                                            <?php echo $invoice[0]->ci_sac; ?>
                                        </td>
                                    </tr>
                                </table>
                                <?php
                                $total = 0;
                                $no = 1;
                                $dc = 0;
                                $rs = 0;
                                if ($invoice[0]->include_debitnote == 'Y' && !empty($exp)) {
                                    $rs = count($exp) + 1;
                                }
                                ?>
                                <table class="table table-bordered" style="margin-bottom:0px;">
                                    <tr style="font-weight: bold;">
                                        <td>Sl. No</td>
                                        <td style="text-align:center;">Description</td>
                                        <td style="text-align:center;">Program Name</td>
                                        <td style="text-align:center;">Program Date</td>

                                        <td style="text-align:center;">Location</td>
                                        <td style="text-align:center;">No. of Days</td>
                                        <td style="text-align:center;">Amount</td>
                                    </tr>
                                    <tr>
                                        <td><?php echo $no; ?></td>
                                        <td style="text-align:center;"><?php echo $invoice[0]->ci_desc; ?></td>
                                        <td style="text-align:center; vertical-align: middle;"  <?php if ($rs > 0) { ?> rowspan="<?php echo $rs; ?>" <?php } ?>><?php echo $project[0]->project_title; ?></td>
                                        <td style="text-align:center; vertical-align: middle;" <?php if ($rs > 0) { ?> rowspan="<?php echo $rs; ?>" <?php } ?>>

                                            <?php
                                            if ($project[0]->project_id != '83386') {
                                                if ($project[0]->project_type == "1") //add by anand
                                                    echo "NA";
                                                else {
                                                    if ($project[0]->training_start_date != $project[0]->training_end_date) {
                                                        echo onlydate_formate($project[0]->training_start_date) . ' To ' . date_formate1($project[0]->training_end_date);
                                                    } else {
                                                        echo date_formate_short($project[0]->training_start_date);
                                                    }
                                                }
                                            } else {
                                                echo 'NA';
                                            }
                                            ?></td>

                                        <td style="text-align:center; vertical-align: middle;" <?php if ($rs > 0) { ?> rowspan="<?php echo $rs; ?>" <?php } ?>><?php echo $project[0]->location_of_training; ?></td>
                                        <td style="text-align:center; vertical-align: middle;" <?php if ($rs > 0) { ?> rowspan="<?php echo $rs; ?>" <?php } ?>><?php
										//echo "Here".$project[0]->duration_display; 
                                            if ($project[0]->duration_display!=0) {
                                                if ($project[0]->half_day == '1') {
                                                    echo 'Half Day';
                                                } else {
                                                    echo $project[0]->training_duration;
                                                }
                                            } else {
                                                echo 'NA';
                                            }
                                            ?></td>
                                        <td style="text-align:right;"><?php
                                            $total_fees = $invoice[0]->ci_charge * $project[0]->training_duration;
                                            echo number_format($total_fees, 2);
                                            ?></td>
                                    </tr>
                                    <?php
                                    $total = $total + $total_fees;
                                    if ($invoice[0]->include_debitnote == 'Y') {
                                        if (!empty($exp)) {
                                            foreach ($exp as $ex_data) {
                                                $no++;
                                                ?>
                                                <tr>
                                                    <td><?php echo $no; ?></td>
                                                    <td style="text-align:center; "><?php echo $ex_data->expense_type; ?></td>
                                                    <td style="text-align:right;"><?php echo number_format($ex_data->in_amount, 2); ?></td>
                                                </tr>
                                                <?php
                                                $total = $total + $ex_data->in_amount;
                                            }
                                        }
                                        ?>
                                        <?php
                                    }
                                    $dupAmount = 0;
                                    $upAmount = $total;
                                    ?>
                                    <tr style="font-size:15px;">
                                        <td colspan="6" class="text-right">Gross Total</td>
                                        <td style="width:100px; text-align: right;"><?php echo number_format($total, 2); ?></td>
                                    </tr>
                                </table>
                                <?php
                                $company = $CI->admin_model->get_single_company($invoice[0]->ci_company);
                                $cmState = $company[0]->gst_state_code;
                                $cState = $company[0]->gst_state_code;
                                $cgst = 0;
                                $sgst = 0;
                                if (!empty($invoice)) {
                                    $cState = $invoice[0]->ci_state_code;
                                }
                                ?>
                                <table class="table table-bordered">
                                    <tr >
                                        <td >PAN No.</td>
                                        <td><?php echo $company[0]->pan_no; ?></td>
                                        <td class="text-right">CGST On Professional Fees @9%</td>
                                        <td style="width:100px; text-align: right;">
                                            <?php
                                            if ($cState == $cmState && $invoice[0]->include_gst == 'Y') {
                                                $cgst = round((9 / 100) * $total, 2);
                                                echo number_format($cgst, 2);
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>GSTIN</td>
                                        <td><?php echo $company[0]->gstn; ?></td>
                                        <td class="text-right">SGST On Professional Fees @9%</td>
                                        <td style="text-align: right;"><?php
                                            if ($cState == $cmState && $invoice[0]->include_gst == 'Y') {
                                                $sgst = round((9 / 100) * $total, 2);
                                                echo number_format($sgst, 2);
                                            } else {
                                                echo '-';
                                            }
                                            ?></td>
                                    </tr>
                                    <tr>
                                        <td>TAN No</td>
                                        <td><?php echo $company[0]->tan_no; ?></td>
                                        <td class="text-right">IGST On Professional Fees @18%</td>
                                        <td style="text-align: right;">

                                            <?php
                                            if ($cState != $cmState && $invoice[0]->include_gst == 'Y') {
                                                $gstamt = round((18 / 100) * $total, 2);
                                                echo number_format($gstamt, 2);
                                            } else {
                                                $gstamt = $cgst + $sgst;
                                                echo '-';
                                            }
                                            $dupGst = 0;
                                            $upGst = $gstamt;
                                            $gTotal = round($total + $gstamt);
                                            ?>

                                        </td>
                                    </tr>
                                    <tr style="font-size:20px;">
                                        <td colspan="2"></td>
                                        <td class="text-right">Total</td>
                                        <td style="text-align:right;">
                                            <?php
                                            echo number_format($gTotal, 2);
                                            ?>
                                        </td>
                                    </tr>
                                    <tr><td colspan="4"><strong>In Words: 
                                                <?php
                                                $pattern = "    -x: minus >>;\n"
                                                        . "    x.x: << point >>;\n"
                                                        . "    zero; one; two; three; four; five; six; seven; eight; nine;\n"
                                                        . "    ten; eleven; twelve; thirteen; fourteen; fifteen; sixteen;\n"
                                                        . "        seventeen; eighteen; nineteen;\n"
                                                        . "    20: twenty[->>];\n"
                                                        . "    30: thirty[->>];\n"
                                                        . "    40: forty[->>];\n"
                                                        . "    50: fifty[->>];\n"
                                                        . "    60: sixty[->>];\n"
                                                        . "    70: seventy[->>];\n"
                                                        . "    80: eighty[->>];\n"
                                                        . "    90: ninety[->>];\n"
                                                        . "    100: << hundred[ >>];\n"
                                                        . "    1000: << thousand[ >>];\n"
                                                        . "    100,000: << lakh[ >>];\n"
                                                        . "    10,000,000: << crore[ >>];\n";
                                                $f = new NumberFormatter('en_IN', NumberFormatter::PATTERN_RULEBASED, $pattern);
                                                //$f->setTextAttribute(NumberFormatter::DEFAULT_RULESET, "%spellout-ordinal");
                                                echo "Rs. " . ucfirst($f->format($gTotal)) . " Only.";
                                                ?>
                                            </strong></td></tr>
                                </table>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>Beneficiary</td>
                                        <td><?php echo $company[0]->company_name; ?></td>
                                        <td rowspan="5" style="text-align: center;">For, <?php echo $company[0]->company_name; ?>
                                            <br/><br/><br/><br/><br/><br/><br/><br/>
                                            Authorized Signatory
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Account No</td>
                                        <td><?php echo $company[0]->account_no; ?></td>
                                    </tr><tr>
                                        <td>IFSC Code</td>
                                        <td><?php echo $company[0]->ifsc_no; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Bank & Branch</td>
                                        <td><?php echo $company[0]->bank_branch; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" colspan="2">
                                            <strong>
                                                * Thanking you and assuring our best services at all times
                                            </strong>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <p style="page-break-before: always"/>

                    <?php } if ($invoice[0]->include_debitnote == 'N' && !empty($exp)) {
                        ?>
                        <p style="margin-top:60px;">&nbsp;</p>
                        <div class="panel panel-default in-print" >
                            <div class="panel-body">
                                <table class="table table-bordered" >
                                    <tr style="font-size:15px;">
                                        <td colspan="3" style="text-align: center;"><h4 style="font-size:15px;">Tax Invoice</h4>
                                            <?php if ($invoice[0]->include_gst == 'N') {
                                                ?>
                                                <h4 style="font-size:12px;">Supply Meant For Export/Supply To SEZ Unit OR SEZ Developer For Authorized Operations<br/>
                                                    Under Bond OR Letter Of Undertaking Without Payment Of Integrated Tax 
                                                </h4>
                                            <?php }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td rowspan="3" style="width:350px;">To, <br/>
                                            <?php echo $client[0]->client_name; ?><?php /* for Deusche bank case */ if ($client[0]->client_id == 38) echo ", " . $invoice[0]->ci_state; ?><br/>
                                            <?php
                                            if (!empty($invoice)) {
                                                echo $invoice[0]->ci_gst_address;
                                            }
                                            ?>
                                        </td>
                                        <td>Invoice Number</td>
                                        <td><?php echo $invoice[0]->ci_no_frmt; ?>D/<?php echo str_pad($invoice[0]->ci_debit_id, 3, "0", STR_PAD_LEFT); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Invoice Date</td>
                                        <td><?php echo date_formate_short($invoice[0]->ci_date); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Ref. No</td>
                                        <td><?php echo $invoice[0]->ci_number; ?></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>State Code :  </strong><?php
                                            if (!empty($invoice)) {
                                                echo $invoice[0]->ci_state_code;
                                            }
                                            ?> 
                                        </td>
                                        <td>PO. No</td>
                                        <td><?php
                                            if (empty($invoice[0]->ci_poNumber)) {
                                                echo 'NA';
                                            } else {
                                                echo $invoice[0]->ci_poNumber;
                                            }
                                            ?></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>GST NO: </strong> <?php
                                            if (!empty($invoice)) {
                                                echo $invoice[0]->ci_gst;
                                            }
                                            ?>
                                        </td>
                                        <td>SAC Code</td>
                                        <td><?php echo $invoice[0]->ci_sac; ?></td>
                                    </tr>

                                </table>
                                <?php
                                $total = 0;
                                $no = 0;
                                ?>
                                <table class="table table-bordered" style="margin-bottom:0px;">
                                    <tr style="font-weight: bold;">
                                        <td style="width:80px;">Sl. No</td>
                                        <td style="text-align:center;">Description</td>
                                        <?php if ($invoice[0]->ci_number == 'NA') {
                                            ?>
                                            <td style="text-align:center;" colspan="4">Module Name</td>
                                        <?php } else {
                                            ?>
                                            <td style="text-align:center;">Program Name</td>
                                            <td style="text-align:center;">Program Date</td>

                                            <td style="text-align:center;">Location</td>
                                            <td style="text-align:center;">No. Of. Days</td>
                                        <?php } ?>
                                        <td style="text-align:center;">Amount</td>
                                    </tr>
                                    <?php
                                    if (!empty($exp)) {
                                        foreach ($exp as $ex_data) {
                                            $no++;
                                            ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td style="text-align:center;"><?php echo $ex_data->expense_type; ?></td>
                                                <?php
                                                if ($invoice[0]->ci_number != 'NA') {
                                                    if ($no == 1) {
                                                        ?>
                                                        <td rowspan="<?php echo count($exp); ?>" style="text-align:center; vertical-align: middle;"><?php echo $project[0]->project_title; ?></td>
                                                        <td rowspan="<?php echo count($exp); ?>" style="text-align:center; vertical-align: middle;">
                                                            <?php
                                                            if ($project[0]->project_type == "1") //add by anand
                                                                echo "NA";
                                                            else {
                                                                if ($project[0]->training_start_date != $project[0]->training_end_date) {
                                                                    echo onlydate_formate($project[0]->training_start_date) . ' To ' . date_formate1($project[0]->training_end_date);
                                                                } else {
                                                                    echo date_formate_short($project[0]->training_start_date);
                                                                }
                                                            }
                                                            ?>
                                                        </td>

                                                        <td rowspan="<?php echo count($exp); ?>" style="text-align:center; vertical-align: middle;"><?php echo $project[0]->location_of_training; ?></td>
                                                        <td rowspan="<?php echo count($exp); ?>" style="text-align:center; vertical-align: middle;"><?php
                                                            if ($project[0]->half_day == '1') {
                                                                echo 'Half Day';
                                                            } else {
                                                                echo $project[0]->training_duration;
                                                            }
                                                            ?></td>
                                                        <?php
                                                    }
                                                } else {
                                                    if ($no == 1) {
                                                        ?>
                                                        <td rowspan="<?php echo count($exp); ?>" colspan="4" style="text-align:center; vertical-align: middle;"><?php echo $invoice[0]->ci_desc; ?></td>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                                <td style="text-align:right; vertical-align: middle;"><?php echo number_format($ex_data->in_amount, 2); ?></td>
                                            </tr>
                                            <?php
                                            $total = $total + $ex_data->in_amount;
                                        }
                                    }
                                    $dupAmount = $total;
                                    ?>
                                    <tr style="font-size:18px;">
                                        <td colspan="6" class="text-right">Gross Total</td>
                                        <td style="width:100px; text-align:right;"><?php echo number_format($total, 2); ?></td>
                                    </tr>
                                </table>
                                <?php
                                $company = $CI->admin_model->get_single_company($invoice[0]->ci_company);
                                $cmState = $company[0]->gst_state_code;
                                $cState = $company[0]->gst_state_code;
                                if (!empty($invoice)) {
                                    $cState = $invoice[0]->ci_state_code;
                                }
                                ?>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>PAN No.</td>
                                        <td><?php echo $company[0]->pan_no; ?></td>
                                        <td class="text-right">CGST On Professional Fees @9%</td>
                                        <td style="width:100px; text-align:right;">
                                            <?php
                                            if ($cState == $cmState && $invoice[0]->include_gst == 'Y') {
                                                $cgst = round((9 / 100) * $total, 2);
                                                echo number_format($cgst, 2);
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>GSTIN</td>
                                        <td><?php echo $company[0]->gstn; ?></td>
                                        <td class="text-right">SGST On Professional Fees @9%</td>
                                        <td style="text-align:right;">
                                            <?php
                                            if ($cState == $cmState && $invoice[0]->include_gst == 'Y') {
                                                $sgst = round((9 / 100) * $total, 2);
                                                echo number_format($sgst, 2);
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>TAN No</td>
                                        <td><?php echo $company[0]->tan_no; ?></td>
                                        <td class="text-right">IGST On Professional Fees @18%</td>
                                        <td style="text-align:right;">
                                            <?php
                                            if ($cState != $cmState && $invoice[0]->include_gst == 'Y') {
                                                $gstamt = round((18 / 100) * $total, 2);
                                                echo number_format($gstamt, 2);
                                            } else {
                                                $gstamt = $cgst + $sgst;
                                                echo '-';
                                            }
                                            $dupGst = $gstamt;
                                            $gTotal = $total + $gstamt;
                                            ?>
                                        </td>
                                    </tr>
                                    <tr style="font-size:20px;">
                                        <td colspan="2"></td>
                                        <td class="text-right">Total</td>
                                        <td style="text-align:right;">
                                            <?php
                                            echo number_format(round($gTotal), 2);
                                            ?>
                                        </td>
                                    </tr>
                                    <tr><td colspan="4"><strong>In Words: 
                                                <?php
                                                $pattern = "    -x: minus >>;\n"
                                                        . "    x.x: << point >>;\n"
                                                        . "    zero; one; two; three; four; five; six; seven; eight; nine;\n"
                                                        . "    ten; eleven; twelve; thirteen; fourteen; fifteen; sixteen;\n"
                                                        . "        seventeen; eighteen; nineteen;\n"
                                                        . "    20: twenty[->>];\n"
                                                        . "    30: thirty[->>];\n"
                                                        . "    40: forty[->>];\n"
                                                        . "    50: fifty[->>];\n"
                                                        . "    60: sixty[->>];\n"
                                                        . "    70: seventy[->>];\n"
                                                        . "    80: eighty[->>];\n"
                                                        . "    90: ninety[->>];\n"
                                                        . "    100: << hundred[ >>];\n"
                                                        . "    1000: << thousand[ >>];\n"
                                                        . "    100,000: << lakh[ >>];\n"
                                                        . "    10,000,000: << crore[ >>];\n";
                                                $f = new NumberFormatter('en_IN', NumberFormatter::PATTERN_RULEBASED, $pattern);
                                                echo "Rs. " . ucwords($f->format(round($gTotal))) . " Only.";
                                                ?>
                                            </strong></td></tr>
                                </table>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>Beneficiary</td>
                                        <td><?php echo $company[0]->company_name; ?></td>
                                        <td rowspan="5" style="text-align: center;">For, <?php echo $company[0]->company_name; ?>
                                            <br/><br/><br/><br/><br/><br/><br/><br/>
                                            Authorized Signatory
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Account No</td>
                                        <td><?php echo $company[0]->account_no; ?></td>
                                    </tr><tr>
                                        <td>IFSC Code</td>
                                        <td><?php echo $company[0]->ifsc_no; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Bank & Branch</td>
                                        <td><?php echo $company[0]->bank_branch; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" colspan="2">
                                            <strong>
                                                * Thanking you and assuring our best services at all times
                                            </strong>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <?php
                    }

                    if (!empty($payment)) {
                        ?>
                        <div class="panel panel-default no-print">
                            <div class="panel-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <td colspan="6"><h4>Payment Details</h4></td>
                                    </tr>
                                    <tr>
                                        <td>Amount</td>
                                        <td>Date</td>
                                        <td>Payment id</td>
                                        <td>Payment Type</td>
                                        <td>Payment Advice</td>
                                        <td></td>
                                    </tr>
                                    <?php foreach ($payment as $pdata) {
                                        ?>
                                        <tr>
                                            <td><?php echo $pdata->cp_amt; ?></td>
                                            <td><?php echo date_formate_short($pdata->cp_date); ?></td>
                                            <td><?php echo $pdata->cp_pay_id; ?></td>
                                            <td><?php
                                                if ($pdata->cp_type == '1') {
                                                    echo 'Professional Fees';
                                                } else {
                                                    echo 'Debit Note';
                                                }
                                                ?></td>
                                            <td>
                                                <?php if ($pdata->cp_type == '1') { ?>
                                                    <input type="checkbox" cid="<?php echo $invoice[0]->ci_id; ?>" <?php
                                                    if ($invoice[0]->ci_pay_advice == '1') {
                                                        echo 'checked';
                                                    }
                                                    ?> id="payad" name="payad" value="1"/>
                                                       <?php } ?>
                                            </td>
                                            <td>
                                                <a href="#" class="btn-sm btn-danger remove-pay" rid="<?php echo $pdata->cp_id; ?>">
                                                    <i class="fa fa-trash"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    <?php }
                                    ?>

                                </table>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                </div>
            </div>
        </div>
        <div class="modal fade" id="pay_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Update Payment</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="pay-form">

                        <div class="modal-body">
                            <input type="hidden" name="cid" value="<?php echo $invoice[0]->ci_id; ?>"/>
                            <input type="hidden" name="upPid" value="<?php echo $invoice[0]->ci_project; ?>"/>
                            <input type="hidden" name="intype" value="1"/>

                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Payment Type <sup>*</sup></span>
                                    <select class="form-control" name="payType">
                                        <option value="1">Professional Fees</option>
                                        <option value="0">Debit Note</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Amount <sup>*</sup></span>
                                    <input type="text" name="amt"  class="form-control" placeholder="Total Amount">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Payment Date <sup>*</sup></span>
                                    <input type="date" name="date"  class="form-control" placeholder="Payment Date">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Payment Notes </span>
                                    <input type="text" name="paymentid"  class="form-control" placeholder="Payment Id">
                                </div>
                            </div>

                            <div class="form-group  col-md-12">
                                <label><input type="checkbox" name="payadvice" value="1"> Payment Advice Pending</label>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="upload_img" class="btn btn-info" >Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="request_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Request</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="edit-request-form">

                        <div class="modal-body">
                            <input type="hidden" name="cid" value="<?php echo $invoice[0]->ci_id; ?>"/>
                            <input type="hidden" name="inumber" value="<?php echo $invoice[0]->ci_number; ?>"/>
                            <input type="hidden" name="client" value="<?php echo $client[0]->client_name; ?>"/>
                            <input type="hidden" name="project" value="<?php echo $project[0]->project_title; ?>"/>
                            <input type="hidden" name="pdate" value="<?php echo $project[0]->training_start_date; ?>"/>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Send Request To  <sup>*</sup></span>
                                    <select  name="email"  class="form-control">
                                        <option value="uday@wagonslearning.com">Uday Shetty</option>
                                        <option value="raviraj@wagonslearning.com">Raviraj Poojary</option>
                                        <option value="sourabh.shah@wagonslearning.com">Sourabh Shah</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Edit Reason </span>
                                    <input type="text" name="reason"  class="form-control" placeholder="Edit Reason">
                                </div>
                            </div>

                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="upload_img" class="btn btn-info" >Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="upload_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Upload Scan copy of invoice</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="upload-form">

                        <div class="modal-body">
                            <input type="hidden" name="upCid" value="<?php echo $invoice[0]->ci_id; ?>"/>
                            <input type="hidden" name="upPid" value="<?php echo $project[0]->project_id; ?>"/>

                            <div class="form-group col-md-12">
                                <div>
                                    <span> Scan Copy </span>
                                    <input type="file" name="img[]" multiple="multiple" accept="image/*,application/pdf"/>
                                </div>
                            </div>

                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="upload_img" class="btn btn-info" >Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {

                $("#pay-form").validate({
                    rules: {
                        amt: "required",
                        date: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#edit-request-form").validate({
                    rules: {
                        email: "required",
                        reason: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#upload-form").validate({
                    rules: {
                        'img[]': "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $('.make-payment-btn').click(function (e) {
                    $('#pay_wraper').modal('show');
                });


                $('.edit-request-btn').click(function (e) {
                    e.preventDefault();
                    $('#request_wraper').modal('show');
                });

                $('.upload-btn').click(function (e) {
                    e.preventDefault();
                    $('#upload_wraper').modal('show');
                });
                
                $('.cancel-invoice-btn').click(function(e){
                    e.preventDefault();
                    var cid = $(this).attr('cid');
                    var pid = $(this).attr('pid');
                    var f = confirm("Are you sure want to cancel invoice ?");
                    if(f==true){
                         $('.page_spin').show();
                        var dataString = "cid=" + cid + "&pid=" + pid + "&page=cancelCInvoice";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('#payad').change(function (e) {
                    var cid = $(this).attr('cid');
                    var status = '0';
                    if ($(this).prop("checked") == true) {
                        status = '1';
                    }
                    $('.page_spin').show();
                    var dataString = "cid=" + cid + "&status=" + status + "&page=updatePayAdvice";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                        }, //success fun end
                    });//ajax end
                });

                $('.remove-pay').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm("Are you sure want to remove ?");
                    if (f == true) {
                        var ele = $(this).parent('td').parent('tr');
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_client_payment";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                ele.remove();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.confirm-btn').click(function (e) {
                    e.preventDefault();
                    var cid = $(this).attr('inid');
                    var pid = $(this).attr('pid');
                    var cm = '<?php echo $invoice[0]->ci_company; ?>';
                    var did = '<?php echo $invoice[0]->ci_debit_id; ?>';
                    var debit = '<?php echo $invoice[0]->include_debitnote; ?>';
                    var exp = '0';
<?php if (!empty($exp)) {
    ?>
                        exp = '1';
<?php }
?>
                    var amt = '<?php echo $upAmount; ?>';
                    var damt = '<?php echo $dupAmount; ?>';
                    var gst = '<?php echo $upGst; ?>';
                    var dgst = '<?php echo $dupGst; ?>';
                    var f = confirm("Are you sure want confirm ?");
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "did=" + did + "&cm=" + cm + "&debit=" + debit + "&exp=" + exp + "&cid=" + cid + "&pid=" + pid + "&amt=" + amt + "&damt=" + damt + "&gst=" + gst + "&dgst=" + dgst + "&page=confirm_client_invoice";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

            });
        </script>

    </body>
</html>