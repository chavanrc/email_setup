<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3 page-title">
                            <h3>Monthly Invoices</h3>
                        </div>
                        <div class="col-md-9">
                            <?php
                            $page = 1;
                            $next = 2;
                            $totalPage = 0;
                            if (isset($_GET['page'])) {
                                $page = $_GET['page'];
                            }
                            if (!empty($count)) {
                                $totalPage = ceil($count[0]->total / 20);
                                if ($count[0]->total > 20) {
                                    ?>
                                    <h4 class="pull-right" style="display: inline-block; margin-top: 10px;"><?php echo (($page - 1) * 20) + 1; ?> - <?php
                                        if ($page < $totalPage) {
                                            echo $page * 20;
                                        } else {
                                            echo $count[0]->total;
                                        }
                                        ?> of <?php echo $count[0]->total; ?> </h4>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page" style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" style="display: inline-block;"><i class="fa fa-list"></i> Client Invoices List </h2>
                            <a href="#" class="create-invoice-btn btn btn-primary pull-right">Create New Invoice</a>
                            <a href="<?php echo base_url(); ?>admin/export_drona_invoice" class="btn btn-warning pull-right" style="margin-right: 10px;">Export</a>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Client</th>
                                        <th>Invoice No.</th>
                                        <th>Month</th>
                                        <th>Invoice Date</th>
                                        <th>Program</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <form action="<?php echo base_url(); ?>admin/drona_invoice" method="POST">
                                    <tr>
                                        <td></td>
                                        <td>
                                            <select class="form-control" id="pclient" name="pclient">
                                                <option value="All"> - All Client -</option>
                                                <?php
                                                if (!empty($client)) {
                                                    foreach ($client as $cl_data) {
                                                        ?>
                                                        <option value="<?php echo $cl_data->client_id; ?>" <?php
                                                        if (isset($_POST['pclient'])) {
                                                            if ($_POST['pclient'] == $cl_data->client_id) {
                                                                echo 'selected';
                                                            }
                                                        }
                                                        ?> > <?php echo $cl_data->client_name; ?> </option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" placeholder="Invoice No" name="invoice"/>
                                        </td>
                                        <td colspan="2">
                                            <select class="form-control" name="month" style="width:65px; display: inline-block">                                                
                                                <option value="All"> All </option>                                                
                                                <option value="NA"> NA </option>                                             
                                                <option value="01"> Jan </option>                                             
                                                <option value="02"> Feb </option>                                                
                                                <option value="03"> Mar </option>                                                
                                                <option value="04"> Apr </option>                                                
                                                <option value="05"> May </option>                                                
                                                <option value="06"> Jun </option>                                                
                                                <option value="07"> Jul </option>                                                
                                                <option value="08"> Aug </option>                                                
                                                <option value="09"> Sep </option>                                                
                                                <option value="10"> Oct </option>                                                
                                                <option value="11"> Nov </option>                                                
                                                <option value="12"> Dec </option>                                                                                            
                                            </select>
                                            <select class="form-control" name="year" style="width:80px; display: inline-block">                                                
                                                <option value="All"> All </option>                                                
                                                <option value="2019"> 2019 </option>                                                
                                                <option value="2020"> 2020 </option>                                                
                                                <option value="2021"> 2021 </option>                                                
                                                <option value="2022"> 2022 </option>                                                                                                                              
                                                <option value="2023"> 2023 </option>                                                                                                                              
                                                <option value="2024"> 2024 </option> 
                                            </select> 

                                        </td>
                                        <td>
                                            <input type="text" class="form-control" placeholder="Program" name="title"/>
                                        </td>

                                        <td>
                                            <select class="form-control" name="status" style="display: inline-block; width: 130px;">
                                                <option value="All"> All </option>
                                                <option value="0"> Pending </option>
                                                <option value="1"> Paid </option>
                                            </select>
                                        </td>
                                        <td>
                                            <button class="btn-sm btn-primary"><i class="fa fa-search"></i></button>
                                        </td>
                                    </tr>
                                </form>
                                <?php
                                if (!empty($invoice)) {
                                    $no = 0;
                                    if (isset($_GET['page'])) {
                                        $no = 20 * ($_GET['page'] - 1);
                                    }
                                    foreach ($invoice as $indata) {
                                        $no++;
                                        ?>
                                        <tr>
                                            <td><?php echo $no; ?></td>
                                            <td><?php echo $indata->client_name; ?></td>
                                            <td>
                                                <?php
                                                if ($indata->di_onlyDebit == '0') {
                                                    echo $indata->di_number;
                                                } else {
                                                    echo $indata->di_debitNumber;
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($indata->di_month != 'NA') {
                                                    $dt = DateTime::createFromFormat('!m', $indata->di_month);
                                                    echo $dt->format('F') . '-' . $indata->di_year;
                                                } else {
                                                    echo 'NA';
                                                }
                                                ?>
                                            </td>
                                            <td><?php echo date_formate_short($indata->di_date); ?></td>
                                            <td>
                                                <?php
                                                if (!empty($indata->di_type)) {
                                                    echo $indata->di_type;
                                                } else {
                                                    echo $indata->di_project;
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <?php
                                                if ($indata->di_status == 0) {
                                                    echo 'Not Confirmed';
                                                }
                                                if ($indata->di_status == 1) {
                                                    if(!empty($indata->di_number) && $indata->di_pay_status=='0'){
                                                        echo 'Payment Pending';
                                                    } else if(!empty($indata->di_debitNumber) && $indata->di_debit_status=='0'){
                                                        echo 'Debit Pay Pending';
                                                    } else {
                                                        echo 'Paid';
                                                    }
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url(); ?>admin/drona_invoice_details/<?php echo $indata->di_id; ?>?page=<?php echo $page; ?>" class="btn-sm btn-info"><i class="fa fa-eye"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                            <div class="text-right" style="font-size:16px; padding: 10px;">
                                <?php
                                if ($page > 1) {
                                    $next = $page + 1;
                                    $prev = $page - 1;
                                    ?>
                                    <a href="<?php echo base_url(); ?>admin/drona_invoice/?page=1"><i class="fa fa-backward" style="font-size:12px;"></i> First</a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>admin/drona_invoice/?page=<?php echo $prev; ?>"><i class="fa fa-caret-left"></i> Prev</a>
                                    <?php
                                }
                                if ($page < $totalPage) {
                                    ?>
                                    &nbsp;&nbsp;<a href="<?php echo base_url(); ?>admin/drona_invoice/?page=<?php echo $next; ?>">Next <i class="fa fa-caret-right"></i> </a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>admin/drona_invoice/?page=<?php echo $totalPage; ?>">Last <i class="fa fa-forward" style="font-size:12px;"></i> </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="clientWraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Select Client to generate new invoice</h4>
                    </div>
                    <form action="" method="POST" id="creatinForm">
                        <div class="modal-body">

                            <div class="form-group col-md-12">

                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Month <sup>*</sup></span>
                                    <select class="form-control" name="year" required>                                                
                                        <option value="2019"> 2019 </option>                                                
                                        <option value="2020"> 2020 </option>                                                
                                        <option value="2021"> 2021 </option>                                                
                                        <option value="2022"> 2022 </option>                                                                                                                              
                                        <option value="2023"> 2023 </option>                                                                                                                              
                                        <option value="2024"> 2024 </option> 
                                    </select> 
                                    <div class="input-group-addon" style="width:150px; padding: 0px; border:0;">
                                        <select class="form-control" name="month" required>                                                
                                            <option value="NA"> NA </option>                                                
                                            <option value="01"> Jan </option>                                                
                                            <option value="02"> Feb </option>                                                
                                            <option value="03"> Mar </option>                                                
                                            <option value="04"> Apr </option>                                                
                                            <option value="05"> May </option>                                                
                                            <option value="06"> Jun </option>                                                
                                            <option value="07"> Jul </option>                                                
                                            <option value="08"> Aug </option>                                                
                                            <option value="09"> Sep </option>                                                
                                            <option value="10"> Oct </option>                                                
                                            <option value="11"> Nov </option>                                                
                                            <option value="12"> Dec </option>                                                                                            
                                        </select> 
                                    </div>

                                </div>

                                <div class="input-group" style="margin-top: 10px;">
                                    <span class="input-group-addon" id="sizing-addon1">Company <sup>*</sup></span>
                                    <select class="form-control" name="company" id="company">
                                        <option value=""> - select - </option>
                                        <?php
                                        if (!empty($company)) {
                                            foreach ($company as $c_data) {
                                                ?>
                                                <option value="<?php echo $c_data->company_id; ?>"><?php echo $c_data->company_name; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Clients <sup>*</sup></span>
                                    <select class="form-control" name="client" id="client">
                                        <option value=""> - select - </option>
                                        <?php
                                        if (!empty($client)) {
                                            foreach ($client as $cl_data) {
                                                ?>
                                                <option value="<?php echo $cl_data->client_id; ?>"><?php echo $cl_data->client_name; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="input-group load-gst" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">GST Location <sup>*</sup></span>
                                    <select class="form-control" name="gst" required>
                                        <option value=""> - select - </option>

                                    </select>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Drona Type <sup>*</sup></span>
                                    <select class="form-control" name="drona">
                                        <option value=""> - select - </option>
                                        <option value="Service Drona">Service Drona</option>
                                        <option value="Sales Drona">Sales Drona </option>
                                        <option value="Neev Drona">Neev Drona</option>
                                        <option value="Project Vijay">Project Vijay</option>

                                    </select>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Vendor Code </span>
                                    <input type="text" class="form-control" name="vcode" placeholder="Vendor Code"/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Location </span>
                                    <input type="text" class="form-control" name="location" placeholder="Location"/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Project </span>
                                    <input type="text" class="form-control" name="project" placeholder="Project"/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Department </span>
                                    <input type="text" class="form-control" name="department" placeholder="Department"/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Name </span>
                                    <input type="text" class="form-control" name="name" placeholder="Name"/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Email </span>
                                    <input type="text" class="form-control" name="email" placeholder="Email"/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">PO. No. </span>
                                    <input type="text" class="form-control" name="poNum" placeholder="PO. No."/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">SES No. </span>
                                    <input type="text" class="form-control" name="sesNum" placeholder="SES No."/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">
                                    <label><input type="checkbox" name="onlydebit" value="1"> Debit Note Only</label>
                                </div>

                            </div>



                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/additional-methods.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {

                $("#creatinForm").validate({
                    rules: {
                        company: "required",
                        client: "required",
                        gst: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });


                $('.create-invoice-btn').click(function (e) {
                    e.preventDefault();
                    $('#clientWraper').modal('show');
                });

                $('#client').change(function () {
                    var client = $(this).val();
                    if (client != '') {
                        $('.page_spin').show();
                        var dataString = "cid=" + client + "&page=get_client_location";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                $('.load-gst').html(data);
                            }, //success fun end
                        });//ajax end
                    }
                });

            });
        </script>

    </body>
</html>