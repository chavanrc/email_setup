<!DOCTYPE html><html lang="en">    
    <head>        
        <meta charset="utf-8">        
        <meta http-equiv="X-UA-Compatible" content="IE=edge">        
        <meta name="viewport" content="width=device-width, initial-scale=1">       
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->        <title>Admin | Payments</title>        <?php include 'css_files.php'; ?>        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">    
    </head>   
    <body>        
        <?php include 'admin_sidemenu.php'; ?>        
        <div class="right-side">            
            <?php include 'admin_topmenu.php'; ?>            
            <div class="row" style="margin: 0px;">                
                <div class="col-md-12">                   
                    <div class="page-title title-left">                        
                        <h3>Invoice : <?php echo $invoice[0]->ci_number; ?></h3>                    
                    </div>                    
                    <div class="page-title title-right text-right">                                                
                        <a href="<?php echo base_url(); ?>admin/client_invoice_details/<?php echo $invoice[0]->ci_id; ?>" class="create-invoice-btn btn btn-danger">Back</a>                    
                    </div>                    
                    <div class="clearfix"></div>                
                </div>                
                <div class="col-md-12 content-page">                    
                    <div class="panel panel-default">                        
                        <div class="panel-body">

                            <?php
                            if ($msg == 1) {
                                ?>
                                <div class="col-md-12">
                                    <div class="alert alert-success">Invoice Details updated</div>
                                </div>
                                <?php
                            }
                            ?>

                            <div  style="padding:10px 0;">                                
                                <strong>Invoice Details </strong>                       
                            </div>
                            <form action="" method="POST" id="updateInvoice">
                                <input type="hidden" name="inid" value="<?php echo $invoice[0]->ci_id; ?>"/>
                                <input type="hidden" name="fid" value="<?php
                                if (!empty($fees)) {
                                    echo $fees[0]->id;
                                }
                                ?>"/>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>Invoice Date</td>
                                        <td>
                                            <?php
                                            $date = explode(' ', $invoice[0]->ci_date);
                                            ?>
                                            <input type="date" name="date" class="form-control" value="<?php echo $date[0]; ?>" style="width:200px;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:230px;">GST Address </td>
                                        <td>
                                            <textarea class="form-control" name="gstAddress"><?php echo $invoice[0]->ci_gst_address; ?></textarea>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:230px;">State Name </td>
                                        <td>
                                            <input type="text" name="statename" class="form-control" value="<?php echo $invoice[0]->ci_state; ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>GST No.</td>
                                        <td>
                                            <input type="text" name="gst" class="form-control" value="<?php echo $invoice[0]->ci_gst; ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>PO No.</td>
                                        <td>
                                            <input type="text" name="poNum" class="form-control" value="<?php echo $invoice[0]->ci_poNumber; ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>SAC Code</td>
                                        <td>
                                            <input type="text" name="sac" class="form-control" value="<?php echo $invoice[0]->ci_sac; ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Ref No.</td>
                                        <td>
                                            <input type="text" name="refNum" class="form-control" value="<?php echo $invoice[0]->ci_refNumber; ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Name</td>
                                        <td>
                                            <input type="text" name="name" class="form-control" value="<?php echo $invoice[0]->ci_name; ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Email id</td>
                                        <td>
                                            <input type="text" name="email" class="form-control" value="<?php echo $invoice[0]->ci_email; ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Description</td>
                                        <td>
                                            <textarea class="form-control" name="desc"><?php echo $invoice[0]->ci_desc; ?></textarea>
                                        </td>
                                    </tr>
                                    <?php
                                    if ($invoice[0]->ci_type == 'Consolidated') {
                                        ?>
                                        <tr>
                                            <td>Name of Program</td>
                                            <td>
                                                <input type="text" name="pname" class="form-control" value="<?php echo $invoice[0]->ci_project; ?>">
                                            </td>
                                        </tr>
                                        <?php
                                    } else {
                                        ?>
                                        <input type="hidden" name="pname"  value="<?php echo $invoice[0]->ci_project; ?>">
                                        <?php
                                    }
                                    ?>
                                    <tr>
                                        <td><?php if ($invoice[0]->ci_type == 'Consolidated') { echo 'Consolidated Amount'; } else { echo 'Professional fees per Day'; } ?></td>
                                        <td>
                                            <input type="text" name="fees" class="form-control" value="<?php echo $invoice[0]->ci_charge; ?>">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Generate Debit Note</td>
                                        <td>
                                            <label><input type="radio" name="indebit" <?php if($invoice[0]->include_debitnote=='Y'){ echo 'checked'; } ?> value="Y"> No</label>
                                            <label style="margin-left:20px;"><input type="radio" name="indebit" <?php if($invoice[0]->include_debitnote=='N'){ echo 'checked'; } ?> value="N"> Yes</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>SEZ</td>
                                        <td>
                                            <label><input type="radio" name="ingst" <?php if($invoice[0]->include_gst=='Y'){ echo 'checked'; } ?> value="Y"> No</label>
                                            <label style="margin-left:20px;"><input type="radio" name="ingst" <?php if($invoice[0]->include_gst=='N'){ echo 'checked'; } ?> value="N"> Yes</label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td>
                                            <button class="btn btn-primary">Submit</button>
                                        </td>
                                    </tr>
                                </table>
                            </form>
                            <div style="padding:10px 0;">
                                <strong>Debit Note</strong>
                                 <a href="#" class="btn btn-sm btn-info pull-right btn-sm prop-status-btn" style="margin-top:-10px;"><i class="fa fa-edit"></i> Add Expenses</a>
                            </div>
                            <table class="table table-bordered">                                
                                <thead>                                    
                                    <tr>                                        
                                        <th>#</th>                                        
                                        <th>Details</th>                                        
                                        <th>Amount</th>                                        
                                        <th>Billed to client</th>                                        
                                        <th>Absorb</th>                                        
                                        <th>Note</th>                                    
                                    </tr>                                
                                </thead>                                
                                <tbody>                                    

                                    <?php
                                    if (!empty($exp)) {
                                        $num = 0;
                                        foreach ($exp as $ex_data) {
                                            $num++;
                                            ?>                                            
                                            <tr>                                                
                                                <td><?php echo $num; ?></td>                                                
                                                <td>                                                    
                                                    <input type="text" class="form-control dbox" eid="<?php echo $ex_data->id; ?>" style="width:300px; display: inline-block;" value="<?php echo $ex_data->expense_type; ?>"/>                                                
                                                </td>                                                
                                                <td><?php echo $ex_data->amount; ?></td>                                                
                                                <td>                                                    
                                                    <input type="text" class="form-control" style="width:150px; display: inline-block;" id="ex<?php echo $ex_data->id; ?>" value="<?php echo $ex_data->in_amount; ?>"/>                                                
                                                </td>                                                
                                                <td>                                                    
                                                    <input type="checkbox" <?php
                                                    if ($ex_data->absorb == 'Y') {
                                                        echo 'checked';
                                                    }
                                                    ?> class="abcheck" eid="<?php echo $ex_data->id; ?>"/>                                                
                                                </td>                                                
                                                <td><?php echo $ex_data->notes; ?></td>                                            
                                            </tr>                                            
                                            <?php
                                        }
                                        ?>
                                        <tr>
                                            <td></td>
                                            <td></td>
                                            <td></td>
                                            <td colspan="3">
                                                <a href="#" class="btn btn-primary up-btn">Update</a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                    ?>                                
                                </tbody>                            
                            </table>                        
                        </div>                    
                    </div>                
                </div>            
            </div>        
        </div>
        
        <div class="modal fade" id="ship_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Record Expenses Incurred On Program</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="ship-form">
                        <?php
                        $cid = "";
                        $cname = "";
                        $camt = "";
                        $cr_ship_date = "";
                        ?>
                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $invoice[0]->ci_project; ?>"/>
                            <input type="hidden" name="handbook" id="pTypeInput" value="Participant Handbook"/>
                            <div class="form-group col-md-12" id="pTypeWraper">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Expense Type <sup>*</sup></span>
                                    <select class="form-control" id="pType" name="pType">
                                        <option value="">- Select -</option>
										<option value="Content Dev. Charges">Content Development Expenses</option>
                                        <option value="Courier Charges">Courier Shipment Expenses</option>
                                        <option value="Food Expenses">Food Expenses</option>
                                        <option value="Lamination Charges">Lamination Expenses</option>
                                        <option value="Misc. Charges">Misc. Expenses</option>
                                        <option value="Participant Handbook Printing Charges">Printing Expenses</option>
                                        <option value="Participant Handbook Shipment Charges">Participant Handbook Courier</option>
                                        <option value="Stay Charges">Stay Charges</option>	
                                        <option value="Stationary Charges">Stationary Purchase</option>	
                                        <option value="Training Props Purchase">Training Material Shipment Expenses</option>
                                        <option value="Travelling Charges">Travelling Expenses</option>
                                        <option value="Xerox Expenses">Xerox Expenses</option>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Date of Expense<sup>*</sup></span>

                                    <input type="text" name="cr_ship_date" placeholder="YYYY-MM-DD"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Vendor <sup>*</sup></span>
                                    <input type="text" name="cname" value="<?php echo $cname; ?>" class="form-control" placeholder="Courier Company Name">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Reference No.<sup>*</sup></span>
                                    <input type="text" name="cid" value="<?php echo $cid; ?>" class="form-control" placeholder="Courier Ref No.">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Expense Amount (Rs.) <sup>*</sup></span>
                                    <input type="text" name="camt" value="<?php echo $camt; ?>" class="form-control" placeholder="Courier Expenses">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Notes<sup>*</sup></span>
                                    <textarea id="comments" name="comments"  class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="upload_img" class="btn btn-info" ><i class="fa fa-upload"></i> Update</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        
<?php include 'js_files.php'; ?>        
        <script type="text/javascript">
            $(document).ready(function () {
                
                $("#ship-form").validate({
                    rules: {
                        cname: "required",
                        cr_ship_date: "required",
                        cid: "required",
                        camt: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });
                
                $('.prop-status-btn').click(function (e) {
                    e.preventDefault();
                    $('#ship_wraper').modal('show');
                    $('#pTypeWraper').show();
                });

                $("#updateInvoice").validate({
                    rules: {
                        gstAddress: "required",
                        gst: "required",
                        desc: "required",
                        fees: "required",
                        sac:"required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $('.update-btn').click(function (e) {
                    e.preventDefault();
                    var eid = $(this).attr('eid');
                    var amt = $('#ex' + eid + '').val();
                    $('.page_spin').show();
                    var dataString = "amt=" + amt + "&eid=" + eid + "&page=update_expense";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                        },
                    });
                });
                $('.fupdate-btn').click(function (e) {
                    e.preventDefault();
                    var eid = $(this).attr('eid');
                    var amt = $('#f' + eid + '').val();
                    $('.page_spin').show();
                    var dataString = "amt=" + amt + "&fid=" + eid + "&page=update_fees";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                        },
                    });
                });
                $('.up-btn').click(function (e) {
                    e.preventDefault();
                    var Ids = [];
                    var fees = [];
                    $('.dbox').each(function () {
                        var eid = $(this).attr('eid');
                        var detail = $(this).val();
                        var amt = $('#ex' + eid + '').val();
                        var det = {id: eid, details: detail, amt: amt};
                        Ids.push(det);
                    });
//                    $('.fbox').each(function () {
//                        var fid = $(this).attr('fid');
//                        var amt = $('#f' + fid + '').val();
//                        var det = {id: fid, amt: amt};
//                        fees.push(det);
//                    });
                    Ids = JSON.stringify(Ids);
                    fees = JSON.stringify(fees);
                    $('.page_spin').show();
                    var dataString = "fee=" + fees + "&updetail=" + Ids + "&page=update_expense";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                        },
                    });
                });
                $('.abcheck').change(function () {
                    var eid = $(this).attr('eid');
                    var status = 'N';
                    if ($(this).is(':checked')) {
                        status = 'Y';
                    }
                    $('.page_spin').show();
                    var dataString = "eid=" + eid + "&status=" + status + "&page=absotb_status";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                        },
                    });
                });
            });
        </script>    
    </body>
</html>