<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Past Programs</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Past Programs</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <?php
                        $page = 1;
                        $next = 2;
                        $totalPage = 0;
                        if (isset($_GET['page'])) {
                            $page = $_GET['page'];
                        }
                        if (!empty($count)) {
                            $totalPage = ceil($count[0]->total / 20);
                            if ($count[0]->total > 20) {
                                ?>
                                <h4 class="pull-right" style="display: inline-block;"><?php echo (($page - 1) * 20) + 1; ?> - <?php
                                    if ($page < $totalPage) {
                                        echo $page * 20;
                                    } else {
                                        echo $count[0]->total;
                                    }
                                    ?> of <?php echo $count[0]->total; ?> </h4>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <?php
                    $parray = array();
                    if (!empty($program)) {
                        foreach ($program as $pdata) {
                            $parray[$pdata->client_id] = $pdata->client_name;
                        }
                    }
                    ?>
                    <div class="panel panel-default">
                        <form action="<?php echo base_url(); ?>admin/export_program" method="POST">
                            <div class="panel-heading">

                                <button type="submit" class="btn btn-sm btn-success pull-right" style="margin-top:0px; margin-right: 20px;"><i class="fa fa-file-excel-o"></i> Export</button>
                                <select class="form-control pull-right" name="client" style="margin-top:0px; margin-right: 20px; width:250px; ">
                                    <option value="All"> - All Client - </option>
                                    <?php
                                    if (!empty($client)) {
                                        foreach ($client as $padata) {
                                            ?>
                                            <option value="<?php echo $padata->client_id; ?>"><?php echo $padata->client_name; ?></option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>

                                <select class="form-control pull-right" name="month" style="margin-top:0px; margin-right: 20px; width:150px; ">
                                    <option value="All"> - All Months - </option>
                                    <option value="01">Jan</option>
                                    <option value="02">Feb</option>
                                    <option value="03">Mar</option>
                                    <option value="04">Apr</option>
                                    <option value="05">May</option>
                                    <option value="06">Jun</option>
                                    <option value="07">Jul</option>
                                    <option value="08">Aug</option>
                                    <option value="09">Sep</option>
                                    <option value="10">Oct</option>
                                    <option value="11">Nov</option>
                                    <option value="12">Dec</option>
                                </select>

                                <select class="form-control pull-right" name="year" style="margin-top:0px; margin-right: 20px; width:150px; ">
                                    <option value="All"> - All Years - </option>
                                    <option value="2019">2019</option>
                                    <option value="2020">2020</option>
                                    <option value="2021">2021</option>
                                    <option value="2022">2022</option>
                                    <option value="2023">2023</option>
                                </select>

                                <select class="form-control pull-right" name="company" style="margin-top:0px; margin-right: 20px; width:250px; ">
                                    <option value="All"> - All Company - </option>
                                    <option value="1">Wagons Management Consulting</option>
                                    <option value="2">Wagons Learning Pvt. Ltd</option>
                                </select>

                                <div class="clearfix"></div>
                            </div>
                        </form>
                        <div class="panel-body">
                            <table class="table table-bordered" style="font-size: 13px;">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>PM</th>
                                        <th>Client</th>
                                        <th>Title</th>
                                        <th>Location</th>
                                        <th>Start Date</th>
                                        <th>Client Invoice</th>
                                        <th>Trainer Invoice</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <form action="<?php echo base_url(); ?>admin/past_programs/" method="POST">
                                    <tr>
                                        <td></td>
                                        <td>
                                            <select class="form-control" id="pm" name="pm">
                                                <option value="All"> - All PM -</option>
                                                <?php
                                                if (!empty($pm)) {
                                                    foreach ($pm as $pmdata) {
                                                        ?>
                                                        <option value="<?php echo $pmdata->user_code; ?>" <?php
                                                        if (isset($_POST['pm'])) {
                                                            if ($_POST['pm'] == $pmdata->user_code) {
                                                                echo 'selected';
                                                            }
                                                        }
                                                        ?> > <?php echo $pmdata->name; ?> </option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                            </select>
                                        </td>
                                        <td colspan="2">
                                            <select class="form-control" id="pclient" name="pclient">
                                                <option value="All"> - All Client -</option>
                                                <?php
                                                if (!empty($client)) {
                                                    foreach ($client as $cl_data) {
                                                        ?>
                                                        <option value="<?php echo $cl_data->client_id; ?>" <?php
                                                        if (isset($_POST['pclient'])) {
                                                            if ($_POST['pclient'] == $cl_data->client_id) {
                                                                echo 'selected';
                                                            }
                                                        }
                                                        ?> > <?php echo $cl_data->client_name; ?> </option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                            </select>
                                        </td>
                                        <td colspan="2">
                                            <input type="date" class="form-control" name="date" value="<?php
                                            if (isset($_POST['date'])) {
                                                echo $_POST['date'];
                                            }
                                            ?>" placeholder="Location"/>
                                        </td>
                                        <td style="width:96px;">
                                            <select class="form-control" id="cinvoice" name="cinvoice">
                                                <option value="All"> - All -</option>
                                                <option value="N">Pending</option>
                                                <option value="Y">Generated</option>
                                            </select>
                                        </td>
                                        <td style="width:96px;">
                                            <select class="form-control" id="tinvoice" name="tinvoice">
                                                <option value="All"> - All -</option>
                                                <option value="0">Pending</option>
                                                <option value="1">Generated</option>
                                            </select>
                                        </td>
                                        <td><button class="btn-sm btn-primary"><i class="fa fa-search"></i></button></td>
                                    </tr>
                                </form>
                                <?php
                                if (!empty($program)) {
                                    $num = 0;
                                    if (isset($_GET['page'])) {
                                        $num = 20 * ($_GET['page'] - 1);
                                    }
                                    foreach ($program as $pm_data) {
                                        $num++;
                                        $pm = $CI->admin_model->get_single_pm($pm_data->user_code);
                                        ?>
                                        <tr <?php if ($pm_data->is_active == 0) { ?> class="label-warning"<?php } ?>>
                                            <td><?php echo $num; ?></td>
                                            <td><?php echo $pm[0]->name; ?></td>
                                            <td><?php echo $pm_data->client_name; ?></td>
                                            <td><?php
                                                if ($pm_data->project_type == '1') {
                                                    echo "<strong style='color:#ED1212;'>Content Invoice</strong><br/>";
                                                }
                                                echo wordwrap($pm_data->project_title, 75, '<br/>');
                                                ?> <?php
                                                if ($pm_data->half_day == "1") {
                                                    echo "<strong style='color:#ED1212;'>( Half Day )</strong>";
                                                }
                                                ?></td>

                                            <td><?php echo $pm_data->location_of_training; ?></td>
                                            <td><?php echo date_formate_short($pm_data->training_start_date); ?></td>
                                            <td><?php
                                                if ($pm_data->client_invoice_raised == 'N') {
                                                    ?>
                                                    <span class="label label-warning">Pending</span>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <span class="label label-success">Generated</span>
                                                    <?php
                                                }
                                                ?></td>
                                            <td>
                                                <?php
                                                if ($pm_data->project_type == '0') {
                                                    if ($pm_data->trainer_invoice_action != '1') {
                                                        ?>
                                                        <a href="#" class="btn btn-sm btn-danger">Pending</a>
                                                        <?php
                                                    }
                                                    if ($pm_data->trainer_invoice_action == '1' && $pm_data->trainer_paid_status == 0) {
                                                        ?>
                                                        <a href="<?php echo base_url(); ?>admin/trainer_inovice_details/<?php echo $pm_data->trainer_id; ?>/<?php echo $pm_data->project_id; ?>?f=program&page=<?php echo $page; ?>" class="btn btn-sm btn-warning">Generated</a>
                                                        <?php
                                                    }
                                                    if ($pm_data->trainer_invoice_action == '1' && $pm_data->trainer_paid_status == 1) {
                                                        ?>
                                                        <a href="<?php echo base_url(); ?>admin/trainer_inovice_details/<?php echo $pm_data->trainer_id; ?>/<?php echo $pm_data->project_id; ?>?f=program&page=<?php echo $page; ?>" class="btn btn-sm btn-success">Paid</a>
                                                        <?php
                                                    }
                                                } else {
                                                    echo 'NA';
                                                }
                                                ?>

                                            </td>
                                            <td><a href="<?php echo base_url(); ?>admin/program_details/<?php echo $pm_data->project_id; ?>?page=<?php echo $page; ?>" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a></td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="9">No Record found</td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                            <div class="text-right" style="font-size:16px; padding: 10px;">
                                <?php
                                if ($page > 1) {
                                    $next = $page + 1;
                                    $prev = $page - 1;
                                    ?>
                                    <a href="<?php echo base_url(); ?>admin/past_programs/?page=1"><i class="fa fa-backward" style="font-size:12px;"></i> First</a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>admin/past_programs/?page=<?php echo $prev; ?>"><i class="fa fa-caret-left"></i> Prev</a>
                                    <?php
                                }
                                if ($page < $totalPage) {
                                    ?>
                                    &nbsp;&nbsp;<a href="<?php echo base_url(); ?>admin/past_programs/?page=<?php echo $next; ?>">Next <i class="fa fa-caret-right"></i> </a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>admin/past_programs/?page=<?php echo $totalPage; ?>">Last <i class="fa fa-forward" style="font-size:12px;"></i> </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {

            });
        </script>

    </body>
</html>