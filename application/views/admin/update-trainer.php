<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Edit Trainer</title>
        <?php include 'css_files.php'; ?>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/multiselect/bootstrap-multiselect.css">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Update Trainer</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>admin/trainer_profile/<?php echo $trainer[0]->user_code; ?>" class="btn btn-danger"><i class="fa fa-backward"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <?php
                if ($msg == 1) {
                    ?>
                    <div class="alert alert-success col-md-4 col-md-offset-4">
                        Trainer Details Updated Successfully.
                    </div>

                    <?php
                }
                ?>
                
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-edit"></i> <?php echo $trainer[0]->name; ?> </h2>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == "error") {
                                ?>
                                <div class="alert alert-danger col-md-4 col-md-offset-4">
                                    This email id is already exist.
                                </div>
                                <?php
                            }
                            ?>
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-trainer-form">
                                <input type="hidden" name="uid" value="<?php echo $trainer[0]->user_code; ?>">
                                <input type="hidden" name="c_email" value="<?php echo $trainer[0]->email; ?>">
                                <input type="hidden" name="cimg" value="<?php echo $trainer[0]->user_photo; ?>">
                                <input type="hidden" name="c_cv" value="<?php echo $trainer[0]->user_cv; ?>">
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">

                                    <div class="input-group">
                                        <span class="input-group-addon"  id="sizing-addon1"> Name <sup>*</sup></span>
                                        <input type="text" name="pname" value="<?php echo $trainer[0]->name; ?>" class="form-control">
                                    </div>

                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"  id="sizing-addon1"> Date of Birth <sup>*</sup></span>
                                        <input type="date" id="dob" value="<?php echo $trainer[0]->user_dob; ?>" data-date-format="YYYY MM DD" name="dob" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">

                                    <div class="input-group">
                                        <label for="gender">Gender : <sup>*</sup></label>
                                        <label class="radio-inline" style="margin: 0px 20px;"><input type="radio" <?php if($trainer[0]->user_gender=='Male'){ echo 'checked'; } ?>  name="gender" value="Male">Male</label>
                                        <label class="radio-inline" ><input type="radio" name="gender" <?php if($trainer[0]->user_gender=='Female'){ echo 'checked'; } ?> value="Female">Female</label>
                                    </div>

                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">

                                    <div class="input-group">
                                        <label for="occupation"> Occupation : <sup>*</sup></label>
                                        <label class="radio-inline" style="margin: 0px 20px;"><input type="radio" <?php if($trainer[0]->freelancer=='Y'){ echo 'checked'; } ?>  name="occupation" value="Y">Freelancer</label>
                                        <label class="radio-inline" ><input type="radio" <?php if($trainer[0]->freelancer=='N'){ echo 'checked'; } ?> name="occupation" value="N">Employed</label>
                                    </div>

                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Country <sup>*</sup></span>
                                        <select class="form-control" name="country">
                                            <option value="India"> India </option>
                                        </select>
                                    </div>
                                    

                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> State <sup>*</sup></span>
                                        <select class="form-control" id="state" name="state">
                                            <option value=""> - Select - </option>
                                            <?php
                                            if (!empty($state)) {
                                                foreach ($state as $st_data) {
                                                    ?>
                                                    <option value="<?php echo $st_data->st_name; ?>" <?php if ($st_data->st_name == $trainer[0]->state) { ?> selected <?php } ?>><?php echo $st_data->st_name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    

                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> City <sup>*</sup></span>
                                        <select class="form-control" id="city" name="city">
                                            <option value=""> - Select - </option>
                                            <?php
                                            if (!empty($city)) {
                                                foreach ($city as $ct_data) {
                                                    ?>
                                                    <option value="<?php echo $ct_data->city; ?>" <?php if ($ct_data->city == $trainer[0]->city) { ?> selected <?php } ?>><?php echo $ct_data->city; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    
                                    
                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label for="address1">Address 1 : <sup>*</sup></label>
                                    <textarea class="form-control" name="address1" id="address1"><?php echo $trainer[0]->address; ?></textarea>
                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Email <sup>*</sup></span>
                                        <input type="text" name="email" value="<?php echo $trainer[0]->email; ?>" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Mobile <sup>*</sup></span>
                                        <input type="text" name="mobile" value="<?php echo $trainer[0]->contact_number; ?>" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Landline </span>
                                        <input type="text" name="landline" value="<?php echo $trainer[0]->contact_number_landline; ?>" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Skype ID </span>
                                        <input type="text" name="skype" value="<?php echo $trainer[0]->skypeID; ?>" class="form-control">
                                    </div>
                                </div>
                                <?php
                                $specification = explode(',', $trainer[0]->trainer_area);
                                ?>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Area of Specialization </span>
                                        <select class="form-control"  id="specification" name="specification[]" multiple="multiple">
                                            <option value="Trainer" <?php if(in_array('Trainer', $specification)){ echo 'selected'; } ?> selected>Trainer</option>
                                            <option value="Consultant" <?php if(in_array('Consultant', $specification)){ echo 'selected'; } ?>>Consultant</option>
                                            <option value="Content" <?php if(in_array('Content', $specification)){ echo 'selected'; } ?>>Content</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <?php
                                $industry = array('Advertising','Event Management','Agriculture/Dairy','Architecture',
                                    'Automobiles',
                                    'Banking/Financial',
                                    'BPO',
                                    'Chemicals',
                                    'Construction',
                                    'Logistics',
                                    'Education/Training',
                                    'Export/Import',
                                    'Fertilizers',
                                    'FMCG',
                                    'Fresher/Trainee',
                                    'Gems and Jewellery',
                                    'Heavy Machinery',
                                    'Hotels/Restaurants',
                                    'Industrial Prods',
                                    'Energy',
                                    'Insurance',
                                    'IT-Hardware',
                                    'IT-Products',
                                    'Legal',
                                    'Media',
                                    'Healthcare',
                                    'NGO',
                                    'Automation',
                                    'Oil and Gas',
                                    'Paper',
                                    'Pharma',
                                    'Packaging',
                                    'Real Estate',
                                    'Recruitment Firm',
                                    'Retailing',
                                    'Security',
                                    'Electronics',
                                    'Shipping/Marine',
                                    'Telecom',
                                    'Textiles'
                                    );
                                $industry_array = explode(',', $trainer[0]->industry);
                                ?>
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Industry </span>
                                        <select class="form-control"  id="industry" name="industry[]" multiple="multiple" style="width: 100%">
                                           <?php
                                           foreach($industry as $in_data)
                                           {
                                               ?>
                                            <option value="<?php echo $in_data; ?>" <?php if(in_array($in_data, $industry_array)){ echo 'selected'; } ?>><?php echo $in_data; ?></option>
                                                <?php
                                           }
                                           ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Profile Photo </span>
                                        <input type="file" name="photo" accept='image/*' class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"  id="sizing-addon1">  Expected Fees P/D</span>
                                        <input type="text" name="fees" value="<?php echo $trainer[0]->expected_fee_per_day; ?>" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Upload CV/Profile </span>
                                        <input type="file" name="cv"   class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Password </span>
                                        <input type="text" name="password"   class="form-control">
                                    </div>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Payment Credit Days </span>
                                        <input type="text" name="trainer_credit"   class="form-control" value="<?php echo $trainer[0]->trainer_credit; ?>">
                                    </div>
                                </div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                    <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                </div>

                                <div class="clearfix"></div>
                                
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script src="<?php echo base_url(); ?>assets/js/multiselect/bootstrap-multiselect.js"></script>
        <script type="text/javascript">

            $(document).ready(function () {
                $('#specification').multiselect({
                    nonSelectedText: '- Select -'
                });

                $('#industry').multiselect({
                    nonSelectedText: '- Select -'
                });
                
                $('#state').change(function () {
                    var state = $(this).val();
                    if (state != '')
                    {
                        var dataString = "state=" + state + "&page=get_city";
                        $.ajax({
                            type: "POST",
                            dataType: "JSON",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('#city option').remove();
                                $('#city').append('<option value=""> - Select - </option>');
                                $('.page_spin').hide();
                                $.each(data, function (i, item) {

                                    $('#city').append('<option value="' + item.city + '">' + item.city + '</option>');
                                });
                            }, //success fun end
                        }); //ajax end 
                    }
                });

                $("#add-trainer-form").validate({
                    rules: {
                        pname: "required",
                        dob: "required",
                        address1: "required",
                        country: "required",
                        state: "required",
                        city: "required",
						occupation: "required",
						specification: "required",
						industry: "required",
						fees: "required",
						trainer_credit: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        mobile: {
                            required: true,
                            number: true
                        }
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

            });

        </script>

    </body>
</html>