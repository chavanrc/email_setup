<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3 page-title">
                            <h3>Client Invoices</h3>
                        </div>
                        <div class="col-md-9">
                            <?php
                            $page = 1;
                            $next = 2;
                            $totalPage = 0;
                            if (isset($_GET['page'])) {
                                $page = $_GET['page'];
                            }
                            if (!empty($count)) {
                                $totalPage = ceil($count[0]->total / 20);
                                if ($count[0]->total > 20) {
                                    ?>
                                    <h4 class="pull-right" style="display: inline-block; margin-top: 10px;"><?php echo (($page - 1) * 20) + 1; ?> - <?php
                                        if ($page < $totalPage) {
                                            echo $page * 20;
                                        } else {
                                            echo $count[0]->total;
                                        }
                                        ?> of <?php echo $count[0]->total; ?> </h4>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page" style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" style="display: inline-block;"><i class="fa fa-list"></i> Client Invoices List </h2>
                            <a href="#" class="create-debit-btn btn btn-info pull-right">Create Debit Note</a>
                            <a href="#" class="create-invoice-btn btn btn-primary pull-right">Create New Invoice</a>

                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" style="font-size: 12px;">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th style="width:150px;">Client</th>
                                        <th>Invoice No.</th>
                                        <th>Date</th>
                                        <th>Program</th>
                                        <th>Status</th>
                                        <th>Amount</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <form action="<?php echo base_url(); ?>admin/client_invoice" method="POST">
                                    <tr>
                                        <td></td>
                                        <td>
                                            <select class="form-control" id="pclient" name="pclient">
                                                <option value="All"> - All Client -</option>
                                                <?php
                                                if (!empty($client)) {
                                                    foreach ($client as $cl_data) {
                                                        ?>
                                                        <option value="<?php echo $cl_data->client_id; ?>" <?php
                                                        if (isset($_POST['pclient'])) {
                                                            if ($_POST['pclient'] == $cl_data->client_id) {
                                                                echo 'selected';
                                                            }
                                                        }
                                                        ?> > <?php echo $cl_data->client_name; ?> </option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                            </select>
                                        </td>
                                        <td colspan="2">
                                            <input type="text" class="form-control" placeholder="Invoice No" name="invoice"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" placeholder="Program" name="title"/>
                                        </td>

                                        <td colspan="2">
                                            <select class="form-control" name="status" style="display: inline-block; width: 130px;">
                                                <option value="All"> All </option>
                                                <option value="0"> Pending </option>
                                                <option value="2"> Debit Pending </option>
                                                <option value="1"> Paid </option>
                                            </select>
                                        </td>
                                        <td>
                                            <button class="btn-sm btn-primary"><i class="fa fa-search"></i></button>
                                        </td>
                                    </tr>
                                </form>
                                <?php
                                if (!empty($invoice)) {
                                    $no = 0;
                                    if (isset($_GET['page'])) {
                                        $no = 20 * ($_GET['page'] - 1);
                                    }
                                    foreach ($invoice as $in_data) {
                                        $no++;
                                        ?>
                                        <tr>
                                            <td <?php if ($in_data->ci_debit_id != '0') { ?> rowspan="2" <?php } ?>><?php echo $no; ?></td>
                                            <td <?php if ($in_data->ci_debit_id != '0') { ?> rowspan="2" <?php } ?> ><?php echo $in_data->client_name; ?></td>
                                            <td><?php echo $in_data->ci_number; ?> </td>
                                            <td <?php if ($in_data->ci_debit_id != '0') { ?> rowspan="2" <?php } ?> ><?php echo date_formate_short($in_data->ci_date); ?></td>
                                            <td <?php if ($in_data->ci_debit_id != '0') { ?> rowspan="2" <?php } ?> ><?php
                                                if ($in_data->ci_type == 'Consolidated') {
                                                    echo 'Multiple';
                                                } else {
                                                    echo $in_data->project_title;
                                                }
                                                ?></td>
                                            <td <?php if ($in_data->ci_debit_id != '0') { ?> rowspan="2" <?php } ?> >
                                                <?php
                                                if ($in_data->ci_status == '0') {
                                                    echo 'Confirmation Pending';
                                                } else if ($in_data->ci_status == '2') {
                                                    echo '<strong style="color:#ED1122;">Cancelled</strong>';
                                                } else {
                                                    echo 'Confirmed';
                                                }
                                                $duration = $in_data->training_duration - 1;
                                                $cr = $in_data->payment_credit_days + $duration;
                                                if ($in_data->ci_type == 'Consolidated') {
                                                    $dueDate = date('d-m-Y', strtotime($in_data->ci_date . ' + ' . $cr . ' days'));
                                                } else {
                                                    $dueDate = date('d-m-Y', strtotime($in_data->ci_date . ' + ' . $cr . ' days'));
                                                }
                                                ?>
                                                <br/>
                                                <?php if ($in_data->ci_status != '2') { ?>
                                                    <strong>Due Date :</strong> <?php echo $dueDate; ?>
                                                <?php } ?>
                                            </td>
                                            <td>
                                                <?php echo round($in_data->ci_amt + $in_data->ci_gstamt); ?><br/>
                                                <strong>Status : </strong><?php
                                                if ($in_data->ci_pay_status == '1') {
                                                    echo 'Paid';
                                                } else {
                                                    echo 'Pending';
                                                }
                                                ?>
                                            </td>
                                            <td <?php if ($in_data->ci_debit_id != '0') { ?> rowspan="2" <?php } ?> >
                                                <a href="<?php echo base_url(); ?>admin/client_invoice_details/<?php echo $in_data->ci_id; ?>?page=<?php echo $page; ?>" class="btn btn-info"><i class="fa fa-eye"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                        if ($in_data->ci_debit_id != '0') {
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php
                                                    if ($in_data->ci_debit_id != '0') {
                                                        echo $in_data->ci_no_frmt;
                                                        ?>D/<?php
                                                        echo str_pad($in_data->ci_debit_id, 3, "0", STR_PAD_LEFT);
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php echo round($in_data->ci_damt + $in_data->ci_dgstamt); ?><br/>
                                                    <strong>Status : </strong><?php
                                                    if ($in_data->ci_debit_status == '1') {
                                                        echo 'Paid';
                                                    } else {
                                                        echo 'Pending';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                }
                                ?>
                                </tbody>
                            </table>
                            <div class="text-right" style="font-size:16px; padding: 10px;">
                                <?php
                                if ($page > 1) {
                                    $next = $page + 1;
                                    $prev = $page - 1;
                                    ?>
                                    <a href="<?php echo base_url(); ?>admin/client_invoice/?page=1"><i class="fa fa-backward" style="font-size:12px;"></i> First</a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>admin/client_invoice/?page=<?php echo $prev; ?>"><i class="fa fa-caret-left"></i> Prev</a>
                                    <?php
                                }
                                if ($page < $totalPage) {
                                    ?>
                                    &nbsp;&nbsp;<a href="<?php echo base_url(); ?>admin/client_invoice/?page=<?php echo $next; ?>">Next <i class="fa fa-caret-right"></i> </a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>admin/client_invoice/?page=<?php echo $totalPage; ?>">Last <i class="fa fa-forward" style="font-size:12px;"></i> </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="clientWraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title ctitle">Select Client to generate new invoice</h4>
                    </div>
                    <form action="<?php echo base_url(); ?>admin/creat_client_invoice" method="POST" id="creatinForm">
                        <div class="itype">

                        </div>
                        <div class="modal-body">

                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Company <sup>*</sup></span>
                                    <select class="form-control" name="company" id="company">
                                        <option value=""> - select - </option>
                                        <?php
                                        if (!empty($company)) {
                                            foreach ($company as $c_data) {
                                                ?>
                                                <option value="<?php echo $c_data->company_id; ?>"><?php echo $c_data->company_name; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Clients <sup>*</sup></span>
                                    <select class="form-control" name="client" id="client">
                                        <option value=""> - select - </option>
                                        <?php
                                        if (!empty($client)) {
                                            foreach ($client as $cl_data) {
                                                ?>
                                                <option value="<?php echo $cl_data->client_id; ?>"><?php echo $cl_data->client_name; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>

                                <div class="input-group load-gst" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">GST Location <sup>*</sup></span>
                                    <select class="form-control" name="gst" required>
                                        <option value=""> - select - </option>

                                    </select>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">PO. No. </span>
                                    <input type="text" class="form-control" name="poNum" placeholder="PO. No."/>
                                </div>

                                <div class="input-group" style="margin-top: 10px;">

                                    <span class="input-group-addon" id="sizing-addon1">Ref. No. </span>
                                    <input type="text" class="form-control" name="refNum" placeholder="Ref. No."/>
                                </div>

                            </div>



                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/additional-methods.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {

                $("#creatinForm").validate({
                    rules: {
                        company: "required",
                        client: "required",
                        gst: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $('.create-debit-btn').click(function (e) {
                    e.preventDefault();
                    $('#clientWraper').modal('show');
                    $('.ctitle').html("Select Client to generate debit note");
                    $('.itype').html('<input type="hidden" name="dproject" value="<?php echo rand(00, 999) . time(); ?>"/>');
                });

                $('.create-invoice-btn').click(function (e) {
                    e.preventDefault();
                    $('#clientWraper').modal('show');
                    $('.itype').html('');
                    $('.ctitle').html("Select Client to generate new invoice");
                });

                $('#client').change(function () {
                    var client = $(this).val();
                    if (client != '') {
                        $('.page_spin').show();
                        var dataString = "cid=" + client + "&page=get_client_location";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                $('.load-gst').html(data);
                            }, //success fun end
                        });//ajax end
                    }
                });


                $('.pay-btn').click(function (e) {
                    e.preventDefault();
                    var pid = $(this).attr('pid');
                    var f = confirm("Are you sure want make payment ?");
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "pid=" + pid + "&page=update_payment";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

            });
        </script>

    </body>
</html>