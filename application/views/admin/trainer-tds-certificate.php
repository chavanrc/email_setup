<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Trainer TDS Certificate</title>
        <?php include 'css_files.php'; ?>
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
        <style>
            .panel-body i{
                display: inline-block;
                width: 20px;
            }
            .top-label{
                padding:10px; font-size: 12px; border:solid 1px #73879C;
                margin-left: -10px;
                color:#fff;
            }
            
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>
            <div class="right-side">
                <?php include 'admin_topmenu.php'; ?>
                     <nav class="navbar navbar-default">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">

                                    <a class="navbar-brand" href="#">Trainer</a>
                                </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li ><a href="<?php echo base_url(); ?>admin/trainer_profile/<?php echo $trainer[0]->user_code; ?>"><i class="fa fa-info"></i> Details</a></li>
                           
                            <li><a href="<?php echo base_url(); ?>admin/trainer_program_engagment/<?php echo $trainer[0]->user_code; ?>"><i class="fa fa-paper-plane"></i> Program Engagement</a></li>
                            <li><a href="<?php echo base_url(); ?>admin/trainer_calendar/<?php echo $trainer[0]->user_code; ?>"><i class="fa fa-calendar"></i> Calendar </a></li>
                            <li class="active"><a href="#"><i class="fa fa-certificate"></i> TDS Certificates </a></li>
                            
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div>
            </nav>
                
                <div class="row" style="margin: 0px;">
                      <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>TDS Certificates</h3>
                        
                    </div>
                    <div class="page-title title-left text-left">
                        
                    </div>
                    <div class="clearfix"></div>
                </div>
                    <?php
                if ($msg == 1) {
                    ?>
                    <div class="alert alert-success col-md-4 col-md-offset-4">
                         Added Successfully.
                    </div>

                    <?php
                }
                ?>
                    <div class="col-md-12 content-page">
                        
                        <form action="" method="POST"  enctype="multipart/form-data" id="add-tds-form">
                            <div class="col-md-6 col-sm-6 col-xs-12">
                                <!--<div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Title </span>
                                        <input type="text" name="title" class="form-control">
                                    </div>
                                </div>-->
                            
                                
                                
                                <div class="form-group">
                                <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Year </span>
                                        <select class="form-control"  name="year">
                                            <option value=""> - Select - </option>
                                            <option value="2017-2018">2017-2018</option>
                                            <option value="2018-2019">2018-2019</option>
                                            <option value="2019-2020">2019-2020</option>
                                            <option value="2020-2021">2020-2021</option>
                                            
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Quater </span>
                                        <select class="form-control" name="quater">
                                            <option value=""> - Select - </option>
                                            <option value="quater 1">quater 1</option>
                                            <option value="quater 2">quater 2</option>
                                            <option value="quater 3">quater 3</option>
                                            <option value="quater 4">quater 4</option>
                                            
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Upload File </span>
                                        <input type="file" class="form-control" name="file" multiple/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <input type="hidden" name="trainer_id" id="trainer_id" value="<?php echo $trainer[0]->user_code; ?>">
                                        <input type="hidden" name="email" id="email" value="<?php echo $trainer[0]->email; ?>">
                                        
                                </div>
                                
                            </div>
                        </form>
                    
                    
                   
                        <div class="col-md-6 col-sm-6 col-xs-12">
                            
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-list"></i> tds certificate </h2>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered" id="data-list">
                                    <thead>
                                        <tr>
                                            
                                           <!--<th>Title</th>-->
                                           
                                            <th>Year</th>
                                            <th>Quater</th>
                                             <th> </th>
                                            <th> </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($tds as $tds_cert) {
                                            ?>
                                            <tr>
                                                
                                                <!--<td><?php echo $tds_cert->tds_title; ?></td>-->
                                                
                                                <td><?php echo $tds_cert->tt_year; ?></td>
                                                <td><?php echo $tds_cert->tt_quater; ?></td>
                                                <td><a href="<?php echo base_url(); ?>assets/upload/tdscertificate/<?php echo $tds_cert->tds_file; ?>" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-download"></i> View</a></td>
                                                <td><a href="#" class="btn btn-sm btn-danger certificate" id="<?php echo $tds_cert->tt_id; ?>" style="margin-bottom: 5px; display: inline-block;"><i class="fa fa-trash"></i></a></td>
                                               
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>    
        </div>
            </div>
        
    </body>
    
        <?php include 'js_files.php'; ?>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/moment.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/jquery.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.js'></script>
        
        <script type="text/javascript">
            $(document).ready(function () {
                
                $('.assign-pm-btn').click(function (e) {
                    e.preventDefault();
                    $('#assign-pm-wrap').modal('show');
                    var cid = $(this).attr('cid');
                    $('#cid').val(cid);
                });


                $('#calendar').fullCalendar({
                    defaultDate: '2017-02-09',
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    events: [
                        {
                            title: 'Admin Dahsboard',
                            start: '2017-02-09'
                        },
                        {
                            title: 'Long Event',
                            start: '2016-12-07',
                            end: '2016-12-10'
                        },
                        {
                            id: 999,
                            title: 'Repeating Event',
                            start: '2016-12-09T16:00:00'
                        },
                        {
                            id: 999,
                            title: 'Repeating Event',
                            start: '2016-12-16T16:00:00'
                        },
                        {
                            title: 'Conference',
                            start: '2016-12-11',
                            end: '2016-12-13'
                        },
                        {
                            title: 'Meeting',
                            start: '2016-12-12T10:30:00',
                            end: '2016-12-12T12:30:00'
                        },
                        {
                            title: 'Lunch',
                            start: '2016-12-12T12:00:00'
                        },
                        {
                            title: 'Meeting',
                            start: '2016-12-12T14:30:00'
                        },
                        {
                            title: 'Happy Hour',
                            start: '2016-12-12T17:30:00'
                        },
                        {
                            title: 'Dinner',
                            start: '2016-12-12T20:00:00'
                        },
                        {
                            title: 'Birthday Party',
                            start: '2016-12-13T07:00:00'
                        },
                        {
                            title: 'Click for Google',
                            url: 'http://google.com/',
                            start: '2016-12-28'
                        }
                    ]
                });


                $("#assign-pm-form").validate({
                    rules: {
                        pm: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        assign_pm();
                    }
                });

                $('.approved-btn').click(function(e){
                    var tid = $(this).attr('tid');
                    
                    $('.page_spin').show();
                    var dataString = "tid=" + tid + "&page=trainer_make_approve";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                });
                 
                  $("#add-tds-form").validate({
                    rules: {
                        title: "required",
                        'file': "required",
                        year: "required",
                        quater: "required",
                        
                        
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });
                
                $('.certificate').click(function (e) {
                    e.preventDefault();
                    var id = $(this).attr('id');
                    var ele = $(this);
                    var f = confirm("Are you sure want to remove ?");
                    if (f == true) {
                        $('.page_spin').show();
                        var dataString = "id=" + id + "&page=remove_tds_certificate";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                ele.parent('td').parent('tr').remove();
                            }, //success fun end
                        });//ajax end
                    }
                });
            });
        </script>

    </body>
</html>