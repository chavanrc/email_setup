<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Clients</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
        <link href='https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' />
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Clients</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>admin/client_gst" class="btn btn-success"> GST Data</a>
                        <a href="<?php echo base_url(); ?>admin/add_client" class="btn btn-primary"><i class="fa fa-user-plus"></i> Create New Client</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" style="display:inline-block;"><i class="fa fa-list"></i> Client List </h2>
                            <select class="form-control pull-right" id="cType" style="width:250px;">
                                <option value="">All</option>
                                <option value="1">Wagons Management Consulting</option>
                                <option value="2">Wagons Learning Pvt. Ltd. </option>
                            </select>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Logo</th>
                                        <th>Industry</th>
                                        <th></th>
                                        <th style="width:100px !important;">Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($client)) {
                                        $num = 0;
                                        foreach ($client as $cl_data) {
                                            $num++;
                                            ?>
                                            <tr class="crow company<?php echo $cl_data->company_id; ?>">
                                                <td><?php echo $num; ?></td>
                                                <td><?php echo $cl_data->client_name; ?><br/>
                                                    <?php
                                                    $check = $CI->admin_model->check_client_assign($cl_data->client_id);
                                                    if (!empty($check)) {
                                                        ?>
                                                        <span class="label label-success">PM</span> : <?php echo $check[0]->name; ?><br/>
                                                        <span class="label label-success">BM</span> : <?php echo $check[0]->bname; ?>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if (!empty($cl_data->logo)) {
                                                        ?>
                                                        <img src="<?php echo base_url(); ?>assets/upload/client/<?php echo $cl_data->logo; ?>" width="50px"/>
                                                    <?php } ?>
                                                </td>
                                                <td><?php echo $cl_data->industry; ?></td>
                                                <td><?php
                                                    if (!empty($check)) {
                                                        ?>
                                                        <a href="#" class="btn-sm btn-success assign-pm-btn"  bm="<?php echo $check[0]->buser_code; ?>" pm="<?php echo $check[0]->user_code; ?>" cid="<?php echo $cl_data->client_id; ?>">Edit PM & BM</a>
                                                        <?php
                                                    } else {
                                                        ?>
                                                        <a href="#" class="btn-sm btn-warning assign-pm-btn"  pm="" bm="" cid="<?php echo $cl_data->client_id; ?>">Assign PM & BM</a>
                                                        <?php
                                                    }
                                                    ?></td>

                                                <td>
                                                    <a href="<?php echo base_url(); ?>admin/upate_client/<?php echo $cl_data->client_id; ?>" class="btn-sm btn-info"><i class="fa fa-edit"></i></a>
                                                    <a href="#" class="btn-sm btn-danger remove-client" cid="<?php echo $cl_data->client_id; ?>"><i class="fa fa-trash"></i></a>
                                                    <a href="<?php echo base_url(); ?>admin/client_gst/<?php echo $cl_data->client_id; ?>" class="btn-sm btn-primary">GST</a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="9">No Record found</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="assign-pm-wrap" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-arrow-right"></i> Assign PM & BM</h4>
                    </div>
                    <div class="modal-body">
                        <form action="" method="POST"  enctype="multipart/form-data" id="assign-pm-form">
                            <input type="hidden" value="" id="cid" name="cid"/>
                            <div class="form-group col-md-8 col-md-offset-2">
                                <select class="form-control" id="pm" name="pm">
                                    <option value=""> - Select Project Manager -</option>
                                    <?php
                                    foreach ($pm as $pm_data) {
                                        if ($pm_data->user_type == 'project manager') {
                                            ?>
                                            <option value="<?php echo $pm_data->user_code; ?>"> <?php echo $pm_data->name; ?> </option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-8 col-md-offset-2">
                                <select class="form-control" id="bm" name="bm">
                                    <option value=""> - Select Business Manager -</option>
                                    <?php
                                    foreach ($pm as $pm_data) {
                                        if ($pm_data->user_type == 'business manager') {
                                            ?>
                                            <option value="<?php echo $pm_data->user_code; ?>"> <?php echo $pm_data->name; ?> </option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-8 col-md-offset-2 text-center">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {

                $('#data-list').DataTable({
                    "pageLength": 25
                });

                $('#cType').change(function () {
                    var ctype = $(this).val();
                    if (ctype == '1') {
                        $('.crow').hide();
                        $('.company1').show();
                    }
                    if (ctype == '2') {
                        $('.crow').hide();
                        $('.company2').show();
                    }
                    if (ctype == '') {
                        $('.crow').show();
                    }
                    $("#data-list").DataTable().destroy();
                    $('#data-list').DataTable({
                        "pageLength": 25
                    });
                });

                $(document).on('click', '.assign-pm-btn', function (e) {
                    e.preventDefault();
                    $('#assign-pm-wrap').modal('show');
                    var cid = $(this).attr('cid');
                    var pm = $(this).attr('pm');
                    var bm = $(this).attr('bm');
                    $('#cid').val(cid);
                    $('#pm').val(pm);
                    $('#bm').val(bm);
                });


                $("#assign-pm-form").validate({
                    rules: {
                        pm: "required",
                        bm: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        assign_pm();
                    }
                });

                function assign_pm()
                {
                    var cid = $('#cid').val();
                    var pm = $('#pm').val();
                    var bm = $('#bm').val();

                    $('.page_spin').show();
                    var dataString = "cid=" + cid + "&pm=" + pm + "&bm=" + bm + "&page=assign_pm";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            $('#assign-pm-wrap').modal('hide');

                        }, //success fun end
                    });//ajax end
                }

                $('.remove-client').click(function (e) {
                    e.preventDefault();
                    var cid = $(this).attr('cid');
                    var ele = $(this);
                    var f = confirm("Are you sure want to remove client ?");
                    if (f == true) {
                        $('.page_spin').show();
                        var dataString = "cid=" + cid + "&page=remove_client";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                alert("Client is deleted");
                                ele.parent('td').parent('tr').remove();
                            }, //success fun end
                        });//ajax end
                    }
                });
            });
        </script>

    </body>
</html>