<table border="1px">
    <thead>
        <tr>
            <th >Program</th>
            <th >Trainer</th>
            <th >Program Date</th>
            <th >Program Duration</th>
            <th >Trainer Fees</th>
            <th >Payment Status</th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($payment)) {
            $num = 0;
            foreach ($payment as $p_data) {
                $num++;
                ?>
                <tr>
                    <td><?php echo $p_data->project_title; ?></td>
                    <td><?php echo $p_data->name; ?></td>
                    <td><?php echo $p_data->training_date_from; ?></td>
                    <td><?php echo $p_data->training_duration; ?></td>
                    <td><?php echo $p_data->amount; ?></td>
                    <td>
                        <?php
                        if ($p_data->trainer_paid_status == '0') {
                            ?>
                            Pending
                            <?php
                        }
                        if ($p_data->trainer_paid_status == '1') {
                            ?>
                            Paid
                            <?php
                        }
                        ?>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>