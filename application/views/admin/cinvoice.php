<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
        <style>
            td, th
            {
                font-size:12px;
            }
            .table > thead > tr > th, .table > tbody > tr > th, .table > tfoot > tr > th, .table > thead > tr > td, .table > tbody > tr > td, .table > tfoot > tr > td{
                border-top:solid 1px #000 !important;
            }
            .table-bordered > thead > tr > th, .table-bordered > tbody > tr > th, .table-bordered > tfoot > tr > th, .table-bordered > thead > tr > td, .table-bordered > tbody > tr > td, .table-bordered > tfoot > tr > td {
                border: 1px solid #000 !important;
            }
        </style>

    </head>
    <body>


        <div class="right-side">

            <div class="row" style="margin: 0px;">

                <div class="col-md-10 content-page" >

                    <?php
                    if (!empty($erequest)) {
                        ?>
                        <strong>Invoice Revision Request</strong>
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Description</th>
                                    <th>Date</th>
                                    <th>Status</th>
                                    <th>Status Date</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($erequest as $er_data) {
                                    ?>
                                    <tr>
                                        <td><?php echo $er_data->er_desc; ?></td>
                                        <td><?php echo date_formate($er_data->er_date); ?></td>
                                        <td>
                                    <?php 
                                    if($er_data->er_status=='0'){
                                        echo 'Pending';
                                    }
                                    if($er_data->er_status=='1'){
                                        echo 'Approved';
                                    }
                                    if($er_data->er_status=='2'){
                                        echo 'Rejected';
                                    }
                                    ?>
                                </td>
                                <td>
                                    <?php
                                    if($er_data->er_status!='0'){
                                        echo date_formate($er_data->er_status_date);
                                    }
                                    ?>
                                </td>
                                        <td>
                                            <?php if($er_data->er_status=='0'){ ?>
                                            <a href="#" cid="<?php echo $er_data->ci_id; ?>" eid="<?php echo $er_data->er_id; ?>" class="btn-sm btn-success accept-btn">Approve</a>
                                            <a href="#" cid="<?php echo $er_data->ci_id; ?>" eid="<?php echo $er_data->er_id; ?>" class="btn-sm btn-warning reject-btn">Reject</a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </tbody>
                        </table>
                        <?php
                    }
                    ?>

                    <div class="panel panel-default" style="margin-top:10px;">
                        <div class="panel-body" >
                            <table class="table table-bordered" >
                                <tr style="font-size:15px;">
                                    <td colspan="3" style="text-align: center;"><h4 style="font-size:15px;">Tax Invoice</h4></td>
                                </tr>
                                <tr>
                                    <td rowspan="3" style="width:500px;">To, <br/>
                                        <?php echo $client[0]->client_name; ?><br/>
                                        <?php
                                        if (!empty($invoice)) {
                                            echo $invoice[0]->ci_gst_address;
                                        }
                                        ?>
                                    </td>


                                    <td>Invoice Number</td>
                                    <td><?php echo $invoice[0]->ci_number; ?><?php //echo $invoice[0]->ci_id;                     ?></td>
                                </tr>
                                <tr>
                                    <td>Invoice Date</td>
                                    <td><?php echo date_formate_short($invoice[0]->ci_date); ?></td>
                                </tr>
                                <tr>
                                    <td>Ref. No</td>
                                    <td><?php echo $invoice[0]->ci_refNumber; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>State Code :  </strong><?php
                                        if (!empty($invoice)) {
                                            echo $invoice[0]->ci_state_code;
                                        }
                                        ?> 
                                    </td>
                                    <td>Name</td>
                                    <td><?php echo $invoice[0]->ci_name; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>GST NO: </strong> <?php
                                        if (!empty($invoice)) {
                                            echo $invoice[0]->ci_gst;
                                        }
                                        ?>
                                    </td>
                                    <td>Email id</td>
                                    <td><?php echo $invoice[0]->ci_email; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>PO. No</td>
                                    <td><?php echo $invoice[0]->ci_poNumber; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                    <td>SAC Code</td>
                                    <td>999293</td>
                                </tr>
                            </table>
                            <?php
                            $total = 0;
                            $no = 1;
                            $dc = 0;
                            ?>
                            <table class="table table-bordered">
                                <tr style="font-weight: bold;">
                                    <td>Sl. No</td>
                                    <td>Description</td>
                                    <td>Program</td>
                                    <td>Date</td>
                                    <td>Location</td>
                                    <td>No. Days</td>
                                    <td>Amount</td>
                                </tr>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td>Professional Fees for training program</td>
                                    <td rowspan="<?php echo $dc; ?>" valign="bottom"><?php echo $project[0]->project_title; ?></td>
                                    <td rowspan="<?php echo $dc; ?>"><?php echo date_formate_short($project[0]->training_start_date); ?></td>
                                    <td rowspan="<?php echo $dc; ?>"><?php echo $project[0]->location_of_training; ?></td>
                                    <td rowspan="<?php echo $dc; ?>"><?php echo $project[0]->training_duration; ?></td>
                                    <td style="text-align:right;"><?php
                                        $total_fees = $project[0]->total_fees * $project[0]->training_duration;
                                        echo number_format($total_fees, 2);
                                        ?></td>
                                </tr>
                                <?php
                                $total = $total + $total_fees;
                                if ($client[0]->debit_note == 'No') {
                                    if (!empty($exp)) {
                                        foreach ($exp as $ex_data) {
                                            if ($ex_data->absorb == 'N') {
                                                $no++;
                                                ?>
                                                <tr>
                                                    <td><?php echo $no; ?></td>
                                                    <td><?php echo $ex_data->expense_type; ?></td>
                                                    <td><?php echo $ex_data->in_amount; ?></td>
                                                </tr>
                                                <?php
                                                $total = $total + $ex_data->in_amount;
                                            }
                                        }
                                    }
                                    ?>
                                    <?php
                                }
                                ?>
                                <tr style="font-size:15px;">
                                    <td colspan="6" class="text-right">Gross Total</td>
                                    <td style="width:100px; text-align: right;"><?php echo number_format($total, 2); ?></td>
                                </tr>
                            </table>
                            <?php
                            $company = $this->admin_model->get_single_company($invoice[0]->ci_company);
                            $cmState = $company[0]->gst_state_code;
                            $cState = $company[0]->gst_state_code;
                            if (!empty($invoice)) {
                                $cState = $invoice[0]->ci_state_code;
                            }
                            ?>
                            <table class="table table-bordered">
                                <tr>
                                    <td>PAN No.</td>
                                    <td><?php echo $company[0]->pan_no; ?></td>
                                    <td class="text-right">CGST On Professional Fees @9%</td>
                                    <td style="width:100px; text-align: right;">
                                        <?php
                                        if ($cState == $cmState) {
                                            $cgst = round((9 / 100) * $total);
                                            echo number_format($cgst, 2);
                                        } else {
                                            echo '-';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>GSTIN</td>
                                    <td><?php echo $company[0]->gstn; ?></td>
                                    <td class="text-right">SGST On Professional Fees @9%</td>
                                    <td style="text-align: right;"><?php
                                        if ($cState == $cmState) {
                                            $sgst = round((9 / 100) * $total);
                                            echo number_format($sgst, 2);
                                        } else {
                                            echo '-';
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>TAN No</td>
                                    <td><?php echo $company[0]->tan_no; ?></td>
                                    <td class="text-right">IGST On Professional Fees @18%</td>
                                    <td style="text-align: right;">

                                        <?php
                                        if ($cState != $cmState) {
                                            $gstamt = round((18 / 100) * $total);
                                            echo number_format($gstamt, 2);
                                        } else {
                                            $gstamt = $cgst + $sgst;
                                            echo '-';
                                        }
                                        $gTotal = $total + $gstamt;
                                        ?>

                                    </td>
                                </tr>
                                <tr style="font-size:20px;">
                                    <td colspan="2"></td>
                                    <td class="text-right">Total</td>
                                    <td style="text-align:right;">
                                        <?php echo number_format($gTotal, 2); ?>
                                    </td>
                                </tr>
                                <tr><td colspan="4"><strong>In Words: 
                                            <?php
                                            $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                            echo "Rs. " . ucwords($f->format($gTotal)) . " Only.";
                                            ?>
                                        </strong></td></tr>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td>Beneficiary</td>
                                    <td><?php echo $company[0]->company_name; ?></td>
                                    <td rowspan="5" style="text-align: center;">For, <?php echo $company[0]->company_name; ?>
                                        <br/><br/><br/><br/><br/><br/><br/><br/>
                                        Authorized Signatory
                                    </td>
                                </tr>
                                <tr>
                                    <td>Account No</td>
                                    <td><?php echo $company[0]->account_no; ?></td>
                                </tr><tr>
                                    <td>IFSC Code</td>
                                    <td><?php echo $company[0]->ifsc_no; ?></td>
                                </tr>
                                <tr>
                                    <td>Bank & Branch</td>
                                    <td><?php echo $company[0]->bank_branch; ?></td>
                                </tr>
                                <tr>
                                    <td class="text-center" colspan="2">
                                        <strong>
                                            * Thanking you and assuring our best services at all times
                                        </strong>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <p style="page-break-before: always"/>
                    <?php if ($client[0]->debit_note == 'Yes') {
                        ?>
                        <p style="margin-top:10px;">&nbsp;</p>
                        <div class="panel panel-default" >
                            <div class="panel-body">
                                <table class="table table-bordered" >
                                    <tr style="font-size:15px;">
                                        <td colspan="3" style="text-align: center;"><h4 style="font-size:15px;">Debit Note</h4></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="3">To, <br/>
                                            <?php echo $client[0]->client_name; ?><br/>
                                            <?php
                                            if (!empty($invoice)) {
                                                echo $invoice[0]->ci_gst_address;
                                            }
                                            ?>
                                        </td>
                                        <td>Invoice Number</td>
                                        <td><?php echo $invoice[0]->ci_number; ?>D</td>
                                    </tr>
                                    <tr>
                                        <td>Invoice Date</td>
                                        <td><?php echo date_formate_short($invoice[0]->ci_date); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Ref. No</td>
                                        <td><?php echo $invoice[0]->ci_refNumber; ?></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>State Code :  </strong><?php
                                            if (!empty($invoice)) {
                                                echo $invoice[0]->ci_state_code;
                                            }
                                            ?> 
                                        </td>
                                        <td>Name</td>
                                        <td><?php echo $invoice[0]->ci_name; ?></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>GST NO: </strong> <?php
                                            if (!empty($invoice)) {
                                                echo $invoice[0]->ci_gst;
                                            }
                                            ?>
                                        </td>
                                        <td>Email id</td>
                                        <td><?php echo $invoice[0]->ci_email; ?></td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>PO. No</td>
                                        <td><?php echo $invoice[0]->ci_poNumber; ?></td>
                                    </tr>
                                    <tr>
                                        <td>
                                        </td>
                                        <td>SAC Code</td>
                                        <td>999293</td>
                                    </tr>
                                </table>
                                <?php
                                $total = 0;
                                $no = 0;
                                ?>
                                <table class="table table-bordered">
                                    <tr style="font-weight: bold;">
                                        <td style="width:80px;">Sl. No</td>
                                        <td>Description</td>
                                        <td>Program</td>
                                        <td>Date</td>
                                        <td>Amount</td>
                                    </tr>
                                    <?php
                                    if (!empty($exp)) {
                                        foreach ($exp as $ex_data) {
                                            if ($ex_data->absorb == 'N') {
                                                $no++;
                                                ?>
                                                <tr>
                                                    <td><?php echo $no; ?></td>
                                                    <td><?php echo $ex_data->expense_type; ?></td>
                                                    <td><?php echo $project[0]->project_title; ?></td>
                                                    <td><?php echo date_formate_short($project[0]->training_start_date); ?></td>
                                                    <td style="text-align:right;"><?php echo number_format($ex_data->in_amount, 2); ?></td>
                                                </tr>
                                                <?php
                                                $total = $total + $ex_data->in_amount;
                                            }
                                        }
                                    }
                                    ?>
                                    <tr style="font-size:18px;">
                                        <td colspan="4" class="text-right">Gross Total</td>
                                        <td style="width:100px; text-align:right;"><?php echo number_format($total, 2); ?></td>
                                    </tr>
                                </table>
                                <?php
                                $company = $this->admin_model->get_single_company($invoice[0]->ci_company);
                                $cmState = $company[0]->gst_state_code;
                                $cState = $company[0]->gst_state_code;
                                if (!empty($invoice)) {
                                    $cState = $invoice[0]->ci_state_code;
                                }
                                ?>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>PAN No.</td>
                                        <td><?php echo $company[0]->pan_no; ?></td>
                                        <td class="text-right">CGST On Professional Fees @9%</td>
                                        <td style="width:100px; text-align:right;">
                                            <?php
                                            if ($cState == $cmState) {
                                                $cgst = round((9 / 100) * $total);
                                                echo number_format($cgst, 2);
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>GSTIN</td>
                                        <td><?php echo $company[0]->gstn; ?></td>
                                        <td class="text-right">SGST On Professional Fees @9%</td>
                                        <td style="text-align:right;">
                                            <?php
                                            if ($cState == $cmState) {
                                                $sgst = round((9 / 100) * $total);
                                                echo number_format($sgst, 2);
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>TAN No</td>
                                        <td><?php echo $company[0]->tan_no; ?></td>
                                        <td class="text-right">IGST On Professional Fees @18%</td>
                                        <td style="text-align:right;">
                                            <?php
                                            if ($cState != $cmState) {
                                                $gstamt = round((18 / 100) * $total);
                                                echo number_format($gstamt, 2);
                                            } else {
                                                $gstamt = $cgst + $sgst;
                                                echo '-';
                                            }
                                            $gTotal = $total + $gstamt;
                                            ?>
                                        </td>
                                    </tr>
                                    <tr style="font-size:20px;">
                                        <td colspan="2"></td>
                                        <td class="text-right">Total</td>
                                        <td style="text-align:right;">
                                            <?php echo number_format($gTotal, 2); ?>
                                        </td>
                                    </tr>
                                    <tr><td colspan="4"><strong>In Words: 
                                                <?php
                                                $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                                echo "Rs. " . ucwords($f->format($gTotal)) . " Only.";
                                                ?>
                                            </strong></td></tr>
                                </table>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>Beneficiary</td>
                                        <td><?php echo $company[0]->company_name; ?></td>
                                        <td rowspan="5" style="text-align: center;">For, <?php echo $company[0]->company_name; ?>
                                            <br/><br/><br/><br/><br/><br/><br/><br/>
                                            Authorized Signatory
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Account No</td>
                                        <td><?php echo $company[0]->account_no; ?></td>
                                    </tr><tr>
                                        <td>IFSC Code</td>
                                        <td><?php echo $company[0]->ifsc_no; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Bank & Branch</td>
                                        <td><?php echo $company[0]->bank_branch; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" colspan="2">
                                            <strong>
                                                * Thanking you and assuring our best services at all times
                                            </strong>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <?php
                    }
                    ?>

                </div>
            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {

                $('.accept-btn').click(function (e) {
                    e.preventDefault();
                    var eid = $(this).attr('eid');
                    var cid = $(this).attr('cid');
                    var project = '<?php echo $project[0]->project_title; ?>';
                    var client = '<?php echo $client[0]->client_name; ?>';
                    var pdate = '<?php echo date_formate_short($project[0]->training_start_date); ?>';
                    $('.page_spin').show();
                    var dataString = "cid="+cid+"&eid=" + eid + "&project="+project+"&client="+client+"&pdate="+pdate+"&status=1&page=accept_edit_request";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                });
                
                $('.reject-btn').click(function (e) {
                    e.preventDefault();
                    var eid = $(this).attr('eid');
                    var cid = $(this).attr('cid');
                    $('.page_spin').show();
                    var dataString = "cid="+cid+"&eid=" + eid + "&status=2&page=accept_edit_request";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                });

            });
        </script>

    </body>
</html>