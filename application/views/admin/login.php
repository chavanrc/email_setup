<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Wagons | Login</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <div class="page_spin">
            <br/>
            <div class="spin_icon">
                <i class="fa fa-spinner fa-spin"></i><br/>
                <span>One moment ...</span>
            </div>
        </div>
        <div class="col-md-4 col-md-offset-4" style="margin-top: 7%;">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h2 class="panel-title" style="font-size:23px;">Login <i class="fa fa-sign-in"></i></h2>
                </div>
                <div class="panel-body">
                    <div class="alert alert-danger login_error" style="display: none;">Wrong Username & Password</div>
                    <form action="" method="POST" id="admin_login_form">
                        <div class="form-group">
                            <input type="text" name="username" id="username" class="form-control" placeholder="Username"/>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" id="password" class="form-control" placeholder="Password"/>
                        </div>
                        <div class="form-group">
                            <a href="#" class="forgot-btn">Forgot Password ?</a> <button type="submit" class="btn btn-primary pull-right"><i class="fa fa-sign-in"></i> Login</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="ForgotPass-wrap" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Forgot Password</h4>
                    </div>
                    <form action="" method="POST" id="forgot-password-form">
                        <div class="modal-body">
                            <div class="alert alert-danger forgot_error" style="display: none;">Email id is not found</div>
                            <div class="alert alert-success forgot_success" style="display: none;">Reset link is sent to your email id.</div>
                            <p>Password reset link will send to your email id.</p>

                            <div class="form-group">
                                <input type="text" id="reg_email" name="reg_email" class="form-control" placeholder="Enter your registered email id"/>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {

                $('.forgot-btn').click(function (e) {
                    e.preventDefault();
                    $('#ForgotPass-wrap').modal('show');
                });

                $('#username').focus(function () {
                    $('.login_error').hide();
                });

                $('#password').focus(function () {
                    $('.login_error').hide();
                });

                $("#admin_login_form").validate({
                    rules: {
                        username: "required",
                        password: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                        example5: {
                            placement: 'right',
                            html: true
                        }
                    },
                    submitHandler: function (form) {
                        admin_login();
                    }


                });

                $("#forgot-password-form").validate({
                    rules: {
                        reg_email: {
                            required: true,
                            email: true
                        },
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        forgot_password();
                    }
                });
                
                function forgot_password()
                {
                    var email = $('#reg_email').val();

                    $('.page_spin').show();
                    var dataString = "email="+email+"&page=forgot_password";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            var replay = $.trim(data);
                            if (replay == '1')
                            {
                                $('.forgot_success').show();
                                $('#reg_email').val('');
                            }
                            else
                            {
                                $('.forgot_error').show();
                            }

                        }, //success fun end
                    });//ajax end
                }

                function admin_login()
                {
                    var username = $('#username').val();
                    var password = $('#password').val();
                   

                    $('.page_spin').show();
                    var dataString = "username=" + username + "&password=" + password + "&page=admin_login";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            var replay = $.trim(data);
                            if (replay == 'admin' || replay == 'drona')
                            {
                                window.location.href = "<?php echo base_url(); ?>admin";
                            }
                            else if(replay == 'project manager')
                            {
                                window.location.href = "<?php echo base_url(); ?>projectmanager";
                            }
                            else if(replay == 'trainer')
                            {
                                window.location.href = "<?php echo base_url(); ?>trainer";
                            }
                            else if(replay == 'business manager')
                            {
                                window.location.href = "<?php echo base_url(); ?>bdm";
                            }
                            else if(replay == 'content manager')
                            {
                                window.location.href = "<?php echo base_url(); ?>cm";
                            }
                            else if(replay == 'admin support')
                            {
                                window.location.href = "<?php echo base_url(); ?>support";
                            }
                            else if(replay == 'training support')
                            {
                                window.location.href = "<?php echo base_url(); ?>training";
                            }
                            else if(replay == 'account')
                            {
                                window.location.href = "<?php echo base_url(); ?>account";
                            }
                            else if(replay == 'director')
                            {
                                window.location.href = "<?php echo base_url(); ?>director";
                            }
                            else
                            {
                                $('.login_error').show();
                            }

                        }, //success fun end
                    });//ajax end
                }



            });
        </script>
    </body>
</html>