<div class="menu">
    <div class="container" >
        <div class="row">
            <div class="col-md-12">
                <nav>
                    <div class="container">
                        <div class="nav-inner"> 
                            <div class="hidden-desktop" id="mobile-menu">

                                <ul class="navmenu">

                                    <li>

                                        <div class="menutop">

                                            <div class="toggle"> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar">
                                                </span></div>
                                            <h2> Menu</h2>

                                        </div>

                                        <ul style="display:none;" class="submenu">

                                            <li>

                                                <ul class="topnav">
                                                    <!-- Mobile Menu-->


                                                    <li><a href="index.php"><span> Home</span></a></li>

                                                    <li><a href="about.php"><span>About Us</span></a></li>

                                                    <li class="level0 parent drop-menu"><a href="javascript:void(0);"><span>MBBS</span> </a>

                                                        <ul style="display: none;" class="level1">

                                                            <li class="level1"> <a href="#"> <span>Part - 1</span> </a> 
                                                                <ul>
                                                                    <li class="level1"> <a href="#"> <span>Part - 1</span> </a> </li>

                                                                    <li class="level1 nav-10-3"> <a href="#"> <span>Part - 1</span> </a> </li>

                                                                    <li class="level1 nav-10-4"> <a href="#"> <span>Part - 1</span> </a> </li>
                                                                </ul>

                                                            </li>

                                                            <li class="level1 nav-10-3"> <a href="#"> <span>Part - 1</span> </a> </li>

                                                            <li class="level1 nav-10-4"> <a href="#"> <span>Part - 1</span> </a> </li>

                                                            <li class="level1"> <a href="#"> <span>Part - 1 </span> </a> 
                                                                <ul>
                                                                    <li class="level1"> <a href="#"> <span>Part - 1</span> </a> </li>

                                                                    <li class="level1 nav-10-3"> <a href="#"> <span>Part - 1</span> </a> </li>

                                                                    <li class="level1 nav-10-4"> <a href="#"> <span>Part - 1</span> </a> </li>
                                                                </ul>
                                                            </li>

                                                            <li class="level1 nav-10-3"> <a href="#"> <span>Part - 1</span> </a> </li>

                                                            <li class="level1 nav-10-4"> <a href="#"> <span>Part - 1</span> </a> </li>

                                                        </ul>

                                                    </li>

                                                    <li class="level0 nav-6 level-top parent"> <a href="javascript:void(0);" class="level-top"> <span>BDS</span> </a>

                                                        <!-- DROPDOWN MENU -->

                                                    </li>

                                                    <li class="level0 parent drop-menu"><a href="javascript:void(0);"><span>BMS</span> </a></li>

                                                    <li class="level0 nav-8 level-top"> <a href="javascript:void(0);" class="level-top"> 
                                                            <span>Homeopathy</span> </a> </li>

                                                    <li class="level0 parent drop-menu"><a href="javascript:void(0);">
                                                            <span>HOW IT WORKS</span> </a>
                                                        <ul>
                                                            <li class="level1"> <a href="#"> <span>Part - 1</span> </a> </li>

                                                            <li class="level1 nav-10-3"> <a href="#"> <span>Part - 1</span> </a> </li>

                                                            <li class="level1 nav-10-4"> <a href="#"> <span>Part - 1</span> </a> </li>
                                                        </ul>                           
                                                    </li>

                                                    <li class="level0 nav-8 level-top"> <a href="contact_us.php" class="level-top"> <span>Contact</span> </a> </li>

                                                    <!-- JavaScript -->

                                                    <!-- End Mobile menu-->

                                                </ul>

                                            </li>

                                        </ul>

                                    </li>

                                </ul>

                            </div> <!--End mobile-menu -->

                            <ul id="nav" class="hidden-xs">
                                <li id="nav-home" class="level0 parent drop-menu"><a href="<?php echo base_url(); ?>"><span>Home</span></a></li>

                                <?php
                                $degree = $CI->home_model->get_degree();
                                if (!empty($degree)) {
                                    foreach ($degree as $dg_data) {
                                        ?>
                                        <li class="level0 parent drop-menu"><a href="<?php echo base_url() ?><?php echo urlencode($dg_data->c_title); ?>"><span><?php echo $dg_data->c_title; ?></span> </a>
                                        <?php
                                        $year = $CI->home_model->get_year_list($dg_data->c_title);
                                        if (!empty($year)) {
                                            ?>
                                                <ul style="display: none;" class="level1">
                                                <?php
                                                foreach ($year as $y_data) {
                                                    ?>
                                                        <li class="level1"> <a href="<?php echo base_url() ?><?php echo urlencode($dg_data->c_title); ?>/<?php echo urlencode($y_data->y_id); ?>"> <span><?php echo $y_data->y_id; ?></span> </a>
                                                        <?php
                                                        $subject = $CI->home_model->get_subject_list($y_data->y_id, $dg_data->c_title);
                                                        if (!empty($subject)) {
                                                            ?>
                                                                <ul>
                                                                <?php
                                                                foreach ($subject as $sb_data) {
                                                                    ?>
                                                                        <li class="level1"> <a href="<?php echo base_url() ?><?php echo urlencode($dg_data->c_title); ?>/<?php echo urlencode($y_data->y_id); ?>/<?php echo urlencode($sb_data->sc_id); ?>"> <span><?php echo $sb_data->sc_id; ?></span> </a> </li>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </ul>
                                                                    <?php
                                                                }
                                                                ?>
                                                        </li>
                                                                <?php
                                                            }
                                                            ?>
                                                </ul>
                                                        <?php
                                                    }
                                                    ?>
                                        </li>
                                                <?php
                                            }
                                        }
                                        ?>
                                <li id="nav-home" class="level0 parent drop-menu"><a href="about.php"><span>ABOUT US</span></a></li>
                                <li class="level0 parent drop-menu"><a href="javascript:void(0);"><span>HOW IT WORKS</span> </a> </li>
                                <li class="level0 nav-8 level-top"> <a href="contact.php" class="level-top"> <span>Contact</span> </a> </li>
                            </ul>
                        </div>
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="page_spin">
    <br/>
    <div class="spin_icon">
        <i class="fa fa-spinner fa-spin"></i><br/>
        <span>One moment ...</span>
    </div>
</div>


