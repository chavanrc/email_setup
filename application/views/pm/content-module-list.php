<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Project Manager | Clients</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Content Modules</h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                            <div class="clearfix"></div>

                            <div class="col-md-6" style="margin-top:15px;">
                                <select class="form-control pull-right" id="client" name="client">
                                    <option value=""> - Select Client -</option>
                                    <?php
                                    if (!empty($clients)) {
                                        foreach ($clients as $cl_data) {
                                            ?>
                                            <option value="<?php echo $cl_data->client_id; ?>" <?php
                                            if ($cid == $cl_data->client_id) {
                                                echo 'selected';
                                            }
                                            ?> ><?php echo $cl_data->client_name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                        <div class="panel-body">
                             <table class="table table-bordered table-striped" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Module</th>
                                        <th>Status</th>
                                        <th>Status Last updated</th>
                                        <th>No. of Programs</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($modl)) {
                                        $no = 0;
                                        foreach ($modl as $cn_data) {
                                            $no++;
                                            ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $cn_data->module_name; ?></td>
                                                <td>
                                                    <?php
                                                    if ($cn_data->content_status == '1') {
                                                        echo 'Closed for update';
                                                    }
                                                    if ($cn_data->content_status == '2') {
                                                        echo 'Open for update';
                                                    }
                                                    if ($cn_data->content_status == '0') {
                                                        echo 'No files';
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($cn_data->content_status != '0' && $cn_data->content_status_date != '0000-00-00 00:00:00') {
                                                        echo date_formate($cn_data->content_status_date);
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $pCount = $this->projectmanager_model->get_module_programCount($cn_data->module_name,$this->session->userdata('pm_code'));
                                                    if(!empty($pCount)){
                                                        echo $pCount[0]->tp;
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="5">No records found</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {
                
                $('#client').change(function (e) {
                        var cid = $(this).val();
                        window.location.href = "<?php echo base_url(); ?>projectmanager/content_modules/" + cid;
                    });

                $("#assign-pm-form").validate({
                    rules: {
                        pm: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        assign_pm();
                    }
                });

            });
        </script>

    </body>
</html>