<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Update - Project Manager</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>admin/project_manager" class="btn btn-danger"><i class="fa fa-backward"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <?php
                if ($msg != 0 && $msg != 'new') {
                    ?>
                    <div class="alert alert-success col-md-4 col-md-offset-4">
                        Project Manager Updated Successfully.
                    </div>
                    <?php
                }
                ?>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-edit"></i> <?php echo $pm[0]->name; ?> </h2>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg =='error') {
                                ?>
                                <div class="alert alert-danger col-md-4 col-md-offset-4">
                                    This email id already exist.
                                </div>
                                <?php
                            }
                            ?>
                            <div class="clearfix"></div>
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-pm-form">
                                <input type="hidden" name="uid" value="<?php echo $pm[0]->user_id; ?>">
                                <input type="hidden" name="c_email" value="<?php echo $pm[0]->email; ?>">
                                <input type="hidden" name="cimg" value="<?php echo $pm[0]->user_photo; ?>">
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Name <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-user"></i></span>
                                            <input type="text" name="fname" value="<?php echo $pm[0]->name; ?>" class="form-control" placeholder="Full Name">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Email id <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-envelope"></i></span>
                                            <input type="text" name="email" value="<?php echo $pm[0]->email; ?>" class="form-control" placeholder="Email id">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Contact Number <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"><i class="fa fa-phone"></i></span>
                                            <input type="text" name="mobile" value="<?php echo $pm[0]->contact_number; ?>" class="form-control" placeholder="Mobile Number">
                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Change Photo
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="file" name="photo" accept='image/*' class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">

            $(document).ready(function () {
                $("#add-pm-form").validate({
                    rules: {
                        fname: "required",
                        password: "required",
                        email: {
                            required: true,
                            email: true
                        },
                        mobile: {
                            required: true,
                            number: true
                        },
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });
            });

        </script>

    </body>
</html>