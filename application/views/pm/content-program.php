<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Trainer Support | Add Program</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/datepicker/jquery.datetimepicker.css">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Content Development</h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-plus"></i> Content Development Details</h2>
                        </div>
                        <div class="panel-body">
                            <?php
                            $pretest = 0;
                            $posttest = 0;
                            $prep = '';
                            $handbook = '';
                            $charge = '';
                            $test = 0;
                            $pmodule = "";
                            $content_under_update = "";
                            $handbook_content = "";
                            $handbook_qnt = "";
                            $handbook_charge = "";
                            $handbook_cost = "";
                            $handbook_desc = "";
                            $handbook_courier = "";
                            $content_cost = "";

                            if (!empty($pcontent)) {
                                $pretest = $pcontent[0]->pre_test;
                                $posttest = $pcontent[0]->post_test;
                                $prep = $pcontent[0]->content_preperation;
                                $handbook = $pcontent[0]->participant_handbook;
                                $charge = $pcontent[0]->content_charges;
                                $test = $pcontent[0]->physical_paper_test;
                                $content_under_update = $pcontent[0]->content_under_update;
                                $pmodule = $pcontent[0]->module;
                                $handbook_content = $pcontent[0]->handbook_content;
                                $handbook_qnt = $pcontent[0]->handbook_qnt;
                                $handbook_charge = $pcontent[0]->handbook_charge;
                                $handbook_cost = $pcontent[0]->handbook_cost;
                                $handbook_desc = $pcontent[0]->handbook_desc;
                                $handbook_courier = $pcontent[0]->handbook_courier;
                                $content_cost = $pcontent[0]->content_cost;
                            }
                            ?>
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-program-form">

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Client <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="client">
                                            <option value=""> - Select -</option>
                                            <?php
                                            if (!empty($client)) {
                                                foreach ($client as $cl_data) {
                                                    ?>
                                                    <option value="<?php echo $cl_data->client_id; ?>"> <?php echo $cl_data->client_name; ?> </option>
                                                    <?php
                                                }
                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Content Title <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="pname" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Location
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="location" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Content Developer
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <div class="input-group">
                                            <input type="text" id="searchKey" name="searchKey" class="form-control" placeholder="Search">
                                            <span class="input-group-btn">
                                                <button class="btn btn-default searchT-btn" type="button">Search</button>
                                            </span>
                                        </div><!-- /input-group -->
                                       	<div class="trainerResult">

                                        </div>
                                        <input type="hidden" name="trainer" id="trainer"/>
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Submission Date 
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="date" id="sdate" name="sdate"  placeholder="yy-mm-dd" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Professional Fees per day <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number" name="fees" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Duration in Days <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number"  name="duration" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Charges Agreed With Client / Day <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <input type="number" value="0"  name="client_charges" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Participants Level 
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="participant_level">
                                            <option value=""> - Select -</option>
                                            <option value="Beginners"> Beginners </option>
                                            <option value="Juniors"> Juniors </option>
                                            <option value="Intermediate"> Intermediate </option>
                                            <option value="Advanced"> Advanced </option>
                                            <option value="Highly Experienced"> Highly Experienced </option>
                                            <option value="Above 40 years Staff"> Above 40 years Staff </option>
                                            <option value="Unskilled Labor"> Unskilled Labor </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Assessment Tests
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label style="margin-right: 20px;"><input type="checkbox" <?php
                                            if ($pretest == '1') {
                                                echo 'checked';
                                            }
                                            ?>  name="pre_test" value='1'>Pre Assessment Applicable</label>
                                        <label style="margin-right: 20px;"><input type="checkbox" <?php
                                            if ($posttest == '1') {
                                                echo 'checked';
                                            }
                                            ?> name="post_test" value='1'>Post Assessment Applicable</label>

                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Content Module
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="content_module" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Content Preperation
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       	<select class="form-control" name="content_preperation" id="content_preperation">
                                            <option value="" selected > Select</option>
                                            <option value="Prepare by CM" > Prepare by Content Manager</option>
                                        </select>
                                        <div class="clearfix"></div>
                                        <div id="c_up" <?php if ($prep == 'Already exist') { ?> style="display:inline;" <?php } else { ?> style="display:none;"<?php } ?>>
                                            <input type="checkbox" value="Y" id="content_under_update" name="content_under_update" <?php if ($content_under_update == "Y") echo "checked"; ?>> Content Requires Updation ?
                                        </div>
                                    </div>
                                </div>


                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Participant Handbook <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" id="participant_handbook" name="participant_handbook">
                                            <option value="Not Required" <?php
                                            if ($handbook == 'Not Required') {
                                                echo 'selected';
                                            }
                                            ?> > Not Required</option>
                                            <option value="Print + Dispatch By Wagons" <?php
                                            if ($handbook == 'Print + Dispatch By Wagons') {
                                                echo 'selected';
                                            }
                                            ?> > Print + Dispatch By Wagons</option>
                                            <option value="Print By Client" <?php
                                            if ($handbook == 'Print By Client') {
                                                echo 'selected';
                                            }
                                            ?> > Print By Client </option>

                                        </select>

                                    </div>
                                </div>
                                <div class="handbookWraper" <?php if ($handbook != 'Print + Dispatch By Wagons') { ?> style="display: none;" <?php } ?>>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Handbook Content <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control" name="handbook_content">
                                                <option value=""> - Select -</option>
                                                <option value="Already exist" <?php
                                                if ($handbook_content == 'Already exist') {
                                                    echo 'selected';
                                                }
                                                ?>>Already exist</option>
                                                <option value="Prepare / Update By CM" <?php
                                                if ($handbook_content == 'Prepare / Update By CM') {
                                                    echo 'selected';
                                                }
                                                ?>>Prepare / Update By CM</option>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Notes for Printing<span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea class="form-control" name="handbook_desc"><?php echo $handbook_desc; ?></textarea>
                                            <span>Mention type of paper, binding, cover type etc..</span>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Print Quantity (No. of Copies)<span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" value="<?php echo $handbook_qnt; ?>" placeholder="No. Copeis" name="handbook_qnt"/>

                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12"> Handbook Printing Charges <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control" id="handbook_charge" name="handbook_charge">
                                                <option value=""> - Select -</option>
                                                <option value="NA" <?php
                                                if ($handbook_charge == 'NA') {
                                                    echo 'selected';
                                                }
                                                ?> > Not Applicable </option>
                                                <option value="Absorb by Wagons" <?php
                                                if ($handbook_charge == 'Absorb by Wagons') {
                                                    echo 'selected';
                                                }
                                                ?> > Wagons To Absorb </option>
                                                <option value="Bill Client" <?php
                                                if ($handbook_charge == 'Bill Client') {
                                                    echo 'selected';
                                                }
                                                ?> > Wagons to Bill Client </option>


                                            </select>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="form-group hcostWraper" <?php if ($handbook_charge != 'Bill Client') { ?>style="display: none;"<?php } ?> >
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Printing Cost Limit<span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" value="<?php echo $handbook_cost; ?>" placeholder="Total Cost" name="handbook_cost"/>

                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Dispatch Location <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea class="form-control" name="handbook_courier"><?php echo $handbook_courier; ?></textarea>

                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Content Development Charges <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" id="content_charges" name="content_charges">
                                            <option value=""> - Select -</option>
                                            <option value="NA" <?php
                                            if ($charge == 'NA') {
                                                echo 'selected';
                                            }
                                            ?> > Not Applicable </option>
                                            <option value="Absorb by Wagons" <?php
                                            if ($charge == 'Absorb by Wagons') {
                                                echo 'selected';
                                            }
                                            ?> > Wagons To Absorb </option>
                                            <option value="Bill Client" <?php
                                            if ($charge == 'Bill Client') {
                                                echo 'selected';
                                            }
                                            ?> > Wagons to Bill Client </option>


                                        </select>
                                        <input type="hidden" class="form-control" value="" placeholder="Total Cost" name="content_cost"/>
                                    </div>
                                </div>
                                
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Phyisical Paper Test? 
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       	<label style="margin-right: 20px;"><input type="checkbox" <?php
                                            if ($test == '1') {
                                                echo 'checked';
                                            }
                                            ?> name="physical_paper_test" value='1'>Yes</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/additional-methods.js"></script>

        <script type="text/javascript">

            $(document).ready(function () {

                $('.searchT-btn').click(function (e) {
                    e.preventDefault();
                    $('.afterSearch').hide();
                    var searchKey = $("#searchKey").val();
                    $('.page_spin').show();
                    var dataString = "searchkey=" + searchKey + "&page=search_trainer";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            $('.trainerResult').html(data);
                        }, //success fun end
                    });//ajax end
                });

                $(document).on('click', '.trainer-item', function (e) {
                    e.preventDefault();
                    var uid = $(this).attr('uid');
                    var name = $(this).attr('name');
                    $('#trainer').val(uid);
                    $("#searchKey").val(name);
                    $(this).siblings('.list-group-item').hide();
                    $(this).hide();
                });

                $("#content_preperation").change(function () {

                    if ($(this).val() == "Already exist")
                    {
                        $("#c_up").show();
                        $('#content_under_update').prop('checked', false); // Unchecks it

                    }
                    else
                    {
                        $("#c_up").hide();
                        $('#content_under_update').prop('checked', false); // Unchecks it
                    }
                });

//                $('#content_charges').change(function (e) {
//                    var ph = $(this).val();
//                    if (ph == 'Bill Client') {
//                        $('.ccostWraper').show();
//                    } else {
//                        $('.ccostWraper').hide();
//                    }
//                });

                $('#handbook_charge').change(function (e) {
                    var ph = $(this).val();
                    if (ph == 'Bill Client') {
                        $('.hcostWraper').show();
                    } else {
                        $('.hcostWraper').hide();
                    }
                });

                $('#participant_handbook').change(function (e) {
                    var ph = $(this).val();
                    if (ph == 'Print + Dispatch By Wagons') {
                        $('.handbookWraper').show();
                    } else {
                        $('.handbookWraper').hide();
                    }
                });

                $("#add-program-form").validate({
                    rules: {
                        pname: "required",
                        client: "required",
                        participants_type: "required",
                        client_name: "required",
                        client_charges: "required",
                        fees: "required",
                        trainer: "required",
                        handbook_content: "required",
                        handbook_qnt: "required",
                        handbook_charge: "required",
                        handbook_cost: "required",
                        content_charges: "required",
                        duration: {
                            required: true,
                            number: true
                        }
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });


            });

        </script>

    </body>
</html>