<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Trainers</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Trainers</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#"><i class="fa fa-info"></i> Active</a></li>
                            <li><a href="<?php echo base_url(); ?>projectmanager/inactive_trainers"><i class="fa fa-info"></i> Inactive</a></li>
                            <li><a href="<?php echo base_url(); ?>projectmanager/pending_trainers"><i class="fa fa-info"></i> Pending Approval</a></li>


                        </ul>

                        <?php
                        $page = 1;
                        $next = 2;
                        $totalPage = 0;
                        if (isset($_GET['page'])) {
                            $page = $_GET['page'];
                        }
                        if (!empty($count)) {
                            $totalPage = ceil($count[0]->total / 20);
                            if ($count[0]->total > 20) {
                                ?>
                                <h4 class="pull-right" style="display: inline-block; margin-left: 20px; margin-top: 10px;"><?php echo (($page - 1) * 20) + 1; ?> - <?php
                                    if ($page < $totalPage) {
                                        echo $page * 20;
                                    } else {
                                        echo $count[0]->total;
                                    }
                                    ?> of <?php echo $count[0]->total; ?> </h4>
                                <?php
                            }
                        }
                        ?>

                        <a href="<?php echo base_url(); ?>projectmanager/add_trainer" class="btn btn-sm btn-primary pull-right" style="margin-top: 10px; margin-left: 20px;"><i class="fa fa-user-plus"></i> Create New Trainer</a>
                        <a href="<?php echo base_url(); ?>projectmanager/search_trainer" class="btn btn-sm btn-info pull-right" style="margin-top:10px;"><i class="fa fa-search"></i> Search</a>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
            <div class="row" style="margin: 0px;">

                <div class="col-md-12 content-page" style="background: #fff;">
                    <table class="table table-bordered" style="margin-top: 15px;">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Contact</th>
                                <th>State</th>
                                <th>City</th>
                                <th>Industry</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            if (!empty($trainer)) {
                                $num = 0;
                                if (isset($_GET['page'])) {
                                    $num = 20 * ($_GET['page'] - 1);
                                }
                                foreach ($trainer as $tr_data) {
                                    $num++;
                                    ?>
                                    <tr>
                                        <td><?php echo $num; ?></td>
                                        <td><?php echo $tr_data->name; ?></td>
                                        <td><?php echo $tr_data->email; ?><br/><?php echo $tr_data->contact_number; ?></td>
                                        <td><?php echo $tr_data->state; ?></td>
                                        <td><?php echo $tr_data->city; ?></td>
                                        <td><?php echo str_replace("/", " ", $tr_data->industry); ?></td>
                                        <td>
                                            <a href="<?php echo base_url(); ?>projectmanager/trainer_profile/<?php echo $tr_data->user_code; ?>" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a>
                                        </td>
                                    </tr>
                                    <?php
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                    <div class="text-right" style="font-size:16px; padding: 10px;">
                        <?php
                        if ($page > 1) {
                            $next = $page + 1;
                            $prev = $page - 1;
                            ?>
                            <a href="<?php echo base_url(); ?>projectmanager/trainers/?page=1"><i class="fa fa-backward" style="font-size:12px;"></i> First</a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>projectmanager/trainers/?page=<?php echo $prev; ?>"><i class="fa fa-caret-left"></i> Prev</a>
                            <?php
                        }
                        if ($page < $totalPage) {
                            ?>
                            &nbsp;&nbsp;<a href="<?php echo base_url(); ?>projectmanager/trainers/?page=<?php echo $next; ?>">Next <i class="fa fa-caret-right"></i> </a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>projectmanager/trainers/?page=<?php echo $totalPage; ?>">Last <i class="fa fa-forward" style="font-size:12px;"></i> </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>


        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {

            });
        </script>

    </body>
</html>