<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Project Manager | Program Details</title>
        <?php include 'css_files.php'; ?>
        <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
        <style>
            .panel-body{
                line-height:25px;
            }
            .ui-datepicker{
                z-index: 99999 !important;
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Programs</a>
                    </div>
                    <?php
                    $eng_count = 0;
                    $eng_count = $CI->projectmanager_model->count_engagement_request($program[0]->project_id);
                    //echo $engage_count;
                    $today = strtotime(date('Y-m-d'));
                    $pdate = explode(' ', $program[0]->training_start_date);
                    $pdate = strtotime($pdate[0]);
                    ?>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#"><i class="fa fa-info"></i> Details</a></li>
                            <?php if ($eng_count > 0) { ?>
                                <li ><a href="<?php echo base_url(); ?>projectmanager/engagement_requests/<?php echo $program[0]->project_id; ?>" ><i class="fa fa-paper-plane"></i> Engagement Requests (<strong style="color:#0000FF;"><?php echo $eng_count; ?></strong>)</a></li>
                            <?php } ?>
                            <li><a href="<?php echo base_url(); ?>projectmanager/program_content/<?php echo $program[0]->project_id; ?>"><i class="fa fa-file-text"></i> Content Setting</a></li>
                            <li><a href="<?php echo base_url(); ?>projectmanager/program_expenses/<?php echo $program[0]->project_id; ?>"><i class="fa fa-money"></i> Program Expenses</a></li>


                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3><?php echo $program[0]->project_title; ?></h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <?php
                        if ($pdate < $today) {
                            $page = 1;
                            if (isset($_GET['page'])) {
                                $page = $_GET['page'];
                            }
                            ?>
                            <a href="<?php echo base_url(); ?>projectmanager/past_programs/?page=<?php echo $page; ?>" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a>
                        <?php } else {
                            ?>
                            <a href="<?php echo base_url(); ?>projectmanager/upcoming_programs" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a>
                        <?php } ?>
                    </div>
                    <div class="clearfix"></div>
                </div>



                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-info"></i> Programs Details </h2>
                            <?php
                            if ($program[0]->edit_status == '1') {
                                if ($program[0]->project_type == '0') {
                                    ?>
                                    <a href="<?php echo base_url(); ?>projectmanager/edit_program/<?php echo $program[0]->project_id; ?>" class="btn btn-sm btn-info pull-right" style="margin-top:-24px;"><i class="fa fa-edit"></i> Edit</a>
                                <?php } else {
                                    ?>
                                    <a href="<?php echo base_url(); ?>projectmanager/edit_content_program/<?php echo $program[0]->project_id; ?>" class="btn btn-sm btn-info pull-right" style="margin-top:-24px;"><i class="fa fa-edit"></i> Edit</a>
                                <?php }
                                ?>
                                <a href="#" class="btn btn-sm btn-info pull-right venue-btn" style="margin-top:-24px; margin-right: 15px;"><i class="fa fa-edit"></i> Update Venue</a>
                                <?php
                            } else if ($program[0]->edit_status == '0') {
                                ?>
                                <a href="#" class="btn btn-sm btn-info pull-right edit-request" style="margin-top:-24px; margin-right: 15px;"><i class="fa fa-edit"></i> Edit Request</a>
                                <?php
                            } else {
                                ?>

                                <a href="#" class="btn btn-sm btn-info pull-right edit-request" style="margin-top:-24px; margin-right: 15px;"><i class="fa fa-edit"></i> Edit Request</a>
                                <span class="pull-right" style="display: inline-block;margin-top:-24px; margin-right: 15px;">Edit Request Sent</span>
                                <?php
                            }
                            // if ($pdate >= $today) {
                            if ($program[0]->is_active == '1') {
                                ?>
                                <a href="#" class="btn-sm btn-warning cancelBtn pull-right" pid="<?php echo $program[0]->project_id; ?>" style="margin-top:-24px; margin-right: 15px;">Cancel</a>
                                <?php
                            } else {
                                ?>
                                <a href="#" class="btn-sm btn-success activeBtn pull-right" pid="<?php echo $program[0]->project_id; ?>" style="margin-top:-24px; margin-right: 15px;">Undo</a>

                                <?php
                            }
                            // }

                            if ($program[0]->is_active == '0') {
                                ?>
                                <span class="pull-right" style="margin-top:-20px; margin-right: 15px; display: inline-block;"> This Program is Canceled</span>
                                <?php
                            }
                            if ($program[0]->is_active == '2') {
                                ?>
                                <span class="pull-right" style="margin-top:-20px; margin-right: 15px; display: inline-block;"> This Program is Disabled</span>
                            <?php }
                            ?>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-2">Program Title <span class="pull-right">:</span></div><div class="col-md-10"><strong><?php echo $program[0]->project_title; ?></strong></div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">Client <span class="pull-right">:</span></div><div class="col-md-10"><strong><?php echo $program[0]->client_name; ?></strong></div>
                            <div class="clearfix"></div>

                            <div class="col-md-2">Program Zone <span class="pull-right">:</span></div><div class="col-md-10"><?php echo $program[0]->zone; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">Participant Type<span class="pull-right">:</span></div><div class="col-md-10"><?php echo $program[0]->participants_type; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">State <span class="pull-right">:</span></div><div class="col-md-10"><?php echo $program[0]->program_state; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">Location <span class="pull-right">:</span></div><div class="col-md-10"><strong><?php echo $program[0]->location_of_training; ?></strong></div>
                            <div class="clearfix"></div>
                            <div class="col-md-2"><?php
                                if ($program[0]->project_type == '1') {
                                    echo 'Submission Date';
                                } else {
                                    echo 'Start Date';
                                }
                                ?> <span class="pull-right">:</span></div><div class="col-md-10"><strong><?php echo date_formate_short($program[0]->training_start_date); ?></strong></div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">Duration <span class="pull-right">:</span></div><div class="col-md-10"><?php echo $program[0]->training_duration; ?> Days <?php if ($program[0]->half_day == '1') { ?><strong style="color:#ED1212;">(Half Day Program)</strong><?php } ?> </div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">Skill Sets <span class="pull-right">:</span></div><div class="col-md-10"><?php echo $program[0]->trainer_skillsets; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">Participants <span class="pull-right">:</span></div><div class="col-md-10"><?php echo $program[0]->no_of_participants; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">Objectives <span class="pull-right">:</span></div><div class="col-md-10"><?php echo $program[0]->objective_of_training; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">Props Required <span class="pull-right">:</span></div><div class="col-md-10"><?php
                                if ($program[0]->props == 1) {
                                    echo 'Yes';
                                } else {
                                    echo 'No';
                                };
                                ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">Stay Arrangements <span class="pull-right">:</span></div><div class="col-md-10"><?php echo $program[0]->stay_arrangement; ?>
                                <?php if (($program[0]->stay_arrangement == "Wagons") || ($program[0]->stay_arrangement == "Trainer")) { ?>
                                    ( Stay Billing Option: 
                                    <?php
                                    if ($program[0]->claim_stay_cost == "Yes")
                                        echo " Claim With Client ";
                                    else
                                        echo "Wagons to Absorb";
                                    ?> )
                                <?php } ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">Travel Arrangements <span class="pull-right">:</span></div><div class="col-md-10"><?php echo $program[0]->travel_arrangement; ?>
                                <?php if (($program[0]->travel_arrangement == "Wagons") || ($program[0]->travel_arrangement == "Trainer")) { ?>
                                    ( Travel Billing Option: 
                                    <?php
                                    if ($program[0]->claim_travel_cost == "Yes")
                                        echo " Claim With Client ";
                                    else
                                        echo "Wagons to Absorb";
                                    ?> )
                                <?php } ?>
                            </div>
                            <div class="clearfix"></div>
                            <?php if (!empty($program[0]->venue)) { ?>
                                <div class="col-md-2">Venue  <span class="pull-right">:</span></div><div class="col-md-10"><?php echo $program[0]->venue; ?></div>
                                <div class="clearfix"></div>
                                <div class="col-md-2">SPOC <span class="pull-right">:</span></div><div class="col-md-10"><?php echo $program[0]->spoc; ?></div>
                                <div class="clearfix"></div>
                            <?php } ?>
                            <div class="col-md-2">Notification States <span class="pull-right">:</span></div><div class="col-md-10">
                                <?php
                                //echo $program[0]->state.', ';
                                $ns = $this->projectmanager_model->get_notification_state($program[0]->project_id);
                                if (!empty($ns)) {
                                    $n_states = "";
                                    foreach ($ns as $ns_data) {
                                        $n_states = $ns_data->state . ',';
                                    }
                                    echo rtrim($n_states, ",");
                                }
                                ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-2">Client Program Charges Amount<span class="pull-right">:</span></div><div class="col-md-10">Rs. <?php echo $program[0]->client_charges; ?> / Day</div>
                            <div class="col-md-2">Trainer Fee Range <span class="pull-right">:</span></div><div class="col-md-10">Rs. <?php echo $program[0]->trainer_fee_range; ?> / Day</div>
                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-graduation-cap"></i> Engaged Trainer </h2>
                            <?php
                            //if ($program[0]->trainer_engage_flag == 0) {
                            ?>
                            <a href="#" class="btn btn-sm btn-info pull-right engage-trainer-btn" pid="<?php echo $program[0]->project_id; ?>" style="margin-top:-24px;"><i class="fa fa-plus"></i> Engage Trainers</a>
                            <?php //}  ?>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 'TTT') {
                                ?>
                                <div class="alert alert-success col-md-6 col-md-offset-3">
                                    TTT Schedule Updated
                                </div>
                                <?php
                            }
                            ?>

                            <?php
                            $tn = $this->projectmanager_model->get_program_trainers($program[0]->project_id);
                            if (!empty($tn)) {
                                $ttt = $this->projectmanager_model->get_ttt($tn[0]->trainer_id, $program[0]->project_id);
                                ?>
                                <table class="table table-bordered">
                                    <tr>
                                        <td style="width:180px;"><strong>TTT Schedule</strong></td>
                                        <?php
                                        if (!empty($ttt)) {
                                            ?>
                                            <td>
                                                <?php
                                                $tstatus = 0;
                                                foreach ($ttt as $tt_data) {
                                                    if ($tt_data->ttt_status == '1') {
                                                        $tstatus = 1;
                                                    }
                                                }
                                                if ($tstatus == 1) {
                                                    foreach ($ttt as $tt_data) {
                                                        if ($tt_data->ttt_status == '1') {
                                                            ?>
                                                            <span style="margin-right:35px"><?php echo date_formate_short($tt_data->ttt_date); ?> &nbsp;&nbsp; <?php echo $tt_data->ttt_time; ?>
                                                                <?php
                                                                if ($tt_data->ttt_status == '1') {
                                                                    $tstatus = 1;
                                                                    ?>
                                                                    <i class="fa fa-check" aria-hidden="true" style="color:green;"></i>
                                                                <?php } ?>
                                                            </span>

                                                            <?php
                                                        }
                                                    }
                                                } else {
                                                    foreach ($ttt as $tt_data) {
                                                        ?>
                                                        <span style="margin-right:35px"><?php echo date_formate_short($tt_data->ttt_date); ?> &nbsp;&nbsp; <?php echo $tt_data->ttt_time; ?>
                                                            <?php
                                                            if ($tt_data->ttt_status == '1') {
                                                                $tstatus = 1;
                                                                ?>
                                                                <i class="fa fa-check" aria-hidden="true" style="color:green;"></i>
                                                            <?php } ?>
                                                        </span>

                                                        <?php
                                                    }
                                                }
                                                ?>
                                            </td>
                                            <?php if ($tstatus == 0) { ?>
                                                <td>
                                                    <a href="#" tid="<?php echo $tn[0]->trainer_id; ?>" class="btn-sm btn-warning ttt-btn">Change</a>
                                                </td>
                                                <?php
                                            }
                                        }
                                        ?>
                                        <td>
                                            <?php if (empty($ttt)) { ?>
                                                <a href="#" tid="<?php echo $tn[0]->trainer_id; ?>" class="btn-sm btn-primary ttt-btn">TTT Schedule</a>
                                            <?php } ?>
                                            <a href="#" mobile="<?php echo $tn[0]->contact_number; ?>" tid="<?php echo $tn[0]->trainer_id; ?>" class="btn-sm btn-warning sms-btn">Send SMS</a>
                                            <?php if ($tn[0]->trainer_invoice_flag == 1) { ?>
                                                <a href="<?php echo base_url(); ?>projectmanager/invoice/<?php echo $tn[0]->trainer_id; ?>/<?php echo $program[0]->project_id; ?>" class="btn-sm btn-primary">Invoice</a>
                                            <?php } ?>
                                        </td>
                                    </tr>
                                </table>

                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Trainer</th>
                                            <th>Date</th>
                                            <th>Payment/Day</th>
                                            <th>Status</th>
                                            <th>Checkin</th>
                                            <th>Checkout</th>
                                            <th>Location</th>
                                            <?php //if ($program[0]->diff > -1) {  ?>
                                            <th></th>
                                            <?php //}  ?>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($tn as $tn_data) {
                                            ?>
                                            <tr>
                                                <td><a href="../trainer_profile/<?php echo $tn_data->user_code; ?>" target="_blank"><?php echo $tn_data->name; ?></a></td>
                                                <td><?php echo date_formate_short($tn_data->program_date . ' 00:00:00'); ?></td>
                                                <td><?php
                                                    if ($tn_data->trainer_invoice_action == 0) {
                                                        ?>
                                                        <input type="text" value="<?php echo $tn_data->amount; ?>" id="amt<?php echo $tn_data->id; ?>" style="padding:3px;"/> <button class="btn-sm btn-success update-btn" pid="<?php echo $tn_data->id; ?>">Update</button>
                                                        <?php
                                                    } else {
                                                        echo $tn_data->amount;
                                                    }
                                                    ?></td>
                                                <td>
                                                    <?php
                                                    if ($tn_data->engage_confirm == 1) {
                                                        echo 'Confirmed';
                                                    } else {
                                                        echo 'Not yet confirmed';
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($tn_data->trainer_checkin_status == '1') {
                                                        echo date_formate($tn_data->trainer_checkin_time);
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($tn_data->trainer_checkout_status == '1') {
                                                        echo date_formate($tn_data->trainer_checkout_time);
                                                        ?>
                                                        <a href="#" assignId="<?php echo $tn_data->assignment_id; ?>" postAsse="<?php echo $tn_data->post_test_trigger_time; ?>" preAsse="<?php echo $tn_data->pre_test_trigger_time; ?>" checkOut="<?php echo $tn_data->trainer_checkout_time; ?>" checkIn="<?php echo $tn_data->trainer_checkin_time; ?>" class="btn-sm btn-warning edit-checkIn">Edit</a>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($tn_data->trainer_checkin_status == '1') {
                                                        ?>
                                                        <a href="https://maps.google.com/?q=<?php echo $tn_data->trainer_lat; ?>,<?php echo $tn_data->trainer_long; ?>" target="_blank"><?php echo $tn_data->trainer_lat; ?>, <?php echo $tn_data->trainer_long; ?></a>
                                                    <?php } ?>
                                                </td>
                                                <?php //if ($program[0]->diff > -1) {  ?>
                                                <td>
                                                    <a href="#" class="btn-sm btn-danger remove-trainer-assign" aid="<?php echo $tn_data->assignment_id; ?>" tid="<?php echo $tn_data->trainer_id; ?>" pid="<?php echo $tn_data->project_id; ?>"><i class="fa fa-trash"></i></a>
                                                </td>
                                                <?php // }  ?>
                                            </tr>
                                        <?php } ?>
                                    </tbody>
                                </table>


                                <?php
                            } else {
                                if ($eng_count != 0) {
                                    echo "<p><strong> $eng_count Trainers Have Shown Interest For The Program</strong>";
                                    echo " >> <a href='" . base_url() . "projectmanager/engagement_requests/" . $program[0]->project_id . "'>View Requests</a></p>";
                                } else
                                    echo "<p><strong>No Trainers Have Submitted Engagement Interest Yet.</strong></p>";
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list-alt"></i> Contents </h2>
                            <!--<a href="#" class="btn btn-sm btn-info pull-right upload-content-btn" cid="<?php // echo $program[0]->client_id;                                            ?>" pid="<?php // echo $program[0]->project_id;                                            ?>" style="margin-top:-24px;"><i class="fa fa-upload"></i> Map Content</a>-->
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 'Content') {
                                ?>
                                <div class="alert alert-success col-md-6 col-md-offset-3">
                                    Content Added Successfully.
                                </div>
                                <?php
                            }
                            ?>
                            <?php
                            $content = $this->projectmanager_model->get_module_content($program[0]->project_id, $program[0]->client_id);
                            //print_r($content);
                            if (!empty($content)) {
                                ?>
                                <h5>Below is list of files required by trainer to conduct the program.</h5>
                                <div class="text-right">
                                    <strong>Status : </strong> <?php
                                    if (!empty($content)) {
                                        if ($content[0]->content_status == '1') {
                                            echo 'Update Closed by CM';
                                        } else {
                                            echo 'Opened by CM for Update';
                                        }
                                    }
                                    ?>
                                    <span style="margin-left:50px; display: inline-block;"><strong>Last Updated :</strong> <?php
                                        if ($content[0]->content_status_date != '0000-00-00 00:00:00') {
                                            echo date_formate($content[0]->content_status_date);
                                        }
                                        ?></span>
                                </div>
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Module</th>
                                            <th>Content Title</th>
                                            <th>Note</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        if (!empty($content)) {
                                            foreach ($content as $cn_data) {
                                                if ($cn_data->for_trainer == '1') {
                                                    ?>
                                                    <tr >
                                                        <td><?php echo $cn_data->module_name; ?></td>
                                                        <td><?php echo $cn_data->content_title; ?></td>
                                                        <td><?php echo $cn_data->content_notes; ?></td>
                                                        <td>
                                                            <a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $cn_data->content_file; ?>" download="<?php echo $cn_data->content_file; ?>" class="btn-sm btn-primary"><i class="fa fa-download"></i></a>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>

                                <div class="text-center">
                                    <?php
                                    if (!empty($content)) {
                                        $check = $this->projectmanager_model->check_content_send($program[0]->project_id);
                                        if (empty($check)) {
                                            if ($program[0]->diff <= 3) {
                                                ?>
                                                <a href="#" class="btn btn-primary confirm-content-btn" pid="<?php echo $program[0]->project_id; ?>">Confirm & Send to Trainer</a>
                                                <?php
                                            }
                                        } else {
                                            ?>
                                            <span style="display:inline-block; padding: 5px 10px; border:solid 1px #ddd; border-radius: 2px;" class="btn-sm btn-warning">
                                                Content was sent to trainer on : <?php echo $check[0]->trainer_pass_date; ?>
                                            </span>
                                            <span style="margin-left: 50px; display:inline-block; padding: 5px 10px; border:solid 1px #ddd; border-radius: 2px;" class="btn-sm btn-danger">
                                                Content access password  : <?php echo $check[0]->trainer_content_pass; ?>
                                            </span>
                                            <?php
                                            if ($check[0]->content_download_date != '0000-00-00 00:00:00') {
                                                ?>
                                                <span style="margin-left: 50px; display:inline-block; padding: 5px 10px; border:solid 1px #ddd; border-radius: 2px;">
                                                    Trainer Download Date  : <?php echo date_formate($check[0]->content_download_date); ?>
                                                </span>
                                                <?php
                                            }
                                        }
                                    }
                                    ?>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>



                <?php
                if ($program[0]->props == 1) {
                    $props = $this->projectmanager_model->get_project_props($program[0]->project_id);
                    $pending = $this->projectmanager_model->get_pending_props($program[0]->project_id);
                    ?>

                    <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-star"></i> Props </h2>

                                <?php if ((!empty($props)) && ($pending[0]->cnt > 0)) { ?>
                                    <a href="#" pid="<?php echo $program[0]->project_id; ?>" class="btn btn-sm btn-info pull-right props-approve-all" style="margin-top:-24px;"> Approve All</a>
                                <?php } ?>
                            </div>
                            <div class="panel-body">
                                <?php
                                if ($msg == 'Props') {
                                    ?>
                                    <div class="alert alert-success col-md-6 col-md-offset-3">
                                        Props Updated Successfully.
                                    </div>
                                    <?php
                                }

                                if (!empty($props)) {
                                    ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Quantity</th>
                                                <th>Uploaded By</th>
                                                <th>Requested Date</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($props as $p_data) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $p_data->prop_name; ?></td>
                                                    <td><?php echo $p_data->quantity; ?></td>

                                                    <td><?php echo $p_data->name; ?> - <?php echo $p_data->user_type; ?></td>
                                                    <td><?php echo date_formate_short($p_data->requested_date); ?></td>
                                                    <td><?php
                                                        if ($p_data->delivery_status == '0') {
                                                            echo 'Approval Pending';
                                                        }
                                                        if ($p_data->delivery_status == '2') {
                                                            echo 'Shipped';
                                                        }
                                                        if ($p_data->delivery_status == '1') {
                                                            echo 'Delivered';
                                                        }
                                                        if ($p_data->delivery_status == '3') {
                                                            echo 'Approved';
                                                        }
                                                        ?></td>
                                                    <td>
                                                        <?php
                                                        if ($p_data->delivery_status == '0') {
                                                            ?>
                                                            <a href="#" class="btn btn-success btn-sm prop-status" status="3" sid="<?php echo $p_data->id; ?>">Approve</a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>

                                                <?php
                                            }
                                            $cr = $this->projectmanager_model->get_courier($program[0]->project_id);
                                            ?>
                                            <tr><td colspan="6"><strong>Notes:</strong><?php echo $p_data->comments; ?></td></tr>
                                            <?php if (!empty($cr)) { ?>
                                                <tr style="background:#ddd;">

                                                    <td>
                                                        Courier Name 
                                                    </td>
                                                    <td>
                                                        Courier Ref # 
                                                    </td>
                                                    <td>
                                                        Courier Expenses 
                                                    </td>
                                                    <td>
                                                    </td>
                                                    <td>
                                                    </td>
                                                </tr>
                                                <?php
                                                foreach ($cr as $crData) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $crData->cr_type; ?></td>
                                                        <td><?php echo $crData->cr_cname; ?></td>
                                                        <td><?php echo $crData->cr_cid; ?></td>
                                                        <td><?php echo $crData->cr_amt; ?></td>
                                                        <td></td>
                                                        <td></td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>

                    <?php
                }
                if ($program[0]->stay_arrangement == 'Wagons' || $program[0]->stay_arrangement == 'Client') {
                    ?>
                    <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-bed"></i> Stay Arrangements </h2>
                            </div>
                            <div class="panel-body">
                                <?php
                                if ($msg == 'Stay') {
                                    ?>
                                    <div class="alert alert-success col-md-6 col-md-offset-3">
                                        Stay details Updated Successfully.
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php
                                if ($program[0]->stay_arrangement == "Wagons") {
                                    echo "<p><strong>Stay Limit : </strong> Rs." . $program[0]->stay_limit;
                                    if ($program[0]->claim_stay_cost == "Y")
                                        echo " - Claim Cost with Client";
                                    else
                                        echo " - Wagons to Absorb Cost</p>";
                                }
                                ?>
                                <?php
                                $stay = $this->projectmanager_model->get_stay_details($program[0]->project_id);
                                if (!empty($stay)) {
                                    ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Trainer</th>
                                                <th>Hotel</th>
                                                <th>Check in & Out</th>
                                                <th>Notes</th>

                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($stay as $st_data) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $st_data->name; ?></td>
                                                    <td><?php echo $st_data->hotel_name; ?></td>
                                                    <td><?php echo date_formate($st_data->checkin_time); ?> to <?php echo date_formate($st_data->checkout_time); ?></td>
                                                    <td><?php echo $st_data->comments; ?></td>

                                                    <td>
                                                        <?php
                                                        if ($st_data->request_status == '0') {
                                                            echo 'Approval Pending';
                                                        } if ($st_data->request_status == '2') {
                                                            echo 'Approved';
                                                        } if ($st_data->request_status == '1') {
                                                            echo 'Booked';
                                                            // echo "<br/>Amount: Rs.".$st_data->amount."<br/>"; 
                                                            if ($st_data->booking_screenshot != '') {
                                                                ?>
                                                                <br/>
                                                                <a href="<?php echo base_url(); ?>assets/upload/stay/<?php echo $st_data->booking_screenshot; ?>" target="_blank">Ticket/Voucher</a>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($st_data->direct_book == '0') {
                                                            ?>
                                                            <i class="fa fa-hand-pointer-o" aria-hidden="true" style="font-size:18px; font-weight: bold;"></i>
                                                            <?php
                                                        }
                                                        if ($st_data->direct_book == '1') {
                                                            ?>
                                                            <i class="fa fa-check-square-o" aria-hidden="true" style="font-size:18px; font-weight: bold;"></i>
                                                            <?php
                                                        }
                                                        if ($st_data->request_status == '0') {
                                                            ?>
                                                            <a href="#" class="btn btn-sm btn-primary stay-status" status='2' sid="<?php echo $st_data->id; ?>" pid="<?php echo $program[0]->project_id; ?>" >Approve</a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                <?php } if ($program[0]->travel_arrangement == 'Wagons' || $program[0]->travel_arrangement == 'Client') { ?>
                    <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-cab"></i> Travel Arrangements </h2>
                            </div>
                            <div class="panel-body">
                                <?php
                                if ($msg == 'Travel') {
                                    ?>
                                    <div class="alert alert-success col-md-6 col-md-offset-3">
                                        Travel details Updated Successfully.
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php
                                if ($program[0]->travel_arrangement == "Wagons") {
                                    echo "<p><strong>Travel Cost Limit : </strong> Rs." . $program[0]->travel_limit;
                                    if ($program[0]->claim_travel_cost == "Y")
                                        echo " - Claim Cost with Client";
                                    else
                                        echo " - Wagons to Absorb Cost</p>";
                                }
                                ?>
                                <?php
                                $travel = $this->projectmanager_model->get_travel_details($program[0]->project_id);
                                if (!empty($travel)) {
                                    ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Trainer</th>
                                                <th>Date</th>
                                                <th>From-To</th>

                                                <th>Mode</th>
                                                <th>Notes</th>

                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($travel as $tv_data) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $tv_data->name; ?></td>
                                                    <td><?php echo date_formate_short($tv_data->date); ?></td>
                                                    <td><?php echo $tv_data->from_loc; ?> To <?php echo $tv_data->to_loc; ?></td>

                                                    <td><?php echo $tv_data->mode; ?></td>
                                                    <td><?php echo $tv_data->comments; ?></td>

                                                    <td>
                                                        <?php
                                                        if ($tv_data->request_status == '0') {
                                                            echo 'Approval Pending';
                                                        } if ($tv_data->request_status == '2') {
                                                            echo 'Approved';
                                                        } if ($tv_data->request_status == '1') {
                                                            echo 'Booked';
                                                            // echo "<br/>Amount: Rs.".$tv_data->amount."<br/>"; 
                                                            if ($tv_data->screenshot != '') {
                                                                ?>
                                                                <br/>
                                                                <a href="<?php echo base_url(); ?>assets/upload/travel/<?php echo $tv_data->screenshot; ?>" target="_blank">Ticket</a>

                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($tv_data->direct_book == '0') {
                                                            ?>
                                                            <i class="fa fa-hand-pointer-o" aria-hidden="true" style="font-size:18px; font-weight: bold;"></i>
                                                            <?php
                                                        }
                                                        if ($tv_data->direct_book == '1') {
                                                            ?>
                                                            <i class="fa fa-check-square-o" aria-hidden="true" style="font-size:18px; font-weight: bold;"></i>
                                                            <?php
                                                        }
                                                        if ($tv_data->request_status == '0') {
                                                            ?>
                                                            <a href="#" class="btn btn-sm btn-primary travel-status" status='2' rid="<?php echo $tv_data->id; ?>" pid="<?php echo $program[0]->project_id; ?>" >Approve</a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list-alt"></i> Forms </h2>
                            <!--<a href="#" class="btn btn-sm btn-info pull-right upload-form-btn" style="margin-top:-24px;"><i class="fa fa-upload"></i> Upload</a>-->
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 'Form') {
                                ?>
                                <div class="alert alert-success col-md-6 col-md-offset-3">
                                    Form Added Successfully.
                                </div>
                                <?php
                            }
                            ?>
                            <?php
                            $form = $this->projectmanager_model->get_project_form($program[0]->project_id);
                            if (!empty($form)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Form Type</th>
                                            <th>Uploaded By</th>
                                            <th>Program Date</th>
                                            <th>Uploaded Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($form as $fm_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $fm_data->form_type; ?></td>

                                                <td><?php echo $fm_data->name; ?> - <?php echo ucfirst($fm_data->user_type); ?></td>
                                                <td><?php echo date_formate_short($fm_data->program_date); ?></td>
                                                <td><?php echo date_formate_short($fm_data->upload_date); ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>assets/upload/form/<?php echo $fm_data->form_file_name; ?>" class="btn btn-primary"><i class="fa fa-download"></i> View</a>
                                                    <?php
                                                    if ($fm_data->upload_by == $this->session->userdata('pm_code')) {
                                                        ?>
                                                        <a href="#" class="btn btn-sm btn-danger remove-form" rid="<?php echo $fm_data->id; ?>"><i class="fa fa-trash"></i></a>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-image"></i> Images </h2>

                        </div>
                        <div class="panel-body">

                            <?php
                            $img = $this->projectmanager_model->get_project_images($program[0]->project_id);
                            if (!empty($img)) {
                                foreach ($img as $im_data) {
                                    ?>

                                    <div class="col-md-3" style="overflow:hidden;">
                                        <a href="<?php echo base_url(); ?>App/assets/upload/<?php echo $im_data->ti_img; ?>" style="display:inline-block; padding: 8px; border:solid 1px #ddd;"><img src="<?php echo base_url(); ?>App/assets/upload/<?php echo $im_data->ti_img; ?>" height="130px"/></a>
                                        <div class="text-center" style="position:absolute; top: 5px; right: 0px; width: 100px; height: 50px;">
                                            <a href="#" class="btn-sm btn-danger remove-img" rid="<?php echo $im_data->ti_id; ?>" iname="<?php echo $im_data->ti_img; ?>"><i class="fa fa-trash"></i></a>
                                        </div>

                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>


            </div>
        </div>






        <div class="modal fade" id="props_add_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Props Needed</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="props-add-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="props_user" value="<?php echo $this->session->userdata('pm_code'); ?>"/>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Props Name <sup>*</sup></span>
                                    <input type="text" name="pname"  class="form-control">
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Quantity <sup>*</sup></span>
                                    <input type="text" name="qnt"  class="form-control">
                                </div>
                            </div>


                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="upload_content_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Content mapping</h4>
                    </div>

                    <div class="modal-body load-content">

                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="upload_form_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Upload Forms</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="add-forms-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="form_user" value="<?php echo $this->session->userdata('pm_code'); ?>"/>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Form Type <sup>*</sup></span>
                                    <input type="text" name="ftype" placeholder="Form Type" class="form-control">
                                </div>
                            </div>
                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Date <sup>*</sup></span>
                                    <input type="date" name="fdate"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Upload Form <sup>*</sup></span>
                                    <input type="file" name="content" class="form-control">
                                </div>
                            </div>

                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <div class="modal fade" id="ttt_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">TTT Schedule Management</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="add-ttt-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="ttt_user" value="<?php echo $this->session->userdata('pm_code'); ?>"/>
                            <input type="hidden" name="tid" id="tid" value=""/>

                            <div class="form-group  col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Date <sup>*</sup></span>
                                    <input type="date" name="tdate1"  class="form-control">
                                </div>
                            </div>

                            <div class="form-group  col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Time <sup>*</sup></span>
                                    <input type="time" name="ttime1"  class="form-control">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group  col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Date <sup>*</sup></span>
                                    <input type="date" name="tdate2"  class="form-control">
                                </div>
                            </div>

                            <div class="form-group  col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Time <sup>*</sup></span>
                                    <input type="time" name="ttime2"  class="form-control">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group  col-md-8">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Date <sup>*</sup></span>
                                    <input type="date" name="tdate3"  class="form-control">
                                </div>
                            </div>

                            <div class="form-group  col-md-4">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Time <sup>*</sup></span>
                                    <input type="time" name="ttime3"  class="form-control">
                                </div>
                            </div>

                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="venue_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Update Venue Details</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="venue-add-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="venue_user" value="<?php echo $this->session->userdata('pm_code'); ?>"/>
                            <div class="form-group col-md-12">
                                <div>
                                    <span >Venue <sup>*</sup></span>
                                    <textarea name="venue" class="form-control"><?php echo $program[0]->venue; ?></textarea>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">SPOC <sup>*</sup></span>
                                    <input type="text" name="spoc" value="<?php echo $program[0]->spoc; ?>" class="form-control">
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Contact Person(Billing) <sup></sup></span>
                                    <input type="text" name="cperson" value="<?php echo $program[0]->contact_person; ?>" class="form-control">
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Contact Email(Billing) <sup></sup></span>
                                    <input type="text" name="cemail" value="<?php echo $program[0]->contact_email; ?>" class="form-control">
                                </div>
                            </div>


                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Update</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="approve_form" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Assign Trainer</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="assign-trainer-form">

                        <div class="modal-body">
                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <input type="text" id="searchKey" name="searchKey" class="form-control" placeholder="Search trainer">
                                    <span class="input-group-btn">
                                        <button class="btn btn-default searchT-btn" type="button">Search</button>
                                    </span>
                                </div><!-- /input-group -->
                            </div>
                            <div class="trainerResult">

                            </div>
                            <div class="afterSearch" style="display:none;">
                                <input type="hidden" id="eProject" name="eProject" value="<?php echo $program[0]->project_id; ?>"/>
                                <input type="hidden" id="eUser" name="eUser" value=""/>

                                <div class="form-group  col-md-12">
                                    <div class="input-group" style="position:relative;">
                                        <span class="input-group-addon" id="sizing-addon1"> From Date <sup>*</sup></span>
                                        <input type="text" id="efrom" name="from" readonly="readonly" class="form-control" style="position:relative;">
                                    </div>
                                </div>
                                <div class="form-group  col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> To Date <sup>*</sup></span>
                                        <input type="text" id="eto" name="to" readonly="readonly"  class="form-control">
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Payment / Day <sup>*</sup></span>
                                        <input type="text" id="eamt" name="amt"  class="form-control">
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1">Brief About Trainer<sup>*</sup></span>
                                        <textarea id="etrainer_brief" name="trainer_brief"  class="form-control"></textarea>
                                    </div>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="input-group">
                                        <span id="sizing-addon1"><strong> TTT Schedule Required ?</strong></span>&nbsp;
                                        <input id="ettt_required" name="ttt_required"  type="checkbox" value="Y"> Yes
                                    </div>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info afterSearch" style="display:none;"><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="smsWraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Send SMS to Trainer</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="venue-add-form">
                        <input type="hidden" name="smobile" id="smobile" />
                        <input type="hidden" name="stid" id="stid" />
                        <div class="modal-body">
                            <div class="form-group col-md-12">
                                <div>
                                    <span >SMS Text <sup>*</sup></span>
                                    <textarea name="smstext" id="smstext" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="button" class="btn btn-info submit-sms" ><i class="fa fa-upload"></i> Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="checkUpdate" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Update Checkin/Checkout</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="checkin-update">
                        <input type="hidden" name="assignId" id="assignId" />
                        <div class="modal-body">
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> CheckIn <sup>*</sup></span>
                                    <input type="datetime" id="checkIn" name="checkIn"  class="form-control" required/>
                                </div>
                                <div class="input-group" style="margin-top:10px;">
                                    <span class="input-group-addon" id="sizing-addon1"> CheckOut <sup>*</sup></span>
                                    <input type="datetime" id="checkOut" name="checkOut"  class="form-control" required/>
                                </div>
                                <div class="input-group" style="margin-top:10px;">
                                    <span class="input-group-addon" id="sizing-addon1"> Pre Assessment <sup>*</sup></span>
                                    <input type="datetime" id="preAsse" name="preAsse"  class="form-control" required/>
                                </div>
                                <div class="input-group" style="margin-top:10px;">
                                    <span class="input-group-addon" id="sizing-addon1"> Post Assessment <sup>*</sup></span>
                                    <input type="datetime" id="postAsse" name="postAsse"  class="form-control" required/>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info"><i class="fa fa-upload"></i> Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <?php
        $date = strtotime("+" . $program[0]->training_duration . " days", strtotime($program[0]->training_start_date));
        $end_date = date("Y-m-d", $date);
        $startDate = new DateTime($program[0]->training_start_date);
        $endDate = new DateTime($end_date . ' 00:00:00');

        $tmpstartDate = $program[0]->training_start_date;
        $tmpendDate = new DateTime($end_date);


        //print_r($tmpendDate);

        $start = 0;
        if ($program[0]->excl_sat == 'Y') {

            while ($startDate <= $endDate) {
                if ($startDate->format('w') == 6) {
                    if ($start == 0) {
                        $tmpstartDate = date('Y-m-d', strtotime($startDate->format('Y-m-d') . ' + 2 days'));
                    }
                    $tmpendDate = date('Y-m-d', strtotime($tmpendDate->format('Y-m-d') . ' + 2 days'));
                    $tmpendDate = new DateTime($tmpendDate);
                }

                $start = 1;
                $startDate->modify('+1 day');
            }
        } else {
            while ($startDate <= $endDate) {
                if ($startDate->format('w') == 0) {
                    if ($start == 0) {
                        $tmpstartDate = date('Y-m-d', strtotime($startDate->format('Y-m-d') . ' + 1 days'));
                    }
                    $tmpendDate = date('Y-m-d', strtotime($tmpendDate->format('Y-m-d') . ' + 1 days'));
                    $tmpendDate = new DateTime($tmpendDate);
                }
                $start = 1;
                $startDate->modify('+1 day');
            }
        }

        $tmpendDate = date('Y-m-d', strtotime($tmpendDate->format('Y-m-d') . ' + 0 days'));
        echo $tmpendDate;
        $jStart = explode(' ', $tmpstartDate);
        $jEnd = explode('-', $tmpendDate);
        $jStart = explode('-', $jStart[0]);
        ?>

        <div class="modal fade" id="request_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Edit Request</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="edit-request-form">

                        <div class="modal-body">
                            <input type="hidden" name="e_pid" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="e_ptitle" value="<?php echo $program[0]->project_title; ?>"/>
                            <input type="hidden" name="e_pclient" value="<?php echo $program[0]->client_name; ?>"/>
                            <input type="hidden" name="e_pdate" value="<?php echo $program[0]->training_start_date; ?>"/>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Send Request To  <sup>*</sup></span>
                                    <select  name="e_email"  class="form-control">
                                        <option value="uday@wagonslearning.com">Uday Shetty</option>
                                        <option value="raviraj@wagonslearning.com">Raviraj Poojary</option>
                                        <option value="sourabh.shah@wagonslearning.com">Sourabh Shah</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Edit Reason </span>
                                    <input type="text" name="e_reason"  class="form-control" placeholder="Edit Reason">
                                </div>
                            </div>

                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="upload_img" class="btn btn-info" >Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script>
            $(function () {

                $("#efrom").datepicker({
                    dateFormat: "yy-mm-dd",
                    changeYear: true,
                    changeMonth: true,
                    minDate: new Date(<?php echo $jStart[0]; ?>, <?php echo $jStart[1] - 1; ?>, <?php echo $jStart[2]; ?>),
                    maxDate: new Date(<?php echo $jEnd[0]; ?>, <?php echo $jEnd[1] - 1; ?>, <?php echo $jEnd[2]; ?>),
                });

                $("#eto").datepicker({
                    dateFormat: "yy-mm-dd",
                    changeYear: true,
                    changeMonth: true,
                    minDate: new Date(<?php echo $jStart[0]; ?>, <?php echo $jStart[1] - 1; ?>, <?php echo $jStart[2]; ?>),
                    maxDate: new Date(<?php echo $jEnd[0]; ?>, <?php echo $jEnd[1] - 1; ?>, <?php echo $jEnd[2]; ?>),
                });

            });
        </script>
        <script type="text/javascript">
            $(document).ready(function () {
                
                $('.edit-checkIn').click(function(e){
                    e.preventDefault();
                    $('#checkUpdate').modal('show');
                    $('#assignId').val($(this).attr('assignId'));
                    $('#checkIn').val($(this).attr('checkIn'));
                    $('#checkOut').val($(this).attr('checkOut'));
                    $('#preAsse').val($(this).attr('preAsse'));
                    $('#postAsse').val($(this).attr('postAsse'));
                });

                $('.edit-request').click(function (e) {
                    e.preventDefault();
                    $('#request_wraper').modal('show');
                });

                $("#edit-request-form").validate({
                    rules: {
                        e_reason: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $('.sms-btn').click(function (e) {
                    e.preventDefault();
                    var mobile = $(this).attr('mobile');
                    var tid = $(this).attr('tid');
                    $('#smsWraper').modal('show');
                    $('#smobile').val(mobile);
                    $('#stid').val(tid);
                });

                $('.upload-form-btn').click(function (e) {
                    e.preventDefault();
                    $('#upload_form_wraper').modal('show');
                });

                $('.props-add-btn').click(function (e) {
                    e.preventDefault();
                    $('#props_add_wraper').modal('show');
                });

                $('.engage-trainer-btn').click(function (e) {
                    e.preventDefault();
                    $('#approve_form').modal('show');
                });

                $('.submit-sms').click(function (e) {
                    var mobile = $('#smobile').val();
                    var text = $('#smstext').val();
                    var tid = $('#stid').val();
                    if (text == '') {
                        alert('Enter text message');
                        return false;
                    }
                    $('.page_spin').show();
                    var dataString = "mobile=" + mobile + "&text=" + text + "&tid=" + tid + "&page=send_trainer_sms";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            $('#smstext').val('');
                            $('#smsWraper').modal('hide');
                            alert('SMS sent successfuly');
                        }, //success fun end
                    });//ajax end
                });

                $('.searchT-btn').click(function (e) {
                    e.preventDefault();
                    $('.afterSearch').hide();
                    var searchKey = $("#searchKey").val();
                    $('.page_spin').show();
                    var dataString = "searchkey=" + searchKey + "&page=search_trainer";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            $('.trainerResult').html(data);
                        }, //success fun end
                    });//ajax end
                });

                $(document).on('click', '.trainer-item', function (e) {
                    e.preventDefault();
                    var uid = $(this).attr('uid');
                    $('#eUser').val(uid);
                    $(this).siblings('.list-group-item').hide();
                    $('.afterSearch').show();
                });

                $("#assign-trainer-form").validate({
                    rules: {
                        from: "required",
                        to: "required",
                        amt: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        assign_trainer();
                    }
                });

                function assign_trainer()
                {
                    $('#approve_form').modal('hide');
                    var from = $('#efrom').val();
                    var to = $('#eto').val();
                    var amt = $('#eamt').val();
                    var pid = $('#eProject').val();
                    var uid = $('#eUser').val();
                    if (uid == '') {
                        return false;
                    }
                    var rid = '';
                    var tr = $('#ettt_required').val();
                    var trainer_brief = $('#etrainer_brief').val();
                    var trainer_brief = trainer_brief.replace(/&/g, " and ");
                    $('.page_spin').show();
                    var dataString = "assign='1'&rid=" + rid + "&pid=" + pid + "&uid=" + uid + "&from=" + from + "&to=" + to + "&amt=" + amt + "&tr=" + tr + "&trainer_brief=" + trainer_brief + "&page=accept_request";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            alert('Trainer is assigned');
                            window.location.reload();
                            $('.status_' + rid + '').children('.label-primary').remove();
                            $('.status_' + rid + '').siblings('td').children('.accept-btn').remove();
                            $('.status_' + rid + '').siblings('td').children('.reject-btn').remove();
                            $('.status_' + rid + '').append('<span class="label label-success">Admin Approved</span>');
                        }, //success fun end
                    });//ajax end
                }

                $('.upload-content-btn').click(function (e) {
                    e.preventDefault();
                    var cid = $(this).attr('cid');
                    var pid = $(this).attr('pid');
                    $('.page_spin').show();
                    var dataString = "cid=" + cid + "&pid=" + pid + "&page=load_content";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/load_content",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            $('#upload_content_wraper').modal('show');
                            $('.load-content').html(data);
                        }, //success fun end
                    });//ajax end

                });

                $(document).on('click', '.confirm-content-btn', function (e) {
                    e.preventDefault();
                    var pid = $(this).attr('pid');
                    $('.page_spin').show();
                    var dataString = "pid=" + pid + "&page=confirm_content";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            alert('Content is sent to trainer');
                            $('#upload_content_wraper').modal('hide');
                            window.location.href = "<?php echo base_url(); ?>projectmanager/program_details/" + pid;
                        }, //success fun end
                    });//ajax end
                });


                $('.assign-pm-btn').click(function (e) {
                    e.preventDefault();
                    $('#assign-pm-wrap').modal('show');
                    var cid = $(this).attr('cid');
                    $('#cid').val(cid);
                });

                $('.ttt-btn').click(function (e) {
                    e.preventDefault();
                    $('#ttt_wraper').modal('show');
                    var tid = $(this).attr('tid');
                    $('#tid').val(tid);
                });

                $('.venue-btn').click(function (e) {
                    e.preventDefault();
                    $('#venue_wraper').modal('show');
                });

                $("#props-add-form").validate({
                    rules: {
                        pname: "required",
                        qnt: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#props-add-form").validate({
                    rules: {
                        pname: "required",
                        qnt: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#add-ttt-form").validate({
                    rules: {
                        tdate1: "required",
                        ttime1: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#add-forms-form").validate({
                    rules: {
                        content: "required",
                        ftype: "required",
                        fdate: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });



                $('.prop-status').click(function (e) {
                    e.preventDefault();
                    var sid = $(this).attr('sid');
                    var status = $(this).attr('status');
                    var pid = '<?php echo $program[0]->project_id; ?>';
                    $(this).remove();
                    $('.page_spin').show();
                    var dataString = "sid=" + sid + "&pid=" + pid + "&status=" + status + "&page=update_props_status";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            alert('Status updated');
                        }, //success fun end
                    });//ajax end
                });

                $('.props-approve-all').click(function (e) {
                    e.preventDefault();
                    var f = confirm("Are you sure want to approve all props ?");
                    var pid = $(this).attr('pid');
                    if (f == true) {
                        $('.page_spin').show();
                        var dataString = "pid=" + pid + "&page=props_approve_all";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.stay-status').click(function (e) {
                    e.preventDefault();
                    var sid = $(this).attr('sid');
                    var pid = $(this).attr('pid');
                    var status = $(this).attr('status');
                    $(this).remove();
                    $('.page_spin').show();
                    var dataString = "pid=" + pid + "&sid=" + sid + "&status=" + status + "&page=update_stay_status";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            alert('Status updated');
                        }, //success fun end
                    });//ajax end
                });

                $('.travel-status').click(function (e) {
                    e.preventDefault();
                    var sid = $(this).attr('rid');
                    var pid = $(this).attr('pid');
                    var status = $(this).attr('status');
                    $(this).remove();
                    $('.page_spin').show();
                    var dataString = "pid=" + pid + "&sid=" + sid + "&status=" + status + "&page=update_travel_status";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            alert('Status updated');
                        }, //success fun end
                    });//ajax end
                });


                $('.remove-content').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove content ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_content";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.remove-form').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove form ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_form";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.remove-props').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove props ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_props";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.cancelBtn').click(function (e) {
                    e.preventDefault();
                    var pid = $(this).attr('pid');
                    $('.page_spin').show();
                    var dataString = "pid=" + pid + "&page=cancelProgram";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                });

                $('.activeBtn').click(function (e) {
                    e.preventDefault();
                    var pid = $(this).attr('pid');
                    $('.page_spin').show();
                    var dataString = "pid=" + pid + "&page=activeProgram";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                });

                $('.update-btn').click(function (e) {
                    e.preventDefault();
                    var pid = $(this).attr('pid');
                    var amt = $('#amt' + pid + '').val();
                    $('.page_spin').show();
                    var dataString = "pid=" + pid + "&amt=" + amt + "&page=updateTrainerAmt";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                });

                $('.remove-img').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var iname = $(this).attr('iname');
                    var ele = $(this).parent('div').parent('div');
                    $('.page_spin').show();
                    var dataString = "rid=" + rid + "&iname=" + iname + "&page=remove_img";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            alert('Image has been removed');
                            ele.remove();
                        }, //success fun end
                    });//ajax end
                });

                $('.remove-trainer-assign').click(function (e) {
                    e.preventDefault();
                    var aid = $(this).attr('aid');
                    var tid = $(this).attr('tid');
                    var pid = $(this).attr('pid');
                    var f = confirm("Are you sure want to dis engage trainer ?");
                    if (f == true) {
                        $('.page_spin').show();
                        var dataString = "aid=" + aid + "&tid=" + tid + "&pid=" + pid + "&page=remove_trainer_assign";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

            });
        </script>

    </body>
</html>