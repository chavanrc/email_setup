<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>PM | Add Trainer</title>
        <?php include 'css_files.php'; ?>
        <style>
            .starWraper{
                font-size: 23px;
                cursor: pointer;
                display: inline-block;
                background: #fff;
                padding: 5px;
                margin-left: -175px;
                letter-spacing: 3px;
                width: 200px;
            }
            .selectStar{
                color: gold;
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Trainers - Add New</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>projectmanager/trainers" class="btn btn-danger"><i class="fa fa-backward"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <?php
                if ($msg == 1) {
                    ?>
                    <div class="alert alert-success col-md-4 col-md-offset-4">
                        Trainer Added Successfully.
                    </div>

                    <?php
                }
                ?>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-plus"></i> Create Trainer </h2>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == "error") {
                                ?>
                                <div class="alert alert-danger col-md-4 col-md-offset-4">
                                    This email id is already exist.
                                </div>
                                <?php
                            }
                            ?>
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-trainer-form">
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">

                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Name <sup>*</sup></span>
                                        <input type="text" name="pname" class="form-control">
                                    </div>

                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"  id="sizing-addon1"> Date of Birth <sup>*</sup></span>
                                        <input type="date" id="dob" data-date-format="YYYY MM DD" name="dob" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">

                                    <div class="input-group">
                                        <label for="gender">Gender : <sup>*</sup></label>
                                        <label class="radio-inline" style="margin: 0px 20px;"><input type="radio" checked  name="gender" value="Male">Male</label>
                                        <label class="radio-inline" ><input type="radio" name="gender" value="Female">Female</label>
                                    </div>

                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">

                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Country <sup>*</sup></span>
                                        <select class="form-control" name="country">
                                            <option value="India"> India </option>
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">

                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> State <sup>*</sup></span>
                                        <select class="form-control" id="state" name="state">
                                            <option value=""> - Select - </option>
                                            <?php
                                            if (!empty($state)) {
                                                foreach ($state as $st_data) {
                                                    ?>
                                                    <option value="<?php echo $st_data->st_name; ?>"><?php echo $st_data->st_name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">

                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> City <sup>*</sup></span>
                                        <select class="form-control" id="city" name="city">
                                            <option value=""> - Select - </option>
                                        </select>
                                    </div>

                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label for="address1">Address 1 : <sup>*</sup></label>
                                    <textarea class="form-control" name="address1" id="address1"></textarea>
                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <label for="address1">Address 2:</label>
                                    <textarea class="form-control" name="address2" id="address2"></textarea>
                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Email <sup>*</sup></span>
                                        <input type="text" name="email" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Mobile <sup>*</sup></span>
                                        <input type="text" name="mobile" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Landline </span>
                                        <input type="text" name="landline" class="form-control">
                                    </div>
                                </div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Skype ID </span>
                                        <input type="text" name="skype" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <label> Rating :  </label>
                                        <input type="text" name="rate" value="" id="rate"/>
                                        <span class="starWraper">
                                            <i class="fa fa-star-o rateStar" rate="1"></i>
                                            <i class="fa fa-star-o rateStar" rate="2"></i>
                                            <i class="fa fa-star-o rateStar" rate="3"></i>
                                            <i class="fa fa-star-o rateStar" rate="4"></i>
                                            <i class="fa fa-star-o rateStar" rate="5"></i>
                                        </span>

                                    </div>
                                    <textarea class="form-control" name="comments" placeholder="Comments" id="comments"></textarea>
                                </div>

                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">

            $(document).ready(function () {
                
                $('.rateStar').click(function () {
                    var rate = $(this).attr('rate');
                    $('#rate').val(rate);
                    $(this).addClass('selectStar');
                    $(this).addClass('fa-star');
                    $(this).removeClass('fa-star-o');
                    $(this).prevAll('.rateStar').addClass('selectStar');
                    $(this).prevAll('.rateStar').addClass('fa-star');
                    $(this).prevAll('.rateStar').removeClass('fa-star-o');

                    $(this).nextAll('.rateStar').removeClass('selectStar');
                    $(this).nextAll('.rateStar').addClass('fa-star-o');
                    $(this).nextAll('.rateStar').removeClass('fa-star');
                });

                $('#state').change(function () {
                    var state = $(this).val();
                    if (state != '')
                    {
                        var dataString = "state=" + state + "&page=get_city";
                        $.ajax({
                            type: "POST",
                            dataType: "JSON",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('#city option').remove();
                                $('#city').append('<option value=""> - Select - </option>');
                                $('.page_spin').hide();
                                $.each(data, function (i, item) {

                                    $('#city').append('<option value="' + item.city + '">' + item.city + '</option>');
                                });
                            }, //success fun end
                        }); //ajax end 
                    }
                });

                $("#add-trainer-form").validate({
                    rules: {
                        pname: "required",
                        dob: "required",
                        address1: "required",
                        country: "required",
                        state: "required",
                        city: "required",
                        rate:"required",
                        email: {
                            required: true,
                            email: true
                        },
                        mobile: {
                            required: true,
                            number: true
                        }
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

            });

        </script>

    </body>
</html>