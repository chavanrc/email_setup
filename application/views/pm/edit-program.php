<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Project Manager | Edit Program</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/datepicker/jquery.datetimepicker.css">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Program - Update</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>projectmanager/program_details/<?php echo $program[0]->project_id; ?>" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <?php
                if ($msg > 0) {
                    ?>
                    <div class="alert alert-success col-md-4 col-md-offset-4">
                        Program updated Successfully.
                    </div>


                    <?php
                }
                //print_r($program[0]);
                ?>



                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-plus"></i> Update Program </h2>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($program[0]->client_invoice_raised == 'N') {
                            ?>
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-program-form">
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Program Status <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="pstatus">
                                            <option value=""> - Select -</option>
                                            <option value="1" <?php if($program[0]->is_active=='1'){ echo 'selected'; } ?> >Active</option>
                                            <option value="0" <?php if($program[0]->is_active=='0'){ echo 'selected'; } ?>>Cancel</option>
											<option value="2" <?php if($program[0]->is_active=='2'){ echo 'selected'; } ?>>Disable</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Client <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12"> <?php echo $program[0]->client_name; ?>
									<input type="hidden" name="client" value="<?php echo $program[0]->client_id; ?>">
									</div>
									
                                        
                                    
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Participant Type <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="participants_type">
                                            <option value=""> - Select -</option>
                                            <option value="Banking" <?php
                                            if ($program[0]->participants_type == 'Banking') {
                                                echo 'selected';
                                            }
                                            ?>> Banking </option>
                                            <option value="Corporate" <?php
                                            if ($program[0]->participants_type == 'Corporate') {
                                                echo 'selected';
                                            }
                                            ?>> Corporate </option>
                                            <option value="NGO" <?php
                                            if ($program[0]->participants_type == 'NGO') {
                                                echo 'selected';
                                            }
                                            ?>> NGO </option>
                                            <option value="Govt" <?php
                                            if ($program[0]->participants_type == 'Govt') {
                                                echo 'selected';
                                            }
                                            ?>> Government </option>
                                            <option value="Academics" <?php
                                            if ($program[0]->participants_type == 'Academics') {
                                                echo 'selected';
                                            }
                                            ?>> Academics </option>	
											<option value="Pharma" <?php
                                            if ($program[0]->participants_type == 'Pharma') {
                                                echo 'selected';
                                            }
                                            ?>> Pharma </option>	
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Client / Participant Group Name <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       <input type="text" name="client_name" class="form-control col-md-7 col-xs-12" value="<?php echo $program[0]->part_name;?>">
                                    </div>
                                </div>
                                                                <div class="clearfix"></div> 
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Program Zone <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="zone">
                                            <option value="" <?php
                                            if ($program[0]->zone == '') {
                                                echo 'selected';
                                            }
                                            ?>> - Select -</option>
                                            <option value="North" <?php
                                            if ($program[0]->zone == 'North') {
                                                echo 'selected';
                                            }
                                            ?>> North </option>
                                            <option value="East" <?php
                                            if ($program[0]->zone == 'East') {
                                                echo 'selected';
                                            }
                                            ?>> East </option>
                                            <option value="West" <?php
                                            if ($program[0]->zone == 'West') {
                                                echo 'selected';
                                            }
                                            ?>> West </option>
                                            <option value="South" <?php
                                            if ($program[0]->zone == 'South') {
                                                echo 'selected';
                                            }
                                            ?>> South </option>

                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Program Title <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="hidden" value="<?php echo $program[0]->project_id; ?>" name="pid">
                                        <input type="text" name="pname" value="<?php echo $program[0]->project_title; ?>" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">State<span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="program-state">
                                            <option value=""> - Select -</option>
                                            <?php
                                            if (!empty($state)) {
                                                foreach ($state as $st_data) {
                                                    ?>
                                                    <option value="<?php echo $st_data->st_name; ?>" <?php if ($st_data->st_name == $program[0]->program_state) { ?> selected <?php } ?>><?php echo $st_data->st_name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Location <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="location" value="<?php echo $program[0]->location_of_training; ?>" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Objective <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea name="objective" class="form-control"><?php echo $program[0]->objective_of_training; ?></textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Start Date <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="date" id="sdate" name="sdate" value="<?php
                                        $sdate = explode(' ', $program[0]->training_start_date);
                                        echo $sdate[0];
                                        ?>"  placeholder="yy-mm-dd" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>



                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Duration <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="duration" value="<?php echo $program[0]->training_duration; ?>" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12"> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label><input type="checkbox" <?php if($program[0]->half_day=='1'){ echo 'checked'; } ?> name="halfday" value='1'>Half Day Program</label>
                                        </div>
                                    </div>
                                <div class="clearfix"></div>
                                
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12"><span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="checkbox"  name="excl_sat" value='Y' <?php if ($program[0]->excl_sat == "Y") echo "checked"; ?>>Exclude Saturday
                                        <input type="checkbox"  name="excl_sun" value='Y' <?php if ($program[0]->excl_sun == "Y") echo "checked"; ?> style="margin-left: 20px;">Exclude Sunday
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Trainer Skill Sets <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <textarea name="skill_set" class="form-control"><?php echo $program[0]->trainer_skillsets; ?></textarea>
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">No. of Participants <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="participants" value="<?php echo $program[0]->no_of_participants; ?>" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Participants Level <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="participant_level">
                                            <option value=""> - Select -</option>
                                            <option value="Beginners" <?php
                                            if ($program[0]->participant_level == 'Beginners') {
                                                echo 'selected';
                                            }
                                            ?>> Beginners </option>
                                            <option value="Juniors" <?php
                                            if ($program[0]->participant_level == 'Juniors') {
                                                echo 'selected';
                                            }
                                            ?>> Juniors </option>
                                            <option value="Intermediate" <?php
                                            if ($program[0]->participant_level == 'Intermediate') {
                                                echo 'selected';
                                            }
                                            ?>> Intermediate </option>
                                            <option value="Advanced" <?php
                                            if ($program[0]->participant_level == 'Advanced') {
                                                echo 'selected';
                                            }
                                            ?>> Advanced </option>
                                            <option value="Highly Experienced" <?php
                                            if ($program[0]->participant_level == 'Highly Experienced') {
                                                echo 'selected';
                                            }
                                            ?>> Highly Experienced </option>
                                            <option value="Above 40 years Staff" <?php
                                            if ($program[0]->participant_level == 'Above 40 years Staff') {
                                                echo 'selected';
                                            }
                                            ?>> Above 40 years Staff </option>
                                            <option value="Unskilled Labor" <?php
                                            if ($program[0]->participant_level == 'Unskilled Labor') {
                                                echo 'selected';
                                            }
                                            ?>> Unskilled Labor </option>
                                        </select>
                                    </div>
                                </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12"> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label><input type="checkbox" <?php if($program[0]->invoice_status=='0'){ echo 'checked'; } ?>  name="invoice_status" value='0'>Trainer Invoice Not Required</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Props Required <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="radio" name="prop" <?php
                                        if ($program[0]->props == '1') {
                                            echo 'checked';
                                        }
                                        ?> value="1"> Yes 
                                        <input type="radio" name="prop" <?php
                                        if ($program[0]->props == '0') {
                                            echo 'checked';
                                        }
                                        ?> value="0" style="margin-left: 25px;"> No 
                                    </div>
                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Stay Arrangement <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="stay" id="stay">
                                            <option value=""> - Select -</option>
                                            <option value="None" <?php
                                            if ($program[0]->stay_arrangement == 'None') {
                                                echo 'selected';
                                            }
                                            ?>> None </option>
                                            <option value="Wagons" <?php
                                            if ($program[0]->stay_arrangement == 'Wagons') {
                                                echo 'selected';
                                            }
                                            ?>> Wagons </option>
                                            <option value="Client" <?php
                                            if ($program[0]->stay_arrangement == 'Client') {
                                                echo 'selected';
                                            }
                                            ?>> Client </option>
                                            <option value="Trainer" <?php
                                            if ($program[0]->stay_arrangement == 'Trainer') {
                                                echo 'selected';
                                            }
                                            ?>> Trainer </option>
                                        </select>
                                    </div>

                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group" id="stay_limit_block" <?php if ($program[0]->stay_arrangement != 'Wagons') { ?>style="display:none;" <?php } ?>>
                                    
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Stay Spend Limit in Rs.<span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="stay_limit" class="form-control col-md-7 col-xs-12" value="<?php echo $program[0]->stay_limit; ?>"><br/>
                                        
                                    </div>
                                </div>
                                <div class="clearfix"></div>
								
								<div class="form-group" id="stay_claim_block" <?php if (($program[0]->stay_arrangement != 'Wagons') && ($program[0]->stay_arrangement != 'Trainer')) { ?>style="display:none;" <?php } ?>>
                                    
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Stay Billing Options <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                     <input type="radio" name="claim_stay_cost" value="Yes" <?php
                                        if ($program[0]->claim_stay_cost == 'Yes') {
                                            echo 'checked';
                                        }
                                        ?>> Claim with Client
                                        <input type="radio" name="claim_stay_cost" value="No" <?php
                                        if ($program[0]->claim_stay_cost != 'Yes') {
                                            echo 'checked';
                                        }
                                        ?> style="margin-left: 25px;"> Absorb By Wagons
								   
                                        
                                    </div>
                                </div>
								
								<div class="clearfix"></div>
										
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Travel Arrangement <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="travel" id="travel">
                                            <option value=""> - Select -</option>
                                            <option value="None" <?php
                                            if ($program[0]->travel_arrangement == 'None') {
                                                echo 'selected';
                                            }
                                            ?>> None </option>
                                            <option value="Wagons" <?php
                                            if ($program[0]->travel_arrangement == 'Wagons') {
                                                echo 'selected';
                                            }
                                            ?>> Wagons </option>
                                            <option value="Client" <?php
                                            if ($program[0]->travel_arrangement == 'Client') {
                                                echo 'selected';
                                            }
                                            ?>> Client </option>
                                            <option value="Trainer" <?php
                                            if ($program[0]->travel_arrangement == 'Trainer') {
                                                echo 'selected';
                                            }
                                            ?>> Trainer </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group" id="travel_limit_block" <?php if ($program[0]->travel_arrangement != 'Wagons') { ?>style="display:none;" <?php } ?>>
                                    <div class="clearfix"></div>
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Travel Spend Limit in Rs.<span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text"  name="travel_limit" class="form-control col-md-7 col-xs-12" value="<?php echo $program[0]->travel_limit; ?>"><br/>
                                        
                                    </div>
                                </div>
                                <div class="clearfix"></div>
								<div class="form-group" id="travel_claim_block" <?php if (($program[0]->travel_arrangement != 'Wagons') && ($program[0]->travel_arrangement != 'Trainer')) { ?>style="display:none;" <?php } ?>>
                                    
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Travel Billing Options <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                     <input type="radio" name="claim_travel_cost" value="Yes" <?php
                                        if ($program[0]->claim_travel_cost == 'Yes') {
                                            echo 'checked';
                                        }
                                        ?> > Claim with Client
                                        <input type="radio" name="claim_travel_cost" value="No"<?php
                                        if ($program[0]->claim_travel_cost != 'Yes') {
                                            echo 'checked';
                                        }
                                        ?> style="margin-left: 25px;"> Absorb By Wagons
								   
                                        
                                    </div>
                                </div>
								 <div class="clearfix"></div>
								
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Program Charges Agreed W/ Client/Day <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">

                                        <input type="text"  name="client_charges" class="form-control col-md-7 col-xs-12" value="<?php echo $program[0]->client_charges; ?>">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Trainer Fee Range/Day <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <select class="form-control" name="trainer_fee_range">
                                            <option value="" <?php
                                            if ($program[0]->trainer_fee_range == '') {
                                                echo 'selected';
                                            }
                                            ?>> - Select -</option>
                                            <option value="1000-2000" <?php
                                            if ($program[0]->trainer_fee_range == '1000-2000') {
                                                echo 'selected';
                                            }
                                            ?>> 1000-2000 </option>
                                            <option value="2000-3000" <?php
                                            if ($program[0]->trainer_fee_range == '2000-3000') {
                                                echo 'selected';
                                            }
                                            ?>> 2000-3000 </option>
                                            <option value="3000-4000" <?php
                                            if ($program[0]->trainer_fee_range == '3000-4000') {
                                                echo 'selected';
                                            }
                                            ?>> 3000-4000 </option>
                                            <option value="4000-5000" <?php
                                            if ($program[0]->trainer_fee_range == '4000-5000') {
                                                echo 'selected';
                                            }
                                            ?>> 4000-5000 </option>
                                            <option value="5000-6000" <?php
                                            if ($program[0]->trainer_fee_range == '5000-6000') {
                                                echo 'selected';
                                            }
                                            ?>> 5000-6000 </option>
                                            <option value="6000-7000" <?php
                                            if ($program[0]->trainer_fee_range == '6000-7000') {
                                                echo 'selected';
                                            }
                                            ?>> 6000-7000 </option>
                                            <option value="7000-8000" <?php
                                            if ($program[0]->trainer_fee_range == '7000-8000') {
                                                echo 'selected';
                                            }
                                            ?>> 7000-8000 </option>
                                            <option value="8000-9000" <?php
                                            if ($program[0]->trainer_fee_range == '8000-9000') {
                                                echo 'selected';
                                            }
                                            ?>> 8000-9000 </option>
                                            <option value="9000-10000" <?php
                                            if ($program[0]->trainer_fee_range == '9000-10000') {
                                                echo 'selected';
                                            }
                                            ?>> 9000-10000 </option>
                                            <option value="10000+" <?php
                                            if ($program[0]->trainer_fee_range == '10000+') {
                                                echo 'selected';
                                            }
                                            ?>> 10000+</option>					
                                        </select>

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                            <?php } else {
                                ?>
                            <h3>Sorry !! Now you can't edit this program</h3>
                                <?php
                            } ?>
                        </div>
                    </div>
                </div>
                <?php ?>
            </div>
        </div>

        <?php include 'js_files.php'; ?>

        <script type="text/javascript">

            $(document).ready(function () {


                $('#stay').on('change', function () {
					//alert(this.value);
                    if (this.value == "Wagons")
					{
                        $("#stay_limit_block").show();
						$("#stay_claim_block").show();
					}
					else if (this.value == "Trainer")
					{
                        $("#stay_limit_block").hide();
						$("#stay_claim_block").show();
					}						
                    else
					{
                        $("#stay_limit_block").hide();
						$("#stay_claim_block").hide();
					}
                });
                $('#travel').on('change', function () {
					//alert(this.value);
                    if (this.value == "Wagons")
					{
                        $("#travel_limit_block").show();
						$("#travel_claim_block").show();
					}
					 else if (this.value == "Trainer")
					{
                        $("#travel_limit_block").hide();
						$("#travel_claim_block").show();
					}
                    else
					{
                        $("#travel_limit_block").hide();
					    $("#travel_claim_block").hide();
					}
                });


                $("#add-program-form").validate({
                    rules: {
                        pname: "required",
                        client: "required",
                        location: "required",
                        sdate: "required",
                        duration: {
                            required: true,
                            number: true
                        },
                        skill_set: "required",
                        participants: {
                            required: true,
                            number: true
                        },
                        participant_level: "required",
                        stay: "required",
                        travel: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });


            });

        </script>

    </body>
</html>