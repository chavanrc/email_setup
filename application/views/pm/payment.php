<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Trainer Professional Fee Invoices</h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list"></i> Trainer Invoice List </h2>
                            <?php
                            $page = 1;
                            $next = 2;
                            $totalPage = 0;
                            if (isset($_GET['page'])) {
                                $page = $_GET['page'];
                            }
                            if (!empty($count)) {
                                $totalPage = ceil($count[0]->total / 20);
                                if ($count[0]->total > 20) {
                                    ?>
                                    <h4 class="pull-right" style="display: inline-block; margin-top: -20px;"><?php echo (($page - 1) * 20) + 1; ?> - <?php
                                        if ($page < $totalPage) {
                                            echo $page * 20;
                                        } else {
                                            echo $count[0]->total;
                                        }
                                        ?> of <?php echo $count[0]->total; ?> </h4>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" style="font-size: 13px;">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th style="width:200px;">Client</th>
                                        <th>Program</th>
                                        <th>Location</th>
                                        <th>Trainer</th>
                                        <th>Inv. Date</th>
                                        <th>Status</th>
                                        <th>Due Date</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <form action="<?php echo base_url(); ?>projectmanager/payments" method="POST">
                                    <tr>
                                        <td colspan="2">
                                            <select class="form-control" id="pclient" name="pclient" style="width:160px;">
                                                <option value="All"> - All Client -</option>
                                                <?php
                                                if (!empty($client)) {
                                                    foreach ($client as $cl_data) {
                                                        ?>
                                                        <option value="<?php echo $cl_data->client_id; ?>" <?php
                                                        if (isset($_POST['pclient'])) {
                                                            if ($_POST['pclient'] == $cl_data->client_id) {
                                                                echo 'selected';
                                                            }
                                                        }
                                                        ?> > <?php echo $cl_data->client_name; ?> </option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" placeholder="Program" name="title"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" placeholder="Location" name="location"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" placeholder="Trainer" name="trainer"/>
                                        </td>
                                        <td>
                                            <input type="date" class="form-control" placeholder="Inv Date" name="idate" style="width:120px;"/>
                                        </td>

                                        <td>
                                            <select class="form-control" name="status" style="display: inline-block; width: 130px;">
                                                <option value="All"> All </option>
                                                <option value="0"> PM Approval Pending </option>
                                                <option value="2"> Support Approval Pending </option>
                                                <option value="3"> Payment Pending </option>
                                                <option value="4"> Payment Paid </option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="date" class="form-control" placeholder="Due Date" name="ddate" style="width:120px;"/>
                                        </td>
                                        <td>
                                            <button class="btn-sm btn-warning"><i class="fa fa-search"></i></button>
                                        </td>
                                    </tr>
                                </form>
                                    <?php
                                    if (!empty($payment)) {
                                        $num = 0;
                                        foreach ($payment as $pm_data) {
                                            $num++;
                                            ?>
                                            <tr>
                                                <td><?php echo $num; ?></td>
                                                <td><?php echo substr($pm_data->client_name, 0, 20) . ".."; ?></td>
                                                <td><?php echo substr($pm_data->project_title, 0, 40) . ".."; ?><br>
                                                    <small><?php echo date_formate_short($pm_data->training_start_date); ?></small>
                                                </td>
                                                <td><?php echo $pm_data->location_of_training; ?></td>
                                                <td><?php echo $pm_data->name; ?></td>
                                                <td><?php echo date_formate_short($pm_data->trainer_invoice_date); ?></td>
                                                <td>
                                                    <?php
                                                    if ($pm_data->trainer_invoice_action == 0 || $pm_data->trainer_invoice_action == '') {
                                                        echo 'PM Approval Pending';
                                                    }
                                                    if ($pm_data->trainer_invoice_action == 2) {
                                                        echo 'Support Approval Pending';
                                                    }
                                                    if ($pm_data->trainer_invoice_action == 1) {
                                                        if ($pm_data->trainer_paid_status == '0') {
                                                            ?>
                                                            Payment Pending
                                                            <?php
                                                        }
                                                        if ($pm_data->trainer_paid_status == '1') {
                                                            ?>
                                                            Paid <br/>
                                                            <?php
                                                            echo date_formate_short($pm_data->trainer_payment_date);
                                                        }
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    $dueDate = date('d-m-Y', strtotime($pm_data->trainer_date_to . ' + ' . $pm_data->trainer_credit . ' days'));
                                                    echo $dueDate;
                                                    ?>
                                                </td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>projectmanager/invoice/<?php echo $pm_data->trainer_id; ?>/<?php echo $pm_data->project_id; ?>" class="btn-sm btn-primary"> <i class="fa fa-eye"></i></a>
                                                </td>

                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="9">No Record found</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <div class="text-right" style="font-size:16px; padding: 10px;">
                                <?php
                                if ($page > 1) {
                                    $next = $page + 1;
                                    $prev = $page - 1;
                                    ?>
                                    <a href="<?php echo base_url(); ?>projectmanager/payments/?page=1"><i class="fa fa-backward" style="font-size:12px;"></i> First</a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>projectmanager/payments/?page=<?php echo $prev; ?>"><i class="fa fa-caret-left"></i> Prev</a>
                                    <?php
                                }
                                if ($page < $totalPage) {
                                    ?>
                                    &nbsp;&nbsp;<a href="<?php echo base_url(); ?>projectmanager/payments/?page=<?php echo $next; ?>">Next <i class="fa fa-caret-right"></i> </a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>projectmanager/payments/?page=<?php echo $totalPage; ?>">Last <i class="fa fa-forward" style="font-size:12px;"></i> </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {


            });
        </script>

    </body>
</html>