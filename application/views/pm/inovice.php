<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Add Payment</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">

                    </div>
                    <div class="page-title title-right text-right no-print">

                        <?php
                        if ($fees[0]->trainer_invoice_action == 2 || $fees[0]->trainer_invoice_action == 1) {
                            if ($fees[0]->trainer_paid_status == 1) {
                                echo 'Paid on ' . date_formate_short($fees[0]->trainer_payment_date);
                            } else {
                                echo 'Approved';
                            }
                            if (empty($fees[0]->trainer_invoice_file)) {
                                ?>
                                <a href="<?php echo base_url(); ?>projectmanager/printinvoice/<?php echo $tid; ?>/<?php echo $pid; ?>"  class="create-invoice-btn btn btn-primary no-print">Print / Download</a>

                                <?php
                            } else {
                                ?>
                                <a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $fees[0]->trainer_invoice_file; ?>" target="_blank" class="create-invoice-btn btn btn-primary no-print">Print / Download</a>
                                <?php
                            }
                        } else {
                            ?>
                            <a href="#" class="btn btn-primary approve-btn pull-right" pid="<?php echo $fees[0]->id; ?>" project_id="<?php echo $pid; ?>" >Approve</a>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-6 content-page" style="font-size: 12px;">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <h3>Invoice</h3>
                            <?php
                            if (!empty($fees)) {
                                if (empty($fees[0]->trainer_invoice_file)) {
                                    ?>
                                    <table class="table table-bordered">
                                        <tr style="background:#656565; color: #fff; font-size: 15px;">
                                            <td>Name & Address of Trainer</td>
                                            <td>Invoice No</td>
                                            <td>Date</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="3">
                                                <?php echo $tDetails[0]->name; ?> <br/>
                                                <?php echo $tDetails[0]->address; ?><br/> <br/>
                                                <?php
                                                if (!empty($fees[0]->trainer_invoice_gst)) {
                                                    ?>
                                                    <strong>GSTIN : <?php echo $fees[0]->trainer_invoice_gst; ?> </strong>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                            <td>T/19-20/<?php echo $fees[0]->id; ?></td>
                                            <td>
                                                <?php
                                                if ($fees[0]->trainer_invoice_flag == 1) {
                                                    echo date_formate_short($fees[0]->trainer_invoice_date);
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr style="background:#656565; color: #fff; font-size: 15px;">
                                            <td colspan="2" class="text-center">Terms</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="text-center"><?php
                                                $startD = explode(' ', $fees[0]->training_date_from);
                                                $endD = explode(' ', $fees[0]->trainer_date_to);
                                                $start = strtotime($startD[0]);
                                                $end = strtotime($endD[0]);

                                                $days_between = ceil(abs($end - $start) / 86400);
                                                $edays = 1;
                                                if ($fees[0]->training_date_from != $fees[0]->trainer_date_to) {
                                                    $edays = 2;
                                                }
                                                echo $tDetails[0]->trainer_credit;
                                                ?> Days</td>
                                        </tr>
                                        <tr style="background:#656565; color: #fff; font-size: 15px;">
                                            <td> Client </td>
                                            <td colspan="2">Bill To</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Name : <?php echo $pDetails[0]->client_name; ?><br/>
                                                Program : <?php echo $pDetails[0]->project_title; ?><br/>
                                                Date : <?php echo $pDetails[0]->training_start_date; ?><br/>
                                                Location : <?php echo $pDetails[0]->location_of_training; ?><br/>
                                            </td>
                                            <td colspan="2">
                                                <?php if ($pDetails[0]->company_id == 1) { ?>
                                                    ATTN: Name / Dept : Accounts 	<br/>	
                                                    Wagons Management Consulting.	<br/>	
                                                    A/7-8 , Srushti Apartment Opp Corporation Bank	<br/>	
                                                    Baner Road, Pune - 411045<br/><br/>
                                                    <strong>GSTIN : 27AABFW3629E1Z8 </strong>
                                                <?php } if ($pDetails[0]->company_id == 2) { ?>
                                                    ATTN: Name / Dept : Accounts 	<br/>	
                                                    Wagons Learning Pvt Ltd.	<br/>	
                                                    A/7-8 , Srushti Apartment Opp Corporation Bank	<br/>	
                                                    Baner Road, Pune - 411045<br/><br/>
                                                    <strong>GSTIN : 27AABCW4538P1ZP </strong>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered">
                                        <tr style="background:#656565; color: #fff; font-size: 15px;">
                                            <td>Sl. No.</td>
                                            <td colspan="2">Description</td>
                                            <td>Rate</td>
                                            <td>Amount</td>
                                        </tr>
                                        <?php
                                        $no = 1;
                                        if (!empty($fees)) {
                                            ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td colspan="2"><?php
                                                    echo 'Professional Fees: ';

                                                    if ($pDetails[0]->half_day == '1')
                                                        echo "(Half Day Program)";
                                                    else
                                                        echo $pDetails[0]->training_duration . " Day(s)";
                                                    ?>
                                                </td>
                                                <td><?php echo $fees[0]->amount; ?></td>
                                                <td align="right"><?php echo number_format(round($fees[0]->amount * $pDetails[0]->training_duration, 2), 2); ?></td>
                                            </tr>
                                            <?php
                                        }
                                        $subTotal = 0;
                                        $total = round($fees[0]->amount * $pDetails[0]->training_duration, 2) + $subTotal;
                                        $gst = 0;
                                        if (!empty($fees[0]->trainer_invoice_gst)) {
                                            ?>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="2">&nbsp;</td>
                                                <td>CGST @ 9%</td>
                                                <td align="right">
                                                    <?php
                                                    if ($fees[0]->trainer_invoice_code == '27') {
                                                        $cgst = round(0.09 * $total, 2);
                                                        echo number_format($cgst, 2);
                                                        $gst = $cgst;
                                                    } else {
                                                        echo '-';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="2">&nbsp;</td>
                                                <td>SGST @ 9%</td>
                                                <td align="right"><?php
                                                    if ($fees[0]->trainer_invoice_code == '27') {
                                                        $sgst = round(0.09 * $total, 2);
                                                        echo number_format($sgst, 2);
                                                        $gst = $gst + $sgst;
                                                    } else {
                                                        echo '-';
                                                    }
                                                    ?></td>
                                            </tr>
                                            <tr>
                                                <td>&nbsp;</td>
                                                <td colspan="2">&nbsp;</td>
                                                <td>IGST @ 18%</td>
                                                <td align="right">
                                                    <?php
                                                    if ($fees[0]->trainer_invoice_code != '27') {
                                                        $igst = round(0.18 * $total, 2);
                                                        echo number_format($igst, 2);
                                                        $gst = $igst;
                                                    } else {
                                                        echo '-';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        <tr style="font-size: 18px;">
                                            <td colspan="4" class="text-right">Total</td>
                                            <td align="right">
                                                <?php
                                                $inv_total = round($total + $gst, 2);
                                                echo number_format($inv_total, 2);
                                                ?>/-
                                            </td>
                                        </tr>
                                        <tr><td colspan="5"><strong>In Words: 
                                                    <?php
                                                    $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                                    echo "Rs. " . ucfirst($f->format($inv_total)) . " Only.";
                                                    ?>
                                                </strong></td></tr>
                                    </table>
                                    <?php
                                } else {
                                    if ($fees[0]->trainer_invoice_flag == 0) {
                                        ?>
                                        <a href="#" class="btn-sm btn-primary editInvoice pull-right">Edit</a>
                                        <div class="clearfix"></div>
                                        <?php
                                    }
                                    ?>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>Invoice Number </td>
                                            <td><?php echo $fees[0]->trainer_invoice_number; ?></td>
                                        </tr>
                                        <tr>
                                            <td>GST Number </td>
                                            <td><?php echo $fees[0]->trainer_invoice_gst; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Total Amount </td>
                                            <td><?php echo $fTotal = $fees[0]->total_bill; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Invoice Copy </td>
                                            <td><a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $fees[0]->trainer_invoice_file; ?>" class="btn-sm btn-primary" target="_blank">View</a></td>
                                        </tr>
                                    </table>
                                    <?php
                                }
                            }
                            ?>
                            <table class="table table-bordered">
                                <tr style="background:#656565; color: #fff; font-size: 15px;">
                                    <td colspan="2">
                                        Bank Details
                                    </td>
                                </tr>
                                <?php
                                if (!empty($bank)) {
                                    ?>
                                    <tr>
                                        <td>Bank Name</td>
                                        <td><?php echo $bank[0]->bank_name; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Account Number</td>
                                        <td><?php echo $bank[0]->bank_account; ?></td>
                                    </tr>
                                    <tr>
                                        <td>IFSC Code</td>
                                        <td><?php echo $bank[0]->bank_ifsc; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Beneficiary Name</td>
                                        <td><?php echo $bank[0]->account_name; ?></td>
                                    </tr>
                                    <tr>
                                        <td>PAN No.</td>
                                        <td><?php echo $bank[0]->pan_no; ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
                <?php if (!empty($debit)) { ?>
                    <div class="col-md-6 content-page" style="font-size: 12px;">
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <div>
                                    <h3 style="display:inline-block;">Debit Note</h3>


                                    <div class="clearfix"></div>
                                </div>
                                <?php if (!empty($fees)) { ?>
                                    <table class="table table-bordered">
                                        <tr style="background:#656565; color: #fff; font-size: 15px;">
                                            <td>Name & Address of Trainer</td>
                                            <td>Invoice No</td>
                                            <td>Date</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="3">
                                                <?php echo $tDetails[0]->name; ?> <br/>
                                                <?php echo $tDetails[0]->address; ?>
                                            </td>
                                            <td>T/19-20/D/<?php echo $fees[0]->id; ?></td>
                                            <td>
                                                <?php
                                                if ($fees[0]->trainer_invoice_flag == 1) {
                                                    echo date_formate_short($fees[0]->trainer_invoice_date);
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr style="background:#656565; color: #fff; font-size: 15px;">
                                            <td colspan="2" class="text-center">Terms</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="text-center"><?php
                                                $startD = explode(' ', $fees[0]->training_date_from);
                                                $endD = explode(' ', $fees[0]->trainer_date_to);
                                                $start = strtotime($startD[0]);
                                                $end = strtotime($endD[0]);

                                                $days_between = ceil(abs($end - $start) / 86400);
                                                $edays = 1;
                                                if ($fees[0]->training_date_from != $fees[0]->trainer_date_to) {
                                                    $edays = 2;
                                                }
                                                //echo $days_between + $edays;
                                                echo $tDetails[0]->trainer_credit;
                                                ?> Days</td>
                                        </tr>
                                        <tr style="background:#656565; color: #fff; font-size: 15px;">
                                            <td> Client </td>
                                            <td colspan="2">Bill To</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Name : <?php echo $pDetails[0]->client_name; ?><br/>
                                                Program : <?php echo $pDetails[0]->project_title; ?><br/>
                                                Date : <?php echo $pDetails[0]->training_start_date; ?><br/>
                                                Location : <?php echo $pDetails[0]->location_of_training; ?><br/>
                                            </td>
                                            <td colspan="2">
                                                <?php if ($pDetails[0]->company_id == 1) { ?>
                                                    ATTN: Name / Dept : Accounts 	<br/>	
                                                    Wagons Management Consulting.	<br/>	
                                                    A/7-8 , Srushti Apartment Opp Corporation Bank	<br/>	
                                                    Baner Road, Pune - 411045<br/><br/>

                                                <?php } if ($pDetails[0]->company_id == 2) { ?>
                                                    ATTN: Name / Dept : Accounts 	<br/>	
                                                    Wagons Learning Pvt Ltd.	<br/>	
                                                    A/7-8 , Srushti Apartment Opp Corporation Bank	<br/>	
                                                    Baner Road, Pune - 411045<br/><br/>

                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered">
                                        <tr style="background:#656565; color: #fff; font-size: 15px;">
                                            <td>Sl. No.</td>
                                            <td colspan="2">Description</td>
                                            <td>Rate</td>
                                            <td colspan="2">Amount</td>
                                        </tr>
                                        <?php
                                        $no = 0;

                                        $subTotal = 0;
                                        if (!empty($debit)) {
                                            foreach ($debit as $d_data) {
                                                $no++;
                                                ?>
                                                <tr>
                                                    <td><?php echo $no; ?></td>
                                                    <td><?php echo $d_data->debit_title; ?> <br/>
                                                        <?php echo $d_data->debit_desc; ?>
                                                    </td>
                                                    <td>
                                                        <?php if (!empty($d_data->debit_file)) { ?>
                                                            <a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $d_data->debit_file; ?>" target="_blank">Receipt Copy</a>
                                                        <?php } ?>
                                                    </td>
                                                    <td align="right">
                                                        <?php echo number_format($d_data->debit_amt, 2); ?>
                                                    </td>
                                                    <td align="right">
                                                        <?php
                                                        echo number_format($d_data->debit_amt, 2);
                                                        if ($d_data->debit_status == '1') {
                                                            $subTotal = $subTotal + $d_data->debit_amt;
                                                        }
                                                        ?>
                                                        <br/>
                                                        <?php
                                                        if ($d_data->debit_status == '0') {
                                                            ?>
                                                            <span style="color:red; font-size: 12px;">Rejected</span>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                    <?php if ($fees[0]->trainer_invoice_action == 0) { ?>
                                                        <td>
                                                            <?php
                                                            if ($fees[0]->trainer_invoice_action == 0 && $d_data->debit_status == '1') {
                                                                ?>
                                                                <a href="#" class="remove-debit btn-sm btn-danger" did="<?php echo $d_data->dn_id; ?>">Reject</a>
                                                            <?php }
                                                            ?>
                                                        </td>
                                                    <?php } ?>
                                                </tr>

                                                <?php
                                            }
                                        }
                                        $total = $subTotal;
                                        ?>
                                        <tr style="font-size: 18px;">
                                            <td colspan="4" class="text-right">Total</td>
                                            <td colspan="2" align="right">
                                                <?php echo number_format($total, 2); ?>/-
                                            </td>
                                        </tr>
                                        <tr><td colspan="6"><strong>In Words: 
                                                    <?php
                                                    //$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                                    echo "Rs. " . ucfirst($f->format($total)) . " Only.";
                                                    ?>
                                                </strong></td></tr>
                                    </table>
                                <?php } ?>
                                <div>
                                    <?php
                                    if (!empty($courier)) {
                                        ?>
                                        <b>Courier Details  </b><br/>
                                        Ref. No : <?php echo $courier[0]->tc_ref; ?><br/>
                                        Details : <?php echo $courier[0]->tc_details; ?><br/>
                                        Courier Date : <?php echo date_formate_short($courier[0]->tc_cdate); ?><br/>
                                        Updated Date : <?php echo date_formate_short($courier[0]->tc_date); ?><br/>
                                    <?php }
                                    ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
            </div>
        </div>
        
        <div class="modal fade" id="confirm_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Confirm Bellow Check list</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="props-add-form">
                        <input type="hidden" name="pid" id="pid" value=""/>
                        <input type="hidden" name="trainer"  value="<?php echo $fees[0]->trainer_id; ?>"/>
                        <input type="hidden" name="project" value="<?php echo $fees[0]->project_id; ?>"/>
                        <div class="modal-body">
                            <div style="padding:10px;">
                                <label>
                                    1. <input type="checkbox" name="check1" value="1" class="checkList"/> Invoice is generated
                                </label>
                            </div>
                            <div style="padding:10px;">
                                <label>
                                    2. <input type="checkbox" name="check2" value="1" class="checkList"/> Debit note is generated
                                </label>
                            </div>
                            <div style="padding:10px;">
                                <label>
                                    3. <input type="checkbox" name="check3" value="1" class="checkList"/> Forms, Attendance, Feedback are attached
                                </label>
                            </div>
                            <div style="padding:10px;">
                                <label>
                                    4. <input type="checkbox" name="check4" value="1" class="checkList"/> Bills with Debit note are attached
                                </label>
                            </div>
                            <div style="padding:10px;">
                                <label>
                                    5. <input type="checkbox" name="check5" value="1" class="checkList"/> Courier charges, shipping charges are all updated.
                                </label>
                            </div>
                            <div style="padding:10px;">
                                <label>
                                    6. <input type="checkbox" name="check6" value="1" class="checkList"/> Content development charges added.
                                </label>
                            </div>

                            <div class="form-group" style="padding:10px;">
                                <textarea class="form-control" name="desc" placeholder="Your Note"></textarea>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" >Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">

            $(document).ready(function () {
                
                $("#props-add-form").validate({
                    rules: {
                        check1: "required",
                        check2: "required",
                        check3: "required",
                        check4: "required",
                        check5: "required",
                        check6: "required",
                        desc: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });
                
                $("#add-content-form").validate({
                    rules: {
                        program: "required",
                        content: "required",
                        dTitle: "required",
                        amt: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $('.approve-btn').click(function (e) {
                    e.preventDefault();
                    var pid = $(this).attr('pid');
                    $('#confirm_wraper').modal('show');
                    $('#pid').val(pid);
                    return false;
                    var project_id = $(this).attr('project_id');
                    $('.page_spin').show();
                    var dataString = "pid=" + pid + "&project_id=" + project_id + "&page=approve_invoice";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                });

                $('.remove-debit').click(function (e) {
                    e.preventDefault();
                    var did = $(this).attr('did');
                    var f = confirm("Are you sure want to reject debit note ?");
                    if (f == true) {
                        $('.page_spin').show();
                        var dataString = "did=" + did + "&page=remove_debit";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

            });

        </script>

    </body>
</html>