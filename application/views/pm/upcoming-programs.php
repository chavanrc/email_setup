<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Project Manager | Upcoming Programs</title>
        <?php include 'css_files.php'; ?>
        <style>
            .vc-class{
                display: none;
            }
            .st-class{
                display: none;
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Upcoming Programs</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="#" class="btn btn-success import-btn"><i class="fa fa-user-plus"></i> Import Program</a>
                        <a href="<?php echo base_url();             ?>projectmanager/add_program" class="btn btn-primary"><i class="fa fa-user-plus"></i> Create New Program</a>

                    </div> 
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" style="display:inline-block;"><i class="fa fa-list"></i> Programs List </h2>
                            <select class="form-control pull-right view-type"  name="view-type" style="width:200px; margin-top: -5px;">
                                <option value="All"> View Minimum </option>
                                <option value="vc"> View Venue & Content </option>
                                <option value="st"> View Stay & Travel </option>
                            </select>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Client</th>
                                        <th>Program Name</th>
                                        <th>Location</th>
                                        <th>Start Date</th>
                                        <th class="gn-class">Status</th>
                                        <th class="gn-class">Engage Requests</th>
                                        <th class="gn-class">Create Date</th>
                                        <th class="vc-class">Venue</th>
                                        <th class="vc-class">Content Status</th>
                                        <th class="st-class">Stay</th>
                                        <th class="st-class">Travel</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($program)) {
                                        $num = 0;
                                        foreach ($program as $pm_data) {
                                            $num++;
                                            $engage_count = $CI->projectmanager_model->count_engagement_request($pm_data->project_id);
                                            ?>
                                            <tr <?php if ($pm_data->is_active != '1') { ?>class="label-warning"<?php } ?>>
                                                <td><?php echo $num; ?></td>
                                                <td><?php echo $pm_data->client_name; ?></td>
                                                <td><?php echo $pm_data->project_title; ?> <?php
                                                    if ($pm_data->half_day == "1") {
                                                        echo "<strong style='color:#ED1212;'>( Half Day )</strong>";
                                                    }
                                                    ?></td>
                                                <td><?php echo $pm_data->location_of_training; ?></td>
                                                <td><?php echo date_formate_short($pm_data->training_start_date); ?></td>
                                                <td class="gn-class"><?php
                                                    if ($pm_data->is_active == 1) {
                                                        if ($pm_data->trainer_engage_flag == 1) {
                                                            echo 'Engaged';
                                                        } else {
                                                            echo 'New';
                                                        }
                                                    } else if ($pm_data->is_active == 2) {
                                                        echo 'Disabled';
                                                    } else {
                                                        echo 'Cancelled';
                                                    }
                                                    ?></td>
                                                <td class="gn-class"><?php echo $engage_count; ?></td>
                                                <td class="gn-class"><?php echo date_formate_short($pm_data->create_date); ?></td>
                                                <td class="vc-class"><?php
                                                    if (empty($pm_data->venue)) {
                                                        echo 'Not Added';
                                                    } else {
                                                        echo 'Added';
                                                    }
                                                    ?></td>
                                                <td class="vc-class"><?php
                                                    if (empty($pm_data->module)) {
                                                        echo 'NA';
                                                    } else {
                                                        if (empty($pm_data->trainer_content_pass)) {
                                                            echo 'Not sent to trainer';
                                                        } else if ($pm_data->content_download_date == '0000-00-00 00:00:00') {
                                                            echo 'Sent to trainer';
                                                        } else {
                                                            echo 'Trainer downloaded';
                                                        }
                                                    }
                                                    ?></td>
                                                <td class="st-class"><?php
                                                    echo $pm_data->stay_arrangement;
                                                    if ($pm_data->stay_arrangement == "Wagons") {
                                                        if ($pm_data->claim_stay_cost == "Yes")
                                                            echo " <br/> Claim Cost with Client";
                                                        else
                                                            echo " <br/> Wagons to Absorb Cost";
                                                    } else if ($pm_data->claim_stay_cost == "Yes") {
                                                        echo " <br/> Claim Cost with Client";
                                                    }
                                                    ?></td>
                                                <td class="st-class"><?php
                                                    echo $pm_data->travel_arrangement;
                                                    if ($pm_data->travel_arrangement == "Wagons") {
                                                        if ($pm_data->claim_travel_cost == "Yes")
                                                            echo " <br/> Claim Cost with Client";
                                                        else
                                                            echo " <br/> Wagons to Absorb Cost";
                                                    } else if ($pm_data->claim_travel_cost == "Yes") {
                                                        echo " <br/> Claim Cost with Client";
                                                    }
                                                    ?></td>
                                                <td><a href="<?php echo base_url(); ?>projectmanager/program_details/<?php echo $pm_data->project_id; ?>" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a></td>
                                            </tr>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <tr>
                                            <td colspan="9">No Record found</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="import-wrap" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-arrow-right"></i> Import Excel Sheet of Program</h4>
                    </div>
                    <div class="modal-body">
                        <form action="" method="POST"  enctype="multipart/form-data" id="import-form">
                            <input type="hidden" value="" id="cid" name="cid"/>
                            <div class="form-group col-md-8 col-md-offset-2">
                                <select class="form-control" id="client" name="client">
                                    <option value=""> - Select Client -</option>
                                    <?php
                                    $client = $this->projectmanager_model->get_client($this->session->userdata('pm_code'));
                                    if (!empty($client)) {
                                        foreach ($client as $cl_data) {
                                            ?>
                                            <option value="<?php echo $cl_data->client_id; ?>"> <?php echo $cl_data->client_name; ?> </option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group  col-md-8 col-md-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Upload File <sup>*</sup></span>
                                    <input type="file" name="content" accept="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet, application/vnd.ms-excel" class="form-control">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-8 col-md-offset-2 text-center">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.view-type').change(function () {
                    var vt = $(this).val();
                    if (vt == 'All') {
                        $('.gn-class').show();
                        $('.vc-class').hide();
                        $('.st-class').hide();
                    }
                    if (vt == 'vc') {
                        $('.gn-class').hide();
                        $('.vc-class').show();
                        $('.st-class').hide();
                    }
                    if (vt == 'st') {
                        $('.gn-class').hide();
                        $('.vc-class').hide();
                        $('.st-class').show();
                    }
                });
            });
        </script>

    </body>
</html>