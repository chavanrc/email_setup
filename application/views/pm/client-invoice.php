<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Project Manager | Clients</title>
        <?php include 'css_files.php'; ?>
        <link href='https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' />
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Client Invoices</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <?php
                        $page = 1;
                        $next = 2;
                        $totalPage = 0;
                        if (isset($_GET['page'])) {
                            $page = $_GET['page'];
                        }
                        if (!empty($count)) {
                            $totalPage = ceil($count[0]->total / 20);
                            if ($count[0]->total > 20) {
                                ?>
                                <h4 class="pull-right" style="display: inline-block; margin-top: 10px;"><?php echo (($page - 1) * 20) + 1; ?> - <?php
                                    if ($page < $totalPage) {
                                        echo $page * 20;
                                    } else {
                                        echo $count[0]->total;
                                    }
                                    ?> of <?php echo $count[0]->total; ?> </h4>
                                <?php
                            }
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page" style="background-color: #fff;">
                    <table class="table table-bordered" style="font-size: 13px; margin-top: 15px;">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Client</th>
                                <th>Invoice No.</th>
                                <th>Invoice Date</th>
                                <th style="width: 200px;">Program</th>
                                <th>Location</th>
                                <th>Status</th>
                                <th>Due Date</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                        <form action="<?php echo base_url(); ?>projectmanager/client_invoice" method="POST">
                            <tr>
                                <td colspan="2">
                                    <select class="form-control" id="pclient" name="pclient" style="width:170px;">
                                        <option value="All"> - All Client -</option>
                                        <?php
                                        if (!empty($client)) {
                                            foreach ($client as $cl_data) {
                                                ?>
                                                <option value="<?php echo $cl_data->client_id; ?>" <?php
                                                if (isset($_POST['pclient'])) {
                                                    if ($_POST['pclient'] == $cl_data->client_id) {
                                                        echo 'selected';
                                                    }
                                                }
                                                ?> > <?php echo $cl_data->client_name; ?> </option>
                                                        <?php
                                                    }
                                                }
                                                ?>
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" placeholder="Invoice No" name="invoice"/>
                                </td>
                                <td>
                                    <input type="date" class="form-control" placeholder="Invoice No" name="idate" style="width: 130px;"/>
                                </td>
                                <td>
                                    <input type="text" class="form-control" placeholder="Program" name="title"/>
                                </td>
                                <td>
                                    <input type="text" class="form-control" placeholder="Location" name="location"/>
                                </td>
                                <td>
                                    <select class="form-control" name="status" style="display: inline-block; width: 100px;">
                                        <option value="All"> All </option>
                                        <option value="0"> Payment Advice Pending </option>
                                        <option value="1"> Payment Advice Received </option>
                                        <option value="2"> Payment Pending </option>
                                        <option value="3"> Payment Received </option>
                                    </select>
                                </td>
                                <td>
                                    <input type="date" class="form-control" placeholder="Invoice No" name="ddate" style="width: 130px;"/>
                                </td>
                                <td>
                                    <button class="btn-sm btn-warning"><i class="fa fa-search"></i></button>
                                </td>
                            </tr>
                        </form>
                        <?php
                        if (!empty($invoice)) {
                            $no = 0;
                            if (isset($_GET['page'])) {
                                $no = 20 * ($_GET['page'] - 1);
                            }
                            foreach ($invoice as $in_data) {
                                $no++;
                                ?>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td><?php echo substr($in_data->client_name, 0, 20) . ".."; ?></td>
                                    <td><?php echo $in_data->ci_number; ?></td>
                                    <td><?php echo date_formate_short($in_data->ci_date); ?></td>
                                    <td><?php echo substr($in_data->project_title, 0, 50) . ".."; ?></td>
                                    <td><?php echo $in_data->location_of_training; ?></td>
                                    <td style="font-size: 12px; width: 90px;">  
                                        Pay : <?php
                                        if ($in_data->ci_pay_status == '1') {
                                            echo 'Paid';
                                        } else {
                                            echo 'Pending';
                                        }
                                        ?><br/>
                                        Pay Advice : 
                                        <?php
                                        if ($in_data->ci_pay_advice == '0') {
                                            echo 'Pending';
                                        } else if ($in_data->ci_pay_advice == '1') {
                                            echo 'Received';
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <?php
                                        $dueDate = date('d-m-Y', strtotime($in_data->ci_date . ' + ' . $in_data->payment_credit_days . ' days'));
                                        echo $dueDate;
                                        ?>
                                    </td>
                                    <td>
                                        <a href="<?php echo base_url(); ?>projectmanager/client_invoice_details/<?php echo $in_data->ci_id; ?>" class="btn-sm btn-primary"><i class="fa fa-eye"></i></a>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                        </tbody>
                    </table>
                    <div class="text-right" style="font-size:16px; padding: 10px;">
                        <?php
                        if ($page > 1) {
                            $next = $page + 1;
                            $prev = $page - 1;
                            ?>
                            <a href="<?php echo base_url(); ?>projectmanager/client_invoice/?page=1"><i class="fa fa-backward" style="font-size:12px;"></i> First</a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>projectmanager/client_invoice/?page=<?php echo $prev; ?>"><i class="fa fa-caret-left"></i> Prev</a>
                            <?php
                        }
                        if ($page < $totalPage) {
                            ?>
                            &nbsp;&nbsp;<a href="<?php echo base_url(); ?>projectmanager/client_invoice/?page=<?php echo $next; ?>">Next <i class="fa fa-caret-right"></i> </a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>projectmanager/client_invoice/?page=<?php echo $totalPage; ?>">Last <i class="fa fa-forward" style="font-size:12px;"></i> </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>


        <?php include 'js_files.php'; ?>
        <script src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                
            });
        </script>

    </body>
</html>