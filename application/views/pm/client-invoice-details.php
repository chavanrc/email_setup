<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Project Manager | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
        <style>
            td, th
            {
                font-size:12px;
            }
        </style>

    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">

                    <?php
                    if ($invoice[0]->ci_img != '') {
                        ?>
                        <div class="page-title title-left">
                            <a href="<?php echo base_url(); ?>assets/upload/client/<?php echo $invoice[0]->ci_img; ?>" target="_blank" class="btn btn-warning"><i class="fa fa-file-image-o" aria-hidden="true"></i> Invoice Copy</a>
                        </div>
                        <?php
                    }
                    ?>

                    <div class="page-title title-right text-right no-print">


                        <a href="<?php echo base_url(); ?>projectmanager/client_invoice" class="create-invoice-btn btn btn-danger">Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-10 content-page" >
                    <div class="panel panel-default in-print">
                        <div class="panel-body" >
                            <table class="table table-bordered" >
                                <tr style="font-size:15px;">
                                    <td colspan="3" style="text-align: center;"><h4 style="font-size:15px;">Tax Invoice <?php if ($invoice[0]->ci_status == '2') { ?><span style="color:#ED1122;"><strong>(Invoice Cancelled)</strong></span><?php } ?></h4></td>
                                </tr>
                                <tr>
                                    <td rowspan="5" style="width:350px;">To, <br/>
                                        <?php echo $client[0]->client_name; ?><br/>
                                        <?php
                                        if (!empty($invoice)) {
                                            echo $invoice[0]->ci_gst_address;
                                        }
                                        ?>
                                    </td>


                                    <td>Invoice Number</td>
                                    <td><?php echo $invoice[0]->ci_number; ?><?php //echo $invoice[0]->ci_id;                             ?></td>
                                </tr>
                                <tr>
                                    <td>Invoice Date</td>
                                    <td><?php echo date_formate_short($invoice[0]->ci_date); ?></td>
                                </tr>
                                <tr>
                                    <td>Ref. No</td>
                                    <td><?php
                                        if ($client[0]->debit_note == 'Yes' && $invoice[0]->ci_debit_id != '0') {
                                            echo $invoice[0]->ci_no_frmt;
                                            ?>D/<?php
                                            echo str_pad($invoice[0]->ci_debit_id, 3, "0", STR_PAD_LEFT);
                                        } else {
                                            echo 'NA';
                                        }
                                        ?></td>
                                </tr>
                                <tr>

                                    <td>Name</td>
                                    <td><?php echo $invoice[0]->ci_name; ?></td>
                                </tr>
                                <tr>

                                    <td>Email id</td>
                                    <td><?php echo $invoice[0]->ci_email; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>State Code :  </strong><?php
                                        if (!empty($invoice)) {
                                            echo $invoice[0]->ci_state_code;
                                        }
                                        ?> 
                                    </td>
                                    <td>PO. No</td>
                                    <td><?php
                                        if (empty($invoice[0]->ci_poNumber)) {
                                            echo 'NA';
                                        } else {
                                            echo $invoice[0]->ci_poNumber;
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>GST NO: </strong> <?php
                                        if (!empty($invoice)) {
                                            echo $invoice[0]->ci_gst;
                                        }
                                        ?>
                                    </td>
                                    <td>SAC Code</td>
                                    <td>999293</td>
                                </tr>
                            </table>
                            <?php
                            $total = 0;
                            $no = 1;
                            $dc = 0;
                            ?>
                            <table class="table table-bordered" style="margin-bottom:0px;">
                                <tr style="font-weight: bold;">
                                    <td>Sl. No</td>
                                    <td style="text-align:center;">Description</td>
                                    <td style="text-align:center;">Program Name</td>
                                    <td style="text-align:center;">Program Date</td>
                                    <td style="text-align:center;">Location</td>
                                    <td style="text-align:center;">No. of Days</td>
                                    <td style="text-align:center;">Amount</td>
                                </tr>
                                <tr>
                                    <td><?php echo $no; ?></td>
                                    <td style="text-align:center;">Professional Fees for training program</td>
                                    <td style="text-align:center;"  valign="bottom"><?php echo $project[0]->project_title; ?></td>
                                    <td style="text-align:center;" ><?php
                                        if ($project[0]->project_id != '83386') {
                                            echo date_formate_short($project[0]->training_start_date);
                                        } else {
                                            echo 'NA';
                                        }
                                        ?></td>
                                    <td style="text-align:center;" ><?php echo $project[0]->location_of_training; ?></td>
                                    <td style="text-align:center;" ><?php
                                        if ($project[0]->project_id != '83386') {
                                            if ($project[0]->half_day == '1') {
                                                echo 'Half Day';
                                            } else {
                                                echo $project[0]->training_duration;
                                            }
                                        } else {
                                            echo 'NA';
                                        }
                                        ?></td>
                                    <td style="text-align:right;"><?php
                                        $total_fees = $invoice[0]->ci_charge * $project[0]->training_duration;
                                        echo number_format($total_fees, 2);
                                        ?></td>
                                </tr>
                                <?php
                                $total = $total + $total_fees;
                                if ($client[0]->debit_note == 'No') {
                                    if (!empty($exp)) {
                                        foreach ($exp as $ex_data) {
                                            if ($ex_data->absorb == 'N') {
                                                $no++;
                                                ?>
                                                <tr>
                                                    <td><?php echo $no; ?></td>
                                                    <td style="text-align:center;"><?php echo $ex_data->expense_type; ?></td>
                                                    <td style="text-align:center;"><?php echo $ex_data->in_amount; ?></td>
                                                </tr>
                                                <?php
                                                $total = $total + $ex_data->in_amount;
                                            }
                                        }
                                    }
                                    ?>
                                    <?php
                                }
                                ?>
                                <tr style="font-size:15px;">
                                    <td colspan="6" class="text-right">Gross Total</td>
                                    <td style="width:100px; text-align: right;"><?php echo number_format($total, 2); ?></td>
                                </tr>
                            </table>
                            <?php
                            $company = $CI->admin_model->get_single_company($invoice[0]->ci_company);
                            $cmState = $company[0]->gst_state_code;
                            $cState = $company[0]->gst_state_code;
                            if (!empty($invoice)) {
                                $cState = $invoice[0]->ci_state_code;
                            }
                            ?>
                            <table class="table table-bordered">
                                <tr >
                                    <td >PAN No.</td>
                                    <td><?php echo $company[0]->pan_no; ?></td>
                                    <td class="text-right">CGST On Professional Fees @9%</td>
                                    <td style="width:100px; text-align: right;">
                                        <?php
                                        if ($cState == $cmState) {
                                            $cgst = round((9 / 100) * $total);
                                            echo number_format($cgst, 2);
                                        } else {
                                            echo '-';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>GSTIN</td>
                                    <td><?php echo $company[0]->gstn; ?></td>
                                    <td class="text-right">SGST On Professional Fees @9%</td>
                                    <td style="text-align: right;"><?php
                                        if ($cState == $cmState) {
                                            $sgst = round((9 / 100) * $total);
                                            echo number_format($sgst, 2);
                                        } else {
                                            echo '-';
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>TAN No</td>
                                    <td><?php echo $company[0]->tan_no; ?></td>
                                    <td class="text-right">IGST On Professional Fees @18%</td>
                                    <td style="text-align: right;">

                                        <?php
                                        if ($cState != $cmState) {
                                            $gstamt = round((18 / 100) * $total);
                                            echo number_format($gstamt, 2);
                                        } else {
                                            $gstamt = $cgst + $sgst;
                                            echo '-';
                                        }
                                        $gTotal = $total + $gstamt;
                                        ?>

                                    </td>
                                </tr>
                                <tr style="font-size:20px;">
                                    <td colspan="2"></td>
                                    <td class="text-right">Total</td>
                                    <td style="text-align:right;">
                                        <?php
                                        $upAmount = number_format($gTotal, 2);
                                        echo $upAmount;
                                        ?>
                                    </td>
                                </tr>
                                <tr><td colspan="4"><strong>In Words: 
                                            <?php
                                            $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                            echo "Rs. " . ucwords($f->format($gTotal)) . " Only.";
                                            ?>
                                        </strong></td></tr>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td>Beneficiary</td>
                                    <td><?php echo $company[0]->company_name; ?></td>
                                    <td rowspan="5" style="text-align: center;">For, <?php echo $company[0]->company_name; ?>
                                        <br/><br/><br/><br/><br/><br/><br/><br/>
                                        Authorized Signatory
                                    </td>
                                </tr>
                                <tr>
                                    <td>Account No</td>
                                    <td><?php echo $company[0]->account_no; ?></td>
                                </tr><tr>
                                    <td>IFSC Code</td>
                                    <td><?php echo $company[0]->ifsc_no; ?></td>
                                </tr>
                                <tr>
                                    <td>Bank & Branch</td>
                                    <td><?php echo $company[0]->bank_branch; ?></td>
                                </tr>
                                <tr>
                                    <td class="text-center" colspan="2">
                                        <strong>
                                            * Thanking you and assuring our best services at all times
                                        </strong>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <p style="page-break-before: always"/>

                    <?php if ($client[0]->debit_note == 'Yes' && !empty($exp)) {
                        ?>
                        <p style="margin-top:60px;">&nbsp;</p>
                        <div class="panel panel-default in-print" >
                            <div class="panel-body">
                                <table class="table table-bordered" >
                                    <tr style="font-size:15px;">
                                        <td colspan="3" style="text-align: center;"><h4 style="font-size:15px;">Tax Invoice</h4></td>
                                    </tr>
                                    <tr>
                                        <td rowspan="3" style="width:350px;">To, <br/>
                                            <?php echo $client[0]->client_name; ?><br/>
                                            <?php
                                            if (!empty($invoice)) {
                                                echo $invoice[0]->ci_gst_address;
                                            }
                                            ?>
                                        </td>
                                        <td>Invoice Number</td>
                                        <td><?php echo $invoice[0]->ci_no_frmt; ?>D/<?php echo str_pad($invoice[0]->ci_debit_id, 3, "0", STR_PAD_LEFT); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Invoice Date</td>
                                        <td><?php echo date_formate_short($invoice[0]->ci_date); ?></td>
                                    </tr>
                                    <tr>
                                        <td>Ref. No</td>
                                        <td><?php echo $invoice[0]->ci_number; ?></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>State Code :  </strong><?php
                                            if (!empty($invoice)) {
                                                echo $invoice[0]->ci_state_code;
                                            }
                                            ?> 
                                        </td>
                                        <td>PO. No</td>
                                        <td><?php
                                            if (empty($invoice[0]->ci_poNumber)) {
                                                echo 'NA';
                                            } else {
                                                echo $invoice[0]->ci_poNumber;
                                            }
                                            ?></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong>GST NO: </strong> <?php
                                            if (!empty($invoice)) {
                                                echo $invoice[0]->ci_gst;
                                            }
                                            ?>
                                        </td>
                                        <td>SAC Code</td>
                                        <td>999293</td>
                                    </tr>

                                </table>
                                <?php
                                $total = 0;
                                $no = 0;
                                ?>
                                <table class="table table-bordered" style="margin-bottom:0px;">
                                    <tr style="font-weight: bold;">
                                        <td style="width:80px;">Sl. No</td>
                                        <td style="text-align:center;">Description</td>
                                        <td style="text-align:center;">Program Name</td>
                                        <td style="text-align:center;">Program Date</td>
                                        <td style="text-align:center;">Amount</td>
                                    </tr>
                                    <?php
                                    if (!empty($exp)) {
                                        foreach ($exp as $ex_data) {
                                            if ($ex_data->absorb == 'N') {
                                                if ($ex_data->in_amount != '0') {
                                                    $no++;
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $no; ?></td>
                                                        <td style="text-align:center; valign:middle;"><?php echo $ex_data->expense_type; ?></td>
                                                        <?php if ($no == 1) { ?>
                                                            <td rowspan="<?php echo count($exp); ?>" style="text-align:center; valign:middle;"><?php echo $project[0]->project_title; ?></td>
                                                            <td rowspan="<?php echo count($exp); ?>" style="text-align:center; valign:middle;"><?php echo date_formate_short($project[0]->training_start_date); ?></td>
                                                        <?php } ?>
                                                        <td style="text-align:right; valign:middle;"><?php echo number_format($ex_data->in_amount, 2); ?></td>
                                                    </tr>
                                                    <?php
                                                    $total = $total + $ex_data->in_amount;
                                                }
                                            }
                                        }
                                    }
                                    ?>
                                    <tr style="font-size:18px;">
                                        <td colspan="4" class="text-right">Gross Total</td>
                                        <td style="width:100px; text-align:right;"><?php echo number_format($total, 2); ?></td>
                                    </tr>
                                </table>
                                <?php
                                $company = $CI->admin_model->get_single_company($invoice[0]->ci_company);
                                $cmState = $company[0]->gst_state_code;
                                $cState = $company[0]->gst_state_code;
                                if (!empty($invoice)) {
                                    $cState = $invoice[0]->ci_state_code;
                                }
                                ?>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>PAN No.</td>
                                        <td><?php echo $company[0]->pan_no; ?></td>
                                        <td class="text-right">CGST On Professional Fees @9%</td>
                                        <td style="width:100px; text-align:right;">
                                            <?php
                                            if ($cState == $cmState) {
                                                $cgst = round((9 / 100) * $total);
                                                echo number_format($cgst, 2);
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>GSTIN</td>
                                        <td><?php echo $company[0]->gstn; ?></td>
                                        <td class="text-right">SGST On Professional Fees @9%</td>
                                        <td style="text-align:right;">
                                            <?php
                                            if ($cState == $cmState) {
                                                $sgst = round((9 / 100) * $total);
                                                echo number_format($sgst, 2);
                                            } else {
                                                echo '-';
                                            }
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>TAN No</td>
                                        <td><?php echo $company[0]->tan_no; ?></td>
                                        <td class="text-right">IGST On Professional Fees @18%</td>
                                        <td style="text-align:right;">
                                            <?php
                                            if ($cState != $cmState) {
                                                $gstamt = round((18 / 100) * $total);
                                                echo number_format($gstamt, 2);
                                            } else {
                                                $gstamt = $cgst + $sgst;
                                                echo '-';
                                            }
                                            $gTotal = $total + $gstamt;
                                            ?>
                                        </td>
                                    </tr>
                                    <tr style="font-size:20px;">
                                        <td colspan="2"></td>
                                        <td class="text-right">Total</td>
                                        <td style="text-align:right;">
                                            <?php echo number_format($gTotal, 2); ?>
                                        </td>
                                    </tr>
                                    <tr><td colspan="4"><strong>In Words: 
                                                <?php
                                                $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                                echo "Rs. " . ucwords($f->format($gTotal)) . " Only.";
                                                ?>
                                            </strong></td></tr>
                                </table>
                                <table class="table table-bordered">
                                    <tr>
                                        <td>Beneficiary</td>
                                        <td><?php echo $company[0]->company_name; ?></td>
                                        <td rowspan="5" style="text-align: center;">For, <?php echo $company[0]->company_name; ?>
                                            <br/><br/><br/><br/><br/><br/><br/><br/>
                                            Authorized Signatory
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Account No</td>
                                        <td><?php echo $company[0]->account_no; ?></td>
                                    </tr><tr>
                                        <td>IFSC Code</td>
                                        <td><?php echo $company[0]->ifsc_no; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Bank & Branch</td>
                                        <td><?php echo $company[0]->bank_branch; ?></td>
                                    </tr>
                                    <tr>
                                        <td class="text-center" colspan="2">
                                            <strong>
                                                * Thanking you and assuring our best services at all times
                                            </strong>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <?php
                    }

                    if (!empty($payment)) {
                        ?>
                        <div class="panel panel-default no-print">
                            <div class="panel-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <td colspan="5"><h4>Payment Details</h4></td>
                                    </tr>
                                    <tr>
                                        <td>Amount</td>
                                        <td>Date</td>
                                        <td>Payment id</td>
                                        <td>Payment Type</td>
                                        <td>Payment Advice</td>
                                    </tr>
                                    <?php foreach ($payment as $pdata) {
                                        ?>
                                        <tr>
                                            <td><?php echo $pdata->cp_amt; ?></td>
                                            <td><?php echo date_formate_short($pdata->cp_date); ?></td>
                                            <td><?php echo $pdata->cp_pay_id; ?></td>
                                            <td><?php
                                                if ($pdata->cp_type == '1') {
                                                    echo 'Professional Fees';
                                                } else {
                                                    echo 'Debit Note';
                                                }
                                                ?></td>
                                            <td>
                                                <?php
                                                if ($pdata->cp_type == '1') {
                                                    if ($invoice[0]->ci_pay_advice == '1') {
                                                        echo 'Received';
                                                    } else if ($invoice[0]->ci_pay_advice == '0') {
                                                        echo 'Pending';
                                                    }
                                                }
                                                ?>

                                            </td>

                                        </tr>
                                    <?php }
                                    ?>

                                </table>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>

<?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {

                $("#pay-form").validate({
                    rules: {
                        amt: "required",
                        date: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $('.make-payment-btn').click(function (e) {
                    $('#pay_wraper').modal('show');
                });

                $('.confirm-btn').click(function (e) {
                    e.preventDefault();
                    var cid = $(this).attr('inid');
                    var f = confirm("Are you sure want confirm ?");
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "cid=" + cid + "&page=confirm_client_invoice";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

            });
        </script>

    </body>
</html>