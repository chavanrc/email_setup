<div class="text-right">
    Status : <?php
    if (!empty($content)) {
        if ($content[0]->content_status == '1') {
            echo 'Closed for Update';
        } else {
            echo 'Open for Update';
        }
    }
    ?>
</div>
<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Module</th>
            <th>Content Title</th>
            <th>Note</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php
        if (!empty($content)) {
            foreach ($content as $cn_data) {
                ?>
                <tr >
                    <td><?php echo $cn_data->module_name; ?></td>
                    <td><?php echo $cn_data->content_title; ?></td>
                    <td><?php echo $cn_data->content_notes; ?></td>
                    <td>
                        <a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $cn_data->content_file; ?>" download="<?php echo $cn_data->content_file; ?>" class="btn-sm btn-primary"><i class="fa fa-download"></i></a>
                    </td>
                </tr>
                <?php
            }
        }
        ?>
    </tbody>
</table>

<div class="text-center">
    <?php
    if (!empty($content)) {
        $check = $this->projectmanager_model->check_content_send($_POST['pid']);
        if (empty($check)) {
            ?>
            <a href="#" class="btn btn-primary confirm-content-btn" pid="<?php echo $_POST['pid']; ?>">Confirm & Send to Trainer</a>
        <?php } else {
            ?>
            <span>
                Content was sent to trainer on : <?php echo $check[0]->trainer_pass_date; ?>
            </span>
            <span style="margin-left: 100px;">
                Content access password  : <?php echo $check[0]->trainer_content_pass; ?>
            </span>
            <?php
        }
    }
    ?>
</div>