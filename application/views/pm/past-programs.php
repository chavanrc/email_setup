<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Project Manager | Past Programs</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Past Programs</h3>
                    </div>
                    <div class="page-title title-right text-right">
                          <a href="#" class="btn btn-success import-btn"><i class="fa fa-user-plus"></i> Import Program</a>
                          <a href="<?php echo base_url();           ?>projectmanager/add_program" class="btn btn-primary"><i class="fa fa-user-plus"></i> Create New Program</a>
                      </div> 
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list"></i> Programs List </h2>
                            <?php
                            $page = 1;
                            $next = 2;
                            $totalPage = 0;
                            if (isset($_GET['page'])) {
                                $page = $_GET['page'];
                            }
                            if (!empty($count)) {
                                $totalPage = ceil($count[0]->total / 20);
                                if ($count[0]->total > 20) {
                                    ?>
                                    <h4 class="pull-right" style="display: inline-block; margin-top: -20px;"><?php echo (($page - 1) * 20) + 1; ?> - <?php
                                        if ($page < $totalPage) {
                                            echo $page * 20;
                                        } else {
                                            echo $count[0]->total;
                                        }
                                        ?> of <?php echo $count[0]->total; ?> </h4>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Client</th>
                                        <th>Title</th>
                                        <th>Location</th>
                                        <th>Start Date</th>
                                        <th>Client Invoice</th>
                                        <th>Trainer Invoice</th>
                                        <th style="width:110px;">Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <form action="<?php echo base_url(); ?>projectmanager/past_programs" method="POST">
                                    <tr>
                                        <td></td>
                                        <td>
                                            <select class="form-control" id="pclient" name="pclient">
                                                <option value="All"> - All Client -</option>
                                                <?php
                                                $client = $this->projectmanager_model->get_client($this->session->userdata('pm_code'));
                                                if (!empty($client)) {
                                                    foreach ($client as $cl_data) {
                                                        ?>
                                                        <option value="<?php echo $cl_data->client_id; ?>" <?php
                                                        if (isset($_POST['pclient'])) {
                                                            if ($_POST['pclient'] == $cl_data->client_id) {
                                                                echo 'selected';
                                                            }
                                                        }
                                                        ?> > <?php echo $cl_data->client_name; ?> </option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                            </select> 
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="title" value="<?php
                                            if (isset($_POST['title'])) {
                                                echo $_POST['title'];
                                            }
                                            ?>" placeholder="Title"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" name="location" value="<?php
                                            if (isset($_POST['location'])) {
                                                echo $_POST['location'];
                                            }
                                            ?>" placeholder="Location"/>
                                        </td>
                                        <td>
                                            <input type="date" class="form-control" name="date" value="<?php
                                            if (isset($_POST['date'])) {
                                                echo $_POST['date'];
                                            }
                                            ?>" placeholder="Location"/>
                                        </td>
                                        <td style="width:96px;">
                                            <select class="form-control" id="cinvoice" name="cinvoice">
                                                <option value="All"> - All -</option>
                                                <option value="N">Pending</option>
                                                <option value="Y">Generated</option>
                                            </select>
                                        </td>
                                        <td style="width:96px;">
                                            <select class="form-control" id="tinvoice" name="tinvoice">
                                                <option value="All"> - All -</option>
                                                <option value="0">Pending</option>
                                                <option value="1">Generated</option>
                                            </select>
                                        </td>
                                        <td> <select class="form-control" id="status" name="status">
                                                <option value=""> All</option>
                                                <option value="0"> Cancelled</option>
                                                <option value="1"> Completed</option>
                                                <option value="2"> Disabled</option>
                                            </select>
                                        </td>
                                        <td >
                                            <button class="btn-sm btn-primary"><i class="fa fa-search"></i></button>
                                        </td>
                                    </tr>
                                </form>
                                <?php
                                if (!empty($program)) {
                                    $num = 0;
                                    if (isset($_GET['page'])) {
                                        $num = 20 * ($_GET['page'] - 1);
                                    }
                                    foreach ($program as $pm_data) {
                                        $num++;
                                        ?>
                                        <tr>
                                            <td><?php echo $num; ?></td>
                                            <td><?php echo $pm_data->client_name; ?></td>
                                            <td><?php
                                                if ($pm_data->project_type == '1') {
                                                    echo "<strong style='color:#ED1212;'>Content Invoice</strong><br/>";
                                                }
                                                echo wordwrap($pm_data->project_title, 75, "<br/>");
                                                ?></td>
                                            <td><?php echo $pm_data->location_of_training; ?></td>
                                            <td><?php echo date_formate_short($pm_data->training_start_date); ?></td>
                                            <td><?php
                                                if ($pm_data->client_invoice_raised == 'N') {
                                                    ?>
                                                    <span class="label label-warning">Pending</span>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <span class="label label-success">Generated</span>
                                                    <?php
                                                }
                                                ?></td>
                                            <td>
                                                <?php
                                                if ($pm_data->project_type == '0') {
                                                    if ($pm_data->trainer_invoice_flag != '1') {
                                                         ?>
                                                    <span class="label label-warning">Pending</span>
                                                    <?php
                                                    }
                                                    if ($pm_data->trainer_invoice_flag == '1') {
                                                        ?>
                                                    <span class="label label-success">Generated</span>
                                                    <?php
                                                    }
                                                } else {
                                                    echo 'NA';
                                                }
                                                ?>

                                            </td>
                                            <td><?php echo $pm_data->pstatus; ?></td>
                                            <td><a href="<?php echo base_url(); ?>projectmanager/program_details/<?php echo $pm_data->project_id; ?>?page=<?php echo $page; ?>" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a></td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="8">No Record found</td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                            <div class="text-right" style="font-size:16px; padding: 10px;">
                                <?php
                                if ($page > 1) {
                                    $next = $page + 1;
                                    $prev = $page - 1;
                                    ?>
                                    <a href="<?php echo base_url(); ?>projectmanager/past_programs/?page=1"><i class="fa fa-backward" style="font-size:12px;"></i> First</a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>projectmanager/past_programs/?page=<?php echo $prev; ?>"><i class="fa fa-caret-left"></i> Prev</a>
                                    <?php
                                }
                                if ($page < $totalPage) {
                                    ?>
                                    &nbsp;&nbsp;<a href="<?php echo base_url(); ?>projectmanager/past_programs/?page=<?php echo $next; ?>">Next <i class="fa fa-caret-right"></i> </a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>projectmanager/past_programs/?page=<?php echo $totalPage; ?>">Last <i class="fa fa-forward" style="font-size:12px;"></i> </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="import-wrap" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-arrow-right"></i> Import Excel Sheet of Program</h4>
                    </div>
                    <div class="modal-body">
                        <form action="" method="POST"  enctype="multipart/form-data" id="import-form">
                            <input type="hidden" value="" id="cid" name="cid"/>
                            <div class="form-group col-md-8 col-md-offset-2">
                                <select class="form-control" id="client" name="client">
                                    <option value=""> - Select Client -</option>
                                    <?php
                                    if (!empty($client)) {
                                        foreach ($client as $cl_data) {
                                            ?>
                                            <option value="<?php echo $cl_data->client_id; ?>"> <?php echo $cl_data->client_name; ?> </option>
                                            <?php
                                        }
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group  col-md-8 col-md-offset-2">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Upload File <sup>*</sup></span>
                                    <input type="file" name="content" accept=".xlsx, .xls, .csv" class="form-control">
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-8 col-md-offset-2 text-center">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.import-btn').click(function (e) {
                    e.preventDefault();
                    $('#import-wrap').modal('show');
                });


                $("#import-form").validate({
                    rules: {
                        client: "required",
                        content: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });
            });
        </script>

    </body>
</html>