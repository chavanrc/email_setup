<?php
$CI = & get_instance();
$CI->load->model('projectmanager_model');
?>
<div class="nav-side-menu no-print">
    <div class="brand"><img src="<?php echo base_url(); ?>assets/images/wagons-logo.png"/></div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

    <div class="menu-list">

        <ul id="menu-content" class="menu-content collapse out">
            <li <?php if ($page_url == 'Dashboard') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>projectmanager/">
                    <i class="fa fa-dashboard"></i> Dashboard
                </a>
            </li>

            <li <?php if ($page_url == 'Clients') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>projectmanager/clients">
                    <i class="fa fa-users "></i> Clients
                </a>
            </li>
            
            

            <li <?php if ($page_url == 'Trainers') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>projectmanager/trainers">
                    <i class="fa fa-graduation-cap"></i> Trainers
                </a>
            </li>

	   <li <?php if ($page_url == 'Add Program') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>projectmanager/content_program">
                    <i class="fa fa-road"></i> Content Development
                </a>
            </li>
            <li <?php if ($page_url == 'Upcoming Programs') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>projectmanager/upcoming_programs">
                    <i class="fa fa-calendar-plus-o"></i> Upcoming Programs
                </a>
            </li>
            <li <?php if ($page_url == 'Past Programs') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>projectmanager/past_programs">
                    <i class="fa fa-calendar-check-o"></i> Past Programs
                </a>
            </li>
            <!--<li <?php //  if ($page_url == 'Content') { ?> class="active" <?php // } ?>>
                <a href="<?php //echo base_url(); ?>projectmanager/contents">
                    <i class="fa fa-file-text-o"></i> Content
                </a>
            </li> -->
            <li <?php if ($page_url == 'Payments') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>projectmanager/payments">
                    <i class="fa fa-money"></i> Trainer Invoices
                </a>
            </li>
            <li <?php if ($page_url == 'Payments') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>projectmanager/client_invoice">
                    <i class="fa fa-money"></i> Client Invoices
                </a>
            </li>
            <li <?php if ($page_url == 'Content') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>projectmanager/content_modules">
                    <i class="fa fa-file-text-o"></i> Content Modules
                </a>
            </li>
	     
        </ul>
    </div>
</div>

<div class="page_spin">
    <br/>
    <div class="spin_icon">
        <i class="fa fa-spinner fa-spin"></i><br/>
        <span>One moment ...</span>
    </div>
</div>
