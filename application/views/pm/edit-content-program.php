<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Trainer Support | Add Program</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/datepicker/jquery.datetimepicker.css">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Content Development</h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-plus"></i> Update Content Development Details</h2>
                        </div>
                        <div class="panel-body">
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-program-form">
                                <input type="hidden" value="<?php echo $program[0]->project_id; ?>" name="pid">
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Client <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="client">
                                            <option value=""> - Select -</option>
                                            <?php
                                            if (!empty($client)) {
                                                foreach ($client as $cl_data) {
                                                    ?>
                                                    <option value="<?php echo $cl_data->client_id; ?>" <?php if($program[0]->client_id==$cl_data->client_id){ echo 'selected'; } ?> > <?php echo $cl_data->client_name; ?> </option>
                                                    <?php
                                                }
                                            }
                                            ?>

                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Content Title <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="pname" value="<?php echo $program[0]->project_title; ?>" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Location <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" name="location" value="<?php echo $program[0]->location_of_training; ?>" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>

                                <div class="clearfix"></div>

                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Submission Date
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="date" id="sdate" name="sdate" value="<?php
                                        $sdate = explode(' ', $program[0]->training_start_date);
                                        echo $sdate[0];
                                        ?>"  placeholder="yy-mm-dd" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>


                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Duration in Days <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="number"  name="duration" value="<?php echo $program[0]->training_duration; ?>" class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12"> </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label><input type="checkbox" <?php if($program[0]->half_day=='1'){ echo 'checked'; } ?> name="halfday" value='1'>Half Day</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Charges Agreed With Client / Day <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-6">
                                        <input type="number" value="<?php echo $program[0]->client_charges; ?>"  name="client_charges"  class="form-control col-md-7 col-xs-12">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Participants Level <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" name="participant_level">
                                            <option value=""> - Select -</option>
                                            <option value="Beginners" <?php
                                            if ($program[0]->participant_level == 'Beginners') {
                                                echo 'selected';
                                            }
                                            ?>> Beginners </option>
                                            <option value="Juniors" <?php
                                            if ($program[0]->participant_level == 'Juniors') {
                                                echo 'selected';
                                            }
                                            ?>> Juniors </option>
                                            <option value="Intermediate" <?php
                                            if ($program[0]->participant_level == 'Intermediate') {
                                                echo 'selected';
                                            }
                                            ?>> Intermediate </option>
                                            <option value="Advanced" <?php
                                            if ($program[0]->participant_level == 'Advanced') {
                                                echo 'selected';
                                            }
                                            ?>> Advanced </option>
                                            <option value="Highly Experienced" <?php
                                            if ($program[0]->participant_level == 'Highly Experienced') {
                                                echo 'selected';
                                            }
                                            ?>> Highly Experienced </option>
                                            <option value="Above 40 years Staff" <?php
                                            if ($program[0]->participant_level == 'Above 40 years Staff') {
                                                echo 'selected';
                                            }
                                            ?>> Above 40 years Staff </option>
                                            <option value="Unskilled Labor" <?php
                                            if ($program[0]->participant_level == 'Unskilled Labor') {
                                                echo 'selected';
                                            }
                                            ?>> Unskilled Labor </option>
                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>

                            </form>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/additional-methods.js"></script>

        <script type="text/javascript">

            $(document).ready(function () {


                $("#add-program-form").validate({
                    rules: {
                        pname: "required",
                        client: "required",
                        location: "required",
                        participants_type: "required",
                        client_name: "required",
                        duration: {
                            required: true,
                            number: true
                        },
                        participant_level: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });


            });

        </script>

    </body>
</html>