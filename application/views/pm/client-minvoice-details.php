<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Project Manager | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Invoice</h3>
                    </div>
                    <div class="page-title title-right text-right no-print">
                        
                        
                        <a href="<?php echo base_url(); ?>projectmanager/client_invoice" class="create-invoice-btn btn btn-danger">Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-10 content-page">
                    <div class="panel panel-default">
                    <div class="panel-body">
                            <table class="table table-bordered">
                                <tr style="font-size:20px;">
                                    <td colspan="3" style="text-align: center;"><h4 style="font-size:20px;">Tax Invoice</h4></td>
                                </tr>
                                <tr>
                                    <td rowspan="5" style="width:500px;">To, <br/>
                                        <?php echo $client[0]->client_name; ?><br/>
                                        <?php
                                        if (!empty($invoice)) {
                                            echo $invoice[0]->ci_gst_address;
                                        }
                                        ?>
                                    </td>
                                    <td>Invoice Number</td>
                                    <td><?php echo $invoice[0]->ci_number; ?></td>
                                </tr>
                                <tr>
                                    <td>Invoice Date</td>
                                    <td><?php echo date_formate_short($invoice[0]->ci_date); ?></td>
                                </tr>
                                <tr>
                                    <td>Ref. No</td>
                                    <td><?php
                                        if (!empty($invoice[0]->ci_refNumber)) {
                                            echo $invoice[0]->ci_refNumber;
                                        } else {
                                            echo 'NA';
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>Name</td>
                                    <td><?php echo $client[0]->person1; ?></td>
                                </tr>
                                <tr>

                                    <td>Email id</td>
                                    <td><?php echo $client[0]->email1; ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>State Code :  </strong><?php
                                        if (!empty($invoice)) {
                                            echo $invoice[0]->ci_state_code;
                                        }
                                        ?> 
                                    </td>
                                    <td>PO. No</td>
                                    <td><?php
                                        if (!empty($invoice[0]->ci_poNumber)) {
                                            echo $invoice[0]->ci_poNumber;
                                        } else {
                                            echo 'NA';
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>
                                        <strong>GST NO: </strong> <?php
                                        if (!empty($invoice)) {
                                            echo $invoice[0]->ci_gst;
                                        }
                                        ?>
                                    </td>
                                    <td>SAC Code</td>
                                    <td>999293</td>
                                </tr>
                            </table>
                            <?php
                            $total = 0;
                            $no = 0;
                            ?>
                            <table class="table table-bordered text-center">
                                <tr style="font-weight: bold;">
                                    <td>Sl. No</td>
                                    <td>Description</td>
                                    <td>Name of the Program</td>
                                    <td>Month of the Program</td>
                                    <td>Training Location</td>
                                    <td>No. of Days</td>
                                    <td>Amount</td>
                                </tr>
                                <tr>
                                    <td>1</td>
                                    <td><?php echo $invoice[0]->ci_desc; ?></td>
                                    <td><?php echo $invoice[0]->ci_project; ?></td>
                                    <td><?php
                                        $dt = DateTime::createFromFormat('!m', $invoice[0]->ci_month);
                                        echo $dt->format('F').'-'.$invoice[0]->ci_year;
                                        ?>
                                    </td>
                                    <td>As per Voucher</td>
                                    <td><?php echo $invoice[0]->ci_days; ?></td>
                                    <td><?php echo $total = $invoice[0]->ci_charge; ?></td>
                                </tr>
                                <tr style="font-size:18px;">
                                    <td colspan="6" class="text-right">Gross Total</td>
                                    <td style="width:100px; text-align:right;"><?php echo number_format($total, 2); ?></td>
                                </tr>

                            </table>
                            <?php
                            $company = $CI->admin_model->get_single_company($invoice[0]->ci_company);
                            $cmState = $company[0]->gst_state_code;
                            $cState = $company[0]->gst_state_code;
                            if (!empty($invoice)) {
                                $cState = $invoice[0]->ci_state_code;
                            }
                            ?>
                            <table class="table table-bordered">
                                <tr>
                                    <td>PAN No.</td>
                                    <td><?php echo $company[0]->pan_no; ?></td>
                                    <td class="text-right">CGST On Professional Fees @9%</td>
                                    <td style="width:100px; text-align:right;">
                                        <?php
                                        if ($cState == $cmState) {
                                            $cgst = round((9 / 100) * $total);
                                            echo number_format($cgst, 2);
                                        } else {
                                            echo '-';
                                        }
                                        ?>
                                    </td>
                                </tr>
                                <tr>
                                    <td>GSTIN</td>
                                    <td><?php echo $company[0]->gstn; ?></td>
                                    <td class="text-right">SGST On Professional Fees @9%</td>
                                    <td style="text-align:right;"><?php
                                        if ($cState == $cmState) {
                                            $sgst = round((9 / 100) * $total);
                                            echo number_format($sgst, 2);
                                        } else {
                                            echo '-';
                                        }
                                        ?></td>
                                </tr>
                                <tr>
                                    <td>TAN No</td>
                                    <td><?php echo $company[0]->tan_no; ?></td>
                                    <td class="text-right">IGST On Professional Fees @18%</td>
                                    <td style="text-align:right;">
                                        <?php
                                        if ($cState != $cmState) {
                                            $gstamt = round((18 / 100) * $total);
                                            echo number_format($gstamt, 2);
                                        } else {
                                            $gstamt = $cgst + $sgst;
                                            echo '-';
                                        }
                                        $gTotal = $total + $gstamt;
                                        ?>
                                    </td>
                                </tr>
                                <tr style="font-size:20px;">
                                    <td colspan="2"></td>
                                    <td class="text-right">Total</td>
                                    <td style="text-align:right;">
                                        <?php
                                        $upAmount = number_format($gTotal, 2);
                                        echo $upAmount;
                                        ?>
                                    </td>
                                </tr>
                            </table>
                            <table class="table table-bordered">
                                <tr>
                                    <td>Beneficiary</td>
                                    <td><?php echo $company[0]->company_name; ?></td>
                                    <td rowspan="5" style="text-align: center;">For, <?php echo $company[0]->company_name; ?>
                                        <br/><br/><br/><br/><br/><br/><br/><br/>
                                        Authorized Signatory
                                    </td>
                                </tr>
                                <tr>
                                    <td>Account No</td>
                                    <td><?php echo $company[0]->account_no; ?></td>
                                </tr><tr>
                                    <td>IFSC Code</td>
                                    <td><?php echo $company[0]->ifsc_no; ?></td>
                                </tr>
                                <tr>
                                    <td>Bank & Branch</td>
                                    <td><?php echo $company[0]->bank_branch; ?></td>
                                </tr>
                                <tr>
                                    <td class="text-center" colspan="2">
                                        <strong>
                                            * Thanking you and assuring our best services at all
                                        </strong>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <?php
                    if (!empty($payment)) {
                        ?>
                        <div class="panel panel-default no-print">
                            <div class="panel-body">
                                <table class="table table-bordered">
                                    <tr>
                                        <td colspan="5"><h4>Payment Details</h4></td>
                                    </tr>
                                    <tr>
                                        <td>Amount</td>
                                        <td>Date</td>
                                        <td>Payment id</td>
                                        <td>Payment Type</td>
                                        <td>Payment Advice</td>
                                    </tr>
                                    <?php foreach ($payment as $pdata) {
                                        ?>
                                        <tr>
                                            <td><?php echo $pdata->cp_amt; ?></td>
                                            <td><?php echo date_formate_short($pdata->cp_date); ?></td>
                                            <td><?php echo $pdata->cp_pay_id; ?></td>
                                            <td><?php
                                                if ($pdata->cp_type == '1') {
                                                    echo 'Professional Fees';
                                                } else {
                                                    echo 'Debit Note';
                                                }
                                                ?></td>
                                            <td>
                                                <?php if ($pdata->cp_type == '1') { 
                                                    if($invoice[0]->ci_pay_advice=='1'){
                                                        echo 'Received';
                                                    } else if($invoice[0]->ci_pay_advice=='0'){
                                                        echo 'Pending';
                                                    }
                                                    
                                                } ?>
                                                    
                                            </td>
                                            
                                        </tr>
                                    <?php }
                                    ?>

                                </table>
                            </div>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
        </div>


        <?php include 'js_files.php'; ?>
        
        <script type="text/javascript">
            $(document).ready(function () {
                
                $("#pay-form").validate({
                    rules: {
                        amt: "required",
                        date: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });
                
                $('.make-payment-btn').click(function(e){
                   $('#pay_wraper').modal('show');
                });


                $('.confirm-btn').click(function (e) {
                    e.preventDefault();
                    var cid = $(this).attr('inid');
                    var f = confirm("Are you sure want confirm ?");
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "cid=" + cid + "&page=confirm_client_invoice";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

            });
        </script>

    </body>
</html>