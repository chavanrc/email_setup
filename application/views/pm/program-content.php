<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Project Manager | Engagement Requests</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Program</a>
                    </div>
                    <?php
                    $eng_count = $CI->projectmanager_model->count_engagement_request($pid);
                    ?>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="<?php echo base_url(); ?>projectmanager/program_details/<?php echo $pid; ?>"program_details><i class="fa fa-info"></i> Details</a></li>
                            <?php if ($eng_count > 0) { ?>
                                <li ><a href="<?php echo base_url(); ?>projectmanager/engagement_requests/<?php echo $pid; ?>" ><i class="fa fa-paper-plane"></i> Engagement Requests (<strong style="color:#0000FF;"><?php echo $eng_count; ?></strong>)</a></li>
                            <?php } ?>
                            <li class="active"><a href="#"><i class="fa fa-file-text"></i> Content Setting</a></li>
                            <li><a href="<?php echo base_url(); ?>projectmanager/program_expenses/<?php echo $pid; ?>"><i class="fa fa-money"></i> Program Expenses</a></li>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Content Settings For the Training Program</h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <?php
                        $pretest = 0;
                        $posttest = 0;
                        $prep = '';
                        $handbook = '';
                        $charge = '';
                        $test = 0;
                        $pmodule = "";
                        $content_under_update = "";
                        $handbook_content = "";
                        $handbook_qnt = "";
                        $handbook_charge = "";
                        $handbook_cost = "";
                        $handbook_desc = "";
                        $handbook_courier = "";
                        $content_cost = "";

                        if (!empty($pcontent)) {
                            $pretest = $pcontent[0]->pre_test;
                            $posttest = $pcontent[0]->post_test;
                            $prep = $pcontent[0]->content_preperation;
                            $handbook = $pcontent[0]->participant_handbook;
                            $charge = $pcontent[0]->content_charges;
                            $test = $pcontent[0]->physical_paper_test;
                            $content_under_update = $pcontent[0]->content_under_update;
                            $pmodule = $pcontent[0]->module;
                            $handbook_content = $pcontent[0]->handbook_content;
                            $handbook_qnt = $pcontent[0]->handbook_qnt;
                            $handbook_charge = $pcontent[0]->handbook_charge;
                            $handbook_cost = $pcontent[0]->handbook_cost;
                            $handbook_desc = $pcontent[0]->handbook_desc;
                            $handbook_courier = $pcontent[0]->handbook_courier;
                            $content_cost = $pcontent[0]->content_cost;
                        }
                        ?>
                        <div class="panel-body">
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-program-form">
                                <input type="hidden" name="pid" value="<?php echo $pid; ?>"/>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Assessment Tests <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <label style="margin-right: 20px;"><input type="checkbox" <?php
                                            if ($pretest == '1') {
                                                echo 'checked';
                                            }
                                            ?>  name="pre_test" value='1'>Pre Assessment Applicable</label>
                                        <label style="margin-right: 20px;"><input type="checkbox" <?php
                                            if ($posttest == '1') {
                                                echo 'checked';
                                            }
                                            ?> name="post_test" value='1'>Post Assessment Applicable</label>

                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Content Module <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       	<select class="form-control" name="content_module" id="content_module">
                                            <option value=""> - Select -</option>
                                            <?php
                                            if (!empty($modules)) {
                                                foreach ($modules as $md) {
                                                    ?>
                                                    <option value="<?php echo $md->module_name; ?>" <?php if ($md->module_name === $pmodule) { ?> selected <?php } ?>><?php echo $md->module_name; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Content Preperation <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       	<select class="form-control" name="content_preperation" id="content_preperation">
                                            <option value=""> - Select -</option>
                                            <option value="Already exist" <?php
                                            if ($prep == 'Already exist') {
                                                echo 'selected';
                                            }
                                            ?> > Already Exists</option>
                                            <option value="Prepare by CM" <?php
                                            if ($prep == 'Prepare by CM') {
                                                echo 'selected';
                                            }
                                            ?> > Prepare by Content Manager</option>
                                            <option value="Client will provide" <?php
                                            if ($prep == 'Client will provide') {
                                                echo 'checked';
                                            }
                                            ?> > Client Will Provide </option>


                                        </select>
                                        <div class="clearfix"></div>
                                        <div id="c_up" <?php if ($prep == 'Already exist') { ?> style="display:inline;" <?php } else { ?> style="display:none;"<?php } ?>>
                                            <input type="checkbox" value="Y" id="content_under_update" name="content_under_update" <?php if ($content_under_update == "Y") echo "checked"; ?>> Content Requires Updation ?
                                        </div>
                                    </div>
                                </div>


                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Participant Handbook <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" id="participant_handbook" name="participant_handbook">
                                            <option value=""> - Select -</option>
                                            <option value="Not Required" <?php
                                            if ($handbook == 'Not Required') {
                                                echo 'selected';
                                            }
                                            ?> > Not Required</option>
                                            <option value="Print + Dispatch By Wagons" <?php
                                            if ($handbook == 'Print + Dispatch By Wagons') {
                                                echo 'selected';
                                            }
                                            ?> > Print + Dispatch By Wagons</option>
                                            <option value="Print By Client" <?php
                                            if ($handbook == 'Print By Client') {
                                                echo 'selected';
                                            }
                                            ?> > Print By Client </option>

                                        </select>

                                    </div>
                                </div>
                                <div class="handbookWraper" <?php if($handbook != 'Print + Dispatch By Wagons'){ ?> style="display: none;" <?php } ?>>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Handbook Content <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control" name="handbook_content">
                                                <option value=""> - Select -</option>
                                                <option value="Already exist" <?php
                                                if ($handbook_content == 'Already exist') {
                                                    echo 'selected';
                                                }
                                                ?>>Already exist</option>
                                                <option value="Prepare / Update By CM" <?php
                                                if ($handbook_content == 'Prepare / Update By CM') {
                                                    echo 'selected';
                                                }
                                                ?>>Prepare / Update By CM</option>
                                            </select>

                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Notes for Printing<span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea class="form-control" name="handbook_desc"><?php echo $handbook_desc; ?></textarea>
											<span>Mention type of paper, binding, cover type etc..</span>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Print Quantity (No. of Copies)<span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" value="<?php echo $handbook_qnt; ?>" placeholder="No. Copeis" name="handbook_qnt"/>

                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12"> Handbook Printing Charges <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control" id="handbook_charge" name="handbook_charge">
                                                <option value=""> - Select -</option>
                                                <option value="NA" <?php
                                                if ($handbook_charge == 'NA') {
                                                    echo 'selected';
                                                }
                                                ?> > Not Applicable </option>
                                                <option value="Absorb by Wagons" <?php
                                                if ($handbook_charge == 'Absorb by Wagons') {
                                                    echo 'selected';
                                                }
                                                ?> > Wagons To Absorb </option>
                                                <option value="Bill Client" <?php
                                                if ($handbook_charge == 'Bill Client') {
                                                    echo 'selected';
                                                }
                                                ?> > Wagons to Bill Client </option>


                                            </select>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="form-group hcostWraper" <?php if ($handbook_charge != 'Bill Client') { ?>style="display: none;"<?php } ?> >
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Printing Cost Limit<span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" class="form-control" value="<?php echo $handbook_cost; ?>" placeholder="Total Cost" name="handbook_cost"/>

                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Dispatch Location <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea class="form-control" name="handbook_courier"><?php echo $handbook_courier; ?></textarea>

                                        </div>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Content Development Charges <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <select class="form-control" id="content_charges" name="content_charges">
                                            <option value=""> - Select -</option>
                                            <option value="NA" <?php
                                            if ($charge == 'NA') {
                                                echo 'selected';
                                            }
                                            ?> > Not Applicable </option>
                                            <option value="Absorb by Wagons" <?php
                                            if ($charge == 'Absorb by Wagons') {
                                                echo 'selected';
                                            }
                                            ?> > Wagons To Absorb </option>
                                            <option value="Bill Client" <?php
                                            if ($charge == 'Bill Client') {
                                                echo 'selected';
                                            }
                                            ?> > Wagons to Bill Client </option>


                                        </select>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group ccostWraper" <?php if ($charge != 'Bill Client') { ?>style="display: none;"<?php } ?>>
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Content Charges Limit <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                        <input type="text" class="form-control" value="<?php echo $content_cost; ?>" placeholder="Total Cost" name="content_cost"/>

                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">Phyisical Paper Test? <span class="required">*</span>
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12">
                                       	<label style="margin-right: 20px;"><input type="checkbox" <?php
                                            if ($test == '1') {
                                                echo 'checked';
                                            }
                                            ?> name="physical_paper_test" value='1'>Yes</label>
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>



        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {

                $("#content_preperation").change(function () {

                    if ($(this).val() == "Already exist")
                    {
                        $("#c_up").show();
                        $('#content_under_update').prop('checked', false); // Unchecks it

                    }
                    else
                    {
                        $("#c_up").hide();
                        $('#content_under_update').prop('checked', false); // Unchecks it
                    }
                });

                $('#content_charges').change(function (e) {
                    var ph = $(this).val();
                    if (ph == 'Bill Client') {
                        $('.ccostWraper').show();
                    } else {
                        $('.ccostWraper').hide();
                    }
                });
                
                $('#handbook_charge').change(function (e) {
                    var ph = $(this).val();
                    if (ph == 'Bill Client') {
                        $('.hcostWraper').show();
                    } else {
                        $('.hcostWraper').hide();
                    }
                });
                
                $('#participant_handbook').change(function (e) {
                    var ph = $(this).val();
                    if (ph == 'Print + Dispatch By Wagons') {
                        $('.handbookWraper').show();
                    } else {
                        $('.handbookWraper').hide();
                    }
                });

                $("#add-program-form").validate({
                    rules: {
                        content_charges: "required",
                        participant_handbook: "required",
                        content_preperation: "required",
                        handbook_content: "required",
                        handbook_qnt: "required",
                        handbook_charge: "required",
                        handbook_cost: "required",
                        content_cost: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        assign_trainer();
                    }
                });

            });
        </script>

    </body>
</html>