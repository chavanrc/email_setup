<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <style>
            *{
                font-family: Helvetica, Arial, sans-serif;
            }
            .bodyWraper{
                width: 700px;
                //padding:15px;
                border:solid 1px #ddd;
                border-radius: 2px;
            }
        </style>
        <style type="text/css" media="screen">
            @media screen {
                * {
                    font-family: 'Oxygen', 'Helvetica Neue', 'Arial', 'sans-serif' !important;
                }
            }
        </style>
        <style type="text/css" media="only screen and (max-width: 480px)">
            /* Mobile styles */
            @media only screen and (max-width: 480px) {
                .bodyWraper{
                    width: 100%;
                }
            }
        </style>   
    </head>
    <body>
        <div class="bodyWraper" style="width: 700px; border:solid 1px #ddd; border-radius: 2px;">
            <div style="padding:15px; background: #cecece;">
            <table cellpadding="0" cellspacing="0" width="100%">
                <tr>
                    <td style="vertical-align: middle;">
                        <a href="<?php echo base_url(); ?>">
                            <img height="40" src="<?php echo base_url(); ?>assets/images/wagons-logo.png" alt="logo"/>
                        </a>
                    </td>
                    <td  style="color: #4d4d4d; text-align: right; vertical-align: middle;">
                        <a href=""><img width="40" height="40" src="http://s3.amazonaws.com/swu-filepicker/k8D8A7SLRuetZspHxsJk_social_08.gif" alt="twitter" /></a>
                        <a href=""><img width="40" height="40" src="http://s3.amazonaws.com/swu-filepicker/LMPMj7JSRoCWypAvzaN3_social_09.gif" alt="facebook" /></a>
                        <a href=""><img width="40" height="40" src="http://s3.amazonaws.com/swu-filepicker/hR33ye5FQXuDDarXCGIW_social_10.gif" alt="rss" /></a>
                    </td>
                </tr>
            </table>
            </div>
            <div style="padding:15px;">
                <div style="font-size: 18px; text-align: center; margin-top: 10px;">
                    Program Edit Request
                </div>
                <div style="font-size: 14px; margin-top: 30px; line-height: 21px;">
                    Hi,<br/><br/>
                    Project Manager needs your approval to edit <?php echo $data['project'] ?> <br/>Client :  <?php echo $data['client'] ?> client. <br/>
                    Program Date : <?php echo $data['pdate'] ?> <br/><br/>
                    Edit Reason : <?php echo $data['reason'] ?>
                </div>
                
                <div style=" margin-top: 40px; text-align: center; margin-bottom: 40px;">
                    
                    <a href='http://sandbox.wagonslearning.com/admin/approve_request/<?php echo $data['rid']; ?>' style="padding: 10px; background: #337ab7; color: #fff; font-size: 15px; text-decoration: none; border-radius: 2px;">Approve Request</a>
                       
                </div>
            </div>
        </div>
        <div style="padding: 10px; font-size: 12px;">
            © Wagons Learning Pvt. Ltd. 2014-2017 | +91 814 900 6055 | contact@wagonslearning.com
        </div>
    </body>
</html>