<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Trainers</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Update</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <?php
                        if ($_GET['type'] == 'Courier') {
                            ?>
                            <a href="<?php echo base_url(); ?>support/program_details/<?php echo $exp[0]->cr_project; ?>" class="btn btn-danger pull-right" style="margin-top: 10px;">Back</a>
                        <?php } ?>
                        <?php
                        if ($_GET['type'] == 'stay') {
                            ?>
                            <a href="<?php echo base_url(); ?>support/program_details/<?php echo $exp[0]->project_id; ?>" class="btn btn-danger pull-right" style="margin-top: 10px;">Back</a>
                        <?php } 
                        if ($_GET['type'] == 'travel') {
                            ?>
                            <a href="<?php echo base_url(); ?>support/program_details/<?php echo $exp[0]->project_id; ?>" class="btn btn-danger pull-right" style="margin-top: 10px;">Back</a>
                        <?php }
                        ?>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
            <div class="row" style="margin: 0px;">

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" >Update <?php
                                if ($_GET['type'] == 'Courier') {
                                    echo 'Courier Details';
                                }
                                if ($_GET['type'] == 'stay') {
                                    echo 'Stay Details';
                                }
                                if ($_GET['type'] == 'travel') {
                                    echo 'Stay Details';
                                }
                                ?>  </h2>

                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php if ($_GET['type'] == 'Courier') { ?>
                                <div class="col-md-6">
                                    <form action="" method="POST"  enctype="multipart/form-data" id="ship-form">

                                        <div class="modal-body">
                                            <input type="hidden" name="cr" value="<?php echo $exp[0]->cr_id; ?>"/>
                                            <input type="hidden" name="project" value="<?php echo $exp[0]->cr_project; ?>"/>
                                            <input type="hidden" name="handbook" id="pTypeInput" value="Participant Handbook"/>
                                            <div class="form-group col-md-12" id="pTypeWraper">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1"> Expense Type <sup>*</sup></span>
                                                    <select class="form-control" id="pType" name="pType">
                                                        <option value="">- Select -</option>
                                                        <option value="Courier Charges" <?php
                                                        if ($exp[0]->cr_type == 'Courier Charges') {
                                                            echo 'selected';
                                                        }
                                                        ?> >Courier Shipment Expenses</option>
                                                        <option value="Food Expenses" <?php
                                                        if ($exp[0]->cr_type == 'Food Expenses') {
                                                            echo 'selected';
                                                        }
                                                        ?> >Food Expenses</option>
                                                        <option value="Lamination Charges" <?php
                                                        if ($exp[0]->cr_type == 'Lamination Charges') {
                                                            echo 'selected';
                                                        }
                                                        ?> >Lamination Expenses</option>
                                                        <option value="Misc. Charges" <?php
                                                        if ($exp[0]->cr_type == 'Misc. Charges') {
                                                            echo 'selected';
                                                        }
                                                        ?> >Misc. Expenses</option>
                                                        <option value="Participant Handbook" <?php
                                                        if ($exp[0]->cr_type == 'Participant Handbook') {
                                                            echo 'selected';
                                                        }
                                                        ?> >Printing Expenses</option>
                                                        <option value="Participant Handbook" <?php
                                                        if ($exp[0]->cr_type == 'Participant Handbook') {
                                                            echo 'selected';
                                                        }
                                                        ?> >Participant Handbook Courier</option>
                                                        <option value="Stay Charges" <?php
                                                        if ($exp[0]->cr_type == 'Stay Charges') {
                                                            echo 'selected';
                                                        }
                                                        ?> >Stay Charges</option>	
                                                        <option value="Stationary Purchase" <?php
                                                        if ($exp[0]->cr_type == 'Stationary Purchase') {
                                                            echo 'selected';
                                                        }
                                                        ?> >Stationary Purchase</option>	
                                                        <option value="Training Props" <?php
                                                        if ($exp[0]->cr_type == 'Training Props') {
                                                            echo 'selected';
                                                        }
                                                        ?> >Training Material Shipment Expenses</option>
                                                        <option value="Traveling Charges" <?php
                                                        if ($exp[0]->cr_type == 'Traveling Charges') {
                                                            echo 'selected';
                                                        }
                                                        ?> >Traveling Expenses</option>
                                                        <option value="Xerox Expenses" <?php
                                                        if ($exp[0]->cr_type == 'Xerox Expenses') {
                                                            echo 'selected';
                                                        }
                                                        ?> >Xerox Expenses</option>

                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1"> Date of Expense<sup>*</sup></span>

                                                    <input type="text" name="cr_ship_date" value="<?php echo $exp[0]->cr_ship_date; ?>" placeholder="YYYY-MM-DD"  class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1"> Vendor <sup>*</sup></span>
                                                    <input type="text" name="cname" value="<?php echo $exp[0]->cr_cname; ?>" class="form-control" placeholder="Courier Company Name">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1"> Reference No.<sup>*</sup></span>
                                                    <input type="text" name="cid" value="<?php echo $exp[0]->cr_cid; ?>" class="form-control" placeholder="Courier Ref No.">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1"> Expense Amount (Rs.) <sup>*</sup></span>
                                                    <input type="text" name="camt" value="<?php echo $exp[0]->cr_amt; ?>" class="form-control" placeholder="Courier Expenses">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1">Notes<sup>*</sup></span>
                                                    <textarea id="comments" name="comments"  class="form-control"><?php echo $exp[0]->comments; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group  col-md-12 text-center">
                                            </div>
                                            <div class="clearfix"></div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" name="upload_img" class="btn btn-info" ><i class="fa fa-upload"></i> Update</button>
                                        </div>
                                    </form>
                                </div>
                                <?php
                            }
                            if ($_GET['type'] == 'stay') {
                                ?>
                                <div class="col-md-6">
                                    <form action="" method="POST"  enctype="multipart/form-data" id="add-stay-form1">

                                        <div class="modal-body">

                                            <input type="hidden" name="project" value="<?php echo $exp[0]->project_id; ?>"/>
                                            <input type="hidden" name="stay_user" value="<?php echo $this->session->userdata('user_code'); ?>"/>
                                            <input type="hidden" name="sid" value="<?php echo $exp[0]->id; ?>"/>

                                            <div class="form-group  col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1"> Hotel Name <sup>*</sup></span>
                                                    <input type="text" name="hotel" value="<?php echo $exp[0]->hotel_name; ?>" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group  col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1"> Check in Time <sup>*</sup></span>
                                                    <input type="text" name="checkin" value="<?php echo $exp[0]->checkin_time; ?>" class="form-control" placeholder="YYYY-MM-DD H:M">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1"> Check out Time <sup>*</sup></span>
                                                    <input type="text" name="checkout" value="<?php echo $exp[0]->checkout_time; ?>"  class="form-control" placeholder="YYYY-MM-DD H:M">
                                                </div>
                                            </div>

                                            <div class="form-group  col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1"> Amount <sup>*</sup></span>
                                                    <input type="text" name="stay_amt" value="<?php echo $exp[0]->expense_amount; ?>" class="form-control">
                                                </div>
                                            </div>

                                            <div class="form-group col-md-12">
                                                <label>Booking Screenshot </label> : <br/>
                                                <input type="file" name="screen"/>
                                                <input type="hidden" name="cfile" value="<?php echo $exp[0]->booking_screenshot; ?>"/>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1"> Payment Status <sup>*</sup></span>

                                                    <select name="pay_status" class="form-control">
                                                        <option value="">Select</option>
                                                        <option value="Paid" <?php
                                                        if ($exp[0]->pay_status == 'Paid') {
                                                            echo 'selected';
                                                        }
                                                        ?>>Already Paid</option>
                                                        <option value="Trainer to Pay" <?php
                                                        if ($exp[0]->pay_status == 'Trainer to Pay') {
                                                            echo 'selected';
                                                        }
                                                        ?>>Trainer to Pay</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1">Notes<sup>*</sup></span>
                                                    <textarea id="comments" name="comments"  class="form-control"><?php echo $exp[0]->comments; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group  col-md-12 text-center">
                                            </div>

                                            <div class="clearfix"></div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Update</button>
                                        </div>
                                    </form>
                                </div>
                                <?php
                            }

                            if ($_GET['type'] == 'travel') {
                                ?>
                                <div class="col-md-6">
                                    <form action="" method="POST"  enctype="multipart/form-data" id="add-travel-form1">

                                        <div class="modal-body">
                                            <input type="hidden" name="project" value="<?php echo $exp[0]->project_id; ?>"/>
                                            <input type="hidden" name="tid" value="<?php echo $exp[0]->id; ?>"/>
                                            <input type="hidden" name="travel_user" value="<?php echo $this->session->userdata('user_code'); ?>"/>

                                            <div class="form-group  col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1"> Date <sup>*</sup></span>
                                                    <input type="text" name="date" value="<?php echo $exp[0]->date; ?>" class="form-control" placeholder="YYYY-MM-DD">
                                                </div>
                                            </div>
                                            <div class="form-group  col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1"> From Location <sup>*</sup></span>
                                                    <input type="text" name="from" value="<?php echo $exp[0]->from_loc; ?>" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1"> To Location <sup>*</sup></span>
                                                    <input type="text" name="to" value="<?php echo $exp[0]->to_loc; ?>" class="form-control">
                                                </div>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1"> Mode <sup>*</sup></span>
                                                    <select class="form-control" name="mode">
                                                        <option value=""> - Select -</option>
                                                        <option value="Bus" <?php
                                                        if ($exp[0]->mode == 'Bus') {
                                                            echo 'selected';
                                                        }
                                                        ?> >Bus</option>
                                                        <option value="Cab" <?php
                                                        if ($exp[0]->mode == 'Cab') {
                                                            echo 'selected';
                                                        }
                                                        ?> >Cab</option>
                                                        <option value="Train" <?php
                                                        if ($exp[0]->mode == 'Train') {
                                                            echo 'selected';
                                                        }
                                                        ?> >Train</option>
                                                        <option value="Air" <?php
                                                        if ($exp[0]->mode == 'Air') {
                                                            echo 'selected';
                                                        }
                                                        ?> >Air</option>
                                                    </select>
                                                </div>
                                            </div>

                                            <div class="form-group  col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1"> Amount <sup>*</sup></span>
                                                    <input type="text" value="<?php echo $exp[0]->expense_amount; ?>" name="travel_amt" class="form-control">
                                                </div>
                                            </div>


                                            <div class="form-group col-md-12">
                                                <label>Booking Screenshot </label> : <br/>
                                                <input type="file" name="screen"/>
                                                <input type="hidden" value="<?php echo $exp[0]->screenshot; ?>" name="cfile"/>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon" id="sizing-addon1">Notes<sup>*</sup></span>
                                                    <textarea id="comments" name="comments" class="form-control"><?php echo $exp[0]->comments; ?></textarea>
                                                </div>
                                            </div>
                                            <div class="form-group  col-md-12 text-center">
                                            </div>
                                            <div class="clearfix"></div>

                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                                        </div>
                                    </form>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php include 'js_files.php'; ?>


        <script type="text/javascript">
            $(document).ready(function () {

                $("#ship-form").validate({
                    rules: {
                        cname: "required",
                        cr_ship_date: "required",
                        cid: "required",
                        camt: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

            });
        </script>

    </body>
</html>