<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin Support | Upcoming Programs</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Upcoming Programs</h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list"></i> Programs List </h2>
                        </div>
                        <div class="page-title title-right text-right">
                        <?php
                        $page = 1;
                        $next = 2;
                        $totalPage = 0;
                        if (isset($_GET['page'])) {
                            $page = $_GET['page'];
                        }
                        if (!empty($count)) {
                            $totalPage = ceil($count[0]->total / 20);
                            if ($count[0]->total > 20) {
                                ?>
                                <h4 class="pull-right" style="display: inline-block;"><?php echo (($page - 1) * 20) + 1; ?> - <?php
                                    if ($page < $totalPage) {
                                        echo $page * 20;
                                    } else {
                                        echo $count[0]->total;
                                    }
                                    ?> of <?php echo $count[0]->total; ?> </h4>
                                <?php
                            }
                        }
                        ?>
                    </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>PM</th>
                                        <th>Client</th>
					<th>Trainer</th>
                                        <th>Title</th>
                                        <th>Location</th>
                                        <th>Start Date</th>
                                        
                                        
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <form action="<?php echo base_url(); ?>support/upcoming_programs" method="POST">
                                    <tr>
                                        <td></td>
                                        <td colspan="2">
                                            <select class="form-control" id="pclient" name="pclient">
                                                <option value="All"> - All Client -</option>
                                                <?php
                                                if (!empty($client)) {
                                                    foreach ($client as $cl_data) {
                                                        ?>
                                                        <option value="<?php echo $cl_data->client_id; ?>" <?php
                                                        if (isset($_POST['pclient'])) {
                                                            if ($_POST['pclient'] == $cl_data->client_id) {
                                                                echo 'selected';
                                                            }
                                                        }
                                                        ?> > <?php echo $cl_data->client_name; ?> </option>
                                                                <?php
                                                            }
                                                        }
                                                        ?>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" placeholder="Program" name="title"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" placeholder="Location" name="location"/>
                                        </td>
                                        <td colspan="2">
                                            <input type="date" class="form-control" placeholder="Start Date" name="sdate"/>
                                        </td>
                                        <td colspan="3">
                                            <button class="btn-sm btn-primary">Search</button>
                                        </td>
                                    </tr>
                                </form>
                                    <?php
                                    if (!empty($program)) {
                                        $num = 0;
					if (isset($_GET['page'])) {
                                        $num = 20 * ($_GET['page'] - 1);
                                    }
                                        foreach ($program as $pm_data) {
					if($pm_data->is_active!='0')
                                        {
                                            $num++;
                                            $pm = $CI->admin_model->get_single_pm($pm_data->user_code);
					    $trainers = $CI->admin_model->get_assigned_trainers($pm_data->project_id);
					    
                                            ?>
                                    <tr <?php if($pm_data->is_active=='0'){ ?> class="label-warning" <?php } ?>>
                                                <td><?php echo $num; ?></td>
                                                <td><?php echo $pm[0]->name; ?></td>
                                                <td><?php echo $pm_data->client_name; ?></td>
                                                <td><?php if(!empty($trainers)) { echo $trainers[0]->name; } ?></td>
                                                <td><?php echo $pm_data->project_title; ?></td>
                                                <td><?php echo $pm_data->location_of_training; ?></td>
                                                <td><?php echo date_formate_short($pm_data->training_start_date); ?></td>
                                                
                                                
                                                <td><a href="<?php echo base_url(); ?>support/program_details/<?php echo $pm_data->project_id; ?>" class="btn btn-sm btn-info">View</a></td>
                                            </tr>
                                            <?php
                                      	  }
					}
                                    }
                                    else
                                    {
                                        ?>
                                            <tr>
                                                <td colspan="9">No Record found</td>
                                            </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <div class="text-right" style="font-size:16px; padding: 10px;">
                                <?php
                                if ($page > 1) {
                                    $next = $page + 1;
                                    $prev = $page - 1;
                                    ?>
                                    <a href="<?php echo base_url(); ?>support/upcoming_programs/?page=1"><i class="fa fa-backward" style="font-size:12px;"></i> First</a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>support/upcoming_programs/?page=<?php echo $prev; ?>"><i class="fa fa-caret-left"></i> Prev</a>
                                    <?php
                                }
                                if ($page < $totalPage) {
                                    ?>
                                    &nbsp;&nbsp;<a href="<?php echo base_url(); ?>support/upcoming_programs/?page=<?php echo $next; ?>">Next <i class="fa fa-caret-right"></i> </a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>support/upcoming_programs/?page=<?php echo $totalPage; ?>">Last <i class="fa fa-forward" style="font-size:12px;"></i> </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {
                
            });
        </script>

    </body>
</html>