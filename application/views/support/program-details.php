<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin Support | Program Details</title>
        <?php include 'css_files.php'; ?>
        <style>
            .panel-body{
                line-height:25px;
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Programs</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#"><i class="fa fa-info"></i> Details</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3><?php echo $program[0]->project_title; ?></h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>support/upcoming_programs" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>



                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-info"></i> Programs Details </h2>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">Program Title <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->project_title; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Client <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->client_name; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4"><strong>Program Status <span class="pull-right">:</span> </strong></div><div class="col-md-8"><strong><?php
                                    if ($program[0]->is_active == '1') {
                                        echo 'Active';
                                    } else {
                                        echo 'Canceled';
                                    }
                                    ?></strong></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Location <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->location_of_training; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Start Date <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->training_start_date; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Duration <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->training_duration; ?> Days <?php if($program[0]->half_day=="1") { echo "<strong>( Half Day )</strong>";  } ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Stay Arrangements <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->stay_arrangement; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Travel Arrangements <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->travel_arrangement; ?></div>
                            <div class="clearfix"></div>
                            <?php if (!empty($program[0]->venue)) { ?>
                                <div class="col-md-4">Venue  <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->venue; ?></div>
                                <div class="clearfix"></div>
                                <div class="col-md-4">SPOC <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->spoc; ?></div>
                                <div class="clearfix"></div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-graduation-cap"></i> Trainers </h2>
                        </div>
                        <div class="panel-body">

                            <?php
                            $tn = $this->projectmanager_model->get_program_trainers($program[0]->project_id);

                            if (!empty($tn)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Trainer</th>
                                            <th>Contact No.</th>
                                            <th>Date</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($tn as $tn_data) {
                                            $cIn = $CI->admin_model->getCheckin($program[0]->project_id, $tn_data->user_code);
                                            ?>
                                            <tr>
                                                <td><a href="/admin/trainer_single_profile/<?php echo $tn_data->user_code; ?>" target="_blank"><?php echo $tn_data->name; ?></a></td>
                                                <td><?php echo $tn_data->contact_number; ?></td>
                                                <td><?php echo date_formate_short($tn_data->program_date.' 00:00:00'); ?></td>

                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>


                <?php
                $cr = $this->projectmanager_model->get_courier($program[0]->project_id);
                $con = $this->projectmanager_model->get_content_setting($program[0]->project_id);
                if (!empty($con)) {
                    if ($con[0]->participant_handbook == 'Print + Dispatch By Wagons') {
                        ?>
                        <div class="col-md-12 content-page">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 class="panel-title" ><i class="fa fa-book"></i> Participants Handbook </h2>

                                    <a href="#" class="btn btn-sm btn-info pull-right btn-sm prop-status-btn1" style="margin-top:-24px;"><i class="fa fa-edit"></i> Update Handbook Shipment</a>

                                </div>
                                <div class="panel-body">
                                    <?php
                                    if ($msg == 'Courier') {
                                        ?>
                                        <div class="alert alert-success col-md-6 col-md-offset-3">
                                            Courier details updated Successfully.
                                        </div>
                                        <?php
                                    }
                                    ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Handbook Qnt.</th>
                                                <th>Payment</th>
                                                <th>Total Cost</th>
                                                <th>Courier Address</th>
                                                <th>Content</th>
                                                <?php
                                                if ($con[0]->handbook_status == '1') {
                                                    ?>
                                                    <th></th>
                                                    <?php
                                                }
                                                ?>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td><?php echo $con[0]->handbook_qnt; ?></td>
                                                <td><?php echo $con[0]->handbook_charge; ?></td>
                                                <td><?php echo $con[0]->handbook_cost; ?></td>
                                                <td><?php echo $con[0]->handbook_courier; ?></td>
                                                <td><?php echo $con[0]->handbook_content; ?></td>
                                                <?php
                                                if ($con[0]->handbook_status == '1') {
                                                    ?>
                                                <td style="width:70px;">
                                                        <a href="<?php echo base_url(); ?>assets/upload/content/support_download.php?cid=<?php echo $program[0]->client_id; ?>&mid=<?php echo $con[0]->module; ?>" class="btn-sm btn-success">
                                                            <i class="fa fa-download"></i>
                                                        </a>
                                                    </td>
                                                    <?php
                                                }
                                                ?>
                                            </tr>
                                            <tr>
                                                <td colspan="6">
                                                    <strong>Description : </strong><?php echo $con[0]->handbook_desc; ?>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <?php
                                    if (!empty($cr)) {
                                        ?>
                                        <table class="table table-bordered">
                                            <tbody>
                                                <?php $total_expense_props = 0; ?>
                                                <tr style="background:#ddd;">
                                                    <td>Type</td>
                                                    <td>
                                                        Courier Name 
                                                    </td>
                                                    <td>
                                                        Date
                                                    </td>
                                                    <td>
                                                        Courier Ref. 
                                                    </td>
                                                    <td>
                                                        Notes
                                                    </td>
                                                    <td>
                                                        Courier Expenses 
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <?php
                                                foreach ($cr as $crData) {
                                                    if ($crData->cr_type == 'Participant Handbook') {
                                                        ?>
                                                        <tr>
                                                            <td><?php echo $crData->cr_type; ?></td>
                                                            <td><?php echo $crData->cr_cname; ?></td>
                                                            <td><?php echo $crData->cr_ship_date; ?></td>
                                                            <td><?php echo $crData->cr_cid; ?></td>
                                                            <td><?php echo $crData->comments; ?></td>
                                                            <td align="right"><?php echo number_format($crData->cr_amt, 2); ?></td>
                                                            <td>
                                                                <a href="<?php echo base_url(); ?>support/update_page/<?php echo $crData->cr_id; ?>?type=Courier" class="btn-sm btn-primary"><i class="fa fa-pencil"></i></a>
                                                            </td>
                                                        </tr>
                                                        <?php
                                                        $total_expense_props += $crData->cr_amt;
                                                    }
                                                }
                                                ?>
                                                        <tr><td colspan="6" align="right"><strong>Rs. <?php echo number_format($total_expense_props, 2); ?></strong></td><td></td></tr>
                                            </tbody>
                                        </table>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
                ?>

                <?php
//if ($program[0]->props == '1') {
                ?>

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-star"></i> Props Shipment / Misc. Expenses Recording</h2>
                            <a href="#" class="btn btn-sm btn-info pull-right btn-sm prop-status-btn" style="margin-top:-24px;"><i class="fa fa-edit"></i> Update Expenses</a>

                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 'Props') {
                                ?>
                                <div class="alert alert-success col-md-6 col-md-offset-3">
                                    Delivery details updated Successfully.
                                </div>
                                <?php
                            }
                            ?>
                            <?php
                            $props = $this->projectmanager_model->get_project_props($program[0]->project_id);
                            if (!empty($props)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Name</th>
                                            <th>Quantity</th>
                                            <th>Uploaded By</th>
                                            <th>Requested Date</th>
                                            <th>Status</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($props as $p_data) {
                                            if ($p_data->delivery_status != '0') {
                                                ?>
                                                <tr>
                                                    <td><?php echo $p_data->prop_name; ?></td>
                                                    <td><?php echo $p_data->quantity; ?></td>

                                                    <td><?php echo $p_data->name; ?> - <?php echo $p_data->user_type; ?></td>
                                                    <td><?php echo date_formate_short($p_data->requested_date); ?></td>
                                                    <td><?php
                                                        if ($p_data->delivery_status == '0') {
                                                            echo 'Not Shipped';
                                                        }
                                                        if ($p_data->delivery_status == '1') {
                                                            echo 'Shipped';
                                                        }
                                                        if ($p_data->delivery_status == '2') {
                                                            echo 'Delivered';
                                                        }
                                                        if ($p_data->delivery_status == '3') {
                                                            echo 'Approved by PM';
                                                        }
                                                        ?></td>

                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                        <tr><td colspan="5"><strong>Notes:</strong><?php echo $p_data->comments; ?> </td></tr>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                            <?php if (!empty($cr)) { ?>
                                <table class="table table-bordered">
                                    <tbody>
                                        <?php $total_expense_props = 0; ?>
                                        <tr style="background:#ddd;">
                                            <td>Type</td>
                                            <td>
                                                Courier Name 
                                            </td>
                                            <td>
                                                Date
                                            </td>
                                            <td>
                                                Courier Ref. 
                                            </td>
                                            <td>
                                                Notes
                                            </td>
                                            <td>
                                                Courier Expenses 
                                            </td>
                                            <td></td>

                                        </tr>
                                        <?php
                                        foreach ($cr as $crData) {
                                            if ($crData->cr_type != 'Participant Handbook') {
                                                ?>
                                                <tr>
                                                    <td><?php echo $crData->cr_type; ?></td>
                                                    <td><?php echo $crData->cr_cname; ?></td>
                                                    <td><?php echo $crData->cr_ship_date; ?></td>
                                                    <td><?php echo $crData->cr_cid; ?></td>
                                                    <td><?php echo $crData->comments; ?></td>
                                                    <td align="right"><?php echo number_format($crData->cr_amt, 2); ?></td>
                                                    <td><a href="<?php echo base_url(); ?>support/update_page/<?php echo $crData->cr_id; ?>?type=Courier" class="btn-sm btn-primary"><i class="fa fa-pencil"></i></a></td>
                                                </tr>
                                                <?php
                                                $total_expense_props += $crData->cr_amt;
                                            }
                                        }
                                        ?>
                                                <tr><td colspan="6" align="right"><strong>Rs. <?php echo number_format($total_expense_props, 2); ?></strong></td><td></td></tr>
                                    </tbody>
                                </table>
                            <?php } ?>
                        </div>
                    </div>
                </div>
                <?php
// }
                ?>
                <?php
                if ($program[0]->stay_arrangement == 'Wagons' || $program[0]->stay_arrangement == 'Client') {
                    ?>
                    <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-bed"></i> Stay Arrangements </h2>
                                <?php if (!empty($tn)) { ?>
                                    <a href="#" class="btn btn-sm btn-info pull-right stay-update-btn" style="margin-top:-24px;"><i class="fa fa-edit"></i> Book Stay</a>
                                <?php } ?>
                            </div>
                            <div class="panel-body">
                                <?php
                                if ($msg == 'Stay') {
                                    ?>
                                    <div class="alert alert-success col-md-6 col-md-offset-3">
                                        Stay details Updated Successfully.
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="clearfix"></div>
                                <?php
                                if ($program[0]->stay_arrangement == "Wagons") {
                                    echo "<p><strong>Stay Limit : </strong> Rs." . $program[0]->stay_limit;
                                    if ($program[0]->claim_stay_cost == "Y")
                                        echo " - Claim Cost with Client";
                                    else
                                        echo " - Wagons to Absorb Cost</p>";
                                }
                                ?>
                                <?php
                                $stay = $this->projectmanager_model->get_stay_details($program[0]->project_id);
                                if (!empty($stay)) {
                                    ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Trainer</th>
                                                <th>Hotel</th>
                                                <th>Check in</th>
                                                <th>Check out</th>
                                                <th>Notes</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $stay_subtotal = 0;
                                            foreach ($stay as $st_data) {

                                                if ($st_data->request_status != '0') {
                                                    $stay_subtotal += $st_data->expense_amount;
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $st_data->name; ?></td>
                                                        <td><?php echo $st_data->hotel_name; ?></td>
                                                        <td><?php echo date_formate($st_data->checkin_time); ?></td>
                                                        <td><?php echo date_formate($st_data->checkout_time); ?></td>
                                                        <td><?php echo $st_data->comments; ?></td>
                                                        <td>
                                                            <?php
                                                            if ($st_data->request_status == '0') {
                                                                echo 'Approval Pending';
                                                            } if ($st_data->request_status == '2') {
                                                                echo 'Approved by PM';
                                                            } if ($st_data->request_status == '1') {
                                                                echo 'Booked<br/>';
                                                                echo 'Rs.' . $st_data->expense_amount;
                                                                ?>
                                                                <?php if ($st_data->booking_screenshot != "") { ?>
                                                                    <br/>
                                                                    <a href="<?php echo base_url(); ?>assets/upload/stay/<?php echo $st_data->booking_screenshot; ?>" target="_blank">Ticket/Voucher</a>
                                                                    <?php
                                                                }
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            if ($st_data->direct_book == '0') {
                                                                ?>
                                                                <i class="fa fa-hand-pointer-o" aria-hidden="true" style="font-size:18px; font-weight: bold;"></i>
                                                                <?php
                                                            }
                                                            if ($st_data->direct_book == '1') {
                                                                ?>
                                                                <i class="fa fa-check-square-o" aria-hidden="true" style="font-size:18px; font-weight: bold;"></i>
                                                                <?php
                                                            }
                                                            if ($st_data->request_status == '2') {
                                                                ?>
                                                                <a href="#" class="btn btn-sm btn-success book-stay" sid="<?php echo $st_data->id; ?>">Book</a>
                                                                <?php
                                                            } else {
                                                            ?>
                                                                <a href="<?php echo base_url(); ?>support/update_page/<?php echo $st_data->id; ?>?type=stay" class="btn-sm btn-primary"><i class="fa fa-pencil"></i></a>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            if ($stay_subtotal > 0) {
                                                ?>
                                                <tr><td colspan="7" align="right"><strong>Rs. <?php echo number_format($stay_subtotal, 2); ?></strong></td></tr>
                                            <?php } ?>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
				<?php } if ($program[0]->travel_arrangement == 'Wagons' || $program[0]->travel_arrangement == 'Client') { ?>
                    <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-cab"></i> Travel Arrangements </h2>
                                <?php if (!empty($tn)) { ?>
                                    <a href="#" class="btn btn-sm btn-info pull-right travel-update-btn" style="margin-top:-24px;"><i class="fa fa-edit"></i> Book Travel</a>
                                <?php } ?>
                            </div>
                            <div class="panel-body">
                                <?php
                                if ($msg == 'Travel') {
                                    ?>
                                    <div class="alert alert-success col-md-6 col-md-offset-3">
                                        Travel details Updated Successfully.
                                    </div>
                                    <?php
                                }
                                ?>
                                <div class="clearfix"></div>
                                <?php
                                if ($program[0]->travel_arrangement == "Wagons") {
                                    echo "<p><strong>Travel Cost Limit : </strong> Rs." . $program[0]->travel_limit;
                                    if ($program[0]->claim_travel_cost == "Y")
                                        echo " - Claim Cost with Client";
                                    else
                                        echo " - Wagons to Absorb Cost</p>";
                                }
                                ?>
                                <?php
                                $travel = $this->projectmanager_model->get_travel_details($program[0]->project_id);
                                if (!empty($travel)) {
                                    ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Trainer</th>
                                                <th>Date</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Mode</th>
                                                <th>Notes</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            $travel_subtotal = 0;
                                            foreach ($travel as $tv_data) {

                                                if ($tv_data->request_status != '0') {
                                                    $travel_subtotal += $tv_data->expense_amount;
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $tv_data->name; ?></td>
                                                        <td><?php echo date_formate_short($tv_data->date); ?></td>
                                                        <td><?php echo $tv_data->from_loc; ?></td>
                                                        <td><?php echo $tv_data->to_loc; ?></td>
                                                        <td><?php echo $tv_data->mode; ?></td>
                                                        <td><?php echo $tv_data->comments; ?></td>
                                                        <td>
                                                            <?php
                                                            if ($tv_data->request_status == '0') {
                                                                echo 'Approval Pending';
                                                            } if ($tv_data->request_status == '2') {
                                                                echo 'Approved By PM';
                                                            } if ($tv_data->request_status == '1') {
                                                                echo 'Booked<br/>';
                                                                echo 'Rs.' . $tv_data->expense_amount;
                                                                ?>
                                                                <?php if ($tv_data->screenshot != "") { ?>
                                                                    <br/>
                                                                    <a href="<?php echo base_url(); ?>assets/upload/travel/<?php echo $tv_data->screenshot; ?>" target="_blank">Ticket</a>
                                                                <?php } ?>
                                                            </td>
                                                        <?php } ?>
                                                        <td>
                                                            <?php
                                                            if ($tv_data->direct_book == '0') {
                                                                ?>
                                                                <i class="fa fa-hand-pointer-o" aria-hidden="true" style="font-size:18px; font-weight: bold;"></i>
                                                                <?php
                                                            }
                                                            if ($tv_data->direct_book == '1') {
                                                                ?>
                                                                <i class="fa fa-check-square-o" aria-hidden="true" style="font-size:18px; font-weight: bold;"></i>
                                                                <?php
                                                            }
                                                            if ($tv_data->request_status == '2') {
                                                                ?>
                                                                <a href="#" class="btn btn-sm btn-primary book-travel" status='2' tid="<?php echo $tv_data->id; ?>">Book</a>
                                                                <?php
                                                            } else {
                                                            ?>
                                                                <a href="<?php echo base_url(); ?>support/update_page/<?php echo $tv_data->id; ?>?type=travel" class="btn-sm btn-primary"><i class="fa fa-pencil"></i></a>
                                                            <?php }
                                                            ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            if ($travel_subtotal > 0) {
                                                ?>
                                                <tr><td colspan="8" align="right"><strong>Rs. <?php echo number_format($travel_subtotal, 2); ?></strong></td></tr>
                                            <?php } ?>

                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
            </div>
        </div>

        <div class="modal fade" id="props_add_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Props Needed</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="props-add-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="props_user" value="<?php echo $this->session->userdata('user_code'); ?>"/>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Props Name <sup>*</sup></span>
                                    <input type="text" name="pname"  class="form-control">
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Quantity <sup>*</sup></span>
                                    <input type="text" name="qnt"  class="form-control">
                                </div>
                            </div>


                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="upload_content_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Upload Content</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="add-content-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="client" value="<?php echo $program[0]->client_id; ?>"/>
                            <input type="hidden" name="content_user" value="<?php echo $this->session->userdata('user_code'); ?>"/>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Content Title <sup>*</sup></span>
                                    <input type="text" name="title" placeholder="Content Title" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">

                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Upload Content <sup>*</sup></span>
                                    <input type="file" name="content" class="form-control">
                                </div>

                            </div>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Content For <sup>*</sup></span>
                                    <select class="form-control" name="ctype">
                                        <option value="project">Program</option>
                                        <option value="client">Client</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="upload_form_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Upload Forms</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="add-forms-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="form_user" value="<?php echo $this->session->userdata('user_code'); ?>"/>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Form Type <sup>*</sup></span>
                                    <input type="text" name="ftype" placeholder="Form Type" class="form-control">
                                </div>
                            </div>
                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Date <sup>*</sup></span>
                                    <input type="text" name="fdate" placeholder="YYYY-MM-DD" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Upload Form <sup>*</sup></span>
                                    <input type="file" name="content" class="form-control">
                                </div>
                            </div>

                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="stay_form_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Stay Arrangements</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="add-stay-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="stay_user" value="<?php echo $this->session->userdata('user_code'); ?>"/>
                            <input type="hidden" name="pid" id='stayId' value=""/>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Amount <sup>*</sup></span>
                                    <input type="text" name="stay_amt" class="form-control">
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Booking Screenshot </label> : <br/>
                                <input type="file" name="screen"/>

                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Payment Status <sup>*</sup></span>

                                    <select name="pay_status" class="form-control">
                                        <option value="">Select</option>
                                        <option value="Paid">Already Paid</option>
                                        <option value="Trainer to Pay">Trainer to Pay</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Notes<sup>*</sup></span>
                                    <textarea id="comments" name="comments"  class="form-control"></textarea>
                                </div>
                            </div>


                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="travel_form_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Travel Arrangements</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="add-travel-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="travel_user" value="<?php echo $this->session->userdata('user_code'); ?>"/>
                            <input type="hidden" name="pid1" id="travelId" value=""/>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Amount <sup>*</sup></span>
                                    <input type="text" name="travel_amt1" class="form-control">
                                </div>
                            </div>


                            <div class="form-group col-md-12">
                                <label>Booking Screenshot </label> : <br/>
                                <input type="file" name="screen"/>

                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Notes<sup>*</sup></span>
                                    <textarea id="comments" name="comments"  class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <div class="modal fade" id="upload_image_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Upload Images</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="add-image-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="img_user" value=""/>

                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Upload Image <sup>*</sup></span>
                                    <input type="file" name="img[]" multiple="multiple" class="form-control">
                                </div>
                            </div>

                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="upload_img" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="ship_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Record Expenses Incurred On Program</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="ship-form">
                        <?php
                        $cid = "";
                        $cname = "";
                        $camt = "";
                        $cr_ship_date = "";
                        ?>
                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="handbook" id="pTypeInput" value="Participant Handbook"/>
                            <div class="form-group col-md-12" id="pTypeWraper">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Expense Type <sup>*</sup></span>
                                    <select class="form-control" id="pType" name="pType">
                                        <option value="">- Select -</option>
										<option value="Content Dev. Charges">Content Development Expenses</option>
                                        <option value="Courier Charges">Courier Shipment Expenses</option>
                                        <option value="Food Expenses">Food Expenses</option>
                                        <option value="Lamination Charges">Lamination Expenses</option>
                                        <option value="Misc. Charges">Misc. Expenses</option>
                                        <option value="Participant Handbook Printing Charges">Printing Expenses</option>
                                        <option value="Participant Handbook Shipment Charges">Participant Handbook Courier</option>
                                        <option value="Stay Charges">Stay Charges</option>	
                                        <option value="Stationary Charges">Stationary Purchase</option>	
                                        <option value="Training Props Purchase">Training Material Shipment Expenses</option>
                                        <option value="Travelling Charges">Travelling Expenses</option>
                                        <option value="Xerox Expenses">Xerox Expenses</option>

                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Date of Expense<sup>*</sup></span>

                                    <input type="text" name="cr_ship_date" placeholder="YYYY-MM-DD"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Vendor <sup>*</sup></span>
                                    <input type="text" name="cname" value="<?php echo $cname; ?>" class="form-control" placeholder="Courier Company Name">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Reference No.<sup>*</sup></span>
                                    <input type="text" name="cid" value="<?php echo $cid; ?>" class="form-control" placeholder="Courier Ref No.">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Expense Amount (Rs.) <sup>*</sup></span>
                                    <input type="text" name="camt" value="<?php echo $camt; ?>" class="form-control" placeholder="Courier Expenses">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Notes<sup>*</sup></span>
                                    <textarea id="comments" name="comments"  class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="upload_img" class="btn btn-info" ><i class="fa fa-upload"></i> Update</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="stay_form_wraper1" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Stay Arrangements Preferences</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="add-stay-form1">

                        <div class="modal-body">

                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="stay_user" value="<?php echo $this->session->userdata('user_code'); ?>"/>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Trainer <sup>*</sup></span>
                                    <select class="form-control" name="trainer">
                                        <option value=""> - Select - </option>
                                        <?php
                                        if (!empty($tn)) {
                                            foreach ($tn as $tnData) {
                                                ?>
                                                <option value="<?php echo $tnData->user_code; ?>"><?php echo $tnData->name; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Hotel Name <sup>*</sup></span>
                                    <input type="text" name="hotel" class="form-control">
                                </div>
                            </div>
                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Check in Time <sup>*</sup></span>
                                    <input type="text" name="checkin"  class="form-control" placeholder="YYYY-MM-DD H:M">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Check out Time <sup>*</sup></span>
                                    <input type="text" name="checkout"  class="form-control" placeholder="YYYY-MM-DD H:M">
                                </div>
                            </div>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Amount <sup>*</sup></span>
                                    <input type="text" name="stay_amt" class="form-control">
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <label>Booking Screenshot </label> : <br/>
                                <input type="file" name="screen"/>

                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Payment Status <sup>*</sup></span>

                                    <select name="pay_status" class="form-control">
                                        <option value="">Select</option>
                                        <option value="Paid">Already Paid</option>
                                        <option value="Trainer to Pay">Trainer to Pay</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Notes<sup>*</sup></span>
                                    <textarea id="comments" name="comments"  class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group  col-md-12 text-center">
                            </div>

                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Update</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="travel_form_wraper1" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Travel Arrangements Preferences</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="add-travel-form1">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="travel_user" value="<?php echo $this->session->userdata('user_code'); ?>"/>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Trainer <sup>*</sup></span>
                                    <select class="form-control" name="trainer">
                                        <option value=""> - Select - </option>
                                        <?php
                                        if (!empty($tn)) {
                                            foreach ($tn as $tnData) {
                                                ?>
                                                <option value="<?php echo $tnData->user_code; ?>"><?php echo $tnData->name; ?></option>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Date <sup>*</sup></span>
                                    <input type="text" name="date" class="form-control" placeholder="YYYY-MM-DD">
                                </div>
                            </div>
                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> From Location <sup>*</sup></span>
                                    <input type="text" name="from"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> To Location <sup>*</sup></span>
                                    <input type="text" name="to"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Mode <sup>*</sup></span>
                                    <select class="form-control" name="mode">
                                        <option value=""> - Select -</option>
                                        <option value="Bus">Bus</option>
                                        <option value="Cab">Cab</option>
                                        <option value="Train">Train</option>
                                        <option value="Air">Air</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Amount <sup>*</sup></span>
                                    <input type="text" name="travel_amt" class="form-control">
                                </div>
                            </div>


                            <div class="form-group col-md-12">
                                <label>Booking Screenshot </label> : <br/>
                                <input type="file" name="screen"/>

                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Notes<sup>*</sup></span>
                                    <textarea id="comments" name="comments"  class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.upload-form-btn').click(function (e) {
                    e.preventDefault();
                    $('#upload_form_wraper').modal('show');
                });

                $('.props-add-btn').click(function (e) {
                    e.preventDefault();
                    $('#props_add_wraper').modal('show');
                });

                $('.stay-update-btn').click(function (e) {
                    e.preventDefault();
                    $('#stay_form_wraper1').modal('show');
                });

                $('.travel-update-btn').click(function (e) {
                    e.preventDefault();
                    $('#travel_form_wraper1').modal('show');
                });

                $('.upload-content-btn').click(function (e) {
                    e.preventDefault();
                    $('#upload_content_wraper').modal('show');
                });

                $('.book-stay').click(function (e) {
                    e.preventDefault();
                    var sid = $(this).attr('sid');
                    $('#stay_form_wraper').modal('show');
                    $('#stayId').val(sid);
                });

                $('.book-travel').click(function (e) {
                    e.preventDefault();
                    var sid = $(this).attr('tid');
                    $('#travel_form_wraper').modal('show');
                    $('#travelId').val(sid);
                });

                $('.images-add-btn').click(function (e) {
                    e.preventDefault();
                    $('#upload_image_wraper').modal('show');
                });


                $('.assign-pm-btn').click(function (e) {
                    e.preventDefault();
                    $('#assign-pm-wrap').modal('show');
                    var cid = $(this).attr('cid');
                    $('#cid').val(cid);
                });

//                $.validator.addMethod("check_box", function (value, element) {
//                   // return this.optional(element) || /^[a-z0-9\\-]+$/i.test(value);
//                    return false;
//                }, "This is required");

                $("#add-travel-form1").validate({
                    rules: {
                        from: "required",
                        to: "required", date: "required",
                        mode: "required",
                        travel_amt: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#add-stay-form1").validate({
                    rules: {
                        hotel: "required",
                        checkin: "required",
                        checkout: "required",
                        stay_amt: "required",
                        trainer: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#add-stay-form").validate({
                    rules: {
                        stay_amt: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#ship-form").validate({
                    rules: {
                        cname: "required",
                        cr_ship_date: "required",
                        cid: "required",
                        camt: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#add-travel-form").validate({
                    rules: {
                        travel_amt: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#props-add-form").validate({
                    rules: {
                        pname: "required",
                        qnt: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#add-content-form").validate({
                    rules: {
                        title: "required",
                        content: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#add-forms-form").validate({
                    rules: {
                        content: "required",
                        ftype: "required",
                        fdate: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                function update_stay() {
                    var check = $(".trainer_list:checked").length;
                    alert(check);

                }

                $('.prop-status').click(function (e) {
                    e.preventDefault();
                    var sid = $(this).attr('sid');
                    var pid = "<?php echo $program[0]->project_id; ?>";
                    var status = $(this).attr('status');
                    $(this).remove();
                    $('.page_spin').show();
                    var dataString = "sid=" + sid + "&pid=" + pid + "&status=" + status + "&page=update_props_status";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            alert('Status updated');
                        }, //success fun end
                    });//ajax end
                });







                $('.remove-content').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove content ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_content";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.remove-form').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove form ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_form";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.remove-props').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove props ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_props";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.prop-status-btn').click(function (e) {
                    e.preventDefault();
                    $('#ship_wraper').modal('show');
                    $('#pTypeWraper').show();
                });

                $('.prop-status-btn1').click(function (e) {
                    e.preventDefault();
                    $('#ship_wraper').modal('show');
                    $('#pTypeWraper').hide();
                    $('#pType').val('Participant Handbook');
                });

            });
        </script>
        <script>
            function initMap() {
                var loc = {lat: <?php echo $cIn[0]->trainer_lat; ?>, lng: <?php echo $cIn[0]->trainer_long; ?>};
                var map = new google.maps.Map(document.getElementById('googleMap'), {
                    zoom: 16,
                    center: loc
                });
                var marker = new google.maps.Marker({
                    position: loc,
                    map: map
                });
            }
        </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDdlPfXgK3snKutJIZj-8ouzJtO0kXUMuQ&callback=initMap"></script>

    </body>
</html>