<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Program Engagement Requests</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Programs</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li ><a href="<?php echo base_url(); ?>support/program_details/<?php echo $pid; ?>"><i class="fa fa-info"></i> Details</a></li>
                            <li class="active"><a href="#"><i class="fa fa-paper-plane"></i> Engagement Requests</a></li>
                            
                            

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Engagement Requests</h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list"></i> Requests List </h2>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Trainer Comment</th>
                                        <th>BDM Comment</th>
                                        <th>Client Comment</th>
                                        <th>Apply Date</th>
                                        <th>Status</th>
                                        <th>Status Date</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($request)) {
                                        $num = 0;
                                        foreach ($request as $pm_data) {
                                            $num++;
                                            ?>
                                            <tr>
                                                <td><?php echo $num; ?></td>
                                                <td><?php echo $pm_data->name; ?></td>
                                                <td><?php echo $pm_data->trainer_comments; ?></td>
                                                <td><?php echo $pm_data->bdm_comments; ?></td>
                                                <td><?php echo $pm_data->client_comments; ?></td>
                                                <td><?php echo date_formate_short($pm_data->apply_date); ?></td>
                                                <td class="status_<?php echo $pm_data->id; ?>"><?php
                                                    if ($pm_data->status == '2') {
                                                        ?>
                                                        <span class="label label-warning">Pending</span>
                                                        <?php
                                                    }
                                                    if ($pm_data->status == '1' && $pm_data->admin_approved == '0') {
                                                        ?>
                                                        <span class="label label-primary">Admin Review Pending</span>
                                                        <?php
                                                    }
                                                    if ($pm_data->status == '0' && $pm_data->admin_approved == '0') {
                                                        ?>
                                                        <span class="label label-danger">Rejected</span>
                                                        <?php
                                                    }
                                                    if ($pm_data->status == '1' && $pm_data->admin_approved == '1') {
                                                        ?>
                                                        <span class="label label-success">Approved</span>
                                                        <?php
                                                    }
                                                    ?></td>
                                                <td>
                                                    <?php
                                                    if ($pm_data->status == '1' && $pm_data->admin_approved == '0') {
                                                        echo date_formate_short($pm_data->engage_date);
                                                    }
                                                    if ($pm_data->status == '0' && $pm_data->admin_approved == '0') {
                                                        echo date_formate_short($pm_data->reject_date);
                                                    }
                                                    if ($pm_data->status == '1' && $pm_data->admin_approved == '1') {
                                                        echo date_formate_short($pm_data->approval_date);
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($pm_data->status == '1' && $pm_data->admin_approved == '0') {
                                                        ?>
                                                        <a href="#" class="btn btn-sm btn-info accept-btn" uid="<?php echo $pm_data->user_code; ?>" pid="<?php echo $pm_data->project_id; ?>" rid="<?php echo $pm_data->id; ?>">Approve</a>
                                                        <a href="#" class="btn btn-sm btn-danger reject-btn" rid="<?php echo $pm_data->id; ?>">Reject</a>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="modal fade" id="approve_form" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Assign Trainer</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="assign-trainer-form">

                        <div class="modal-body">
                            <input type="hidden" id="project" name="project" value=""/>
                            <input type="hidden" id="user" name="user" value=""/>
                            <input type="hidden" id="eid" name="eid" value=""/>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> From Date <sup>*</sup></span>
                                    <input type="date" id="from" name="from" class="form-control">
                                </div>
                            </div>
                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> To Date <sup>*</sup></span>
                                    <input type="date" id="to" name="to"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Payment <sup>*</sup></span>
                                    <input type="text" id="amt" name="amt"  class="form-control">
                                </div>
                            </div>

                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {
                
                
                $("#assign-trainer-form").validate({
                    rules: {
                        from: "required",
                        to: "required",
                        amt: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        assign_trainer();
                    }
                });
                
                function assign_trainer()
                {
                    $('#approve_form').modal('hide');
                    var from = $('#from').val();
                    var to = $('#to').val();
                    var amt = $('#amt').val();
                    var pid = $('#project').val();
                    var uid = $('#user').val();
                    var rid = $('#eid').val();
                    $('.page_spin').show();
                    var dataString = "rid=" + rid + "&pid="+pid+"&uid="+uid+"&from="+from+"&to="+to+"&amt="+amt+"&page=accept_request";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            alert('Status Updated');
                            $('.status_' + rid + '').children('.label-primary').remove();
                            $('.status_' + rid + '').siblings('td').children('.accept-btn').remove();
                            $('.status_' + rid + '').siblings('td').children('.reject-btn').remove();
                            $('.status_' + rid + '').append('<span class="label label-success">Admin Approved</span>');
                        }, //success fun end
                    });//ajax end
                }
                
                $('.accept-btn').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var pid = $(this).attr('pid');
                    var uid = $(this).attr('uid');
                    $('#approve_form').modal('show');
                    $('#user').val(uid);
                    $('#project').val(pid);
                    $('#eid').val(rid);
                    
                });
                
                $('.reject-btn').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    
                    $('.page_spin').show();
                    var dataString = "rid=" + rid + "&page=reject_request";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            alert('Status Updated');
                            $('.status_' + rid + '').children('.label-warning').remove();
                            $('.status_' + rid + '').siblings('td').children('.accept-btn').remove();
                            $('.status_' + rid + '').siblings('td').children('.reject-btn').remove();
                            $('.status_' + rid + '').append('<span class="label label-danger">Rejected</span>');
                        }, //success fun end
                    });//ajax end
                });



            });
        </script>

    </body>
</html>