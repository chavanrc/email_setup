<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Invoice</h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list"></i> Invoice List </h2>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Program</th>
                                        <th>Trainer</th>
                                        <th>Invoice</th>
                                        <th>Date</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($payment)) {
                                        $num = 0;
                                        foreach ($payment as $pm_data) {
                                            $num++;
                                            ?>
                                            <tr>
                                                <td><?php echo $num; ?></td>
                                                <td><?php echo $pm_data->project_title; ?></td>
                                                <td><?php echo $pm_data->name; ?></td>
                                                <td><a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $pm_data->trainer_invoice_file; ?>" class="btn btn-sm btn-primary"><i class="fa fa-download"></i> View</a></td>
                                                <td><?php echo date_formate_short($pm_data->trainer_invoice_date); ?></td>
                                                <td>
                                                    <?php
                                                    if($pm_data->trainer_paid_status=='0')
                                                    {
                                                        ?>
                                                        Pending
                                                        <?php
                                                    }
                                                    if($pm_data->trainer_paid_status=='1')
                                                    {
                                                        ?>
                                                        Paid
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if($pm_data->trainer_paid_status=='0')
                                                    {
                                                        ?>
                                                    <a href="#" class="btn btn-sm btn-info pay-btn" pid="<?php echo $pm_data->id; ?>">Pay</a>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    else
                                    {
                                        ?>
                                            <tr>
                                                <td colspan="7">No Record found</td>
                                            </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                
                $('#data-list').DataTable();
                
                
                $('.pay-btn').click(function(e){
                    e.preventDefault();
                   var pid = $(this).attr('pid');
                   var f = confirm("Are you sure want make payment ?");
                   if(f==true)
                   {
                       $('.page_spin').show();
                        var dataString = "pid=" + pid + "&page=update_payment";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                   }
                });
                
            });
        </script>

    </body>
</html>