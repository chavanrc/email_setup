<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Contents</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Contents</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>admin/add_content" class="btn btn-primary"><i class="fa fa-user-plus"></i> Add New Content</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list"></i> Contents </h2>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>File</th>
                                        <th>Uploaded By</th>
                                        <th>User Type</th>
                                        <th>Date</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($content)) {
                                        $num = 0;
                                        foreach ($content as $cn_data) {
                                            $num++;
                                            ?>
                                            <tr>
                                                <td><?php echo $num; ?></td>
                                                <td><?php echo $cn_data->filename; ?></td>
                                                <td><a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $cn_data->file_path; ?>" class="btn btn-sm btn-primary"><i class="fa fa-download"></i> View</a></td>
                                                <td><?php echo $cn_data->name; ?></td>
                                                <td><?php echo $cn_data->user_type; ?></td>
                                                <td><?php echo date_formate_short($cn_data->upload_date); ?></td>
                                                <td>
                                                    <?php
                                                    if($cn_data->user_code==$this->session->userdata('user_code'))
                                                    {
                                                        ?>
                                                    <a href="#" class="btn btn-sm btn-danger remove-content" rid="<?php echo $cn_data->id; ?>"><i class="fa fa-trash"></i> Remove</a>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    else
                                    {
                                        ?>
                                            <tr>
                                                <td colspan="7">No Record found</td>
                                            </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                
                $('#data-list').DataTable();
                
                
                $('.remove-content').click(function(e){
                   var rid = $(this).attr('rid');
                   var f = confirm("Are you sure want remove ?");
                   if(f==true)
                   {
                       $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_content";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                   }
                });
                
            });
        </script>

    </body>
</html>