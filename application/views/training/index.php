<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Trainer | Dashboard</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

         <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px; padding-top: 20px;">

                <div class="col-md-6">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h4 style="display: inline-block;"><i class="fa fa-tasks"></i> To Do Task</h4>
                        </div>
                        <div class="box-body" style="height:200px; overflow-y: scroll;">
                            <table class="table table-bordered" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Program</th>
                                        <th>Task</th>
                                        <th>Due Date</th>
                                    </tr>
                                </thead>
                                <tbody style="font-size:13px;">
                                    <?php
                                    
                                    $note = $CI->admin_model->get_notification($this->session->userdata('t_code'));
                                    if (!empty($note)) {
                                        $no = 0;
                                        foreach ($note as $nt_data) {
                                            if($nt_data->todo_task==1){
                                                $no++;
                                            ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>training/program_details/<?php echo $nt_data->project_id; ?>">
                                                    <?php echo $nt_data->project_title; ?>
                                                    </a>
                                                </td>
                                                <td><?php echo $nt_data->notification_text; ?>
                                                <br/><br/>
                                                <span style="font-size:12px; color: #777;">
                                                    <?php echo date_formate($nt_data->notification_date); ?>
                                                </span>
                                                </td>
                                                <td><?php if($nt_data->due_date!='0000-00-00 00:00:00'){ 
                                                    $dd = explode(' ', $nt_data->due_date);
                                                    if($dd[0]<date('Y-m-d')){
                                                        $now = time(); // or your date as well
                                                        $ndate = strtotime($dd[0]);
                                                        $datediff = $now - $ndate;
                                                        echo ceil($datediff/86000).' Days Due';
                                                    } else {
                                                        echo date_formate_short($nt_data->due_date);
                                                    }
                                                    
                                                } ?></td>

                                            </tr>
                                            <?php
                                            }
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>  
                </div>
                <div class="col-md-6">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h4 style="display: inline-block;"><i class="fa fa-info"></i> Notifications</h4>
                        </div>
                        <div class="box-body" style="height:200px; overflow-y: scroll;">
                            <table class="table table-bordered" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Program</th>
                                        <th>Notification</th>

                                    </tr>
                                </thead>
                                <tbody style="font-size:13px;">
                                    <?php
                                    if (!empty($note)) {
                                        $no = 0;
                                        foreach ($note as $nt_data) {
                                            if($nt_data->todo_task==0){
                                                $no++;
                                            ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>training/program_details/<?php echo $nt_data->project_id; ?>">
                                                    <?php echo $nt_data->project_title; ?>
                                                    </a>
                                                </td>
                                                <td><?php echo $nt_data->notification_text; ?>
                                                <br/><br/>
                                                <span style="font-size:12px; color: #777;">
                                                    <?php echo date_formate($nt_data->notification_date); ?>
                                                </span>
                                                </td>

                                            </tr>
                                            <?php
                                            }
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
 		</div>
            </div>
	</div>
        </div>
<?php include 'js_files.php'; ?>
    </body>
</html>