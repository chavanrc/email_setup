<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Trainer Support | Add Program</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/datepicker/jquery.datetimepicker.css">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Program - Create New</h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-plus"></i> Program Details</h2>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg > 0) {
                                ?>
				<h4>Mention Preferences for Props, Stay, Travel Required for the training program</h4>
                                <form action="" method="POST"  enctype="multipart/form-data" id="add-program-form">
                                    
                                    <input type="hidden" value="<?php echo $msg; ?>" name="pid"/>
                                    
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Props Required <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="radio" name="prop" value="1"> Yes 
                                            <input type="radio" name="prop" checked value="0" style="margin-left: 25px;"> No 
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Stay Arrangement <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control" name="stay" id="stay">
                                                <option value=""> - Select -</option>
                                                <option value="None"> None </option>
                                                <option value="Wagons"> Wagons </option>
                                                <option value="Client"> Client </option>
                                                <option value="Trainer"> Trainer </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group" id="stay_limit_block" style="display:none;">
                                        <div class="clearfix"></div>
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Stay Spend Limit in Rs.<span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text"  name="stay_limit" class="form-control col-md-7 col-xs-12"><br/>
                                            
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
									 <div class="form-group" id="stay_claim_block" style="display:none;">
                                        <div class="clearfix"></div>
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Stay Billing Options<span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="radio" name="claim_stay_cost" value="Yes" checked>Claim with Client
                                            <input type="radio" name="claim_stay_cost" value="No">Absorb By Wagons
                                            
                                        </div>
                                    </div>
									
									<div class="clearfix"></div>

                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Travel Arrangement <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control" name="travel" id="travel">
                                                <option value=""> - Select -</option>
                                                <option value="None"> None </option>
                                                <option value="Wagons"> Wagons </option>
                                                <option value="Client"> Client </option>
                                                <option value="Trainer"> Trainer </option>

                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group" id="travel_limit_block" style="display:none;">
                                        <div class="clearfix"></div>
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Travel Spend Limit in Rs.<span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text"  name="travel_limit" class="form-control col-md-7 col-xs-12"><br/>
                                            
                                        </div>
                                    </div>
									 <div class="clearfix"></div>
									 
									 <div class="form-group" id="travel_claim_block" style="display:none;">
                                        <div class="clearfix"></div>
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Travel Billing Options<span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="radio" name="claim_travel_cost" value="Yes" checked>Claim with Client
                                            <input type="radio" name="claim_travel_cost" value="No">Absorb By Wagons
                                            
                                        </div>
                                    </div>
									<div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Program Charges Agreed With Client / Day <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            
						<input type="text"  name="client_charges" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Trainer Fee Range / Day <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-6">
                                            <select class="form-control" name="trainer_fee_range">
                                                <option value=""> - Select -</option>
                                                <option value="1000-2000"> 1000-2000 </option>
                                                <option value="2000-3000"> 2000-3000 </option>
                                                <option value="3000-4000"> 3000-4000 </option>
                                                <option value="4000-5000"> 4000-5000 </option>
                                                <option value="5000-6000"> 5000-6000 </option>
                                                <option value="6000-7000"> 6000-7000 </option>
                                                <option value="7000-8000"> 7000-8000 </option>
                                                <option value="8000-9000"> 8000-9000 </option>
                                                <option value="9000-10000"> 9000-10000 </option>
                                                <option value="10000+"> 10000+</option>					
                                            </select>

                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Trainer Notification States<span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control" multiple="multiple" name="other_state[]">
                                                <option value=""> - Select -</option>
                                                <option value="Arunachal Pradesh">Arunachal Pradesh</option><option value="Assam">Assam</option><option value="Andhra Pradesh">Andhra Pradesh</option><option value="Andaman &amp; Nicobar">Andaman &amp; Nicobar</option><option value="Bihar">Bihar</option><option value="Chandigarh">Chandigarh</option><option value="Chhattisgarh">Chhattisgarh</option><option value="Delhi">Delhi</option><option value="Daman &amp; Diu">Daman &amp; Diu</option><option value="Nagar Haveli">Nagar Haveli</option><option value="Gujarat">Gujarat</option><option value="Goa">Goa</option><option value="Himachal Pradesh">Himachal Pradesh</option><option value="Haryana">Haryana</option><option value="Jammu &amp; Kashmir">Jammu &amp; Kashmir</option><option value="Jharkhand">Jharkhand</option><option value="Karnataka">Karnataka</option><option value="Kerala">Kerala</option><option value="Lakshadweep">Lakshadweep</option><option value="Manipur">Manipur</option><option value="Mizoram">Mizoram</option><option value="Meghalaya">Meghalaya</option><option value="Madhya Pradesh">Madhya Pradesh</option><option value="Maharashtra">Maharashtra</option><option value="Nagaland">Nagaland</option><option value="Orissa">Orissa</option><option value="Punjab">Punjab</option><option value="Pondicherry">Pondicherry</option><option value="Rajasthan">Rajasthan</option><option value="Sikkim">Sikkim</option><option value="Telangana">Telangana</option><option value="Tripura">Tripura</option><option value="Tamil Nadu">Tamil Nadu</option><option value="Uttarakhand">Uttarakhand</option><option value="Uttar Pradesh">Uttar Pradesh</option><option value="West Bengal">West Bengal</option><option value="Other">Other</option>

                                            </select>
					     <br/>
					    <span>Choose states you want to notify trainers residing in them about the program</span>
                                        </div>
                                    </div>
                                    
                                    <div class="clearfix"></div>

                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                            <button type="reset" class="btn btn-default">Reset</button>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>


                                </form>


                                <?php
                            } else {
                                ?>
                                <form action="" method="POST"  enctype="multipart/form-data" id="add-program-form">

                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Client <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control" name="client">
                                                <option value=""> - Select -</option>
                                                <?php
                                                if (!empty($client)) {
                                                    foreach ($client as $cl_data) {
                                                        ?>
                                                        <option value="<?php echo $cl_data->client_id; ?>"> <?php echo $cl_data->client_name; ?> </option>
                                                        <?php
                                                    }
                                                }
                                                ?>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Participant Type <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control" name="participants_type">
                                                <option value=""> - Select -</option>
												<option value="Academics"> Academics </option>
                                                <option value="Banking"> Banking </option>
                                                <option value="Corporate"> Corporate </option>
												<option value="Govt"> Government </option>
                                                <option value="NGO"> NGO </option>
												<option value="Pharma"> Pharma </option>
                                                
                                                	

                                            </select>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Client / Participant Group Name <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="client_name" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Program Zone <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control" name="zone">
                                                <option value=""> - Select -</option>
                                                <option value="North"> North </option>
                                                <option value="East"> East </option>
                                                <option value="West"> West </option>
                                                <option value="South"> South </option>

                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Program Title <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="pname" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Program State<span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control" name="program_state">
                                                <option value=""> - Select -</option>
                                                <option value="Arunachal Pradesh">Arunachal Pradesh</option><option value="Assam">Assam</option><option value="Andhra Pradesh">Andhra Pradesh</option><option value="Andaman &amp; Nicobar">Andaman &amp; Nicobar</option><option value="Bihar">Bihar</option><option value="Chandigarh">Chandigarh</option><option value="Chhatisgarh">Chhatisgarh</option><option value="Delhi">Delhi</option><option value="Daman &amp; Diu">Daman &amp; Diu</option><option value="Nagar Haveli">Nagar Haveli</option><option value="Gujarat">Gujarat</option><option value="Goa">Goa</option><option value="Himachal Pradesh">Himachal Pradesh</option><option value="Haryana">Haryana</option><option value="Jammu &amp; Kashmir">Jammu &amp; Kashmir</option><option value="Jharkhand">Jharkhand</option><option value="Karnataka">Karnataka</option><option value="Kerala">Kerala</option><option value="Lakshadweep">Lakshadweep</option><option value="Manipur">Manipur</option><option value="Mizoram">Mizoram</option><option value="Meghalaya">Meghalaya</option><option value="Madhya Pradesh">Madhya Pradesh</option><option value="Maharashtra">Maharashtra</option><option value="Nagaland">Nagaland</option><option value="Orissa">Orissa</option><option value="Punjab">Punjab</option><option value="Pondicherry">Pondicherry</option><option value="Rajasthan">Rajasthan</option><option value="Sikkim">Sikkim</option><option value="Telangana">Telangana</option><option value="Tripura">Tripura</option><option value="Tamil Nadu">Tamil Nadu</option><option value="Uttarakhand">Uttarakhand</option><option value="Uttar Pradesh">Uttar Pradesh</option><option value="West Bengal">West Bengal</option><option value="Other">Other</option>

                                            </select>
                                        </div>
                                    </div>
                                    
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Location <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text" name="location" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Objective <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea name="objective" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Start Date <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="date" id="sdate" name="sdate"  placeholder="yy-mm-dd" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>



                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Duration in Days <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text"  name="duration" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12"> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label><input type="checkbox"  name="halfday" value='1'>Half Day Program</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12"><span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="checkbox"  name="excl_sat" value='Y'>Exclude Saturday
                                            <input type="checkbox"  name="excl_sun" value='Y'>Exclude Sunday
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Trainer Skill Sets <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <textarea name="skill_set" class="form-control"></textarea>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>

                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">No. of Participants <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <input type="text"  name="participants" class="form-control col-md-7 col-xs-12">
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
				  <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">Participants Level <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <select class="form-control" name="participant_level">
                                                <option value=""> - Select -</option>
                                                <option value="Beginners"> Beginners </option>
                                                <option value="Juniors"> Juniors </option>
                                                <option value="Intermediate"> Intermediate </option>
                                                <option value="Advanced"> Advanced </option>
                                                <option value="Highly Experienced"> Highly Experienced </option>
                                                <option value="Above 40 years Staff"> Above 40 years Staff </option>
                                                <option value="Unskilled Labor"> Unskilled Labor </option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12"> <span class="required">*</span>
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label><input type="checkbox" value="1" checked name="sendNoti"/> Send Email Notification to trainer.</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12"> </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12">
                                            <label><input type="checkbox"  name="invoice_status" value='0'>Trainer Invoice Not Required</label>
                                        </div>
                                    </div>
                                    <div class="clearfix"></div>
                                    
                                    <div class="form-group">
                                        <div class="label col-md-3 col-sm-3 col-xs-12">
                                        </div>
                                        <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                            <button type="reset" class="btn btn-default">Reset</button>
                                            <button type="submit" class="btn btn-primary">Submit</button>
                                        </div>
                                    </div>

                                </form>
                            <?php } ?>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php include 'js_files.php'; ?>

        <script type="text/javascript">

            $(document).ready(function () {


                $('#stay').on('change', function () {
                    if (this.value == "Wagons")
					{
                        $("#stay_limit_block").show();
						$("#stay_claim_block").show();
					}
					else if (this.value == "Trainer")
					{
                        $("#stay_limit_block").hide();
						$("#stay_claim_block").show();
					}						
                    else
					{
                        $("#stay_limit_block").hide();
						$("#stay_claim_block").hide();
					}
                });
                $('#travel').on('change', function () {
                    if (this.value == "Wagons")
					{
                        $("#travel_limit_block").show();
						$("#travel_claim_block").show();
					}
					 else if (this.value == "Trainer")
					{
                        $("#travel_limit_block").hide();
						$("#travel_claim_block").show();
					}
                    else
					{
                        $("#travel_limit_block").hide();
					    $("#travel_claim_block").hide();
					}
                });

                $("#add-program-form").validate({
                    rules: {
                        pname: "required",
                        client: "required",
                        location: "required",
                        participants_type: "required",
                        client_name: "required",
                        zone: "required",
                        sdate: "required",
                        duration: {
                            required: true,
                            number: true
                        },
                        skill_set: "required",
                        participants: {
                            required: true,
                            number: true
                        },
                        participant_level: "required",
                        stay: "required",
                        travel: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });


            });

        </script>

    </body>
</html>