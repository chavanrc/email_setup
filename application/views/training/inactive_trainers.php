<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Inactive Trainers</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Trainers</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li ><a href="<?php echo base_url(); ?>training/trainers"><i class="fa fa-info"></i> Active</a></li>
                            <li class="active"><a href=""><i class="fa fa-info"></i> Inactive</a></li>
                            <li><a href="<?php echo base_url(); ?>training/pending_trainers"><i class="fa fa-info"></i> Pending Approval</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
            <div class="row" style="margin: 0px;">
                
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list"></i> Inactive Trainers List </h2>
                            <a href="<?php echo base_url(); ?>training/add_trainer" class="btn btn-sm btn-primary pull-right" style="margin-top: -20px;"><i class="fa fa-user-plus"></i> Create New Trainer</a>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th style="width: 20%;">Name</th>
                                        <th style="width: 20%;">Contact</th>
                                        <th style="width: 20%;">City</th>
                                        <th style="width: 20%;">Date</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($trainer)) {
                                        $num = 0;
                                        foreach ($trainer as $tr_data) {
                                            $num++;
                                            ?>
                                            <tr>
                                                <td><?php echo $num; ?></td>
                                                <td><?php echo $tr_data->name; ?><br/>
                                                    
                                                </td>
                                                <td><?php echo $tr_data->email; ?><br/><?php echo $tr_data->contact_number; ?></td>
                                                
                                                <td><?php echo $tr_data->state.' <br/> '.$tr_data->city; ?>
                                                </td>
                                                
                                                
                                                <td><?php echo date_formate_short($tr_data->create_date); ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>training/trainer_profile/<?php echo $tr_data->user_code; ?>" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="assign-pm-wrap" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-arrow-right"></i> Assign Project Manager</h4>
                    </div>
                    <div class="modal-body">
                    <form action="" method="POST"  enctype="multipart/form-data" id="assign-pm-form">
                        <input type="hidden" value="" id="cid" name="cid"/>
                        <div class="form-group col-md-8 col-md-offset-2">
                                <select class="form-control" id="pm" name="pm">
                                    <option value=""> - Select Project Manager -</option>
                                    <?php
                                    foreach ($pm as $pm_data) {
                                        ?>
                                        <option value="<?php echo $pm_data->user_id; ?>"> <?php echo $pm_data->name; ?> </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                        </div>
                        <div class="clearfix"></div>
                        <div class="form-group col-md-8 col-md-offset-2 text-center">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                        </div>
                        <div class="clearfix"></div>
                    </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <?php include 'js_files.php'; ?>
        
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        
        <script type="text/javascript">
            $(document).ready(function () {
                
                $('#data-list').DataTable();
                
                $('.assign-pm-btn').click(function(e){
                   e.preventDefault();
                   $('#assign-pm-wrap').modal('show');
                   var cid = $(this).attr('cid');
                   $('#cid').val(cid);
                });
                

                $("#assign-pm-form").validate({
                    rules: {
                        pm: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        assign_pm();
                    }
                });

                function assign_pm()
                {
                    var cid = $('#cid').val();
                    var pm = $('#pm').val();

                    $('.page_spin').show();
                    var dataString = "cid=" + cid + "&pm=" + pm + "&page=assign_pm";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            $('#assign-pm-wrap').modal('hide');

                        }, //success fun end
                    });//ajax end
                }
            });
        </script>

    </body>
</html>