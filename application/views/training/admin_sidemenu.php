<?php
$CI = & get_instance();
$CI->load->model('admin_model');

$TR = & get_instance();
$TR->load->model('trainer_model');

?>
<div class="nav-side-menu no-print">
    <div class="brand"><img src="<?php echo base_url(); ?>assets/images/wagons-logo.png"/></div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

    <div class="menu-list">

        <ul id="menu-content" class="menu-content collapse out">
            <li <?php if ($page_url == 'Dashboard') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>training/">
                    <i class="fa fa-dashboard"></i> Dashboard
                </a>
            </li>
            
            <li <?php if ($page_url == 'Add Program') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>training/add_program">
                    <i class="fa fa-road"></i> Add New Program
                </a>
            </li>
            
            <li <?php if ($page_url == 'Upcoming Programs') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>training/upcoming_programs">
                    <i class="fa fa-calendar-plus-o"></i> Upcoming Programs
                </a>
            </li>
         
            <li <?php if ($page_url == 'Past Programs') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>training/past_programs">
                    <i class="fa fa-calendar-check-o"></i>Completed Programs
                </a>
            </li>
            <li <?php if ($page_url == 'Trainers') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>training/trainers">
                    <i class="fa fa-graduation-cap"></i> Trainers
                </a>
            </li>
            <li <?php if ($page_url == 'Past Programs') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>training/invoice_list">
                    <i class="fa fa-calendar-check-o"></i> Trainer Invoices
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="page_spin">
    <br/>
    <div class="spin_icon">
        <i class="fa fa-spinner fa-spin"></i><br/>
        <span>One moment ...</span>
    </div>
</div>
