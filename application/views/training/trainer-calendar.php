<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Trainer Profile</title>
        <?php include 'css_files.php'; ?>
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
        <style>
            .panel-body i{
                display: inline-block;
                width: 20px;
            }
            
            .top-label{
                padding:10px; font-size: 12px; border:solid 1px #73879C;
                margin-left: -10px;
                color:#fff;
            }
            
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Trainer</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li ><a href="<?php echo base_url(); ?>admin/trainer_profile/<?php echo $trainer[0]->user_code; ?>"><i class="fa fa-info"></i> Details</a></li>
                            <li><a href="<?php echo base_url(); ?>admin/trainer_experience/<?php echo $trainer[0]->user_code; ?>"><i class="glyphicon glyphicon-blackboard"></i> Training Experience</a></li>
                            <li><a href="<?php echo base_url(); ?>admin/trainer_program_engagment/<?php echo $trainer[0]->user_code; ?>"><i class="fa fa-paper-plane"></i> Program Engagement</a></li>
                            <li class="active"><a href="#"><i class="fa fa-calendar"></i> Calendar </a></li>
                            
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

            <div class="row" style="margin: 0px;">
                <div class="col-md-12 content-page">
                  

                    <div class="col-md-10">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-calendar"></i> Trainer Calendar </h2>
                            </div>
                            <div class="panel-body" style="line-height: 25px;">
                                <div id='calendar'></div>
                            </div>
                        </div>


                    </div>

                    <div class="clearfix"></div>
                   

                </div>
            </div>
        </div>


        <?php include 'js_files.php'; ?>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/moment.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/jquery.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.js'></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.assign-pm-btn').click(function (e) {
                    e.preventDefault();
                    $('#assign-pm-wrap').modal('show');
                    var cid = $(this).attr('cid');
                    $('#cid').val(cid);
                });


                $('#calendar').fullCalendar({
                    defaultDate: '2017-02-09',
                    editable: false,
                    eventLimit: true, // allow "more" link when too many events
                    events: [
                        <?php
                        $calendar = $CI->admin_model->get_trainer_calendor($trainer[0]->user_code);
                        if(!empty($calendar))
                        {
                            foreach($calendar as $cal_data)
                            {
                                ?>
                                         {
                            title: '<?php echo $cal_data->tc_title; ?>',
                            start: '<?php echo $cal_data->from_date; ?>',
                            end: '<?php echo $cal_data->to_date; ?>'
                        },       
                                <?php
                            }
                        }
                        ?>
                    ]
                });


                
                    
                
            });
        </script>

    </body>
</html>