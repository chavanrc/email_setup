<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Program Details</title>
        <?php include 'css_files.php'; ?>
        <style>
            thead:before, thead:after { display: none; }
            tbody:before, tbody:after { display: none; }
            table tr td{
                padding: 5px;
                text-align: center;
            }
            .rows{
                height: 80px;
                border-top: solid 1px #000;
                margin-left: 50px;
            }
            .rows-unit{
                height: 79px;
                font-size: 14px;
                position: relative;
                top: -5px
            }
            .y-title{
                text-align: left;
                display: inline-block;
                font-size: 18px;
                transform: rotate(-90deg);
                width: 300px;
                //margin-left: -100px;
            }
        </style>
        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    </head>
    <body>



        <div>
            <?php
            if (!empty($fGroup)) {
                $gno = 0;
                foreach ($fGroup as $fdata) {
                    ?>
                    <table class="table-bordered" style="width:100%; page-break-after: always;">
                        <?php
                        $gno++;
                        $datapoint = array();
                        if (!empty($fdata->fq_group)) {
                            ?>
                            <tr style="font-weight:bold; background:#5bc0de;">
                                <td style="width:320px; text-align: left;"><?php echo $fdata->fq_group; ?></td>
                                <td >5</td>
                                <td >4</td>
                                <td >3</td>
                                <td >2</td>
                                <td >1</td>
                                <td >Total</td>
                                <td >Average</td>
                            </tr>
                            <?php
                            $que = $this->admin_model->get_fdQuestions($fdata->fq_group);
                            if (!empty($que)) {
                                $g1 = $g2 = $g3 = $g4 = $g5 = 0;
                                foreach ($que as $qdata) {
                                    ?>
                                    <tr>
                                        <td style="text-align: left;"><?php echo $qdata->fq_title; ?></td>
                                        <td>
                                            <?php
                                            echo $total1 = $this->admin_model->get_avgAns($qdata->fq_id, '5', $pid);
                                            $g5 += $total1;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo $total2 = $this->admin_model->get_avgAns($qdata->fq_id, '4', $pid);
                                            $g4 += $total2;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo $total3 = $this->admin_model->get_avgAns($qdata->fq_id, '3', $pid);
                                            $g3 += $total3;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo $total4 = $this->admin_model->get_avgAns($qdata->fq_id, '2', $pid);
                                            $g2 += $total4;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            echo $total5 = $this->admin_model->get_avgAns($qdata->fq_id, '1', $pid);
                                            $g1 += $total5;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $gtotal = $total1 + $total2 + $total3 + $total4 + $total5;
                                            echo $gtotal;
                                            ?>
                                        </td>
                                        <td>
                                            <?php
                                            $avg = 0;
                                            if ($gtotal > 0) {
                                                $avg = (($total1 * 5) + ($total2 * 4) + ($total3 * 3) + ($total4 * 2) + ($total5 * 1)) / $gtotal;
                                                echo round($avg, 2);
                                            }
                                            ?>
                                        </td>
                                    </tr>

                                    <?php
                                    array_push($datapoint, array("y" => $avg, "label" => $qdata->fq_title));
                                }
                            }
                            $gg = $g5 + $g4 + $g3 + $g2 + $g1;
                            if ($gg > 0) {
                                ?>
                                <tr>
                                    <td class="text-right"><strong>Total</strong></td>
                                    <td>
                                        <?php echo $g5; ?><br/>
                                        <?php
                                        echo round(($g5 / $gg) * 100, 2) . '%';
                                        ?>
                                    </td>
                                    <td><?php echo $g4; ?><br/>
                                        <?php
                                        echo round(($g4 / $gg) * 100, 2) . '%';
                                        ?></td>
                                    <td><?php echo $g3; ?><br/>
                                        <?php
                                        echo round(($g3 / $gg) * 100, 2) . '%';
                                        ?></td>
                                    <td><?php echo $g2; ?><br/>
                                        <?php
                                        echo round(($g2 / $gg) * 100, 2) . '%';
                                        ?></td>
                                    <td><?php echo $g1; ?><br/>
                                        <?php
                                        echo round(($g1 / $gg) * 100, 2) . '%';
                                        ?></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td colspan="8" style="background:#fff; position: relative; padding: 15px;">
                                        <div style="height: 400px; position: relative; margin-top: 20px;">

                                            <div class="rows"></div>
                                            <div class="rows"></div>
                                            <div class="rows"></div>
                                            <div class="rows"></div>
                                            <div class="rows" style="border-bottom: solid 1px #000;"></div>
                                            <div style="position:relative; top:-400px;">

                                                <div style="height: 250px; width: 50px; margin-top: 150px  float: left;">
                                                    <span class="y-title">
                                                        Average Ratings (in points)
                                                    </span>
                                                </div>

                                                <div style="height: 400px; width: 50px; border-right: solid 1px #000; bottom: 0px; margin-left: -35px; float: left;">
                                                    <div class="rows-unit">5</div>
                                                    <div class="rows-unit">4</div>
                                                    <div class="rows-unit">3</div>
                                                    <div class="rows-unit">2</div>
                                                    <div class="rows-unit">1</div>
                                                </div>

                                                <?php
                                                $r = 0;
                                                foreach ($datapoint as $dy) {
                                                    $r++;
                                                    $d = 70;
                                                    $h = ($dy['y'] / 5) * 400;
                                                    $t = 400 - $h;
                                                    ?>
                                                    <div style="width:50px; background: red;height: <?php echo $h; ?>px; margin-top:<?php echo $t; ?>px; margin-left: <?php echo $d; ?>px; float:left;"></div>
                                                <?php }
                                                ?>
                                            </div>

                                        </div>
                                    </td>
                                </tr>
                                <?php
                            }
                        }
                        ?>
                    </table>
                    <?php
                }
            }
            ?>

        </div>

        <?php include 'js_files.php'; ?>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.5.3/jspdf.debug.js" integrity="sha384-NaWTHo/8YCBYJ59830LTz/P4aQZK1sS0SneOgAvhsIl3zBu8r9RevNg5lHCHAuQ/" crossorigin="anonymous"></script>
        <script type="text/javascript">
            $(document).ready(function () {


            });
        </script>

    </body>
</html>