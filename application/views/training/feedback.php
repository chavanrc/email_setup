<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Program Details</title>
        <?php include 'css_files.php'; ?>
        <style>
            .panel-body{
                line-height:25px;
            }
            @media print {
                .panel-body{
                    padding:0px !important;
                }
                .panel{
                    border:solid 0px;
                }
                a{
                    display:none;
                }
            }
        </style>
        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Programs</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li ><a href="<?php echo base_url(); ?>training/program_details/<?php echo $pid; ?>"><i class="fa fa-info"></i> Details</a></li>
                            <li><a href="<?php echo base_url(); ?>training/program_expenses/<?php echo $pid; ?>"><i class="fa fa-paper-plane"></i> Program Expenses</a></li>
                            <li class="active"><a href="<?php echo base_url(); ?>training/feedback/<?php echo $pid; ?>"><i class="fa fa-comments-o"></i> Feedback</a></li>


                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

            <div class="row" style="margin: 0px;">
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading no-print">
                            <h2 class="panel-title" style="display:inline-block;"><i class="fa fa-info"></i> Feedback </h2>

                            <a href="<?php echo base_url(); ?>training/add_feedback/<?php echo $pid; ?>" class="btn-sm btn-primary pull-right"> Add New Feedback</a>
                            <a href="#" class="btn-sm btn-success pull-right send-fd-btn" style="margin-right:10px;"> Send Feedback</a>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">

                            <?php
                            if (!empty($fGroup)) {
                                $gno = 0;
                                foreach ($fGroup as $fdata) {
                                    ?>
                                    <table class="table table-bordered" style="page-break-after: always;">
                                        <?php
                                        $gno++;
                                        $datapoint = array();
                                        if (!empty($fdata->fq_group)) {
                                            ?>
                                            <tr style="background:#5bc0de;  font-weight:bold;">
                                                <td><?php echo $fdata->fq_group; ?></td>
                                                <td>5</td>
                                                <td>4</td>
                                                <td>3</td>
                                                <td>2</td>
                                                <td>1</td>
                                                <td>Total</td>
                                                <td>Average</td>
                                            </tr>
                                            <?php
                                            $que = $CI->admin_model->get_fdQuestions($fdata->fq_group);
                                            if (!empty($que)) {
                                                $g1 = $g2 = $g3 = $g4 = $g5 = 0;
                                                foreach ($que as $qdata) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $qdata->fq_title; ?></td>
                                                        <td>
                                                            <?php
                                                            echo $total1 = $CI->admin_model->get_avgAns($qdata->fq_id, '5', $pid);
                                                            $g5 += $total1;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            echo $total2 = $CI->admin_model->get_avgAns($qdata->fq_id, '4', $pid);
                                                            $g4 += $total2;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            echo $total3 = $CI->admin_model->get_avgAns($qdata->fq_id, '3', $pid);
                                                            $g3 += $total3;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            echo $total4 = $CI->admin_model->get_avgAns($qdata->fq_id, '2', $pid);
                                                            $g2 += $total4;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            echo $total5 = $CI->admin_model->get_avgAns($qdata->fq_id, '1', $pid);
                                                            $g1 += $total5;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $gtotal = $total1 + $total2 + $total3 + $total4 + $total5;
                                                            echo $gtotal;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $avg = 0;
                                                            if ($gtotal > 0) {
                                                                $avg = (($total1 * 5) + ($total2 * 4) + ($total3 * 3) + ($total4 * 2) + ($total5 * 1)) / $gtotal;
                                                                echo round($avg, 2);
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>

                                                    <?php
                                                    array_push($datapoint, array("y" => $avg, "label" => $qdata->fq_title));
                                                }
                                            }
                                            $gg = $g5 + $g4 + $g3 + $g2 + $g1;
                                            if ($gg > 0) {
                                                ?>
                                                <tr>
                                                    <td class="text-right"><strong>Total</strong></td>
                                                    <td>
                                                        <?php echo $g5; ?><br/>
                                                        <?php
                                                        echo round(($g5 / $gg) * 100, 2) . '%';
                                                        ?>
                                                    </td>
                                                    <td><?php echo $g4; ?><br/>
                                                        <?php
                                                        echo round(($g4 / $gg) * 100, 2) . '%';
                                                        ?></td>
                                                    <td><?php echo $g3; ?><br/>
                                                        <?php
                                                        echo round(($g3 / $gg) * 100, 2) . '%';
                                                        ?></td>
                                                    <td><?php echo $g2; ?><br/>
                                                        <?php
                                                        echo round(($g2 / $gg) * 100, 2) . '%';
                                                        ?></td>
                                                    <td><?php echo $g1; ?><br/>
                                                        <?php
                                                        echo round(($g1 / $gg) * 100, 2) . '%';
                                                        ?></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="8">
                                                        <?php
                                                        if ($gno == 1) {
                                                            $w = '800px';
                                                        }
                                                        if ($gno == 2) {
                                                            $w = '100%';
                                                        }
                                                        if ($gno == 3) {
                                                            $w = '400px';
                                                        }
                                                        ?>
                                                        <div id="chartContainer<?php echo $gno; ?>" style="height: 450px; width: 800px;"></div>
                                                        <script type="text/javascript">

                                                            var chart = new CanvasJS.Chart("chartContainer<?php echo $gno; ?>", {
                                                                animationEnabled: true,
                                                                theme: "light1",
                                                                title: {
                                                                    text: " "
                                                                },
                                                                axisY: {
                                                                    title: "Average Rating (in points)"
                                                                },
                                                                axisX: {
                                                                    labelFontSize: 13,
                                                                    labelMaxWidth: 120,
                                                                    labelWrap: true,
                                                                    interval: 1
                                                                },
                                                                data: [{
                                                                        type: "column",
                                                                        yValueFormatString: "#,##0.## points",
                                                                        dataPoints: <?php echo json_encode($datapoint, JSON_NUMERIC_CHECK); ?>
                                                                    }]
                                                            });
                                                            chart.render();
                                                        </script>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </table>
                                    <?php
                                }
                            }
                            ?>

                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="modal fade" id="send-wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Send Feedback Report</h4>
                    </div>
                    <div class="modal-body">
                        <input type="email" id="email" placeholder="Email id" class="form-control"/>
                        <input type="hidden" id="pid" value="<?php echo $pid; ?>"/>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary submit-btn">Submit</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->



        <?php include 'js_files.php'; ?>

        <script type="text/javascript">
            $(document).ready(function () {
                $('.send-fd-btn').click(function (e) {
                    e.preventDefault();
                    $('#send-wraper').modal('show');
                });

                $('.submit-btn').click(function (e) {
                    var email = $('#email').val();
                    var id = $('#pid').val();
                    if (email != '') {
                        $('.page_spin').show();
                        var dataString = "id=" + id + "&email="+email+"&page=send_feedback";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                alert('Email sent successfully');
                                $('#email').val('');
                            }, //success fun end
                        });//ajax end
                    }
                });

            });
        </script>

    </body>
</html>