<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Trainers</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Programs</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li ><a href="<?php echo base_url(); ?>training/program_details/<?php echo $pid; ?>"><i class="fa fa-info"></i> Details</a></li>
                            <li><a href="<?php echo base_url(); ?>training/program_expenses/<?php echo $pid; ?>"><i class="fa fa-paper-plane"></i> Program Expenses</a></li>
                            <li class="active"><a href="<?php echo base_url(); ?>training/feedback/<?php echo $pid; ?>"><i class="fa fa-comments-o"></i> Feedback</a></li>


                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
            <div class="row" style="margin: 0px;">

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" style="display: inline-block;"> Add New Feedback </h2>
                            <a href="<?php echo base_url(); ?>training/feedback/<?php echo $pid; ?>" class="btn-sm btn-danger pull-right"> Back</a>
                            <div class="clearfix"></div>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 1) {
                                ?>
                                <div class="alert alert-success col-md-4 col-md-offset-4">
                                    Feedback Added Successfully.
                                </div>

                                <?php
                            }
                            if ($msg == 2) {
                                ?>
                                <div class="alert alert-warning col-md-4 col-md-offset-4">
                                    Failed !! This employee code is already exist.
                                </div>

                                <?php
                            }
                            ?>
                            <div class="clearfix"></div>
                            <form action="" method="POST" id="feedbackForm">
                                <div class="form-group">
                                    
                                    <input type="hidden" name="project" value="<?php echo $pid; ?>"/>
                                    
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Employee Code</span>
                                        <input type="text" name="pname" class="form-control"/>
                                    </div>

                                    <table class="table table-bordered" style="margin-top: 15px;">
                                        <?php
                                        if (!empty($fGroup)) {
                                            foreach ($fGroup as $fdata) {
                                                if (!empty($fdata->fq_group)) {
                                                    ?>
                                                    <tr class="label-info">
                                                        <td><?php echo $fdata->fq_group; ?></td>
                                                        <td>5</td>
                                                        <td>4</td>
                                                        <td>3</td>
                                                        <td>2</td>
                                                        <td>1</td>
                                                    </tr>
                                                    <?php
                                                    $que = $CI->admin_model->get_fdQuestions($fdata->fq_group);
                                                    if (!empty($que)) {
                                                        foreach ($que as $qdata) {
                                                            ?>
                                                            <tr>
                                                                <td style="width: 600px;"><?php echo $qdata->fq_title; ?></td>
                                                                <td>
                                                                    <label><input type="radio" value="5" name="ansR<?php echo $qdata->fq_id; ?>"/></label>
                                                                </td>
                                                                <td>
                                                                    <label><input type="radio" value="4" name="ansR<?php echo $qdata->fq_id; ?>"/></label>
                                                                </td>
                                                                <td>
                                                                    <label><input type="radio" value="3" name="ansR<?php echo $qdata->fq_id; ?>"/></label>
                                                                </td>
                                                                <td>
                                                                    <label><input type="radio" value="2" name="ansR<?php echo $qdata->fq_id; ?>"/></label>
                                                                </td>
                                                                <td>
                                                                    <label><input type="radio" value="1" name="ansR<?php echo $qdata->fq_id; ?>"/></label>
                                                                </td>
                                                            </tr>

                                                            <?php
                                                        }
                                                    }
                                                } else {
                                                    $que = $CI->admin_model->get_fdQuestions($fdata->fq_group);
                                                    if (!empty($que)) {
                                                        foreach ($que as $qdata) {
                                                            ?>
                                                            <tr>
                                                                <td colspan="6">
                                                                    <div style="padding: 10px 0px;"><?php echo $qdata->fq_title; ?></div>
                                                                    <textarea class="form-control" name="ansR<?php echo $qdata->fq_id; ?>"></textarea>
                                                                </td>
                                                            </tr>

                                                            <?php
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                    </table>


                                    <div class="text-right" style="margin-top: 15px;">
                                        <button class="btn btn-primary" type="submit">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <?php include 'js_files.php'; ?>

        <script type="text/javascript">
            $(document).ready(function () {

                $("#feedbackForm").validate({
                    rules: {
                        project: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

            });
        </script>

    </body>
</html>