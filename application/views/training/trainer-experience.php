<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Trainer Experience</title>
        <?php include 'css_files.php'; ?>
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
        <style>
            .panel-body i{
                display: inline-block;
                width: 20px;
            }
            .top-label{
                padding:10px; font-size: 12px; border:solid 1px #73879C;
                margin-left: -10px;
                color:#fff;
            }
            
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Trainer</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li><a href="<?php echo base_url(); ?>admin/trainer_profile/<?php echo $trainer[0]->user_code; ?>"><i class="fa fa-info"></i> Details</a></li>
                            <li class="active"><a href="#"><i class="glyphicon glyphicon-blackboard"></i> Training Experience</a></li>
                            <li><a href="<?php echo base_url(); ?>admin/trainer_program_engagment/<?php echo $trainer[0]->user_code; ?>"><i class="fa fa-paper-plane"></i> Program Engagement</a></li>
                            
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3> <?php echo $trainer[0]->name; ?></h3>
                    </div>
                    <div class="page-title title-right text-right">
                        
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                  
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-info"></i> Training Experience </h2>
                        </div>
                        <div class="panel-body" style="line-height: 25px;">
                            <?php
                            if(!empty($exp))
                            {
                                ?>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Program</th>
                                        <th>Training Area</th>
                                        
                                        <th>Industry</th>
                                        <th>Company</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    foreach($exp as  $ex_data)
                                    {
                                        ?>
                                    <tr>
                                        <td><?php echo $ex_data->program_title; ?></td>
                                        <td><?php echo $ex_data->area_of_training; ?></td>
                                        
                                        <td><?php echo $ex_data->industry; ?></td>
                                        <td><?php echo $ex_data->company; ?></td>
                                    </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>


        <?php include 'js_files.php'; ?>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/moment.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/jquery.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.js'></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.assign-pm-btn').click(function (e) {
                    e.preventDefault();
                    $('#assign-pm-wrap').modal('show');
                    var cid = $(this).attr('cid');
                    $('#cid').val(cid);
                });


                $('#calendar').fullCalendar({
                    defaultDate: '2017-02-09',
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    events: [
                        {
                            title: 'Admin Dahsboard',
                            start: '2017-02-09'
                        },
                        {
                            title: 'Long Event',
                            start: '2016-12-07',
                            end: '2016-12-10'
                        },
                        {
                            id: 999,
                            title: 'Repeating Event',
                            start: '2016-12-09T16:00:00'
                        },
                        {
                            id: 999,
                            title: 'Repeating Event',
                            start: '2016-12-16T16:00:00'
                        },
                        {
                            title: 'Conference',
                            start: '2016-12-11',
                            end: '2016-12-13'
                        },
                        {
                            title: 'Meeting',
                            start: '2016-12-12T10:30:00',
                            end: '2016-12-12T12:30:00'
                        },
                        {
                            title: 'Lunch',
                            start: '2016-12-12T12:00:00'
                        },
                        {
                            title: 'Meeting',
                            start: '2016-12-12T14:30:00'
                        },
                        {
                            title: 'Happy Hour',
                            start: '2016-12-12T17:30:00'
                        },
                        {
                            title: 'Dinner',
                            start: '2016-12-12T20:00:00'
                        },
                        {
                            title: 'Birthday Party',
                            start: '2016-12-13T07:00:00'
                        },
                        {
                            title: 'Click for Google',
                            url: 'http://google.com/',
                            start: '2016-12-28'
                        }
                    ]
                });


                $("#assign-pm-form").validate({
                    rules: {
                        pm: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        assign_pm();
                    }
                });

                $('.approved-btn').click(function(e){
                    var tid = $(this).attr('tid');
                    
                    $('.page_spin').show();
                    var dataString = "tid=" + tid + "&page=trainer_make_approve";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                });
                    
                
            });
        </script>

    </body>
</html>