<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Program Details</title>
        <?php include 'css_files.php'; ?>
        <style>
            .panel-body{
                line-height:25px;
            }
            @media print {
         .panel-body{
                padding:0px !important;
            }
            .panel{
                border:solid 0px;
            }
            a{
                display:none;
            }
      }
        </style>
        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
    </head>
    <body>

        <div class="right-side" style="margin-left:8%;">

            

            <div class="row" style="margin: 0px;">
                <div class="col-md-12 content-page">
                    <div class="text-center" style="background:#2a3f54; padding: 5px;">
                        <img src="http://sandbox.wagonslearning.com/assets/images/wagons-logo.png"/>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading no-print">
                            <h2 class="panel-title" style="font-size:25px;">Feedback </h2>
                            <div style="margin-top:20px;">
                                <strong>Project : </strong> <?php echo $project[0]->project_title; ?><br/>
                                <strong>Location : </strong> <?php echo $project[0]->location_of_training; ?><br/>
                                <strong>Venue : </strong> <?php echo $project[0]->venue; ?><br/>
                                <strong>Date : </strong> <?php echo date_formate_short($project[0]->training_start_date); ?><br/>
                            </div>

                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            
                                <?php
                                if (!empty($fGroup)) {
                                    $gno = 0;
                                    foreach ($fGroup as $fdata) {
                                        ?>
                                        <table class="table table-bordered" style="page-break-after: always;">
                                        <?php
                                        $gno++;
                                        $datapoint = array();
                                        if (!empty($fdata->fq_group)) {
                                            ?>
                                            <tr style="background:#5bc0de;  font-weight:bold;">
                                                <td><?php echo $fdata->fq_group; ?></td>
                                                <td>5</td>
                                                <td>4</td>
                                                <td>3</td>
                                                <td>2</td>
                                                <td>1</td>
                                                <td>Total</td>
                                                <td>Average</td>
                                            </tr>
                                            <?php
                                            $que = $this->admin_model->get_fdQuestions($fdata->fq_group);
                                            if (!empty($que)) {
                                                $g1 = $g2 = $g3 = $g4 = $g5 = 0;
                                                foreach ($que as $qdata) {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $qdata->fq_title; ?></td>
                                                        <td>
                                                            <?php
                                                            echo $total1 = $this->admin_model->get_avgAns($qdata->fq_id, '5',$pid);
                                                            $g5 += $total1;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            echo $total2 = $this->admin_model->get_avgAns($qdata->fq_id, '4',$pid);
                                                            $g4 += $total2;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            echo $total3 = $this->admin_model->get_avgAns($qdata->fq_id, '3',$pid);
                                                            $g3 += $total3;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            echo $total4 = $this->admin_model->get_avgAns($qdata->fq_id, '2',$pid);
                                                            $g2 += $total4;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            echo $total5 = $this->admin_model->get_avgAns($qdata->fq_id, '1',$pid);
                                                            $g1 += $total5;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $gtotal = $total1 + $total2 + $total3 + $total4 + $total5;
                                                            echo $gtotal;
                                                            ?>
                                                        </td>
                                                        <td>
                                                            <?php
                                                            $avg = 0;
                                                            if ($gtotal > 0) {
                                                                $avg = (($total1 * 5) + ($total2 * 4) + ($total3 * 3) + ($total4 * 2) + ($total5 * 1)) / $gtotal;
                                                                echo round($avg, 2);
                                                            }
                                                            ?>
                                                        </td>
                                                    </tr>

                                                    <?php
                                                    array_push($datapoint, array("y" => $avg, "label" => $qdata->fq_title));
                                                }
                                            }
                                            $gg = $g5 + $g4 + $g3 + $g2 + $g1;
                                            if ($gg > 0) {
                                                ?>
                                                <tr>
                                                    <td class="text-right"><strong>Total</strong></td>
                                                    <td>
                                                        <?php echo $g5; ?><br/>
                                                        <?php
                                                        echo round(($g5 / $gg) * 100, 2) . '%';
                                                        ?>
                                                    </td>
                                                    <td><?php echo $g4; ?><br/>
                                                        <?php
                                                        echo round(($g4 / $gg) * 100, 2) . '%';
                                                        ?></td>
                                                    <td><?php echo $g3; ?><br/>
                                                        <?php
                                                        echo round(($g3 / $gg) * 100, 2) . '%';
                                                        ?></td>
                                                    <td><?php echo $g2; ?><br/>
                                                        <?php
                                                        echo round(($g2 / $gg) * 100, 2) . '%';
                                                        ?></td>
                                                    <td><?php echo $g1; ?><br/>
                                                        <?php
                                                        echo round(($g1 / $gg) * 100, 2) . '%';
                                                        ?></td>
                                                    <td></td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td colspan="8">
                                                        <?php
                                                        if ($gno == 1) {
                                                            $w = '800px';
                                                        }
                                                        if ($gno == 2) {
                                                            $w = '100%';
                                                        }
                                                        if ($gno == 3) {
                                                            $w = '400px';
                                                        }
                                                        ?>
                                                        <div id="chartContainer<?php echo $gno; ?>" style="height: 450px; width: 800px;"></div>
                                                        <script type="text/javascript">

                                                            var chart = new CanvasJS.Chart("chartContainer<?php echo $gno; ?>", {
                                                                animationEnabled: true,
                                                                theme: "light1",
                                                                title: {
                                                                    text: " "
                                                                },
                                                                axisY: {
                                                                    title: "Average Rating (in points)"
                                                                },
                                                                axisX: {
                                                                    labelFontSize: 13,
                                                                    labelMaxWidth: 120,
                                                                    labelWrap: true,
                                                                    interval: 1
                                                                },
                                                                data: [{
                                                                        type: "column",
                                                                        yValueFormatString: "#,##0.## points",
                                                                        dataPoints: <?php echo json_encode($datapoint, JSON_NUMERIC_CHECK); ?>
                                                                    }]
                                                            });
                                                            chart.render();
                                                        </script>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                        </table>
                                        <?php
                                    }
                                }
                                ?>
                            
                        </div>
                    </div>
                </div>

            </div>
        </div>



        <?php include 'js_files.php'; ?>

        <script type="text/javascript">
            $(document).ready(function () {


            });
        </script>

    </body>
</html>