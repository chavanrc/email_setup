<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Trainers</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Trainers</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#"><i class="fa fa-info"></i> Active</a></li>
                            <li><a href="<?php echo base_url(); ?>training/inactive_trainers"><i class="fa fa-info"></i> Inactive</a></li>
                            <li><a href="<?php echo base_url(); ?>training/pending_trainers"><i class="fa fa-info"></i> Pending Approval</a></li>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
            <div class="row" style="margin: 0px;">

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list"></i> Active Trainers List </h2>
                            <?php
                            $page = 1;
                            $next = 2;
                            $totalPage = 0;
                            if (isset($_GET['page'])) {
                                $page = $_GET['page'];
                            }
                            if (!empty($count)) {
                                $totalPage = ceil($count[0]->total / 20);
                                if ($count[0]->total > 20) {
                                    ?>
                                    <h4 class="pull-right" style="display: inline-block; margin-left: 20px; margin-top: -10px;"><?php echo (($page - 1) * 20) + 1; ?> - <?php
                                        if ($page < $totalPage) {
                                            echo $page * 20;
                                        } else {
                                            echo $count[0]->total;
                                        }
                                        ?> of <?php echo $count[0]->total; ?> </h4>
                                    <?php
                                }
                            }
                            ?>
                            <a href="<?php echo base_url(); ?>training/add_trainer" class="btn btn-sm btn-primary pull-right" style="margin-top: -20px; margin-left: 20px;"><i class="fa fa-user-plus"></i> Create New Trainer</a>
                            <a href="<?php echo base_url(); ?>training/search_trainer" class="btn btn-sm btn-info pull-right" style="margin-top:-20px;"><i class="fa fa-search"></i> Search</a>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th style="width: 20%;">Name</th>
                                        <th style="width: 20%;">Contact</th>
                                        <th style="width: 20%;">City</th>
                                        <th style="width: 20%;">Date</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($trainer)) {
                                        $num = 0;
                                        foreach ($trainer as $tr_data) {
                                            $num++;
                                            ?>
                                            <tr>
                                                <td><?php echo $num; ?></td>
                                                <td><?php echo $tr_data->name; ?><br/>

                                                </td>
                                                <td><?php echo $tr_data->email; ?><br/><?php echo $tr_data->contact_number; ?></td>

                                                <td><?php echo $tr_data->state . ' <br/> ' . $tr_data->city; ?>
                                                </td>


                                                <td><?php echo date_formate_short($tr_data->create_date); ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>training/trainer_profile/<?php echo $tr_data->user_code; ?>" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                            <div class="text-right" style="font-size:16px; padding: 10px;">
                        <?php
                        if ($page > 1) {
                            $next = $page + 1;
                            $prev = $page - 1;
                            ?>
                            <a href="<?php echo base_url(); ?>training/trainers/?page=1"><i class="fa fa-backward" style="font-size:12px;"></i> First</a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>training/trainers/?page=<?php echo $prev; ?>"><i class="fa fa-caret-left"></i> Prev</a>
                            <?php
                        }
                        if ($page < $totalPage) {
                            ?>
                            &nbsp;&nbsp;<a href="<?php echo base_url(); ?>training/trainers/?page=<?php echo $next; ?>">Next <i class="fa fa-caret-right"></i> </a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>training/trainers/?page=<?php echo $totalPage; ?>">Last <i class="fa fa-forward" style="font-size:12px;"></i> </a>
                        <?php } ?>
                    </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="assign-pm-wrap" data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title"><i class="fa fa-arrow-right"></i> Assign Project Manager</h4>
                    </div>
                    <div class="modal-body">
                        <form action="" method="POST"  enctype="multipart/form-data" id="assign-pm-form">
                            <input type="hidden" value="" id="cid" name="cid"/>
                            <div class="form-group col-md-8 col-md-offset-2">
                                <select class="form-control" id="pm" name="pm">
                                    <option value=""> - Select Project Manager -</option>
                                    <?php
                                    foreach ($pm as $pm_data) {
                                        ?>
                                        <option value="<?php echo $pm_data->user_id; ?>"> <?php echo $pm_data->name; ?> </option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="clearfix"></div>
                            <div class="form-group col-md-8 col-md-offset-2 text-center">
                                <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <?php include 'js_files.php'; ?>

        <script type="text/javascript">
            $(document).ready(function () {

            });
        </script>

    </body>
</html>