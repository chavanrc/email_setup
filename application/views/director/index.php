<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Director | Dashboard</title>
        <?php include 'css_files.php'; ?>

    </head>
    <body>
        <?php
        $CI = & get_instance();
        $CI->load->model('admin_model');
        ?>

        <div>
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px; padding-top: 20px;">

                <div class="col-md-8">
                    <div class="box box-success">
                        <div class="box-header with-border text-center">
                            <h4 style="display: inline-block;">Total Programs</h4>
                        </div>
                        <div class="box-body">
                            <div id="chartContainer" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box box-success">
                        <div class="box-header with-border text-center">
                            <h4 style="display: inline-block;">Clients Program</h4>
                        </div>
                        <div class="box-body">
                            <div id="chartContainer2" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-12">
                    <div class="box box-success">
                        <div class="box-header with-border text-center">
                            <h4 style="display: inline-block;">Turnover Summary</h4>
                        </div>
                        <div class="box-body">
                            <div id="chartContainer1" style="height: 370px; width: 100%;"></div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>

                <div class="col-md-5">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h4 style="display: inline-block;">Client Invoice</h4>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Amount</th>
                                        <th>GST</th>
                                        <th>Total Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($amt)) {
                                        ?>
                                        <tr>
                                            <td>Professional Fees</td>
                                            <td>
                                                <?php echo number_format(round($amt[0]->amt)); ?>
                                            </td>
                                            <td>
                                                <?php echo number_format(round($amt[0]->gst)); ?>
                                            </td>
                                            <td>
                                                <?php echo $ptotal = number_format(round($amt[0]->amt) + round($amt[0]->gst)); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Debit Note</td>
                                            <td>
                                                <?php echo number_format(round($amt[0]->damt)); ?>
                                            </td>
                                            <td>
                                                <?php echo number_format(round($amt[0]->dgst)); ?>
                                            </td>
                                            <td>
                                                <?php echo $dtotal = number_format(round($amt[0]->damt) + round($amt[0]->dgst)); ?>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Total</td>
                                            <td><?php
                                                $tamt = round($amt[0]->damt) + round($amt[0]->amt);
                                                echo number_format($tamt);
                                                ?></td>
                                            <td><?php
                                                $tgst = round($amt[0]->dgst) + round($amt[0]->gst);
                                                echo number_format($tgst);
                                                ?></td>
                                            <td><?php echo number_format($tamt + $tgst); ?></td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h4 style="display: inline-block;">Trainer Invoice</h4>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>Item</th>
                                        <th>Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Professional Fees</td>
                                        <td>
                                            <?php
                                            echo number_format(round($pamt[0]->amt));
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Debit Note</td>
                                        <td>
                                            <?php
                                            echo number_format($damt[0]->amt);
                                            ?>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Total</td>
                                        <td><?php echo number_format(round($pamt[0]->amt) + round($damt[0]->amt)); ?></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

                <div class="col-md-7">
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h4 style="display: inline-block;">Client Payment Pending</h4>
                        </div>
                        <div class="box-body" style="height: 400px; overflow-y:scroll;">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Invoice</th>
                                        <th>Client</th>
                                        <th>Total Amount</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    $no = 0;
                                    if (!empty($pending)) {
                                        foreach ($pending as $pdata) {
                                            $no++;
                                            ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td>
                                                    <?php echo $pdata->num; ?>
                                                </td>
                                                <td>
                                                    <?php echo $pdata->cname; ?>
                                                </td>
                                                <td class="text-right">
                                                    <?php echo number_format($pdata->amt, 2); ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
                <div class="clearfix"></div>
            </div>
        </div>

        <?php
        include 'js_files.php';

        $dataPoints = array();
        if (!empty($prog)) {
            foreach ($prog as $pdata) {
                array_push($dataPoints, array("y" => $pdata->total, "label" => $pdata->mon));
            }
        }

        $tempExp = array();
        foreach ($exp as $edata) {
            $tempExp[$edata->mon] = $edata->amt;
        }

        $tempFees = array();
        foreach ($fees as $fdata) {
            if ($fdata->mon != 'May-2019') {
                if (array_key_exists($fdata->mon, $tempExp)) {
                    $amt = $tempExp[$fdata->mon] + $fdata->amt;
                } else {
                    $amt = $fdata->amt;
                }
                $tempFees[$fdata->mon] = $amt;
            }
        }
        

        $dataPoints1 = array();
        if (!empty($tempFees)) {
            foreach ($tempFees as $key => $data) {
                array_push($dataPoints1, array("y" => $data, "label" => $key));
            }
        }


        $income = array();
        if (!empty($income1)) {
            foreach ($income1 as $idata) {
                $income[$idata->mon] = $idata->amt;
            }
        }
        if (!empty($income2)) {
            foreach ($income2 as $idata) {
                $mon = $idata->mon . '-' . $idata->yr;
                if (array_key_exists($mon, $income)) {
                    $amt = $income[$mon] + $idata->amt;
                    $income[$mon] = $amt;
                } else {
                    $income[$mon] = $idata->amt;
                }
            }
        }

        if (!empty($income3)) {
            foreach ($income3 as $idata) {
                if (array_key_exists($idata->mon, $income)) {
                    $amt = $income[$idata->mon] + $idata->amt;
                    $income[$idata->mon] = $amt;
                } else {
                    $income[$idata->mon] = $idata->amt;
                }
            }
        }

        $dataPoints2 = array();
        if (!empty($income)) {
            foreach ($income as $key1 => $data1) {
                array_push($dataPoints2, array("y" => $data1, "label" => $key1));
            }
        }
        
        $dataDiff = array_diff_key($tempFees,$income);
        if(!empty($dataDiff)){
            foreach ($dataDiff as $key1 => $data1) {
                array_push($dataPoints2, array("y" => 0, "label" => $key1));
            }
        }
        
        $dataDiff = array_diff_key($income,$tempFees);
        if(!empty($dataDiff)){
            foreach ($dataDiff as $key1 => $data1) {
                array_push($dataPoints1, array("y" => 0, "label" => $key1));
            }
        }
        

        $dataPoints3 = array();

        $ctotal = 0;
        if (!empty($cprog)) {
            foreach ($cprog as $cdata) {
                $ctotal +=$cdata->total;
                array_push($dataPoints3, array("label" => $cdata->client_name, "y" => $cdata->total));
            }
        }

        $rtotal = 100 - $ctotal;
        array_push($dataPoints3, array("label" => "Others", "y" => $rtotal));
        ?>
        <script src="https://canvasjs.com/assets/script/canvasjs.min.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {

                $('#client-option').change(function () {
                    var cid = $(this).val();
                    if (cid != '') {
                        window.location.href = "<?php echo base_url(); ?>director/client/" + cid;
                    } else {
                        window.location.href = "<?php echo base_url(); ?>director";
                    }
                });

                var chart = new CanvasJS.Chart("chartContainer", {
                    theme: "light2",
                    title: {
                        text: "<?php echo $fy; ?>"
                    },
                    axisY: {
                        title: "Number of Programs"
                    },
                    data: [{
                            type: "line",
                            dataPoints: <?php echo json_encode($dataPoints, JSON_NUMERIC_CHECK); ?>
                        }]
                });
                chart.render();

                var chart1 = new CanvasJS.Chart("chartContainer1", {
                    theme: "light2",
                    title: {
                        text: "<?php echo $fy; ?>"
                    },
                    legend: {
                        cursor: "pointer",
                        verticalAlign: "center",
                        horizontalAlign: "right"
                    },
                    data: [{
                            type: "column",
                            name: "Expense",
                            showInLegend: true,
                            dataPoints: <?php echo json_encode($dataPoints1, JSON_NUMERIC_CHECK); ?>
                        }, {
                            type: "column",
                            name: "Income",
                            showInLegend: true,
                            dataPoints: <?php echo json_encode($dataPoints2, JSON_NUMERIC_CHECK); ?>
                        }]
                });
                chart1.render();

                var chart3 = new CanvasJS.Chart("chartContainer2", {
                    animationEnabled: true,
                    theme: "light2",
                    title: {
                        text: "<?php echo $fy; ?>"
                    },
                    data: [{
                            type: "pie",
                            yValueFormatString: "#,##0.00\"%\"",
                            indexLabel: "{label} ({y})",
                            dataPoints: <?php echo json_encode($dataPoints3, JSON_NUMERIC_CHECK); ?>
                        }]
                });
                chart3.render();

            });
        </script>

    </body>
</html>