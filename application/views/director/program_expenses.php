<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Program Engagement Requests</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Programs</a>
                    </div>
				
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                           <li><a href="<?php echo base_url(); ?>bdm/program_details/<?php echo $pid; ?>"><i class="fa fa-info"></i> Details</a></li>
                            
                            <li class="active"><a href="#"><i class="fa fa-money"></i> Program Expenses</a></li>
                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Program Expenses</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <!--<a href="#" class="btn btn-primary add-btn">Add Expense</a>-->
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Details</th>
                                        <th>Amount</th>
                                        <th>Note</th>
                                        <th>Added By</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>1</td>
                                        <td>
                                            Professional Fees
                                        </td>
                                        <td>
                                            <?php echo $pfess = ($fees[0]->duration*$fees[0]->total); ?>
                                        </td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                    <?php
                                    $trainer_subtotal = 0;
                                    $wagons_subtotal = $pfess;
                                    if (!empty($exp)) {
                                        $num = 1;
                                        foreach ($exp as $ex_data) {
                                            if($ex_data->added_by!='Trainer'){
                                            $num++;
                                            $wagons_subtotal += $ex_data->amount;
                                            ?>
                                            <tr>
                                                <td><?php echo $num; ?></td>
                                                <td><?php echo $ex_data->expense_type; ?></td>
                                                <td><?php echo number_format($ex_data->amount, 2); ?></td>
                                                <td><?php echo $ex_data->notes; ?></td>
                                                <td><?php echo $ex_data->added_by; ?></td>
                                            </tr>
                                            <?php
                                            }
                                        }
                                    }
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td colspan="3" ><strong><?php echo number_format($wagons_subtotal, 2); ?></strong></td></tr>
                                </tbody>
                            </table>
                            <div class="panel-heading">
                                <h2 class="panel-title" >Debit Note (Expenses Incurred by Trainer) </h2>
                            </div>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Details</th>
                                        <th>Amount</th>
                                        <th>Note</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($debit)) {
                                        $num = 0;
                                        foreach ($debit as $db_data) {
                                            if($db_data->debit_status==1){
                                            $trainer_subtotal += $db_data->debit_amt;
                                            }
                                            $num++;
                                            ?>
                                            <tr>
                                                <td><?php echo $num; ?></td>
                                                <td><?php echo $db_data->debit_title; ?></td>
                                                <td><?php echo number_format($db_data->debit_amt, 2); ?>  
                                                <?php
                                                if($db_data->debit_status==0){
                                                    echo '(Rejected)';
                                                }
                                                ?>
                                                </td>
                                                <td><?php echo $db_data->debit_desc; ?></td>
                                            </tr>
                                            <?php
                                        }
                                    }
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td colspan="2" ><strong><?php echo number_format($trainer_subtotal, 2); ?></strong></td></tr>
                                </tbody>
                            </table>
                            <div> <strong>Total Expenses: <?php echo number_format($wagons_subtotal + $trainer_subtotal, 2); ?></strong></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="expense_form" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add Expense</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="expense-form">

                        <div class="modal-body">
                            <input type="hidden" id="project" name="pid" value="<?php echo $pid; ?>"/>

                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Debit Type <sup>*</sup></span>
                                    <select class="form-control" name="dTitle">
                                        <option value="">- Select -</option>
                                        <option value="Courier Charges">Courier Shipment Charges</option>
                                        <option value="Food Expenses">Food Expenses</option>
                                        <option value="Lamination Charges">Lamination Charges</option>
                                        <option value="Misc. Charges">Misc. Charges</option>
                                        <option value="Stay Charges">Stay Charges</option>	
                                        <option value="Stationary Purchase">Stationary Purchase</option>	
                                        <option value="Training Props">Training Props Purchase</option>
                                        <option value="Traveling Charges">Traveling Charges</option>
                                        <option value="Xerox">Xerox</option>

                                    </select>
                                </div>
                            </div>



                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Description</span>
                                    <input type="text" name="desc" class="form-control"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Amount</span>
                                    <input type="text" name="amt" class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" >Submit</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {


                $("#expense-form").validate({
                    rules: {
                        dTitle: "required",
                        amt: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        assign_trainer();
                    }
                });


                $('.add-btn').click(function (e) {
                    e.preventDefault();
                    $('#expense_form').modal('show');
                });


            });
        </script>

    </body>
</html>