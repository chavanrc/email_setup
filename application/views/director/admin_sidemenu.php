<?php
$CI = & get_instance();
$CI->load->model('admin_model');

$TR = & get_instance();
$TR->load->model('trainer_model');

?>
<div class="nav-side-menu no-print">
    <div class="brand"><img src="<?php echo base_url(); ?>assets/images/wagons-logo.png"/></div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

    <div class="menu-list">

        <ul id="menu-content" class="menu-content collapse out">
            <li <?php if ($page_url == 'Dashboard') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>bdm/">
                    <i class="fa fa-dashboard"></i> Dashboard
                </a>
            </li>
            
            
        </ul>
    </div>
</div>

<div class="page_spin no-print">
    <br/>
    <div class="spin_icon">
        <i class="fa fa-spinner fa-spin"></i><br/>
        <span>One moment ...</span>
    </div>
</div>
