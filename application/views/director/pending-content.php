<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-9 page-title">
                            <h3>Content Pending Modules</h3>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page" style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" style="display: inline-block;"><?php echo $content[0]->client; ?></h2>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" style="font-size: 13px;">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Module Name</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php
                                if (!empty($content)) {
                                    $num = 0;
                                    foreach ($content as $cdata) {
                                        $num++;
                                        ?>
                                        <tr>
                                            <td><?php echo $num; ?></td>
                                            <td><?php echo $cdata->module_name; ?></td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="2">No Record found</td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#data-list').DataTable();
            });
        </script>

    </body>
</html>