<div class="top_nav no-print">
    <img src="<?php echo base_url(); ?>assets/images/wagons-logo.png" class="pull-left" style="margin-left: 20px;"/>
    <select class="form-control pull-left" name="client" id="client-option" style="width:300px; margin-top: 3px; margin-left: 50px;">
        <option value=""> All Clients </option>
        <?php
        if(!empty($client)){
            foreach($client as $cldata){
                ?>
        <option value="<?php echo $cldata->client_id; ?>" <?php if(!empty($cid)){ if($cid==$cldata->client_id){ echo 'selected'; } } ?> ><?php echo $cldata->client_name; ?></option>
                <?php
            }
        }
        ?>
    </select>
                <div class="nav_menu nav_menu_right">
                    <ul class="nav-menu-list">
                        <li class="nav-menu-list-item" style="padding-right: 40px;">
                            <a href="#" data-toggle="dropdown" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                                <?php echo $this->session->userdata('t_name'); ?>
                                <span class="fa fa-angle-down"></span>
                            </a>
                            <ul class="dropdown-menu" aria-labelledby="dLabel">
                                <li><a href="<?php echo base_url(); ?>bdm/profile"><i class="fa fa-user"></i> Profile </a></li>
                                <li><a href="<?php echo base_url(); ?>admin/bdm_logout"><i class="fa fa-sign-out"></i> Logout </a></li>
                            </ul>
                        </li>

                    </ul>
                </div>
                <div class="clearfix"></div>
            </div>