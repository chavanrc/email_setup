<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Program Details</title>
        <?php include 'css_files.php'; ?>
        <style>
            .panel-body{
                line-height:25px;
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>

            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">

                        <a class="navbar-brand" href="#">Programs</a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#"><i class="fa fa-info"></i> Details</a></li>

                            <li><a href="<?php echo base_url(); ?>bdm/program_expenses/<?php echo $program[0]->project_id; ?>"><i class="fa fa-money"></i> Program Expenses</a></li>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3><?php echo $program[0]->project_title; ?></h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <?php 
                        $sdate = explode(' ', $program[0]->training_start_date);
                        $tdate = date('Y-m-d');
                        $past = 0;
                        if(strtotime($sdate[0])<  strtotime($tdate)){
                            $past = 1;
                        }
                        ?>
                        
                        <a href="<?php echo base_url(); ?>bdm/client_programs/<?php echo $program[0]->client_id; ?><?php if(isset($_GET['page'])){ echo '?page='.$_GET['page']; } ?>" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a>
                        
                    </div>
                    <div class="clearfix"></div>
                </div>



                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-info"></i> Programs Details </h2>
                        </div>
                        <div class="panel-body">
                            <div class="col-md-4">Program Title <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->project_title; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Client <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->client_name; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4"><strong>Program Status <span class="pull-right">:</span> </strong></div><div class="col-md-8"><strong><?php if($program[0]->is_active=='1'){ echo 'Active'; } else { echo 'Canceled'; } ?></strong></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Location <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->location_of_training; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Start Date <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->training_start_date; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Duration <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->training_duration; ?> Days <?php if($program[0]->half_day=='1'){ ?><strong style="color:#ED1212;">( Half Day Program )</strong><?php } ?> </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Skill Sets <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->trainer_skillsets; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Participants <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->no_of_participants; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Objectives <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->objective_of_training; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Stay Arrangements <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->stay_arrangement; ?> 
                                <?php
                                if (($program[0]->stay_arrangement == "Wagons") || ($program[0]->stay_arrangement == "Trainer")){
                                    echo "<strong style='color:#ED1212;'>( Limit of Rs. " . $program[0]->stay_limit;
                                    ?>
                                    <?php
                                    if ($program[0]->claim_stay_cost == "Yes") {
                                        echo "; Claim with Client ) </strong>";
                                    } else {
                                        echo "; Wagons to Absorb )</strong>";
                                    }
                                }
                                ?>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Travel Arrangements <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->travel_arrangement; ?>
                                <?php
                                if (($program[0]->travel_arrangement == "Wagons") || ($program[0]->travel_arrangement == "Trainer")) {
                                    echo "<strong style='color:#ED1212;'>( Limit Rs. " . $program[0]->travel_limit;
                                    ?>
                                    <?php
                                    if ($program[0]->claim_travel_cost == "Yes") {
                                        echo "; Claim with Client ) </strong>";
                                    } else {
                                        echo "; Wagons to Absorb ) </strong>";
                                    }
                                }
                                ?>

                            </div>
                            <div class="clearfix"></div>

                            <div class="col-md-4">Venue  <span class="pull-right">:</span></div><div class="col-md-8"> <?php if (!empty($program[0]->venue)) { ?><?php echo $program[0]->venue; ?> <?php } else { ?><strong>Not Updated</strong> <?php } ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">SPOC <span class="pull-right">:</span></div><div class="col-md-8"><?php if (!empty($program[0]->spoc)) { ?><?php echo $program[0]->spoc; ?><?php } else { ?><strong>Not Updated</strong> <?php } ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Client Program Charges Amount<span class="pull-right">:</span></div><div class="col-md-8">Rs. <?php echo $program[0]->client_charges; ?> / Day</div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Trainer Fee Range <span class="pull-right">:</span></div><div class="col-md-8">Rs. <?php echo $program[0]->trainer_fee_range; ?> / Day</div>
                            <div class="clearfix"></div>

                        </div>
                    </div>
                </div>

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-graduation-cap"></i> Trainers </h2>
                        </div>
                        <div class="panel-body">
                            <p>Profession Fee Range: Rs.<?php echo $program[0]->trainer_fee_range; ?></p>

                            <?php
                            $tn = $this->projectmanager_model->get_program_trainers($program[0]->project_id);

                            if (!empty($tn)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Trainer</th>
                                            <th>Date</th>
                                            <th>Payment</th>
                                            <th>Checkin</th>
                                            <th>Checkout</th>
                                            <th>Location</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($tn as $tn_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $tn_data->name; ?></td>
                                                <td><?php echo date_formate_short($tn_data->program_date.' 00:00:00'); ?></td>
                                                <td><?php echo $tn_data->amount; ?></td>
                                                <td>
                                                    <?php
                                                    if ($tn_data->trainer_checkin_status == '1') {
                                                        echo date_formate($tn_data->trainer_checkin_time);
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($tn_data->trainer_checkout_status == '1') {
                                                        echo date_formate($tn_data->trainer_checkout_time);
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($tn_data->trainer_checkin_status == '1') {
                                                        ?>
                                                    <a href="https://maps.google.com/?q=<?php echo $tn_data->trainer_lat; ?>,<?php echo $tn_data->trainer_long; ?>" target="_blank"><?php echo $tn_data->trainer_lat; ?>, <?php echo $tn_data->trainer_long; ?></a>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <?php
                if ($program[0]->props == '1') {
                    ?>

                    <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-star"></i> Props </h2>

                            </div>
                            <div class="panel-body">
                                <?php
                                if ($msg == 'Props') {
                                    ?>
                                    <div class="alert alert-success col-md-6 col-md-offset-3">
                                        Delivery details updated Successfully.
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php
                                $props = $this->projectmanager_model->get_project_props($program[0]->project_id);
                                if (!empty($props)) {
                                    ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Quantity</th>
                                                <th>Uploaded By</th>
                                                <th>Requested Date</th>
                                                <th>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($props as $p_data) {
                                                if ($p_data->delivery_status != '0') {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $p_data->prop_name; ?></td>
                                                        <td><?php echo $p_data->quantity; ?></td>

                                                        <td><?php echo $p_data->name; ?> - <?php echo $p_data->user_type; ?></td>
                                                        <td><?php echo date_formate_short($p_data->requested_date); ?></td>
                                                        <td><?php
                                                            if ($p_data->delivery_status == '0') {
                                                                echo 'Not Shipped';
                                                            }
                                                            if ($p_data->delivery_status == '2') {
                                                                echo 'Shipped';
                                                            }
                                                            if ($p_data->delivery_status == '1') {
                                                                echo 'Delivered';
                                                            }
                                                            if ($p_data->delivery_status == '3') {
                                                                echo 'Approved';
                                                            }
                                                            ?></td>

                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            $cr = $this->projectmanager_model->get_courier($program[0]->project_id);
                                            ?>
                                            <tr style="background:#ddd;">
                                                <td></td>
                                                <td>
                                                    Courier Name : <?php
                                                    if (!empty($cr)) {
                                                        echo $cr[0]->cr_cname;
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    Courier Id : <?php
                                                    if (!empty($cr)) {
                                                        echo $cr[0]->cr_cid;
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    Courier Amt : <?php
                                                    if (!empty($cr)) {
                                                        echo $cr[0]->cr_amt;
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list-alt"></i> Forms </h2>
                            <!--<a href="#" class="btn btn-sm btn-info pull-right upload-form-btn" style="margin-top:-24px;"><i class="fa fa-upload"></i> Upload</a>-->
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 'Form') {
                                ?>
                                <div class="alert alert-success col-md-6 col-md-offset-3">
                                    Form Added Successfully.
                                </div>
                                <?php
                            }
                            ?>
                            <?php
                            $form = $this->projectmanager_model->get_project_form($program[0]->project_id);
                            if (!empty($form)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Form Type</th>
                                            <th>Uploaded By</th>
                                            <th>Program Date</th>
                                            <th>Uploaded Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($form as $fm_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $fm_data->form_type; ?></td>

                                                <td><?php echo $fm_data->name; ?> - <?php echo $fm_data->user_type; ?></td>
                                                <td><?php echo date_formate_short($fm_data->program_date); ?></td>
                                                <td><?php echo date_formate_short($fm_data->upload_date); ?></td>
                                                <td>
                                                    
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>
                
                <?php
                if ($program[0]->stay_arrangement == 'Wagons' || $program[0]->stay_arrangement == 'Client') {
                    ?>
                    <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-bed"></i> Stay Arrangements </h2>
                            </div>
                            <div class="panel-body">
                                <?php
                                if ($msg == 'Stay') {
                                    ?>
                                    <div class="alert alert-success col-md-6 col-md-offset-3">
                                        Stay details Updated Successfully.
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php
                                $stay = $this->projectmanager_model->get_stay_details($program[0]->project_id);
                                if (!empty($stay)) {
                                    ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Trainer</th>
                                                <th>Hotel</th>
                                                <th>Check in</th>
                                                <th>Check out</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($stay as $st_data) {
                                                if ($st_data->request_status != '0') {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $st_data->name; ?></td>
                                                        <td><?php echo $st_data->hotel_name; ?></td>
                                                        <td><?php echo date_formate($st_data->checkin_time); ?></td>
                                                        <td><?php echo date_formate($st_data->checkout_time); ?></td>

                                                        <td>
                                                            <?php
                                                            if ($st_data->request_status == '0') {
                                                                echo 'Approval Pending';
                                                            } if ($st_data->request_status == '2') {
                                                                echo 'Approved';
                                                            } if ($st_data->request_status == '1') {
                                                                echo 'Booked';
                                                                ?>
                                                                <br/>
                                                                <a href="<?php echo base_url(); ?>assets/upload/stay/<?php echo $st_data->booking_screenshot; ?>" target="_blank">Ticket/Voucher</a>
                                                                <?php
                                                            }
                                                            ?>
                                                        </td>
                                                        <td>
                                                            
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
				<?php } if ($program[0]->travel_arrangement == 'Wagons' || $program[0]->travel_arrangement == 'Client') { ?>
                    <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-cab"></i> Travel Arrangements </h2>
                            </div>
                            <div class="panel-body">
                                <?php
                                if ($msg == 'Travel') {
                                    ?>
                                    <div class="alert alert-success col-md-6 col-md-offset-3">
                                        Travel details Updated Successfully.
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php
                                $travel = $this->projectmanager_model->get_travel_details($program[0]->project_id);
                                if (!empty($travel)) {
                                    ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Trainer</th>
                                                <th>Date</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Mode</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($travel as $tv_data) {
                                                if ($tv_data->request_status != '0') {
                                                    ?>
                                                    <tr>
                                                        <td><?php echo $tv_data->name; ?></td>
                                                        <td><?php echo date_formate_short($tv_data->date); ?></td>
                                                        <td><?php echo $tv_data->from_loc; ?></td>
                                                        <td><?php echo $tv_data->to_loc; ?></td>
                                                        <td><?php echo $tv_data->mode; ?></td>
                                                        <td>
                                                            <?php
                                                            if ($tv_data->request_status == '0') {
                                                                echo 'Approval Pending';
                                                            } if ($tv_data->request_status == '2') {
                                                                echo 'Approved';
                                                            } if ($tv_data->request_status == '1') {
                                                                echo 'Booked';
                                                                ?>
                                                                <br/>
                                                                <a href="<?php echo base_url(); ?>assets/upload/travel/<?php echo $tv_data->screenshot; ?>" target="_blank">Ticket</a>
                                                            </td>
                                                        <?php } ?>
                                                        <td>
                                                            
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-image"></i> Images </h2>
                            <!--<a href="#" class="btn btn-sm btn-info pull-right images-add-btn" style="margin-top:-24px;"><i class="fa fa-upload"></i> Upload</a>-->
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 'Image') {
                                ?>
                                <div class="alert alert-success col-md-6 col-md-offset-3">
                                    Images Updated Successfully.
                                </div>
                                <div class="clearfix"></div>
                                <?php
                            }
                            ?>
                            <?php
                            $img = $this->projectmanager_model->get_project_images($program[0]->project_id);
                            if (!empty($img)) {
                                foreach ($img as $im_data) {
                                    ?>

                                    <div class="col-md-3" style="overflow:hidden;">
                                        <a href="<?php echo base_url(); ?>App/assets/upload/<?php echo $im_data->ti_img; ?>" style="display:inline-block; padding: 8px; border:solid 1px #ddd;"><img src="<?php echo base_url(); ?>App/assets/upload/<?php echo $im_data->ti_img; ?>" height="150px"/></a>

                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            
        </div>

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {

                $('.create-invoice-btn').click(function (e) {
                    e.preventDefault();
                    $('#clientWraper').modal('show');
                });

                $('.upload-form-btn').click(function (e) {
                    e.preventDefault();
                    $('#upload_form_wraper').modal('show');
                });

                $('.props-add-btn').click(function (e) {
                    e.preventDefault();
                    $('#props_add_wraper').modal('show');
                });

                $('.upload-content-btn').click(function (e) {
                    e.preventDefault();
                    $('#upload_content_wraper').modal('show');
                });

                $('.book-stay').click(function (e) {
                    e.preventDefault();
                    var sid = $(this).attr('sid');
                    $('#stay_form_wraper').modal('show');
                    $('#stayId').val(sid);
                });

                $('.book-travel').click(function (e) {
                    e.preventDefault();
                    var sid = $(this).attr('tid');
                    $('#travel_form_wraper').modal('show');
                    $('#travelId').val(sid);
                });

                $('.images-add-btn').click(function (e) {
                    e.preventDefault();
                    $('#upload_image_wraper').modal('show');
                });


                $('.assign-pm-btn').click(function (e) {
                    e.preventDefault();
                    $('#assign-pm-wrap').modal('show');
                    var cid = $(this).attr('cid');
                    $('#cid').val(cid);
                });

                $("#creatinForm").validate({
                    rules: {
                        gst: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

//                $.validator.addMethod("check_box", function (value, element) {
//                   // return this.optional(element) || /^[a-z0-9\\-]+$/i.test(value);
//                    return false;
//                }, "This is required");

                $("#add-stay-form").validate({
                    rules: {
                        stay_amt: "required",
                        screen: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#ship-form").validate({
                    rules: {
                        cname: "required",
                        cid: "required",
                        camt: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#add-travel-form").validate({
                    rules: {
                        travel_amt: "required",
                        screen: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#props-add-form").validate({
                    rules: {
                        pname: "required",
                        qnt: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#add-content-form").validate({
                    rules: {
                        title: "required",
                        content: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#add-forms-form").validate({
                    rules: {
                        content: "required",
                        ftype: "required",
                        fdate: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                function update_stay() {
                    var check = $(".trainer_list:checked").length;
                    alert(check);

                }

                $('.prop-status').click(function (e) {
                    e.preventDefault();
                    var sid = $(this).attr('sid');
                    var pid = "<?php echo $program[0]->project_id; ?>";
                    var status = $(this).attr('status');
                    $(this).remove();
                    $('.page_spin').show();
                    var dataString = "sid=" + sid + "&pid=" + pid + "&status=" + status + "&page=update_props_status";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            alert('Status updated');
                        }, //success fun end
                    });//ajax end
                });







                $('.remove-content').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove content ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_content";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.remove-form').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove form ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_form";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.remove-props').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove props ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_props";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.prop-status-btn').click(function (e) {
                    e.preventDefault();
                    $('#ship_wraper').modal('show');
                });

            });
        </script>
        <script>
            function initMap() {
                var loc = {lat: <?php echo $cIn[0]->trainer_lat; ?>, lng: <?php echo $cIn[0]->trainer_long; ?>};
                var map = new google.maps.Map(document.getElementById('googleMap'), {
                    zoom: 16,
                    center: loc
                });
                var marker = new google.maps.Marker({
                    position: loc,
                    map: map
                });
            }
        </script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBg6EjX8z0Kt41VMSZBEiLTbpMmRiMZHok&callback=initMap"></script>

    </body>
</html>