<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-3 page-title">
                            <h3>Trainer Invoices</h3>
                        </div>
                        <div class="col-md-9">
                            <?php
                            $page = 1;
                            $next = 2;
                            $totalPage = 0;
                            if (isset($_GET['page'])) {
                                $page = $_GET['page'];
                            }
                            if (!empty($count)) {
                                $totalPage = ceil($count[0]->total / 20);
                                if ($count[0]->total > 20) {
                                    ?>
                                    <h4 class="pull-right" style="display: inline-block;"><?php echo (($page - 1) * 20) + 1; ?> - <?php
                                        if ($page < $totalPage) {
                                            echo $page * 20;
                                        } else {
                                            echo $count[0]->total;
                                        }
                                        ?> of <?php echo $count[0]->total; ?> </h4>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page" style="margin-top: 10px;">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" style="display: inline-block;"><i class="fa fa-list"></i> Trainer Invoice List </h2>
                            <a href="<?php echo base_url(); ?>bdm/export_trainer_invoice" class="btn btn-sm btn-success pull-right" style=" margin-right: 20px;"><i class="fa fa-file-excel-o"></i> Export</a>
                            <div class="clearfix"></div>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" style="font-size: 13px;">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Program</th>
                                        <th>Trainer</th>
                                        <th>Location</th>
                                        <th>Client</th>
                                        <th>Start Date</th>
                                        <th>End Date</th>
                                        <th>Invoice Date</th>
                                        <th>Status</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                                <form action="<?php echo base_url(); ?>bdm/trainer_invoices" method="POST">
                                    <tr>
                                        <td></td>
                                        <td>
                                            <input type="text" class="form-control" placeholder="Program" name="title"/>
                                        </td>
                                        <td>
                                            <input type="text" class="form-control" placeholder="Trainer" name="trainer"/>
                                        </td>


                                        <td colspan="6">
                                            <input type="date" class="form-control" placeholder="Start Date" name="sdate" style="display: inline-block; width: 155px;"/>
                                            to
                                            <input type="date" class="form-control" placeholder="Start Date" name="tdate" style="display: inline-block;  width: 155px;"/>

                                            <select class="form-control" name="status" style="display: inline-block; width: 130px;">
                                                <option value="All"> All </option>
                                                <option value="0"> Pending </option>
                                                <option value="1"> Paid </option>
                                            </select>
                                        </td>
                                        <td>
                                            <button class="btn-sm btn-primary"><i class="fa fa-search"></i></button>
                                        </td>
                                    </tr>
                                </form>
                                <?php
                                if (!empty($payment)) {
                                    $num = 0;
                                    if (isset($_GET['page'])) {
                                        $num = 20 * ($_GET['page'] - 1);
                                    }
                                    foreach ($payment as $pm_data) {
                                        $num++;
                                        ?>
                                        <tr>
                                            <td><?php echo $num; ?></td>
                                            <td><?php echo $pm_data->project_title; ?></td>
                                            <td><?php echo $pm_data->name; ?></td>
                                            <td><?php echo $pm_data->location_of_training; ?></td>
                                            <td><?php echo $pm_data->client_name; ?></td>

                                            <td><?php echo date_formate_short($pm_data->training_date_from); ?></td>
                                            <td><?php echo date_formate_short($pm_data->trainer_date_to); ?></td>
                                            <td><?php echo date_formate_short($pm_data->trainer_invoice_date); ?></td>
                                            <td>
                                                <?php
                                                if ($pm_data->trainer_paid_status == '0') {
                                                    ?>
                                                    Pending
                                                    <?php
                                                }
                                                if ($pm_data->trainer_paid_status == '1') {
                                                    ?>
                                                    Paid
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                            <td>
                                                <a href="<?php echo base_url(); ?>bdm/trainer_inovice_details/<?php echo $pm_data->trainer_id; ?>/<?php echo $pm_data->project_id; ?>?f=invoie&page=<?php echo $page; ?>" class="btn btn-sm btn-info"><i class="fa fa-eye"></i></a>
                                            </td>
                                        </tr>
                                        <?php
                                    }
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="7">No Record found</td>
                                    </tr>
                                    <?php
                                }
                                ?>
                                </tbody>
                            </table>
                            <div class="text-right" style="font-size:16px; padding: 10px;">
                                <?php
                                if ($page > 1) {
                                    $next = $page + 1;
                                    $prev = $page - 1;
                                    ?>
                                    <a href="<?php echo base_url(); ?>bdm/trainer_invoices/?page=1"><i class="fa fa-backward" style="font-size:12px;"></i> First</a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>bdm/trainer_invoices/?page=<?php echo $prev; ?>"><i class="fa fa-caret-left"></i> Prev</a>
                                    <?php
                                }
                                if ($page < $totalPage) {
                                    ?>
                                    &nbsp;&nbsp;<a href="<?php echo base_url(); ?>bdm/trainer_invoices/?page=<?php echo $next; ?>">Next <i class="fa fa-caret-right"></i> </a>
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a href="<?php echo base_url(); ?>bdm/trainer_invoices/?page=<?php echo $totalPage; ?>">Last <i class="fa fa-forward" style="font-size:12px;"></i> </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {

                $('#data-list').DataTable();


                $('.pay-btn').click(function (e) {
                    e.preventDefault();
                    var pid = $(this).attr('pid');
                    var f = confirm("Are you sure want make payment ?");
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "pid=" + pid + "&page=update_payment";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

            });
        </script>

    </body>
</html>