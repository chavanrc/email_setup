<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Add bank Details</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Bank Details</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>trainer/trainer_profile/<?php echo $trainer[0]->user_code; ?>" class="btn btn-danger"><i class="fa fa-backward"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12 content-page">
                    <?php
                    if($msg==1)
                    {
                        ?>
                    <div class="alert alert-success col-md-6 col-md-offset-3">
                        Bank Details updated
                    </div>
                        <?php
                    }
                    
                    $aname = "";
                    $bname = "";
                    $anumber = "";
                    $ifsc = "";
                    $branch = "";
		    $pan = "";
                    
                    if(!empty($bank)){
                        $aname = $bank[0]->account_name;
                        $bname = $bank[0]->bank_name;
                        $anumber = $bank[0]->bank_account;
                        $ifsc = $bank[0]->bank_ifsc;
                        $branch = $bank[0]->bank_branch;
			$pan = $bank[0]->pan_no;
			$pan_image = $bank[0]->pan_image;
                    }
                    
                    ?>
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-plus"></i> Update Bank Details </h2>
                        </div>
                        <div class="panel-body" class="col-md-6">
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-bank-form">
                                <input type="hidden" value="<?php echo $trainer[0]->user_code; ?>" name="user"/>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">

                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Account holder Name </span>
                                        <input type="text" name="aname" value="<?php echo $aname; ?>" class="form-control">
                                    </div>

                                </div>
                                <div class="clearfix"></div>

                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Bank Name </span>
                                        <input type="text" name="bname" value="<?php echo $bname; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Account Number</span>
                                        <input type="text" name="anumber" value="<?php echo $anumber; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6 col-sm-3 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> IFSC Code</span>
                                        <input type="text" name="ifsc" value="<?php echo $ifsc; ?>" class="form-control">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group col-md-6 col-sm-3 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"  id="sizing-addon1"> Branch Name</span>
                                        <input type="text" name="branch"  value="<?php echo $branch; ?>" class="form-control">
                                    </div>
                                </div>
				<div class="clearfix"></div>
                                <div class="form-group col-md-6 col-sm-3 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"  id="sizing-addon1"> PAN No.</span>
                                        <input type="text" name="pan_no"  value="<?php echo $pan; ?>" class="form-control">
                                    </div>
                                </div>
				<div class="clearfix"></div>
                                <div class="form-group col-md-6 col-sm-3 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon"  id="sizing-addon1"> Upload PAN Image</span>
                                        <input type="file" name="pan_image"  class="form-control">
                                    </div>
                                </div>
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3" >
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
			
                    </div>
                </div>

            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">

            $(document).ready(function () {
                $("#add-bank-form").validate({
                    rules: {
                        aname: "required",
                        bname: "required",
                        anumber: "required",
                        ifsc: "required",
                        branch: "required",
			pan_no: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

            });

        </script>

    </body>
</html>