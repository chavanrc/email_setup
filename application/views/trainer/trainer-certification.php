<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Trainer | Certifications</title>
        <?php include 'css_files.php'; ?>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/multiselect/bootstrap-multiselect.css">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Add Certification</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>trainer/trainer_profile/<?php echo $trainer[0]->user_code; ?>" class="btn btn-danger"><i class="fa fa-backward"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <?php
                    if($msg==1)
                    {
                        ?>
                    <div class="alert alert-success col-md-6 col-md-offset-3">
                        Certification Updated Successfully.
                    </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-plus"></i> <?php echo $trainer[0]->name; ?> </h2>
                        </div>
                        <div class="panel-body">
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-certification-form">
                                <input type="hidden" value="<?php echo $trainer[0]->user_code; ?>" name="user"/>
                                
                                <div class="form-group col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Certification Name</span>
                                        <input type="text" name="title" placeholder="Certification Name" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Level if Any</span>
                                        <input type="text" name="level" placeholder="Level if Any" class="form-control">
                                    </div>
                                </div>
<!--                                <div class="form-group col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Valid Upto</span>-->
                                        <input type="hidden" name="valid" placeholder="Validity" class="form-control">
<!--                                    </div>
                                </div>-->
                                
                                <div class="form-group col-md-6">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Awarded By</span>
                                        <input type="text" name="award" placeholder="Awarded By" class="form-control">
                                    </div>
                                </div>
                                
                                
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script src="<?php echo base_url(); ?>assets/js/multiselect/bootstrap-multiselect.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#industry').multiselect({
                    nonSelectedText: '- Select Function -'
                });
                
                $("#add-certification-form").validate({
                    rules: {
                        title:"required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });
                
            });
        </script>

    </body>
</html>