<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Trainer | Program Details Hi</title>
        <?php include 'css_files.php'; ?>
        <style>
            .panel-body{
                line-height:25px;
            }
        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>

            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3><?php // print_r($program); echo $program[0]->project_title;                             ?></h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>trainer/upcoming_programs" class="btn btn-danger"><i class="fa fa-arrow-left"></i> Back</a>
                        <?php
                        $today = date("Y-m-d");
                        $program_date = date("Y-m-d", strtotime($program[0]->training_start_date));
                        if ($program[0]->invoice_status == '1') {
                            if ($fees[0]->trainer_invoice_flag == 0) {
                                if ($today > $program_date) {
                                    if (empty($fees[0]->trainer_invoice_gst)) {
                                        ?>
                                        <div class="pull-right" style="width:150px; position: relative;">
                                            <a href="#" class="btn btn-primary dropdown-toggle" id="drop3" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"> Generate Invoice <span class="caret"></span> </a> 
                                            <ul class="dropdown-menu" aria-labelledby="drop3"> 
                                                <li><a href="#" class="genInvoiceBtn">Tax Invoice</a></li> 
                                                <li><a href="<?php echo base_url(); ?>trainer/invoice/<?php echo $program[0]->project_id; ?>">Normal Invoice</a></li> 
                                            </ul>
                                        </div>

                                        <?php
                                    } else {
                                        ?>
                                        <a href="<?php echo base_url(); ?>trainer/invoice/<?php echo $program[0]->project_id; ?>" class="btn btn-success">Invoice</a>
                                        <?php
                                    }
                                }
                            } else {
                                if ($fees[0]->trainer_invoice_flag != 4) {
                                    if (empty($fees[0]->trainer_invoice_file)) {
                                        ?>
                                        <a href="<?php echo base_url(); ?>trainer/invoice/<?php echo $program[0]->project_id; ?>"  class="create-invoice-btn btn btn-primary" target="_blank" >Print/Download Invoice</a><br/>
                                        <?php
                                    } else {
                                        ?>
                                        <a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $fees[0]->trainer_invoice_file; ?>"  class="create-invoice-btn btn btn-primary" target="_blank" >Print/Download Invoice</a><br/>  
                                        <?php
                                    }
                                    echo 'Invoice Generated On : ' . date_formate_short($fees[0]->trainer_invoice_date);
                                }
                            }
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-info"></i> Programs Details </h2>
                        </div>
                        <div class="panel-body">
                            <p><strong>Project Manager:</strong> <?php echo $program[0]->name; ?>; Contact No: <?php echo $program[0]->contact_number; ?>
                            <div class="col-md-4">Program Title <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->project_title; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Client <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->client_name; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4"><strong>Program Status <span class="pull-right">:</span> </strong></div><div class="col-md-8"><strong><?php
                                    if ($program[0]->is_active == '1') {
                                        echo 'Active';
                                    } else {
                                        echo 'Canceled';
                                    }
                                    ?></strong></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Location <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->location_of_training; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">State <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->program_state; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Start Date <span class="pull-right">:</span></div><div class="col-md-8"><?php echo date_formate_short($program[0]->training_start_date); ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Duration <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->training_duration; ?> Days 
							<?php if($program[0]->half_day=="1") { echo "<strong>( Half Day )</strong>";  } ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Skill Sets <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->trainer_skillsets; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Participants <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->no_of_participants; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Objectives <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->objective_of_training; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Stay Arrangements <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->stay_arrangement; ?></div>
                            <div class="clearfix"></div>
                            <div class="col-md-4">Travel Arrangements <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->travel_arrangement; ?></div>
                            <div class="clearfix"></div>
                            <?php if (!empty($program[0]->venue)) { ?>
                                <div class="col-md-4">Venue  <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->venue; ?></div>
                                <div class="clearfix"></div>
                                <div class="col-md-4">SPOC <span class="pull-right">:</span></div><div class="col-md-8"><?php echo $program[0]->spoc; ?></div>
                                <div class="clearfix"></div>
                            <?php } ?>

                            <div class="clearfix"></div>
                        </div>
                    </div>
                </div>
                <?php
                $ttt = $this->projectmanager_model->get_ttt($this->session->userdata('t_code'), $program[0]->project_id);
                if (!empty($ttt)) {
                    ?>
                    <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-calendar"></i> TTT Schedules </h2>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Date</th>
                                            <th>Time</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 0;
                                        $tstatus = 0;
                                        foreach ($ttt as $ts) {
                                            if ($ts->ttt_status == '1') {
                                                $tstatus = 1;
                                            }
                                        }
                                        if ($tstatus == 1) {
                                            foreach ($ttt as $tt_data) {
                                                if ($tt_data->ttt_status == '1') {
                                                    $no++;
                                                    ?>
                                                    <tr>
                                                        <td style="width:50px"><?php echo $no; ?></td>
                                                        <td style="width:150px"><?php echo date_formate_short($tt_data->ttt_date); ?></td>
                                                        <td style="width:150px"><?php echo $tt_data->ttt_time; ?></td>
                                                        <td>
                                                            <?php
                                                            if ($tt_data->ttt_status == '1') {
                                                                $tstatus = 1;
                                                                ?>
                                                                <i class="fa fa-check" aria-hidden="true" style="color:green;"></i>
                                                                <?php
                                                            }
                                                            if ($tstatus == 0) {
                                                                ?>
                                                                <a href="#" tid="<?php echo $tt_data->id; ?>" pid="<?php echo $tt_data->project_id; ?>" class="btn-sm btn-primary ttt-accept-btn">Accept</a>
                                                            <?php } ?>
                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                        } else {
                                            foreach ($ttt as $tt_data) {
                                                $no++;
                                                ?>
                                                <tr>
                                                    <td style="width:50px"><?php echo $no; ?></td>
                                                    <td style="width:150px"><?php echo date_formate_short($tt_data->ttt_date); ?></td>
                                                    <td style="width:150px"><?php echo $tt_data->ttt_time; ?></td>
                                                    <td>
                                                        <?php
                                                        if ($tt_data->ttt_status == '1') {
                                                            $tstatus = 1;
                                                            ?>
                                                            <i class="fa fa-check" aria-hidden="true" style="color:green;"></i>
                                                            <?php
                                                        }
                                                        if ($tstatus == 0) {
                                                            ?>
                                                            <a href="#" tid="<?php echo $tt_data->id; ?>" pid="<?php echo $tt_data->project_id; ?>" class="btn-sm btn-primary ttt-accept-btn">Accept</a>
                                                        <?php } ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>
                <?php
                $cIn = $CI->projectmanager_model->getCheckin($program[0]->project_id, $this->session->userdata('t_code'));
                if (!empty($cIn)) {
                    ?>
                    <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" style="display:inline-block;"><i class="fa fa-list-alt"></i>Checkin & Checkout <span style='font-size:10px; color:#ED1515;'></span></h2>

                            </div>
                            <div class="panel-body">

                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Date</th>
                                            <th>Checkin</th>
                                            <th>Checkout</th>
                                            <th>Location</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($cIn as $cin_data) {
                                            ?>
                                            <tr>
                                                <td>
                                                    <?php echo date_formate_short($cin_data->program_date . ' 00:00:00'); ?>
                                                </td>
                                                <td>
                                                    <?php echo date_formate($cin_data->trainer_checkin_time); ?>
                                                </td>
                                                <td>
                                                    <?php if ($cin_data->trainer_checkout_status == 1) { ?>
                                                        <?php echo date_formate($cin_data->trainer_checkout_time); ?>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($cin_data->trainer_checkin_status == '1') {
                                                        ?>
                                                        <a href="https://maps.google.com/?q=<?php echo $cin_data->trainer_lat; ?>,<?php echo $cin_data->trainer_long; ?>" target="_blank"><?php echo $cin_data->trainer_lat; ?>, <?php echo $cin_data->trainer_long; ?></a>
                                                    <?php } ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" style="display:inline-block;"><i class="fa fa-list-alt"></i> Contents <span style='font-size:10px; color:#ED1515;'>Will be made available 3 days prior to program</span></h2>
                            <?php
                            $checkContent = $this->projectmanager_model->checkContent($program[0]->project_id, $this->session->userdata('t_code'));

                            if ($checkContent != '0') {
                                ?>
                                <a href="#" class="btn-sm btn-success pull-right content-download-btn" mid="<?php echo $checkContent; ?>"><i class="fa fa-download"></i> Download Content</a>
                                <?php
                            }
                            ?>
                        </div>
                        <div class="panel-body">
                            <?php
                            $check = $this->projectmanager_model->check_content_send($program[0]->project_id);
                            if (!empty($check)) {
                                if ($check[0]->content_download_date != '0000-00-00 00:00:00') {
                                    ?>
                                    <div style="padding:10px;">
                                        <span>
                                            <b> Previous Download Date :</b> <?php echo date_formate($check[0]->content_download_date); ?>
                                        </span>
                                    </div>
                                    <?php
                                }
                            }

                            $content = $this->projectmanager_model->get_project_content($program[0]->project_id, $program[0]->client_id);
                            if (!empty($content)) {
                                ?>
                                <h5>Content List</h5>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Content Title</th>
                                            <th>For</th>
                                            <th>Uploaded By</th>
                                            <th>Uploaded Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($content as $cnt_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $cnt_data->filename; ?></td>
                                                <td>
                                                    <?php
                                                    if ($cnt_data->project_id == '0') {
                                                        echo 'Client';
                                                    } else {
                                                        echo 'Program';
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php echo $cnt_data->name; ?> - <?php echo $cnt_data->user_type; ?></td>
                                                <td><?php echo date_formate_short($cnt_data->upload_date); ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $cnt_data->file_path; ?>" class="btn btn-primary"><i class="fa fa-download"></i> View</a>
                                                    <?php
                                                    if ($cnt_data->upload_by == $this->session->userdata('t_code')) {
                                                        ?>
                                                        <a href="#" class="btn btn-sm btn-danger remove-content" rid="<?php echo $cnt_data->id; ?>"><i class="fa fa-trash"></i></a>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>



                <?php if ($program[0]->props == "1") { ?>
                    <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-star"></i> Props <span style='font-size:10px; color:#ED1515;'>Must be mentioned 5 days prior to program date</span></h2>
                                <?php if ($program[0]->diff >= 5) { ?><a href="#" class="btn btn-sm btn-info pull-right props-add-btn" style="margin-top:-24px;"><i class="fa fa-plus"></i> Request Props</a><?php } ?>
                            </div>
                            <div class="panel-body">
                                <?php
                                if ($msg == 'Props') {
                                    ?>
                                    <div class="alert alert-success col-md-6 col-md-offset-3">
                                        Props Updated Successfully.
                                    </div>
                                    <?php
                                }

                                $props = $this->projectmanager_model->get_project_props($program[0]->project_id);
                                if (!empty($props)) {
                                    ?>

                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Quantity</th>

                                                <th>Requested Date</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($props as $p_data) {
                                                ?>
                                                <tr>
                                                    <td><?php echo $p_data->prop_name; ?></td>
                                                    <td><?php echo $p_data->quantity; ?></td>


                                                    <td><?php echo date_formate_short($p_data->requested_date); ?></td>
                                                    <td><?php
                                                        if ($p_data->delivery_status == '0') {
                                                            echo 'Approval Pending';
                                                        }
                                                        if ($p_data->delivery_status == '2') {
                                                            echo 'Shipped';
                                                        }
                                                        if ($p_data->delivery_status == '1') {
                                                            echo 'Delivered';
                                                        }
                                                        if ($p_data->delivery_status == '3') {
                                                            echo 'Approved';
                                                        }
                                                        ?></td>
                                                    <td>
                                                        <?php
                                                        if ($p_data->requested_by == $this->session->userdata('t_code') && $p_data->delivery_status == '0') {
                                                            ?>
                                                            <a href="#" class="btn btn-sm btn-danger remove-props" rid="<?php echo $p_data->id; ?>"><i class="fa fa-trash"></i></a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            $cr = $this->projectmanager_model->get_courier($program[0]->project_id);
                                            ?>
                                            <tr><td colspan="5"><strong>Notes:</strong> <?php echo $p_data->comments; ?></td>
                                            <tr style="background:#eee;">

                                                <td colspan="5">
                                                    Shipping Details: <?php
                                                    if (!empty($cr)) {
                                                        
                                                    } else {
                                                        echo "Not Shipped";
                                                    }
                                                    if (!empty($cr)) {
                                                        foreach ($cr as $crData) {
                                                            echo $crData->cr_type . ' : ' . $crData->cr_cname . ' ; Ref#: ' . $crData->cr_cid . '<br/>';
                                                        }
                                                    }
                                                    ?>
                                                </td>

                                            </tr>

                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                <?php
                if ($program[0]->stay_arrangement == 'Wagons' || $program[0]->stay_arrangement == 'Client') {
                    ?>
                    <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-bed"></i> Stay Arrangements <span style='font-size:10px; color:#ED1515;'>Must be mentioneed 5 days prior to program date.</span></h2>
                                <?php if ($program[0]->diff >= 5) { ?><a href="#" class="btn btn-sm btn-info pull-right stay-update-btn" style="margin-top:-24px;"><i class="fa fa-edit"></i> Mention Preferences</a><?php } ?>
                            </div>
                            <div class="panel-body">
                                <?php
                                if ($msg == 'Stay') {
                                    ?>
                                    <div class="alert alert-success col-md-6 col-md-offset-3">
                                        Stay details Updated Successfully.
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php
                                $stay = $this->projectmanager_model->get_stay_details($program[0]->project_id);
                                if (!empty($stay)) {
                                    ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>

                                                <th>Hotel</th>
                                                <th>Check in</th>
                                                <th>Check out</th>
                                                <th>Notes</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($stay as $st_data) {
                                                ?>
                                                <tr>

                                                    <td><?php echo $st_data->hotel_name; ?></td>
                                                    <td><?php echo date_formate($st_data->checkin_time); ?></td>
                                                    <td><?php echo date_formate($st_data->checkout_time); ?></td>
                                                    <td><?php echo $st_data->comments; ?>
                                                        <?php
                                                        if ($st_data->pay_status == "Trainer to Pay") {
                                                            echo "<br/>Payment: Trainer to Pay Rs." . $st_data->expense_amount . " @ Hotel and send bills";
                                                        } else {
                                                            echo "<br/>Payment: Booking is paid";
                                                        }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($st_data->request_status == '0') {
                                                            echo 'Approval Pending';
                                                        } if ($st_data->request_status == '2') {
                                                            echo 'Approved';
                                                        } if ($st_data->request_status == '1') {
                                                            echo 'Booked';
                                                            ?>
                                                            <?php if ($st_data->booking_screenshot != "") { ?>
                                                                <br/>
                                                                <a href="<?php echo base_url(); ?>assets/upload/stay/<?php echo $st_data->booking_screenshot; ?>" target="_blank">Ticket/Voucher</a>
                                                            <?php } ?>
                                                        <?php }
                                                        ?>
                                                    </td>
                                                    <td>
                                                        <?php
                                                        if ($st_data->direct_book == '0') {
                                                            ?>
                                                            <i class="fa fa-hand-pointer-o" aria-hidden="true" style="font-size:18px; font-weight: bold;"></i>
                                                            <?php
                                                        }
                                                        if ($st_data->direct_book == '1') {
                                                            ?>
                                                            <i class="fa fa-check-square-o" aria-hidden="true" style="font-size:18px; font-weight: bold;"></i>
                                                            <?php
                                                        }
                                                        if ($st_data->request_status == '0') {
                                                            ?>
                                                            <a href="#" class="btn btn-sm btn-danger remove-stay" rid="<?php echo $st_data->id; ?>"><i class="fa fa-trash"></i></a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                <?php }
                ?>
                <?php
                if ($program[0]->travel_arrangement == 'Wagons' || $program[0]->travel_arrangement == 'Client') {
                    ?>
                    <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-cab"></i> Travel Arrangements <span style='font-size:10px; color:#ED1515;'>Must be mentioned 3 days prior to program date.</span></h2>
                                <?php if ($program[0]->diff >= 1) { ?><a href="#" class="btn btn-sm btn-info pull-right travel-update-btn" style="margin-top:-24px;"><i class="fa fa-edit"></i>Mention Preferences</a><?php } ?>
                            </div>
                            <div class="panel-body">
                                <?php
                                if ($msg == 'Travel') {
                                    ?>
                                    <div class="alert alert-success col-md-6 col-md-offset-3">
                                        Travel details Updated Successfully.
                                    </div>
                                    <?php
                                }
                                ?>
                                <?php
                                $travel = $this->projectmanager_model->get_travel_details($program[0]->project_id);
                                if (!empty($travel)) {
                                    ?>
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>

                                                <th>Date</th>
                                                <th>From</th>
                                                <th>To</th>
                                                <th>Mode</th>
                                                <th>Notes</th>
                                                <th>Status</th>
                                                <th></th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php
                                            foreach ($travel as $tv_data) {
                                                ?>
                                                <tr>

                                                    <td><?php echo date_formate_short($tv_data->date); ?></td>
                                                    <td><?php echo $tv_data->from_loc; ?></td>
                                                    <td><?php echo $tv_data->to_loc; ?></td>
                                                    <td><?php echo $tv_data->mode; ?></td>
                                                    <td><?php echo $tv_data->comments; ?></td>
                                                    <td>
                                                        <?php
                                                        if ($tv_data->request_status == '0') {
                                                            echo 'Approval Pending';
                                                        } if ($tv_data->request_status == '2') {
                                                            echo 'Approved';
                                                        } if ($tv_data->request_status == '1') {
                                                            echo 'Booked';
                                                            ?>
                                                            <?php if ($tv_data->screenshot != "") { ?>
                                                                <br/>
                                                                <a href="<?php echo base_url(); ?>assets/upload/travel/<?php echo $tv_data->screenshot; ?>" target="_blank">Ticket</a>
                                                            <?php } ?>
                                                        </td>
                                                    <?php } ?>
                                                    <td>
                                                        <?php
                                                        if ($tv_data->direct_book == '0') {
                                                            ?>
                                                            <i class="fa fa-hand-pointer-o" aria-hidden="true" style="font-size:18px; font-weight: bold;"></i>
                                                            <?php
                                                        }
                                                        if ($tv_data->direct_book == '1') {
                                                            ?>
                                                            <i class="fa fa-check-square-o" aria-hidden="true" style="font-size:18px; font-weight: bold;"></i>
                                                            <?php
                                                        }
                                                        if ($tv_data->request_status == '0') {
                                                            ?>
                                                            <a href="#" class="btn btn-sm btn-danger remove-travel" rid="<?php echo $tv_data->id; ?>"><i class="fa fa-trash"></i></a>
                                                            <?php
                                                        }
                                                        ?>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>
                                        </tbody>
                                    </table>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>


                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list-alt"></i> Upload Forms </h2>
                            <a href="#" class="btn btn-sm btn-info pull-right upload-form-btn" style="margin-top:-24px;"><i class="fa fa-upload"></i> Upload</a>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 'Form') {
                                ?>
                                <div class="alert alert-success col-md-6 col-md-offset-3">
                                    Form Added Successfully.
                                </div>
                                <?php
                            }
                            ?>
                            <?php
                            $form = $this->projectmanager_model->get_project_form($program[0]->project_id);
                            if (!empty($form)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Form Type</th>
                                            <th>Uploaded By</th>
                                            <th>Program Date</th>
                                            <th>Uploaded Date</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($form as $fm_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $fm_data->form_type; ?></td>

                                                <td><?php echo $fm_data->name; ?> - <?php echo $fm_data->user_type; ?></td>
                                                <td><?php echo date_formate_short($fm_data->program_date); ?></td>
                                                <td><?php echo date_formate_short($fm_data->upload_date); ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>assets/upload/form/<?php echo $fm_data->form_file_name; ?>" class="btn btn-primary"><i class="fa fa-download"></i> View</a>
                                                    <?php
                                                    if ($fm_data->upload_by == $this->session->userdata('t_code')) {
                                                        ?>
                                                        <a href="#" class="btn btn-sm btn-danger remove-form" rid="<?php echo $fm_data->id; ?>"><i class="fa fa-trash"></i></a>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                </div>

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-image"></i> Images </h2>
                            <a href="#" class="btn btn-sm btn-info pull-right images-add-btn" style="margin-top:-24px;"><i class="fa fa-upload"></i> Upload</a>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 'Image') {
                                ?>
                                <div class="alert alert-success col-md-6 col-md-offset-3">
                                    Images Updated Successfully.
                                </div>
                                <div class="clearfix"></div>
                                <?php
                            }
                            ?>
                            <?php
                            $img = $this->projectmanager_model->get_project_images($program[0]->project_id);
                            if (!empty($img)) {
                                foreach ($img as $im_data) {
                                    ?>

                                    <div class="col-md-3" style="overflow:hidden;">
                                        <a href="<?php echo base_url(); ?>App/assets/upload/<?php echo $im_data->ti_img; ?>" style="display:inline-block; padding: 8px; border:solid 1px #ddd;"><img src="<?php echo base_url(); ?>App/assets/upload/<?php echo $im_data->ti_img; ?>" height="130px"/></a>
                                        <div class="text-center" style="position:absolute; top: 5px; left: 5px; width: 50px; height: 50px;">
                                            <a href="#" class="btn-sm btn-danger remove-img" rid="<?php echo $im_data->ti_id; ?>" iname="<?php echo $im_data->ti_img; ?>"><i class="fa fa-trash"></i></a>
                                        </div>

                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="modal fade" id="props_add_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Props Needed</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="props-add-form">

                        <div class="modal-body">
                            <div class="row">
                                <div class="form-group col-md-8">
                                    <input type="text" name="pname1"  class="form-control" placeholder="Props Name"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text" name="qnt1"  class="form-control" placeholder="Quantity"/>
                                </div>
                                <div class="form-group col-md-8">
                                    <input type="text" name="pname2"  class="form-control" placeholder="Props Name"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text" name="qnt2"  class="form-control" placeholder="Quantity"/>
                                </div>
                                <div class="form-group col-md-8">
                                    <input type="text" name="pname3"  class="form-control" placeholder="Props Name"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text" name="qnt3"  class="form-control" placeholder="Quantity"/>
                                </div>
                                <div class="form-group col-md-8">
                                    <input type="text" name="pname4"  class="form-control" placeholder="Props Name"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text" name="qnt4"  class="form-control" placeholder="Quantity"/>
                                </div>
                                <div class="form-group col-md-8">
                                    <input type="text" name="pname5"  class="form-control" placeholder="Props Name"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text" name="qnt5"  class="form-control" placeholder="Quantity"/>
                                </div>
                                <div class="form-group col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1">Notes<sup>*</sup></span>
                                        <textarea id="comments" name="comments"  class="form-control"></textarea>
                                    </div>
                                </div>

                            </div>

                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="props_user" value="<?php echo $this->session->userdata('t_code'); ?>"/>

                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="upload_content_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Upload Content</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="add-content-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="client" value="<?php echo $program[0]->client_id; ?>"/>
                            <input type="hidden" name="content_user" value="<?php echo $this->session->userdata('t_code'); ?>"/>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Content Title <sup>*</sup></span>
                                    <input type="text" name="title" placeholder="Content Title" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">

                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Upload Content <sup>*</sup></span>
                                    <input type="file" name="content" class="form-control">
                                </div>

                            </div>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Content For <sup>*</sup></span>
                                    <select class="form-control" name="ctype">
                                        <option value="project">Program</option>
                                        <option value="client">Client</option>
                                    </select>
                                </div>
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="upload_form_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Upload Forms</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="add-forms-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="form_user" value="<?php echo $this->session->userdata('t_code'); ?>"/>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Form Type <sup>*</sup></span>
                                    <select class="form-control" name="ftype">
                                        <option value="">- Select -</option>
                                        <option value="Attendance Form">Attendance Form</option>
                                        <option value="Feedback Form">Feedback Form</option>
                                        <option value="Assessment Score Sheet">Assessment Score Sheet</option>
                                        <option value="Other Forms">Other Forms</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Date <sup>*</sup></span>
                                    <input type="date" name="fdate"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Upload Form <sup>*</sup></span>
                                    <input type="file" name="content" class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Notes<sup>*</sup></span>
                                    <textarea id="comments" name="comments"  class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <div class="modal fade" id="upload_image_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Upload Images</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="add-image-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="img_user" value="<?php echo $this->session->userdata('t_code'); ?>"/>

                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Upload Image <sup>*</sup></span>
                                    <input type="file" name="img[]" multiple="multiple" class="form-control">
                                </div>
                            </div>

                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" name="upload_img" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="stay_form_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Stay Arrangements Preferences</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="add-stay-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="stay_user" value="<?php echo $this->session->userdata('t_code'); ?>"/>
                            <input type="hidden" name="trainer" value="<?php echo $this->session->userdata('t_code'); ?>"/>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Hotel Name <sup>*</sup></span>
                                    <input type="text" name="hotel" class="form-control">
                                </div>
                            </div>
                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Check in Time <sup>*</sup></span>
                                    <input type="datetime-local" name="checkin"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Check out Time <sup>*</sup></span>
                                    <input type="datetime-local" name="checkout"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Notes<sup>*</sup></span>
                                    <textarea id="comments" name="comments"  class="form-control"></textarea>
                                    <span>Mention Any class, room /floor / view preferences, eating preferences etc.</span>
                                </div>
                            </div>
                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="travel_form_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Travel Arrangements Preferences</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="add-travel-form">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" name="travel_user" value="<?php echo $this->session->userdata('t_code'); ?>"/>
                            <input type="hidden" name="trainer" value="<?php echo $this->session->userdata('t_code'); ?>"/>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Date <sup>*</sup></span>
                                    <input type="date" name="date" class="form-control">
                                </div>
                            </div>
                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> From Location <sup>*</sup></span>
                                    <input type="text" name="from"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> To Location <sup>*</sup></span>
                                    <input type="text" name="to"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Mode <sup>*</sup></span>
                                    <select class="form-control" name="mode">
                                        <option value=""> - Select -</option>
                                        <option value="Bus">Bus</option>
                                        <option value="Cab">Cab</option>
                                        <option value="Train">Train</option>
                                        <option value="Air">Air</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Notes<sup>*</sup></span>
                                    <textarea id="comments" name="comments"  class="form-control"></textarea>
                                    <span>Mention Train / Flight No, Timing of departure, class, on board facilities etc.</span>
                                </div>
                            </div>
                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Upload</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="passwordWraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Download Content</h4>
                    </div>

                    <div class="modal-body">
                        <input type="hidden" id="passProject" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                        <input type="hidden" id="passTrainer" name="trainer" value="<?php echo $this->session->userdata('t_code'); ?>"/>
                        <input type="hidden" id="mid" name="mid" />

                        <div class="form-group  col-md-12">
                            <div class="input-group">
                                <span class="input-group-addon" id="sizing-addon1"> Password  <sup>*</sup></span>
                                <input type="password" id="password" name="password" placeholder="Enter Password " class="form-control">
                            </div>
                        </div>

                        <div class="form-group  col-md-12">
                            The content shared with you is a property of the client and Wagons Learning. The content ( or a part of it) shall not be used for any  training program ,other than the allotted training/s. The content will not be shared with anyone without the written permission of Wagons Learning.
                        </div>
                        
                        <div class="clearfix"></div>

                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-info pass-btn" ><i class="fa fa-upload"></i> Submit</button>
                    </div>

                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="createWraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Generate GST Tax Invoice</h4>
						<p>Use it only in case you are GST registered</p>
                    </div>

                    <div class="modal-body">


                        <form action="<?php echo base_url(); ?>trainer/invoice/<?php echo $program[0]->project_id; ?>" enctype="multipart/form-data" method="POST" id="gInvoice">
                            <input type="hidden" id="passProject" name="project" value="<?php echo $program[0]->project_id; ?>"/>
                            <input type="hidden" id="passTrainer" name="trainer" value="<?php echo $this->session->userdata('t_code'); ?>"/>
                            <input type="hidden" id="mid" name="mid" />

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> GST Number  <sup>*</sup></span>
                                    <input type="text" id="gstNumber" name="gstNumber" value="<?php echo $this->session->userdata('t_gst'); ?>" placeholder="GST Number " class="form-control">
                                </div>
                            </div>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> State Code  <sup>*</sup></span>
                                    <input type="number" id="gstNumber" name="stateCode" value="<?php echo $this->session->userdata('t_gst_code'); ?>" placeholder="State Code " class="form-control">
                                </div>
                            </div>
                            <div class="inBtnWrap" style="display:none;">
                                <div class="col-md-12" style="font-size:15px; text-align: center;">
                                    <button type="submit" class="btn btn-success">Use Wagons Invoice Format</button>
                                </div>
                                <div class="col-md-12" style="font-size:15px; margin-top: 10px; text-align: center;">
                                    <h3>OR</h3>
                                </div>
                                <div class="col-md-12" style="font-size:15px; margin-top: 10px; margin-bottom: 10px; text-align: center;">
                                    <button type="button" class="btn btn-primary upInvoice">Upload Your Invoice Format</button>
                                </div>
                            </div>

                            <div class="inWrap">
                                <div class="form-group  col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Invoice Number  <sup>*</sup></span>
                                        <input type="text" id="gstNumber" name="inNumber" placeholder="Invoice Number " class="form-control">
                                    </div>
                                </div>
                                <div class="form-group  col-md-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Invoice Amount  <sup>*</sup></span>
                                        <input type="text" id="gstNumber" name="inAmount" placeholder="Invoice Amount " class="form-control">
                                    </div>
                                </div>
                                <div class="form-group  col-md-12">
                                    <div class="input-group">
                                        <span> Invoice  <sup>*</sup></span>
                                        <input type="file" accept="application/pdf,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" name="inFile"/>
										<br/>
									<small>(upload pdf/xls/xlsx files)</small>
                                    </div>
                                </div>
                                <div class="form-group  col-md-12 text-center">
                                    <button type="submit" class="btn btn-primary">Upload</button>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </form>


                        <div class="clearfix"></div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


        <?php include 'js_files.php'; ?>
        <script type="text/javascript">
            $(document).ready(function () {

//                $('.upInvoice').click(function (e) {
//                    $('.inBtnWrap').hide();
//                    $('.inWrap').show();
//                });
//
                $('.genInvoiceBtn').click(function (e) {
                    e.preventDefault();
                    $('#createWraper').modal('show');
//                    $('.inBtnWrap').show();
//                    $('.inWrap').hide();
                });

                $('.content-download-btn').click(function (e) {
                    e.preventDefault();
                    $('#passwordWraper').modal('show');
                    var mid = $(this).attr('mid');
                    $('#mid').val(mid);
                });

                $('.upload-form-btn').click(function (e) {
                    e.preventDefault();
                    $('#upload_form_wraper').modal('show');
                });

                $('.props-add-btn').click(function (e) {
                    e.preventDefault();
                    $('#props_add_wraper').modal('show');
                });

                $('.upload-content-btn').click(function (e) {
                    e.preventDefault();
                    $('#upload_content_wraper').modal('show');
                });

                $('.stay-update-btn').click(function (e) {
                    e.preventDefault();
                    $('#stay_form_wraper').modal('show');
                });

                $('.travel-update-btn').click(function (e) {
                    e.preventDefault();
                    $('#travel_form_wraper').modal('show');
                });

                $('.images-add-btn').click(function (e) {
                    e.preventDefault();
                    $('#upload_image_wraper').modal('show');
                });


                $('.assign-pm-btn').click(function (e) {
                    e.preventDefault();
                    $('#assign-pm-wrap').modal('show');
                    var cid = $(this).attr('cid');
                    $('#cid').val(cid);
                });

                $("#gInvoice").validate({
                    rules: {
                        gstNumber: "required",
                        inNumber: "required",
                        inAmount: "required",
                        inFile: "required",
                        stateCode: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#add-stay-form").validate({
                    rules: {
                        hotel: "required",
                        checkin: "required",
                        checkout: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#add-travel-form").validate({
                    rules: {
                        from: "required",
                        to: "required", date: "required",
                        screen: "required",
                        mode: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#props-add-form").validate({
                    rules: {
                        pname1: "required",
                        qnt1: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#add-content-form").validate({
                    rules: {
                        title: "required",
                        content: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#add-forms-form").validate({
                    rules: {
                        content: "required",
                        ftype: "required",
                        fdate: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $('.pass-btn').click(function (e) {
                    e.preventDefault();
                    var pid = $('#passProject').val();
                    var tid = $('#passTrainer').val();
                    var mid = $('#mid').val();
                    var pass = $('#password').val();
                    $('.page_spin').show();
                    var dataString = "pid=" + pid + "&tid=" + tid + "&mid=" + mid + "&pass=" + pass + "&page=download_content";
                    $.ajax({
                        type: "POST", url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            var reply = $.trim(data);
                            if (reply == '1') {
                                $('#passwordWraper').modal('hide');
                                window.location.href = "<?php echo base_url(); ?>assets/upload/content/download.php?id=" + mid + "&type=trainer";
                            } else {
                                alert('Password is invalid');
                            }
                        }, //success fun end
                    });//ajax end
                });

                $('.remove-content').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove content ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_content";
                        $.ajax({
                            type: "POST", url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.remove-form').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove form ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_form";
                        $.ajax({
                            type: "POST", url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.remove-img').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var iname = $(this).attr('iname');
                    var f = confirm('Are you sure want to remove image ?');
                    if (f == true) {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&iname=" + iname + "&page=remove_img";
                        $.ajax({
                            type: "POST", url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.remove-props').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove props ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_props";
                        $.ajax({
                            type: "POST", url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.remove-stay').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove stay ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_stay";
                        $.ajax({
                            type: "POST", url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.remove-travel').click(function (e) {
                    e.preventDefault();
                    var rid = $(this).attr('rid');
                    var f = confirm('Are you sure want to remove travel details ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_travel";
                        $.ajax({
                            type: "POST", url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

                $('.ttt-accept-btn').click(function (e) {
                    e.preventDefault();
                    var tid = $(this).attr('tid');
                    var pid = $(this).attr('pid');
                    var f = confirm('Are you sure want to accept this schedule ?');
                    if (f == true)
                    {
                        $('.page_spin').show();
                        var dataString = "tid=" + tid + "&project=" + pid + "&page=accept_ttt";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>projectmanager/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

            });
        </script>

    </body>
</html>