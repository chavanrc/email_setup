<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Payments</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Program Invoices</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <!--<a href="<?php // echo base_url();  ?>trainer/add_payment" class="btn btn-primary"><i class="fa fa-user-plus"></i> Add New Invoice</a>-->
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list"></i> Program Invoice List </h2>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Invoice No.</th>
                                        <th>Program</th>
                                        <th>Program Date</th>

                                        <th>Invoice Date</th>
                                        <th>Amount</th>
                                        <th>Payment Status</th>
                                        <th>Payment Date</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                    if (!empty($payment)) {
                                        $num = 0;
                                        foreach ($payment as $pm_data) {
                                            $num++;
                                            ?>
                                            <tr>
                                                <td><?php echo $num; ?></td>
                                                <td><a href="<?php echo base_url(); ?>trainer/invoice/<?php echo $pm_data->project_id; ?>" class="btn btn-sm btn-primary">T/18-19/<?php echo $pm_data->id; ?></a></td>
                                                <td><a href="<?php echo base_url(); ?>trainer/program_details/<?php echo $pm_data->project_id; ?>"><?php echo $pm_data->project_title; ?></a> <?php echo $pm_data->location_of_training; ?></td>
                                                <td><?php echo date_formate_short($pm_data->training_date_from); ?></td>

                                                <td><?php echo date_formate_short($pm_data->trainer_invoice_date); ?></td>
                                                <td></td>
                                                <td>
                                                    <?php
                                                    if ($pm_data->trainer_paid_status == '0') {
                                                        if ($pm_data->trainer_invoice_action == '') {
                                                            echo 'PM Approval Pending';
                                                        } else if ($pm_data->trainer_invoice_action == '2') {
                                                            echo 'Training Support Approval Pending';
                                                        } else {
                                                            ?>
                                                            Invoice Approved, Payment Pending
                                                            <?php
                                                        }
                                                    }
                                                    if ($pm_data->trainer_paid_status == '1') {
                                                        ?>
                                                        Paid
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                                <td><?php
                                            if ($pm_data->trainer_paid_status == '1') {
                                                echo date_formate_short($pm_data->trainer_payment_date);
                                            }
                                                    ?></td>
                                            </tr>
                                                    <?php
                                                }
                                            } else {
                                                ?>
                                        <tr>
                                            <td colspan="8">No Invoice Records found.</td>
                                        </tr>
                                        <?php
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

<?php include 'js_files.php'; ?>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {

                $('#data-list').DataTable();



            });
        </script>

    </body>
</html>