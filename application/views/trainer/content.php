<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Contents</title>
        <?php include 'css_files.php'; ?>
        <link href="<?php echo base_url(); ?>assets/css/jquery-ui.css" rel="stylesheet">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Common Reference Documents</h3>
                    </div>
                    
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-list"></i> Documents </h2>
                        </div>
                        <div class="panel-body">
                            <table class="table table-bordered" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Title</th>
                                        <th>File</th>
                                        <th>Details</th>
                                       
                                        <th>Added On</th>
                                        
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                            <tr>
                                                <td>1</td>
                                                <td>Attendance Sheet for Programs</td>
                                                <td><a href="<?php echo base_url(); ?>assets/upload/commonforms/Attendance_Sheet.xls" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-download"></i> View</a></td>
                                                <td>This Must be downloaded for taking attendance of participants. If there are multiple days for the program please append signature column.</td>
                                                
                                                <td>1<sup>st</sup> December 2018</td>
                                               
                                            </tr>
					   <tr>
                                                <td>2</td>
                                                <td>Participant Feedback Form</td>
                                                <td><a href="<?php echo base_url(); ?>assets/upload/commonforms/Participant_feedback_format.doc" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-download"></i> View</a></td>
                                                <td>This Must be downloaded for taking feedback from each participant about the program conducted</td>
                                                
                                                <td>3<sup>rd</sup> December 2018</td>
                                               
                                            </tr>
											  <tr>
                                                <td>3</td>
                                                <td>Program Guidelines</td>
                                                <td><a href="<?php echo base_url(); ?>assets/upload/commonforms/Program-Guidelines.pdf" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-download"></i> View</a></td>
                                                <td>Please refer this for pre and post program guidelines and directions</td>
                                                
                                                <td>4<sup>th</sup> December 2018</td>
                                               
                                            </tr>
											
											
                                          
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                
                $('#data-list').DataTable();
                
                
                $('.remove-content').click(function(e){
                   var rid = $(this).attr('rid');
                   var f = confirm("Are you sure want remove ?");
                   if(f==true)
                   {
                       $('.page_spin').show();
                        var dataString = "rid=" + rid + "&page=remove_content";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>admin/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                   }
                });
                
            });
        </script>

    </body>
</html>