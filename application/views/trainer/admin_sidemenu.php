<?php
$CI = & get_instance();
$CI->load->model('projectmanager_model');

$TR = & get_instance();
$TR->load->model('trainer_model');

?>
<div class="nav-side-menu no-print">
    <div class="brand"><img src="<?php echo base_url(); ?>assets/images/wagons-logo.png"/></div>
    <i class="fa fa-bars fa-2x toggle-btn" data-toggle="collapse" data-target="#menu-content"></i>

    <div class="menu-list">

        <ul id="menu-content" class="menu-content collapse out">
            <li <?php if ($page_url == 'Dashboard') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>trainer/">
                    <i class="fa fa-dashboard"></i> Dashboard
                </a>
            </li>
            
            <li <?php if ($page_url == 'New Programs') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>trainer/new_programs">
                    <i class="fa fa-calendar-plus-o"></i> New Programs
                </a>
            </li>

            <li <?php if ($page_url == 'Upcoming Programs') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>trainer/upcoming_programs">
                    <i class="fa fa-calendar-plus-o"></i> Upcoming Programs
                </a>
            </li>
            <li <?php if ($page_url == 'Past Programs') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>trainer/past_programs">
                    <i class="fa fa-calendar-check-o"></i> Past Programs
                </a>
            </li>
            <li <?php if ($page_url == 'Content') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>trainer/contents">
                    <i class="fa fa-file-text-o"></i> Documents
                </a>
            </li>
            <li <?php if ($page_url == 'Payment') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>trainer/payments">
                    <i class="fa fa-money"></i> Invoices
                </a>
            </li>
            <li <?php if ($page_url == 'tdscertificate') { ?> class="active" <?php } ?>>
                <a href="<?php echo base_url(); ?>trainer/tds_certificate">
                    <i class="fa fa-certificate"></i> TDS Certificate
                </a>
            </li>
            
        </ul>
    </div>
</div>

<div class="page_spin">
    <br/>
    <div class="spin_icon">
        <i class="fa fa-spinner fa-spin"></i><br/>
        <span>One moment ...</span>
    </div>
</div>
