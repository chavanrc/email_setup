<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Trainer | TDS Certificate</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
    <?php include 'admin_sidemenu.php'; ?>
</body>
<div class="right-side">
     <?php include 'admin_topmenu.php'; ?>
    <div class="row" style="margin: 0px;">
        
        <div class="col-md-12">
           
                    <div class="page-title title-left">
                        <h3>TDS Documents Uploaded by Wagons</h3>
                    </div>
                    <div class="page-title title-right text-right">
                    </div>
                    <div class="clearfix"></div>
                    
                    <div class="col-md-6 col-sm-6 col-xs-12">
                            
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-list"></i> TDS Documents </h2>
                            </div>
                            <div class="panel-body">
                                <table class="table table-bordered" id="data-list">
                                    <thead>
                                        <tr>
                                            <th>Year</th>
                                            <th>Quater</th>
                                             <th> </th>
                                           
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($tds as $tds_cert) {
                                            ?>
                                            <tr>
                                                <td><?php echo $tds_cert->tt_year; ?></td>
                                                <td><?php echo $tds_cert->tt_quater; ?></td>
                                                <td><a href="<?php echo base_url(); ?>assets/upload/tdscertificate/<?php echo $tds_cert->tds_file; ?>" class="btn btn-sm btn-primary" target="_blank"><i class="fa fa-download"></i> View</a></td>
                                                
                                               
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            
                        </div>
                    </div>
                </div>
    </div>
</div>