<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Trainer | Dashboard</title>
        <?php include 'css_files.php'; ?>
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>
      
        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px; padding-top: 20px;">

                <div class="col-md-4">
                    <div class="box box-primary">
                        <div class="box-header with-border text-center">
                            <h4 style="display: inline-block;">New Program</h4>
                        </div>
                        <div class="box-body text-center">
                            <span style="font-size:25px">
                                <?php
                                if (!empty($newP)) {
                                    echo $newP[0]->np;
                                } else {
                                    echo 0;
                                }
                                ?>
                            </span>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box box-info">
                        <div class="box-header with-border text-center">
                            <h4 style="display: inline-block;">Upcoming Program</h4>
                        </div>
                        <div class="box-body text-center">
                            <span style="font-size:25px">
                                <?php
                                if (!empty($upP)) {
                                    echo $upP[0]->up;
                                } else {
                                    echo 0;
                                }
                                ?>
                            </span>

                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="box box-success">
                        <div class="box-header with-border text-center">
                            <h4 style="display: inline-block;">Programs Conducted</h4>
                        </div>
                        <div class="box-body text-center">
                            <span style="font-size:25px">
                                <?php
                                if (!empty($pastP)) {
                                    echo $pastP[0]->pp;
                                } else {
                                    echo 0;
                                }
                                ?>
                            </span>

                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="col-md-6">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h4 style="display: inline-block;"><i class="fa fa-tasks"></i> To Do Task</h4>
                        </div>
                        <div class="box-body" style="height:200px; overflow-y: scroll;">
                            <table class="table table-bordered" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Program</th>
                                        <th>Task</th>
                                        <th>Due Date</th>
                                    </tr>
                                </thead>
                                <tbody style="font-size:13px;">
                                    <?php
                                    
                                    $note = $CI->projectmanager_model->get_notification($this->session->userdata('t_code'));
                                    if (!empty($note)) {
                                        $no = 0;
                                        foreach ($note as $nt_data) {
                                            if($nt_data->todo_task==1){
                                                $no++;
                                            ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>trainer/program_details/<?php echo $nt_data->project_id; ?>">
                                                    <?php echo $nt_data->project_title; ?>
                                                    </a>
                                                </td>
                                                <td><?php echo $nt_data->notification_text; ?>
                                                <br/><br/>
                                                <span style="font-size:12px; color: #777;">
                                                    <?php echo date_formate($nt_data->notification_date); ?>
                                                </span>
                                                </td>
                                                <td><?php if($nt_data->due_date!='0000-00-00 00:00:00'){ 
                                                    $dd = explode(' ', $nt_data->due_date);
                                                    if($dd[0]<date('Y-m-d')){
                                                        $now = time(); // or your date as well
                                                        $ndate = strtotime($dd[0]);
                                                        $datediff = $now - $ndate;
                                                        echo ceil($datediff/86000).' Days Due';
                                                    } else {
                                                        echo date_formate_short($nt_data->due_date);
                                                    }
                                                    
                                                } ?></td>

                                            </tr>
                                            <?php
                                            }
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h4 style="display: inline-block;"><i class="fa fa-info"></i> Notifications</h4>
                        </div>
                        <div class="box-body" style="height:200px; overflow-y: scroll;">
                             <table class="table table-bordered" id="data-list">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Program</th>
                                        <th>Notification</th>

                                    </tr>
                                </thead>
                                <tbody style="font-size:13px;">
                                    <?php
                                    if (!empty($note)) {
                                        $no = 0;
                                        foreach ($note as $nt_data) {
                                            if($nt_data->todo_task==0){
                                                $no++;
                                            ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>trainer/program_details/<?php echo $nt_data->project_id; ?>">
                                                    <?php echo $nt_data->project_title; ?>
                                                    </a>
                                                </td>
                                                <td><?php echo $nt_data->notification_text; ?>
                                                <br/><br/>
                                                <span style="font-size:12px; color: #777;">
                                                    <?php echo date_formate($nt_data->notification_date); ?>
                                                </span>
                                                </td>

                                            </tr>
                                            <?php
                                            }
                                        }
                                    }
                                    ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="box box-warning">
                        <div class="box-header with-border">
                            <h4 style="display: inline-block;"><i class="fa fa-calendar"></i> Calendar</h4>
                        </div>
                        <div class="box-body text-center">
                            <div id='calendar'></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
	 <?php include 'js_files.php'; ?>
        <?php
        $program = $TR->trainer_model->get_progrom_date($this->session->userdata('t_code'));
       // print_r($program);
//        exit();
        ?>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/moment.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/jquery.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.js'></script>
        <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&libraries=geometry"></script>

        <script>
        var p1 = new google.maps.LatLng(12.9783484, 77.56839830000001);
        var p2 = new google.maps.LatLng(15.384607, 75.08149939999998);

       // alert(calcDistance(p1, p2));

        //calculates distance between two points in km's
        function calcDistance(p1, p2) {
          return (google.maps.geometry.spherical.computeDistanceBetween(p1, p2) / 1000).toFixed(2);
        }

        </script>
        <script type="text/javascript">

            $(document).ready(function () {

                $('#calendar').fullCalendar({
                    header: {
                        left: 'prev,next today',
                        center: 'title',
                        right: 'month,agendaWeek'
                    },
                    defaultDate: '<?php echo date('Y-m-d'); ?>',
                    editable: true,
                    eventLimit: true, // allow "more" link when too many events
                    events: [
                        <?php
                        if(!empty($program))
                        {
                            foreach($program as $pm_data)
                            {
                                $date = strtotime($pm_data->trainer_date_to);
                                $end_date =  date("Y-m-d", $date);
                                $startDate = new DateTime($pm_data->training_date_from);
                                $endDate = new DateTime($end_date);
                                
                                $tempArray = array();
                                if($pm_data->excl_sat=='Y'){
                                $tmpstartDate = $pm_data->training_date_from;
                                $tmpendDate = new DateTime($end_date);
                                while ($startDate <= $endDate) {
                                    if ($startDate->format('w') == 6) {
                                        $sat = new DateTime($startDate->format('Y-m-d'));
                                        $tempArray[] = array('start'=>$tmpstartDate,'end'=>$sat->modify('-0 day'));
                                        $endDate->modify('+2 days');
                                        $tmpstartDate = date('Y-m-d', strtotime($startDate->format('Y-m-d'). ' + 2 days'));
                                    }
                                    $startDate->modify('+1 day');
                                }
                                $tempArray[] = array('start'=>$tmpstartDate,'end'=>$endDate);
                                } else {
                                    $tmpstartDate = $pm_data->training_date_from;
                                $tmpendDate = new DateTime($end_date);
                                while ($startDate <= $endDate) {
                                    if ($startDate->format('w') == 0) {
                                        $sat = new DateTime($startDate->format('Y-m-d'));
                                        $tempArray[] = array('start'=>$tmpstartDate,'end'=>$sat->modify('-0 day'));
                                        $endDate->modify('+1 days');
                                        $tmpstartDate = date('Y-m-d', strtotime($startDate->format('Y-m-d'). ' + 1 days'));
                                    }
                                    $startDate->modify('+1 day');
                                }
                                $tempArray[] = array('start'=>$tmpstartDate,'end'=>$endDate);
                                }
                                foreach($tempArray as $dd){
                                ?>
                                                {
                                                    url: '<?php echo base_url(); ?>trainer/program_details/<?php echo $pm_data->project_id; ?>',
                                                    title : '<?php echo $pm_data->project_title; ?>',
						    program : '<?php echo $pm_data->project_title; ?>',
                                                    program1 : '<b>Location:</b> <?php echo $pm_data->location_of_training." <br/> <b>Program:</b> ".$pm_data->project_title; ?><br/>',
                                                    start : '<?php echo $dd['start']; ?>',
                                                    end : '<?php echo $dd['end']->format("Y-m-d"); ?>',
                                                },
                                <?php
                                }
                            }
                        }
                        ?>
                    ],
                    eventRender: function (eventObj, $el) {
                                            $el.popover({
                                                title: eventObj.title,
                                                content: eventObj.program1,
                                                trigger: 'hover',
                                                html: 'true',
                                                placement: 'top',
                                                container: 'body'
                                            });
                                        },
                });

            });

        </script>

    </body>
</html>