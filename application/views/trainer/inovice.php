<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Add Payment</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">

                    </div>
                    <div class="page-title title-right text-right">

                        <?php
                        if ($fees[0]->trainer_invoice_flag == 0) {
                            ?>
                            <a href="#" class="btn btn-warning reset-btn" pid="<?php echo $fees[0]->id; ?>" project_id="<?php echo $pid; ?>" >Reset</a>
                            <?php
                            if (!empty($bank)) {
                                ?>
                                <a href="#" class="btn btn-success confirm-btn" pid="<?php echo $fees[0]->id; ?>" project_id="<?php echo $pid; ?>" >Confirm Invoice</a>
                            <?php } else {
                                ?>
                                <a style="font-size: 16px; margin-right: 15px; display: inline-block;" href="<?php echo base_url(); ?>trainer/update_bank/<?php echo $this->session->userdata('t_code'); ?>">Update your bank details to generate invoice</a>
                            <?php }
                            ?>

                            <a href="<?php echo base_url(); ?>trainer/add_payment/<?php echo $pid; ?>" class="btn btn-primary">Add Debit Note/ Expense</a>
                            <?php
                        } else {
                            if ($fees[0]->trainer_invoice_action == '') {
                                ?>
                                <a href="<?php echo base_url(); ?>trainer/add_payment/<?php echo $pid; ?>" class="btn btn-primary">Add Debit Note/ Expense</a>
                                <?php
                            }
                            if ($fees[0]->trainer_invoice_action == 1) {
                                ?>
                                <a href="<?php echo base_url(); ?>trainer/printinvoice/<?php echo $pid; ?>" class="create-invoice-btn btn btn-primary" target="_blank" >Print / Download</a><br/>
                                <?php
                            }
                            echo 'Invoice Generated On : ' . date_formate_short($fees[0]->trainer_invoice_date);
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>


                <div class="col-md-6 content-page" style="font-size: 12px;">
                    <div class="panel panel-default">

                        <div class="panel-body">
                            <h3 style="display: inline-block;"><?php
                                if (!empty($fees[0]->trainer_invoice_gst)) {
                                    echo 'Tax ';
                                }
                                ?>Invoice</h3>
                            <?php
                            if (!empty($fees)) {
                                if (empty($fees[0]->trainer_invoice_file)) {
                                    ?>
                                    <table class="table table-bordered">
                                        <tr style="background:#656565; color: #fff; font-size: 15px;">
                                            <td>Name & Address of Trainer</td>
                                            <td>Invoice No</td>
                                            <td>Date</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="3">
                                                <?php echo $tDetails[0]->name; ?> <br/>
                                                <?php echo $tDetails[0]->address; ?><br/><br/>
                                                <?php
                                                if (!empty($fees[0]->trainer_invoice_gst)) {
                                                    ?>
                                                    <strong>GSTIN : <?php echo $fees[0]->trainer_invoice_gst; ?> </strong>
                                                    <?php
                                                }
                                                ?>
                                            </td>
                                            <td>T/19-20/<?php echo $fees[0]->id; ?></td>
                                            <td>
                                                <?php
                                                if ($fees[0]->trainer_invoice_flag == 1) {
                                                    echo date_formate_short($fees[0]->trainer_invoice_date);
                                                } else {
                                                    echo date("d/m/Y");
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr style="background:#656565; color: #fff; font-size: 15px;">
                                            <td colspan="2" class="text-center">Terms</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="text-center"><?php
                                                $startD = explode(' ', $fees[0]->training_date_from);
                                                $endD = explode(' ', $fees[0]->trainer_date_to);
                                                $start = strtotime($startD[0]);
                                                $end = strtotime($endD[0]);

                                                $days_between = ceil(abs($end - $start) / 86400);
                                                $edays = 1;
                                                if ($fees[0]->training_date_from != $fees[0]->trainer_date_to) {
                                                    $edays = 2;
                                                }
                                                echo $tDetails[0]->trainer_credit;
                                                ?> Days</td>
                                        </tr>
                                        <tr style="background:#656565; color: #fff; font-size: 15px;">
                                            <td> Client </td>
                                            <td colspan="2">Bill To</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Name : <?php echo $pDetails[0]->client_name; ?><br/>
                                                Program : <?php echo $pDetails[0]->project_title; ?><br/>
                                                Date : <?php echo strftime("%d/%m/%Y", strtotime($pDetails[0]->training_start_date)); ?><br/>
                                                Location : <?php echo $pDetails[0]->location_of_training; ?><br/>
                                            </td>
                                            <td colspan="2">
                                                <?php if ($pDetails[0]->company_id == 1) { ?>
                                                    ATTN: Name / Dept : Accounts 	<br/>	
                                                    Wagons Management Consulting.	<br/>	
                                                    A/7-8 , Srushti Apartment Opp Corporation Bank	<br/>	
                                                    Baner Road, Pune - 411045<br/><br/>
                                                    <strong>GSTIN : 27AABFW3629E1Z8 </strong>
                                                <?php } if ($pDetails[0]->company_id == 2) { ?>
                                                    ATTN: Name / Dept : Accounts 	<br/>	
                                                    Wagons Learning Pvt Ltd.	<br/>	
                                                    A/7-8 , Srushti Apartment Opp Corporation Bank	<br/>	
                                                    Baner Road, Pune - 411045<br/><br/>
                                                    <strong>GSTIN : 27AABCW4538P1ZP </strong>
                                                <?php } ?>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered">
                                        <tr style="background:#656565; color: #fff; font-size: 15px;">
                                            <td>Sl. No.</td>
                                            <td >Description</td>
                                            <td>Rate</td>
                                            <td>Amount</td>
                                        </tr>
                                        <?php
                                        $no = 1;
                                        if (!empty($fees)) {
                                            ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td ><?php
                                                    echo 'Professional Fees: ';
                                                    if ($pDetails[0]->half_day == '1')
                                                        echo "(Half Day Program)";
                                                    else
                                                        echo $pDetails[0]->training_duration . " Day(s)";
                                                    ?></td>
                                                <td><?php echo $fees[0]->amount; ?></td>
                                                <td align="right"><?php echo number_format(round($fees[0]->amount * $pDetails[0]->training_duration, 2), 2); ?></td>
                                            </tr>
                                            <?php
                                        }
                                        $subTotal = 0;
                                        $total = round($fees[0]->amount * $pDetails[0]->training_duration, 2) + $subTotal;

                                        $gst = 0;
                                        if (!empty($fees[0]->trainer_invoice_gst)) {
                                            ?>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>CGST @ 9%</td>
                                                <td align="right">
                                                    <?php
                                                    if ($fees[0]->trainer_invoice_code == '27') {
                                                        $cgst = round(0.09 * $total, 2);
                                                        echo number_format($cgst, 2);
                                                        $gst = $cgst;
                                                    } else {
                                                        echo '-';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>SGST @ 9%</td>
                                                <td align="right"><?php
                                                    if ($fees[0]->trainer_invoice_code == '27') {
                                                        $sgst = round(0.09 * $total, 2);
                                                        echo number_format($sgst, 2);
                                                        $gst = $gst + $sgst;
                                                    } else {
                                                        echo '-';
                                                    }
                                                    ?></td>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <td></td>
                                                <td>IGST @ 18%</td>
                                                <td align="right">
                                                    <?php
                                                    if ($fees[0]->trainer_invoice_code != '27') {
                                                        $igst = round(0.18 * $total, 2);
                                                        echo number_format($igst, 2);
                                                        $gst = $igst;
                                                    } else {
                                                        echo '-';
                                                    }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                        <tr style="font-size: 18px;">
                                            <td colspan="3" class="text-right">Total</td>
                                            <td align="right">
                                                <?php
                                                $total = round($total + $gst, 2);
                                                echo number_format($total, 2);
                                                $fTotal = $total;
                                                ?>/-
                                            </td>
                                        </tr>
                                        <tr><td colspan="5"><strong>In Words: 
                                                    <?php
                                                    $f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                                    echo "Rs. " . ucfirst($f->format($total)) . " Only.";
                                                    ?>
                                                </strong></td></tr>
                                    </table>
                                    <?php
                                } else {
                                    if ($fees[0]->trainer_invoice_flag == 0) {
                                        ?>
                                        <a href="#" class="btn-sm btn-primary editInvoice pull-right">Edit</a>
                                        <div class="clearfix"></div>
                                        <?php
                                    }
                                    ?>
                                    <table class="table table-bordered">
                                        <tr>
                                            <td>Invoice Number </td>
                                            <td><?php echo $fees[0]->trainer_invoice_number; ?></td>
                                        </tr>
                                        <tr>
                                            <td>GST Number </td>
                                            <td><?php echo $fees[0]->trainer_invoice_gst; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Total Amount </td>
                                            <td><?php echo $fTotal = $fees[0]->total_bill; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Invoice Copy </td>
                                            <td><a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $fees[0]->trainer_invoice_file; ?>" class="btn-sm btn-primary" target="_blank">View</a></td>
                                        </tr>
                                    </table>
                                    <?php
                                }
                            }
                            ?>
                            <table class="table table-bordered">
                                <tr style="background:#656565; color: #fff; font-size: 15px;">
                                    <td colspan="2">
                                        Bank Details
                                    </td>
                                </tr>
                                <?php
                                if (!empty($bank)) {
                                    ?>
                                    <tr>
                                        <td>Bank Name</td>
                                        <td><?php echo $bank[0]->bank_name; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Account Number</td>
                                        <td><?php echo $bank[0]->bank_account; ?></td>
                                    </tr>
                                    <tr>
                                        <td>IFSC Code</td>
                                        <td><?php echo $bank[0]->bank_ifsc; ?></td>
                                    </tr>
                                    <tr>
                                        <td>Beneficiary Name</td>
                                        <td><?php echo $bank[0]->account_name; ?></td>
                                    </tr>
                                    <tr>
                                        <td>PAN No.</td>
                                        <td><?php echo $bank[0]->pan_no; ?></td>
                                    </tr>
                                    <?php
                                } else {
                                    ?>
                                    <tr>
                                        <td colspan="2">
                                            <strong>Update your Bank details to generate invoice</strong>
                                        </td>
                                    </tr>
                                    <?php
                                }
                                ?>
                            </table>
                        </div>
                    </div>
                </div>
                <?php if (!empty($debit)) { ?>
                    <div class="col-md-6 content-page" style="font-size: 12px;">
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <h3>Debit Note</h3>
                                <?php if (!empty($fees)) { ?>
                                    <table class="table table-bordered">
                                        <tr style="background:#656565; color: #fff; font-size: 15px;">
                                            <td>Name & Address of Trainer</td>
                                            <td>Invoice No</td>
                                            <td>Date</td>
                                        </tr>
                                        <tr>
                                            <td rowspan="3">
                                                <?php echo $tDetails[0]->name; ?> <br/>
                                                <?php echo $tDetails[0]->address; ?>
                                            </td>
                                            <td>T/18-19/D/<?php echo $fees[0]->id; ?></td>
                                            <td>
                                                <?php
                                                if ($fees[0]->trainer_invoice_flag == 1) {
                                                    echo date_formate_short($fees[0]->trainer_invoice_date);
                                                } else {
                                                    echo date("d/m/Y");
                                                }
                                                ?>
                                            </td>
                                        </tr>
                                        <tr style="background:#656565; color: #fff; font-size: 15px;">
                                            <td colspan="2" class="text-center">Terms</td>
                                        </tr>
                                        <tr>
                                            <td colspan="2" class="text-center"><?php
                                                $startD = explode(' ', $fees[0]->training_date_from);
                                                $endD = explode(' ', $fees[0]->trainer_date_to);
                                                $start = strtotime($startD[0]);
                                                $end = strtotime($endD[0]);

                                                $days_between = ceil(abs($end - $start) / 86400);
                                                $edays = 1;
                                                if ($fees[0]->training_date_from != $fees[0]->trainer_date_to) {
                                                    $edays = 2;
                                                }
                                                //echo $days_between + $edays;
                                                echo $tDetails[0]->trainer_credit;
                                                ?> Days</td>
                                        </tr>
                                        <tr style="background:#656565; color: #fff; font-size: 15px;">
                                            <td> Client </td>
                                            <td colspan="2">Bill To</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                Name : <?php echo $pDetails[0]->client_name; ?><br/>
                                                Program : <?php echo $pDetails[0]->project_title; ?><br/>
                                                Date : <?php echo strftime("%d/%m/%Y", strtotime($pDetails[0]->training_start_date)); ?><br/>
                                                Location : <?php echo $pDetails[0]->location_of_training; ?><br/>
                                            </td>
                                            <td colspan="2">
                                                ATTN: Name / Dept : Accounts 	<br/>	
                                                Wagons Learning Pvt Ltd.	<br/>	
                                                A-8 , Srushti Apartment Opp Corporation Bank	<br/>	
                                                Baner Road, Pune - 411045<br/>
                                            </td>
                                        </tr>
                                    </table>
                                    <table class="table table-bordered">
                                        <tr style="background:#656565; color: #fff; font-size: 15px;">
                                            <td>Sl. No.</td>
                                            <td colspan="2">Description</td>
                                            <td>Rate</td>
                                            <td colspan="2">Amount</td>
                                        </tr>
                                        <?php
                                        $no = 1;

                                        $subTotal = 0;

                                        foreach ($debit as $d_data) {
                                            $no++;
                                            ?>
                                            <tr>
                                                <td><?php echo $no; ?></td>
                                                <td><?php echo $d_data->debit_title; ?> <br/>
                                                    <?php echo $d_data->debit_desc; ?>
                                                </td>
                                                <td>
                                                    <?php if (!empty($d_data->debit_file)) { ?>
                                                        <a href="<?php echo base_url(); ?>assets/upload/content/<?php echo $d_data->debit_file; ?>" target="_blank">Receipt Copy</a>
                                                    <?php } ?>
                                                </td>
                                                <td>
                                                    <?php echo number_format($d_data->debit_amt, 2); ?>
                                                </td>
                                                <td align="right">
                                                    <?php
                                                    echo number_format($d_data->debit_amt, 2);
                                                    if ($d_data->debit_status == '1') {
                                                        $subTotal = $subTotal + $d_data->debit_amt;
                                                    } else {
                                                        ?>
                                                        <br/>
                                                        <span style="font-size: 12px; color: red">Rejected</span>
                                                        <?php
                                                    }
                                                    ?>
                                                </td>
                                                <td>
                                                    <?php
                                                    if ($fees[0]->trainer_invoice_flag == 0) {
                                                        ?>
                                                        <a href="#" class="remove-debit" did="<?php echo $d_data->dn_id; ?>" style="color:red;"><i class="fa fa-close"></i></a>
                                                    <?php }
                                                    ?>
                                                </td>
                                            </tr>
                                            <?php
                                        }

                                        $total = $subTotal;
                                        ?>
                                        <tr style="font-size: 18px;">
                                            <td colspan="4" class="text-right">Total</td>
                                            <td colspan="2" align="right">
                                                <?php echo number_format($total, 2); ?>/-
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="6">
                                                <strong>In Words: 
                                                    <?php
                                                    //$f = new NumberFormatter("en", NumberFormatter::SPELLOUT);
                                                    echo "Rs. " . ucfirst($f->format($total)) . " Only.";
                                                    ?>
                                                </strong>
                                            </td>
                                        </tr>
                                    </table>
                                <?php } ?>
                                <div>
                                    <?php
                                    if (!empty($courier)) {
                                        ?>
                                        <b>Courier Details  </b><br/>
                                        Ref. No : <?php echo $courier[0]->tc_ref; ?><br/>
                                        Details : <?php echo $courier[0]->tc_details; ?><br/>
                                        Courier Date : <?php echo date_formate_short($courier[0]->tc_cdate); ?><br/>
                                        Updated Date : <?php echo date_formate_short($courier[0]->tc_date); ?><br/>
                                        <?php
                                    } else {
                                        if ($fees[0]->trainer_invoice_flag == '2') {
                                            
                                        }
                                        ?>
                                        <div style="padding:10px 0; font-size: 16px;">Send Original bills to claim your debit notes.</div>
                                        <a href="#" class="btn btn-primary courier-btn">Update Courier Details</a>
                                    <?php } ?>
                                </div>
                            </div>


                        </div>
                    </div>
                <?php } ?>

                <div class="clearfix"></div>
                <?php if (!empty($payment)) { ?>

                    <div class="col-md-12 content-page">
                        <div class="panel panel-default">
                            <div class="panel-body">
                                <div>
                                    <h3 style="display:inline-block;">Payment Details</h3>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6" style="margin-top: 10px;">
                                        <table class="table table-bordered">
                                            <tr>
                                                <td>Total Professional Fees</td>
                                                <td style="text-align: right;"><?php echo $fTotal; ?>.00</td>
                                            </tr>
                                            <tr>
                                                <td>TDS(10%)</td>
                                                <td style="text-align: right;"><?php
                                                    echo '-' . $tds = $fTotal * .10;
                                                    ?>.00</td>
                                            </tr>
                                            <?php
                                            $dAmt = 0;
                                            if (!empty($debit)) {
                                                ?>
                                                <tr>
                                                    <td>Debit Note </td>
                                                    <td style="text-align: right;"><?php
                                                        $dAmt = $total;
                                                        echo $total;
                                                        ?>.00</td>
                                                </tr>
                                            <?php } ?>
                                            <tr>
                                                <td>Total</td>
                                                <td style="text-align: right;"><?php
                                                    echo $pTotal = $fTotal - $tds + $dAmt;
                                                    ?>.00</td>
                                            </tr>

                                        </table>
                                    </div>
                                    <div class="col-md-6"></div>
                                    <div class="clearfix"></div>
                                </div>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Amount Paid</th>
                                            <th>Date</th>
                                            <th>Payment Id</th>
                                            <th>Payment Advice</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td><?php echo $payment[0]->tp_amt; ?></td>
                                            <td><?php echo $payment[0]->tp_pay_date; ?></td>
                                            <td><?php echo $payment[0]->tp_pay_id; ?></td>
                                            <td><?php
                                                if (!empty($payment[0]->tp_advice)) {
                                                    ?>
                                                    <a download="advice" href="<?php echo base_url(); ?>assets/upload/content/<?php echo $payment[0]->tp_advice; ?>" class="btn btn-primary">Download</a>
                                                    <?php
                                                }
                                                ?></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>

                        </div>
                    </div>
                <?php } ?>

            </div>
        </div>

        <div class="modal fade" id="courier_wraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Update Courier Details </h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="cUpdateForm">

                        <div class="modal-body">
                            <input type="hidden" name="project" value="<?php echo $pDetails[0]->project_id; ?>"/>
                            <input type="hidden" name="user" value="<?php echo $tDetails[0]->user_code; ?>"/>
                            <input type="hidden" name="pname" value="<?php echo $pDetails[0]->project_title; ?>"/>

                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Courier Ref. <sup>*</sup></span>
                                    <input type="text" name="ref" class="form-control">
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div>
                                    <span >Courier Details <sup>*</sup></span>
                                    <textarea name="desc" class="form-control" placeholder="Mention document you sent."></textarea>
                                </div>
                            </div>

                            <div class="form-group col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1">Date. <sup>*</sup></span>
                                    <input type="date" name="cdate" class="form-control">
                                </div>
                            </div>

                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Update</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="createWraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Upload Tax Invoice</h4>
                        <p>Use only if you are GST Registered</p>
                    </div>

                    <div class="modal-body">
                        <form action="" enctype="multipart/form-data" method="POST" id="gInvoice">
                            <input type="hidden" id="passProject" name="project" value="<?php echo $fees[0]->project_id; ?>"/>
                            <input type="hidden" id="passTrainer" name="trainer" value="<?php echo $this->session->userdata('t_code'); ?>"/>
                            <input type="hidden" id="mid" name="mid" />

                            <input type="hidden" id="gstNumber" name="stateCode" value="<?php echo $this->session->userdata('t_gst_code'); ?>" placeholder="State Code " class="form-control">

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> GST Number  <sup>*</sup></span>
                                    <input type="text" id="gstNumber" name="gstNumber" value="<?php echo $fees[0]->trainer_invoice_gst; ?>" placeholder="GST Number " class="form-control">
                                    <input type="hidden" name="cfile" value="<?php echo $fees[0]->trainer_invoice_file; ?>"/>
                                </div>
                            </div>
                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Invoice Number  <sup>*</sup></span>
                                    <input type="text" id="gstNumber" name="inNumber" value="<?php echo $fees[0]->trainer_invoice_number; ?>" placeholder="Invoice Number " class="form-control">
                                </div>
                            </div>
                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Invoice Amount  <sup>*</sup></span>
                                    <input type="text" id="gstNumber" name="inAmount" value="<?php echo $fees[0]->total_bill; ?>" placeholder="Invoice Amount " class="form-control">
                                </div>
                            </div>
                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span> Invoice <sup>*</sup></span>
                                    <input type="file" accept="application/pdf,application/vnd.ms-excel,application/vnd.openxmlformats-officedocument.spreadsheetml.sheet" name="inFile"/>
                                    <br/>
                                    <small>(upload pdf/xls/xlsx files)</small>

                                </div>

                            </div>
                            <div class="form-group  col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">Upload</button>
                            </div>
                            <div class="clearfix"></div>
                        </form>


                        <div class="clearfix"></div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div class="modal fade" id="termWraper" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Terms & Conditions</h4>
                    </div>

                    <div class="modal-body">
                        1. In case the trainer needs to submit physical forms like attendance sheet ,feedback forms , assessments etc and/or petrol bills along with their physical invoice- Their payment period of 45 days would start from the date we receive their physical invoices along with required support documents & /or required forms in our Pune Office.<br/><br/>
                        2. In case there is no reimbursement & /or any formats to be submitted  and only invoice needs to be updated in CRM-  45th day would be counted from the date the complete and correct invoice is uploaded on the CRM.<br/><br/>
                        3. Also, please note that for fuel claims, the petrol/diesel/CNG bills needs to be sent to Pune office for reimbursement. This bills should of any prior dates of the training program.<br/>
                        <br/><br/><br/>
                        <div class="text-center">
                            <button class="btn btn-primary ok-invoice" style="width:90px;">OK</button>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">

            $(document).ready(function () {

                $('.editInvoice').click(function (e) {
                    e.preventDefault();
                    $('#createWraper').modal('show');
                });

                $('.courier-btn').click(function (e) {
                    e.preventDefault();
                    $('#courier_wraper').modal('show');
                });

                $("#gInvoice").validate({
                    rules: {
                        gstNumber: "required",
                        inNumber: "required",
                        inAmount: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

                $("#cUpdateForm").validate({
                    rules: {
                        ref: "required",
                        desc: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });
                
                function ginvoice(){
                    var pid = '<?php echo $fees[0]->id; ?>';
                    var project_id = '<?php echo $pid; ?>';
                    $('.page_spin').show();
                    var dataString = "project_id=" + project_id + "&pid=" + pid + "&page=confirm_invoice";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>trainer/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                }
                
                $('.ok-invoice').click(function (e) {
                    e.preventDefault();
                    $('#termWraper').modal('hide');
                    ginvoice();
                });

                $('.confirm-btn').click(function (e) {
                    e.preventDefault();
                    $('#termWraper').modal('show');
                });

                $('.reset-btn').click(function (e) {
                    e.preventDefault();
                    var pid = $(this).attr('pid');
                    var project_id = $(this).attr('project_id');
                    $('.page_spin').show();
                    var dataString = "project_id=" + project_id + "&pid=" + pid + "&page=reset_invoice";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>trainer/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.href = "<?php echo base_url(); ?>trainer/program_details/" + project_id;

                        }, //success fun end
                    });//ajax end
                });

                $('.remove-debit').click(function (e) {
                    e.preventDefault();
                    var did = $(this).attr('did');
                    var f = confirm("Are you sure want to reject debit note ?");
                    if (f == true) {
                        $('.page_spin').show();
                        var dataString = "did=" + did + "&page=remove_debit_note";
                        $.ajax({
                            type: "POST",
                            url: "<?php echo base_url(); ?>trainer/ajax_page",
                            data: dataString,
                            success: function (data) {
                                $('.page_spin').hide();
                                window.location.reload();
                            }, //success fun end
                        });//ajax end
                    }
                });

            });

        </script>

    </body>
</html>