<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Add Payment</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Content - Add New Payslip</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="../invoice/<?php echo $pid; ?>" class="btn btn-danger"><i class="fa fa-backward"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="col-md-12 content-page">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-plus"></i> Upload Payslip </h2>
                        </div>
                        <div class="panel-body">
                            <?php
                            if ($msg == 1) {
                                ?>
                                <div class="alert alert-success col-md-6 col-md-offset-3">
                                    Payslip Updated Successfully.
                                </div>
                                <?php
                            }
                            ?>
                            <div class="col-md-6">
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-content-form">
                                <input type="hidden" value="<?php echo $this->session->userdata('t_code'); ?>" name="tid"/>
				<?php if(!isset($pid)) { ?>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1">Program <sup>*</sup></span>
                                        <select class="form-control" name="program">
                                            <option value="">- Select -</option>
                                            <?php 
                                            if(!empty($program))
                                            {
                                                foreach($program as $pm_data)
                                                {
                                                    ?>
                                            <option value="<?php echo $pm_data->project_id; ?>" <?php if($pm_data->project_id==$pid){ echo 'selected'; } ?>><?php echo $pm_data->project_title; ?></option>
                                                    <?php
                                                }
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <?php } else  { ?>
				 <input type="hidden" value="<?php echo $pid; ?>" name="program"/>
				<?php } ?>
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Debit Type <sup>*</sup></span>
                                        <select class="form-control" name="dTitle">
                                            <option value="">- Select -</option>
					    <option value="Courier Charges">Courier Charges</option>
					    <option value="Food Expenses">Food Expenses</option>
					    <option value="Lamination Charges">Lamination Charges</option>
					    <option value="Misc. Expenses">Misc. Expenses</option>
					    <option value="Stay Charges">Stay Charges</option>
					    <option value="Stationary Charges">Stationary Purchase</option>
					    <option value="Training Props Purchase">Training Props Purchase</option>
                                            <option value="Travelling Charges">Travelling Charges</option>
                                            <option value="Xerox Expenses">Xerox Expenses</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Description</span>
                                        <input type="text" name="desc" class="form-control"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Amount <sup>*</sup></span>
                                        <input type="number" name="amt" class="form-control" />
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Upload Slip</span>
                                        <input type="file" name="content" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">

            $(document).ready(function () {
                $("#add-content-form").validate({
                    rules: {
                        program: "required",
                        dTitle: "required",
                        amt: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });

            });

        </script>

    </body>
</html>