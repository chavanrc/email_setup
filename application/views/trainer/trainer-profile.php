<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Trainer | Profile</title>
        <?php include 'css_files.php'; ?>
        <link href='<?php echo base_url(); ?>assets/css/jquery-ui.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/css/datepicker/jquery.datetimepicker.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
        <style>
            .panel-body i{
                display: inline-block;
                width: 20px;
            }
            .top-label{
                padding:10px; font-size: 12px; border:solid 1px #73879C;
                margin-left: -10px;
                color:#fff;
            }

            .ui-datepicker{
                z-index: 1060 !important;
            }

        </style>
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#"><i class="fa fa-info"></i> Details</a></li>
                            <li><a href="<?php echo base_url(); ?>trainer/trainer_engagment_request/<?php echo $trainer[0]->user_code; ?>"><i class="fa fa-paper-plane"></i> Engagement Requests</a></li>

                        </ul>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>

            <div class="row" style="margin: 0px;">
                <div class="col-md-6">
                    <div class="page-title title-left">
                        <h3> <?php echo $trainer[0]->name; ?></h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <?php
                        if ($trainer[0]->is_active == 1) {
                            ?>
                            <span class="top-label label-info">Active</span>
                            <?php
                        } else {
                            ?>
                            <span class="top-label label-danger">In Active</span>
                            <?php
                        }
                        if ($trainer[0]->admin_approved == 1) {
                            ?>
                            <span class="top-label label-success" style="margin-right:30px;">Approved</span>
                            <?php
                        } else {
                            ?>
                            <span class="top-label label-warning" style="margin-right:30px;">Approval Pending</span>
                            <?php
                        }
                        ?>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-info"></i> Basic Details </h2>
                                <a href="#" class="btn btn-sm btn-default pull-right change-pass-btn" style="margin-top:-25px;"><i class="fa fa-shield"></i> Change Password</a>
                                <a href="<?php echo base_url(); ?>trainer/update_trainer/<?php echo $trainer[0]->user_code; ?>" class="btn btn-sm btn-default pull-right" style="margin-top:-25px;"><i class="fa fa-edit"></i> Edit</a>
                            </div>
                            <div class="panel-body" style="line-height: 25px; position: relative;">

                                <img src="<?php echo base_url(); ?>assets/upload/trainer/<?php echo $trainer[0]->user_photo; ?>" style="width:150px;padding: 5px; border: solid 1px #ddd; position: absolute; right: 20px;">
                                <i class="fa fa-user"></i> <?php echo $trainer[0]->name; ?><br/>
                                <i class="glyphicon glyphicon-blackboard"></i> <?php echo $trainer[0]->trainer_area; ?><br/>
                                <i class="fa fa-envelope"></i> <?php echo $trainer[0]->email; ?><br/>
                                <i class="fa fa-mobile-phone"></i> <?php echo $trainer[0]->contact_number; ?><br/>
                                <i class="fa fa-birthday-cake"></i> <?php echo $trainer[0]->user_dob; ?><br/>
                                <i class="fa fa-venus-mars"></i> <?php echo $trainer[0]->user_gender; ?><br/>
                                <i class="fa fa-map-marker"></i> <?php echo $trainer[0]->country; ?><br/>
                                <i class="fa fa-map-pin"></i> <?php echo $trainer[0]->state; ?> , <?php echo $trainer[0]->city; ?><br/>
                                <i class="fa fa-map-signs"></i> <?php echo $trainer[0]->address; ?><br/>
                                <i class="fa fa-phone"></i> <?php echo $trainer[0]->contact_number_landline; ?><br/>
                                <i class="fa fa-skype"></i> <?php echo $trainer[0]->skypeID; ?><br/>
                                <i class="fa fa-credit-card"></i> <?php echo $trainer[0]->pan_no; ?><br/>
                                Industry :  <?php echo $trainer[0]->industry; ?><br/>
                                Freelancer :  <?php
                                if ($trainer[0]->freelancer == 'Y') {
                                    echo 'Yes';
                                }
                                if ($trainer[0]->freelancer == 'N') {
                                    echo 'No';
                                }
                                ?><br/>
                                Expected Fees P/D :  <?php echo $trainer[0]->expected_fee_per_day; ?><br/>
                            </div>
                        </div>
                        <?php
                        if (!empty($trainer[0]->user_cv)) {
                            ?>
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h2 class="panel-title" ><i class="fa fa-info"></i> Trainer CV / Profile </h2>
                                </div>
                                <div class="panel-body" style="line-height: 25px;">
                                    <a href="<?php echo base_url(); ?>assets/upload/trainer/<?php echo $trainer[0]->user_cv; ?>" class="btn btn-info btn-block">Download <i class="fa fa-download"></i></a>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>

                    <div class="col-md-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h2 class="panel-title" ><i class="fa fa-calendar"></i> Trainer Calendar </h2>
                                <a href="#" id="update-calendar-btn" class="btn btn-sm btn-default pull-right" style="margin-top:-25px;"><i class="fa fa-edit"></i> Update My Calendar</a>
                            </div>
                            <div class="panel-body" style="line-height: 25px;">
                                <?php
                                if ($msg == 'Calendar') {
                                    ?>
                                    <div class="alert alert-success">
                                        Calendar saved
                                    </div>
                                    <?php
                                }
                                ?>
                                <div id='calendar'></div>
                            </div>
                        </div>


                    </div>

                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-info"></i> Work Experience </h2>
                            <a href="<?php echo base_url(); ?>trainer/add_work/<?php echo $trainer[0]->user_code; ?>" class="btn btn-sm btn-default pull-right" style="margin-top:-25px;"><i class="fa fa-plus"></i> Add</a>
                        </div>
                        <div class="panel-body" style="line-height: 25px;">
                            <?php
                            $work = $CI->admin_model->get_trainer_workExperiance($trainer[0]->user_code);
                            if (!empty($work)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Designation</th>
                                            <th>Function</th>
                                            <th>Industry</th>
                                            <th>Company</th>
                                            <th>Period</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($work as $wk_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $wk_data->work_designation; ?></td>
                                                <td><?php echo $wk_data->work_function; ?></td>
                                                <td><?php echo $wk_data->work_industry; ?></td>
                                                <td><?php echo $wk_data->work_orgn; ?></td>
                                                <td><?php echo $wk_data->work_period_from; ?> - <?php echo $wk_data->work_period_to; ?> </td>
                                                <td>
                                                    <a href="<?php echo base_url(); ?>trainer/update_work/<?php echo $wk_data->user_code; ?>/<?php echo $wk_data->id; ?>" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></a>
                                                    <a href="#" class="btn btn-sm btn-danger remove-work" rid="<?php echo $wk_data->id; ?>"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>


                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="glyphicon glyphicon-blackboard"></i> Training Experience </h2>
                            <a href="<?php echo base_url(); ?>trainer/add_training/<?php echo $trainer[0]->user_code; ?>" class="btn btn-sm btn-default pull-right" style="margin-top:-25px;"><i class="fa fa-plus"></i> Add</a>
                        </div>
                        <div class="panel-body" style="line-height: 25px;">
                            <?php
                            $exp = $CI->admin_model->get_trainer_experience($trainer[0]->user_code);
                            if (!empty($exp)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Training Area</th>
                                            <th>Program</th>
                                            <th>Company</th>
                                            <th>Industry</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($exp as $ex_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $ex_data->area_of_training; ?></td>
                                                <td><?php echo $ex_data->program_title; ?></td>
                                                <td><?php echo $ex_data->company; ?></td>
                                                <td><?php echo $ex_data->industry; ?></td>
                                                <td>
                                                    <a href="#" class="btn btn-sm btn-danger remove-exp" rid="<?php echo $ex_data->id; ?>"><i class="fa fa-trash"></i></a>
                                                </td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-graduation-cap"></i> Education </h2>
                            <a href="<?php echo base_url(); ?>trainer/add_education/<?php echo $trainer[0]->user_code; ?>" class="btn btn-sm btn-default pull-right" style="margin-top:-25px;"><i class="fa fa-plus"></i> Add</a>
                        </div>
                        <div class="panel-body" style="line-height: 25px;">
                            <?php
                            $education = $CI->admin_model->trainer_education($trainer[0]->user_code);
                            if (!empty($education)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Qualification</th>
                                            <th>Discipline</th>
                                            <th>University</th>
                                            <th>Year of Passing</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($education as $ed_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $ed_data->qualification; ?></td>
                                                <td><?php echo $ed_data->discipline; ?></td>
                                                <td><?php echo $ed_data->university; ?></td>
                                                <td><?php echo $ed_data->year_of_passing; ?></td>
                                                <td><a href="#" class="btn btn-sm btn-danger remove-education" rid="<?php echo $ed_data->id; ?>"><i class="fa fa-trash"></i></a></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-graduation-cap"></i> Certifications </h2>
                            <a href="<?php echo base_url(); ?>trainer/add_certification/<?php echo $trainer[0]->user_code; ?>" class="btn btn-sm btn-default pull-right" style="margin-top:-25px;"><i class="fa fa-plus"></i> Add</a>
                        </div>
                        <div class="panel-body" style="line-height: 25px;">
                            <?php
                            $cert = $CI->admin_model->trainer_certifications($trainer[0]->user_code);
                            if (!empty($cert)) {
                                ?>
                                <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                            <th>Certification</th>
                                            <th>Level</th>
                                            <th>Awarded By</th>
                                            <!--<th>Validity</th>-->
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        foreach ($cert as $cr_data) {
                                            ?>
                                            <tr>
                                                <td><?php echo $cr_data->certification_in; ?></td>
                                                <td><?php echo $cr_data->certification_level; ?></td>
                                                <td><?php echo $cr_data->awarded_by; ?></td>
                                                
                                                <td><a href="#" class="btn btn-sm btn-danger remove-education" rid="<?php echo $cr_data->id; ?>"><i class="fa fa-trash"></i></a></td>
                                            </tr>
                                            <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    
                     <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-bank"></i> Bank Details </h2>
                            <a href="<?php echo base_url(); ?>trainer/update_bank/<?php echo $trainer[0]->user_code; ?>" class="btn btn-sm btn-default pull-right" style="margin-top:-25px;"><i class="fa fa-plus"></i> Update</a>
                        </div>
                        <div class="panel-body" style="line-height: 25px;">
                            <?php
                            $bank = $CI->admin_model->trainer_bankDetails($trainer[0]->user_code);
                            if (!empty($bank)) {
                                ?>
                                <table class="table table-bordered">
                                    <tbody>
                                        <tr>
                                            <td style="width:40%">Bank Name</td>
                                            <td><?php echo $bank[0]->bank_name; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Account Holder Name</td>
                                            <td><?php echo $bank[0]->account_name; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Account Number</td>
                                            <td><?php echo $bank[0]->bank_account; ?></td>
                                        </tr>
                                        <tr>
                                            <td>IFSC Code</td>
                                            <td><?php echo $bank[0]->bank_ifsc; ?></td>
                                        </tr>
                                        <tr>
                                            <td>Branch Name</td>
                                            <td><?php echo $bank[0]->bank_branch; ?></td>
                                        </tr>
					 <tr>
                                            <td>PAN No.</td>
                                            <td><?php echo $bank[0]->pan_no; ?><br/>
						<?php if($bank[0]->pan_image !="") { ?>
						<img src="/assets/upload/pan/<?php echo $bank[0]->pan_image; ?>" width="150">
						<?php } else { ?>
						PAN not Uploaded
						<?php } ?>
					   </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                </div>
            </div>
        </div>

        <div class="modal fade" id="update_dates" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Update My Calendar</h4>
                    </div>
                    <form action="" method="POST"  enctype="multipart/form-data" id="update-date-form">

                        <div class="modal-body">
                            <input type="hidden" name="date_user" value="<?php echo $this->session->userdata('t_code'); ?>"/>


                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> From <sup>*</sup></span>
                                    <input type="text" id="from" name="from"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> To <sup>*</sup></span>
                                    <input type="text" id="to" name="to"  class="form-control">
                                </div>
                            </div>

                            <div class="form-group  col-md-12">
                                <div class="input-group">
                                    <span class="input-group-addon" id="sizing-addon1"> Title <sup>*</sup></span>
                                    <input type="text" name="title" placeholder="Title" class="form-control">
                                </div>
                            </div>


                            <div class="form-group  col-md-12 text-center">
                            </div>
                            <div class="clearfix"></div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-info" ><i class="fa fa-upload"></i> Update</button>
                        </div>
                    </form>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->


         <div class="modal fade" id="update_password" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Change Password</h4>
                    </div>
                    <div class="modal-body">
                        <form action="" id="password-form" method="POST">
                                    <div class="alert alert-danger pass-error" style="display:none;">
                                        Current Password is wrong.
                                    </div>
                                    <div class="alert alert-success pass-success" style="display:none;">
                                        Password updated successfully.
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"> Current Password <sup>*</sup></span>
                                            <input type="password" id="old_password" name="old_password" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="input-group">
                                            <span class="input-group-addon" id="sizing-addon1"> New Password <sup>*</sup></span>
                                            <input type="password" id="new_password" name="new_password" class="form-control">
                                        </div>
                                    </div>
                                    <div class="form-group text-right">
                                        <button class="btn btn-info" type="submit"><i class="fa fa-send"></i> Update</button>
                                    </div>
                                </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <script src="<?php echo base_url(); ?>assets/js/jquery-1.11.3.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/bootstrap.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/jquery.validate-1.14.0.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/jquery-ui.js"></script>
        <script src="<?php echo base_url(); ?>assets/js/validation/additional-methods.js"></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/moment.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/jquery.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/fullcalendar/fullcalendar.min.js'></script>
        <script src='<?php echo base_url(); ?>assets/js/datepicker/jquery.datetimepicker.js'></script>
        <script type="text/javascript">
            $(document).ready(function () {

                $('#update-calendar-btn').click(function (e) {
                    e.preventDefault();
                    $('#update_dates').modal('show');
                });
                
                $('.change-pass-btn').click(function (e) {
                    e.preventDefault();
                    $('#update_password').modal('show');
                });


                var dateFormat = "yy-mm-dd",
                        from = $("#from")
                        .datepicker({
                            defaultDate: "+1w",
                            changeMonth: true,
                            numberOfMonths: 3,
                            dateFormat: 'yy-mm-dd'
                        })
                        .on("change", function () {
                            to.datepicker("option", "minDate", getDate(this));
                        }),
                        to = $("#to").datepicker({
                    defaultDate: "+1w",
                    changeMonth: true,
                    numberOfMonths: 3,
                    dateFormat: 'yy-mm-dd'
                })
                        .on("change", function () {
                            from.datepicker("option", "maxDate", getDate(this));
                        });

                function getDate(element) {
                    var date;
                    try {
                        date = $.datepicker.parseDate(dateFormat, element.value);
                    } catch (error) {
                        date = null;
                    }

                    return date;
                }

                $("#update-date-form").validate({
                    rules: {
                        from: "required",
                        to: "required",
                        title: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });


                $('#calendar').fullCalendar({
                    defaultDate: '<?php echo date("Y-m-d"); ?>',
                    editable: false,
                    eventLimit: true, // allow "more" link when too many events
                    events: [
<?php
if (!empty($calendar)) {
    foreach ($calendar as $cal_data) {
        ?>
                                {
                                    title: '<?php echo $cal_data->tc_title; ?>',
                                    start: '<?php echo $cal_data->from_date; ?>',
                                    end: '<?php echo $cal_data->to_date; ?>'
                                },
        <?php
    }
}
?>


                    ]
                });


                $("#assign-pm-form").validate({
                    rules: {
                        pm: "required",
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        assign_pm();
                    }
                });

                $('.approved-btn').click(function (e) {
                    var tid = $(this).attr('tid');

                    $('.page_spin').show();
                    var dataString = "tid=" + tid + "&page=trainer_make_approve";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                });
                
                $('.remove-work').click(function(e){
                e.preventDefault();
                var rid = $(this).attr('rid');
                var f = confirm("Are you sure want to remove work experience ?");
                if(f==true)
                {
                    $('.page_spin').show();
                    var dataString = "rid=" + rid + "&page=remove_work_experience";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                }
                });
                
                $('.remove-exp').click(function(e){
                e.preventDefault();
                var rid = $(this).attr('rid');
                var f = confirm("Are you sure want to remove training experience ?");
                if(f==true)
                {
                    $('.page_spin').show();
                    var dataString = "rid=" + rid + "&page=remove_training_experience";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                }
                });
                
                $('.remove-education').click(function(e){
                e.preventDefault();
                var rid = $(this).attr('rid');
                var f = confirm("Are you sure want to remove education details ?");
                if(f==true)
                {
                    $('.page_spin').show();
                    var dataString = "rid=" + rid + "&page=remove_education";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                }
                });
                
                $('.remove-certification').click(function(e){
                e.preventDefault();
                var rid = $(this).attr('rid');
                var f = confirm("Are you sure want to remove certification details ?");
                if(f==true)
                {
                    $('.page_spin').show();
                    var dataString = "rid=" + rid + "&page=remove_certification";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            window.location.reload();
                        }, //success fun end
                    });//ajax end
                }
                });
                
                
                $("#password-form").validate({
                    rules: {
                        old_password: "required",
                        new_password: "required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    },
                    submitHandler: function (form) {
                        change_password();
                    }
                });
                
                $('#old_password, #new_password').focus(function(){
                   $('.pass-error').hide(); 
                   $('.pass-success').hide(); 
                });
                
                function change_password()
                {
                    var old_pass = $('#old_password').val();
                    var new_pass = $('#new_password').val();
                    var uid = '<?php echo $this->session->userdata('t_code'); ?>';
                    
                    $('.page_spin').show();
                    var dataString = "old_pass=" + old_pass + "&new_pass=" + new_pass + "&uid="+uid+"&page=change_password";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo base_url(); ?>admin/ajax_page",
                        data: dataString,
                        success: function (data) {
                            $('.page_spin').hide();
                            var replay = $.trim(data);
                            if(replay=='1')
                            {
                                $('.pass-success').show();
                            }
                            else{
                                $('.pass-error').show();
                            }

                        }, //success fun end
                    });//ajax end
                }


            });
        </script>

    </body>
</html>