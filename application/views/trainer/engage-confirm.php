<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Admin | Add Payment</title>
        <?php include 'css_files.php'; ?>
    </head>
    <body>

        <div class="right-side">
            
            <div class="row" style="margin: 20px;">
                <div class="col-md-6 col-md-offset-2">
                    <div class="alert alert-success" style="font-size: 18px;">
                        <strong>Thank you,</strong> <br/>
                        You have successfully confirmed your program engagement.
                    </div>
                </div>

                
            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script type="text/javascript">


        </script>

    </body>
</html>