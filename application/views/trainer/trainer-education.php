<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Trainer | Education</title>
        <?php include 'css_files.php'; ?>
        <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/multiselect/bootstrap-multiselect.css">
    </head>
    <body>
        <?php include 'admin_sidemenu.php'; ?>

        <div class="right-side">
            <?php include 'admin_topmenu.php'; ?>
            <div class="row" style="margin: 0px;">
                <div class="col-md-12">
                    <div class="page-title title-left">
                        <h3>Add Education</h3>
                    </div>
                    <div class="page-title title-right text-right">
                        <a href="<?php echo base_url(); ?>trainer/trainer_profile/<?php echo $trainer[0]->user_code; ?>" class="btn btn-danger"><i class="fa fa-backward"></i> Back</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 content-page">
                    <?php
                    if($msg==1)
                    {
                        ?>
                    <div class="alert alert-success col-md-6 col-md-offset-3">
                        Education Updated Successfully.
                    </div>
                        <?php
                    }
                    ?>
                    <div class="clearfix"></div>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <h2 class="panel-title" ><i class="fa fa-plus"></i> <?php echo $trainer[0]->name; ?> </h2>
                        </div>
                        <div class="panel-body">
                            <form action="" method="POST"  enctype="multipart/form-data" id="add-education-form">
                                <input type="hidden" value="<?php echo $trainer[0]->user_code; ?>" name="user"/>
                                
                                
                                <?php
                                $graducation = array('B.A','B.Arch','BCA','B.B.A','B.Com','B.Ed','BDS','BHM','B.Pharma',
                                    'B.Sc','B.Tech/B.E.','LLB','MBBS','Diploma','BVSC','Other');
                                $pg = array('CA','CS','ICWA (CMA)','M.A','M.Arch','M.Com','M.Ed','M.Pharma','M.Sc','M.Tech',
                                    'MBA/PGDM','MCA','MS','PG Diploma','MVSC','MCM','Other');
                                $doct = array('Ph.D/Doctorate','MPHIL','Other');
                                ?>
                                <div class="form-group col-md-3 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> <i class="fa fa-graduation-cap"></i> </span>
                                        <select class="form-control"  name="qualificatioin">
                                            <option value="">- Select -</option>
                                            <optgroup label="Graduation"/>
                                           <?php
                                           foreach($graducation as $gd_data)
                                           {
                                               ?>
                                            <option value="Graduation,<?php echo $gd_data; ?>"><?php echo $gd_data; ?></option>
                                                <?php
                                           }
                                           ?>
                                            <optgroup label="Post Graduation"/>
                                           <?php
                                           foreach($pg as $pg_data)
                                           {
                                               ?>
                                            <option value="Post Graduation,<?php echo $pg_data; ?>"><?php echo $pg_data; ?></option>
                                                <?php
                                           }
                                           ?>
                                            <optgroup label="Doctorate"/>
                                            <?php
                                           foreach($doct as $d_data)
                                           {
                                               ?>
                                            <option value="Doctorate,<?php echo $d_data; ?>"><?php echo $d_data; ?></option>
                                                <?php
                                           }
                                           ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-5 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> University</span>
                                        <input type="text" name="university" class="form-control">
                                    </div>
                                </div>
                                
                                <div class="form-group col-md-4 col-sm-6 col-xs-12">
                                    <div class="input-group">
                                        <span class="input-group-addon" id="sizing-addon1"> Year of Passing</span>
                                        <select class="form-control" name="year">
                                            <option value="">Select</option>
                                            <?php
                                            $i = 1961;
					    $current_year = date ("Y");
                                            for($i=1961; $i <= $current_year; $i++)
                                            {
                                                ?>
                                            <option value="<?php echo $i; ?>"><?php echo $i; ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="clearfix"></div>
                                <div class="form-group">
                                    <div class="label col-md-3 col-sm-3 col-xs-12">
                                    </div>
                                    <div class="col-md-6 col-sm-6 col-xs-12" style="text-align: center;">
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php include 'js_files.php'; ?>
        <script src="<?php echo base_url(); ?>assets/js/multiselect/bootstrap-multiselect.js"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('#industry').multiselect({
                    nonSelectedText: '- Select Function -'
                });
                
                $("#add-education-form").validate({
                    rules: {
                        qualificatioin:"required",
                        university:"required",
                        year:"required"
                    },
                    tooltip_options: {
                        inst: {
                            trigger: 'focus',
                        },
                    }
                });
                
            });
        </script>

    </body>
</html>