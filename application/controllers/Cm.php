<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class cm extends CI_Controller {

    public function index() {
        $this->check_sess();
        $data['page_url'] = 'Dashboard';
        $this->load->view('cm/index', $data);
    }

    public function page_map($pr, $sc) {
        $this->$pr($sc);
    }

    public function check_sess() {
        if (!$this->session->userdata('cm')) {
            redirect('admin/login');
        }
    }

    public function profile() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['msg'] = 'new';
        $data['page_url'] = 'CM';
        if (isset($_POST['date_user'])) {
            $data['msg'] = $this->admin_model->save_trainer_calendar();
        }
        $data['cm'] = $this->admin_model->get_single_trainer($this->session->userdata('t_code'));
        $data['calendar'] = $this->admin_model->get_trainer_calendor($this->session->userdata('t_code'));
        $this->load->view('cm/profile', $data);
    }

    public function add_content() {
        $this->check_sess();
        $this->load->model('cm_model');
        $data['msg'] = 'new';
        $data['page_url'] = 'CM';

        if (isset($_POST['title'])) {
            $data['msg'] = $this->cm_model->save_content();
            if ($data['msg'] == 1) {
                redirect('cm/content_database?client=' . $_POST['client'] . '&modl=' . $_POST['modl'] . '&add=true');
            }
        }
        $this->load->view('cm/add-content', $data);
    }
    
    public function updateFile(){
        $typeArray = array('xls'=>'Spredsheet',
            'xlsx'=>'Spredsheet',
            'csv'=>'Spredsheet',
            'doc'=>'Document',
            'docx'=>'Document',
            'txt'=>'Document',
            'odt'=>'Document',
            'pdf'=>'PDF',
            'ppt'=>'Presentation',
            'pptx'=>'Presentation',
            'mp4'=>'Video',
            'mp3'=>'Video',
            'wav'=>'Video',
            'mov'=>'Video',
            'wmv'=>'Video',
            'flv'=>'Video',
            'jpg'=>'Image',
            'jpg'=>'Image',
            'png'=>'Image',
            'jpeg'=>'Image',
            'gif'=>'Image');
        $query = $this->db->query("SELECT content_id,file_type FROM content");
        $result = $query->result();
        foreach($result as $rsData){
            $this->db->query("UPDATE content SET content_type='".$typeArray[$rsData->file_type]."' WHERE content_id='".$rsData->content_id."'");
        }
    }

    public function edit_content($id) {
        $this->check_sess();
        $this->load->model('cm_model');
        $data['msg'] = 'new';
        $data['page_url'] = 'CM';
        if (isset($_POST['title'])) {
            $data['msg'] = $this->cm_model->update_content();
            if ($data['msg'] == 1) {
                redirect('cm/content_database?client=' . $_POST['client'] . '&modl=' . $_POST['modl'] . '&up=true');
            }
        }
        $data['content'] = $this->cm_model->get_single_content($id);
        $this->load->view('cm/edit-content', $data);
    }

    public function content_database() {
        $this->check_sess();
        $this->load->model('cm_model');
        $data['msg'] = 'new';
        $data['page_url'] = 'CM';
        $this->load->view('cm/content-list', $data);
    }

    public function search_content() {
        $this->check_sess();
        $this->load->model('cm_model');
        $data['msg'] = 'new';
        $data['page_url'] = 'CM';
        $this->load->view('cm/search-content', $data);
    }

    public function recent_content() {
        $this->check_sess();
        $this->load->model('cm_model');
        $data['msg'] = 'new';
        $data['page_url'] = 'CM';
        $data['content'] = $this->cm_model->get_recent_content();
        $this->load->view('cm/recent-content', $data);
    }

    public function load_content() {
        $this->load->model('cm_model');
        $data['msg'] = 'new';
        $data['page_url'] = 'CM';
        $data['modl'] = $this->cm_model->get_client_module($_POST['cid'], $_POST['modl']);
        $this->load->view('cm/load-content', $data);
    }

    public function search_load() {
        $this->load->model('cm_model');
        $data['msg'] = 'new';
        $data['page_url'] = 'CM';
        $data['search'] = $this->cm_model->search_content();
        $this->load->view('cm/load-search', $data);
    }

    public function client_modules($id = NULL) {
        $this->load->model('cm_model');
        $data['msg'] = 'new';
        $data['page_url'] = 'CM';
        $data['cid'] = $id;
        if (isset($_POST['cid'])) {
            $data['msg'] = $this->cm_model->save_module();
        }
        if (isset($_POST['cmid'])) {
            $data['msg'] = $this->cm_model->update_module();
        }
        $data['modl'] = $this->cm_model->get_client_module($id);
        $this->load->view('cm/all-modules', $data);
    }
    
    public function program_details($id) {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 'new';
        if(isset($_POST['tid'])){
            $this->projectmanager_model->updateCmTTT();
        }
        $data['program'] = $this->projectmanager_model->get_single_program($id);
        $this->load->view('cm/program-details', $data);
    }

    public function download_zip_today() {

        echo custome_encode('55');

//        $files = array(base_url().'assets/upload/content/1_ticket.pdf', base_url().'assets/upload/content/ticket.pdf');
//        $zipname = 'file.zip';
//        $zip = new ZipArchive;
//        $zip->open($zipname, ZipArchive::CREATE);
//        foreach ($files as $file) {
//            $zip->addFile($file);
//        }
//        $zip->close();
//
//        header('Content-Type: application/zip');
//        header('Content-disposition: attachment; filename=' . $zipname);
//        header('Content-Length: ' . filesize($zipname));
//        readfile($zipname);
    }

    public function logout() {
        $this->session->unset_userdata('cm', '');
        $this->session->unset_userdata('t_name', '');
        $this->session->unset_userdata('t_id', '');
        $this->session->unset_userdata('t_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function ajax_page() {
        $this->load->model('cm_model');
        echo $this->cm_model->$_POST['page']($_POST);
    }

}
