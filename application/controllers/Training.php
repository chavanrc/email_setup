<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class training extends CI_Controller {

    public function index() {
        $this->check_sess();
        $data['page_url'] = 'Dashboard';
        $this->load->view('training/index', $data);
    }

    public function page_map($pr, $sc) {
        $this->$pr($sc);
    }

    public function check_sess() {
        if (!$this->session->userdata('training')) {
            redirect('admin/login');
        }
    }

    public function profile() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['msg'] = 'new';
        $data['page_url'] = 'Support';
        if (isset($_POST['date_user'])) {
            $data['msg'] = $this->admin_model->save_trainer_calendar();
        }
        $data['support'] = $this->admin_model->get_single_trainer($this->session->userdata('t_code'));
        $data['calendar'] = $this->admin_model->get_trainer_calendor($this->session->userdata('t_code'));
        $this->load->view('training/profile', $data);
    }
    
    public function add_program() {
        $this->check_sess();
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Create Program';
        $data['msg'] = 0;
        if (isset($_POST['client'])) {
            $data['msg'] = $this->projectmanager_model->save_program();
        }
        if (isset($_POST['stay'])) {
            $data['msg'] = $this->projectmanager_model->update1_program();
            redirect('training/program_content/' . $data['msg']);
        }
        $data['client'] = $this->projectmanager_model->get_client();
        $this->load->view('training/add-program', $data);
    }
    
    public function program_content($id) {
        $this->check_sess();
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Program Content';
        $data['msg'] = 0;
        $data['pid'] = $id;
        if (isset($_POST['pid'])) {
            $data['msg'] = $this->projectmanager_model->save_content_setting();
            redirect('training/program_details/' . $id);
        }
        $data['pcontent'] = $this->projectmanager_model->get_content_setting($id);
        $data['modules'] = $this->projectmanager_model->get_modules($id);
        $this->load->view('training/program-content', $data);
    }

    public function upcoming_programs($id) {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 0;
        $data['client'] = $this->admin_model->get_basic_client();
        $data['program'] = $this->admin_model->get_basic_programs();
        $data['count'] = $this->admin_model->getUpCount();
        $this->load->view('training/upcoming-programs', $data);
    }

    public function past_programs($id) {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Past Programs';
        $data['msg'] = 0;
        $data['client'] = $this->admin_model->get_basic_client();
        $data['pm'] = $this->admin_model->get_basicpm();
        $data['program'] = $this->admin_model->get_basic_past_programs();
        $data['count'] = $this->admin_model->getPastCount();
        $this->load->view('training/past-programs', $data);
    }
    
    public function export_program() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        
        $filter = "";
        
        
        if($_POST['year']!='All'){
            $filter .= " AND YEAR(p.training_start_date)='".$_POST['year']."' ";
        }
        
        if($_POST['month']!='All'){
            $filter .= " AND MONTH(p.training_start_date)='".$_POST['month']."' ";
        }
        
        if($_POST['client']!='All'){
            $filter .= " AND p.client_id='".$_POST['client']."' ";
        }
        
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT p.client_id,p.project_id,p.project_title,p.client_charges,p.client_payment_receieved,p.client_debit_received,p.location_of_training,p.training_start_date,p.training_end_date,p.training_duration,p.venue,p.stay_arrangement,p.travel_arrangement,p.program_state,p.zone,(SELECT client_name FROM clients WHERE client_id=p.client_id ) AS client_name, (SELECT invoice_type FROM clients WHERE client_id=p.client_id ) AS invoice_type,(SELECT cm.company_name FROM clients c INNER JOIN companies cm ON c.company_id=cm.company_id  WHERE client_id=p.client_id ) AS company_name,(SELECT SUM(expense_amount) FROM program_stay WHERE project_id=p.project_id) AS stayAmount,(SELECT SUM(expense_amount) FROM program_travel WHERE project_id=p.project_id) AS travelAmount,(SELECT SUM(debit_amt) FROM debit_note WHERE project=p.project_id AND debit_status='1') AS trainer_debit,u.name AS pname,t.name,t.city,t.trainer_credit,pt.amount,pt.trainer_date_to,pt.trainer_invoice_flag,pt.trainer_paid_status,pt.trainer_invoice_action,pt.invoice_print FROM training_projects p INNER JOIN application_users u ON p.user_code=u.user_code, program_trainers pt INNER JOIN application_users t ON t.user_code=pt.trainer_id WHERE p.training_start_date<'" . $today . "' AND p.training_start_date>='2019-07-1' $filter AND  p.is_active='1' AND pt.project_id=p.project_id ORDER BY p.training_start_date DESC");

        $data['programs'] = $query->result();
        $filename = "Programs" . time() . ".xls";
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $this->load->view('admin/export-program', $data);
    }

    public function program_details($id) {
        $this->check_sess();
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 'new';
        if (isset($_POST['content_user'])) {
            $data['msg'] = $this->projectmanager_model->save_content();
        }
        if (isset($_POST['form_user'])) {
            $data['msg'] = $this->projectmanager_model->save_form();
        }
        if (isset($_POST['cname'])) {
            $data['msg'] = $this->projectmanager_model->save_courier();
        }
        if (isset($_POST['stay_user'])) {
            $data['msg'] = $this->projectmanager_model->book_stay();
        }
        if (isset($_POST['travel_user'])) {
            $data['msg'] = $this->projectmanager_model->book_travel();
        }
        if (isset($_POST['img_user'])) {
            $data['msg'] = $this->projectmanager_model->save_images();
        }
        $data['program'] = $this->projectmanager_model->get_single_program($id);

        $this->load->view('training/program-details', $data);
    }

    public function program_expenses($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';

        if (!empty($id)) {
            $this->load->model('projectmanager_model');
            if (isset($_POST['amt'])) {
                $this->projectmanager_model->save_support_expense();
            }
            $data['exp'] = $this->projectmanager_model->get_program_expense($id);
            $data['debit'] = $this->projectmanager_model->get_debit_note($id);
            $data['fees'] = $this->projectmanager_model->get_project_fees($id);
        }
        $data['pid'] = $id;
        $this->load->view('training/program-expense', $data);
    }

    public function engagement_requests($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        if (!empty($id)) {
            $this->load->model('projectmanager_model');
            $data['request'] = $this->projectmanager_model->get_requests($id);
        }
        $data['pid'] = $id;
        $this->load->view('training/engagement-request', $data);
    }

    public function invoice_list() {
        $this->check_sess();
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Payments';
        $data['msg'] = 'new';
        $data['client'] = $this->projectmanager_model->get_basic_client();
        $data['payment'] = $this->projectmanager_model->get_support_payment();
        $data['count'] = $this->projectmanager_model->getTInvoiceCount();
        $this->load->view('training/payments', $data);
    }

    public function invoice($tid, $pid) {
        $this->check_sess();
        $this->load->model('trainer_model');
        $data['page_url'] = 'Payment';
        $data['msg'] = 'new';
        if (isset($_POST['pid'])) {
            $this->trainer_model->support_confirm_invoice();
        }
        $data['debit'] = $this->trainer_model->get_invoice_details($tid, $pid);
        $data['fees'] = $this->trainer_model->get_trainer_fees($tid, $pid);
        $data['pDetails'] = $this->trainer_model->get_client_details($pid);
        $data['tDetails'] = $this->trainer_model->get_trainer_details($tid);
        $data['bank'] = $this->trainer_model->get_trainer_bank($tid);
        $data['tid'] = $tid;
        $data['pid'] = $pid;
        $data['courier'] = $this->trainer_model->get_bills_courier($pid, $tid);
        $this->load->view('training/inovice', $data);
    }

    public function printinvoice($tid, $pid) {
        $this->check_sess();
        $this->load->model('trainer_model');
        $data['page_url'] = 'Payment';
        $data['msg'] = 'new';
        $data['pid'] = $pid;
        $data['debit'] = $this->trainer_model->get_invoice_details($tid, $pid);
        $data['fees'] = $this->trainer_model->get_trainer_fees($tid, $pid);
        $data['pDetails'] = $this->trainer_model->get_client_details($pid);
        $data['tDetails'] = $this->trainer_model->get_trainer_details($tid);
        $data['bank'] = $this->trainer_model->get_trainer_bank($tid);

        $this->load->view('training/inovice_print', $data);
    }

    public function trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['trainer'] = $this->admin_model->get_trainer();
        $data['count'] = $this->admin_model->getTrainerCount();
        $this->load->view('training/trainers', $data);
    }

    public function inactive_trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['trainer'] = $this->admin_model->get_inactive_trainer();
        $this->load->view('training/inactive_trainers', $data);
    }

    public function pending_trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['trainer'] = $this->admin_model->get_pending_trainer();
        $this->load->view('training/pending_trainers', $data);
    }
    
    public function search_trainer() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['state'] = $this->admin_model->get_state();
        $this->load->view('training/search-trainer', $data);
    }

    public function add_trainer() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['msg'] = "new";
        if (isset($_POST['pname'])) {
            $data['msg'] = $this->admin_model->save_trainer();
        }
        $data['state'] = $this->admin_model->get_state();
        $this->load->view('training/add-trainer', $data);
    }

    public function trainer_profile($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['trainer'] = $this->admin_model->get_single_trainer($id);
            $this->load->view('training/trainer-profile', $data);
        }
    }

    public function update_trainer($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            if (isset($_POST['pname'])) {
                $data['msg'] = $this->admin_model->update_trainer();
            }
            $data['trainer'] = $this->admin_model->get_single_trainer($id);
            $data['state'] = $this->admin_model->get_state();
            $data['city'] = $this->admin_model->get_city_list($data['trainer'][0]->state);
            $this->load->view('training/update-trainer', $data);
        }
    }

    public function add_feedback($id) {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Feedback';
        $data['msg'] = "new";
        $data['pid'] = $id;
        if (isset($_POST['pname'])) {
            $data['msg'] = $this->admin_model->save_feedback();
        }
        $data['fGroup'] = $this->admin_model->get_feedbackGroup();
        $this->load->view('training/add-feedback', $data);
    }

    public function feedback($id) {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Feedback';
        $data['msg'] = "new";
        $data['pid'] = $id;
        $data['fGroup'] = $this->admin_model->get_feedbackGroup();
        $this->load->view('training/feedback', $data);
    }

    public function download_feedback($id) {
        $id = onlyStringDecode($id);
        $this->load->model('admin_model');
        $data['page_url'] = 'Feedback';
        $data['msg'] = "new";
        $data['pid'] = $id;
        $data['project'] = $this->admin_model->get_basic_project($id);
        $data['fGroup'] = $this->admin_model->get_feedbackGroup();
        $this->load->view('training/view-feedback', $data);
    }

    public function logout() {
        $this->session->unset_userdata('training', '');
        $this->session->unset_userdata('t_name', '');
        $this->session->unset_userdata('t_id', '');
        $this->session->unset_userdata('t_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function ajax_page() {
        $this->load->model('trainer_model');
        echo $this->trainer_model->$_POST['page']($_POST);
    }

}
