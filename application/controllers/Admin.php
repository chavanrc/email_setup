<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin extends CI_Controller {

    public function index() {
        $this->check_sess();
        if ($this->session->userdata('type') == 'drona') {
            redirect('admin/drona_invoice');
        }
        $this->load->model('admin_model');
        $data['page_url'] = 'Dashboard';
        $data['week'] = $this->admin_model->getWeekCount();
        $data['month'] = $this->admin_model->getMonthCount();
        $data['total'] = $this->admin_model->getTotalCount();
        $this->load->view('admin/index', $data);
    }
    
    public function getData(){
        $filename = "Debitfile" . time() . ".xls";
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $query = $this->db->query("SELECT d.*,u.name,u.email,p.project_title,p.location_of_training,p.training_start_date FROM debit_note d INNER JOIN application_users u ON d.trainer=u.user_code INNER JOIN  training_projects p ON p.project_id=d.project WHERE d.debit_file!='' AND  d.debit_status='1'");
        $result = $query->result();
        echo '<table border="1px">';
        echo '<tr>';
        echo '<td>Trainer</td>';
        echo '<td>Email</td>';
        echo '<td>Program</td>';
        echo '<td>Date</td>';
        echo '<td>Location</td>';
        echo '<td>Debit Type</td>';
        echo '<td>Desc</td>';
        echo '<td>Amount</td>';
        echo '<td>File Name</td>';
        echo '</tr>';
        foreach($result as $rs){
            echo '<tr>';
            echo '<td>'.$rs->name.'</td>';
            echo '<td>'.$rs->email.'</td>';
            echo '<td>'.$rs->project_title.'</td>';
            echo '<td>'.$rs->training_start_date.'</td>';
            echo '<td>'.$rs->location_of_training.'</td>';
            echo '<td>'.$rs->debit_title.'</td>';
            echo '<td>'.$rs->debit_desc.'</td>';
            echo '<td>'.$rs->debit_amt.'</td>';
            echo '<td>'.$rs->debit_file.'</td>';
            echo '</tr>';
        }
        echo '</table>';
    }

    public function page_map($pr, $sc) {
        $this->$pr($sc);
    }
    

    public function check_sess() {
        if (!$this->session->userdata('admin')) {
            redirect('admin/login');
        }
    }

    public function login() {

        $this->load->view('admin/login');
    }

    public function logout() {
        $this->session->unset_userdata('drona', '');
        $this->session->unset_userdata('admin', '');
        $this->session->unset_userdata('name', '');
        $this->session->unset_userdata('user_id', '');
        $this->session->unset_userdata('user_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function pm_logout() {
        $this->session->unset_userdata('pm', '');
        $this->session->unset_userdata('pm_name', '');
        $this->session->unset_userdata('pm_id', '');
        $this->session->unset_userdata('pm_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function trainer_logout() {
        $this->session->unset_userdata('trainer', '');
        $this->session->unset_userdata('t_name', '');
        $this->session->unset_userdata('t_id', '');
        $this->session->unset_userdata('t_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function bdm_logout() {
        $this->session->unset_userdata('bdm', '');
        $this->session->unset_userdata('t_name', '');
        $this->session->unset_userdata('t_id', '');
        $this->session->unset_userdata('t_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function cm_logout() {
        $this->session->unset_userdata('cm', '');
        $this->session->unset_userdata('t_name', '');
        $this->session->unset_userdata('t_id', '');
        $this->session->unset_userdata('t_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function support_logout() {
        $this->session->unset_userdata('support', '');
        $this->session->unset_userdata('t_name', '');
        $this->session->unset_userdata('t_id', '');
        $this->session->unset_userdata('t_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function add_client() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Clients';
        $data['msg'] = 0;
        if (isset($_POST['fname'])) {
            $data['msg'] = $this->admin_model->save_client();
        }
        $data['pm'] = $this->admin_model->get_pm();
        $this->load->view('admin/add-clients', $data);
    }

    public function update_password($code = NULL, $id = NULL) {
        if (!empty($code) && !empty($id)) {
            $this->load->model('admin_model');
            // $link = $this->admin_model->forgot_password('anilk.acs@gmail.com');
            $data['email'] = custome_decode($id);
            $data['user_code'] = $code;
            $this->load->view('admin/update-password', $data);
        }
    }

    public function email_temp() {
        $this->load->view('email_temp/forgot-password');
    }
    
    public function approve_request($rid){
        $this->db->query("UPDATE training_projects SET edit_status='1' WHERE edit_status='".$rid."'");
        echo '<h2>Request Approved</h2>';
    }

    public function clients() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Clients';
        $data['client'] = $this->admin_model->get_client();
        $data['pm'] = $this->admin_model->get_pm();
        $this->load->view('admin/clients', $data);
    }

    public function client_gst($id = NULL) {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Clients';
        $data['cid'] = $id;
        $data['msg'] = 0;
        if (isset($_POST['ccid'])) {
            $data['msg'] = $this->admin_model->addClient_gst();
        }
        if (isset($_POST['cid'])) {
            $this->load->view('admin/lib/excel_reader2');
            $this->load->view('admin/lib/SpreadsheetReader');
            $data['msg'] = $this->admin_model->import_gst();
        }
        $data['client'] = $this->admin_model->get_basic_client();
        $data['gst'] = $this->admin_model->get_client_gst($id);
        $this->load->view('admin/client-gst', $data);
    }

    public function upate_client($id = NULL) {
        $this->check_sess();
        $this->load->model('admin_model');
        if (!empty($id)) {
            $data['msg'] = 0;
            $data['page_url'] = 'Clients';
            if (isset($_POST['fname'])) {
                $data['msg'] = $this->admin_model->update_client();
            }
            $data['client'] = $this->admin_model->get_single_client($id);
            $this->load->view('admin/edit-client', $data);
        }
    }

    public function add_pm() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Clients';
        $data['msg'] = 'new';
        if (isset($_POST['fname'])) {
            $data['msg'] = $this->admin_model->save_pm();
        }
        $data['client'] = $this->admin_model->get_client_assign_pending();
        $this->load->view('admin/add-pm', $data);
    }

    public function profile() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Dashboard';
        $data['trainer'] = $this->admin_model->get_single_trainer($this->session->userdata('user_code'));
        $this->load->view('admin/profile', $data);
    }

    public function project_manager() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Project Managers';
        $data['pm'] = $this->admin_model->get_pm();
        $data['client'] = $this->admin_model->get_client_assign_pending();
        $this->load->view('admin/project-manager', $data);
    }

    public function edit_pm($id) {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Clients';
        $data['msg'] = 'new';
        if (isset($_POST['fname'])) {
            $data['msg'] = $this->admin_model->update_pm();
        }
        $data['pm'] = $this->admin_model->get_single_pm($id);
        $this->load->view('admin/edit-pm', $data);
    }

    public function add_content() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Content';
        $data['msg'] = 'new';
        if (isset($_POST['content_user'])) {
            $data['msg'] = $this->admin_model->save_content();
        }
        $this->load->view('admin/add-content', $data);
    }

    public function contents() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Content';
        $data['msg'] = 'new';
        $data['content'] = $this->admin_model->get_all_content();
        $this->load->view('admin/content', $data);
    }

    public function payments() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Payments';
        $data['msg'] = 'new';
        $data['payment'] = $this->admin_model->get_all_payment();
        $this->load->view('admin/payment', $data);
    }

    public function trainer_invoices() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Payments';
        if(isset($_POST['sel'])){
            $this->admin_model->makeBulkPayment();
        }
        $data['msg'] = 'new';
        $data['payment'] = $this->admin_model->get_all_payment();
        $data['count'] = $this->admin_model->getTInvoiceCount();
        $this->load->view('admin/invoice-list', $data);
    }

    public function export_trainer_invoice() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Payments';
        $data['msg'] = 'new';
        $query = $this->db->query("SELECT pt.*,p.project_id,p.project_title,p.training_duration,u.user_code,u.name FROM program_trainers pt INNER JOIN training_projects p ON pt.project_id=p.project_id INNER JOIN application_users u ON  pt.trainer_id=u.user_code WHERE pt.trainer_invoice_flag='1' AND pt.trainer_invoice_action='1' ORDER BY p.training_start_date DESC");
        $data['payment'] = $query->result();
        $filename = "TrainerInvoice" . time() . ".xls";
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $this->load->view('admin/export-trainer-invoice', $data);
    }

    public function trainer_inovice_details($tid, $pid) {
        $this->check_sess();

        $data['page_url'] = 'Payment';
        $data['msg'] = 'new';
        if (isset($_POST['pid'])) {
            $this->load->model('admin_model');
            $this->admin_model->update_payment();
        }
        $data['tid'] = $tid;
        $data['pid'] = $pid;
        $this->load->model('trainer_model');
        $data['debit'] = $this->trainer_model->get_invoice_details($tid, $pid);
        $data['fees'] = $this->trainer_model->get_trainer_fees($tid, $pid);
        $data['pDetails'] = $this->trainer_model->get_client_details($pid);
        $data['tDetails'] = $this->trainer_model->get_trainer_details($tid);
        $data['bank'] = $this->trainer_model->get_trainer_bank($tid);
        $data['payment'] = $this->trainer_model->get_trainer_payment($data['fees'][0]->id);
        $this->load->view('admin/trainer-invoice', $data);
    }

    public function trainer_inovice_print($tid, $pid) {
        $this->check_sess();

        $data['page_url'] = 'Payment';
        $data['msg'] = 'new';
        $this->load->model('trainer_model');
        $data['debit'] = $this->trainer_model->get_invoice_details($tid, $pid);
        $data['fees'] = $this->trainer_model->get_trainer_fees($tid, $pid);
        $data['pDetails'] = $this->trainer_model->get_client_details($pid);
        $data['tDetails'] = $this->trainer_model->get_trainer_details($tid);
        $data['bank'] = $this->trainer_model->get_trainer_bank($tid);
        $data['payment'] = $this->trainer_model->get_trainer_payment($data['fees'][0]->id);
        $this->load->view('admin/trainer-invoice-print', $data);
    }

    public function trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['trainer'] = $this->admin_model->get_trainer();
        $data['count'] = $this->admin_model->getTrainerCount();
        $this->load->view('admin/trainers', $data);
    }

    public function export_trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $query = $this->db->query("SELECT * FROM application_users WHERE user_type='trainer' AND is_active='1' ORDER BY create_date DESC");
        $data['trainer'] = $query->result();
        $filename = "Trainer" . time() . ".xls";
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $this->load->view('admin/export-trainer', $data);
    }

    public function export_program() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';

        $filter = "";
        $clientFilter = "";

        if ($_POST['year'] != 'All') {
            $filter .= " AND YEAR(p.training_start_date)='" . $_POST['year'] . "' ";
        }

        if ($_POST['month'] != 'All') {
            $filter .= " AND MONTH(p.training_start_date)='" . $_POST['month'] . "' ";
        }

        if ($_POST['client'] != 'All') {
            $filter .= " AND p.client_id='" . $_POST['client'] . "' ";
        }
        if(isset($_POST['company'])){
            if($_POST['company']!='All'){
                $clientFilter = " AND cc.company_id='" . $_POST['company'] . "' ";
            }
        }

        $today = date('Y-m-d');
        $query = $this->db->query("SELECT p.client_id,p.project_id,p.project_title,p.client_charges,p.client_payment_receieved,p.client_debit_received,p.location_of_training,p.training_start_date,p.training_end_date,p.training_duration,p.venue,p.stay_arrangement,p.travel_arrangement,p.program_state,p.zone,(SELECT client_name FROM clients WHERE client_id=p.client_id ) AS client_name, (SELECT invoice_type FROM clients WHERE client_id=p.client_id ) AS invoice_type,(SELECT cm.company_name FROM clients c INNER JOIN companies cm ON c.company_id=cm.company_id  WHERE c.client_id=p.client_id) AS company_name,(SELECT SUM(expense_amount) FROM program_stay WHERE project_id=p.project_id) AS stayAmount,(SELECT SUM(expense_amount) FROM program_travel WHERE project_id=p.project_id) AS travelAmount,(SELECT SUM(debit_amt) FROM debit_note WHERE project=p.project_id AND debit_status='1') AS trainer_debit,u.name AS pname,t.name,t.city,t.trainer_credit,pt.amount,pt.trainer_date_to,pt.trainer_invoice_flag,pt.trainer_paid_status,pt.trainer_invoice_action,pt.invoice_print FROM training_projects p INNER JOIN application_users u ON p.user_code=u.user_code INNER JOIN clients cc ON p.client_id=cc.client_id $clientFilter, program_trainers pt INNER JOIN application_users t ON t.user_code=pt.trainer_id WHERE p.training_start_date<'" . $today . "' AND p.training_start_date>='2019-07-1' $filter AND  p.is_active='1' AND pt.project_id=p.project_id ORDER BY p.training_start_date DESC");

        $data['programs'] = $query->result();
        $filename = "Programs" . time() . ".xls";
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $this->load->view('admin/export-program', $data);
    }

    public function inactive_trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['trainer'] = $this->admin_model->get_inactive_trainer();
        $this->load->view('admin/inactive_trainers', $data);
    }

    public function pending_trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['trainer'] = $this->admin_model->get_pending_trainer();
        $this->load->view('admin/pending_trainers', $data);
    }

    public function search_trainer() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['state'] = $this->admin_model->get_state();
        $this->load->view('admin/search-trainer', $data);
    }

    public function add_trainer() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['msg'] = "new";
        if (isset($_POST['pname'])) {
            $data['msg'] = $this->admin_model->save_trainer();
        }
        $data['state'] = $this->admin_model->get_state();
        $this->load->view('admin/add-trainer', $data);
    }

    public function trainer_profile($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['trainer'] = $this->admin_model->get_single_trainer($id);
            $data['exp'] = $this->admin_model->get_trainer_experience($id);
            $this->load->view('admin/trainer-profile', $data);
        }
    }

    public function trainer_single_profile($id) {
        if (!empty($id)) {
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['trainer'] = $this->admin_model->get_single_trainer($id);
            $data['exp'] = $this->admin_model->get_trainer_experience($id);
            $data['exp'] = $this->admin_model->get_trainer_experience($id);
            $this->load->view('admin/trainer-single-profile', $data);
        }
    }

    public function trainer_calendar($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $this->load->view('admin/trainer-calendar', $data);
        }
    }
    
    public function trainer_tds_certificate($tid) {
       if (!empty($tid)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            $data["trainer_id"]=$tid;
            $data['trainer'] = $this->admin_model->get_basic_trainer($tid);
            //$data['engage'] = $this->admin_model->get_trainer_engagement($id);
            if (isset($_POST['year'])) {
            $data['msg'] = $this->admin_model->save_trainer_tds_certificate();
            }
            $data['tds'] = $this->admin_model->get_all_tds_certificate($tid);
            //$this->load->view('email_temp/tds-certificate');
            $this->load->view('admin/trainer-tds-certificate', $data);
       }
    }
    
    public function tds_email_temp() {
        $this->load->view('email_temp/tds-certificate');
    }
    

    public function update_trainer($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            if (isset($_POST['pname'])) {
                $data['msg'] = $this->admin_model->update_trainer();
            }
            $data['trainer'] = $this->admin_model->get_single_trainer($id);
            $data['state'] = $this->admin_model->get_state();
            $data['city'] = $this->admin_model->get_city_list($data['trainer'][0]->state);
            $this->load->view('admin/update-trainer', $data);
        }
    }

    public function trainer_experience($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $data['exp'] = $this->admin_model->get_trainer_experience($id);
            $this->load->view('admin/trainer-experience', $data);
        }
    }

    public function trainer_program_engagment($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $data['engage'] = $this->admin_model->get_trainer_engagement($id);
            $this->load->view('admin/trainer-program-engagement', $data);
        }
    }

    public function upcoming_programs($id) {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 0;
        $data['client'] = $this->admin_model->get_basic_client();
        $data['program'] = $this->admin_model->get_basic_programs();
        $data['count'] = $this->admin_model->getUpCount();
        $this->load->view('admin/upcoming-programs', $data);
    }

    public function past_programs($id) {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Past Programs';
        $data['msg'] = 0;
        $data['client'] = $this->admin_model->get_basic_client();
        $data['pm'] = $this->admin_model->get_basicpm();
        $data['program'] = $this->admin_model->get_basic_past_programs();
        $data['count'] = $this->admin_model->getPastCount();
        $this->load->view('admin/past-programs', $data);
    }

    public function review($id, $u) {
        $id = explode('-', custome_decode($id));
        $data['id'] = $id[0];
        $data['pid'] = $id[1];
        $data['type'] = custome_decode($u);
        $this->load->model('admin_model');
        $data['msg'] = 0;
        if (isset($_POST['comment'])) {
            $data['msg'] = $this->admin_model->save_review();
        }
        $data['program'] = $this->admin_model->get_request_details($data['id']);
        $this->load->view('admin/review', $data);
    }

    public function program_details($id) {
        $this->check_sess();
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 'new';
        if (isset($_POST['content_user'])) {
            $data['msg'] = $this->projectmanager_model->save_content();
        }
        if (isset($_POST['form_user'])) {
            $data['msg'] = $this->projectmanager_model->save_form();
        }
        if (isset($_POST['cname'])) {
            $data['msg'] = $this->projectmanager_model->save_courier();
        }
        if (isset($_POST['stay_user'])) {
            $data['msg'] = $this->projectmanager_model->book_stay();
        }
        if (isset($_POST['travel_user'])) {
            $data['msg'] = $this->projectmanager_model->book_travel();
        }
        if (isset($_POST['img_user'])) {
            $data['msg'] = $this->projectmanager_model->save_images();
        }
        $data['program'] = $this->projectmanager_model->get_single_program($id);

        $this->load->view('admin/program-details', $data);
    }

    public function program_expenses($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';

        if (!empty($id)) {
            $this->load->model('projectmanager_model');
            $data['exp'] = $this->projectmanager_model->get_program_expense($id);
            $data['debit'] = $this->projectmanager_model->get_debit_note($id);
            $data['fees'] = $this->projectmanager_model->get_project_fees($id);
        }
        $data['pid'] = $id;
        $this->load->view('admin/program-expense-list', $data);
    }

    public function engagement_requests($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        if (!empty($id)) {
            $this->load->model('projectmanager_model');
            $data['request'] = $this->projectmanager_model->get_requests($id);
        }
        $data['pid'] = $id;
        $this->load->view('admin/engagement-request', $data);
    }

    public function edit_gst($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        $data['msg'] = "";
        if (isset($_POST['gid'])) {
            $data['msg'] = $this->admin_model->update_gst();
        }
        $data['gst'] = $this->admin_model->get_single_gst($id);
        $this->load->view('admin/edit-client-gst', $data);
    }

    public function client_invoice() {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        $data['client'] = $this->admin_model->get_basic_client();
        $data['company'] = $this->admin_model->get_basic_company();
        $data['invoice'] = $this->admin_model->get_allclient_invoice();
        $data['count'] = $this->admin_model->getCInvoiceCount();
        $this->load->view('admin/client-invoice', $data);
    }

    public function export_client_invoice() {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        $data['invoice'] = $this->admin_model->get_cinvoicefor_export();
        $filename = "ClientInvoice" . time() . ".xls";
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $this->load->view('admin/export-client-invoice', $data);
    }

    public function export_all_invoice() {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        $data['client'] = $this->admin_model->get_basic_client();
        
        
        if (isset($_POST['pclient'])) {
            $data['invoice'] = $this->admin_model->get_cinvoicefor_export();
//            print_r($data['invoice']);
//        return;
            $filename = "ClientInvoice" . time() . ".xls";
            header("Content-Type: application/xls");
            header("Content-Disposition: attachment; filename=\"$filename\"");
           $this->load->view('admin/export-client-invoice', $data);
            return;
        } else {
            $this->load->view('admin/export-all-invoice', $data);
        }
    }

    public function creat_client_invoice() {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        if (isset($_POST['dproject'])) {
            $msg = $this->admin_model->creat_dInvoice();
            if ($msg != 0) {
                redirect('admin/client_invoice_details/' . $msg);
            }
        }
        if (isset($_POST['project'])) {
            $msg = $this->admin_model->creat_pInvoice();
            if ($msg != 0) {
                redirect('admin/client_invoice_details/' . $msg);
            }
        }
        if (isset($_POST['tdate'])) {
            $msg = $this->admin_model->creat_mInvoice();
            if ($msg != 0) {
                redirect('admin/client_invoice_details/' . $msg);
            }
        }
        $data['client'] = $this->admin_model->get_single_client($_POST['client']);
        $this->load->view('admin/creat-client-invoice', $data);
    }

    public function client_invoice_details($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        if (isset($_POST['amt'])) {
            $this->admin_model->save_client_payment();
        }
        if (isset($_POST['reason'])) {
            $this->admin_model->save_edit_request();
        }
        if (isset($_POST['upCid'])) {
            $this->admin_model->save_invoice_copy();
        }
        $data['invoice'] = $this->admin_model->get_client_invoice($id);
        if (!empty($data['invoice'])) {
            $data['payment'] = $this->admin_model->get_client_payment($data['invoice'][0]->ci_id, '0');
            $data['erequest'] = $this->admin_model->get_allEdit_request($data['invoice'][0]->ci_number);
            $data['client'] = $this->admin_model->get_single_client($data['invoice'][0]->ci_client);
            if ($data['invoice'][0]->ci_type == 'Program') {
                $data['project'] = $this->admin_model->get_basic_project($data['invoice'][0]->ci_project);
                $data['exp'] = $this->admin_model->get_project_expenseForInvoie($data['invoice'][0]->ci_project);
                $this->load->view('admin/client-invoice-details', $data);
            } else {
                $data['pr'] = $this->admin_model->get_month_project($data['invoice'][0]->ci_client, $data['invoice'][0]->ci_year, $data['invoice'][0]->ci_month);
                $this->load->view('admin/client-minvoice-details', $data);
            }
        }
    }

    public function cinvoice($id, $eid) {
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        $data['erequest'] = $this->admin_model->get_edit_request($eid);
        $data['invoice'] = $this->admin_model->get_client_invoice($id);
        if (!empty($data['invoice'])) {
            $data['payment'] = $this->admin_model->get_client_payment($data['invoice'][0]->ci_id, '0');
            $data['client'] = $this->admin_model->get_single_client($data['invoice'][0]->ci_client);
            if ($data['invoice'][0]->ci_type == 'Program') {
                $data['project'] = $this->admin_model->get_basic_project($data['invoice'][0]->ci_project);
                $data['exp'] = $this->admin_model->get_project_expenseForInvoie($data['invoice'][0]->ci_project);
                $this->load->view('admin/cinvoice', $data);
            } else {
                $data['pr'] = $this->admin_model->get_month_project($data['invoice'][0]->ci_client, $data['invoice'][0]->ci_year, $data['invoice'][0]->ci_month);
                $this->load->view('admin/client-minvoice-details', $data);
            }
        }
    }

    public function expense($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        $data['msg'] = "";
        if (isset($_POST['gstAddress'])) {
            $data['msg'] = $this->admin_model->update_client_invoice();
        }
        if (isset($_POST['cname'])) {
            $data['msg'] = $this->admin_model->save_courier();
        }
        $data['invoice'] = $this->admin_model->get_client_invoice($id);
        if (!empty($data['invoice'])) {
            $data['exp'] = $this->admin_model->get_program_expense($data['invoice'][0]->ci_project);
            $data['fees'] = $this->admin_model->get_trainer_fees($data['invoice'][0]->ci_project);
            $data['pid'] = $data['invoice'][0]->ci_project;
            $this->load->view('admin/program-expense', $data);
        }
    }

    public function drona_invoice() {
        $this->check_sess();
        $data['page_url'] = 'Drona Invoice';
        $this->load->model('admin_model');
        if (isset($_POST['company'])) {
            $id = $this->admin_model->save_drona_invoice();
            redirect('admin/drona_invoice_details/' . $id);
        }
        $data['client'] = $this->admin_model->get_basic_client();
        $data['company'] = $this->admin_model->get_basic_company();
        $data['invoice'] = $this->admin_model->get_drona_invoice();
        $data['count'] = $this->admin_model->getDronaCount();
        $this->load->view('admin/drona-invoice-list', $data);
    }
    
    public function export_drona_invoice(){
        $this->check_sess();
        $data['page_url'] = 'Drona Invoice';
        $this->load->model('admin_model');
        $query = $this->db->query("SELECT d.*,c.client_name FROM drona_invoice d INNER JOIN clients c ON d.di_client=c.client_id WHERE d.di_status='1' ORDER BY d.di_id DESC");
        $data['invoice'] = $query->result();
        $filename = "DronaInvoice" . time() . ".xls";
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $this->load->view('admin/export-drona-invoice', $data);
    }

    public function drona_invoice_details($id) {
        $this->check_sess();
        $data['page_url'] = 'Drona Invoice';
        $this->load->model('admin_model');
        if (isset($_POST['upCid'])) {
            $this->admin_model->save_dinvoice_copy();
        }
        if (isset($_POST['amt'])) {
            $this->admin_model->save_client_payment();
        }
        if (isset($_POST['reason'])) {
            $this->admin_model->save_edit_request();
        }
        if (isset($_POST['drona'])) {
            $this->admin_model->update_drona_invoice();
        }
        if (isset($_POST['in_number'])) {
            $this->admin_model->save_drona_expense();
        }
        $data['client'] = $this->admin_model->get_basic_client();
        $data['company'] = $this->admin_model->get_basic_company();

        $data['invoice'] = $this->admin_model->get_singleDrona_invoice($id);
        if (!empty($data['invoice'])) {
            $data['payment'] = $this->admin_model->get_client_payment($data['invoice'][0]->di_id, '3');
            $data['cm'] = $this->admin_model->get_singleCompany($data['invoice'][0]->di_company);
            $data['exp'] = $this->admin_model->get_drona_expense($data['invoice'][0]->di_id);
            $data['location'] = $this->admin_model->get_client_gstLocation($data['invoice'][0]->di_client);
            if ($data['invoice'][0]->di_onlyDebit == '0') {
                $data['erequest'] = $this->admin_model->get_allEdit_request($data['invoice'][0]->di_number);
            } else {
                $data['erequest'] = $this->admin_model->get_allEdit_request($data['invoice'][0]->di_debitNumber);
            }
        }
        $this->load->view('admin/drona-invoice-details', $data);
    }

    public function dinvoice($id, $eid) {
        $data['page_url'] = 'Drona Invoice';
        $this->load->model('admin_model');

        $data['invoice'] = $this->admin_model->get_singleDrona_invoice($id);
        if (!empty($data['invoice'])) {
            $data['cm'] = $this->admin_model->get_singleCompany($data['invoice'][0]->di_company);
            $data['exp'] = $this->admin_model->get_drona_expense($data['invoice'][0]->di_id);
            $data['location'] = $this->admin_model->get_client_gstLocation($data['invoice'][0]->di_client);
            if ($data['invoice'][0]->di_onlyDebit == '0') {
                $data['erequest'] = $this->admin_model->get_allEdit_request($data['invoice'][0]->di_number);
            } else {
                $data['erequest'] = $this->admin_model->get_allEdit_request($data['invoice'][0]->di_debitNumber);
            }
        }
        $this->load->view('admin/dinvoice', $data);
    }
    
    public function edit_user() {
        $this->load->model('admin_model');
        $this->admin_model->updateUserProfile();
        redirect($_POST['callback']);
    }

    public function ajax_page() {
        $this->load->model('admin_model');
        echo $this->admin_model->$_POST['page']($_POST);
    }

}
