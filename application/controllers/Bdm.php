<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class bdm extends CI_Controller {

    public function index() {
        $this->check_sess();
        $data['page_url'] = 'Dashboard';
        $this->load->model('business_model');
        $data['client'] = $this->business_model->get_bdmBasic_client($this->session->userdata('t_code'));
        $this->load->view('bdm/index', $data);
    }

    public function page_map($pr, $sc) {
        $this->$pr($sc);
    }

    public function check_sess() {
        if (!$this->session->userdata('bdm')) {
            redirect('admin/login');
        }
    }

    public function profile() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['msg'] = 'new';
        $data['page_url'] = 'Trainers';
        if(isset($_POST['date_user']))
        {
            $data['msg'] = $this->admin_model->save_trainer_calendar();
        }
        $data['bdm'] = $this->admin_model->get_single_trainer($this->session->userdata('t_code'));
        $data['calendar'] = $this->admin_model->get_trainer_calendor($this->session->userdata('t_code'));
        $this->load->view('bdm/profile', $data);
    }
    
    public function client_programs($id=NULL) {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 0;
        $data['cid']=$id;
        $bdm = $this->session->userdata('t_code');
        $data['program'] = $this->admin_model->get_client_upPrograms($id,$bdm);
        $data['count'] = $this->admin_model->getClientUpCount();
        $this->load->view('bdm/client-upcoming-program', $data);
    }
    
    public function past_programs() {
        $this->check_sess();
        $this->load->model('business_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 0;
        $id = $this->session->userdata('t_code');
        $data['client'] = $this->business_model->get_bdmBasic_client($id);
        $data['pm'] = $this->business_model->get_basicpm();
        $data['program'] = $this->business_model->get_bdmPastPrograms($id);
        //print_r($data['program']);
        $data['count'] = $this->business_model->getBDMPastCount();
        $this->load->view('bdm/past-program', $data);
    }
    
    public function export_program() {
        $this->check_sess();
        $bdm = $this->session->userdata('t_code');
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';

        $filter = "";


        if ($_POST['year'] != 'All') {
            $filter .= " AND YEAR(p.training_start_date)='" . $_POST['year'] . "' ";
        }

        if ($_POST['month'] != 'All') {
            $filter .= " AND MONTH(p.training_start_date)='" . $_POST['month'] . "' ";
        }

        if ($_POST['client'] != 'All') {
            $filter .= " AND p.client_id='" . $_POST['client'] . "' ";
        }

        $today = date('Y-m-d');
        $query = $this->db->query("SELECT p.client_id,p.project_id,p.project_title,p.client_charges,p.client_payment_receieved,p.client_debit_received,p.location_of_training,p.training_start_date,p.training_end_date,p.training_duration,p.venue,p.stay_arrangement,p.travel_arrangement,p.program_state,p.zone,(SELECT client_name FROM clients WHERE client_id=p.client_id ) AS client_name, (SELECT invoice_type FROM clients WHERE client_id=p.client_id ) AS invoice_type,(SELECT cm.company_name FROM clients c INNER JOIN companies cm ON c.company_id=cm.company_id  WHERE client_id=p.client_id ) AS company_name,(SELECT SUM(expense_amount) FROM program_stay WHERE project_id=p.project_id) AS stayAmount,(SELECT SUM(expense_amount) FROM program_travel WHERE project_id=p.project_id) AS travelAmount,(SELECT SUM(debit_amt) FROM debit_note WHERE project=p.project_id AND debit_status='1') AS trainer_debit,u.name AS pname,t.name,t.city,t.trainer_credit,pt.amount,pt.trainer_date_to,pt.trainer_invoice_flag,pt.trainer_paid_status,pt.trainer_invoice_action,pt.invoice_print FROM training_projects p INNER JOIN application_users u ON p.user_code=u.user_code, program_trainers pt INNER JOIN application_users t ON t.user_code=pt.trainer_id,client_projectmanagers cp WHERE cp.bm_id='".$bdm."' AND p.client_id=cp.client_id AND p.training_start_date<'" . $today . "' AND p.training_start_date>='2019-07-1' $filter AND  p.is_active='1' AND pt.project_id=p.project_id ORDER BY p.training_start_date DESC");

        $data['programs'] = $query->result();
        $filename = "Programs" . time() . ".xls";
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $this->load->view('admin/export-program', $data);
    }
    
    public function program_details($id) {
        $this->check_sess();
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 'new';
        $data['program'] = $this->projectmanager_model->get_single_program($id);
        $this->load->view('bdm/program-details', $data);
    }
    
    public function program_expenses($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';

        if (!empty($id)) {
            $this->load->model('projectmanager_model');
            $data['exp'] = $this->projectmanager_model->get_program_expense($id);
            $data['debit'] = $this->projectmanager_model->get_debit_note($id);
            $data['fees'] = $this->projectmanager_model->get_project_fees($id);
        }
        $data['pid'] = $id;
        $this->load->view('bdm/program_expenses', $data);
    }
    
    public function client_invoice() {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('business_model');
        $data['client'] = $this->business_model->get_bdmBasic_client($this->session->userdata('t_code'));
        $data['company'] = $this->business_model->get_basic_company();
        $data['invoice'] = $this->business_model->get_bdmClient_invoice($this->session->userdata('t_code'));
        $data['count'] = $this->business_model->getBDMCICount();
        $this->load->view('bdm/client-invoice', $data);
    }
    
    public function client_invoice_details($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        $data['invoice'] = $this->admin_model->get_client_invoice($id);
        if (!empty($data['invoice'])) {
            $data['payment'] = $this->admin_model->get_client_payment($data['invoice'][0]->ci_id, '0');
            $data['client'] = $this->admin_model->get_single_client($data['invoice'][0]->ci_client);
            if ($data['invoice'][0]->ci_type == 'Program') {
                $data['project'] = $this->admin_model->get_basic_project($data['invoice'][0]->ci_project);
                $data['exp'] = $this->admin_model->get_project_expenseForInvoie($data['invoice'][0]->ci_project);
                $this->load->view('bdm/client-invoice-details', $data);
            } else {
                $data['pr'] = $this->admin_model->get_month_project($data['invoice'][0]->ci_client, $data['invoice'][0]->ci_year, $data['invoice'][0]->ci_month);
                $this->load->view('bdm/client-minvoice-details', $data);
            }
        }
    }
    
    public function export_all_invoice() {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        $data['client'] = $this->admin_model->get_basic_client();
        
        
        if (isset($_POST['pclient'])) {
            $data['invoice'] = $this->admin_model->get_cinvoicefor_export();
//            print_r($data['invoice']);
//        return;
            $filename = "ClientInvoice" . time() . ".xls";
            header("Content-Type: application/xls");
            header("Content-Disposition: attachment; filename=\"$filename\"");
            $this->load->view('admin/export-client-invoice', $data);
            return;
        } else {
            $this->load->view('admin/export-all-invoice', $data);
        }
    }
    
    public function trainer_invoices() {
        $this->check_sess();
        $this->load->model('business_model');
        $data['page_url'] = 'Payments';
        $data['msg'] = 'new';
        $data['payment'] = $this->business_model->get_bdmTrainerInvoice($this->session->userdata('t_code'));
        $data['count'] = $this->business_model->getBDMTICount();
        $this->load->view('bdm/invoice-list', $data);
    }
    
    public function trainer_inovice_details($tid, $pid) {
        $this->check_sess();

        $data['page_url'] = 'Payment';
        $data['msg'] = 'new';
        if (isset($_POST['pid'])) {
            $this->load->model('admin_model');
            $this->admin_model->update_payment();
        }
        $data['tid'] = $tid;
        $data['pid'] = $pid;
        $this->load->model('trainer_model');
        $data['debit'] = $this->trainer_model->get_invoice_details($tid, $pid);
        $data['fees'] = $this->trainer_model->get_trainer_fees($tid, $pid);
        $data['pDetails'] = $this->trainer_model->get_client_details($pid);
        $data['tDetails'] = $this->trainer_model->get_trainer_details($tid);
        $data['bank'] = $this->trainer_model->get_trainer_bank($tid);
        $data['payment'] = $this->trainer_model->get_trainer_payment($data['fees'][0]->id);
        $this->load->view('bdm/trainer-invoice', $data);
    }
    
    public function trainer_inovice_print($tid, $pid) {
        $this->check_sess();

        $data['page_url'] = 'Payment';
        $data['msg'] = 'new';
        $this->load->model('trainer_model');
        $data['debit'] = $this->trainer_model->get_invoice_details($tid, $pid);
        $data['fees'] = $this->trainer_model->get_trainer_fees($tid, $pid);
        $data['pDetails'] = $this->trainer_model->get_client_details($pid);
        $data['tDetails'] = $this->trainer_model->get_trainer_details($tid);
        $data['bank'] = $this->trainer_model->get_trainer_bank($tid);
        $data['payment'] = $this->trainer_model->get_trainer_payment($data['fees'][0]->id);
        $this->load->view('admin/trainer-invoice-print', $data);
    }
    
    
    public function export_trainer_invoice() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Payments';
        $data['msg'] = 'new';
        $id = $this->session->userdata('t_code');
        $query = $this->db->query("SELECT pt.*,p.project_id,p.project_title,p.training_duration,u.user_code,u.name FROM training_projects p,program_trainers pt INNER JOIN application_users u ON  pt.trainer_id=u.user_code,client_projectmanagers cp WHERE cp.bm_id='".$id."' AND p.client_id=cp.client_id AND pt.project_id=p.project_id AND pt.trainer_invoice_flag='1' AND pt.trainer_invoice_action='1' ORDER BY p.training_start_date DESC");
        $data['payment'] = $query->result();
        $filename = "TrainerInvoice" . time() . ".xls";
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $this->load->view('admin/export-trainer-invoice', $data);
    }
    
    public function pending_content($id){
        $this->check_sess();
        $this->load->model('business_model');
        $data['page_url'] = 'Payments';
        $data['msg'] = 'new';
        $data['content'] = $this->business_model->getBDMContent($id);
        $this->load->view('bdm/pending-content', $data);
    }

    

    public function logout() {
        $this->session->unset_userdata('trainer', '');
        $this->session->unset_userdata('t_name', '');
        $this->session->unset_userdata('t_id', '');
        $this->session->unset_userdata('t_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function ajax_page() {
        $this->load->model('trainer_model');
        echo $this->trainer_model->$_POST['page']($_POST);
    }

}
