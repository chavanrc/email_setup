<?php

defined('BASEPATH') OR exit('No direct script access allowed');
date_default_timezone_set('Asia/Kolkata');

class Webservices extends CI_Controller {

    public $cdate = null;
    public $full_date = null;

    function __construct() {
        parent::__construct();
        $this->output->set_header('Access-Control-Allow-Origin: *');
        $datestring = "%Y-%m-%d";
        $datestring1 = "%Y-%m-%d %h:%i:%s";
        $time = time();
        $this->cdate = mdate($datestring, $time);
        //$this->full_date = mdate($datestring1, $time);
        $this->full_date = date('Y-m-d H:i:s');
    }

    public function index() {
        //$this->load->model('home_model');
        //$this->load->view('index');
        echo $this->full_date;
    }

    public function page_map($pr, $sc = NULL) {
        $this->$pr($sc);
    }

    public function about() {
        $this->load->view('about');
    }

    public function trainer_login() {

        if (isset($_POST['username'])) {
            $data[] = array();
            $mobile = $_POST['username'];
            $pass = $_POST['password'];

            //$mobile = '9886783411';
            //$pass = '1234';

            $this->db->where('contact_number', $mobile);
            $this->db->where('app_password', $pass);

            $query = $this->db->get('application_users');
            if ($query->num_rows()) {
                $result = $query->result();
                $tid = $result[0]->user_code;
                $name = $result[0]->name;
                $email = $result[0]->email;
                $usertype = $result[0]->user_type;
                $projectId = "";
                $projectName = "";
                $checkinTime = "";
                $checkinStatus = "";
                $checkoutTime = "";
                $checkoutStatus = "";
                $city = "";
                $preTest = "0";
                $postTest = "0";
                $aid = "";

                $today = date('Y-m-d');

                $query1 = $this->db->query("SELECT p.project_id, p.project_title, p.location_of_training, t.assignment_id, t.trainer_checkin_status,t.trainer_checkin_time,t.trainer_checkout_time,t.trainer_checkout_status, t.pre_test_trigger, t.post_test_trigger FROM trainer_assigned t, training_projects p WHERE t.trainer_id='" . $tid . "' AND t.program_date='" . $today . "' AND p.project_id=t.project_id AND p.is_active='1'");


                $result1 = $query1->result();
                if (!empty($result1)) {
                    $aid = $result1[0]->assignment_id;
                    $projectId = $result1[0]->project_id;
                    $projectName = $result1[0]->project_title;
                    $city = $result1[0]->location_of_training;
                    if ($result1[0]->trainer_checkin_status == '1') {
                        $checkinTime = date_formate($result1[0]->trainer_checkin_time);
                    }
                    $checkinStatus = $result1[0]->trainer_checkin_status;
                    $checkoutTime = $result1[0]->trainer_checkout_time;
                    $checkoutStatus = $result1[0]->trainer_checkout_status;
                    $preTest = $result1[0]->pre_test_trigger;
                    $postTest = $result1[0]->post_test_trigger;
                }

                $data[0] = array('status' => '1', 'aid'=>$aid, 'trainerId' => $tid, 'trainerName' => $name, 'trainerEmail' => $email, 'projectId' => $projectId, 'projectName' => $projectName, 'city' => $city, 'usertype' => $usertype, 'checkinTime' => $checkinTime, 'checkinStatus' => $checkinStatus, 'checkoutTime' => $checkoutTime, 'checkoutStatus' => $checkoutStatus, 'preTest' => $preTest, 'postTest' => $postTest);
            } else {
                $data[0] = array('status' => '0');
            }

            echo json_encode($data);
        }
    }

    public function getProjects() {

        $today = date('Y-m-d');

        $query1 = $this->db->query("SELECT p.project_id, p.project_title, t.trainer_id FROM trainer_assigned t, training_projects p WHERE  t.program_date='" . $today . "' AND p.project_id=t.project_id AND p.client_id='4'");
        $result = $query1->result();
        if (!empty($result)) {

            $no = 0;
            foreach ($result as $rs_data) {
                $data[$no] = array('pId' => $rs_data->project_id,
                    'pName' => $rs_data->project_title);
                $no++;
            }

            echo json_encode($data);
        } else {
            echo '0';
        }
    }

    public function trainer_checkin() {

        if (isset($_POST['trainerId'])) {
            $tId = $_POST['trainerId'];
            $pId = $_POST['projectId'];
            $lat = $_POST['lat'];
            $lng = $_POST['lng'];

            $query = $this->db->query("SELECT c.trainer_checkin_status,u.name FROM trainer_assigned c INNER JOIN application_users u ON c.trainer_id=u.user_code WHERE c.trainer_id='" . $tId . "' AND c.project_id='" . $pId . "'");
            if ($query->num_rows()) {

                $result = $query->result();
                $tName = $result[0]->name;

                $query1 = $this->db->query("SELECT p.project_title,p.location_of_training,u.contact_number FROM training_projects p, application_users u WHERE p.project_id='" . $pId . "' AND u.user_code=p.user_code");
                $result1 = $query1->result();

                $pName = $result1[0]->project_title;
                $pCity = $result1[0]->location_of_training;
                $pm = $result1[0]->contact_number;

                $data = array('trainer_lat' => $lat,
                    'trainer_long' => $lng,
                    'trainer_checkin_status' => '1',
                    'trainer_checkin_time' => $this->full_date);

                $this->db->where('assignment_id', $_POST['aid']);
                $this->db->update('trainer_assigned', $data);

                $msg = $tName . " has started the program " . $pName . " at " . $pCity . " on " . date('H:i');
                send_sms($pm, $msg);
            }

            echo date_formate($this->full_date);
        } else {
            echo 0;
        }
    }

    public function trainer_checkout() {

        if (isset($_POST['trainerId'])) {
            $tId = $_POST['trainerId'];
            $pId = $_POST['projectId'];

            $query = $this->db->query("SELECT trainer_checkout_status FROM trainer_assigned WHERE trainer_id='" . $tId . "' AND project_id='" . $pId . "' AND trainer_checkout_status='1'");
            if (!$query->num_rows()) {
                $data = array(
                    'trainer_checkout_status' => '1',
                    'trainer_checkout_time' => $this->full_date);

                $this->db->where('assignment_id', $_POST['aid']);
                $this->db->update('trainer_assigned', $data);
            }

            echo 1;
        } else {
            echo 0;
        }
    }

    public function participants_details() {
        if (isset($_POST['uid'])) {
            $uid = $_POST['uid'];
            $pid = $_POST['pid'];
            $data[] = array();
            $preAttd = 0;
            $postAttd = 0;
            $feedback = 0;
            $query = $this->db->query("SELECT p.*, a.pre_test_trigger, a.post_test_trigger FROM participants p, trainer_assigned a  WHERE p.p_id='" . $uid . "' AND p.project_id='" . $pid . "' AND a.project_id=p.project_id ");
            $result = $query->result();

            $query1 = $this->db->query("SELECT project_id FROM assessment_answers WHERE project_id='" . $pid . "' AND participant_id='" . $uid . "' AND question_type='pre'");
            $result1 = $query1->result();
            if (!empty($result1)) {
                $preAttd = 1;
            }

            $query2 = $this->db->query("SELECT project_id FROM assessment_answers WHERE project_id='" . $pid . "' AND participant_id='" . $uid . "' AND question_type='post'");
            $result2 = $query2->result();
            if (!empty($result2)) {
                $postAttd = 1;
            }

            $query3 = $this->db->query("SELECT id FROM feedback_answers WHERE project_id='" . $pid . "' AND p_id='" . $uid . "'");
            $result3 = $query3->result();
            if (!empty($result3)) {
                $feedback = 1;
            }

            if (!empty($result)) {
                $data[0] = array('preScore' => $result[0]->pre_score,
                    'postScore' => $result[0]->post_score,
                    'preTrigger' => $result[0]->pre_test_trigger,
                    'postTrigger' => $result[0]->post_test_trigger,
                    'vpa' => $result[0]->vpa_address,
                    'preAttd' => $preAttd,
                    'postAttd' => $postAttd,
                    'feedback' => $feedback);

                echo json_encode($data);
            } else {
                echo '0';
            }
        } else {
            echo '0';
        }
    }

    public function getProjectImg() {
        $query = $this->db->query("SELECT ti_img FROM trainer_img WHERE project_id='" . $_POST['pid'] . "'");
        $result = $query->result();
        if (!empty($result)) {
            echo json_encode($result);
        } else {
            echo '0';
        }
    }

    public function uploadImg() {
        if (!empty($_POST['img'])) {
            $img = time() . '.jpg';
            $path = './App/assets/upload/' . $img;
            file_put_contents($path, base64_decode($_POST['img']));

            $data = array('project_id' => $_POST['pid'],
                'trainer_id' => $_POST['tid'],
                'ti_img' => $img,
                'ti_date' => $this->full_date);

            $this->db->insert('trainer_img', $data);

            $SourceFile = $path . $img;
            $WaterMarkText = date('Y-m-d H:s a');
            $DestinationFile = $path . $img;
            list($width, $height) = getimagesize($SourceFile);
            $image_p = imagecreatetruecolor($width, $height);
            $image = imagecreatefromjpeg($SourceFile);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);
            $black = imagecolorallocate($image_p, 255, 193, 7);
            $font = './assets/fonts/Heebo-Bold.ttf';
            $font_size = 60;
            imagettftext($image_p, $font_size, 0, 50, 100, $black, $font, $WaterMarkText);
            if ($DestinationFile <> '') {
                imagejpeg($image_p, $DestinationFile, 50);
            } else {
                header('Content-Type: image/jpeg');
                imagejpeg($image_p, null, 50);
            };
            imagedestroy($image);
            imagedestroy($image_p);
        }
    }

    public function uploadForm() {
        if (!empty($_POST['img'])) {
            $img = time() . '.jpg';
            $path = './assets/upload/form/' . $img;
            file_put_contents($path, base64_decode($_POST['img']));

            $data = array(
                'form_file_name' => $img,
                'project_id' => $_POST['pid'],
                'form_type' => 'Atendance Form',
                'program_date' => date('Y-m-d'),
                'comments' => '',
                'upload_by' => $_POST['tid'],
                'upload_date' => date('Y-m-d'),
                'upload_type'=>'1');

            $this->db->insert('program_forms', $data);

            $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training,c.client_name,u.email,u.user_code FROM training_projects p INNER JOIN application_users u ON p.user_code=u.user_code, clients c  WHERE p.project_id='" . $_POST['pid'] . "' AND c.client_id=p.client_id");
            $project = $query->result();



            $email = $project[0]->email;
            $pmCode = $project[0]->user_code;

            $tsQuery = $this->db->query("SELECT user_code FROM application_users WHERE user_type='training support'");
            $tsResult = $tsQuery->result();

            $tsCode = $tsResult[0]->user_code;

            $data = array('project' => $project[0]->project_title." : ".$project[0]->location_of_training,
                'start_date' => $project[0]->training_start_date,
                'company' => $project[0]->client_name,
                'type' => 'Attendance Sheet',
                'link' => $img);

            $note = array('user' => $pmCode,
                'project' => $project[0]->project_id,
                'task' => 0,
                'type' => 'Attendance Sheet',
                'due_date' => '',
                'text' => "Trainer has uploaded Attendance sheet for the program : " . $project[0]->project_title." : ".$project[0]->location_of_training." for client ".$project[0]->client_name);
            $this->push_notification($note);

            $note = array('user' => $tsCode,
                'project' => $project[0]->project_id,
                'task' => 0,
                'type' => 'Attendance Sheet',
                'due_date' => '',
                'text' => "Trainer has uploaded Attendance sheet for the program : " . $project[0]->project_title." : ".$project[0]->location_of_training." for client ".$project[0]->client_name);
            $this->push_notification($note);
            
            form_attach_mail($email, $data);
        }
    }

    public function push_notification($val) {
        $data = array('user_id' => $val['user'],
            'project_id' => $val['project'],
            'notification_text' => $val['text'],
            'todo_task' => $val['task'],
            'notification_type' => $val['type'],
            'due_date' => $val['due_date'],
            'notification_date' => $this->full_date);

        $this->db->insert('notifications', $data);
    }

    public function addVPA() {
        $data = array('vpa_address' => $_POST['vpa'], 'vpa_no' => 0);
        $this->db->where('p_id', $_POST['pid']);
        $this->db->update('participants', $data);
    }

    public function addNoVPA() {
        $data = array('vpa_no' => 1, 'no_vpa_reason' => $_POST['reason']);
        $this->db->where('p_id', $_POST['pid']);
        $this->db->update('participants', $data);
    }

    public function participants_signup() {

        $today = date('Y-m-d H:i:s');

        $check = $this->db->query("SELECT phone_number FROM participants WHERE phone_number='" . $_POST['mobile'] . "' AND project_id='" . $_POST['project'] . "'");

        if (!$check->num_rows()) {
            $data = array('attendance_flag' => '1',
                'name' => $_POST['name'],
                'phone_number' => $_POST['mobile'],
                'organization' => $_POST['org'],
                'location' => $_POST['city'],
                'project_id' => $_POST['project'],
                'email' => $_POST['email'],
                'login_time' => $today,
                'vpa_address' => $_POST['vpa']);

            $this->db->insert('participants', $data);
            $pid = $this->db->insert_id();

            $query = $this->db->query("SELECT project_title FROM training_projects WHERE project_id='" . $_POST['project'] . "'");
            $result = $query->result();
            $pName = $result[0]->project_title;

            $query1 = $this->db->query("SELECT u.name FROM trainer_assigned t, application_users u WHERE t.project_id='" . $_POST['project'] . "' AND u.user_code=t.trainer_id");
            $result1 = $query1->result();
            $tName = $result1[0]->name;

            $data1[0] = array('pid' => $pid, 'pName' => $pName, 'tName' => $tName);

            echo json_encode($data1);
        } else {
            echo '0';
        }
    }

    public function preTest_triger() {
        if (isset($_POST['trainerId'])) {
            $tId = $_POST['trainerId'];
            $pId = $_POST['projectId'];

            $data = array('pre_test_trigger' => 1,
                'pre_test_trigger_time' => $this->full_date);

            $this->db->where('trainer_id', $tId);
            $this->db->where('project_id', $pId);
            $this->db->update('trainer_assigned', $data);
            echo 1;
        } else {
            echo 0;
        }
    }

    public function postTest_triger() {
        if (isset($_POST['trainerId'])) {
            $tId = $_POST['trainerId'];
            $pId = $_POST['projectId'];

            $data = array('post_test_trigger' => 1,
                'post_test_trigger_time' => $this->full_date);

            $this->db->where('trainer_id', $tId);
            $this->db->where('project_id', $pId);
            $this->db->update('trainer_assigned', $data);
            echo 1;
        } else {
            echo 0;
        }
    }

    public function preTest_questions() {
        $data[] = array();
        if (isset($_POST['projectId'])) {
            $pId = $_POST['projectId'];
            $check = $this->db->query("SELECT pre_test_trigger FROM trainer_assigned WHERE project_id='" . $pId . "' AND pre_test_trigger='1'");
            if ($check->num_rows()) {
                $query = $this->db->query("SELECT participants_type FROM training_projects WHERE project_id='" . $pId . "'");
                $result = $query->result();
                if (!empty($result)) {
                    $query2 = $this->db->query("SELECT * FROM assessment_questions WHERE participant_type='" . $result[0]->participants_type . "' AND assessment_type='pre' AND question_status='0' ORDER BY question_id");

                    $question = $query2->result();
                    if (!empty($question)) {
                        $no = 0;
                        foreach ($question as $qn_data) {

                            $data1[$no] = array(
                                'questionId' => $qn_data->question_id,
                                'question' => $qn_data->question_text,
                                'option_1' => $qn_data->answer_1,
                                'option_2' => $qn_data->answer_2,
                                'option_3' => $qn_data->answer_3,
                                'option_4' => $qn_data->answer_4,
                                'option_5' => $qn_data->answer_5,
                                'status' => $qn_data->question_status,
                                'sup' => $qn_data->question_support);
                            $no++;

                            if ($qn_data->question_support == '1') {
                                $query3 = $this->db->query("SELECT * FROM assessment_questions WHERE  assessment_type='pre' AND question_status='1' ORDER BY question_id");
                                $result3 = $query3->result();
                                if (!empty($result3)) {
                                    foreach ($result3 as $q_data) {
                                        $data1[$no] = array(
                                            'questionId' => $q_data->question_id,
                                            'question' => $q_data->question_text,
                                            'option_1' => $q_data->answer_1,
                                            'option_2' => $q_data->answer_2,
                                            'option_3' => $q_data->answer_3,
                                            'option_4' => $q_data->answer_4,
                                            'option_5' => $q_data->answer_5,
                                            'status' => $q_data->question_status,
                                            'sup' => $q_data->question_support);
                                        $no++;
                                    }
                                }
                            }
                        }
                        $data[0] = array('status' => 1, 'qBody' => $data1);
                    } else {
                        $data[0] = array('status' => 0);
                    }
                } else {
                    $data[0] = array('status' => 0);
                }
            } else {
                $data[0] = array('status' => 0);
            }
        } else {
            $data[0] = array('status' => 0);
        }

        echo json_encode($data);
    }

    public function postTest_questions() {
        $data[] = array();
        if (isset($_POST['projectId'])) {
            $pId = $_POST['projectId'];
            $check = $this->db->query("SELECT pre_test_trigger FROM trainer_assigned WHERE project_id='" . $pId . "' AND post_test_trigger='1'");
            if ($check->num_rows()) {
                $query = $this->db->query("SELECT participants_type FROM training_projects WHERE project_id='" . $pId . "'");
                $result = $query->result();
                if (!empty($result)) {
                    $query2 = $this->db->query("SELECT * FROM assessment_questions WHERE participant_type='" . $result[0]->participants_type . "' AND assessment_type='post' ORDER BY question_id");
                    $question = $query2->result();
                    if (!empty($question)) {
                        $no = 0;
                        foreach ($question as $qn_data) {
                            $data1[$no] = array(
                                'questionId' => $qn_data->question_id,
                                'question' => $qn_data->question_text,
                                'option_1' => $qn_data->answer_1,
                                'option_2' => $qn_data->answer_2,
                                'option_3' => $qn_data->answer_3,
                                'option_4' => $qn_data->answer_4,
                                'option_5' => $qn_data->answer_5);
                            $no++;
                        }
                        $data[0] = array('status' => 1, 'qBody' => $data1);
                    } else {
                        $data[0] = array('status' => 0);
                    }
                } else {
                    $data[0] = array('status' => 0);
                }
            } else {
                $data[0] = array('status' => 0);
            }
        } else {
            $data[0] = array('status' => 0);
        }

        echo json_encode($data);
    }

    public function save_pre_test() {
        $pid = $_POST['pid'];
        $uid = $_POST['uid'];
        $data = json_decode($_POST['ans']);
        //$data = (array)$data;
        //print_r($data);
        //return;
        //$this->db->query("INSERT INTO tmsav (td_data) VALUES('".$_POST['ans']."')");
        //return;
        // print_r($data);
        //echo $data->{1};
        //return;
        $query = $this->db->query("SELECT participants_type FROM training_projects WHERE project_id='" . $pid . "'");
        $result = $query->result();
        if (!empty($result)) {

            $query2 = $this->db->query("SELECT * FROM assessment_questions WHERE (participant_type='" . $result[0]->participants_type . "' || participant_type='') AND assessment_type='pre'");
            $question = $query2->result();

            if (!empty($question)) {
                $totalScore = 0;
                foreach ($question as $qn_data) {
                    $qid = $qn_data->question_id;
                    $ans = $qn_data->correct_ans;
                    $rAns = "";
                    $score = 0;
                    if (property_exists($data, $qid)) {
                        $rAns = $data->{$qid};
                        if ($rAns == $ans) {
                            $score = 1;
                        }
                        // }
                        $totalScore +=$score;
                        $data1 = array('project_id' => $pid,
                            'participant_id' => $uid,
                            'question_id' => $qid,
                            'answer' => $rAns,
                            'score' => $score,
                            'question_type' => 'pre');

                        $this->db->insert('assessment_answers', $data1);
                        echo $this->db->last_query();
                    }
                }

                $this->db->query("UPDATE participants SET pre_score='" . $totalScore . "' WHERE p_id='" . $uid . "'");
            }
        }
    }

    public function save_post_test() {
        $pid = $_POST['pid'];
        $uid = $_POST['uid'];
        $data = json_decode($_POST['ans']);
        // print_r($data);
        //echo $data->{1};
        //return;
        $query = $this->db->query("SELECT participants_type FROM training_projects WHERE project_id='" . $pid . "'");
        $result = $query->result();
        if (!empty($result)) {

            $query2 = $this->db->query("SELECT * FROM assessment_questions WHERE participant_type='" . $result[0]->participants_type . "' AND assessment_type='post'");
            $question = $query2->result();
            if (!empty($question)) {
                $totalScore = 0;
                foreach ($question as $qn_data) {
                    $qid = $qn_data->question_id;
                    $ans = $qn_data->correct_ans;
                    $rAns = "";
                    $score = 0;
                    // if(array_key_exists($qid,$data))
                    // {
                    $rAns = $data->{$qid};
                    if ($rAns == $ans) {
                        $score = 1;
                    }
                    // }
                    $totalScore +=$score;
                    $data1 = array('project_id' => $pid,
                        'participant_id' => $uid,
                        'question_id' => $qid,
                        'answer' => $rAns,
                        'score' => $score,
                        'question_type' => 'post');

                    $this->db->insert('assessment_answers', $data1);
                    echo $this->db->last_query();
                }

                $this->db->query("UPDATE participants SET post_score='" . $totalScore . "' WHERE p_id='" . $uid . "'");
            }
        }
    }

    public function deleteAns() {
        $this->db->query("DELETE FROM assessment_answers WHERE project_id='71'");
    }

    public function getAdminProgram() {
        $today = date('Y-m-d');

        $query = $this->db->query("SELECT p.project_id, p.project_title, p.location_of_training, t.program_date, t.trainer_checkin_status,t.pre_test_trigger,t.post_test_trigger,t.trainer_checkout_status,u.name FROM trainer_assigned t INNER JOIN application_users u ON u.user_code=t.trainer_id, training_projects p WHERE t.program_date='" . $today . "' AND  p.project_id=t.project_id AND p.client_id='4' ORDER BY p.project_id DESC");

        $result = $query->result();

        $data = array();
        if (!empty($result)) {
            $no = 0;
            foreach ($result as $rs_data) {
                $data[$no] = array('pId' => $rs_data->project_id,
                    'pTitle' => $rs_data->project_title,
                    'pCity' => $rs_data->location_of_training,
                    'pCheckin' => $rs_data->trainer_checkin_status,
                    'pCheckout' => $rs_data->trainer_checkout_status,
                    'preTrigger' => $rs_data->pre_test_trigger,
                    'postTrigger' => $rs_data->post_test_trigger,
                    'tName' => $rs_data->name,
                    'pDate' => date_formate_short($rs_data->program_date . " 00:00:00"));

                $no++;
            }

            echo json_encode($data);
        } else {
            echo "0";
        }
    }

    public function getWeekProgram() {
        $today = date('Y-m-d');

        $query = $this->db->query("SELECT p.project_id, p.project_title, p.location_of_training, t.program_date, t.trainer_checkin_status,t.pre_test_trigger,t.post_test_trigger,t.trainer_checkout_status,u.name FROM trainer_assigned t INNER JOIN application_users u ON u.user_code=t.trainer_id,  training_projects p WHERE WEEKOFYEAR(t.program_date) = WEEKOFYEAR(NOW()) AND p.project_id=t.project_id AND p.client_id='4' ORDER BY p.project_id DESC");

        $result = $query->result();

        $data = array();
        if (!empty($result)) {
            $no = 0;
            foreach ($result as $rs_data) {
                $data[0]['program'][$no] = array('pId' => $rs_data->project_id,
                    'pTitle' => $rs_data->project_title,
                    'pCity' => $rs_data->location_of_training,
                    'pCheckin' => $rs_data->trainer_checkin_status,
                    'pCheckout' => $rs_data->trainer_checkout_status,
                    'preTrigger' => $rs_data->pre_test_trigger,
                    'postTrigger' => $rs_data->post_test_trigger,
                    'tName' => $rs_data->name,
                    'pDate' => date_formate_short($rs_data->program_date . " 00:00:00"));

                $no++;
            }
            $query1 = $this->db->query("SELECT COUNT(p.p_id) as tP,SUM(p.bhim_download) as tB FROM trainer_assigned t, participants p WHERE WEEKOFYEAR(t.program_date) = WEEKOFYEAR(NOW()) AND p.project_id=t.project_id");
            $result1 = $query1->result();
            $tP = $result1[0]->tP;
            $tB = $result1[0]->tB;

            $query2 = $this->db->query("SELECT COUNT(p.p_id) as tV FROM trainer_assigned t, participants p WHERE WEEKOFYEAR(t.program_date) = WEEKOFYEAR(NOW()) AND p.project_id=t.project_id AND p.vpa_address!='' ");
            $result2 = $query2->result();
            $tV = $result2[0]->tV;

            $data[0]['stat'][0] = array('tp' => $tP, 'tb' => $tB, 'tv' => $tV);
            echo json_encode($data);
        } else {
            echo "0";
        }
    }

    public function getMonthProgram() {
        $today = date('Y-m-d');

        $query = $this->db->query("SELECT p.project_id, p.project_title, p.location_of_training, t.program_date, t.trainer_checkin_status,t.pre_test_trigger,t.post_test_trigger,t.trainer_checkout_status,u.name FROM trainer_assigned t INNER JOIN application_users u ON u.user_code=t.trainer_id, training_projects p WHERE YEAR(t.program_date) = YEAR(CURRENT_DATE()) AND MONTH(t.program_date) = MONTH(CURRENT_DATE()) AND p.project_id=t.project_id AND p.client_id='4' ORDER BY p.project_id DESC");

        $result = $query->result();

        $data = array();
        if (!empty($result)) {
            $no = 0;
            foreach ($result as $rs_data) {
                $data[0]['program'][$no] = array('pId' => $rs_data->project_id,
                    'pTitle' => $rs_data->project_title,
                    'pCity' => $rs_data->location_of_training,
                    'pCheckin' => $rs_data->trainer_checkin_status,
                    'pCheckout' => $rs_data->trainer_checkout_status,
                    'preTrigger' => $rs_data->pre_test_trigger,
                    'postTrigger' => $rs_data->post_test_trigger,
                    'tName' => $rs_data->name,
                    'pDate' => date_formate_short($rs_data->program_date . " 00:00:00"));

                $no++;
            }

            $query1 = $this->db->query("SELECT COUNT(p.p_id) as tP,SUM(p.bhim_download) as tB FROM trainer_assigned t, participants p WHERE YEAR(t.program_date) = YEAR(CURRENT_DATE()) AND MONTH(t.program_date) = MONTH(CURRENT_DATE()) AND p.project_id=t.project_id");
            $result1 = $query1->result();
            $tP = $result1[0]->tP;
            $tB = $result1[0]->tB;

            $query2 = $this->db->query("SELECT COUNT(p.p_id) as tV FROM trainer_assigned t, participants p WHERE YEAR(t.program_date) = YEAR(CURRENT_DATE()) AND MONTH(t.program_date) = MONTH(CURRENT_DATE()) AND p.project_id=t.project_id AND p.vpa_address!='' ");
            $result2 = $query2->result();
            $tV = $result2[0]->tV;

            $data[0]['stat'][0] = array('tp' => $tP, 'tb' => $tB, 'tv' => $tV);

            echo json_encode($data);
        } else {
            echo "0";
        }
    }

    public function getAllProgram() {
        $today = date('Y-m-d');

        $query = $this->db->query("SELECT p.project_id, p.project_title, p.location_of_training, t.program_date, t.trainer_checkin_status,t.pre_test_trigger,t.post_test_trigger,t.trainer_checkout_status,u.name FROM trainer_assigned t INNER JOIN application_users u ON u.user_code=t.trainer_id, training_projects p WHERE p.project_id=t.project_id AND p.client_id='4' ORDER BY p.project_id DESC");

        $result = $query->result();

        $data = array();
        if (!empty($result)) {
            $no = 0;
            foreach ($result as $rs_data) {
                $data[0]['program'][$no] = array('pId' => $rs_data->project_id,
                    'pTitle' => $rs_data->project_title,
                    'pCity' => $rs_data->location_of_training,
                    'pCheckin' => $rs_data->trainer_checkin_status,
                    'pCheckout' => $rs_data->trainer_checkout_status,
                    'preTrigger' => $rs_data->pre_test_trigger,
                    'postTrigger' => $rs_data->post_test_trigger,
                    'tName' => $rs_data->name,
                    'pDate' => date_formate_short($rs_data->program_date . " 00:00:00"));

                $no++;
            }
            $query1 = $this->db->query("SELECT COUNT(p.p_id) as tP,SUM(p.bhim_download) as tB FROM trainer_assigned t, participants p WHERE p.project_id=t.project_id");
            $result1 = $query1->result();
            $tP = $result1[0]->tP;
            $tB = $result1[0]->tB;

            $query2 = $this->db->query("SELECT COUNT(p.p_id) as tV FROM trainer_assigned t, participants p WHERE  p.project_id=t.project_id AND p.vpa_address!='' ");
            $result2 = $query2->result();
            $tV = $result2[0]->tV;

            $bank = 0;
            $gov = 0;
            $cop = 0;
            $ngo = 0;

            $query3 = $this->db->query("SELECT COUNT(p.project_id) as tpr FROM trainer_assigned t, training_projects p WHERE p.project_id=t.project_id AND p.participants_type='Banking'");
            $result3 = $query3->result();
            $bank = $result3[0]->tpr;

            $query4 = $this->db->query("SELECT COUNT(p.project_id) as tpr FROM trainer_assigned t, training_projects p WHERE p.project_id=t.project_id AND p.participants_type='Govt'");
            $result4 = $query4->result();
            $gov = $result4[0]->tpr;


            $query5 = $this->db->query("SELECT COUNT(p.project_id) as tpr FROM trainer_assigned t, training_projects p WHERE p.project_id=t.project_id AND p.participants_type='Corporate'");
            $result5 = $query5->result();
            $cop = $result5[0]->tpr;


            $query6 = $this->db->query("SELECT COUNT(p.project_id) as tpr FROM trainer_assigned t, training_projects p WHERE p.project_id=t.project_id AND p.participants_type='NGO'");
            $result6 = $query6->result();
            $ngo = $result6[0]->tpr;

            $data[0]['stat'][0] = array('tp' => $tP, 'tb' => $tB, 'tv' => $tV, 'bank' => $bank, 'gov' => $gov, 'cop' => $cop, 'ngo' => $ngo);

            echo json_encode($data);
        } else {
            echo "0";
        }
    }

    public function getFeedback() {
        $query = $this->db->query("SELECT * FROM feedback_questions");
        $result = $query->result();
        if (!empty($result)) {
            $data = array();
            $no = 0;
            foreach ($result as $rs_data) {
                $data[$no] = array('qid' => $rs_data->id, 'qTitle' => $rs_data->question_text);
                $no++;
            }
            echo json_encode($data);
        } else {
            echo "0";
        }
    }

    public function saveFeedback() {
        $pid = $_POST['pid'];
        $uid = $_POST['uid'];
        $data = json_decode($_POST['ans']);

        $query = $this->db->query("SELECT * FROM feedback_questions");
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $data1 = array('p_id' => $uid,
                    'project_id' => $pid,
                    'question_id' => $rs_data->id,
                    'answer' => $data->{$rs_data->id});

                $this->db->insert('feedback_answers', $data1);
            }
        }
    }

    public function bhim() {
        $uid = $_POST['uid'];
        $this->db->query("UPDATE participants SET bhim_download ='1' WHERE p_id='" . $uid . "'");
        //echo $this->db->last_query();
    }

    public function getParticipants() {
        $pid = $_POST['pid'];
        $query = $this->db->query("SELECT * FROM participants WHERE project_id='" . $pid . "'");
        $result = $query->result();
        if (!empty($result)) {
            $data = array();
            $no = 0;
            foreach ($result as $rs_data) {

                $preAttd = 0;
                $postAttd = 0;
                $feedback = 0;

                $query1 = $this->db->query("SELECT project_id FROM assessment_answers WHERE project_id='" . $pid . "' AND participant_id='" . $rs_data->p_id . "' AND question_type='pre'");
                $result1 = $query1->result();
                if (!empty($result1)) {
                    $preAttd = 1;
                }

                $query2 = $this->db->query("SELECT project_id FROM assessment_answers WHERE project_id='" . $pid . "' AND participant_id='" . $rs_data->p_id . "' AND question_type='post'");
                $result2 = $query2->result();
                if (!empty($result2)) {
                    $postAttd = 1;
                }

                $query3 = $this->db->query("SELECT id FROM feedback_answers WHERE project_id='" . $pid . "' AND p_id='" . $rs_data->p_id . "'");
                $result3 = $query3->result();
                if (!empty($result3)) {
                    $feedback = 1;
                }

                $data[$no] = array('pName' => ucwords(trim($rs_data->name)),
                    'pid' => $rs_data->p_id,
                    'pMobile' => $rs_data->phone_number,
                    'preScore' => $rs_data->pre_score,
                    'postScore' => $rs_data->post_score,
                    'vpa' => $rs_data->vpa_address,
                    'noVpa' => $rs_data->vpa_no,
                    'noVpaReason' => $rs_data->no_vpa_reason,
                    'preS' => $preAttd,
                    'postS' => $postAttd,
                    'feedback' => $feedback);

                $no++;
            }

            echo json_encode($data);
        } else {
            echo "0";
        }
    }

    public function getFullProject() {
        $pid = $_POST['pid'];
        //$pid = '71';
        $query = $this->db->query("SELECT p.*, t.name FROM trainer_assigned p, application_users t WHERE p.project_id='" . $pid . "' AND t.user_code=p.trainer_id");
        $result = $query->result();
        if (!empty($result)) {
            $checkIn = time_formate($result[0]->trainer_checkin_time);
            $checkOut = time_formate($result[0]->trainer_checkout_time);
            $preTime = time_formate($result[0]->pre_test_trigger_time);
            $postTime = time_formate($result[0]->post_test_trigger_time);

            $query1 = $this->db->query("SELECT COUNT(p_id) as pTotal FROM participants WHERE project_id='" . $pid . "'");
            $result1 = $query1->result();

            $query2 = $this->db->query("SELECT COUNT(p_id) as vpaTotal FROM participants WHERE project_id='" . $pid . "' AND vpa_address!=''");
            $result2 = $query2->result();



            $pTotal = 0;
            $vpaTotal = 0;
            $bhimTotal = 0;
            if (!empty($result1)) {
                $pTotal = $result1[0]->pTotal;

                $query3 = $this->db->query("SELECT SUM(bhim_download) as bhimTotal FROM participants WHERE project_id='" . $pid . "'");
                $result3 = $query3->result();

                if (!empty($result3)) {
                    if ($result3[0]->bhimTotal != null) {
                        $bhimTotal = $result3[0]->bhimTotal;
                    }
                }
            }
            if (!empty($result2)) {
                $vpaTotal = $result2[0]->vpaTotal;
            }


            $data[0] = array('tName' => $result[0]->name,
                'pre' => $result[0]->pre_test_trigger,
                'post' => $result[0]->post_test_trigger,
                'lat' => $result[0]->trainer_lat,
                'lng' => $result[0]->trainer_long,
                'chekcInTime' => $checkIn,
                'checkOutTIme' => $checkOut,
                'checkIn' => $result[0]->trainer_checkin_status,
                'checkOut' => $result[0]->trainer_checkout_status,
                'preTime' => $preTime,
                'postTime' => $postTime,
                'pTotal' => $pTotal,
                'vpaTotal' => $vpaTotal,
                'bhimTotal' => $bhimTotal);

            echo json_encode($data);
        } else {
            echo "0";
        }
    }

    public function getImages() {
        $pid = $_POST['pid'];

        $query = $this->db->query("SELECT * FROM trainer_img WHERE project_id='" . $pid . "'");
        $result = $query->result();
        if (!empty($result)) {
            $data = array();
            $no = 0;
            foreach ($result as $rs_data) {
                if (!empty($rs_data->ti_img)) {
                    $data[$no] = array('id' => $rs_data->ti_id, 'img' => $rs_data->ti_img);
                    $no++;
                }
            }

            echo json_encode($data);
        }
    }

    public function getForms() {
        $pid = $_POST['pid'];

        $query = $this->db->query("SELECT form_file_name FROM program_forms WHERE project_id='" . $pid . "' AND upload_type='1'");
        $result = $query->result();
        if (!empty($result)) {
            echo json_encode($result);
        } else {
            echo '0';
        }
    }

    public function waterMark($src) {
        ini_set('memory_limit', '240M');
        $SourceFile = "./App/assets/upload/" . $src;
        $WaterMarkText = date('Y-m-d H:s a');
        $DestinationFile = "./assets/images/admin34.png";
        list($width, $height) = getimagesize($SourceFile);
        $image_p = imagecreatetruecolor($width, $height);
        $image = imagecreatefromjpeg($SourceFile);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width, $height);
        $black = imagecolorallocate($image_p, 193, 7, 1);
        $font = './assets/fonts/Heebo-Bold.ttf';
        $font_size = 60;
        imagettftext($image_p, $font_size, 0, 50, 100, $black, $font, $WaterMarkText);
        if ($DestinationFile <> '') {
            imagejpeg($image_p, $DestinationFile, 50);
        } else {
            header('Content-Type: image/jpeg');
            imagejpeg($image_p, null, 50);
        };
        imagedestroy($image);
        imagedestroy($image_p);
    }

    public function save_trainer_feedback() {
        $data = json_decode($_POST['ans']);
        $data1 = (array) $data;
        //print_r($data1);
        foreach ($data1 as $key => $val) {
            $dd = array('project_id' => $_POST['pid'],
                'trainer_id' => $_POST['tid'],
                'tf_question' => $key,
                'tf_ans' => $val,
                'tf_date' => date('Y-m-d'));

            $this->db->insert('trainer_feedback', $dd);
        }
    }

    public function save_participants() {

        $check = $this->db->query("SELECT project_id FROM participants WHERE project_id='" . $_POST['pid'] . "' AND phone_number='" . $_POST['mobile'] . "'");
        if ($check->num_rows()) {
            echo '0';
        } else {
            $data = array('project_id' => $_POST['pid'],
                'phone_number' => $_POST['mobile'],
                'name' => $_POST['name'],
                'designation' => $_POST['desg'],
                'create_time' => $this->full_date);

            $this->db->insert('participants', $data);
            echo '1';
        }
    }

}
