<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class CronJobs extends CI_Controller {

    public function page_map($pr, $sc) {
        $this->$pr($sc);
    }

    public function check_job() {
        $today = date('Y-m-d H:i:s');
        $data = array('tdate' => $today);

        $this->db->insert('cron_test', $data);
    }

    public function new_program() {
        $query = $this->db->query("SELECT * FROM email_notifications  ORDER BY en_id LIMIT 100");
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $data = array('title' => 'Notification: New Program ',
                    'body' => 'New ' . $rs_data->project_type . ' Program open for engagement for  date: ' . date_formate_short($rs_data->project_date." 00:00:00"));
                $email = $rs_data->email_id;

                $this->db->query("DELETE FROM email_notifications WHERE email_id='" . $rs_data->email_id . "' AND project_id='" . $rs_data->project_id . "'");

                new_program_trainer($email, $data);
            }
        }
    }

    public function pm() {
        $mailArray = array();
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,u.user_code,u.email FROM training_projects p INNER JOIN  application_users u ON p.user_code=u.user_code, program_trainers pt WHERE p.is_active='1' AND p.training_start_date >= CURDATE() AND  p.training_start_date <= ( CURDATE() + INTERVAL 3 DAY ) AND pt.project_id=p.project_id AND pt.trainer_content_pass='' GROUP BY p.project_id");
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $data = array('title' => 'Notification: Content Mapping Pending',
                    'body' => 'Content mapping is pending for project you assigned',
                    'project' => $rs_data->project_title,
                    'location' => $rs_data->location_of_training,
                    'date' => date_formate_short($rs_data->training_start_date));

                $email = $rs_data->email;
                $mailArray[$email][] = $data;
                //mail_to_pm($email, $data);
            }
        }

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,u.user_code,u.email FROM training_projects p INNER JOIN  application_users u ON p.user_code=u.user_code WHERE p.is_active='1' AND  p.training_start_date = ( CURDATE() + INTERVAL 2 DAY )");
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $data = array('title' => 'Notification: Upcoming Program ',
                    'body' => 'Tommorow you have program',
                    'project' => $rs_data->project_title,
                    'location' => $rs_data->location_of_training,
                    'date' => date_formate_short($rs_data->training_start_date));

                $email = $rs_data->email;
                $mailArray[$email][] = $data;
            }
        }

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,u.user_code,u.email FROM training_projects p INNER JOIN  application_users u ON p.user_code=u.user_code WHERE p.is_active='1' AND   p.training_start_date = ( CURDATE() + INTERVAL 1 DAY )");
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $data = array('title' => 'Notification: Today Program Alert',
                    'body' => 'Today you have program in ' . $rs_data->location_of_training,
                    'project' => $rs_data->project_title,
                    'location' => $rs_data->location_of_training,
                    'date' => date_formate_short($rs_data->training_start_date));

                $email = $rs_data->email;
                $mailArray[$email][] = $data;
            }
        }

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,u.user_code,u.email FROM training_projects p INNER JOIN  application_users u ON p.user_code=u.user_code WHERE p.is_active='1' AND  p.training_start_date >= CURDATE() AND  p.training_start_date <= ( CURDATE() + INTERVAL 3 DAY ) AND p.venue=''");
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $data = array('title' => 'Notification: Venue & SPOC Update Pending',
                    'body' => 'You have pending task to update venue and SPOC for program you are assigned ',
                    'project' => $rs_data->project_title,
                    'location' => $rs_data->location_of_training,
                    'date' => date_formate_short($rs_data->training_start_date));

                $email = $rs_data->email;
                $mailArray[$email][] = $data;
            }
        }

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,u.user_code,u.email FROM training_projects p INNER JOIN  application_users u ON p.user_code=u.user_code, trainer_assigned ta WHERE p.is_active='1' AND  p.training_start_date >= CURDATE() AND  p.training_start_date <= ( CURDATE() + INTERVAL 5 DAY ) AND ta.project_id=p.project_id AND ta.ttt_required='Y' AND ta.ttt_schedule_date IS NULL GROUP BY p.project_id ");
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $rs_data) {

                $query2 = $this->db->query("SELECT * FROM ttt_schedules WHERE project_id='" . $rs_data->project_id . "'");
                if (!$query2->num_rows()) {
                    $data = array('title' => 'TTT Schedule is pending',
                        'body' => 'TTT Schedule is pending for program you assigned',
                        'project' => $rs_data->project_title,
                        'location' => $rs_data->location_of_training,
                        'date' => date_formate_short($rs_data->training_start_date));

                    $email = $rs_data->email;
                    $mailArray[$email][] = $data;
                }
            }
        }

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,u.user_code,u.email FROM training_projects p INNER JOIN  application_users u ON p.user_code=u.user_code, ttt_schedules tt WHERE  tt.ttt_date = ( CURDATE() + INTERVAL 1 DAY ) AND p.project_id=tt.project_id AND p.is_active='1'   GROUP BY p.project_id");
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $data = array('title' => 'Notification: TTT Schedule Today',
                    'body' => 'Today there is TTT Schedule for program you assigned ',
                    'project' => $rs_data->project_title,
                    'location' => $rs_data->location_of_training,
                    'date' => date_formate_short($rs_data->training_start_date));

                $email = $rs_data->email;
                $mailArray[$email][] = $data;
            }
        }

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,u.user_code,u.email FROM program_content_settings ps, training_projects p INNER JOIN  application_users u ON p.user_code=u.user_code WHERE ps.participant_handbook!='Not Required' AND p.project_id=ps.project_id AND p.training_start_date >= CURDATE() AND p.is_active='1'");
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $check = $this->db->query("SELECT project_id FROM program_expenses WHERE project_id='" . $rs_data->project_id . "' AND expense_type='Participant Handbook'");
                if (!$check->num_rows()) {
                    $email1 = '';
                    $query2 = $this->db->query("SELECT email FROM application_users WHERE user_type='admin support' || user_type='content manager'");
                    $result2 = $query2->result();
                    if (!empty($result2)) {
                        foreach ($result2 as $rsData) {
                            $email1 .=$rsData->email . ',';
                        }
                    }

                    $email1 = rtrim($email1, ',');

                    $data = array('title' => 'Notification: Participant Handbook Pending Delivery',
                        'body' => 'Participant Handbook is not delivered for program ',
                        'project' => $rs_data->project_title,
                        'location' => $rs_data->location_of_training,
                        'date' => date_formate_short($rs_data->training_start_date));

                    $email = $email1;
                    $mailArray[$email][] = $data;

//                    $data = array('title' => 'Notification: Participant Handbook Pending Delivery',
//                        'body' => 'Participant Handbook delivery is pending for the below program',
//                        'project' => $rs_data->project_title,
//                        'location' => $rs_data->location_of_training,
//                        'date' => date_formate_short($rs_data->training_start_date));
                    //$mailArray[$email1][] = $data;
                    //mail_to_admin($email1, $data);
                }
            }
        }

        //print_r($mailArray);
        if (!empty($mailArray)) {
            foreach ($mailArray as $em => $val) {
                $data2['dd'] = $val;
                mail_to_pm($em, $val);
                //$this->load->view('email_temp/pm-template',$data2);
            }
        }
    }

    public function trainer_mail() {

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,u.user_code,u.email,u.app_password,u.contact_number FROM training_projects p, program_trainers pt INNER JOIN  application_users u ON pt.trainer_id=u.user_code WHERE p.is_active='1' AND  p.training_start_date = CURDATE() AND pt.project_id=p.project_id GROUP BY p.project_id");
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $rs_data) {

                $pmQuery = $this->db->query("SELECT u.name,u.contact_number FROM training_projects p,application_users u WHERE p.project_id='" . $rs_data->project_id . "' AND u.user_code=p.user_code");
                $pmResult = $pmQuery->result();

                $bText = "Thank you for successfully completing the program. <br/><br/>";
                $bText .= "<b>This mail is to inform you about the processes to be followed post completion of program.</b> <br/>";
                $bText .= "Upload soft copies of Attendance Sheet, Feedback Sheet, Test Papers, Assessment sheet or any other training documents by logging into your trainer login area<br/>";
                $bText .= "Review & Upload Images if you have not uploaded in mobile app on the day of training<br/>";
                $bText .= "Generate Professional Fee Invoice and Add debit note with expenses incurred for the program (subject to agreement and approval/consent of respective PM).<br/>";
                $bText .= "For Expenses if applicable post approval from PM, please send hard copy of debit note along with original vouchers / bills (like cab bills, petrol bills, train/bus tickets etc.) to Wagons Pune Office.<br/>";
                $bText .= "Please ensure you generate Invoice and Debit Note within 3 days of completion of program..<br/><br/>";
                $bText .= "In case of any query, kindly contact PM : " . $pmResult[0]->name . " on " . $pmResult[0]->contact_number . "<br/>";

                $data = array('title' => 'Program completion task',
                    'body' => $bText,
                    'project' => $rs_data->project_title,
                    'location' => $rs_data->location_of_training,
                    'date' => date_formate_short($rs_data->training_start_date));

                $email = $rs_data->email;
                mail_to_trainer($email, $data);
            }
        }

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,u.user_code,u.email,u.app_password,u.contact_number FROM training_projects p, program_trainers pt INNER JOIN  application_users u ON pt.trainer_id=u.user_code WHERE p.is_active='1' AND  p.training_start_date = ( CURDATE() + INTERVAL 3 DAY ) AND pt.project_id=p.project_id GROUP BY p.project_id");
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $rs_data) {

                $pmQuery = $this->db->query("SELECT u.name,u.contact_number FROM training_projects p,application_users u WHERE p.project_id='" . $rs_data[0]->project_id . "' AND u.user_code=p.user_code");
                $pmResult = $pmQuery->result();

                $bText = "This is to inform you that you have a program scheduled with Wagons Learning. As part of the program process you need to download Wagons Trainer App from playstore using below mentioned link";
                $bText .= "<br/><br/><b>Please download app with this link</b> <br/>";
                $bText .= "Below are the credentials to login to the app. <br/> <br/>";
                $bText .= "Login : " . $rs_data->contact_number . "<br/>";
                $bText .= "Password : " . $rs_data->app_password . "<br/><br/>";
                $bText .= "Once you reach the program venue you need to login to mobile app and check-in for the program by selecting program name<br/>";
                $bText .= "During the program you can take sessionís photographs and of attendance sheet as well and upload it in the app itself.<br/>";
                $bText .= "Post completion of program you need to check-out from the app. You can however re-login to upload further images before the close of the day.<br/><br/>";
                $bText .= "In case of any query, kindly contact PM : " . $pmResult[0]->name . " on " . $pmResult[0]->contact_number . "<br/>";

                $data = array('title' => 'Notification: Upcoming Program Alert',
                    'body' => $bText,
                    'project' => $rs_data->project_title,
                    'location' => $rs_data->location_of_training,
                    'date' => date_formate_short($rs_data->training_start_date));

                $email = $rs_data->email;
                mail_to_trainer($email, $data);
            }
        }

        $mailArray = array();
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,u.user_code,u.email FROM training_projects p, program_trainers pt INNER JOIN  application_users u ON pt.trainer_id=u.user_code WHERE p.is_active='1' AND  p.training_start_date = ( CURDATE() + INTERVAL 2 DAY ) AND pt.project_id=p.project_id GROUP BY p.project_id");
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $data = array('title' => 'Notification: Tomorrow Program Alert',
                    'body' => 'Tommorow you have program',
                    'project' => $rs_data->project_title,
                    'location' => $rs_data->location_of_training,
                    'date' => date_formate_short($rs_data->training_start_date));

                $email = $rs_data->email;
                $mailArray[$email][] = $data;
                //mail_to_trainer($email, $data);
            }
        }

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,u.user_code,u.email FROM training_projects p, program_trainers pt INNER JOIN  application_users u ON pt.trainer_id=u.user_code WHERE p.is_active='1' AND  p.training_start_date = ( CURDATE() + INTERVAL 1 DAY ) AND pt.project_id=p.project_id GROUP BY p.project_id");
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $data = array('title' => 'Notification: Today Program Alert',
                    'body' => 'Today you have program in ' . $rs_data->location_of_training,
                    'project' => $rs_data->project_title,
                    'location' => $rs_data->location_of_training,
                    'date' => date_formate_short($rs_data->training_start_date));

                $email = $rs_data->email;
                $mailArray[$email][] = $data;
                //mail_to_trainer($email, $data);
            }
        }

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,u.user_code,u.email FROM training_projects p, program_trainers pt INNER JOIN  application_users u ON pt.trainer_id=u.user_code WHERE  pt.trainer_date_to <= ( CURDATE() - INTERVAL 3 DAY ) AND pt.trainer_invoice_flag='0' AND p.project_id=pt.project_id AND p.is_active='1' AND p.invoice_status='1'");
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $data = array('title' => 'Notification: Invoice Pending Alert',
                    'body' => 'Your invoice generation is pending for program you assigned',
                    'project' => $rs_data->project_title,
                    'location' => $rs_data->location_of_training,
                    'date' => date_formate_short($rs_data->training_start_date));

                $email = $rs_data->email;
                $mailArray[$email][] = $data;

                $data1 = array('user_id' => $rs_data->user_code,
                    'project_id' => $rs_data->project_id,
                    'notification_text' => 'Your invoice generation is pending for program ' . $rs_data->project_title,
                    'todo_task' => '1',
                    'notification_type' => 'Invoice Generation Pending',
                    'due_date' => '',
                    'notification_date' => date('Y-m-d'));

                $this->db->insert('notifications', $data1);

                //mail_to_trainer($email, $data);
            }
        }

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,u.user_code,u.email FROM training_projects p, program_trainers pt INNER JOIN  application_users u ON pt.trainer_id=u.user_code WHERE  pt.training_date_from >= ( CURDATE() + INTERVAL 1 DAY ) AND pt.trainer_content_pass!='' AND pt.content_download_date='0000-00-00 00:00:00' AND p.project_id=pt.project_id AND p.is_active='1'");
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $data = array('title' => 'Notification: Training Content Available',
                    'body' => 'Content is mapped to program you assigned',
                    'project' => $rs_data->project_title,
                    'location' => $rs_data->location_of_training,
                    'date' => date_formate_short($rs_data->training_start_date));

                $email = $rs_data->email;
                $mailArray[$email][] = $data;
                //mail_to_trainer($email, $data);
            }
        }

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,u.user_code,u.email FROM training_projects p, program_trainers pt INNER JOIN  application_users u ON pt.trainer_id=u.user_code WHERE  pt.trainer_invoice_flag='2' AND p.project_id=pt.project_id");
        $result = $query->result();



        if (!empty($result)) {
            $query2 = $this->db->query("SELECT email FROM application_users WHERE user_type='training support'");
            $result2 = $query2->result();
            if (!empty($result2)) {
                foreach ($result as $rs_data) {
                    $data = array('title' => 'Notification: Training Content Available',
                        'body' => 'Content is mapped to program you assigned',
                        'project' => $rs_data->project_title,
                        'location' => $rs_data->location_of_training,
                        'date' => date_formate_short($rs_data->training_start_date));

                    $email = $result2[0]->email;
                    $mailArray[$email][] = $data;
                    //mail_to_ts($email, $data);
                }
            }
        }

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,u.user_code,u.email FROM training_projects p, program_trainers pt INNER JOIN  application_users u ON pt.trainer_id=u.user_code WHERE  pt.training_date_from = ( CURDATE() + INTERVAL 1 DAY )  AND p.project_id=pt.project_id");
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $data = array('title' => 'Notification: Post Training Processes',
                    'body' => 'Please finish your post training activities for the program you have been assigned',
                    'project' => $rs_data->project_title,
                    'location' => $rs_data->location_of_training,
                    'date' => date_formate_short($rs_data->training_start_date));

                $email = $rs_data->email;
                $mailArray[$email][] = $data;
                //mail_to_trainer($email, $data);
            }
        }

        if (!empty($mailArray)) {
            foreach ($mailArray as $em => $val) {
                $data2['dd'] = $val;
                mail_to_pm($em, $val);
                //$this->load->view('email_temp/pm-template',$data2);
            }
        }
    }

    public function admin_mail() {
        $adminEmail = 'accounts@wagonslearning.com';
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,DATEDIFF(current_date,p.training_start_date) as diff,u.user_code,u.email,u.name,u.trainer_credit FROM training_projects p, program_trainers pt INNER JOIN  application_users u ON pt.trainer_id=u.user_code WHERE pt.trainer_paid_status='0' AND p.project_id=pt.project_id AND p.is_active='1'");
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $rs_data) {
                if ($rs_data->diff >= $rs_data->trainer_credit) {
                    $data = array('title' => 'Notification: Trainer Invoice Payment Pending',
                        'body' => 'Trainer Payment pending for below program',
                        'project' => $rs_data->project_title,
                        'location' => $rs_data->location_of_training,
                        'date' => date_formate_short($rs_data->training_start_date));

                    // mail_to_admin($adminEmail, $data);
                    $mailArray[$adminEmail][] = $data;
                }
            }
        }


        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,p.half_day,u.user_code,u.email,DATEDIFF(current_date,p.training_start_date) as diff FROM training_projects p INNER JOIN  application_users u ON p.user_code=u.user_code WHERE p.client_charges!='0' AND p.is_active='1' AND  p.training_start_date <= ( CURDATE() - INTERVAL 8 DAY ) AND p.client_invoice_raised='N' ");
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $halfday = "";
                if($rs_data->half_day=='1'){
                    $halfday = " (Half Day Program) ";
                }
                if ($rs_data->diff >= 7) {
                    $data = array('title' => 'Notification: Client Invoice Over Due',
                        'body' => 'Client Invoice generation is over due for bellow program',
                        'project' => $rs_data->project_title,
                        'location' => $rs_data->location_of_training,
                        'date' => date_formate_short($rs_data->training_start_date)." ".$halfday);
                } else {

                    $data = array('title' => 'Notification: Client Invoice Pending',
                        'body' => 'Client Invoice generation is pending for bellow program',
                        'project' => $rs_data->project_title,
                        'location' => $rs_data->location_of_training,
                        'date' => date_formate_short($rs_data->training_start_date)." ".$halfday);
                }

                //mail_to_admin($adminEmail, $data);
                $mailArray[$adminEmail][] = $data;
            }
        }
        
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,u.user_code,u.email FROM training_projects p INNER JOIN  application_users u ON p.user_code=u.user_code WHERE p.is_active='1' AND  p.training_start_date = ( CURDATE() - INTERVAL 2 DAY )");
        $result = $query->result();

        if (!empty($result)) {
            foreach ($result as $rs_data) {
                    $data = array('title' => 'Program Completion',
                        'body' => 'Bellow program is completed.',
                        'project' => $rs_data->project_title,
                        'location' => $rs_data->location_of_training,
                        'date' => date_formate_short($rs_data->training_start_date));
                    
                mail_to_admin($adminEmail, $data);
                //$mailArray[$adminEmail][] = $data;
            }
        }

        if (!empty($mailArray)) {
            foreach ($mailArray as $em => $val) {
                $data2['dd'] = $val;
                mail_to_pm($em, $val);
                //$this->load->view('email_temp/pm-template',$data2);
            }
        }
    }

    public function note_mail() {
        $query = $this->db->query("SELECT u.user_code,u.email,u.last_logged,COUNT(n.id) as total FROM  program_trainers pt, application_users u LEFT JOIN notifications n ON u.user_code=n.user_id AND u.last_logged<n.notification_date WHERE  pt.trainer_date_to > ( CURDATE() - INTERVAL 60 DAY ) AND u.user_code=pt.trainer_id GROUP BY pt.trainer_id");
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $data = array('total' => $rs_data->total);
                if ($rs_data->total > 0) {
                    notification_mail($rs_data->email, $data);
                }
            }
        }
        
        $query = $this->db->query("SELECT u.user_code,u.email,u.last_logged,COUNT(n.id) as total FROM   application_users u LEFT JOIN notifications n ON u.user_code=n.user_id AND u.last_logged<n.notification_date WHERE  u.user_type !='trainer' GROUP BY u.user_code");
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $rs_data) {
                $data = array('total' => $rs_data->total);
                if ($rs_data->total > 0) {
                    notification_mail($rs_data->email, $data);
                }
            }
        }
    }

}

?>