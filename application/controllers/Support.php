<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class support extends CI_Controller {

    public function index() {
        $this->check_sess();
        $data['page_url'] = 'Dashboard';
        $this->load->view('support/index', $data);
    }

    public function page_map($pr, $sc) {
        $this->$pr($sc);
    }

    public function check_sess() {
        if (!$this->session->userdata('support')) {
            redirect('admin/login');
        }
    }

    public function profile() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['msg'] = 'new';
        $data['page_url'] = 'Support';
        if (isset($_POST['date_user'])) {
            $data['msg'] = $this->admin_model->save_trainer_calendar();
        }
        $data['support'] = $this->admin_model->get_single_trainer($this->session->userdata('t_code'));
        $data['calendar'] = $this->admin_model->get_trainer_calendor($this->session->userdata('t_code'));
        $this->load->view('support/profile', $data);
    }

    public function upcoming_programs($id) {
        $this->load->model('admin_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 0;
        $data['client'] = $this->admin_model->get_basic_client();
        $data['program'] = $this->admin_model->get_basic_programs();
        $data['count'] = $this->admin_model->getUpCount();
        $this->load->view('support/upcoming-programs', $data);
    }

    public function past_programs($id) {
        $this->load->model('admin_model');
        $data['page_url'] = 'Past Programs';
        $data['msg'] = 0;
        $data['client'] = $this->admin_model->get_basic_client();
        $data['program'] = $this->admin_model->get_basic_past_programs();
        $data['count'] = $this->admin_model->getPastCount();
        $this->load->view('support/past-programs', $data);
    }

    public function review($id, $u) {
        $id = explode('-', custome_decode($id));
        $data['id'] = $id[0];
        $data['pid'] = $id[1];
        $data['type'] = custome_decode($u);
        $this->load->model('admin_model');
        $data['msg'] = 0;
        if (isset($_POST['comment'])) {
            $data['msg'] = $this->admin_model->save_review();
        }
        $data['program'] = $this->admin_model->get_request_details($data['id']);
        $this->load->view('support/review', $data);
    }

    public function program_details($id) {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 'new';
        if (isset($_POST['content_user'])) {
            $data['msg'] = $this->projectmanager_model->save_content();
        }
        if (isset($_POST['form_user'])) {
            $data['msg'] = $this->projectmanager_model->save_form();
        }
        if (isset($_POST['cname'])) {
            $data['msg'] = $this->projectmanager_model->save_courier();
        }
        if (isset($_POST['pid'])) {
            $data['msg'] = $this->projectmanager_model->book_stay();
        }
        if (isset($_POST['checkin'])) {
            $data['msg'] = $this->projectmanager_model->admin_save_stay();
        }
        if (isset($_POST['mode'])) {
            $data['msg'] = $this->projectmanager_model->admin_save_travel();
        }
        if (isset($_POST['travel_amt1'])) {
            $data['msg'] = $this->projectmanager_model->book_travel();
        }
        if (isset($_POST['img_user'])) {
            $data['msg'] = $this->projectmanager_model->save_images();
        }
        $data['program'] = $this->projectmanager_model->get_single_program($id);

        $this->load->view('support/program-details', $data);
    }

    public function update_page($id) {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 'new';
        if (isset($_POST['cname'])) {
            $this->projectmanager_model->update_courier();
            redirect('support/program_details/' . $_POST['project']);
        }
        if (isset($_POST['checkin'])) {
            $this->projectmanager_model->update_stay();
            redirect('support/program_details/' . $_POST['project']);
        }
        if (isset($_POST['mode'])) {
            $this->projectmanager_model->update_travel();
            redirect('support/program_details/' . $_POST['project']);
        }
        if ($_GET['type'] == 'Courier') {
            $data['exp'] = $this->projectmanager_model->get_singleCourier($id);
        }
        if ($_GET['type'] == 'stay') {
            $data['exp'] = $this->projectmanager_model->get_singleStay($id);
        }
        if ($_GET['type'] == 'travel') {
            $data['exp'] = $this->projectmanager_model->get_singleTravel($id);
        }
        $this->load->view('support/update-page', $data);
    }

    public function engagement_requests($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        if (!empty($id)) {
            $this->load->model('projectmanager_model');
            $data['request'] = $this->projectmanager_model->get_requests($id);
        }
        $data['pid'] = $id;
        $this->load->view('support/engagement-request', $data);
    }

    public function logout() {
        $this->session->unset_userdata('support', '');
        $this->session->unset_userdata('t_name', '');
        $this->session->unset_userdata('t_id', '');
        $this->session->unset_userdata('t_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function ajax_page() {
        $this->load->model('trainer_model');
        echo $this->trainer_model->$_POST['page']($_POST);
    }

}
