<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->output->set_header('Access-Control-Allow-Origin: *');
    }

    public function index() {
        $this->load->model('home_model');
        $this->load->view('index');
    }

    public function page_map($pr, $sc = NULL) {
        $this->$pr($sc);
    }

    public function about() {
        $this->load->view('about');
    }
    
    public function update_img()
    {
        $query = $this->db->query("SELECT pd_id FROM products");
        $result = $query->result();
        foreach($result as $rs_data)
        {
            $query2 = $this->db->query("SELECT pd_id FROM product_img WHERE pd_id='".$rs_data->pd_id."'");
            if(!$query2->num_rows())
            {
                $this->db->query("INSERT INTO product_img (pd_id,pd_img) VALUES('".$rs_data->pd_id."','book_default.png')");
            }
        }
    }

    public function products($c, $y = false, $s = false) {
        if (empty($c)) {
            redirect('home/error');
            return FALSE;
        }
        $degree = urldecode($c);
        $year = '';
        $sub = '';
        if (!empty($y)) {
            $year = urldecode($y);
        }
        if (!empty($s)) {
            $sub = urldecode($s);
            $data['dg'] = $degree;
            $data['yr'] = $year;
            $data['sb'] = $sub;
            $this->load->view('subject-products.php', $data);
            return FALSE;
        }
        if (!empty($y)) {
            $data['dg'] = $degree;
            $data['yr'] = $year;
            $this->load->view('year-products.php', $data);
            return FALSE;
        }
        $data['dg'] = $degree;
        $this->load->view('degree-products.php', $data);
    }

    public function product_details($pd = false, $id = false) {
        if (empty($pd) || empty($id)) {
            redirect('home/error');
            return FALSE;
        }
        $this->load->model('home_model');
        $data['book'] = $this->home_model->get_book($id);
        if (empty($data['book'])) {
            redirect('home/error');
            return FALSE;
        }
        $this->load->view('product-desc.php', $data);
    }

    public function cart() {
        $this->check_session();
        $this->load->model('home_model');
        if (isset($_POST['fname'])) {
            $this->home_model->update_address();
        }
        $data['user'] = $this->home_model->get_user($this->session->userdata('user'));
        $data['address'] = $this->home_model->get_shipping_address($this->session->userdata('user'));

        $this->load->view('final-checkout.php', $data);
    }

    public function update_cart() {
        $this->check_session();
        if (isset($_POST['pid'])) {
            $this->load->model('home_model');
            $this->home_model->add_to_cart($_POST);
            $data['sh_address'] = $this->home_model->get_shipping_address($this->session->userdata('user'));
            $this->load->view('cart_box.php', $data);
        }
    }

    public function register() {
        $this->load->model('home_model');
        $data['msg'] = '';
        if (isset($_POST['fname'])) {
            $data['msg'] = $this->home_model->save_user();
            if ($data['msg'] == '1') {
                redirect('home/cart');
            }
        }
        if (isset($_POST['mobile'])) {
            $data['msg'] = $this->home_model->user_login();
            if ($data['msg'] == '3') {
                redirect('home/cart');
            }
        }
        $this->load->view('account.php', $data);
    }

    public function account() {
        $this->check_session();
        $this->load->model('home_model');
        $data['msg'] = '';
        if (isset($_POST['fname'])) {
            $this->home_model->update_address();
            $data['msg'] = 3;
        }
        if (isset($_POST['cu'])) {
            $data['msg'] = $this->home_model->update_account();
        }
        $data['user'] = $this->home_model->get_user($this->session->userdata('user'));
        $data['address'] = $this->home_model->get_shipping_address($this->session->userdata('user'));
        $data['order'] = $this->home_model->get_order_list($this->session->userdata('user'));

        $this->load->view('profile.php', $data);
    }

    public function order_cod() {
        $this->check_session();
        if (isset($_POST['oid'])) {
            $this->load->model('home_model');
            $oid = $this->home_model->order_cod();
            if ($oid > 1) {
                $this->session->set_flashdata('payment', 'true');
                redirect('home/order_details/' . $oid);
            }
        }
    }

    public function order_details($id) {
        $this->check_session();
        $this->load->model('home_model');
        if (isset($_POST['oid'])) {
            $this->home_model->cancel_order();
        }
        $data['pd'] = $this->home_model->get_order($this->session->userdata('user'), $id);
        $this->load->view('order_details.php', $data);
    }

    public function search_sugg() {
        if (isset($_GET['term'])) {
            $this->load->model('home_model');
            $search_key = $_GET['term'];
            echo $this->home_model->search_suggestion($search_key);
        }
    }

    public function search_books() {
        if (isset($_GET['tags'])) {
            $this->load->model('home_model');
            $key = explode('in', $_GET['tags']);

            if (count($key) > 0) {
                if (count($key) > 1) {
                    if (trim($key[1]) == "Authour") {
                        $data['search'] = $this->home_model->get_authour_search(trim($key[0]));
                    } else {
                        $data['search'] = $this->home_model->get_search($key[0]);
                    }
                } else {
                    $data['search'] = $this->home_model->get_search($key[0]);
                }
            } else {
                $data['search'] = $this->home_model->get_search($_GET['tags']);
            }
            $data['tags'] = $_GET['tags'];
            $this->load->view('search_result.php', $data);
        }
    }

    public function check_session() {
        if (!$this->session->userdata('user')) {
            redirect('home/register');
        }
    }

    public function ajax_page() {
        $this->load->model('home_model');
        echo $this->home_model->$_POST['page']($_POST);
    }

    public function logout() {
        $this->session->unset_userdata('user');
        $this->session->unset_userdata('u_name');
        $this->session->unset_userdata('cart');
        redirect('home');
    }

}
