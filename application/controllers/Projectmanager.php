<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class projectmanager extends CI_Controller {

    public function index() {
        $this->check_sess();
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Dashboard';
        $data['week'] = $this->projectmanager_model->getWeekCount($this->session->userdata('pm_code'));
        $data['month'] = $this->projectmanager_model->getMonthCount($this->session->userdata('pm_code'));
        $data['total'] = $this->projectmanager_model->getTotalCount($this->session->userdata('pm_code'));
        $this->load->view('pm/index', $data);
    }

    public function page_map($pr, $sc) {
        $this->$pr($sc);
    }

    public function check_sess() {
        if (!$this->session->userdata('pm')) {
            redirect('admin/login');
        }
    }

    public function test_template() {
        $data['msg'] = "welcome";
        test_mail('anilk.acs@gmail.com', $data);
        //$this->load->view('email_temp/pm-template');
    }
    
    public function add_client() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Clients';
        $data['msg'] = 0;
        if (isset($_POST['fname'])) {
            $data['msg'] = $this->admin_model->save_client();
        }
        $data['pm'] = $this->admin_model->get_pm();
        $this->load->view('pm/add-clients', $data);
    }

    public function login() {
        if ($this->session->userdata('pm')) {
            redirect('projectmanager');
        }
        $this->load->view('admin/login');
    }

    public function profile() {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Dashborad';
        $data['trainer'] = $this->projectmanager_model->get_single_pm($this->session->userdata('pm_code'));
        $this->load->view('pm/profile', $data);
    }

    public function clients() {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Clients';
        $data['client'] = $this->projectmanager_model->get_client($this->session->userdata('pm_code'));
        $this->load->view('pm/clients', $data);
    }

    public function content_modules($id = NULL) {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Content';
        $data['cid'] = $id;
        $data['clients'] = $this->projectmanager_model->get_client($this->session->userdata('pm_code'));
        $data['modl'] = $this->projectmanager_model->get_client_module($id);
        $this->load->view('pm/content-module-list', $data);
    }

    public function add_program() {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Create Program';
        $data['msg'] = 0;
        if (isset($_POST['client'])) {
            $data['msg'] = $this->projectmanager_model->save_program();
        }
        if (isset($_POST['stay'])) {
            $data['msg'] = $this->projectmanager_model->update1_program();
            redirect('projectmanager/program_content/' . $data['msg']);
        }
        $data['client'] = $this->projectmanager_model->get_client($this->session->userdata('pm_code'));
        $this->load->view('pm/add-program', $data);
    }

    public function program_content($id) {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Program Content';
        $data['msg'] = 0;
        $data['pid'] = $id;
        if (isset($_POST['pid'])) {
            $data['msg'] = $this->projectmanager_model->save_content_setting();
            redirect('projectmanager/program_details/' . $id);
        }
        $data['pcontent'] = $this->projectmanager_model->get_content_setting($id);
        $data['modules'] = $this->projectmanager_model->get_modules($id);
        $this->load->view('pm/program-content', $data);
    }

    public function content_program($id = NULL) {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Create Program';
        $data['msg'] = 0;
        if (isset($_POST['client'])) {
            $data['msg'] = $this->projectmanager_model->save_content_program();
            redirect('projectmanager/program_details/' . $data['msg']);
        }
        $data['client'] = $this->projectmanager_model->get_client($this->session->userdata('pm_code'));
        $data['modules'] = $this->projectmanager_model->get_modules();
        $data['pid'] = $id;
        if (!empty($id)) {
            $data['program'] = $this->projectmanager_model->get_cp_program($id);
            $data['pcontent'] = $this->projectmanager_model->get_content_setting($id);
        }
        $this->load->view('pm/content-program', $data);
    }

    public function edit_content_program($id = NULL) {
        if (!empty($id)) {
            $this->load->model('projectmanager_model');
            $data['page_url'] = 'Create Program';
            $data['msg'] = 0;
            if (isset($_POST['client'])) {
                $data['msg'] = $this->projectmanager_model->update_content_program();
                redirect('projectmanager/program_details/' . $id);
            }
            $data['client'] = $this->projectmanager_model->get_client($this->session->userdata('pm_code'));
            $data['modules'] = $this->projectmanager_model->get_modules();
            $data['program'] = $this->projectmanager_model->get_cp_program($id);
            $data['pcontent'] = $this->projectmanager_model->get_content_setting($id);

            $this->load->view('pm/edit-content-program', $data);
        }
    }

    public function upcoming_programs($id) {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Upcoming Programs';
        $this->load->view('pm/spreadsheet-reader/php-excel-reader/excel_reader2.php');
        $this->load->view('pm/spreadsheet-reader/SpreadsheetReader.php');
        $data['msg'] = 0;
        if (isset($_POST['client'])) {
            $data['msg'] = $this->projectmanager_model->import_program();
        }
        $data['program'] = $this->projectmanager_model->get_basic_programs($this->session->userdata('pm_code'));
        $this->load->view('pm/upcoming-programs', $data);
    }

    public function past_programs($id) {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Past Programs';
        $this->load->view('pm/spreadsheet-reader/php-excel-reader/excel_reader2.php');
        $this->load->view('pm/spreadsheet-reader/SpreadsheetReader.php');
        $data['msg'] = 0;
        if (isset($_POST['client'])) {
            $data['msg'] = $this->projectmanager_model->import_program();
        }
        $data['program'] = $this->projectmanager_model->get_basic_past_programs($this->session->userdata('pm_code'));

        $data['count'] = $this->projectmanager_model->getPastCount();
        $this->load->view('pm/past-programs', $data);
    }

    public function program_details($id) {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 'new';
        
        if(isset($_POST['e_pid'])){
            $rid = time().$_POST['e_pid'];
            $this->db->query("UPDATE training_projects SET edit_status='".$rid."' WHERE project_id='".$_POST['e_pid']."'");
            $edata = array('reason'=>$_POST['e_reason'],'project'=>$_POST['e_ptitle'],'client'=>$_POST['e_pclient'],'pdate'=>$_POST['e_pdate'],'rid'=>$rid);
            program_edit_request($_POST['e_email'],$edata);
        }
        
        if (isset($_POST['content_user'])) {
            $data['msg'] = $this->projectmanager_model->save_content();
        }
        if (isset($_POST['form_user'])) {
            $data['msg'] = $this->projectmanager_model->save_form();
        }
        if (isset($_POST['props_user'])) {
            $data['msg'] = $this->projectmanager_model->save_props();
        }
        if (isset($_POST['ttt_user'])) {
            $data['msg'] = $this->projectmanager_model->save_ttt();
        }
        if (isset($_POST['venue_user'])) {
            $data['msg'] = $this->projectmanager_model->update_venue();
        }
        if (isset($_POST['assignId'])) {
            $data['msg'] = $this->projectmanager_model->update_checkIn();
        }
        $data['program'] = $this->projectmanager_model->get_single_program($id);

        $this->load->view('pm/program-details', $data);
    }

    public function load_content() {
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['content'] = $this->projectmanager_model->get_module_content();
        $this->load->view('pm/load-content', $data);
    }

    public function engagement_requests($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        if (!empty($id)) {
            $this->load->model('projectmanager_model');
            $data['request'] = $this->projectmanager_model->get_requests($id);
        }
        $data['pid'] = $id;
        $this->load->view('pm/engagement-request', $data);
    }

    public function trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['trainer'] = $this->admin_model->get_trainer();
        $data['count'] = $this->admin_model->getTrainerCount();
        $this->load->view('pm/trainers', $data);
    }

    public function inactive_trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['trainer'] = $this->admin_model->get_inactive_trainer();
        $this->load->view('pm/inactive_trainers', $data);
    }

    public function pending_trainers() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['trainer'] = $this->admin_model->get_pending_trainer();
        $this->load->view('pm/pending_trainers', $data);
    }

    public function search_trainer() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['state'] = $this->admin_model->get_state();
        $this->load->view('pm/search-trainer', $data);
    }

    public function add_trainer() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';
        $data['msg'] = "new";
        if (isset($_POST['pname'])) {
            $data['msg'] = $this->admin_model->save_trainer();
        }
        $data['state'] = $this->admin_model->get_state();
        $this->load->view('pm/add-trainer', $data);
    }

    public function trainer_profile($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['trainer'] = $this->admin_model->get_single_trainer($id);
            $this->load->view('pm/trainer-profile', $data);
        }
    }

    public function update_trainer($id) {
        if (!empty($id)) {
            // $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            if (isset($_POST['pname'])) {
                $data['msg'] = $this->admin_model->update_trainer();
            }
            $data['trainer'] = $this->admin_model->get_single_trainer($id);
            $data['state'] = $this->admin_model->get_state();
            $data['city'] = $this->admin_model->get_city_list($data['trainer'][0]->state);
            $this->load->view('pm/update-trainer', $data);
        }
    }

    public function trainer_engagment_request($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $data['engage'] = $this->admin_model->get_trainer_engagement($id);
            $this->load->view('pm/trainer-program-engagement', $data);
        }
    }

    public function edit_program($id) {
        if (!empty($id)) {
            $this->check_sess();
            $data['msg'] = "new";
            $data['page_url'] = 'Upcoming Programs';
            $this->load->model('projectmanager_model');
            if (isset($_POST['client'])) {
                $data['msg'] = $this->projectmanager_model->update_program();
            }
            $data['program'] = $this->projectmanager_model->get_single_program($id);
            $data['client'] = $this->projectmanager_model->get_client($this->session->userdata('pm_code'));
            $data['state'] = $this->projectmanager_model->get_state();
            $this->load->view('pm/edit-program', $data);
        }
    }

    public function add_content() {
        $this->check_sess();
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Content';
        $data['msg'] = 'new';
        if (isset($_POST['content_user'])) {
            $data['msg'] = $this->projectmanager_model->save_single_content();
        }
        $this->load->view('pm/add-content', $data);
    }

    public function program_expenses($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';

        if (!empty($id)) {
            $this->load->model('projectmanager_model');
            if (isset($_POST['amt'])) {
                $this->projectmanager_model->save_support_expense();
            }
            $data['exp'] = $this->projectmanager_model->get_program_expense($id);
            $data['debit'] = $this->projectmanager_model->get_debit_note($id);
            $data['fees'] = $this->projectmanager_model->get_project_fees($id);
        }
        $data['pid'] = $id;
        $this->load->view('pm/program-expense', $data);
    }

    public function contents() {
        $this->check_sess();
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Content';
        $data['msg'] = 'new';
        $data['content'] = $this->projectmanager_model->get_all_content();
        $this->load->view('pm/content', $data);
    }

    public function client_invoice() {
        $this->check_sess();
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Payments';
        $data['client'] = $this->projectmanager_model->get_basic_client();
        $data['msg'] = 'new';
        $data['invoice'] = $this->projectmanager_model->get_client_invoice($this->session->userdata('pm_code'));
        $data['count'] = $this->projectmanager_model->get_ciCount();
        $this->load->view('pm/client-invoice', $data);
    }

    public function client_invoice_details($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        $data['invoice'] = $this->admin_model->get_client_invoice($id);
        if (!empty($data['invoice'])) {
            $data['payment'] = $this->admin_model->get_client_payment($data['invoice'][0]->ci_id, '0');
            $data['client'] = $this->admin_model->get_single_client($data['invoice'][0]->ci_client);
            if ($data['invoice'][0]->ci_type == 'Program') {
                $data['project'] = $this->admin_model->get_basic_project($data['invoice'][0]->ci_project);
                $data['exp'] = $this->admin_model->get_project_expense($data['invoice'][0]->ci_project);
                $this->load->view('pm/client-invoice-details', $data);
            } else {
                $data['pr'] = $this->admin_model->get_month_project($data['invoice'][0]->ci_client, $data['invoice'][0]->ci_year, $data['invoice'][0]->ci_month);
                $this->load->view('pm/client-minvoice-details', $data);
            }
        }
    }

    public function payments() {
        $this->check_sess();
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Payments';
        $data['msg'] = 'new';
        $data['client'] = $this->projectmanager_model->get_basic_client();
        $data['payment'] = $this->projectmanager_model->get_PM_payment($this->session->userdata('pm_code'));
//        if(isset($_POST['pclient'])){
//        print_r($data['payment']);
//        return;
//        }
        $data['count'] = $this->projectmanager_model->get_countPayment();
        $this->load->view('pm/payment', $data);
    }

    public function invoice($tid, $pid) {
        $this->check_sess();
        $this->load->model('trainer_model');
        $data['page_url'] = 'Payment';
        $data['msg'] = 'new';
        if (isset($_POST['pid'])) {
            $this->trainer_model->support_confirm_invoice();
        }
        $data['debit'] = $this->trainer_model->get_invoice_details($tid, $pid);
        $data['fees'] = $this->trainer_model->get_trainer_fees($tid, $pid);
        $data['pDetails'] = $this->trainer_model->get_client_details($pid);
        $data['tDetails'] = $this->trainer_model->get_trainer_details($tid);
        $data['bank'] = $this->trainer_model->get_trainer_bank($tid);
        $data['tid'] = $tid;
        $data['pid'] = $pid;
        $data['courier'] = $this->trainer_model->get_bills_courier($pid, $tid);
        $this->load->view('pm/inovice', $data);
    }

    public function printinvoice($tid, $pid) {
        $this->check_sess();
        $this->load->model('trainer_model');
        $data['page_url'] = 'Payment';
        $data['msg'] = 'new';
        $data['pid'] = $pid;
        $data['debit'] = $this->trainer_model->get_invoice_details($tid, $pid);
        $data['fees'] = $this->trainer_model->get_trainer_fees($tid, $pid);
        $data['pDetails'] = $this->trainer_model->get_client_details($pid);
        $data['tDetails'] = $this->trainer_model->get_trainer_details($tid);
        $data['bank'] = $this->trainer_model->get_trainer_bank($tid);

        $this->load->view('pm/inovice_print', $data);
    }

    public function logout() {
        $this->session->unset_userdata('pm', '');
        $this->session->unset_userdata('pm_name', '');
        $this->session->unset_userdata('pm_id', '');
        $this->session->unset_userdata('pm_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function ajax_page() {
        $this->load->model('projectmanager_model');
        echo $this->projectmanager_model->$_POST['page']($_POST);
    }
    
    public function tmail(){
        testMail('anilk.acs@gmail.com');
    }

}
