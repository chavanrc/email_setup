<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Account extends CI_Controller {

    public function index() {
        $this->check_sess();
        redirect('account/past_programs');
    }

    public function page_map($pr, $sc) {
        $this->$pr($sc);
    }

    public function check_sess() {
        if (!$this->session->userdata('account')) {
            redirect('admin/login');
        }
    }

    public function login() {

        $this->load->view('admin/login');
    }

    public function logout() {
        $this->session->unset_userdata('drona', '');
        $this->session->unset_userdata('admin', '');
        $this->session->unset_userdata('name', '');
        $this->session->unset_userdata('user_id', '');
        $this->session->unset_userdata('user_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }
    
    public function account_logout() {
        $this->session->unset_userdata('account', '');
        $this->session->unset_userdata('name', '');
        $this->session->unset_userdata('user_id', '');
        $this->session->unset_userdata('user_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function pm_logout() {
        $this->session->unset_userdata('pm', '');
        $this->session->unset_userdata('pm_name', '');
        $this->session->unset_userdata('pm_id', '');
        $this->session->unset_userdata('pm_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function trainer_logout() {
        $this->session->unset_userdata('trainer', '');
        $this->session->unset_userdata('t_name', '');
        $this->session->unset_userdata('t_id', '');
        $this->session->unset_userdata('t_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function bdm_logout() {
        $this->session->unset_userdata('bdm', '');
        $this->session->unset_userdata('t_name', '');
        $this->session->unset_userdata('t_id', '');
        $this->session->unset_userdata('t_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function cm_logout() {
        $this->session->unset_userdata('cm', '');
        $this->session->unset_userdata('t_name', '');
        $this->session->unset_userdata('t_id', '');
        $this->session->unset_userdata('t_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function support_logout() {
        $this->session->unset_userdata('support', '');
        $this->session->unset_userdata('t_name', '');
        $this->session->unset_userdata('t_id', '');
        $this->session->unset_userdata('t_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    

    public function update_password($code = NULL, $id = NULL) {
        if (!empty($code) && !empty($id)) {
            $this->load->model('admin_model');
            // $link = $this->admin_model->forgot_password('anilk.acs@gmail.com');
            $data['email'] = custome_decode($id);
            $data['user_code'] = $code;
            $this->load->view('account/update-password', $data);
        }
    }

    public function email_temp() {
        $this->load->view('email_temp/forgot-password');
    }

    public function clients() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Clients';
        $data['client'] = $this->admin_model->get_client();
        $data['pm'] = $this->admin_model->get_pm();
        $this->load->view('account/clients', $data);
    }

    public function client_gst($id = NULL) {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Clients';
        $data['cid'] = $id;
        $data['msg'] = 0;
        if (isset($_POST['ccid'])) {
            $data['msg'] = $this->admin_model->addClient_gst();
        }
        if (isset($_POST['cid'])) {
            $this->load->view('admin/lib/excel_reader2');
            $this->load->view('admin/lib/SpreadsheetReader');
            $data['msg'] = $this->admin_model->import_gst();
        }
        $data['client'] = $this->admin_model->get_basic_client();
        $data['gst'] = $this->admin_model->get_client_gst($id);
        $this->load->view('account/client-gst', $data);
    }


    public function profile() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Dashboard';
        $data['trainer'] = $this->admin_model->get_single_trainer($this->session->userdata('user_code'));
        $this->load->view('account/profile', $data);
    }

    public function payments() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Payments';
        $data['msg'] = 'new';
        $data['payment'] = $this->admin_model->get_all_payment();
        $this->load->view('account/payment', $data);
    }

    public function trainer_invoices() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Payments';
        $data['msg'] = 'new';
        $data['payment'] = $this->admin_model->get_all_payment();
        $data['count'] = $this->admin_model->getTInvoiceCount();
        $this->load->view('account/invoice-list', $data);
    }

    public function export_trainer_invoice() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Payments';
        $data['msg'] = 'new';
        $query = $this->db->query("SELECT pt.*,p.project_id,p.project_title,p.training_duration,u.user_code,u.name FROM program_trainers pt INNER JOIN training_projects p ON pt.project_id=p.project_id INNER JOIN application_users u ON  pt.trainer_id=u.user_code WHERE pt.trainer_invoice_flag='1' AND pt.trainer_invoice_action='1' ORDER BY p.training_start_date DESC");
        $data['payment'] = $query->result();
        $filename = "TrainerInvoice" . time() . ".xls";
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $this->load->view('account/export-trainer-invoice', $data);
    }

    public function trainer_inovice_details($tid, $pid) {
        $this->check_sess();

        $data['page_url'] = 'Payment';
        $data['msg'] = 'new';
        if (isset($_POST['pid'])) {
            $this->load->model('admin_model');
            $this->admin_model->update_payment();
        }
        $data['tid'] = $tid;
        $data['pid'] = $pid;
        $this->load->model('trainer_model');
        $data['debit'] = $this->trainer_model->get_invoice_details($tid, $pid);
        $data['fees'] = $this->trainer_model->get_trainer_fees($tid, $pid);
        $data['pDetails'] = $this->trainer_model->get_client_details($pid);
        $data['tDetails'] = $this->trainer_model->get_trainer_details($tid);
        $data['bank'] = $this->trainer_model->get_trainer_bank($tid);
        $data['payment'] = $this->trainer_model->get_trainer_payment($data['fees'][0]->id);
        $this->load->view('account/trainer-invoice', $data);
    }

    public function trainer_inovice_print($tid, $pid) {
        $this->check_sess();

        $data['page_url'] = 'Payment';
        $data['msg'] = 'new';
        $this->load->model('trainer_model');
        $data['debit'] = $this->trainer_model->get_invoice_details($tid, $pid);
        $data['fees'] = $this->trainer_model->get_trainer_fees($tid, $pid);
        $data['pDetails'] = $this->trainer_model->get_client_details($pid);
        $data['tDetails'] = $this->trainer_model->get_trainer_details($tid);
        $data['bank'] = $this->trainer_model->get_trainer_bank($tid);
        $data['payment'] = $this->trainer_model->get_trainer_payment($data['fees'][0]->id);
        $this->load->view('account/trainer-invoice-print', $data);
    }


    public function export_program() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Trainers';

        $filter = "";


        if ($_POST['year'] != 'All') {
            $filter .= " AND YEAR(p.training_start_date)='" . $_POST['year'] . "' ";
        }

        if ($_POST['month'] != 'All') {
            $filter .= " AND MONTH(p.training_start_date)='" . $_POST['month'] . "' ";
        }

        if ($_POST['client'] != 'All') {
            $filter .= " AND p.client_id='" . $_POST['client'] . "' ";
        }

        $today = date('Y-m-d');
        $query = $this->db->query("SELECT p.client_id,p.project_id,p.project_title,p.client_charges,p.client_payment_receieved,p.location_of_training,p.training_start_date,p.training_end_date,p.training_duration,p.venue,p.stay_arrangement,p.travel_arrangement,p.program_state,p.zone,(SELECT client_name FROM clients WHERE client_id=p.client_id ) AS client_name, (SELECT invoice_type FROM clients WHERE client_id=p.client_id ) AS invoice_type,(SELECT cm.company_name FROM clients c INNER JOIN companies cm ON c.company_id=cm.company_id  WHERE client_id=p.client_id ) AS company_name,(SELECT SUM(expense_amount) FROM program_stay WHERE project_id=p.project_id) AS stayAmount,(SELECT SUM(expense_amount) FROM program_travel WHERE project_id=p.project_id) AS travelAmount,(SELECT SUM(debit_amt) FROM debit_note WHERE project=p.project_id AND debit_status='1') AS trainer_debit,u.name AS pname,t.name,t.city,t.trainer_credit,pt.amount,pt.trainer_date_to,pt.trainer_invoice_flag,pt.trainer_paid_status,pt.trainer_invoice_action FROM training_projects p INNER JOIN application_users u ON p.user_code=u.user_code, program_trainers pt INNER JOIN application_users t ON t.user_code=pt.trainer_id WHERE p.training_start_date<'" . $today . "' AND p.training_start_date>='2019-07-1' $filter AND  p.is_active='1' AND pt.project_id=p.project_id ORDER BY p.training_start_date DESC");

        $data['programs'] = $query->result();
        $filename = "Programs" . time() . ".xls";
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $this->load->view('account/export-program', $data);
    }


    public function trainer_single_profile($id) {
        if (!empty($id)) {
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['trainer'] = $this->admin_model->get_single_trainer($id);
            $data['exp'] = $this->admin_model->get_trainer_experience($id);
            $this->load->view('account/trainer-single-profile', $data);
        }
    }

    public function past_programs() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['page_url'] = 'Past Programs';
        $data['msg'] = 0;
        $data['client'] = $this->admin_model->get_basic_client();
        $data['pm'] = $this->admin_model->get_basicpm();
        $data['program'] = $this->admin_model->get_basic_past_programs();
        $data['count'] = $this->admin_model->getPastCount();
        $this->load->view('account/past-programs', $data);
    }

    public function program_details($id) {
        $this->check_sess();
        $this->load->model('projectmanager_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = 'new';
        if (isset($_POST['content_user'])) {
            $data['msg'] = $this->projectmanager_model->save_content();
        }
        if (isset($_POST['form_user'])) {
            $data['msg'] = $this->projectmanager_model->save_form();
        }
        if (isset($_POST['cname'])) {
            $data['msg'] = $this->projectmanager_model->save_courier();
        }
        if (isset($_POST['stay_user'])) {
            $data['msg'] = $this->projectmanager_model->book_stay();
        }
        if (isset($_POST['travel_user'])) {
            $data['msg'] = $this->projectmanager_model->book_travel();
        }
        if (isset($_POST['img_user'])) {
            $data['msg'] = $this->projectmanager_model->save_images();
        }
        $data['program'] = $this->projectmanager_model->get_single_program($id);

        $this->load->view('account/program-details', $data);
    }

    public function program_expenses($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';

        if (!empty($id)) {
            $this->load->model('projectmanager_model');
            $data['exp'] = $this->projectmanager_model->get_program_expense($id);
            $data['debit'] = $this->projectmanager_model->get_debit_note($id);
            $data['fees'] = $this->projectmanager_model->get_project_fees($id);
        }
        $data['pid'] = $id;
        $this->load->view('account/program-expense-list', $data);
    }

    public function edit_gst($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        $data['msg'] = "";
        if (isset($_POST['gid'])) {
            $data['msg'] = $this->admin_model->update_gst();
        }
        $data['gst'] = $this->admin_model->get_single_gst($id);
        $this->load->view('account/edit-client-gst', $data);
    }

    public function client_invoice() {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        $data['client'] = $this->admin_model->get_basic_client();
        $data['company'] = $this->admin_model->get_basic_company();
        $data['invoice'] = $this->admin_model->get_allclient_invoice();
        $data['count'] = $this->admin_model->getCInvoiceCount();
        $this->load->view('account/client-invoice', $data);
    }

    public function export_client_invoice() {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        $data['invoice'] = $this->admin_model->get_cinvoicefor_export();
        $filename = "ClientInvoice" . time() . ".xls";
        header("Content-Type: application/xls");
        header("Content-Disposition: attachment; filename=\"$filename\"");
        $this->load->view('account/export-client-invoice', $data);
    }

    public function export_all_invoice() {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        $data['client'] = $this->admin_model->get_basic_client();
        $data['invoice'] = $this->admin_model->get_cinvoicefor_export();
        if (isset($_POST['pclient'])) {
            
            $filename = "ClientInvoice" . time() . ".xls";
            header("Content-Type: application/xls");
            header("Content-Disposition: attachment; filename=\"$filename\"");
            $this->load->view('account/export-client-invoice', $data);
            return;
        } else {
            $this->load->view('account/export-all-invoice', $data);
        }
    }


    public function client_invoice_details($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        if (isset($_POST['amt'])) {
            $this->admin_model->save_client_payment();
        }
        if (isset($_POST['inumber'])) {
            $this->admin_model->save_edit_request();
        }
        if (isset($_POST['upCid'])) {
            $this->admin_model->save_invoice_copy();
        }
        $data['invoice'] = $this->admin_model->get_client_invoice($id);
        if (!empty($data['invoice'])) {
            $data['payment'] = $this->admin_model->get_client_payment($data['invoice'][0]->ci_id, '0');
            $data['erequest'] = $this->admin_model->get_allEdit_request($data['invoice'][0]->ci_number);
            $data['client'] = $this->admin_model->get_single_client($data['invoice'][0]->ci_client);
            if ($data['invoice'][0]->ci_type == 'Program') {
                $data['project'] = $this->admin_model->get_basic_project($data['invoice'][0]->ci_project);
                $data['exp'] = $this->admin_model->get_project_expenseForInvoie($data['invoice'][0]->ci_project);
                $this->load->view('account/client-invoice-details', $data);
            } else {
                $data['pr'] = $this->admin_model->get_month_project($data['invoice'][0]->ci_client, $data['invoice'][0]->ci_year, $data['invoice'][0]->ci_month);
                $this->load->view('account/client-minvoice-details', $data);
            }
        }
    }

    public function cinvoice($id, $eid) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        $data['erequest'] = $this->admin_model->get_edit_request($eid);
        $data['invoice'] = $this->admin_model->get_client_invoice($id);
        if (!empty($data['invoice'])) {
            $data['payment'] = $this->admin_model->get_client_payment($data['invoice'][0]->ci_id, '0');
            $data['client'] = $this->admin_model->get_single_client($data['invoice'][0]->ci_client);
            if ($data['invoice'][0]->ci_type == 'Program') {
                $data['project'] = $this->admin_model->get_basic_project($data['invoice'][0]->ci_project);
                $data['exp'] = $this->admin_model->get_project_expenseForInvoie($data['invoice'][0]->ci_project);
                $this->load->view('account/cinvoice', $data);
            } else {
                $data['pr'] = $this->admin_model->get_month_project($data['invoice'][0]->ci_client, $data['invoice'][0]->ci_year, $data['invoice'][0]->ci_month);
                $this->load->view('account/client-minvoice-details', $data);
            }
        }
    }

    public function expense($id) {
        $this->check_sess();
        $data['page_url'] = 'Create Program';
        $this->load->model('admin_model');
        $data['msg'] = "";
        if (isset($_POST['gstAddress'])) {
            $data['msg'] = $this->admin_model->update_client_invoice();
        }
        $data['invoice'] = $this->admin_model->get_client_invoice($id);
        if (!empty($data['invoice'])) {
            $data['exp'] = $this->admin_model->get_program_expense($data['invoice'][0]->ci_project);
            $data['fees'] = $this->admin_model->get_trainer_fees($data['invoice'][0]->ci_project);
            $data['pid'] = $data['invoice'][0]->ci_project;
            $this->load->view('account/program-expense', $data);
        }
    }

    public function drona_invoice() {
        $this->check_sess();
        $data['page_url'] = 'Drona Invoice';
        $this->load->model('admin_model');
        $data['client'] = $this->admin_model->get_basic_client();
        $data['company'] = $this->admin_model->get_basic_company();
        $data['invoice'] = $this->admin_model->get_drona_invoice();
        $data['count'] = $this->admin_model->getDronaCount();
        $this->load->view('account/drona-invoice-list', $data);
    }

    public function drona_invoice_details($id) {
        $this->check_sess();
        $data['page_url'] = 'Drona Invoice';
        $this->load->model('admin_model');
        if (isset($_POST['upCid'])) {
            $this->admin_model->save_dinvoice_copy();
        }
        if (isset($_POST['amt'])) {
            $this->admin_model->save_client_payment();
        }
        if (isset($_POST['inumber'])) {
            $this->admin_model->save_edit_request();
        }
        if (isset($_POST['drona'])) {
            $this->admin_model->update_drona_invoice();
        }
        if (isset($_POST['in_number'])) {
            $this->admin_model->save_drona_expense();
        }
        $data['client'] = $this->admin_model->get_basic_client();
        $data['company'] = $this->admin_model->get_basic_company();

        $data['invoice'] = $this->admin_model->get_singleDrona_invoice($id);
        if (!empty($data['invoice'])) {
            $data['payment'] = $this->admin_model->get_client_payment($data['invoice'][0]->di_id, '1');
            $data['cm'] = $this->admin_model->get_singleCompany($data['invoice'][0]->di_company);
            $data['exp'] = $this->admin_model->get_drona_expense($data['invoice'][0]->di_id);
            $data['location'] = $this->admin_model->get_client_gstLocation($data['invoice'][0]->di_client);
            if ($data['invoice'][0]->di_onlyDebit == '0') {
                $data['erequest'] = $this->admin_model->get_allEdit_request($data['invoice'][0]->di_number);
            } else {
                $data['erequest'] = $this->admin_model->get_allEdit_request($data['invoice'][0]->di_debitNumber);
            }
        }
        $this->load->view('account/drona-invoice-details', $data);
    }

    public function dinvoice($id, $eid) {
        $data['page_url'] = 'Drona Invoice';
        $this->load->model('admin_model');

        $data['invoice'] = $this->admin_model->get_singleDrona_invoice($id);
        if (!empty($data['invoice'])) {
            $data['cm'] = $this->admin_model->get_singleCompany($data['invoice'][0]->di_company);
            $data['exp'] = $this->admin_model->get_drona_expense($data['invoice'][0]->di_id);
            $data['location'] = $this->admin_model->get_client_gstLocation($data['invoice'][0]->di_client);
            if ($data['invoice'][0]->di_onlyDebit == '0') {
                $data['erequest'] = $this->admin_model->get_allEdit_request($data['invoice'][0]->di_number);
            } else {
                $data['erequest'] = $this->admin_model->get_allEdit_request($data['invoice'][0]->di_debitNumber);
            }
        }
        $this->load->view('account/dinvoice', $data);
    }

    public function ajax_page() {
        $this->load->model('admin_model');
        echo $this->admin_model->$_POST['page']($_POST);
    }

}
