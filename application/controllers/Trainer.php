<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class trainer extends CI_Controller {

    public function index() {
        $this->check_sess();
        $this->load->model('trainer_model');
        $data['page_url'] = 'Dashboard';
        $data['newP'] = $this->trainer_model->count_new_program();
        $data['upP'] = $this->trainer_model->count_upcoming_programs($this->session->userdata('t_code'));
        $data['pastP'] = $this->trainer_model->count_past_programs($this->session->userdata('t_code'));
        $this->load->view('trainer/index', $data);
    }

    public function page_map($pr, $sc) {
        $this->$pr($sc);
    }

    public function check_sess() {
        if (!$this->session->userdata('trainer')) {
            redirect('admin/login');
        }
    }

    public function trainer_profile() {
        $this->check_sess();
        $this->load->model('admin_model');
        $data['msg'] = 'new';
        $data['page_url'] = 'Trainers';
        if(isset($_POST['date_user']))
        {
            $data['msg'] = $this->admin_model->save_trainer_calendar();
        }
        $data['trainer'] = $this->admin_model->get_single_trainer($this->session->userdata('t_code'));
        $data['calendar'] = $this->admin_model->get_trainer_calendor($this->session->userdata('t_code'));
        $this->load->view('trainer/trainer-profile', $data);
    }

    public function update_trainer($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            if (isset($_POST['pname'])) {
                $data['msg'] = $this->admin_model->update_trainer();
            }
            $data['trainer'] = $this->admin_model->get_single_trainer($id);
            $data['state'] = $this->admin_model->get_state();
            $data['city'] = $this->admin_model->get_city_list($data['trainer'][0]->state);
            $this->load->view('trainer/update-trainer', $data);
        }
    }

    public function add_work($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            if (isset($_POST['designation'])) {
                $data['msg'] = $this->admin_model->add_trainer_work();
            }
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $this->load->view('trainer/trainer-work', $data);
        }
    }
    
    public function add_training($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            if (isset($_POST['company'])) {
                $data['msg'] = $this->admin_model->add_trainer_experience();
            }
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $this->load->view('trainer/add-experience', $data);
        }
    }

    public function update_work($code, $id = NULL) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            if (isset($_POST['designation'])) {
                $data['msg'] = $this->admin_model->update_trainer_work();
            }
            $data['trainer'] = $this->admin_model->get_basic_trainer($code);
            $data['work'] = $this->admin_model->get_single_trainer_work($code, $id);
            $this->load->view('trainer/edit-work', $data);
        }
    }

    public function add_education($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            if (isset($_POST['qualificatioin'])) {
                $data['msg'] = $this->admin_model->add_trainer_education();
            }
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $this->load->view('trainer/trainer-education', $data);
        }
    }

    public function add_certification($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            if (isset($_POST['title'])) {
                $data['msg'] = $this->admin_model->add_trainer_certification();
            }
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $this->load->view('trainer/trainer-certification', $data);
        }
    }

    public function trainer_experience($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $data['exp'] = $this->admin_model->get_trainer_experience($id);
            $this->load->view('trainer/trainer-experience', $data);
        }
    }
    
    public function update_bank($id){
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            if(isset($_POST['aname'])){
                $data['msg'] = $this->admin_model->save_bankDetails();
            }
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $data['bank'] = $this->admin_model->trainer_bankDetails($id);
            $this->load->view('trainer/trainer-bank', $data);
        }
    }


    public function new_programs() {
        $this->check_sess();
        $this->load->model('trainer_model');
        $data['page_url'] = 'New Programs';
        $data['program'] = $this->trainer_model->get_new_program();
        $this->load->view('trainer/new-programs', $data);
    }
    
    
    
    public function program_details($id) {
        $this->load->model('projectmanager_model');
        
        $data['msg'] = 'new';
        if(isset($_POST['content_user']))
        {
            $data['msg'] = $this->projectmanager_model->save_content();
        }
        if(isset($_POST['form_user']))
        {
            $data['msg'] = $this->projectmanager_model->save_form();
        }
        if(isset($_POST['props_user']))
        {
            $data['msg'] = $this->projectmanager_model->save_props();
        }
        if(isset($_POST['img_user']))
        {
            $data['msg'] = $this->projectmanager_model->save_images();
        }
        if (isset($_POST['stay_user'])) {
            $data['msg'] = $this->projectmanager_model->save_stay();
        }
        if (isset($_POST['travel_user'])) {
            $data['msg'] = $this->projectmanager_model->save_travel();
        }
        $data['program'] = $this->projectmanager_model->get_single_program($id);
	$data['fees'] = $this->projectmanager_model->get_trainer_fees($this->session->userdata('t_code'),$id);
        if($data['program'][0]->trainer_engage_flag=='0' || empty($data['fees']))
        {
            $data['page_url'] = 'New Programs';
            $this->load->view('trainer/new-program-details', $data);
        }
        else{
            $data['page_url'] = 'Upcoming Programs';
           $this->load->view('trainer/program-details', $data); 
        }
    }

    public function trainer_engagment_request($id) {
        if (!empty($id)) {
            $this->check_sess();
            $this->load->model('admin_model');
            $data['page_url'] = 'Trainers';
            $data['msg'] = "new";
            $data['trainer'] = $this->admin_model->get_basic_trainer($id);
            $data['engage'] = $this->admin_model->get_trainer_engagement($id);
            $this->load->view('trainer/trainer-program-engagement', $data);
        }
    }
    
    public function upcoming_programs()
    {
        $this->check_sess();
        $this->load->model('trainer_model');
        $data['page_url'] = 'Upcoming Programs';
        $data['msg'] = "new";
        $data['program'] = $this->trainer_model->get_upcoming_programs($this->session->userdata('t_code'));
        $this->load->view('trainer/upcoming-programs', $data);
       
    }
    
    public function tds_certificate($tid){
        
        $this->check_sess();
        $this->load->model('trainer_model');
        $this->load->model('admin_model');
        $data['page_url'] = 'tdscertificate';
        $data['msg'] = "new";
        //$data["trainer_id"]=$tid;
        //$data['program'] = $this->trainer_model->get_upcoming_programs($this->session->userdata('t_code'));
        
        $data['tds'] = $this->admin_model->get_all_tds_certificate($this->session->userdata('t_code'));
        //$data['trainer'] = $this->admin_model->get_basic_trainer($tid);
        $this->load->view('trainer/tds-certificate', $data);
       
    
    }
    public function past_programs()
    {
        $this->check_sess();
        $this->load->model('trainer_model');
        $data['page_url'] = 'Past Programs';
        $data['msg'] = "new";
        $data['program'] = $this->trainer_model->get_past_programs($this->session->userdata('t_code'));
        $this->load->view('trainer/past-programs', $data);
       
    }
    
    public function add_payment($id=NULL) {
        $this->check_sess();
        $this->load->model('trainer_model');
        $data['page_url'] = 'Content';
        $data['msg'] = 'new';
        $data['pid'] = $id;
        if(isset($_POST['tid']))
        {
            $data['msg'] = $this->trainer_model->save_payment();
        }
        $data['program'] = $this->trainer_model->get_trainer_program_list($this->session->userdata('t_code'));
        $this->load->view('trainer/add-payment', $data);
    }
    
    public function payments() {
        $this->check_sess();
        $this->load->model('trainer_model');
        $data['page_url'] = 'Payment';
        $data['msg'] = 'new';
        $data['payment'] = $this->trainer_model->get_payment_list($this->session->userdata('t_code'));
        $this->load->view('trainer/payment', $data);
    }
    
    public function invoice($id) {
        $this->check_sess();
        $this->load->model('trainer_model');
        $data['page_url'] = 'Payment';
        $data['msg'] = 'new';
        $data['pid']=$id;
        if(isset($_POST['gstNumber'])){
            $this->trainer_model->save_trainer_gstInvoice();
        }
        if(isset($_POST['ref'])){
            $this->trainer_model->save_bills_courier();
        }
        $data['debit'] = $this->trainer_model->get_invoice_details($this->session->userdata('t_code'),$id);
        $data['fees'] = $this->trainer_model->get_trainer_fees($this->session->userdata('t_code'),$id);
        $data['pDetails'] = $this->trainer_model->get_client_details($id);
        $data['tDetails'] = $this->trainer_model->get_trainer_details($this->session->userdata('t_code'));
        $data['bank'] = $this->trainer_model->get_trainer_bank($this->session->userdata('t_code'));
        $data['payment'] = $this->trainer_model->get_trainer_payment($data['fees'][0]->id);
        $data['courier'] = $this->trainer_model->get_bills_courier($id,$this->session->userdata('t_code'));
        $this->load->view('trainer/inovice', $data);
    }
    public function printinvoice($id) {
        $this->check_sess();
        $this->load->model('trainer_model');
        $data['page_url'] = 'Payment';
        $data['msg'] = 'new';
        $data['pid']=$id;
        $data['debit'] = $this->trainer_model->get_invoice_details($this->session->userdata('t_code'),$id);
        $data['fees'] = $this->trainer_model->get_trainer_fees($this->session->userdata('t_code'),$id);
        $data['pDetails'] = $this->trainer_model->get_client_details($id);
        $data['tDetails'] = $this->trainer_model->get_trainer_details($this->session->userdata('t_code'));
        $data['bank'] = $this->trainer_model->get_trainer_bank($this->session->userdata('t_code'));
        
        $this->load->view('trainer/inovice_print', $data);
    }
    public function add_content() {
        $this->check_sess();
        $this->load->model('trainer_model');
        $data['page_url'] = 'Content';
        $data['msg'] = 'new';
        if(isset($_POST['content_user']))
        {
            $data['msg'] = $this->trainer_model->save_content();
        }
        $this->load->view('trainer/add-content', $data);
    }
    
    public function contents() {
        $this->check_sess();
        $this->load->model('trainer_model');
        $data['page_url'] = 'Content';
        $data['msg'] = 'new';
        $data['content'] = $this->trainer_model->get_all_content($this->session->userdata('t_code'));
        $this->load->view('trainer/content', $data);
    }
    
    public function confirm_engage($tid,$pid){
        $this->db->query("UPDATE program_trainers SET engage_confirm='1' WHERE id='".$pid."'");
        $this->load->view('trainer/engage-confirm');
    }

    public function logout() {
        $this->session->unset_userdata('trainer', '');
        $this->session->unset_userdata('t_name', '');
        $this->session->unset_userdata('t_id', '');
        $this->session->unset_userdata('t_code', '');
        $this->session->unset_userdata('type', '');
        redirect('admin');
    }

    public function ajax_page() {
        $this->load->model('trainer_model');
        echo $this->trainer_model->$_POST['page']($_POST);
    }

}
