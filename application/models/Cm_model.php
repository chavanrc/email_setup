<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cm_model extends CI_Model {

    public $cdate = null;
    public $full_date = null;

    public function __construct() {
        $datestring = "%Y-%m-%d";
        $datestring1 = "%Y-%m-%d %H:%i:%s";
        $time = time();
        $this->cdate = mdate($datestring, $time);
        $this->full_date = mdate($datestring1, $time);
    }

    public function image_resize($file_name, $path, $newpath, $newWidth) {
        $img_size = getimagesize($path . $file_name);
        $newHeight = round(($newWidth / $img_size[0]) * $img_size[1]);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $file_name;
        $config['new_image'] = $newpath . $file_name;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $newWidth;
        $config['height'] = $newHeight;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    public function get_clients() {
        $query = $this->db->query("SELECT client_id,company_id,client_name FROM clients ORDER BY client_name");
        return $query->result();
    }

    public function get_cmodules() {
        $query = $this->db->query("SELECT * FROM client_modules");
        return $query->result();
    }

    public function load_module($val) {
        $query = $this->db->query("SELECT * FROM client_modules WHERE client_id='" . $val['cid'] . "'");
        $result = $query->result();
        ?>
        <select class="form-control" id="modl" name="modl">
            <option value=""> - Select Module -</option>
            <?php
            if (!empty($result)) {
                foreach ($result as $rs_data) {
                    ?>
                    <option value="<?php echo $rs_data->cm_id; ?>"><?php echo $rs_data->module_name; ?></option>
                    <?php
                }
            }
            ?>
        </select>
        <?php
    }

    public function save_content() {
        
        $typeArray = array('xls'=>'Spredsheet',
            'xlsx'=>'Spredsheet',
            'csv'=>'Spredsheet',
            'doc'=>'Document',
            'docx'=>'Document',
            'txt'=>'Document',
            'odt'=>'Document',
            'pdf'=>'PDF',
            'ppt'=>'Presentation',
            'pptx'=>'Presentation',
            'PPTX'=>'Presentation',
            'mp4'=>'Video',
            'mp3'=>'Video',
            'wav'=>'Video',
            'mov'=>'Video',
            'wmv'=>'Video',
            'flv'=>'Video',
            'jpg'=>'Image',
            'jpg'=>'Image',
            'png'=>'Image',
            'jpeg'=>'Image',
            'gif'=>'Image');

        $handbook = 0;
        if (isset($_POST['handbook'])) {
            $handbook = 1;
        }

        $path = './assets/upload/content/';
        $img_name = $_FILES['doc']['name'];
        $tmp_name = $_FILES['doc']['tmp_name'];

        if (!empty($img_name[0])) {
            $count = count($img_name);
            for ($i = 0; $i < $count; $i++) {
                $ext = pathinfo($img_name[$i]);
                $check = $this->db->query("SELECT content_id FROM content WHERE content_file='" . $img_name[$i] . "'");
                $fCount = 0;
                $imgName = $img_name[$i];
                if ($check->num_rows()) {
                    $result = $check->result();
                    $fCount = count($result);
                    $imgName = $ext['filename'] . "_" . $fCount . '.' . $ext['extension'];
                }
                if ($file_name = content_upload_new($path, $imgName, $tmp_name[$i])) {
                    $file = $file_name;
                }

                $data = array('client_id' => $_POST['client'],
                    'module_id' => $_POST['modl'],
                    'content_title' => $_POST['title'],
                    'content_notes' => $_POST['note'],
                    'content_type'=>$typeArray[$ext['extension']],
                    'for_trainer' => $_POST['trainerStatus'],
                    'content_file' => $file,
                    'file_type' => $ext['extension'],
                    'file_upType' => $_POST['upType'],
                    'content_handbook' => $handbook,
                    'last_update' => $this->full_date);

                $this->db->insert('content', $data);
            }
        } else {
            $file = $_POST['fName'];
            $ext = pathinfo($file);
            if (!file_exists($path . $file)) {
                return 2;
            }

            $data = array('client_id' => $_POST['client'],
                'module_id' => $_POST['modl'],
                'content_title' => $_POST['title'],
                'content_notes' => $_POST['note'],
                'content_type'=>$typeArray[$ext['extension']],
                'for_trainer' => $_POST['trainerStatus'],
                'content_file' => $file,
                'file_type' => $ext['extension'],
                'file_upType' => $_POST['upType'],
                'content_handbook' => $handbook,
                'last_update' => $this->full_date);

            $this->db->insert('content', $data);
        }
        $this->db->query("UPDATE client_modules SET content_status='2',content_status_date='" . $this->full_date . "' WHERE cm_id='" . $_POST['modl'] . "'");

        return 1;
    }

    public function save_module() {
        $data = array('client_id' => $_POST['cid'],
            'module_name' => $_POST['modl']);

        $this->db->insert('client_modules', $data);
        return 1;
    }

    public function update_module() {
        $data = array('module_name' => $_POST['modl']);

        $this->db->where('cm_id', $_POST['cmid']);
        $this->db->update('client_modules', $data);
        return 2;
    }

    public function get_client_module($id, $mid = NULL) {
        if (!empty($mid)) {
            $query = $this->db->query("SELECT * FROM client_modules WHERE cm_id='" . $mid . "' AND client_id='" . $id . "'");
        } else {
            $query = $this->db->query("SELECT * FROM client_modules WHERE client_id='" . $id . "'");
        }
        return $query->result();
    }

    public function get_content($id) {
        $query = $this->db->query("SELECT * FROM content WHERE module_id='" . $id . "'");
        return $query->result();
    }

    public function get_single_content($id) {
        $query = $this->db->query("SELECT * FROM content WHERE content_id='" . $id . "'");
        return $query->result();
    }

    public function update_content() {
        
        $typeArray = array('xls'=>'Spredsheet',
            'xlsx'=>'Spredsheet',
            'csv'=>'Spredsheet',
            'doc'=>'Document',
            'docx'=>'Document',
            'txt'=>'Document',
            'odt'=>'Document',
            'pdf'=>'PDF',
            'ppt'=>'Presentation',
            'pptx'=>'Presentation',
            'PPTX'=>'Presentation',
            'mp4'=>'Video',
            'mp3'=>'Video',
            'wav'=>'Video',
            'mov'=>'Video',
            'wmv'=>'Video',
            'flv'=>'Video',
            'jpg'=>'Image',
            'jpg'=>'Image',
            'png'=>'Image',
            'jpeg'=>'Image',
            'gif'=>'Image');
        
        $file = $_POST['cdoc'];
        $ext = pathinfo($file);
        $path = './assets/upload/content/';

        if ($_POST['upType'] == 1) {
            $img_name = $_FILES['doc']['name'];
            $tmp_name = $_FILES['doc']['tmp_name'];
            if (!empty($img_name)) {
                $ext = pathinfo($img_name);
                $check = $this->db->query("SELECT content_id FROM content WHERE content_file='" . $img_name . "'");
                $fCount = 0;
                if ($check->num_rows()) {
                    $result = $check->result();
                    $fCount = count($result);
                    $img_name = $ext['filename'] . "_" . $fCount . '.' . $ext['extension'];
                }

                if ($file_name = content_upload_new($path, $img_name, $tmp_name)) {
                    $file = $file_name;
                }
            }
        } else {
            $file = $_POST['fName'];
            $ext = pathinfo($file);
            if (!file_exists($path . $file)) {
                return 2;
            }
        }

        $data = array('client_id' => $_POST['client'],
            'module_id' => $_POST['modl'],
            'content_title' => $_POST['title'],
            'content_notes' => $_POST['note'],
            'content_type'=>$typeArray[$ext['extension']],
            'for_trainer' => $_POST['trainerStatus'],
            'content_file' => $file,
            'file_type' => $ext['extension'],
            'file_upType' => $_POST['upType'],
            'last_update' => $this->full_date);

        $this->db->where('content_id', $_POST['cid']);
        $this->db->update('content', $data);

        $this->db->query("UPDATE client_modules SET content_status='2' WHERE cm_id='" . $_POST['modl'] . "'");

        return 1;
    }

    public function remove_content($val) {
        $this->db->query("DELETE FROM content WHERE content_id='" . $val['cid'] . "'");
        $query = $this->db->query("SELECT content_file FROM content WHERE content_file='".$val['file']."'");
        if(!$query->num_rows()){
            unlink('./assets/upload/content/' . $val['file']);
        }
    }

    public function complete_content($val) {
        $this->db->query("UPDATE client_modules SET content_status='" . $val['status'] . "', content_status_date='" . $this->full_date . "' WHERE cm_id='" . $val['cid'] . "'");

        if ($val['status'] == '1') {
            $query = $this->db->query("SELECT u.email,u.user_code FROM client_modules m, training_projects p, application_users u WHERE m.cm_id='" . $val['cid'] . "' AND p.client_id=m.client_id AND u.user_code=p.user_code");
            $pmEmail = $query->result();

            $query1 = $this->db->query("SELECT m.module_name,c.client_name FROM client_modules m, clients c WHERE m.cm_id='" . $val['cid'] . "' AND c.client_id=m.client_id");
            $client = $query1->result();

            $eData = array('module' => $client[0]->module_name, 'client' => $client[0]->client_name);

            $note = array('user' => $pmEmail[0]->user_code,
                'project' => '',
                'task' => 0,
                'type' => 'Content Update',
                'due_date' => '',
                'text' => "CM has completed updates on Module " . $client[0]->module_name . "  for client " . $client[0]->client_name);
            $this->push_notification($note);

            content_complete_mail($pmEmail[0]->email, $eData);
        }
    }

    public function get_file_type() {
        $query = $this->db->query("SELECT file_type FROM content GROUP BY file_type");
        return $query->result();
    }

    public function search_content() {
        $filter = "";
        if ($_POST['title'] != "") {
            $filter .= " ( c.content_title LIKE '%" . $_POST['title'] . "%' || c.content_notes LIKE '%" . $_POST['title'] . "%' ) AND ";
        }

        if ($_POST['client'] != "") {
            $filter .=" c.client_id='" . $_POST['client'] . "' AND ";
        }

        if ($_POST['modl'] != "") {
            $filter .=" c.module_id='" . $_POST['modl'] . "' AND ";
        }

        if ($_POST['ftype'] != "") {
            $filter .=" c.file_type='" . $_POST['ftype'] . "' AND ";
        }

        $query = $this->db->query("SELECT c.*,m.*,cl.client_id,cl.client_name FROM content c, client_modules m, clients cl WHERE $filter m.cm_id=c.module_id AND cl.client_id=m.client_id");
        return $query->result();
    }

    public function get_recent_content() {
        $query = $this->db->query("SELECT c.*,m.*,cl.client_id,cl.client_name FROM content c, client_modules m, clients cl WHERE m.cm_id=c.module_id AND cl.client_id=m.client_id ORDER BY c.content_id DESC LIMIT 10");
        return $query->result();
    }

    public function get_incomplete() {
        $query = $this->db->query("SELECT m.*,cl.client_id,cl.client_name FROM client_modules m, clients cl WHERE  m.content_status='2' AND cl.client_id=m.client_id");
        return $query->result();
    }

    public function get_all_modules() {
        $query = $this->db->query("SELECT c.client_name,m.* FROM client_modules m,clients c WHERE c.client_id=m.client_id ORDER BY c.client_name");
        return $query->result();
    }

    public function get_notification($id) {
        $query = $this->db->query("SELECT n.*,p.project_title FROM notifications n, training_projects p WHERE n.user_id='" . $id . "' AND p.project_id=n.project_id ORDER BY n.id DESC");
        return $query->result();
    }

    public function push_notification($val) {
        $data = array('user_id' => $val['user'],
            'project_id' => $val['project'],
            'notification_text' => $val['text'],
            'todo_task' => $val['task'],
            'notification_type' => $val['type'],
            'due_date' => $val['due_date'],
            'notification_date' => $this->full_date);

        $this->db->insert('notifications', $data);
    }

    public function updateHandbookStatus($val) {
        $this->db->query("UPDATE program_content_settings SET handbook_status='1' WHERE project_id='" . $val['pid'] . "'");
        $query = $this->db->query("SELECT p.project_id,p.project_title,u.user_code,u.email FROM training_projects p,application_users u WHERE p.project_id='" . $val['pid'] . "' AND u.user_code=p.user_code");
        $result = $query->result();
        if (!empty($result)) {
            $note = array('user' => $result[0]->user_code,
                'project' => $result[0]->project_id,
                'task' => 0,
                'type' => 'Handbook Content Update',
                'due_date' => '',
                'text' => "Handbook content is updated by CM for program " . $result[0]->project_title);
            $this->push_notification($note);
        }

        $query = $this->db->query("SELECT u.user_code,u.email FROM application_users u WHERE  u.user_type='admin support'");
        $result2 = $query->result();
        if (!empty($result2)) {
            $note = array('user' => $result2[0]->user_code,
                'project' => $result[0]->project_id,
                'task' => 0,
                'type' => 'Handbook Content Update',
                'due_date' => '',
                'text' => "Handbook content is updated by CM for program " . $result[0]->project_title);
            $this->push_notification($note);
        }
    }

}
?>

