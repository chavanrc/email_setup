<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class trainer_model extends CI_Model {

    public $cdate = null;
    public $full_date = null;

    public function __construct() {
        $datestring = "%Y-%m-%d";
        $datestring1 = "%Y-%m-%d %H:%i:%s";
        $time = time();
        $this->cdate = mdate($datestring, $time);
        $this->full_date = mdate($datestring1, $time);
    }

    public function image_resize($file_name, $path, $newpath, $newWidth) {
        $img_size = getimagesize($path . $file_name);
        $newHeight = round(($newWidth / $img_size[0]) * $img_size[1]);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $file_name;
        $config['new_image'] = $newpath . $file_name;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $newWidth;
        $config['height'] = $newHeight;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    public function get_new_program() {
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.trainer_engage_flag,p.training_start_date,p.training_end_date,p.training_duration,p.is_active,p.create_date,c.client_id,c.client_name FROM training_projects p, clients c WHERE  p.training_start_date >= CURDATE() AND c.client_id=p.client_id");
        return $query->result();
    }

    public function count_new_program() {
        $query = $this->db->query("SELECT COUNT(project_id) as np FROM training_projects WHERE trainer_engage_flag='0' AND training_start_date >= CURDATE()");
        return $query->result();
    }

    public function check_request($id, $code) {
        $query = $this->db->query("SELECT project_id,status,admin_approved,reject_date FROM training_engagements WHERE user_code='" . $code . "' AND project_id='" . $id . "'");
        return $query->result();
    }

    //Email PM post engagement request
    public function trainer_enagage_request($val) {
        $data = array('project_id' => $val['pid'],
            'user_code' => $val['code'],
            'status' => '2',
            'trainer_comments' => $val['comment'],
            'apply_date' => $this->cdate,
            'admin_approved' => '0');

        $this->db->insert('training_engagements', $data);

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training,c.client_name,c.email1,c.phone1,u.email,u.user_code FROM training_projects p INNER JOIN application_users u ON p.user_code=u.user_code, clients c  WHERE p.project_id='" . $val['pid'] . "' AND c.client_id=p.client_id");
        $details = $query->result();
        $trainer = $this->get_basic_trainer($val['code']);
        $msg = array('project' => $details[0]->project_title,
            'project_date' => $details[0]->training_start_date,
            'company' => $details[0]->client_name,
            'city' => $details[0]->location_of_training,
            'email' => $details[0]->email1,
            'name' => $trainer[0]->name,
            'tcom' => $trainer[0]->company_name,
            'tcity' => $trainer[0]->city,
            'contact' => $trainer[0]->contact_number,
            'temail' => $trainer[0]->email
        );

        $note = array('user' => $details[0]->user_code,
            'project' => $val['pid'],
            'task' => 1,
            'type' => 'Trainer Engage',
            'due_date' => '',
            'text' => "Action Required for Trainer Engagement Request " . $details[0]->project_title);
        $this->push_notification($note);

        engagement_request_mail($details[0]->email, $msg);
    }

    public function get_basic_trainer($id) {
        $query = $this->db->query("SELECT name,email,company_name,city,contact_number FROM application_users WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function get_basic_program_detail($id) {
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,u.email,u.name FROM training_projects p, application_users u WHERE p.project_id='" . $id . "' AND u.user_code=p.user_code");
        return $query->result();
    }


	public function get_all_tds_certificate($id){
        $query = $this->db->query("SELECT * FROM trainer_tds WHERE trainer_id='" . $id .  "'");
        return $query->result();
    }
	
    public function get_upcoming_programs($id) {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT p.project_id,p.client_id,p.project_title,p.location_of_training,p.training_start_date,p.training_duration,c.client_name FROM program_trainers pt, training_projects p, clients c WHERE pt.trainer_id='" . $id . "' AND p.project_id=pt.project_id AND p.training_start_date>='" . $today . "' AND c.client_id=p.client_id ORDER BY p.training_start_date DESC");
        return $query->result();
    }

    public function count_upcoming_programs($id) {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT COUNT(p.project_id) as up FROM program_trainers pt, training_projects p WHERE pt.trainer_id='" . $id . "' AND p.project_id=pt.project_id AND p.training_start_date>='" . $today . "'");
        return $query->result();
    }

    public function get_past_programs($id) {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT p.project_id,p.client_id,p.project_title,p.location_of_training,p.training_start_date,p.training_duration,c.client_name, p.half_day FROM training_engagements e, training_projects p, clients c WHERE e.user_code='" . $id . "' AND e.status='1' AND e.admin_approved='1' AND p.project_id=e.project_id AND p.training_start_date <'" . $today . "' AND c.client_id=p.client_id ORDER BY training_start_date DESC");
        return $query->result();
    }

    public function count_past_programs($id) {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT COUNT(p.project_id) as pp FROM training_engagements e, training_projects p WHERE e.user_code='" . $id . "' AND e.status='1' AND e.admin_approved='1' AND p.project_id=e.project_id AND p.training_start_date<'" . $today . "' ");
        return $query->result();
    }

    public function get_trainer_program_list($id) {
        $query = $this->db->query("SELECT pt.id,p.project_id,p.project_title FROM program_trainers pt, training_projects p WHERE pt.trainer_id='" . $id . "' AND p.project_id=pt.project_id");
        return $query->result();
    }

    public function get_progrom_date($id) {
        $month = date('m');
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.excl_sun,p.excl_sat,t.training_date_from,t.trainer_date_to, p.location_of_training FROM training_projects p, program_trainers t WHERE t.trainer_id='" . $id . "' AND p.project_id=t.project_id");
//        return $this->db->last_query();
        return $query->result();
    }

    public function get_all_content($id) {
        $query = $this->db->query("SELECT c.*, u.name,u.user_type,u.user_code FROM training_content c,application_users u WHERE  u.user_code=c.upload_by AND (u.user_code='" . $id . "' || u.user_type='admin' || u.user_type='project manager') ORDER BY c.id");
        return $query->result();
    }

    public function save_content() {
        $path = './assets/upload/content/';
        $img_name = $_FILES['content']['name'];
        $tmp_name = $_FILES['content']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $project = '0';
        $client = '0';

        $data = array('filename' => $_POST['title'],
            'file_path' => $file,
            'project_id' => $project,
            'client_id' => $client,
            'upload_by' => $_POST['content_user'],
            'upload_date' => $this->cdate);

        $this->db->insert('training_content', $data);

        return 1;
    }

    public function save_payment() {
        $file = "";
        $path = './assets/upload/content/';
        $img_name = $_FILES['content']['name'];
        $tmp_name = $_FILES['content']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $data = array('project' => $_POST['program'],
            'trainer' => $_POST['tid'],
            'debit_title' => $_POST['dTitle'],
            'debit_desc' => $_POST['desc'],
            'debit_amt' => $_POST['amt'],
            'debit_file' => $file,
            'debit_date' => $this->cdate);

        $this->db->insert('debit_note', $data);

        $query3 = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training, u.name, u.email,u.user_code FROM application_users u, training_projects p WHERE p.project_id='" . $_POST['program'] . "' AND u.user_code=p.user_code");
        $project = $query3->result();

        $note = array('user' => $project[0]->user_code,
            'project' => $project[0]->project_id,
            'task' => 0,
            'type' => 'Add Expense',
            'due_date' => '',
            'text' => $_POST['amt'] . " " . $_POST['dTitle'] . " added by trainer for the program : " . $project[0]->project_title . " ");
        $this->push_notification($note);

        return 1;
    }

    public function get_payment_list($id) {
        $query = $this->db->query("SELECT pt.*,p.project_id,p.project_title, p.location_of_training FROM program_trainers pt, training_projects p WHERE pt.trainer_id='" . $id . "' AND pt.trainer_invoice_flag='1' AND p.project_id=pt.project_id ORDER BY p.training_start_date DESC");
        return $query->result();
    }

    public function get_invoice_details($tid, $pid) {
        $query = $this->db->query("SELECT * FROM debit_note WHERE project='" . $pid . "' AND trainer='" . $tid . "'");
        return $query->result();
    }

    public function get_trainer_fees($tid, $pid) {
        $query = $this->db->query("SELECT * FROM program_trainers WHERE project_id='" . $pid . "' AND trainer_id='" . $tid . "'");
        return $query->result();
    }

    public function get_client_details($pid) {
        $query = $this->db->query("SELECT p.project_id, p.project_title,p.location_of_training,p.training_start_date,p.training_end_date, c.client_name,c.company_id,p.training_duration, p.half_day FROM training_projects p, clients c WHERE p.project_id='" . $pid . "' AND c.client_id=p.client_id");
        return $query->result();
    }

    public function get_trainer_details($tid) {
        $query = $this->db->query("SELECT t.user_code,t.name,t.address, t.trainer_credit, t.city FROM application_users t WHERE t.user_code='" . $tid . "' AND user_type='trainer'");
        return $query->result();
    }

    public function get_trainer_bank($tid) {
        $query = $this->db->query("SELECT * FROM trainer_bank WHERE user_code='" . $tid . "'");
        return $query->result();
    }

    //when invoice generated by trainer email PM & Training Support
    public function confirm_invoice($val) {
        $this->db->query("UPDATE program_trainers SET trainer_invoice_flag='1', trainer_invoice_date='" . $this->cdate . "' WHERE id='" . $val['pid'] . "'");

        $query2 = $this->db->query("SELECT u.name,u.email,u.contact_number FROM program_trainers p, application_users u WHERE p.project_id='" . $val['project_id'] . "' AND u.user_code=p.trainer_id");
        $result = $query2->result();


        $trainer_name = $result[0]->name;
        $trainer_contact = $result[0]->contact_number;

        $tid = $this->session->userdata('t_code');
        
        $this->db->query("UPDATE notifications SET todo_task='0' WHERE project_id='".$val['project_id']."' AND user_id='".$tid."' AND notification_type='Invoice Generation Pending' ");

        $query3 = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training, u.name, u.email,u.user_code FROM application_users u, training_projects p WHERE p.project_id='" . $val['project_id'] . "' AND u.user_code=p.user_code");
        $project = $query3->result();
        $pm_email = $project[0]->email;
        $pm_name = $project[0]->name;

        $query3 = $this->db->query("SELECT u.name, u.email FROM application_users u  WHERE u.user_type='training support'");
        $result = $query3->result();
        $support_name = $result[0]->name;
        $support_email = $result[0]->email;

        $email = $support_email . "," . $pm_email;

        $data = array('pname' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'training_location' => $project[0]->location_of_training,
            'trainer_name' => $trainer_name,
            'trainer_contact' => $trainer_contact,
            'pmname' => $pm_name,
            'status' => 'TrainerInvoiceGenerated');

        $note = array('user' => $project[0]->user_code,
            'project' => $project[0]->project_id,
            'task' => 1,
            'type' => 'Trainer Invoice',
            'due_date' => '',
            'text' => "Invoice by trainer for the program : " . $project[0]->project_title . " has been generated, require your action");
        $this->push_notification($note);

        project_mail_pm($email, $data);
    }

    public function remove_debit_note($val) {
        $this->db->query("DELETE FROM debit_note WHERE dn_id='" . $val['did'] . "'");
    }

    //when invoice confirmed by training Support >> email PM & admin
    public function support_confirm_invoice() {
        $this->db->query("UPDATE program_trainers SET trainer_invoice_action='1',trainer_invoice_note='" . $_POST['desc'] . "',support_approve_date='" . $this->full_date . "' WHERE id='" . $_POST['pid'] . "'");

        $query = $this->db->query("SELECT * FROM debit_note WHERE project='" . $_POST['project'] . "' AND trainer='" . $_POST['trainer'] . "' AND debit_status='1'");
        if ($query->num_rows()) {
            $result = $query->result();
            foreach ($result as $rs_data) {
                $data = array('project_id' => $rs_data->project,
                    'expense_type' => $rs_data->debit_title,
                    'notes' => 'Trainer Debit : ' . $rs_data->debit_desc,
                    'amount' => $rs_data->debit_amt,
                    'in_amount' => $rs_data->debit_amt,
                    'added_by' => 'Trainer',
                    'added_date' => $this->full_date);

                $this->db->insert('program_expenses', $data);
            }
        }

        $query2 = $this->db->query("SELECT name, contact_number, trainer_credit FROM  application_users  WHERE user_code='" . $_POST['trainer'] . "'");
        $result = $query2->result();


        $trainer_name = $result[0]->name;
        $trainer_contact = $result[0]->contact_number;
        $trainer_credit = $result[0]->trainer_credit;

        

        $query3 = $this->db->query("SELECT u.name, u.email,u.user_code FROM application_users u, training_projects p WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.user_code");
        $result = $query3->result();
        $pm_email = $result[0]->email;
        $pm_name = $result[0]->name;
        $pmCode = $result[0]->user_code;

        $query3 = $this->db->query("SELECT u.name, u.email,u.user_code FROM application_users u  WHERE u.user_type='admin'");
        $result = $query3->result();
        $admin_name = $result[0]->name;
        $admin_email = $result[0]->email;
        $aCode = $result[0]->user_code;

        $query3 = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training, u.name, u.email,u.user_code FROM application_users u, training_projects p WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.user_code");
        $project = $query3->result();

        $email = $admin_email . "," . $pm_email;

        $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $project[0]->project_id . "' AND notification_type='PM Approve Invoice'");
        
        $dueDate = date('d-m-Y', strtotime($project[0]->training_start_date . ' + ' . $trainer_credit . ' days'));
        
        $data = array('project' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'company' => '',
            'location' => $project[0]->location_of_training,
            'trainer_name' => $trainer_name,
            'trainer_contact' => $trainer_contact,
            'pm_name' => $pm_name,
            'status' => 'TrainerInvoiceApproved');

        $note = array('user' => $aCode,
            'project' => $project[0]->project_id,
            'task' => 1,
            'type' => 'TS Confirm Invoice',
            'due_date' => $dueDate,
            'text' => "Invoice Generated by Trainer has been reviewed and confirmed for payment processing");
        $this->push_notification($note);

        $note = array('user' => $pmCode,
            'project' => $project[0]->project_id,
            'task' => 0,
            'type' => 'TS Confirm Invoice',
            'due_date' => $dueDate,
            'text' => "Invoice Generated by Trainer has been reviewed and confirmed for payment processing");
        $this->push_notification($note);

        project_mail_admin($email, $data);
    }

    public function get_trainer_payment($id) {
        $query = $this->db->query("SELECT * FROM trainer_payment WHERE p_id='" . $id . "'");
        return $query->result();
    }

    public function save_bills_courier() {
        $data = array('project_id' => $_POST['project'],
            'trainer_id' => $_POST['user'],
            'tc_ref' => $_POST['ref'],
            'tc_details' => $_POST['desc'],
            'tc_cdate'=>$_POST['cdate'],
            'tc_date' => $this->full_date);

        $this->db->insert('trainer_bills_courier', $data);

        $query = $this->db->query("SELECT email,user_code FROM application_users  WHERE user_type='trainer support'");
        $result = $query->result();
        
        $tName = $this->session->userdata('t_name');

        if (!empty($result)) {
            $note = array('user' => $result[0]->user_code,
                'project' => $_POST['project'],
                'task' => 0,
                'type' => 'Trainer Bills',
                'due_date' => '',
                'text' => "Trainer ".$tName." has updated courier details to claim debit note");
            $this->push_notification($note);
            
            $eData = array('title'=>'Trainer debit note courier details',
                'body'=>'Trainer '.$tName.' has updated debit note courier details.',
                'project'=>$_POST['pname'],
                'date'=>'',
                'location'=>'');
            
            mail_to_ts($result[0]->email,$eData);
        }
    }
    
    public function get_bills_courier($pid,$tid){
        $query = $this->db->query("SELECT * FROM trainer_bills_courier WHERE project_id='".$pid."' AND trainer_id='".$tid."'");
        return $query->result();
    }

    public function push_notification($val) {
        $data = array('user_id' => $val['user'],
            'project_id' => $val['project'],
            'notification_text' => $val['text'],
            'todo_task' => $val['task'],
            'notification_type' => $val['type'],
            'due_date' => $val['due_date'],
            'notification_date' => $this->full_date);

        $this->db->insert('notifications', $data);
    }
    
    public function save_trainer_gstInvoice(){
        $file = "";
        if(isset($_POST['cfile'])){
            $file = $_POST['cfile'];
        }
        $path = './assets/upload/content/';
        $img_name = $_FILES['inFile']['name'];
        $tmp_name = $_FILES['inFile']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }
        $amount = $_POST['inAmount'];
        $inNum = $_POST['inNumber'];
        
        $data = array('total_bill'=>$amount,
            'trainer_invoice_file'=>$file,
            'trainer_invoice_number'=>$inNum,
            'trainer_invoice_code'=>$_POST['stateCode'],
            'trainer_invoice_gst'=>$_POST['gstNumber']);
        
        $this->db->where('trainer_id',$_POST['trainer']);
        $this->db->where('project_id',$_POST['project']);
        $this->db->update('program_trainers',$data);
        
        $data1 = array('state_code'=>$_POST['stateCode'],
            'GST_number'=>$_POST['gstNumber']);
        
        $this->db->where('user_code',$_POST['trainer']);
        $this->db->update('application_users',$data1);
    }
    
    public function reset_invoice($val){
        $data = array('total_bill'=>'',
            'trainer_invoice_file'=>'',
            'trainer_invoice_number'=>'',
            'trainer_invoice_code'=>'',
            'trainer_invoice_gst'=>'');
        
        $this->db->where('id',$val['pid']);
        $this->db->update('program_trainers',$data);
    }

}
