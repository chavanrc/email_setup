<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class projectmanager_model extends CI_Model {

    public $cdate = null;
    public $full_date = null;

    public function __construct() {
        $datestring = "%Y-%m-%d";
        $datestring1 = "%Y-%m-%d %H:%i:%s";
        $time = time();
        $this->cdate = mdate($datestring, $time);
        $this->full_date = mdate($datestring1, $time);
    }

    public function image_resize($file_name, $path, $newpath, $newWidth) {
        $img_size = getimagesize($path . $file_name);
        $newHeight = round(($newWidth / $img_size[0]) * $img_size[1]);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $file_name;
        $config['new_image'] = $newpath . $file_name;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $newWidth;
        $config['height'] = $newHeight;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    /* Checks User exists or not */

    public function pm_login($val) {
        $this->db->where('email', $val['username']);
        $this->db->where('password', md5($val['password']));
        $this->db->where('user_type', 'project manager');
        $query = $this->db->get('application_users');
        if ($query->num_rows()) {
            $result = $query->result();
            $this->session->set_userdata('pm', $result[0]->email);
            $this->session->set_userdata('pm_name', $result[0]->name);
            $this->session->set_userdata('pm_id', $result[0]->user_id);
            $this->session->set_userdata('pm_code', $result[0]->user_code);
            $this->session->set_userdata('pm_photo', $result[0]->user_photo);
            $this->session->set_userdata('type', $result[0]->user_type);
            return 1;
        } else {
            return 0;
        }
    }
    
    public function get_basic_client() {
        $query = $this->db->query("SELECT client_id,client_name FROM clients ORDER BY client_name");
        return $query->result();
    }

    public function get_single_pm($id) {
//        $this->db->where('user_code', $id);
//        $query = $this->db->get('application_users');
        $query = $this->db->query("SELECT * FROM application_users WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function get_client($id = NULL) {
        if (empty($id)) {
            $query = $this->db->query("SELECT * FROM clients c, client_projectmanagers cp WHERE c.client_id=cp.client_id");
        } else {
            $query = $this->db->query("SELECT * FROM clients c, client_projectmanagers cp WHERE cp.pm_id='" . $id . "' AND c.client_id=cp.client_id");
        }
        return $query->result();
    }

    public function get_single_client($id) {
        $query = $this->db->query("SELECT * FROM clients WHERE 	client_id='" . $id . "'");
        return $query->result();
    }

    public function get_modules($id = NULL) {
        if (empty($id)) {
            $query = $this->db->query("SELECT module_name FROM client_modules");
        } else {
            $query = $this->db->query("SELECT cm.module_name FROM client_modules cm, training_projects tp WHERE tp.project_id='" . $id . "' AND cm.client_id=tp.client_id");
        }
        return $query->result();
    }

    public function get_client_invoice($id) {
        $filter = "";
        $cpFilter = "";
        $pFilter = "";
        if(isset($_POST['pclient'])){
            if($_POST['pclient']!='All'){
                $cpFilter .=" AND cp.client_id='".$_POST['pclient']."' ";
            }
            if(!empty($_POST['invoice'])){
                $filter .=" AND ci.ci_number='".$_POST['invoice']."' ";
            }
            if(!empty($_POST['idate'])){
                $filter .=" AND DATE(ci.ci_date)='".$_POST['idate']."' ";
            }
            if(!empty($_POST['ddate'])){
                $filter .=" AND DATE(DATE_ADD(ci.ci_date, INTERVAL c.payment_credit_days DAY))='".$_POST['ddate']."' ";
            }
            if(!empty($_POST['title'])){
                $pFilter .=" AND p.project_title LIKE '%".$_POST['title']."%' ";
            }
            if(!empty($_POST['location'])){
                $pFilter .=" AND p.location_of_training LIKE '%".$_POST['location']."%' ";
            }
            if($_POST['status']!='All'){
                if($_POST['status']=='0' || $_POST['status']=='1'){
                    $filter .=" AND ci.ci_pay_advice='".$_POST['status']."' ";
                }
                if($_POST['status']=='2'){
                    $filter .=" AND ci.ci_pay_status='0' ";
                }
                if($_POST['status']=='3'){
                    $filter .=" AND ci.ci_pay_status='1' ";
                }
            }
        }
        
        $p = 0;
        if (isset($_GET['page']) && $this->session->userdata('INVOICEQUERY')) {
            $p = $_GET['page'];
            $queryString = $this->session->userdata('INVOICEQUERY');
        } else {
            $queryString = "FROM client_projectmanagers cp, clients c, client_invoice ci INNER JOIN training_projects p ON ci.ci_project=p.project_id $pFilter WHERE cp.pm_id='" . $id . "' $cpFilter AND c.client_id=cp.client_id AND ci.ci_client=c.client_id AND ci.ci_status='1' $filter ";
            $this->session->set_userdata('INVOICEQUERY', $queryString);
        }
        $lmt = "LIMIT 20 OFFSET 0";
        if ($p > 1) {
            $p = ($p - 1) * 20;
            $lmt = "LIMIT 20 OFFSET " . $p;
        }
        
        $query = $this->db->query("SELECT ci.*,c.client_id,c.client_name,c.payment_credit_days,p.project_title, p.location_of_training $queryString ORDER BY ci.ci_id DESC $lmt");
        //return $this->db->last_query();
        return $query->result();
    }
    
    public function get_ciCount(){
        $queryString = $this->session->userdata('INVOICEQUERY');
        $query = $this->db->query("SELECT COUNT(ci.ci_id) AS total $queryString");
        return $query->result();
    }
    
    public function get_PM_payment($id) {
        $filter = "";
        $tFilter = "";
        $pFilter = "";
        
        if(isset($_POST['pclient'])){
            if($_POST['pclient']!='All'){
                $pFilter .=" AND p.client_id='".$_POST['pclient']."' ";
            }
            if(!empty($_POST['trainer'])){
                $tFilter .=" AND u.name  LIKE '%".$_POST['trainer']."%' ";
            }
            if(!empty($_POST['title'])){
                $pFilter .=" AND p.project_title LIKE '%".$_POST['title']."%' ";
            }
            if(!empty($_POST['location'])){
                $pFilter .=" AND p.location_of_training LIKE '%".$_POST['location']."%' ";
            }
            
            if($_POST['status']!='All'){
                if($_POST['status']=='1' || $_POST['status']=='2'){
                    $filter .=" AND pt.trainer_invoice_action='".$_POST['status']."' ";
                }
                if($_POST['status']=='0'){
                    $filter .=" AND ( pt.trainer_invoice_action IS NULL OR pt.trainer_invoice_action = '' OR pt.trainer_invoice_action = '0' ) ";
                }
                if($_POST['status']=='3'){
                    $filter .=" AND pt.trainer_paid_status='0' AND pt.trainer_invoice_action='1' ";
                }
                if($_POST['status']=='4'){
                    $filter .=" AND pt.trainer_paid_status='1' AND pt.trainer_invoice_action='1' ";
                }
            }
            if(!empty($_POST['ddate'])){
                $pFilter .=" AND DATE(DATE_ADD(pt.trainer_date_to, INTERVAL u.trainer_credit DAY))='".$_POST['ddate']."' ";
            }
            if(!empty($_POST['idate'])){
                $filter .=" AND DATE(pt.trainer_invoice_date)='".$_POST['idate']."' ";
            }
        }
        
        $p = 0;
        if (isset($_GET['page']) && $this->session->userdata('TINVOICEQUERY')) {
            $p = $_GET['page'];
            $queryString = $this->session->userdata('TINVOICEQUERY');
        } else {
            $queryString = "FROM program_trainers pt INNER JOIN application_users u ON  pt.trainer_id=u.user_code $tFilter, training_projects p, clients c WHERE pt.trainer_invoice_flag='1' $filter AND p.project_id=pt.project_id $pFilter AND p.user_code='".$id."' AND c.client_id = p.client_id";
            $this->session->set_userdata('TINVOICEQUERY', $queryString);
        }
        $lmt = "LIMIT 20 OFFSET 0";
        if ($p > 1) {
            $p = ($p - 1) * 20;
            $lmt = "LIMIT 20 OFFSET " . $p;
        }
        
        $query = $this->db->query("SELECT pt.*,p.project_id,p.project_title, p.training_start_date, p.location_of_training, u.user_code,u.name,u.trainer_credit, c.client_name $queryString ORDER BY p.training_start_date DESC $lmt");
        //return $this->db->last_query();
        return $query->result();
    }
    
    public function get_countPayment(){
        $queryString = $this->session->userdata('TINVOICEQUERY');
        $query = $this->db->query("SELECT COUNT(pt.id) AS total $queryString ");
        return $query->result();
    }

    public function get_trainer() {
        $query = $this->db->query("SELECT user_id,user_code,name,trainer_area,email,contact_number,state,city,create_date,is_active,admin_approved,user_photo FROM application_users WHERE user_type='trainer' AND is_active='1' ORDER BY name");
        return $query->result();
    }

    public function accept_content_request($val) {
        $data1 = array('project_id' => $val['pid'],
            'user_code' => $val['uid'],
            'status' => '1',
            'admin_approved' => '1',
            'approval_date' => $this->full_date,
            'apply_date' => $this->full_date);

        $this->db->insert('training_engagements', $data1);
        $val['rid'] = $this->db->insert_id();

        $this->db->query("UPDATE training_engagements SET status='1', admin_approved='1', approval_date='" . $this->full_date . "' WHERE id='" . $val['rid'] . "'");
        $this->db->query("UPDATE training_projects SET trainer_engage_flag='1' WHERE project_id='" . $val['pid'] . "'");

        $query = $this->db->query("SELECT client_charges FROM training_projects WHERE project_id='" . $val['pid'] . "'");
        $result = $query->result();
        $client_amt = $result[0]->client_charges;


        $data = array('project_id' => $val['pid'],
            'trainer_id' => $val['uid'],
            'training_date_from' => $val['from'],
            'trainer_date_to' => $val['to'],
            'amount' => $val['amt'],
            'in_amount' => $client_amt);

        $this->db->insert('program_trainers', $data);
        $ptid = $this->db->insert_id();

        $this->db->query("UPDATE program_trainers SET trainer_date_to=DATE_ADD(training_date_from, INTERVAL " . $val['duration'] . " DAY) WHERE id='" . $ptid . "'");

        $data = array('project_id' => $val['pid'],
            'trainer_id' => $val['uid'],
            'trainer_brief' => '',
            'ttt_required' => '',
            'program_date' => $val['from']);

        $this->db->insert('trainer_assigned', $data);


        $trainer = $this->get_basic_trainer($val['uid']);

        $tMobile = $trainer[0]->contact_number;
        $tCode = $trainer[0]->user_code;

        $program = $this->get_basic_program_detail($val['pid']);



        $pmQuery = $this->db->query("SELECT u.name,u.contact_number FROM training_projects p,application_users u WHERE p.project_id='" . $program[0]->project_id . "' AND u.user_code=p.user_code");
        $pmResult = $pmQuery->result();
        $dates = $val['from'];

        $eData = array('title' => 'Content Development Assigned',
            'body' => 'Content Development  for program  is assigned. <br /> 3.	Login to your trainer portal regularly till the completion of the program to get regular updates. <br/> 4.	Any update / alterations to the training program in the system will be notified by email kindly ensure you are connected to your emails all the time.',
            'project' => $program[0]->project_title,
            'date' => $dates,
            'fees' => $val['amt'],
            'trainer' => $trainer[0]->user_code,
            'pid' => $ptid,
            'pm' => $pmResult[0]->name,
            'mobile' => $pmResult[0]->contact_number,
            'location' => $program[0]->location_of_training);

       // mail_to_trainer($trainer[0]->email, $eData);
        // mail_to_trainer('anil@eneblur.com', $eData);
    }

    public function save_pcontent_setting($pid) {

        $preTest = 0;
        $content_under_update = "N";
        if (isset($_POST['pre_test'])) {
            $preTest = 1;
        }
        $postTest = 0;
        if (isset($_POST['post_test'])) {
            $postTest = 1;
        }$Test = 0;
        if (isset($_POST['physical_paper_test'])) {
            $Test = 1;
        }
        if (isset($_POST['content_under_update'])) {
            $content_under_update = "Y";
        }

        $handbook_content = "";
        $handbook_qnt = "";
        $handbook_charge = "";
        $handbook_cost = "";
        $handbook_desc = "";
        $handbook_courier = "";
        $content_cost = "";
        $handbook_status = 0;

        if ($_POST['participant_handbook'] == 'Print + Dispatch By Wagons') {
            $handbook_content = $_POST['handbook_content'];
            $handbook_qnt = $_POST['handbook_qnt'];
            $handbook_charge = $_POST['handbook_charge'];

            $handbook_desc = $_POST['handbook_desc'];
            $handbook_courier = $_POST['handbook_courier'];
        }

        if ($handbook_content == 'Already exist') {
            $handbook_status = 1;
        }

        if ($handbook_charge == 'Bill Client') {
            $handbook_cost = $_POST['handbook_cost'];
        }

        if ($_POST['content_charges'] == 'Bill Client') {
            $content_cost = $_POST['content_cost'];
        }

        $ucodequery = $this->db->query("SELECT user_code FROM training_projects WHERE project_id='" . $pid . "'");
        $rs = $ucodequery->result();

        $data = array('project_id' => $pid,
            'pre_test' => $preTest,
            'post_test' => $postTest,
            'content_preperation' => $_POST['content_preperation'],
            'participant_handbook' => $_POST['participant_handbook'],
            'content_charges' => $_POST['content_charges'],
            'physical_paper_test' => $Test,
            'module' => $_POST['content_module'],
            'content_under_update' => $content_under_update,
            'user_code' => $rs[0]->user_code,
            'handbook_content' => $handbook_content,
            'handbook_desc' => $handbook_desc,
            'handbook_qnt' => $handbook_qnt,
            'handbook_charge' => $handbook_charge,
            'handbook_cost' => $handbook_cost,
            'handbook_courier' => $handbook_courier,
            'handbook_status' => $handbook_status,
            'content_cost' => $content_cost,
        );

        $check = $this->db->query("SELECT * FROM program_content_settings WHERE project_id='" . $pid . "'");
        if ($check->num_rows()) {
            $this->db->where('project_id', $pid);
            $this->db->update('program_content_settings', $data);
            $mailSubject = "Training Program Recently Updated";
        } else {
            $this->db->insert('program_content_settings', $data);
            $mailSubject = "New Training Program Created";
        }

        $query = $this->db->query("SELECT p.project_title,p.project_id,p.location_of_training,p.training_start_date,p.stay_arrangement,p.travel_arrangement,p.trainer_engage_flag,c.client_name FROM training_projects p, clients c WHERE p.project_id='" . $pid . "' AND c.client_id=p.client_id");
        $project = $query->result();

        $query1 = $this->db->query("SELECT u.user_code,u.email FROM training_projects p, application_users u WHERE  p.project_id='" . $pid . "' AND u.user_code=p.user_code");
        $pmEmail = $query1->result();

        $query2 = $this->db->query("SELECT email,user_code FROM application_users WHERE user_type='content manager'");
        $cmEmail = $query2->result();

        $query3 = $this->db->query("SELECT email,user_code FROM application_users WHERE user_type='admin support'");
        $asEmail = $query3->result();



        $eData = array('module' => $_POST['content_module'],
            'content_preperation' => $_POST['content_preperation'],
            'handbook' => $_POST['participant_handbook'],
            'project' => $project[0]->project_title,
            'date' => $project[0]->training_start_date,
            'city' => $project[0]->location_of_training,
            'company' => $project[0]->client_name);

        $eMail = $pmEmail[0]->email;

       // content_setting_mail($eMail, $eData);

        if ($_POST['participant_handbook'] != 'Not Required') {
            $dDate = date('Y-m-d', (strtotime('-3 day', strtotime($project[0]->training_start_date))));
            $note = array('user' => $asEmail[0]->user_code,
                'project' => $pid,
                'task' => 1,
                'type' => 'AS Content',
                'due_date' => $dDate,
                'text' => "Participant Handbook Requires to be Dispatched to " . $project[0]->location_of_training);
            $this->push_notification($note);

            $eData = array('title' => 'Participant handbook required',
                'body' => $handbook_qnt . ' Participant handbook required for following progrom. Login to your account for more details',
                'project' => $project[0]->project_title,
                'date' => $project[0]->training_start_date,
                'location' => $project[0]->location_of_training);

          //  mail_to_as($asEmail[0]->email, $eData);
        }

        $lData = array('u_id' => $rs[0]->user_code,
            'al_user' => '',
            'project_id' => $pid,
            'al_title' => 'Updated content setting',
            'al_date' => $this->full_date);

        $this->db->insert('activity_log', $lData);

        $mailTitle = "New Program (" . $project[0]->project_title . ", " . date_formate_short($project[0]->training_start_date) . "," . $project[0]->client_name . ")  Below is a summary of activities you will require to monitor as part of operations.";
        $mailBody = "";
        if ($project[0]->trainer_engage_flag == '0') {
            $mailBody .="Trainer Engagement is pending <br/>";
        }
        if ($project[0]->stay_arrangement == 'Wagons') {
            $mailBody .="Stay Booking Pending <br/>";
        }
        if ($project[0]->travel_arrangement == 'Wagons') {
            $mailBody .="Travel Booking Pending <br/>";
        }
        $mailBody .="Venue & SPOC Update Pending <br/>";
        $mailBody .="TTT Schedule Input Pending <br/>";
        $mailBody .="Content Mapping Pending <br/>";
        if ($handbook_content == 'Prepare / Update By CM') {
            $mailBody .="Participant Handbook Content Pending <br/>";
        }
        if ($_POST['participant_handbook'] == 'Print + Dispatch By Wagons') {
            $mailBody .="Participant Handbook Shipment Pending <br/>";
        }
        if ($_POST['content_preperation'] == 'Prepare by CM') {
            $mailBody .="Content Preperation by CM <br/>";
        }

        $mailData = array('sub' => $mailSubject,
            'title' => $mailTitle,
            'body' => $mailBody,
            'project' => $project[0]->project_title,
            'location' => $project[0]->location_of_training,
            'date' => $project[0]->training_start_date);

      //  new_program_task($pmEmail[0]->email, $mailData);
    }

    public function save_content_program() {
        $excl_sat = "N";
        $excl_sun = "N";
        $invoice_status = '1';
        $halfday = '0';
        if (isset($_POST['halfday'])) {
            $halfday = '1';
        }

        $queryU = $this->db->query("SELECT pm_id FROM client_projectmanagers WHERE client_id='" . $_POST['client'] . "'");
        $rs = $queryU->result();
        $ucode = $rs[0]->pm_id;

        $data = array('user_code' => $ucode,
            'client_id' => $_POST['client'],
            'project_title' => $_POST['pname'],
            'location_of_training' => $_POST['location'],
            'training_start_date' => $_POST['sdate'],
            'training_duration' => $_POST['duration'],
            'create_date' => $this->full_date,
            'excl_sat' => $excl_sat,
            'excl_sun' => $excl_sun,
            'participant_level' => $_POST['participant_level'],
            'client_charges' => $_POST['client_charges'],
            'trainer_engage_flag' => '1',
            'props_delivery_status' => '0',
            'is_active' => '1',
            'half_day' => $halfday,
            'invoice_status' => $invoice_status,
            'project_type' => '1'
        );

        $this->db->insert('training_projects', $data);

        $id = $this->db->insert_id();
        $duration = $_POST['duration'] - 1;
        $this->db->query("UPDATE training_projects SET training_end_date=DATE_ADD(training_start_date, INTERVAL $duration DAY) WHERE project_id='" . $id . "'");

        $this->session->set_flashdata('new_program', 'true');
        if (!empty($_POST['trainer'])) {
            $cdata = array('pid' => $id, 'uid' => $_POST['trainer'], 'amt' => $_POST['fees'], 'from' => $_POST['sdate'], 'to' => $_POST['sdate'], 'duration' => $duration);
            $this->accept_content_request($cdata);
        }
        $this->save_pcontent_setting($id);
        return $id;
    }

    public function update_content_program() {
        $halfday = '0';
        if (isset($_POST['halfday'])) {
            $halfday = '1';
        }


        $data = array('client_id' => $_POST['client'],
            'project_title' => $_POST['pname'],
            'location_of_training' => $_POST['location'],
            'training_start_date' => $_POST['sdate'],
            'training_duration' => $_POST['duration'],
            'client_charges' => $_POST['client_charges'],
            'create_date' => $this->cdate,
            'participant_level' => $_POST['participant_level'],
            'half_day' => $halfday
        );

        $this->db->where('project_id', $_POST['pid']);
        $this->db->update('training_projects', $data);
    }

    public function get_cp_program($id) {
        $query = $this->db->query("SELECT p.project_id,p.participant_level,p.half_day,p.client_charges,p.training_duration,p.client_id,p.project_title,p.location_of_training,p.training_start_date FROM training_projects p WHERE p.project_id='" . $id . "'");
        return $query->result();
    }

    public function save_program() {
        $excl_sat = "N";
        $excl_sun = "N";
        $invoice_status = '1';
        $halfday = '0';
        if (isset($_POST['halfday'])) {
            $halfday = '1';
        }
        if (isset($_POST['invoice_status'])) {
            $invoice_status = '0';
        }
        if (isset($_POST['excl_sat']))
            $excl_sat = "Y";
        if (isset($_POST['excl_sun']))
            $excl_sun = "Y";

        $queryU = $this->db->query("SELECT pm_id FROM client_projectmanagers WHERE client_id='" . $_POST['client'] . "'");
        $rs = $queryU->result();
        $ucode = $rs[0]->pm_id;

        $data = array('user_code' => $ucode,
            'client_id' => $_POST['client'],
            'project_title' => $_POST['pname'],
            'location_of_training' => $_POST['location'],
            'objective_of_training' => $_POST['objective'],
            'training_start_date' => $_POST['sdate'],
            'training_duration' => $_POST['duration'],
            'trainer_skillsets' => $_POST['skill_set'],
            'no_of_participants' => $_POST['participants'],
            'create_date' => $this->cdate,
            'zone' => $_POST['zone'],
            'client_name' => $_POST['client_name'],
            'excl_sat' => $excl_sat,
            'excl_sun' => $excl_sun,
            'program_state' => $_POST['program_state'],
            'participants_type' => $_POST['participants_type'],
            'participant_level' => $_POST['participant_level'],
            'trainer_engage_flag' => '0',
            'props_delivery_status' => '0',
            'is_active' => '1',
            'half_day' => $halfday,
            'invoice_status' => $invoice_status
        );

        $this->db->insert('training_projects', $data);

        $id = $this->db->insert_id();
        $duration = $_POST['duration'] - 1;
        $this->db->query("UPDATE training_projects SET training_end_date=DATE_ADD(training_start_date, INTERVAL $duration DAY) WHERE project_id='" . $id . "'");

        if (isset($_POST['sendNoti'])) {
            $query2 = $this->db->query("SELECT user_code,email FROM application_users WHERE state='" . $_POST['program_state'] . "'");
            $result2 = $query2->result();
            if (!empty($result2)) {
                foreach ($result2 as $rs) {
                    $note = array('user' => $rs->user_code,
                        'project' => $id,
                        'task' => 0,
                        'type' => 'New Program',
                        'due_date' => '',
                        'text' => "New " . $_POST['participants_type'] . " Program open for engagement for  date: " . $_POST['sdate']);
                    $this->push_notification($note);

                    $data2 = array('project_id' => $id,
                        'project_type' => $_POST['participants_type'],
                        'project_date' => $_POST['sdate'],
                        'email_id' => $rs->email,
                        'email_status' => 0);

                    $this->db->insert('email_notifications', $data2);
                }
            }
        }

        $lData = array('u_id' => $ucode,
            'al_user' => '',
            'project_id' => $id,
            'al_title' => 'New Program created',
            'al_date' => $this->full_date);

        $this->db->insert('activity_log', $lData);

        $this->session->set_flashdata('new_program', 'true');
        return $id;
    }

    public function update1_program() {
        $data = array(
            'create_date' => $this->cdate,
            'props' => $_POST['prop'],
            'stay_arrangement' => $_POST['stay'],
            'travel_arrangement' => $_POST['travel'],
            'travel_limit' => $_POST['travel_limit'],
            'stay_limit' => $_POST['stay_limit'],
            'claim_travel_cost' => $_POST['claim_travel_cost'],
            'claim_stay_cost' => $_POST['claim_stay_cost'],
            'client_charges' => $_POST['client_charges'],
            'trainer_fee_range' => $_POST['trainer_fee_range']
        );
        $this->db->where('project_id', $_POST['pid']);
        $this->db->update('training_projects', $data);

        $query1 = $this->db->query("SELECT project_title,participants_type,training_start_date FROM training_projects WHERE project_id='" . $_POST['pid'] . "'");
        $pResult = $query1->result();

        if (isset($_POST['other_state'])) {
            foreach ($_POST['other_state'] as $os_data) {
                $data = array('project' => $_POST['pid'], 'state' => $os_data);
                $this->db->insert('notification_state', $data);

                $query2 = $this->db->query("SELECT user_code,email FROM application_users WHERE state='" . $os_data . "'");
                $result2 = $query2->result();
                if (!empty($result2)) {
                    foreach ($result2 as $rs) {
                        $note = array('user' => $rs->user_code,
                            'project' => $_POST['pid'],
                            'task' => 0,
                            'type' => 'New Program',
                            'due_date' => '',
                            'text' => "New " . $pResult[0]->participants_type . " Program open for engagement for  date: " . $pResult[0]->training_start_date);
                        $this->push_notification($note);

                        $data2 = array('project_id' => $_POST['pid'],
                            'project_type' => $pResult[0]->participants_type,
                            'project_date' => $pResult[0]->training_start_date,
                            'email_id' => $rs->email,
                            'email_status' => 0);

                        $this->db->insert('email_notifications', $data2);
                    }
                }
            }
        }

        return $_POST['pid'];
    }

    public function update_program() {

        $excl_sat = "N";
        $excl_sun = "N";
        $invoice_status = '1';
        $halfday = '0';
        if (isset($_POST['halfday'])) {
            $halfday = '1';
        }
        if (isset($_POST['invoice_status'])) {
            $invoice_status = '0';
        }
        if (isset($_POST['excl_sat']))
            $excl_sat = "Y";
        if (isset($_POST['excl_sun']))
            $excl_sun = "Y";

        $data = array('user_code' => $this->session->userdata('pm_code'),
            'client_id' => $_POST['client'],
            'project_title' => $_POST['pname'],
            'location_of_training' => $_POST['location'],
            'objective_of_training' => $_POST['objective'],
            'training_start_date' => $_POST['sdate'],
            'training_duration' => $_POST['duration'],
            'trainer_skillsets' => $_POST['skill_set'],
            'no_of_participants' => $_POST['participants'],
            'create_date' => $this->cdate,
            'participant_level' => $_POST['participant_level'],
            'props' => $_POST['prop'],
            'stay_arrangement' => $_POST['stay'],
            'travel_arrangement' => $_POST['travel'],
            'zone' => $_POST['zone'],
            'excl_sat' => $excl_sat,
            'excl_sun' => $excl_sun,
            'participants_type' => $_POST['participants_type'],
            'program_state' => $_POST['program-state'],
            'travel_limit' => $_POST['travel_limit'],
            'stay_limit' => $_POST['stay_limit'],
            'claim_travel_cost' => $_POST['claim_travel_cost'],
            'claim_stay_cost' => $_POST['claim_stay_cost'],
            'trainer_fee_range' => $_POST['trainer_fee_range'],
            'client_charges' => $_POST['client_charges'],
            'is_active' => $_POST['pstatus'],
            'client_name' => $_POST['client_name'],
            'half_day' => $halfday,
            'invoice_status' => $invoice_status
        );
        $this->db->where('project_id', $_POST['pid']);
        $this->db->update('training_projects', $data);

        $duration = $_POST['duration'] - 1;
        $this->db->query("UPDATE training_projects SET training_end_date=DATE_ADD(training_start_date, INTERVAL $duration DAY) WHERE project_id='" . $_POST['pid'] . "'");

        $query = $this->db->query("SELECT u.email FROM program_trainers pt,application_users u WHERE pt.project_id='" . $_POST['pid'] . "' AND u.user_code=pt.trainer_id");
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $r_data) {
                $eTitle = "Program Updation";
                $eBody = "Program you engaged is updated, please login and check the details";
                if ($_POST['pstatus'] == '0') {
                    $eTitle = "Program Canceled";
                    $eBody = "Program you engaged is Canceled by project manager.";
                }
                $eData = array('title' => $eTitle,
                    'body' => $eBody,
                    'project' => $_POST['pname'],
                    'date' => $_POST['sdate'],
                    'location' => $_POST['location']);
             //   mail_to_trainer($r_data->email, $eData);
            }
        }

        $lData = array('u_id' => $this->session->userdata('pm_code'),
            'al_user' => $this->session->userdata('pm_name'),
            'project_id' => $_POST['pid'],
            'al_title' => 'Program is updated',
            'al_date' => $this->full_date);

        $this->db->insert('activity_log', $lData);

        return $_POST['pid'];
    }

    public function cancelProgram($val) {
        $this->db->query("UPDATE training_projects SET is_active='0',trainer_engage_flag='0' WHERE project_id='" . $val['pid'] . "'");
        $query = $this->db->query("SELECT u.email FROM program_trainers pt,application_users u WHERE pt.project_id='" . $val['pid'] . "' AND u.user_code=pt.trainer_id");
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $r_data) {
                $eData = array('title' => 'Program Canceled',
                    'body' => 'Program you engaged is Canceled by project manager.',
                    'project' => $_POST['pname'],
                    'date' => $_POST['sdate'],
                    'location' => $_POST['location']);
              //  mail_to_trainer($r_data->email, $eData);
            }
        }

        $this->db->query("DELETE FROM program_trainers WHERE project_id='" . $val['pid'] . "'");
        $this->db->query("DELETE FROM trainer_assigned WHERE project_id='" . $val['pid'] . "'");
        $this->db->query("DELETE FROM training_engagements WHERE project_id='" . $val['pid'] . "'");

        $query = $this->db->query("SELECT u.email FROM training_projects p,application_users u WHERE p.project_id='" . $val['pid'] . "' AND u.user_code=p.user_code");
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $r_data) {
                $eData = array('title' => 'Program Canceled',
                    'body' => 'Program you engaged is Canceled by project manager.',
                    'project' => $_POST['pname'],
                    'date' => $_POST['sdate'],
                    'location' => $_POST['location']);
             //   mail_to_trainer($r_data->email, $eData);
            }
        }

        $eData = array('title' => 'Program Canceled',
            'body' => 'Program you engaged is Canceled by project manager.',
            'project' => $_POST['pname'],
            'date' => $_POST['sdate'],
            'location' => $_POST['location']);
      //  mail_to_trainer('ashish@wagonslearning.com', $eData);
    }

    public function activeProgram($val) {
        $this->db->query("UPDATE training_projects SET is_active='1' WHERE project_id='" . $val['pid'] . "'");
    }

    public function import_program() {

        $path = './assets/upload/content/';
        $img_name = $_FILES['content']['name'];
        $tmp_name = $_FILES['content']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $Reader = new SpreadsheetReader($path . $file_name);
                $sheetCount = count($Reader->sheets());
                for ($i = 0; $i < $sheetCount; $i++) {

                    $Reader->ChangeSheet($i);
                    foreach ($Reader as $Row) {
                        if ($Row[0] != 'Date of Program') {
                            $sdate = strtotime($Row[0]);
                            $data = array('user_code' => $this->session->userdata('pm_code'),
                                'client_id' => $_POST['client'],
                                'project_title' => $Row[3],
                                'location_of_training' => $Row[1],
                                'training_start_date' => date("Y-m-d", $sdate),
                                'training_duration' => '1',
                                'no_of_participants' => $Row[4],
                                'is_active' => '0',
                                'create_date' => $this->cdate,
                                'mail_flag' => '0',
                                'trainer_engage_flag' => '0',
                                'props' => $Row[5],
                                'props_delivery_status' => '0',
                                'stay_arrangement' => $Row[6],
                                'travel_arrangement' => $Row[7],
                                'program_state' => $Row[2],
                            );

                            $this->db->insert('training_projects', $data);
                        }
                    }
                }
            }
        }
    }

    public function get_single_program($id) {
        $query = $this->db->query("SELECT p.*,c.*, u.name, u.contact_number, DATEDIFF(p.training_start_date, current_date) as diff, p.client_name as part_name FROM training_projects p, clients c, application_users u WHERE p.project_id='" . $id . "' AND c.client_id=p.client_id AND u.user_code=p.user_code");
        return $query->result();
    }

    public function get_notification_state($id) {
        $query = $this->db->query("SELECT * FROM notification_state WHERE project='" . $id . "'");
        return $query->result();
    }

    public function get_basic_programs($id) {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,p.training_end_date,p.trainer_engage_flag,p.is_active,p.create_date,p.stay_arrangement,p.claim_stay_cost,p.travel_arrangement,p.claim_travel_cost,p.venue,c.client_id,c.client_name, p.half_day,pc.module,pt.trainer_content_pass,pt.content_download_date FROM training_projects p LEFT JOIN program_content_settings pc ON p.project_id=pc.project_id LEFT JOIN program_trainers pt ON p.project_id=pt.project_id, clients c WHERE p.user_code='" . $id . "' AND p.training_start_date>='" . $today . "' AND c.client_id=p.client_id ORDER BY training_start_date ASC");
        return $query->result();
    }

    public function count_engagement_request($id) {
        $query = $this->db->query("SELECT COUNT(project_id) as total FROM training_engagements WHERE project_id='" . $id . "'");
        $result = $query->result();
        return $result[0]->total;
    }

    public function get_past_programs($id) {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,p.training_end_date,p.is_active,p.create_date,c.client_id,c.client_name FROM training_projects p, clients c WHERE p.user_code='" . $id . "' AND p.training_start_date<'" . $today . "' AND c.client_id=p.client_id ORDER BY p.training_start_date DESC");
        return $query->result();
    }

    public function get_basic_past_programs($id) {
        $today = date('Y-m-d');
        $filter = "";
        $filter1 = " training_projects p LEFT JOIN program_trainers t ON p.project_id=t.project_id";
        if (isset($_POST['pclient'])) {
            if ($_POST['pclient'] != 'All') {
                $filter .=" AND p.client_id='" . $_POST['pclient'] . "' ";
            }
            if (!empty($_POST['title'])) {
                $filter .=" AND p.project_title LIKE '%" . $_POST['title'] . "%' ";
            }
            if (!empty($_POST['location'])) {
                $filter .=" AND p.location_of_training LIKE '%" . $_POST['location'] . "%' ";
            }
            if (!empty($_POST['date'])) {
                $filter .=" AND DATE(p.training_start_date)='" . $_POST['date'] . "' ";
            }
            if ($_POST['status'] != "") {
                $filter .=" AND p.is_active='" . $_POST['status'] . "' ";
            }
            if ($_POST['cinvoice'] != "All") {
                $filter .=" AND p.client_invoice_raised='" . $_POST['cinvoice'] . "' ";
            }
            if ($_POST['tinvoice'] == '0') {
                $filter1 = " training_projects p INNER JOIN program_trainers t ON p.project_id=t.project_id AND t.trainer_invoice_flag !='1' ";
            }
            if ($_POST['tinvoice'] == '1') {
                $filter1 = " training_projects p INNER JOIN program_trainers t ON p.project_id=t.project_id AND t.trainer_invoice_flag ='1' ";
            }
        }

        $p = 0;
        if (isset($_GET['page']) && $this->session->userdata('PASTQUERY')) {
            $p = $_GET['page'];
            $queryString = $this->session->userdata('PASTQUERY');
        } else {
            $queryString = "FROM  $filter1, clients c WHERE p.training_start_date<'" . $today . "' $filter AND p.user_code='" . $id . "' AND c.client_id=p.client_id";
            $this->session->set_userdata('PASTQUERY', $queryString);
        }
        $lmt = "LIMIT 20 OFFSET 0";
        if ($p > 1) {
            $p = ($p - 1) * 20;
            $lmt = "LIMIT 20 OFFSET " . $p;
        }

        $query = $this->db->query("SELECT p.project_id,p.user_code,p.project_title,p.location_of_training,p.training_start_date,p.training_end_date,p.project_type,p.client_invoice_raised,p.is_active, CASE p.is_active WHEN 0 THEN 'Cancelled' WHEN 2 THEN 'Disabled' ELSE 'Complete' END AS pstatus, p.create_date,c.client_id,c.client_name,t.trainer_id,t.trainer_paid_status,t.trainer_invoice_action, t.trainer_invoice_flag $queryString  ORDER BY p.training_start_date DESC $lmt");
        return $query->result();
    }

    public function getPastCount() {
        $queryString = $this->session->userdata('PASTQUERY');
        $query = $this->db->query("SELECT COUNT(p.project_id) as total $queryString");
        return $query->result();
    }

    public function save_content() {
        $path = './assets/upload/content/';
        $img_name = $_FILES['content']['name'];
        $tmp_name = $_FILES['content']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $query1 = $this->db->query("SELECT u.email FROM application_users u, training_projects p WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.user_code");
        $pmEmail = $query1->result();

        $project = $_POST['project'];
        $client = $_POST['client'];

        if ($_POST['ctype'] == 'project') {
            $client = '0';
        }
        if ($_POST['ctype'] == 'client') {
            $project = '0';
        }

        $data = array('filename' => $_POST['title'],
            'file_path' => $file,
            'project_id' => $project,
            'client_id' => $client,
            'upload_by' => $_POST['content_user'],
            'upload_date' => $this->cdate);

        $this->db->insert('training_content', $data);

        if ($_POST['ctype'] == 'project') {

            $query3 = $this->db->query("SELECT p.project_title,p.project_id,p.location_of_training,p.training_start_date,c.client_name FROM training_projects p, clients c WHERE p.project_id='" . $_POST['project'] . "' AND c.client_id=p.client_id");
            $project = $query3->result();

            $query2 = $this->db->query("SELECT u.name,u.email FROM program_trainers p, application_users u WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.trainer_id");
            $result = $query2->result();
            $email = '';
            foreach ($result as $da) {
                $email .=$da->email . ',';
            }
            $email .= $pmEmail[0]->email;
            $email = rtrim($email, ',');

            $eData = array('project' => $project[0]->project_title,
                'start_date' => $project[0]->training_start_date,
                'company' => $project[0]->client_name);

          //  content_map_mail($email, $eData);
        }

        return $project;
    }

    public function get_project_content($id, $cid = NULL) {
        $query = $this->db->query("SELECT tc.*, u.name, u.user_type, u.user_code FROM training_content tc, application_users u  WHERE tc.project_id='" . $id . "'  AND u.user_code=tc.upload_by");
        return $query->result();
    }

    public function save_form() {
        $path = './assets/upload/form/';
        $img_name = $_FILES['content']['name'];
        $tmp_name = $_FILES['content']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = content_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $data = array(
            'form_file_name' => $file,
            'project_id' => $_POST['project'],
            'form_type' => $_POST['ftype'],
            'program_date' => $_POST['fdate'],
            'comments' => $_POST['comments'],
            'upload_by' => $_POST['form_user'],
            'upload_date' => $this->cdate);

        $this->db->insert('program_forms', $data);

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,c.client_name,u.email,u.user_code FROM training_projects p INNER JOIN application_users u ON p.user_code=u.user_code, clients c  WHERE p.project_id='" . $_POST['project'] . "' AND c.client_id=p.client_id");
        $project = $query->result();



        $email = $project[0]->email;
        $pmCode = $project[0]->user_code;

        $tsQuery = $this->db->query("SELECT user_code FROM application_users WHERE user_type='training support'");
        $tsResult = $tsQuery->result();

        $tsCode = $tsResult[0]->user_code;

        $data = array('project' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'company' => $project[0]->client_name,
            'type' => $_POST['ftype'],
            'link' => $file);

        if ($_POST['ftype'] == 'Atendance Form') {
            $note = array('user' => $pmCode,
                'project' => $project[0]->project_id,
                'task' => 0,
                'type' => 'Attendance Sheet',
                'due_date' => '',
                'text' => "Trainer has uploaded Attendance sheet for the program : " . $project[0]->project_title);
            $this->push_notification($note);

            $note = array('user' => $tsCode,
                'project' => $project[0]->project_id,
                'task' => 0,
                'type' => 'Attendance Sheet',
                'due_date' => '',
                'text' => "Trainer has uploaded Attendance sheet for the program : " . $project[0]->project_title);
            $this->push_notification($note);
        }

        if ($_POST['ftype'] == 'Feedback Form') {
            $note = array('user' => $pmCode,
                'project' => $project[0]->project_id,
                'task' => 0,
                'type' => 'Feedback Form',
                'due_date' => '',
                'text' => "Trainer has uploaded feedback form for the program : " . $project[0]->project_title);
            $this->push_notification($note);

            $note = array('user' => $tsCode,
                'project' => $project[0]->project_id,
                'task' => 0,
                'type' => 'Feedback Form',
                'due_date' => '',
                'text' => "Trainer has uploaded feedback form for the program : " . $project[0]->project_title);
            $this->push_notification($note);
        }

      //  form_attach_mail($email, $data);

        return 'Form';
    }

    public function get_project_form($id) {
        $query = $this->db->query("SELECT tf.*, u.name, u.user_type, u.user_code FROM program_forms tf, application_users u  WHERE tf.project_id='" . $id . "'  AND u.user_code=tf.upload_by");
        return $query->result();
    }

    public function save_images() {
        $photo = "";

        $path = '././App/assets/upload/';
        $img_name = $_FILES['img']['name'];
        $tmp_name = $_FILES['img']['tmp_name'];
        $img_size = count($img_name);
        for ($i = 0; $i < $img_size; $i++) {
            if ($file_name = image_upload($path, $img_name[$i], $tmp_name[$i])) {
                $photo = $file_name;

                $data = array(
                    'project_id' => $_POST['project'],
                    'ti_img' => $photo,
                    'trainer_id' => $_POST['img_user'],
                    'ti_date' => $this->cdate);

                $this->db->insert('trainer_img', $data);
            }
        }

        return 'Image';
    }

    public function get_project_images($id) {
        $query = $this->db->query("SELECT * FROM trainer_img WHERE project_id='" . $id . "'");
        return $query->result();
    }

    public function remove_img($val) {
        $this->db->query("DELETE FROM trainer_img WHERE ti_id='" . $val['rid'] . "'");
        $filename = '././App/assets/upload/' . $val['iname'];
        unlink($filename);
    }

    public function remove_props($val) {
        $this->db->query("DELETE FROM program_props WHERE id='" . $val['rid'] . "'");
    }

    public function get_project_props($id) {
        $query = $this->db->query("SELECT p.*, u.name, u.user_type, u.user_code FROM program_props p, application_users u  WHERE p.project_id='" . $id . "'  AND u.user_code=p.requested_by");
        return $query->result();
    }

    public function get_pending_props($id) {
        $query = $this->db->query("SELECT count(p.id) as cnt FROM program_props p WHERE p.project_id='" . $id . "'  AND p.delivery_status=0");
        return $query->result();
    }

    public function get_requests($id, $code = NULL) {
        $query = $this->db->query("SELECT r.*, u.name, u.user_code, u.email, u.contact_number, tp.training_start_date,tp.training_duration,tp.excl_sat FROM training_engagements r INNER JOIN training_projects tp ON r.project_id=tp.project_id, application_users u WHERE r.project_id='" . $id . "' AND u.user_code=r.user_code");
        return $query->result();
    }

    public function accept_request($val) {
        $this->db->query("UPDATE training_engagements SET status='1', engage_date='" . $this->cdate . "', admin_comments='" . $val['comment'] . "' WHERE id='" . $val['rid'] . "'");
        $trainer = $this->get_basic_trainer($val['uid']);
        $program = $this->get_basic_program_detail($val['rid']);
        $msg = array('pname' => $program[0]->project_title, 'pdate' => $program[0]->training_start_date, 'ploc' => $program[0]->location_of_training, 'email' => $trainer[0]->email, 'name' => $trainer[0]->name, 'status' => 'Rejected');
        $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $program[0]->project_id . "' AND notification_type='Trainer Engage'");
//        $note = array('user' => $trainer[0]->user_code,
//            'project' => $program[0]->project_id,
//            'task' => 0,
//            'type' => 'Accept Request',
//            'due_date' => '',
//            'text' => "Your Engagement Request for " . $program[0]->project_title . " was Approved and You have been selected to conduct the program on " . date_formate_short($program[0]->training_start_date) . ".");
//        $this->push_notification($note);

    //    reject_status_mail($trainer[0]->email, $msg);
    }

    public function reject_request($val) {
        $this->db->query("UPDATE training_engagements SET status='0', reject_date='" . $this->cdate . "', admin_comments='" . $val['comment'] . "' WHERE id='" . $val['rid'] . "'");
        $trainer = $this->get_basic_trainer($val['uid']);
        $program = $this->get_basic_program_detail($val['rid']);
        $msg = array('pname' => $program[0]->project_title, 'pdate' => $program[0]->training_start_date, 'ploc' => $program[0]->location_of_training, 'email' => $trainer[0]->email, 'name' => $trainer[0]->name, 'status' => 'Rejected');
        $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $program[0]->project_id . "' AND notification_type='Trainer Engage'");
        $note = array('user' => $trainer[0]->user_code,
            'project' => $program[0]->project_id,
            'task' => 0,
            'type' => 'Reject Request',
            'due_date' => '',
            'text' => "Your Engagement Request for " . $program[0]->project_title . " was not confirmed by Wagons Staff. ");
        $this->push_notification($note);

      //  reject_status_mail($trainer[0]->email, $msg);
    }

    public function remove_trainer_assign($val) {
        $this->db->query("UPDATE training_projects SET trainer_engage_flag='0' WHERE project_id='" . $val['pid'] . "'");
        $this->db->query("DELETE FROM training_engagements WHERE project_id='" . $val['pid'] . "' AND user_code='" . $val['tid'] . "'");
        $this->db->query("DELETE FROM program_trainers WHERE project_id='" . $val['pid'] . "' AND trainer_id='" . $val['tid'] . "'");
        $this->db->query("DELETE FROM trainer_assigned WHERE project_id='" . $val['pid'] . "' AND trainer_id='" . $val['tid'] . "'");
    }

    public function get_basic_program_detail($id) {
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,u.email,u.name FROM training_projects p, application_users u WHERE p.project_id='" . $id . "' AND u.user_code=p.user_code");
        return $query->result();
    }

    public function get_basic_trainer($id) {
        $query = $this->db->query("SELECT name,email,company_name,city,contact_number,user_code FROM application_users WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function remove_content($val) {
        $this->db->query("DELETE FROM training_content WHERE id='" . $val['rid'] . "'");
    }

    public function remove_form($val) {
        $this->db->query("DELETE FROM program_forms WHERE id='" . $val['rid'] . "'");
    }

    public function program_props($val) {
        $this->db->query("DELETE FROM program_props WHERE id='" . $val['rid'] . "'");
    }

    public function remove_stay($val) {
        $this->db->query("DELETE FROM program_stay WHERE id='" . $val['rid'] . "'");
    }

    public function remove_travel($val) {
        $this->db->query("DELETE FROM program_travel WHERE id='" . $val['rid'] . "'");
    }

    //PROPS REQUEST BY TRAINER
    public function save_props() {

        for ($i = 1; $i < 6; $i++) {
            if (!empty($_POST['pname' . $i])) {
                $data = array(
                    'prop_name' => $_POST['pname' . $i],
                    'project_id' => $_POST['project'],
                    'quantity' => $_POST['qnt' . $i],
                    'comments' => $_POST['comments'],
                    'requested_by' => $_POST['props_user'],
                    'requested_date' => $this->cdate);

                $this->db->insert('program_props', $data);
            }
        }
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date, p.location_of_training, u.email,u.name,u.user_code FROM training_projects p, application_users u WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.user_code");
        $project = $query->result();
        $data = array('pname' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'training_location' => $project[0]->location_of_training,
            'pmname' => $project[0]->name,
            'status' => 'PropsRequest');
        $email = $project[0]->email;

        $note = array('user' => $project[0]->user_code,
            'project' => $project[0]->project_id,
            'task' => 1,
            'type' => 'Trainer New Props',
            'due_date' => '',
            'text' => "Trainer " . $this->session->userdata('t_name') . " has Requested  for Props, Action Required.");
        $this->push_notification($note);

    //    project_mail_pm($email, $data);
        return 'Props';
    }

    //APPROVAL OF PROPS MAIL TO ADMIN SUPPORT
    public function update_props_status($val) {
        $this->db->query("UPDATE program_props SET delivery_status='" . $val['status'] . "' WHERE id='" . $val['sid'] . "'");

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,c.client_name FROM training_projects p, clients c  WHERE p.project_id='" . $val['pid'] . "' AND c.client_id=p.client_id");
        $project = $query->result();

        $query2 = $this->db->query("SELECT u.name,u.email FROM program_props p, application_users u WHERE p.id='" . $val['sid'] . "' AND u.user_code=p.requested_by");
        $result = $query2->result();
        $tName = $result[0]->name;

        $query3 = $this->db->query("SELECT user_code,email FROM application_users WHERE user_type='admin support'");
        $asEmail = $query3->result();

        $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $project[0]->project_id . "' AND notification_type='Trainer New Props'");

        if ($val['status'] == '3') {
            $data = array('project' => $project[0]->project_title,
                'start_date' => $project[0]->training_start_date,
                'company' => $project[0]->client_name,
                'status' => 'Props');

            $note = array('user' => $asEmail[0]->user_code,
                'project' => $project[0]->project_id,
                'task' => 1,
                'type' => 'PM Approve Props',
                'due_date' => '',
                'text' => "Props Requested by " . $tName . " have been approved by PM, Action Required");
            $this->push_notification($note);

        //    props_status_mail($asEmail[0]->email, $data);
        }
        return 'props';
    }

    public function props_approve_all($val) {
        $this->db->query("UPDATE program_props SET delivery_status='3' WHERE project_id='" . $val['pid'] . "'");

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training, c.client_name FROM training_projects p, clients c  WHERE p.project_id='" . $val['pid'] . "' AND c.client_id=p.client_id");
        $project = $query->result();


        $query2 = $this->db->query("SELECT name, email FROM application_users  WHERE user_type='admin support'");
        $result = $query2->result();
        $email = '';
        foreach ($result as $da) {
            $email .=$da->email . ',';
        }

        $query3 = $this->db->query("SELECT user_code,email FROM application_users WHERE user_type='admin support'");
        $asEmail = $query3->result();

        $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $project[0]->project_id . "' AND notification_type='Trainer New Props'");

        $email = rtrim($email, ',');
        $email .="," . $asEmail[0]->email;
        $data = array('project' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'company' => $project[0]->client_name,
            'location' => $project[0]->location_of_training,
            'status' => 'PropsApproval');

        $note = array('user' => $asEmail[0]->user_code,
            'project' => $project[0]->project_id,
            'task' => 1,
            'type' => 'PM Approve Props',
            'due_date' => '',
            'text' => "Props Requested for program : " . $project[0]->project_title . " have been approved by PM, Action Required");
        $this->push_notification($note);

       // project_mail_support($email, $data);

        return 'props';
    }

//PREFERENE ADD BY TRAINER EMAIL PM
    public function save_stay() {
        if (isset($_POST['pay_status']))
            $pay_status = $_POST['pay_status'];
        else
            $pay_status = "";

        $data = array('project_id' => $_POST['project'],
            'trainer_id' => $_POST['trainer'],
            'hotel_name' => $_POST['hotel'],
            'checkin_time' => $_POST['checkin'],
            'checkout_time' => $_POST['checkout'],
            'pay_status' => $pay_status,
            'comments' => $_POST['comments'],
            'request_status' => '0',
            'entered_by' => $_POST['stay_user']);

        $this->db->insert('program_stay', $data);


        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date, p.location_of_training, u.email,u.name,u.user_code FROM training_projects p, application_users u WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.user_code");
        $project = $query->result();
        $data = array('pname' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'training_location' => $project[0]->location_of_training,
            'pmname' => $project[0]->name,
            'status' => 'StayRequest');
        $email = $project[0]->email;

        $note = array('user' => $project[0]->user_code,
            'project' => $project[0]->project_id,
            'task' => 1,
            'type' => 'Trainer Stay Request',
            'due_date' => '',
            'text' => $this->session->userdata('t_name') . " has submited Stay Prefs for the program " . $project[0]->project_title);
        $this->push_notification($note);

      //  project_mail_pm($email, $data);

        return 'Stay';
    }

    public function get_stay_details($id) {
        $query = $this->db->query("SELECT s.*,u.name,u.email,u.user_code FROM program_stay s, application_users u WHERE s.project_id='" . $id . "' AND u.user_code=s.trainer_id");
        return $query->result();
    }

    //APPROVAL BY PM, EMAIL ADMIN SUPPORT
    public function update_stay_status($val) {
        $this->db->query("UPDATE program_stay SET request_status='" . $val['status'] . "' WHERE id='" . $val['sid'] . "'");

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training, c.client_name FROM training_projects p, clients c  WHERE p.project_id='" . $val['pid'] . "' AND c.client_id=p.client_id");
        $project = $query->result();

        $query2 = $this->db->query("SELECT name, email,user_code FROM application_users  WHERE user_type='admin support'");
        $result = $query2->result();
        $email = '';
        foreach ($result as $da) {
            $email .=$da->email . ',';
        }

        $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $project[0]->project_id . "' AND notification_type='Trainer Stay Request'");

        $email = rtrim($email, ',');
        if ($val['status'] == '2') {
            $data = array('project' => $project[0]->project_title,
                'start_date' => $project[0]->training_start_date,
                'company' => $project[0]->client_name,
                'location' => $project[0]->location_of_training,
                'status' => 'StayApproval');

            $note = array('user' => $result[0]->user_code,
                'project' => $project[0]->project_id,
                'task' => 1,
                'type' => 'PM Approve Stay',
                'due_date' => '',
                'text' => "Stay Prefs Submitted by Trainer has been approved by PM, Book Stay for the Trainer");
            $this->push_notification($note);

        //    project_mail_support($email, $data);
        }
    }

    //ACTUAL STAY BOOKING >> MAIL TRAINER & PM
    public function admin_save_stay() {

        $file = "";
        $path = './assets/upload/stay/';
        $img_name = $_FILES['screen']['name'];
        $tmp_name = $_FILES['screen']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $data = array('project_id' => $_POST['project'],
            'trainer_id' => $_POST['trainer'],
            'hotel_name' => $_POST['hotel'],
            'checkin_time' => $_POST['checkin'],
            'checkout_time' => $_POST['checkout'],
            'pay_status' => $_POST['pay_status'],
            'comments' => $_POST['comments'],
            'expense_amount' => $_POST['stay_amt'],
            'request_status' => 1,
            'direct_book' => 1,
            'booking_screenshot' => $file,
            'entered_by' => $_POST['stay_user']);

        $this->db->insert('program_stay', $data);
        $rfid = $this->db->insert_id();

        $data = array('project_id' => $_POST['project'],
            'ref_id' => $rfid,
            'expense_type' => 'Stay Expenses',
            'amount' => $_POST['stay_amt'],
            'in_amount' => $_POST['stay_amt'],
            'voucher_image' => $file,
            'added_by' => $this->session->userdata('t_name'),
            'added_date' => $this->full_date);

        $this->db->insert('program_expenses', $data);


        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training, c.client_name FROM training_projects p, clients c  WHERE p.project_id='" . $_POST['project'] . "' AND c.client_id=p.client_id");
        $project = $query->result();

        $query2 = $this->db->query("SELECT u.name,u.email,u.user_code FROM program_trainers p, application_users u WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.trainer_id");
        $result = $query2->result();
        $email = '';
        $eUid = array();
        foreach ($result as $da) {
            $email .=$da->email . ',';
            array_push($eUid, $da->user_code);
        }

        $email = rtrim($email, ',');

        $data = array('project' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'location' => $project[0]->location_of_training,
            'company' => $project[0]->client_name,
            'status' => 'Stay');

        $query3 = $this->db->query("SELECT u.email,u.user_code FROM training_projects p, application_users u WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.user_code");
        $result = $query2->result();

        $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $project[0]->project_id . "' AND notification_type='PM Approve Stay'");

        $email .="," . $result[0]->email;
        array_push($eUid, $result[0]->user_code);

        foreach ($eUid as $uid) {
            $note = array('user' => $uid,
                'project' => $project[0]->project_id,
                'task' => 0,
                'type' => 'AS Book Stay',
                'due_date' => '',
                'text' => "Stay Booked for the training program : " . $project[0]->project_title);
            $this->push_notification($note);
        }

     //   props_status_mail($email, $data);

        return 'Stay';
    }

    public function update_stay() {
        $file = $_POST['cfile'];
        $path = './assets/upload/stay/';
        $img_name = $_FILES['screen']['name'];
        $tmp_name = $_FILES['screen']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $data = array('project_id' => $_POST['project'],
            'hotel_name' => $_POST['hotel'],
            'checkin_time' => $_POST['checkin'],
            'checkout_time' => $_POST['checkout'],
            'pay_status' => $_POST['pay_status'],
            'comments' => $_POST['comments'],
            'expense_amount' => $_POST['stay_amt'],
            'booking_screenshot' => $file,
            'entered_by' => $_POST['stay_user']);

        $this->db->where('id', $_POST['sid']);
        $this->db->update('program_stay', $data);
        $rfid = $_POST['sid'];

        $data = array('project_id' => $_POST['project'],
            'ref_id' => $rfid,
            'expense_type' => 'Stay Expenses',
            'amount' => $_POST['stay_amt'],
            'in_amount' => $_POST['stay_amt'],
            'voucher_image' => $file,
            'added_by' => $this->session->userdata('t_name'),
            'added_date' => $this->full_date);

        $this->db->where('ref_id', $rfid);
        $this->db->update('program_expenses', $data);
    }

    public function get_singleStay($id) {
        $query = $this->db->query("SELECT * FROM program_stay WHERE id='" . $id . "'");
        return $query->result();
    }

    //Actual Booking of Stay, Email PM & TRAINER
    public function book_stay() {
        $file = "";
        $path = './assets/upload/stay/';
        $img_name = $_FILES['screen']['name'];
        $tmp_name = $_FILES['screen']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $this->db->query("UPDATE program_stay SET request_status='1',booking_screenshot='" . $file . "', expense_amount='" . $_POST['stay_amt'] . "' WHERE id='" . $_POST['pid'] . "'");

        $data = array('project_id' => $_POST['project'],
            'ref_id' => $_POST['pid'],
            'expense_type' => 'Stay Expenses',
            'amount' => $_POST['stay_amt'],
            'in_amount' => $_POST['stay_amt'],
            'voucher_image' => $file,
            'notes' => $_POST['comments'],
            'added_by' => $this->session->userdata('t_name'),
            'added_date' => $this->full_date);

        $this->db->insert('program_expenses', $data);

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training, c.client_name FROM training_projects p, clients c  WHERE p.project_id='" . $_POST['project'] . "' AND c.client_id=p.client_id");
        $project = $query->result();

        $query2 = $this->db->query("SELECT u.name,u.email,u.user_code FROM program_trainers p, application_users u WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.trainer_id");
        $result = $query2->result();
        $email = '';
        $eUid = array();
        foreach ($result as $da) {
            $email .=$da->email . ',';
            array_push($eUid, $da->user_code);
        }

        $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $project[0]->project_id . "' AND notification_type='PM Approve Stay'");

        $email = rtrim($email, ',');

        $data = array('project' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'location' => $project[0]->location_of_training,
            'company' => $project[0]->client_name,
            'status' => 'Stay');

        $query3 = $this->db->query("SELECT u.email FROM training_projects p, application_users u WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.user_code");
        $result = $query2->result();

        $email .="," . $result[0]->email;
        array_push($eUid, $result[0]->user_code);

        foreach ($eUid as $uid) {
            $note = array('user' => $uid,
                'project' => $project[0]->project_id,
                'task' => 0,
                'type' => 'AS Book Stay',
                'due_date' => '',
                'text' => "Admin booked trainer stay for " . $project[0]->project_title);
            $this->push_notification($note);
        }

     //   props_status_mail($email, $data);
        return 'Stay';
    }

//STAY PREFERENCE BY TRAINER >> Mail to PM
    public function save_travel() {

        $data = array('project_id' => $_POST['project'],
            'trainer_id' => $_POST['trainer'],
            'date' => $_POST['date'],
            'from_loc' => $_POST['from'],
            'to_loc' => $_POST['to'],
            'mode' => $_POST['mode'],
            'comments' => $_POST['comments'],
            'request_status' => '0',
            'entered_by' => $_POST['travel_user']);

        $this->db->insert('program_travel', $data);

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date, p.location_of_training, u.email,u.name,u.user_code FROM training_projects p, application_users u WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.user_code");
        $project = $query->result();
        $data = array('pname' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'training_location' => $project[0]->location_of_training,
            'pmname' => $project[0]->name,
            'status' => 'TravelRequest');

        $email = $project[0]->email;

        $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $project[0]->project_id . "' AND notification_type='PM Approve Travel'");

        $note = array('user' => $project[0]->user_code,
            'project' => $project[0]->project_id,
            'task' => 1,
            'type' => 'Trainer Travel Request',
            'due_date' => '',
            'text' => $this->session->userdata('t_name') . " has submitted Travel Prefs for the program, Action Required ");
        $this->push_notification($note);

    //    project_mail_pm($email, $data);
        return 'Travel';
    }

//Booking of Travel by Admin Support >> email Trainer and PM
    public function admin_save_travel() {

        $file = "";
        $path = './assets/upload/travel/';
        $img_name = $_FILES['screen']['name'];
        $tmp_name = $_FILES['screen']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $data = array('project_id' => $_POST['project'],
            'trainer_id' => $_POST['trainer'],
            'date' => $_POST['date'],
            'from_loc' => $_POST['from'],
            'to_loc' => $_POST['to'],
            'mode' => $_POST['mode'],
            'expense_amount' => $_POST['travel_amt'],
            'comments' => $_POST['comments'],
            'request_status' => '1',
            'direct_book' => 1,
            'screenshot' => $file,
            'entered_by' => $_POST['travel_user']);

        $this->db->insert('program_travel', $data);
        $rfid = $this->db->insert_id();

        $data = array('project_id' => $_POST['project'],
            'ref_id' => $rfid,
            'expense_type' => 'Travel Expenses',
            'amount' => $_POST['travel_amt'],
            'in_amount' => $_POST['travel_amt'],
            'notes' => $_POST['comments'],
            'voucher_image' => $file,
            'added_by' => $this->session->userdata('t_name'),
            'added_date' => $this->full_date);

        $this->db->insert('program_expenses', $data);

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training, c.client_name FROM training_projects p, clients c  WHERE p.project_id='" . $_POST['project'] . "' AND c.client_id=p.client_id");
        $project = $query->result();

        $query2 = $this->db->query("SELECT u.name,u.email,u.user_code FROM program_trainers p, application_users u WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.trainer_id");
        $result = $query2->result();
        $email = '';
        $eUid = array();
        foreach ($result as $da) {
            $email .=$da->email . ',';
            array_push($eUid, $da->user_code);
        }

        $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $project[0]->project_id . "' AND notification_type='PM Approve Travel'");

        $email = rtrim($email, ',');

        $data = array('project' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'location' => $project[0]->location_of_training,
            'company' => $project[0]->client_name,
            'status' => 'Travel');

        $query3 = $this->db->query("SELECT u.email,u.user_code FROM training_projects p, application_users u WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.user_code");
        $result = $query2->result();

        $email .="," . $result[0]->email;
        array_push($eUid, $result[0]->user_code);
        foreach ($eUid as $uid) {
            $note = array('user' => $uid,
                'project' => $project[0]->project_id,
                'task' => 0,
                'type' => 'AS Book Travel',
                'due_date' => '',
                'text' => "Admin booked travel  for " . $project[0]->project_title);
            $this->push_notification($note);
        }
    //    props_status_mail($email, $data);


        return 'Travel';
    }

    public function update_travel() {
        $file = $_POST['cfile'];
        $path = './assets/upload/travel/';
        $img_name = $_FILES['screen']['name'];
        $tmp_name = $_FILES['screen']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $data = array('project_id' => $_POST['project'],
            'date' => $_POST['date'],
            'from_loc' => $_POST['from'],
            'to_loc' => $_POST['to'],
            'mode' => $_POST['mode'],
            'expense_amount' => $_POST['travel_amt'],
            'comments' => $_POST['comments'],
            'screenshot' => $file,
            'entered_by' => $_POST['travel_user']);

        $this->db->where('id', $_POST['tid']);
        $this->db->update('program_travel', $data);
        $rfid = $_POST['tid'];

        $data = array('project_id' => $_POST['project'],
            'ref_id' => $rfid,
            'expense_type' => 'Travel Expenses',
            'amount' => $_POST['travel_amt'],
            'in_amount' => $_POST['travel_amt'],
            'notes' => $_POST['comments'],
            'voucher_image' => $file,
            'added_by' => $this->session->userdata('t_name'),
            'added_date' => $this->full_date);

        $this->db->where('ref_id', $rfid);
        $this->db->update('program_expenses', $data);
    }

    public function get_singleTravel($id) {
        $query = $this->db->query("SELECT * FROM program_travel WHERE id='" . $id . "'");
        return $query->result();
    }

    public function get_travel_details($id) {
        $query = $this->db->query("SELECT t.*,u.name,u.email,u.user_code FROM program_travel t, application_users u WHERE t.project_id='" . $id . "' AND u.user_code=t.trainer_id");
        return $query->result();
    }

    //APPROVAL OF TRAVEL by PM >> Email Admin Support
    public function update_travel_status($val) {
        $this->db->query("UPDATE program_travel SET request_status='" . $val['status'] . "' WHERE id='" . $val['sid'] . "'");

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training, c.client_name FROM training_projects p, clients c  WHERE p.project_id='" . $val['pid'] . "' AND c.client_id=p.client_id");
        $project = $query->result();

        $query2 = $this->db->query("SELECT name, email,user_code FROM application_users  WHERE user_type='admin support'");
        $result = $query2->result();
        $email = '';
        foreach ($result as $da) {
            $email .=$da->email . ',';
        }

        $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $project[0]->project_id . "' AND notification_type='Trainer Travel Request'");

        $email = rtrim($email, ',');
        if ($val['status'] == '2') {
            $data = array('project' => $project[0]->project_title,
                'start_date' => $project[0]->training_start_date,
                'company' => $project[0]->client_name,
                'location' => $project[0]->location_of_training,
                'status' => 'TravelApproval');

            $note = array('user' => $result[0]->user_code,
                'project' => $project[0]->project_id,
                'task' => 1,
                'type' => 'PM Approve Travel',
                'due_date' => '',
                'text' => "Travel Prefs submitted by Trainer has been approved by PM, Book Travel for the trainer");
            $this->push_notification($note);

       //     project_mail_support($email, $data);
        }
    }

    //actual booking of travel >> Mail to Trainer & PM
    public function book_travel() {
        $file = "";
        $path = './assets/upload/travel/';
        $img_name = $_FILES['screen']['name'];
        $tmp_name = $_FILES['screen']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $this->db->query("UPDATE program_travel SET request_status='1',screenshot='" . $file . "', expense_amount='" . $_POST['travel_amt1'] . "'  WHERE id='" . $_POST['pid1'] . "'");

        $data = array('project_id' => $_POST['project'],
            'ref_id' => $_POST['pid1'],
            'expense_type' => 'Travel Expenses',
            'amount' => $_POST['travel_amt1'],
            'in_amount' => $_POST['travel_amt1'],
            'voucher_image' => $file,
            'notes' => $_POST['comments'],
            'added_by' => $this->session->userdata('t_name'),
            'added_date' => $this->full_date);

        $this->db->insert('program_expenses', $data);

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training, c.client_name FROM training_projects p, clients c  WHERE p.project_id='" . $_POST['project'] . "' AND c.client_id=p.client_id");
        $project = $query->result();

        $query2 = $this->db->query("SELECT u.name,u.email,u.user_code FROM program_trainers p, application_users u WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.trainer_id");
        $result = $query2->result();
        $email = '';
        $eUid = array();
        foreach ($result as $da) {
            $email .=$da->email . ',';
            array_push($eUid, $da->user_code);
        }

        $email = rtrim($email, ',');

        $data = array('project' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'location' => $project[0]->location_of_training,
            'company' => $project[0]->client_name,
            'status' => 'Travel');

        $query3 = $this->db->query("SELECT u.email,u.user_code FROM training_projects p, application_users u WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.user_code");
        $result = $query2->result();

        $email .="," . $result[0]->email;
        array_push($eUid, $result[0]->user_code);
        foreach ($eUid as $uid) {
            $note = array('user' => $uid,
                'project' => $project[0]->project_id,
                'task' => 0,
                'type' => 'AS Book Travel',
                'due_date' => '',
                'text' => "Travel Booked for the Program : " . $project[0]->project_title);
            $this->push_notification($note);
        }
      //  props_status_mail($email, $data);

        return 'Travel';
    }

    public function get_program_trainers($id) {

        $query = $this->db->query("SELECT t.*,u.name,u.email,u.user_code, u.contact_number,ta.* FROM program_trainers t INNER JOIN application_users u ON t.trainer_id=u.user_code, trainer_assigned ta WHERE t.project_id='" . $id . "' AND ta.trainer_id=t.trainer_id AND ta.project_id='" . $id . "'");
        return $query->result();
    }

    public function send_trainer_sms($val) {
        send_sms($val['mobile'], $val['text']);
        $pm = $this->session->userdata('pm_code');
        $data = array('sms_from' => $pm,
            'sms_to' => $val['tid'],
            'sms_mobile' => $val['mobile'],
            'sms_text' => $val['text'],
            'sms_date' => $this->full_date);

        $this->db->insert('sms_notification', $data);
    }

    public function get_progrom_date($id) {
        $month = date('m');
        $query = $this->db->query("SELECT project_id,project_title,training_start_date,training_duration,trainer_engage_flag,excl_sun,excl_sat, location_of_training FROM training_projects WHERE user_code='" . $id . "' AND is_active!='0'");
        return $query->result();
    }

    public function get_all_content() {
        $query = $this->db->query("SELECT c.*, u.name,u.user_type,u.user_code FROM training_content c,application_users u WHERE  u.user_code=c.upload_by ORDER BY c.id");
        return $query->result();
    }

    public function save_single_content() {
        $path = './assets/upload/content/';
        $img_name = $_FILES['content']['name'];
        $tmp_name = $_FILES['content']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $project = '0';
        $client = '0';

        $data = array('filename' => $_POST['title'],
            'file_path' => $file,
            'project_id' => $project,
            'client_id' => $client,
            'upload_by' => $_POST['content_user'],
            'upload_date' => $this->cdate);

        $this->db->insert('training_content', $data);

        return 1;
    }

    public function send_to_bdm($val) {
        $this->db->query("UPDATE training_engagements SET status='3' WHERE project_id='" . $val['pid'] . "'");
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training,c.client_name,c.email1,c.phone1,u.email FROM training_projects p INNER JOIN application_users u ON p.user_code=u.user_code, clients c  WHERE p.project_id='" . $val['pid'] . "' AND c.client_id=p.client_id");
        // return $this->db->last_query();
        $details = $query->result();
        $trainer = $this->get_basic_trainer($val['uid']);
        $msg = array('project' => $details[0]->project_title,
            'project_date' => $details[0]->training_start_date,
            'company' => $details[0]->client_name,
            'city' => $details[0]->location_of_training,
            'email' => $details[0]->email1,
            'name' => $trainer[0]->name,
            'tcom' => $trainer[0]->company_name,
            'tcity' => $trainer[0]->city,
            'contact' => $trainer[0]->contact_number,
            'temail' => $trainer[0]->email,
            'tcode' => $trainer[0]->user_code,
            'bdm' => $val['name'],
            'link' => 'admin/review/' . custome_encode($val['rid'] . '-' . $val['pid']) . '/' . custome_encode('BDM'),
        );
        echo $msg['link'];
     //   bdm_review_mail($val['email'], $msg);
    }

    public function get_state() {
        $query = $this->db->query("SELECT * FROM state");
        return $query->result();
    }

    public function send_to_client($val) {
        $this->db->query("UPDATE training_engagements SET status='4' WHERE project_id='" . $val['pid'] . "'");
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training,c.client_name,c.email1,c.phone1,u.email FROM training_projects p INNER JOIN application_users u ON p.user_code=u.user_code, clients c  WHERE p.project_id='" . $val['pid'] . "' AND c.client_id=p.client_id");
        // return $this->db->last_query();
        $details = $query->result();
        $trainer = $this->get_basic_trainer($val['uid']);
        $msg = array('project' => $details[0]->project_title,
            'project_date' => $details[0]->training_start_date,
            'company' => $details[0]->client_name,
            'city' => $details[0]->location_of_training,
            'email' => $details[0]->email1,
            'name' => $trainer[0]->name,
            'tcom' => $trainer[0]->company_name,
            'tcity' => $trainer[0]->city,
            'contact' => $trainer[0]->contact_number,
            'temail' => $trainer[0]->email,
            'tcode' => $trainer[0]->user_code,
            'bdm' => $val['name'],
            'link' => 'admin/review/' . custome_encode($val['rid'] . '-' . $val['pid']) . '/' . custome_encode('Client'),
        );
        // echo $msg['link'];
     //   bdm_review_mail($val['email'], $msg);
    }

    public function get_content_setting($id) {
        $query = $this->db->query("SELECT * FROM program_content_settings WHERE project_id='" . $id . "'");
        return $query->result();
    }

    public function save_content_setting() {

        $preTest = 0;
        $content_under_update = "N";
        if (isset($_POST['pre_test'])) {
            $preTest = 1;
        }
        $postTest = 0;
        if (isset($_POST['post_test'])) {
            $postTest = 1;
        }$Test = 0;
        if (isset($_POST['physical_paper_test'])) {
            $Test = 1;
        }
        if (isset($_POST['content_under_update'])) {
            $content_under_update = "Y";
        }

        $handbook_content = "";
        $handbook_qnt = "";
        $handbook_charge = "";
        $handbook_cost = "";
        $handbook_desc = "";
        $handbook_courier = "";
        $content_cost = "";
        $handbook_status = 0;

        if ($_POST['participant_handbook'] == 'Print + Dispatch By Wagons') {
            $handbook_content = $_POST['handbook_content'];
            $handbook_qnt = $_POST['handbook_qnt'];
            $handbook_charge = $_POST['handbook_charge'];

            $handbook_desc = $_POST['handbook_desc'];
            $handbook_courier = $_POST['handbook_courier'];
        }

        if ($handbook_content == 'Already exist') {
            $handbook_status = 1;
        }

        if ($handbook_charge == 'Bill Client') {
            $handbook_cost = $_POST['handbook_cost'];
        }

        if ($_POST['content_charges'] == 'Bill Client') {
            $content_cost = $_POST['content_cost'];
        }

        $ucodequery = $this->db->query("SELECT user_code FROM training_projects WHERE project_id='" . $_POST['pid'] . "'");
        $rs = $ucodequery->result();

        $data = array('project_id' => $_POST['pid'],
            'pre_test' => $preTest,
            'post_test' => $postTest,
            'content_preperation' => $_POST['content_preperation'],
            'participant_handbook' => $_POST['participant_handbook'],
            'content_charges' => $_POST['content_charges'],
            'physical_paper_test' => $Test,
            'module' => $_POST['content_module'],
            'content_under_update' => $content_under_update,
            'user_code' => $rs[0]->user_code,
            'handbook_content' => $handbook_content,
            'handbook_desc' => $handbook_desc,
            'handbook_qnt' => $handbook_qnt,
            'handbook_charge' => $handbook_charge,
            'handbook_cost' => $handbook_cost,
            'handbook_courier' => $handbook_courier,
            'handbook_status' => $handbook_status,
            'content_cost' => $content_cost,
        );

        $check = $this->db->query("SELECT * FROM program_content_settings WHERE project_id='" . $_POST['pid'] . "'");
        if ($check->num_rows()) {
            $this->db->where('project_id', $_POST['pid']);
            $this->db->update('program_content_settings', $data);
            $mailSubject = "Training Program Recently Updated";
        } else {
            $this->db->insert('program_content_settings', $data);
            $mailSubject = "New Training Program Created";
        }

        $query = $this->db->query("SELECT p.project_title,p.project_id,p.location_of_training,p.training_start_date,p.stay_arrangement,p.travel_arrangement,p.trainer_engage_flag,c.client_name FROM training_projects p, clients c WHERE p.project_id='" . $_POST['pid'] . "' AND c.client_id=p.client_id");
        $project = $query->result();

        $query1 = $this->db->query("SELECT u.user_code,u.email FROM training_projects p, application_users u WHERE  p.project_id='" . $_POST['pid'] . "' AND u.user_code=p.user_code");
        $pmEmail = $query1->result();

        $query2 = $this->db->query("SELECT email,user_code FROM application_users WHERE user_type='content manager'");
        $cmEmail = $query2->result();

        $query3 = $this->db->query("SELECT email,user_code FROM application_users WHERE user_type='admin support'");
        $asEmail = $query3->result();

        if ($handbook_content == 'Prepare / Update By CM') {
            $eData = array('title' => 'Handbook content required',
                'body' => 'Content is required for participants handbook. <br/> <b>Details</b> : ' . $handbook_desc,
                'project' => $project[0]->project_title,
                'date' => $project[0]->training_start_date,
                'location' => $project[0]->location_of_training);

         //   mail_to_as($cmEmail[0]->email, $eData);
        }

        $eData = array('module' => $_POST['content_module'],
            'content_preperation' => $_POST['content_preperation'],
            'handbook' => $_POST['participant_handbook'],
            'project' => $project[0]->project_title,
            'date' => $project[0]->training_start_date,
            'city' => $project[0]->location_of_training,
            'company' => $project[0]->client_name);

        if ($_POST['content_preperation'] != 'Already exist' || $content_under_update == "Y") {
            $eMail = $pmEmail[0]->email . ', ' . $cmEmail[0]->email;

            $dDate = date('Y-m-d', (strtotime('-5 day', strtotime($project[0]->training_start_date))));
            if ($dDate <= date('Y-m-d')) {
                $dDate = date('Y-m-d');
            }
            $note = array('user' => $cmEmail[0]->user_code,
                'project' => $_POST['pid'],
                'task' => 1,
                'type' => 'CM Content',
                'due_date' => $dDate,
                'text' => "Content Development Work : " . $project[0]->project_title);
            $this->push_notification($note);
        } else {
            $eMail = $pmEmail[0]->email;
        }


      //  content_setting_mail($eMail, $eData);

        if ($_POST['participant_handbook'] != 'Not Required') {
            $dDate = date('Y-m-d', (strtotime('-3 day', strtotime($project[0]->training_start_date))));
            $note = array('user' => $asEmail[0]->user_code,
                'project' => $_POST['pid'],
                'task' => 1,
                'type' => 'AS Content',
                'due_date' => $dDate,
                'text' => "Participant Handbook Requires to be Dispatched to " . $project[0]->location_of_training);
            $this->push_notification($note);

            $eData = array('title' => 'Participant handbook required',
                'body' => $handbook_qnt . ' Participant handbook required for following progrom. Login to your account for more details',
                'project' => $project[0]->project_title,
                'date' => $project[0]->training_start_date,
                'location' => $project[0]->location_of_training);

        //    mail_to_as($asEmail[0]->email, $eData);
        }

        $lData = array('u_id' => $rs[0]->user_code,
            'al_user' => '',
            'project_id' => $_POST['pid'],
            'al_title' => 'Updated content setting',
            'al_date' => $this->full_date);

        $this->db->insert('activity_log', $lData);

        $mailTitle = "New Program (" . $project[0]->project_title . ", " . date_formate_short($project[0]->training_start_date) . "," . $project[0]->client_name . ")  Below is a summary of activities you will require to monitor as part of operations.";
        $mailBody = "";
        if ($project[0]->trainer_engage_flag == '0') {
            $mailBody .="Trainer Engagement is pending <br/>";
        }
        if ($project[0]->stay_arrangement == 'Wagons') {
            $mailBody .="Stay Booking Pending <br/>";
        }
        if ($project[0]->travel_arrangement == 'Wagons') {
            $mailBody .="Travel Booking Pending <br/>";
        }
        $mailBody .="Venue & SPOC Update Pending <br/>";
        $mailBody .="TTT Schedule Input Pending <br/>";
        $mailBody .="Content Mapping Pending <br/>";
        if ($handbook_content == 'Prepare / Update By CM') {
            $mailBody .="Participant Handbook Content Pending <br/>";
        }
        if ($_POST['participant_handbook'] == 'Print + Dispatch By Wagons') {
            $mailBody .="Participant Handbook Shipment Pending <br/>";
        }
        if ($_POST['content_preperation'] == 'Prepare by CM') {
            $mailBody .="Content Preperation by CM <br/>";
        }

        $mailData = array('sub' => $mailSubject,
            'title' => $mailTitle,
            'body' => $mailBody,
            'project' => $project[0]->project_title,
            'location' => $project[0]->location_of_training,
            'date' => $project[0]->training_start_date);

       // new_program_task($pmEmail[0]->email, $mailData);

        $mailBody = "";
        $mailBodyCheck = 0;
        if ($project[0]->stay_arrangement == 'Wagons') {
            $mailBody .="Stay Booking Pending <br/>";
            $mailBodyCheck = 1;
        }
        if ($project[0]->travel_arrangement == 'Wagons') {
            $mailBody .="Travel Booking Pending <br/>";
            $mailBodyCheck = 1;
        }
        if ($_POST['participant_handbook'] == 'Print + Dispatch By Wagons') {
            $mailBody .="Particpant Handbook Shipment Pending <br/>";
            $mailBodyCheck = 1;
        }
        $mailBody .="Record Any Expenses Incurred on Program <br/>";

        $mailData = array('sub' => $mailSubject,
            'title' => $mailTitle,
            'body' => $mailBody,
            'project' => $project[0]->project_title,
            'location' => $project[0]->location_of_training,
            'date' => $project[0]->training_start_date);
        if ($mailBodyCheck == 1) {
        //    new_program_task($asEmail[0]->email, $mailData);
        }
    }

//Misc Couriier by ADMIN SUPPORT , EMAIL TRAINER & PM IF PROPS COURIERED
    public function save_courier() {

        $pType = $_POST['pType'];

        $data = array('cr_project' => $_POST['project'],
            'cr_type' => $pType,
            'cr_cname' => $_POST['cname'],
            'cr_ship_date' => $_POST['cr_ship_date'],
            'cr_cid' => $_POST['cid'],
            'cr_amt' => $_POST['camt'],
            'comments' => $_POST['comments'],
            'cr_date' => $this->cdate);

//        $check = $this->db->query("SELECT * FROM courier WHERE cr_project='" . $_POST['project'] . "'");
//        if ($check->num_rows()) {
//            $this->db->where('cr_project', $_POST['project']);
//            $this->db->update('courier', $data);
//        } else {
        $this->db->insert('courier', $data);
        $rfid = $this->db->insert_id();

        $data1 = array('project_id' => $_POST['project'],
            'ref_id' => $rfid,
            'expense_type' => $pType,
            'notes' => $_POST['comments'],
            'amount' => $_POST['camt'],
            'in_amount' => $_POST['camt'],
            'added_by' => $this->session->userdata('t_name'),
            'added_date' => $this->full_date);

        $this->db->insert('program_expenses', $data1);

        if ($_POST['pType'] == "Training Props") {
            $this->db->query("UPDATE program_props SET delivery_status='1' WHERE project_id='" . $_POST['project'] . "'");
        }

        //SEND MAIL TO TRAINER AND PM ONLY IF SHipment was for PROPS
        $projectId = '';
        if ($_POST['pType'] == "Training Props" || $_POST['pType'] == "Misc. Charges") {

            $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,c.client_name FROM training_projects p, clients c  WHERE p.project_id='" . $_POST['project'] . "' AND c.client_id=p.client_id");
            $project = $query->result();

            $query2 = $this->db->query("SELECT u.name,u.email,u.user_code FROM program_trainers p, application_users u WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.trainer_id");
            $result = $query2->result();
            $email = '';
            $eUid = array();
            foreach ($result as $da) {
                $email .=$da->email . ',';
                array_push($eUid, $da->user_code);
            }
            $email = rtrim($email, ',');

            $query3 = $this->db->query("SELECT u.email,u.user_code FROM application_users u, training_projects p WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.user_code");
            $result = $query3->result();
            $pm_email = $result[0]->email;
            array_push($eUid, $result[0]->user_code);

            $email .=',' . $pm_email;
            if ($pType == "Training Props") {

                $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $project[0]->project_id . "' AND notification_type='PM Approve Props'");

                $data = array('project' => $project[0]->project_title,
                    'start_date' => $project[0]->training_start_date,
                    'company' => $project[0]->client_name,
                    'status' => 'PropsShipped');
                foreach ($eUid as $uid) {
                    $note = array('user' => $uid,
                        'project' => $_POST['project'],
                        'task' => 0,
                        'type' => 'AS Props Ship',
                        'due_date' => '',
                        'text' => "Admin Support has Shipped Props Requiested for the program  " . $project[0]->project_title);
                    $this->push_notification($note);
                }
                //return $eUid;
           //     props_status_mail($email, $data);
            }
            if ($pType == "Misc. Charges") {
                $note = array('user' => $result[0]->user_code,
                    'project' => $_POST['project'],
                    'task' => 0,
                    'type' => 'AS Misc Update',
                    'due_date' => '',
                    'text' => "Admin Support update Misc Courier Details for  " . $project[0]->project_title);
                $this->push_notification($note);
            }
            $projectId = $project[0]->project_id;
        }
        if ($pType == 'Participant Handbook') {
            $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $projectId . "' AND notification_type='AS Content'");
            return 'Courier';
        } else {
            return 'Props';
        }
    }

    public function update_courier() {
        $pType = $_POST['pType'];

        $data = array('cr_project' => $_POST['project'],
            'cr_type' => $pType,
            'cr_cname' => $_POST['cname'],
            'cr_ship_date' => $_POST['cr_ship_date'],
            'cr_cid' => $_POST['cid'],
            'cr_amt' => $_POST['camt'],
            'comments' => $_POST['comments'],
            'cr_date' => $this->cdate);

        $this->db->where('cr_id', $_POST['cr']);
        $this->db->update('courier', $data);
        $rfid = $_POST['cr'];

        $data1 = array('project_id' => $_POST['project'],
            'ref_id' => $rfid,
            'expense_type' => $pType,
            'notes' => $_POST['comments'],
            'amount' => $_POST['camt'],
            'in_amount' => $_POST['camt'],
            'added_by' => $this->session->userdata('t_name'),
            'added_date' => $this->full_date);

        $this->db->where('ref_id', $rfid);
        $this->db->update('program_expenses', $data1);

        if ($pType == 'Participant Handbook') {
            return 'Courier';
        } else {
            return 'Props';
        }
    }

    public function get_courier($id) {
        $query = $this->db->query("SELECT * FROM courier WHERE cr_project='" . $id . "'");
        return $query->result();
    }

    public function get_singleCourier($id) {
        $query = $this->db->query("SELECT * FROM courier WHERE cr_id='" . $id . "'");
        return $query->result();
    }

    public function save_ttt() {

        $this->db->query("DELETE FROM ttt_schedules WHERE trainer_id='" . $_POST['tid'] . "' AND project_id='" . $_POST['project'] . "'");

        $data = array('project_id' => $_POST['project'],
            'trainer_id' => $_POST['tid'],
            'ttt_date' => $_POST['tdate1'],
            'ttt_time' => $_POST['ttime1']);

        $this->db->insert('ttt_schedules', $data);
        $ttid = $this->db->insert_id();

        $t1 = 0;
        $t2 = 0;

        if (!empty($_POST['tdate2']) && !empty($_POST['ttime2'])) {
            $data = array('project_id' => $_POST['project'],
                'trainer_id' => $_POST['tid'],
                'ttt_date' => $_POST['tdate2'],
                'ttt_time' => $_POST['ttime2']);

            $this->db->insert('ttt_schedules', $data);
            $t1 = 1;
        }

        if (!empty($_POST['tdate3']) && !empty($_POST['ttime3'])) {
            $data = array('project_id' => $_POST['project'],
                'trainer_id' => $_POST['tid'],
                'ttt_date' => $_POST['tdate3'],
                'ttt_time' => $_POST['ttime3']);

            $this->db->insert('ttt_schedules', $data);
        }
        //INFORM ABOUT TTT SCHEDULES TO TRAINER

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training, c.client_name FROM training_projects p, clients c  WHERE p.project_id='" . $_POST['project'] . "' AND c.client_id=p.client_id");
        $project = $query->result();

        $query2 = $this->db->query("SELECT u.name,u.email,u.user_code FROM program_trainers p, application_users u WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.trainer_id");
        $result = $query2->result();
        $eUid = array();
        $email = '';
        foreach ($result as $da) {
            $email .=$da->email . ',';
            array_push($eUid, $da->user_code);
        }
        $email = rtrim($email, ',');


        $data = array('project' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'location' => $project[0]->location_of_training,
            'company' => $project[0]->client_name,
            'status' => 'TTTSchedule');

        foreach ($eUid as $uid) {
            $note = array('user' => $uid,
                'project' => $project[0]->project_id,
                'task' => 1,
                'type' => 'TTT Schedule',
                'due_date' => '',
                'text' => "TTT Schedule has been updated by PM, Kindly Choose your prefered schedule ");
            $this->push_notification($note);
        }

     //   props_status_mail($email, $data);

        if ($t1 == 0 && $t2 == 0) {
            $val = array('tid' => $ttid, 'project' => $_POST['project']);
            $this->accept_ttt($val);
        }

        return 'TTT';
    }

    public function get_ttt($tid, $pid) {
        $query = $this->db->query("SELECT * FROM ttt_schedules WHERE trainer_id='" . $tid . "' AND project_id='" . $pid . "'");
        return $query->result();
    }

    public function accept_ttt($val) {
        $this->db->query("UPDATE ttt_schedules SET ttt_status='1' WHERE id='" . $val['tid'] . "'");
        //INFORM PM, CM, BD ABOUT SELECTED TTT SCHEDULE

        $queryT = $this->db->query("SELECT ttt_date FROM ttt_schedules WHERE id='" . $val['tid'] . "'");
        $resultT = $queryT->result();

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training, c.client_name FROM training_projects p, clients c  WHERE p.project_id='" . $val['project'] . "' AND c.client_id=p.client_id");
        $project = $query->result();

        $query2 = $this->db->query("SELECT u.name,u.contact_number FROM program_trainers p, application_users u WHERE p.project_id='" . $val['project'] . "' AND u.user_code=p.trainer_id");
        $result = $query2->result();
        $email = '';
        foreach ($result as $da) {
            $trainer_name = $da->name;
            $trainer_contact = $da->contact_number;
        }


        $query3 = $this->db->query("SELECT u.email,u.user_code FROM application_users u, training_projects p WHERE p.project_id='" . $val['project'] . "' AND u.user_code=p.user_code");
        $result = $query3->result();
        $pm_email = $result[0]->email;
        $pmCode = $result[0]->user_code;

        $email = $pm_email;

        $query3 = $this->db->query("SELECT u.email,u.user_code FROM application_users u  WHERE u.user_type='content manager'");
        $result = $query3->result();
        $cm_email = $result[0]->email;
        $cmCode = $result[0]->user_code;

        $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $project[0]->project_id . "' AND notification_type='TTT Schedule'");

        $data = array('project' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'company' => $project[0]->client_name,
            'location' => $project[0]->location_of_training,
            'trainer_name' => $trainer_name,
            'trainer_contact' => $trainer_contact,
            'status' => 'TTTAccept');

        $note = array('user' => $pmCode,
            'project' => $project[0]->project_id,
            'task' => 0,
            'type' => 'TTT Schedule Accept',
            'due_date' => '',
            'text' => "Trainer has selected a TTT schedule for the program : " . $project[0]->project_title);
        $this->push_notification($note);

        $note = array('user' => $cmCode,
            'project' => $project[0]->project_id,
            'task' => 1,
            'type' => 'TTT Schedule Accept',
            'due_date' => $resultT->ttt_date,
            'text' => "Trainer has selected a TTT schedule for the program : " . $project[0]->project_title);
        $this->push_notification($note);

     //   project_mail_support($email, $data);

        return 'TTT';
    }

    public function update_venue() {
        $this->db->query("UPDATE training_projects SET venue='" . $_POST['venue'] . "', spoc='" . $_POST['spoc'] . "' WHERE project_id='" . $_POST['project'] . "'");
        //INFORM TRAINER ABOUT VENUE UPDATE

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training, c.client_name FROM training_projects p, clients c  WHERE p.project_id='" . $_POST['project'] . "' AND c.client_id=p.client_id");
        $project = $query->result();

        $query2 = $this->db->query("SELECT u.name,u.email,u.user_code FROM program_trainers p, application_users u WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.trainer_id");
        $result = $query2->result();
        $email = '';
        $eUid = array();
        foreach ($result as $da) {
            $email .=$da->email . ',';
            array_push($eUid, $da->user_code);
        }
        $email = rtrim($email, ',');


        $data = array('project' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'location' => $project[0]->location_of_training,
            'company' => $project[0]->client_name,
            'status' => 'Venue');

        foreach ($eUid as $uid) {
            $note = array('user' => $uid,
                'project' => $project[0]->project_id,
                'task' => 0,
                'type' => 'PM Venue Update',
                'due_date' => '',
                'text' => "Venue and SPOC details for the program has been updated by PM ");
            $this->push_notification($note);
        }

     //   props_status_mail($email, $data);
    }
    
    public function update_checkIn(){
        $data = array('post_test_trigger_time'=>$_POST['postAsse'],
            'pre_test_trigger_time'=>$_POST['preAsse'],
            'trainer_checkout_time'=>$_POST['checkOut'],
            'trainer_checkin_time'=>$_POST['checkIn']);
        
        $this->db->where('assignment_id',$_POST['assignId']);
        $this->db->update('trainer_assigned',$data);
    }

    //PM approves invoice , Email trainer support
    public function approve_invoice($val) {
        //trainer_invoice_action='2',
        $this->db->query("UPDATE program_trainers SET trainer_invoice_action='2', pm_approve_date='" . $this->full_date . "' WHERE id='" . $val['pid'] . "'");

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training, c.client_name FROM training_projects p, clients c  WHERE p.project_id='" . $val['project_id'] . "' AND c.client_id=p.client_id");
        $project = $query->result();

        $query2 = $this->db->query("SELECT u.name,u.contact_number FROM program_trainers p, application_users u WHERE p.project_id='" . $val['project_id'] . "' AND u.user_code=p.trainer_id");
        $result = $query2->result();
        $email = '';
        foreach ($result as $da) {
            $trainer_name = $da->name;
            $trainer_contact = $da->contact_number;
        }


        $query3 = $this->db->query("SELECT u.name,u.email,u.user_code FROM application_users u, training_projects p WHERE p.project_id='" . $val['project_id'] . "' AND u.user_code=p.user_code");
        $result = $query3->result();
        $pm_name = $result[0]->email;



        $query3 = $this->db->query("SELECT u.email,u.user_code FROM application_users u  WHERE u.user_type='Training support'");
        $result = $query3->result();
        $email = $result[0]->email;
        $tsCode = $result[0]->user_code;

        $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $project[0]->project_id . "' AND notification_type='Trainer Invoice'");

        $data = array('project' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'company' => $project[0]->client_name,
            'location' => $project[0]->location_of_training,
            'trainer_name' => $trainer_name,
            'trainer_contact' => $trainer_contact,
            'pm_name' => $pm_name,
            'status' => 'TrainerInvoiceApproved');

        $note = array('user' => $tsCode,
            'project' => $project[0]->project_id,
            'task' => 1,
            'type' => 'PM Approve Invoice',
            'due_date' => '',
            'text' => "Invoice generated by Trainer has been approved by PM, Kindly Take review of invoice and supporting documents and forward to admin for clearance  ");
        $this->push_notification($note);

     //   project_mail_support($email, $data);
    }

    public function remove_debit($val) {
        $this->db->query("UPDATE debit_note SET debit_status='0' WHERE dn_id='" . $val['did'] . "'");
    }

    public function get_support_payment() {
        
        $filter = "";
        $tFilter = "";
        $pFilter = "";
        
        if(isset($_POST['pclient'])){
            if($_POST['pclient']!='All'){
                $pFilter .=" AND p.client_id='".$_POST['pclient']."' ";
            }
            if(!empty($_POST['trainer'])){
                $tFilter .=" AND u.name  LIKE '%".$_POST['trainer']."%' ";
            }
            if(!empty($_POST['title'])){
                $pFilter .=" AND p.project_title LIKE '%".$_POST['title']."%' ";
            }
            if($_POST['status']!='All'){
                if($_POST['status']=='0' || $_POST['status']=='1' || $_POST['status']=='2'){
                    $filter .=" AND pt.trainer_invoice_action='".$_POST['status']."' ";
                }
                if($_POST['status']=='3'){
                    $filter .=" AND pt.trainer_paid_status='0' ";
                }
                if($_POST['status']=='4'){
                    $filter .=" AND pt.trainer_paid_status='1' ";
                }
            }
        }

        $p = 0;
        if (isset($_GET['page']) && $this->session->userdata('TINVOICE')) {
            $p = $_GET['page'];
            $queryString = $this->session->userdata('TINVOICE');
        } else {
            $queryString = "FROM program_trainers pt INNER JOIN application_users u ON  pt.trainer_id=u.user_code $tFilter, training_projects p INNER JOIN clients c ON p.client_id=c.client_id WHERE pt.trainer_invoice_flag='1'  $filter AND p.project_id=pt.project_id $pFilter";
            $this->session->set_userdata('TINVOICE', $queryString);
        }
        $lmt = "LIMIT 20 OFFSET 0";
        if ($p > 1) {
            $p = ($p - 1) * 20;
            $lmt = "LIMIT 20 OFFSET " . $p;
        }

        $query = $this->db->query("SELECT pt.*,p.project_id,p.project_title,p.training_start_date,p.location_of_training,c.client_name,u.user_code,u.name $queryString ORDER BY p.training_start_date DESC $lmt");
        return $query->result();
        
    }
    
    public function getTInvoiceCount() {
        $queryString = $this->session->userdata('TINVOICE');
        $query = $this->db->query("SELECT COUNT(pt.id) as total $queryString");
        return $query->result();
    }

    public function get_program_expense($id) {
        $query = $this->db->query("SELECT * FROM program_expenses WHERE project_id='" . $id . "'");
        return $query->result();
    }

    public function get_debit_note($id) {
        $query = $this->db->query("SELECT * FROM debit_note WHERE project='" . $id . "'");
        return $query->result();
    }

    public function save_support_expense() {
        $data = array('project_id' => $_POST['pid'],
            'expense_type' => $_POST['dTitle'],
            'notes' => $_POST['desc'],
            'amount' => $_POST['amt'],
            'in_amount' => $_POST['amt'],
            'added_by' => $this->session->userdata('t_name'),
            'added_date' => $this->full_date);

        $this->db->insert('program_expenses', $data);
    }

    public function get_project_fees($id) {
        $query = $this->db->query("SELECT SUM(p.amount) as total, (SELECT training_duration FROM training_projects t WHERE t.project_id=p.project_id) as duration  FROM program_trainers p WHERE p.project_id='" . $id . "'");
        return $query->result();
    }

    public function get_trainer_fees($tid, $pid) {
        $query = $this->db->query("SELECT * FROM program_trainers WHERE project_id='" . $pid . "' AND trainer_id='" . $tid . "'");
        return $query->result();
    }

    public function update_trainer() {
        if ($_POST['c_email'] != $_POST['email']) {
            $check = $this->db->query("SELECT email FROM application_users WHERE email='" . $_POST['email'] . "'");
            if ($check->num_rows()) {
                return 'error';
            }
        }

        $photo = $_POST['cimg'];
        $cv = $_POST['c_cv'];

        $path = './assets/upload/trainer/';
        $img_name = $_FILES['photo']['name'];
        $tmp_name = $_FILES['photo']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $this->image_resize($file_name, $path, $path, '250');
                $photo = $file_name;
            }
        }

        $img_name = $_FILES['cv']['name'];
        $tmp_name = $_FILES['cv']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $cv = $file_name;
            }
        }

        $data = array(
            'name' => $_POST['pname'],
            'address' => $_POST['address1'],
            'city' => $_POST['city'],
            'state' => $_POST['state'],
            'country' => $_POST['country'],
            'contact_number' => $_POST['mobile'],
            'contact_number_landline' => $_POST['landline'],
            'user_dob' => $_POST['dob'],
            'user_gender' => $_POST['gender'],
            'email' => $_POST['email'],
            'skypeID' => $_POST['skype'],
            'user_photo' => $photo,
            'user_cv' => $cv,
            'is_active' => '1',
            'freelancer' => $_POST['occupation'],
            'trainer_area' => implode(',', $_POST['specification']),
            'industry' => implode(',', $_POST['industry']),
            'expected_fee_per_day' => $_POST['fees'],
            'trainer_credit' => $_POST['trainer_credit'],
            'admin_approved' => '1',
        );

        $this->db->where('user_code', $_POST['uid']);
        $this->db->update('application_users', $data);

        if (!empty($_POST['password'])) {
            $pass = md5($_POST['password']);
            $this->db->query("UPDATE application_users SET password='" . $pass . "' WHERE user_code='" . $_POST['uid'] . "'");
        }

        return 1;
    }

    public function checkProject_module($id) {
        $query = $this->db->query("SELECT m.module, p.client_id FROM program_content_settings m, training_projects p WHERE m.project_id='" . $id . "' AND (m.content_preperation!='Already exist' || m.content_under_update='Y') AND p.project_id=m.project_id");
        $result = $query->result();
        if (!empty($result)) {
            $md = $result[0]->module;
            $client = $result[0]->client_id;
            $query2 = $this->db->query("SELECT cm_id FROM client_modules WHERE module_name='" . $md . "' AND client_id='" . $client . "' AND content_status='1'");
            $result2 = $query2->result();
            if (!empty($result2)) {
                $cm = $result2[0]->cm_id;
                $cmDecode = custome_encode($cm);
                return $cmDecode;
            }
            return 0;
        }
        return 0;
    }

    public function checkContent($pid, $tid) {
        $query = $this->db->query("SELECT trainer_content_pass FROM program_trainers WHERE project_id='" . $pid . "' AND trainer_id='" . $tid . "' AND trainer_content_pass!='' ");
        $result = $query->result();
        if (!empty($result)) {
            $query = $this->db->query("SELECT m.module, p.client_id FROM program_content_settings m, training_projects p WHERE m.project_id='" . $pid . "' AND (m.content_preperation!='Already exist' || m.content_under_update='Y') AND p.project_id=m.project_id");
            $result = $query->result();
            if (!empty($result)) {
                $md = $result[0]->module;
                $client = $result[0]->client_id;
                $query2 = $this->db->query("SELECT cm_id FROM client_modules WHERE module_name='" . $md . "' AND client_id='" . $client . "'");
                $result2 = $query2->result();
                if (!empty($result2)) {
                    $cm = $result2[0]->cm_id;
                    $cmDecode = custome_encode($cm);
                    return $cmDecode;
                }
                return 0;
            }
            return 0;
        }
        return 0;
    }

    public function download_content($val) {
        $pass = $val['pass'];
        $query = $this->db->query("SELECT project_id FROM program_trainers WHERE project_id='" . $val['pid'] . "' AND trainer_id='" . $val['tid'] . "' AND trainer_content_pass='" . $pass . "'");
        if ($query->num_rows()) {
            $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $val['pid'] . "' AND notification_type='PM Content Map'");
            $this->db->query("UPDATE program_trainers SET content_download_date='" . $this->full_date . "' WHERE project_id='" . $val['pid'] . "' AND trainer_id='" . $val['tid'] . "' AND content_download_date='0000-00-00 00:00:00'");
            return 1;
        }
        return 0;
    }

    public function get_projectModule($pid, $cid) {
        $query = $this->db->query("SELECT m.module FROM program_content_settings m WHERE m.project_id='" . $pid . "'");
        return $query->result();
    }

    public function get_module_content($pid, $cid) {
        $query = $this->db->query("SELECT m.module FROM program_content_settings m WHERE m.project_id='" . $pid . "'");
        $result = $query->result();
        if (!empty($result)) {
            $query = $this->db->query("SELECT * FROM client_modules m, content c WHERE m.client_id='" . $cid . "' AND m.module_name='" . $result[0]->module . "' AND c.module_id=m.cm_id");
            return $query->result();
        }
    }

    public function check_content_send($id) {
        $query = $this->db->query("SELECT trainer_content_pass,trainer_pass_date,content_download_date FROM program_trainers WHERE project_id='" . $id . "'  AND trainer_content_pass!='' ");
        return $query->result();
    }

    public function confirm_content($val) {
        $length = 6;
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        $pass = $randomString;

        $data = array('trainer_content_pass' => $pass,
            'trainer_pass_date' => $this->full_date);

        $this->db->where('project_id', $val['pid']);
        $this->db->update('program_trainers', $data);

        $query = $this->db->query("SELECT p.project_title,p.project_id,c.client_name,u.email,u.user_code FROM program_trainers pr INNER JOIN application_users u ON pr.trainer_id=u.user_code,training_projects p, clients c WHERE pr.project_id='" . $val['pid'] . "' AND p.project_id=pr.project_id AND c.client_id=p.client_id");
        $result = $query->result();

        $email = '';
        $eUid = array();
        foreach ($result as $da) {
            $email .=$da->email . ',';
            array_push($eUid, $da->user_code);
        }
        $email = rtrim($email, ',');

        $eData = array('password' => $pass,
            'project' => $result[0]->project_title,
            'company' => $result[0]->client_name);
        foreach ($eUid as $uid) {
            $note = array('user' => $uid,
                'project' => $result[0]->project_id,
                'task' => 1,
                'type' => 'PM Content Map',
                'due_date' => '',
                'text' => "Content required for the training program has been mapped to you, Kindly Download the content. ");
            $this->push_notification($note);
        }

    //    content_password_mail($email, $eData);
    }

    public function search_trainer($val) {
        $query = $this->db->query("SELECT name,user_code,city,state,contact_number FROM application_users WHERE name LIKE '%" . $val['searchkey'] . "%' AND user_type='trainer' AND is_active='1' AND admin_approved='1'");
        $result = $query->result();
        if (!empty($result)) {
            ?>
            <div class="clearfix"></div>
            <div class="form-group list-group col-md-12" style="padding-left:15px !important;">
                <?php
                foreach ($result as $rs_data) {
                    ?>
                    <a href="#" class="list-group-item trainer-item" name="<?php echo $rs_data->name; ?>" uid="<?php echo $rs_data->user_code; ?>"><?php echo $rs_data->name; ?> , <?php echo $rs_data->city; ?> , <?php echo $rs_data->state; ?> <br/> <span style="font-size:13px;"><?php echo $rs_data->contact_number; ?></span> </a>
                    <?php
                }
                ?>
            </div>
            <div class="clearfix"></div>
            <?php
        }
    }

    public function get_client_module($id) {
        $query = $this->db->query("SELECT * FROM client_modules WHERE client_id='" . $id . "'");
        return $query->result();
    }

    public function get_module_programCount($mid, $uid) {
        $query = $this->db->query("SELECT COUNT(project_id) as tp FROM program_content_settings WHERE module='" . $mid . "' AND user_code='" . $uid . "'");
        return $query->result();
    }

    public function get_bills_courier($pid, $tid) {
        $query = $this->db->query("SELECT * FROM trainer_bills_courier WHERE project_id='" . $pid . "' AND trainer_id='" . $tid . "'");
        return $query->result();
    }

    public function push_notification($val) {
        $data = array('user_id' => $val['user'],
            'project_id' => $val['project'],
            'notification_text' => $val['text'],
            'todo_task' => $val['task'],
            'notification_type' => $val['type'],
            'due_date' => $val['due_date'],
            'notification_date' => $this->full_date);

        $this->db->insert('notifications', $data);
    }

    public function get_notification($id) {
        $query = $this->db->query("SELECT n.*,p.project_title FROM notifications n, training_projects p WHERE n.user_id='" . $id . "' AND p.project_id=n.project_id ORDER BY n.id DESC");
        return $query->result();
    }

    public function send_sms($mob, $text) {
        $postData = array(
            'authkey' => '114809A5StsMQlelI5751142d',
            'mobiles' => "$mob",
            'message' => "$text",
            'sender' => 'WAGONS', // will this work for promotion ?
            'route' => '4'
        );

        //API URL
        $url = "http://t.askspidy.com/api/sendhttp.php";

        // init the resource
        $ch = curl_init();
        curl_setopt_array($ch, array(
            CURLOPT_URL => $url,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $postData
                //,CURLOPT_FOLLOWLOCATION => true
        ));


        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $output = curl_exec($ch);

        if (curl_errno($ch)) {
            $return = false;
        } else {
            $return = true;
        }
        curl_close($ch);

        return $return;
    }

    public function getWeekCount($id) {
        $query = $this->db->query("SELECT COUNT(project_id) as total FROM training_projects WHERE WEEKOFYEAR(training_start_date)=WEEKOFYEAR(NOW()) AND YEAR(training_start_date)=YEAR(NOW())  AND is_active='1' AND user_code='" . $id . "'");
        $result = $query->result();
        return $result[0]->total;
    }

    public function getMonthCount($id) {
        $query = $this->db->query("SELECT COUNT(project_id) as total FROM training_projects WHERE MONTH(training_start_date)=MONTH(NOW()) AND YEAR(training_start_date)=YEAR(NOW())  AND is_active='1' AND user_code='" . $id . "'");
        $result = $query->result();
        return $result[0]->total;
    }

    public function getTotalCount($id) {
        $query = $this->db->query("SELECT COUNT(project_id) as total FROM training_projects WHERE user_code='" . $id . "'  AND is_active='1'");
        $result = $query->result();
        return $result[0]->total;
    }

    public function updateCmTTT() {
        $this->db->query("UPDATE trainer_assigned SET ttt_cm_note='" . $_POST['desc'] . "',ttt_status='1' WHERE project_id='" . $_POST['pid'] . "' AND trainer_id='" . $_POST['tid'] . "'");
        $uid = $this->session->userdata('t_code');
        $this->db->query("UPDATE notifications SET todo_task='2' WHERE user_id='" . $uid . "' AND project_id='" . $_POST['pid'] . "' AND notification_type='TTT Schedule Accept'");

        $query = $this->db->query("SELECT p.project_id,p.project_title,u.user_code,u.email FROM training_projects p,application_users u WHERE p.project_id='" . $_POST['pid'] . "' AND u.user_code=p.user_code");
        $result = $query->result();
        if (!empty($result)) {
            $note = array('user' => $result[0]->user_code,
                'project' => $result[0]->project_id,
                'task' => 0,
                'type' => 'TTT Update',
                'due_date' => '',
                'text' => "Trainer TTT Schedule is updated by CM for program " . $result[0]->project_title);
            $this->push_notification($note);
        }
    }

    public function checkHandbookStatus($id) {
        $query = $this->db->query("SELECT handbook_status FROM program_content_settings WHERE project_id='" . $id . "'");
        $result = $query->result();
        return $result[0]->handbook_status;
    }

    public function getCheckin($pid, $tid) {
        $query = $this->db->query("SELECT * FROM trainer_assigned WHERE project_id='" . $pid . "' AND trainer_id='" . $tid . "'");
        //return $this->db->last_query();
        return $query->result();
    }

    public function updateTrainerAmt($val) {
        $this->db->query("UPDATE program_trainers SET amount='" . $val['amt'] . "' WHERE id='" . $val['pid'] . "'");
    }

}
