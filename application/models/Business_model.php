<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Business_model extends CI_Model {

    public $cdate = null;
    public $full_date = null;

    public function __construct() {
        $datestring = "%Y-%m-%d";
        $datestring1 = "%Y-%m-%d %H:%i:%s";
        $time = time();
        $this->cdate = mdate($datestring, $time);
        $this->full_date = mdate($datestring1, $time);
    }

    public function image_resize($file_name, $path, $newpath, $newWidth) {
        $img_size = getimagesize($path . $file_name);
        $newHeight = round(($newWidth / $img_size[0]) * $img_size[1]);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $file_name;
        $config['new_image'] = $newpath . $file_name;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $newWidth;
        $config['height'] = $newHeight;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }
    
    public function get_bdmBasic_client($id) {
        $query = $this->db->query("SELECT c.client_id,c.client_name FROM clients c, client_projectmanagers cp WHERE cp.bm_id='" . $id . "' AND c.client_id=cp.client_id ORDER BY client_name");
        return $query->result();
    }
    
    public function get_basicpm() {
        $query = $this->db->query("SELECT user_id,user_code,name FROM application_users WHERE user_type in ( 'project manager') ");
        return $query->result();
    }
    
    public function get_bdmPastPrograms($id) {
        $today = date('Y-m-d');
        $filter = "";
        $filter1 = "";
        if (isset($_POST['pclient'])) {
            if (isset($_POST['title'])) {
                if (!empty($_POST['title'])) {
                    $filter .= " AND p.project_title LIKE '%" . $_POST['title'] . "%' ";
                }
                if (!empty($_POST['location'])) {
                    $filter .= " AND p.location_of_training LIKE '%" . $_POST['location'] . "%' ";
                }
            }

            if (!empty($_POST['date'])) {
                $filter .=" AND DATE(p.training_start_date)='" . $_POST['date'] . "' ";
            }
            if (isset($_POST['cinvoice'])) {

                if ($_POST['pm'] != 'All') {
                    $filter .= " AND p.user_code='" . $_POST['pm'] . "'";
                }
                if ($_POST['cinvoice'] != 'All') {
                    $filter .= " AND p.client_invoice_raised='" . $_POST['cinvoice'] . "'";
                }
                if ($_POST['tinvoice'] != 'All') {
                    $filter1 .= " AND t.trainer_invoice_action='" . $_POST['tinvoice'] . "'";
                }
            }
        }

        $p = 0;
        if (isset($_GET['page']) && $this->session->userdata('BDMPASTQUERY')) {
            $p = $_GET['page'];
            $queryString = $this->session->userdata('BDMPASTQUERY');
        } else {
            $queryString = "FROM training_projects p LEFT JOIN program_trainers t ON p.project_id=t.project_id $filter1 INNER JOIN  clients c ON p.client_id=c.client_id,client_projectmanagers cp WHERE cp.bm_id='" . $id . "' AND p.client_id=cp.client_id AND p.is_active='1' AND p.training_start_date<'" . $today . "'  $filter";
            $this->session->set_userdata('BDMPASTQUERY', $queryString);
        }
        $lmt = "LIMIT 20 OFFSET 0";
        if ($p > 1) {
            $p = ($p - 1) * 20;
            $lmt = "LIMIT 20 OFFSET " . $p;
        }

        $query = $this->db->query("SELECT p.project_id,p.client_id,p.user_code,p.project_type,p.project_title,p.location_of_training,p.training_start_date,p.training_end_date,p.is_active,p.create_date,c.client_id,c.client_name,t.trainer_id,t.trainer_paid_status,t.trainer_invoice_action, p.client_invoice_raised, p.half_day $queryString  ORDER BY p.project_id DESC $lmt");
        //return $this->db->last_query();
        return $query->result();
    }
    
    public function getBDMPastCount() {
        $queryString = $this->session->userdata('BDMPASTQUERY');
        $query = $this->db->query("SELECT COUNT(p.project_id) as total $queryString");
        return $query->result();
    }
    
    public function get_basic_company() {
        $query = $this->db->query("SELECT * FROM companies");
        return $query->result();
    }
    
    public function get_bdmClient_invoice($id) {
        $filter = "";
        $title = "";
        if (isset($_POST['pclient'])) {
            if ($_POST['pclient'] != 'All') {
                $filter .="  ci.ci_client='" . $_POST['pclient'] . "' AND ";
            }
            if ($_POST['invoice'] != '') {
                $filter .="  ci.ci_number='" . $_POST['invoice'] . "' AND ";
            }
            if ($_POST['status'] != 'All') {
                if ($_POST['status'] == '2') {
                    $filter .=" ci.ci_debit_id!='0' AND ci.ci_debit_status='0' AND ";
                } else {
                    $filter .="  ci.ci_pay_status='" . $_POST['status'] . "' AND ";
                }
            }
            
            if($_POST['month']!='All'){
                $filter .="  MONTH(ci.ci_date)='" . $_POST['month'] . "' AND ";
            }
            if($_POST['year']!='All'){
                $filter .="  YEAR(ci.ci_date)='" . $_POST['year'] . "' AND ";
            }
        }



        $p = 0;
        if (isset($_GET['page']) && $this->session->userdata('BDMCINVOICE')) {
            $p = $_GET['page'];
            $queryString = $this->session->userdata('BDMCINVOICE');
        } else {
            $queryString = "FROM client_invoice ci LEFT JOIN training_projects p ON ci.ci_project=p.project_id $title, clients c,client_projectmanagers cp WHERE cp.bm_id='" . $id . "' AND ci.ci_client=cp.client_id AND ci.ci_status='1' AND $filter  c.client_id=ci.ci_client";
            $this->session->set_userdata('BDMCINVOICE', $queryString);
        }
        $lmt = "LIMIT 20 OFFSET 0";
        if ($p > 1) {
            $p = ($p - 1) * 20;
            $lmt = "LIMIT 20 OFFSET " . $p;
        }

        $query = $this->db->query("SELECT ci.*,c.client_id,c.client_name,c.payment_credit_days,p.project_title,p.training_start_date,p.training_duration,p.client_payment_receieved,p.client_debit_received $queryString ORDER BY ci.ci_id DESC $lmt");
        return $query->result();
    }

    public function getBDMCICount() {
        $queryString = $this->session->userdata('BDMCINVOICE');
        $query = $this->db->query("SELECT COUNT(ci.ci_id) as total $queryString");
        return $query->result();
    }
    
    public function get_bdmTrainerInvoice($id) {
        $pay = "";
        $date = "";
        $user = "";
        $project = "";
        if (isset($_POST['status'])) {
            if ($_POST['status'] != 'All') {
                $pay = " AND pt.trainer_paid_status='" . $_POST['status'] . "' ";
            }

            if ($_POST['trainer'] != '') {
                $user = " AND u.name LIKE '%" . $_POST['trainer'] . "%' ";
            }

            if ($_POST['title'] != '') {
                $project = " AND p.project_title LIKE '%" . $_POST['title'] . "%' ";
            }
        }

        if (isset($_POST['sdate']) && isset($_POST['tdate'])) {
            if (!empty($_POST['sdate']) && !empty($_POST['tdate'])) {
                $date = " AND DATE(pt.training_date_from)>= '" . $_POST['sdate'] . "' AND DATE(pt.training_date_from)<= '" . $_POST['tdate'] . "' ";
            }
        }

        $p = 0;
        if (isset($_GET['page']) && $this->session->userdata('BDMTINVOICE')) {
            $p = $_GET['page'];
            $queryString = $this->session->userdata('BDMTINVOICE');
        } else {
            $queryString = "FROM program_trainers pt INNER JOIN application_users u ON  pt.trainer_id=u.user_code $user, client_projectmanagers cp, training_projects p INNER JOIN clients c ON p.client_id=c.client_id WHERE pt.trainer_invoice_flag='1' AND pt.trainer_invoice_action='1' $pay $date AND cp.bm_id='" . $id . "' AND p.client_id=cp.client_id AND p.project_id=pt.project_id $project";
            $this->session->set_userdata('BDMTINVOICE', $queryString);
        }
        $lmt = "LIMIT 20 OFFSET 0";
        if ($p > 1) {
            $p = ($p - 1) * 20;
            $lmt = "LIMIT 20 OFFSET " . $p;
        }

        $query = $this->db->query("SELECT pt.*,p.project_id,p.project_title,p.location_of_training,c.client_name,u.user_code,u.name $queryString ORDER BY p.training_start_date DESC $lmt");
        return $query->result();
    }

    public function getBDMTICount() {
        $queryString = $this->session->userdata('BDMTINVOICE');
        $query = $this->db->query("SELECT COUNT(pt.id) as total $queryString");
        return $query->result();
    }
    
    public function getBDMContentPending($id) {
        $query = $this->db->query("SELECT COUNT(ct.cm_id) AS total,ct.client_id, (SELECT client_name FROM clients WHERE client_id=ct.client_id) as client FROM client_modules ct,client_projectmanagers cp WHERE cp.bm_id='" . $id . "' AND ct.content_status='0' AND ct.client_id=cp.client_id GROUP BY ct.client_id");
        return $query->result();
    }

    public function getBDMContent($id) {
        $query = $this->db->query("SELECT ct.*, (SELECT client_name FROM clients WHERE client_id=ct.client_id) as client FROM client_modules ct WHERE  ct.client_id='" . $id . "' AND ct.content_status='0'");
        return $query->result();
    }
    
    public function getBDMUpcomingProgram($id) {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT COUNT(t.project_id) AS total,t.client_id,(SELECT client_name FROM clients WHERE client_id=t.client_id) as client FROM training_projects t,client_projectmanagers c WHERE c.bm_id='" . $id . "' AND t.client_id=c.client_id AND t.training_start_date>='" . $today . "' AND t.is_active='1' GROUP BY t.client_id ORDER BY total DESC LIMIT 5");
        return $query->result();
    }

    public function getBDMTotalUpcoming($id) {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT COUNT(t.project_id) AS total FROM training_projects t,client_projectmanagers c WHERE c.bm_id='" . $id . "' AND t.client_id=c.client_id AND t.training_start_date>='" . $today . "' AND t.is_active='1'");
        return $query->result();
    }

    public function getBDMTotalPast($id) {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT COUNT(t.project_id) AS total FROM training_projects t,client_projectmanagers c WHERE c.bm_id='" . $id . "' AND t.client_id=c.client_id AND t.training_start_date<'" . $today . "' AND t.is_active='1'");
        return $query->result();
    }

    public function getBDMInvoiceTotal($id) {
        $query = $this->db->query("SELECT SUM(c.ci_amt) AS amt,SUM(c.ci_gstamt) AS gst,SUM(c.ci_dgstamt) AS dgst,SUM(c.ci_damt) AS damt FROM client_invoice c, client_projectmanagers cp WHERE cp.bm_id='" . $id . "' AND c.ci_client=cp.client_id");
        return $query->result();
    }

    public function getBDMTrainerAmount($id) {
        $today = date('Y-m-d');
        $query = $this->db->query("SELECT SUM(pt.amount*(DATEDIFF(pt.trainer_date_to,pt.training_date_from)+1)) AS amt FROM program_trainers pt,training_projects p,client_projectmanagers c WHERE c.bm_id='" . $id . "' AND p.client_id=c.client_id  AND pt.project_id=p.project_id AND pt.trainer_invoice_flag='1'");
        return $query->result();
    }

    public function getBDMTrainerDebit($id) {
        $query = $this->db->query("SELECT SUM(d.debit_amt) AS amt FROM debit_note d,training_projects p,client_projectmanagers c WHERE c.bm_id='" . $id . "' AND p.client_id=c.client_id  AND d.project=p.project_id AND d.debit_status='1'");
        return $query->result();
    }
}