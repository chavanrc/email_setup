<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Admin_model extends CI_Model {

    public $cdate = null;
    public $full_date = null;

    public function __construct() {
        $datestring = "%Y-%m-%d";
        $datestring1 = "%Y-%m-%d %H:%i:%s";
        $time = time();
        $this->cdate = mdate($datestring, $time);
        $this->full_date = mdate($datestring1, $time);
    }

    public function image_resize($file_name, $path, $newpath, $newWidth) {
        $img_size = getimagesize($path . $file_name);
        $newHeight = round(($newWidth / $img_size[0]) * $img_size[1]);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $file_name;
        $config['new_image'] = $newpath . $file_name;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $newWidth;
        $config['height'] = $newHeight;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    /* Checks User exists or not */

    public function admin_login($val) {
        $this->db->where('email', $val['username']);
        $this->db->where('password', md5($val['password']));
        //$this->db->where('password', $val['password']);
        $this->db->where('is_active', '1');
        $this->db->where('admin_approved', '1');
        $query = $this->db->get('application_users');
        if ($query->num_rows()) {
            $result = $query->result();

            $this->db->query("UPDATE application_users SET last_logged='" . $this->full_date . "' WHERE user_id='" . $result[0]->user_id . "'");

            if ($result[0]->user_type == 'admin') {
                $this->session->set_userdata('admin', $result[0]->email);
                $this->session->set_userdata('name', $result[0]->name);
                $this->session->set_userdata('user_id', $result[0]->user_id);
                $this->session->set_userdata('user_code', $result[0]->user_code);
                $this->session->set_userdata('user_photo', $result[0]->user_photo);
                $this->session->set_userdata('type', $result[0]->user_type);
            }
            if ($result[0]->user_type == 'project manager') {
                $this->session->set_userdata('pm', $result[0]->email);
                $this->session->set_userdata('pm_name', $result[0]->name);
                $this->session->set_userdata('pm_id', $result[0]->user_id);
                $this->session->set_userdata('pm_code', $result[0]->user_code);
                $this->session->set_userdata('pm_photo', $result[0]->user_photo);
                $this->session->set_userdata('type', $result[0]->user_type);
            }
            if ($result[0]->user_type == 'trainer') {
                $this->session->set_userdata('trainer', $result[0]->email);
                $this->session->set_userdata('t_name', $result[0]->name);
                $this->session->set_userdata('t_id', $result[0]->user_id);
                $this->session->set_userdata('t_code', $result[0]->user_code);
                $this->session->set_userdata('t_photo', $result[0]->user_photo);
                $this->session->set_userdata('type', $result[0]->user_type);
                $this->session->set_userdata('t_gst', $result[0]->GST_number);
                $this->session->set_userdata('t_gst_code', $result[0]->state_code);
            }
            if ($result[0]->user_type == 'business manager') {
                $this->session->set_userdata('bdm', $result[0]->email);
                $this->session->set_userdata('t_name', $result[0]->name);
                $this->session->set_userdata('t_id', $result[0]->user_id);
                $this->session->set_userdata('t_code', $result[0]->user_code);
                $this->session->set_userdata('t_photo', $result[0]->user_photo);
                $this->session->set_userdata('type', $result[0]->user_type);
            }
            if ($result[0]->user_type == 'director') {
                $this->session->set_userdata('director', $result[0]->email);
                $this->session->set_userdata('t_name', $result[0]->name);
                $this->session->set_userdata('t_id', $result[0]->user_id);
                $this->session->set_userdata('t_code', $result[0]->user_code);
                $this->session->set_userdata('t_photo', $result[0]->user_photo);
                $this->session->set_userdata('type', $result[0]->user_type);
            }
            if ($result[0]->user_type == 'admin support') {
                $this->session->set_userdata('support', $result[0]->email);
                $this->session->set_userdata('t_name', $result[0]->name);
                $this->session->set_userdata('t_id', $result[0]->user_id);
                $this->session->set_userdata('t_code', $result[0]->user_code);
                $this->session->set_userdata('t_photo', $result[0]->user_photo);
                $this->session->set_userdata('type', $result[0]->user_type);
            }
            if ($result[0]->user_type == 'content manager') {
                $this->session->set_userdata('cm', $result[0]->email);
                $this->session->set_userdata('t_name', $result[0]->name);
                $this->session->set_userdata('t_id', $result[0]->user_id);
                $this->session->set_userdata('t_code', $result[0]->user_code);
                $this->session->set_userdata('t_photo', $result[0]->user_photo);
                $this->session->set_userdata('type', $result[0]->user_type);
            }
            if ($result[0]->user_type == 'training support') {
                $this->session->set_userdata('training', $result[0]->email);
                $this->session->set_userdata('t_name', $result[0]->name);
                $this->session->set_userdata('t_id', $result[0]->user_id);
                $this->session->set_userdata('t_code', $result[0]->user_code);
                $this->session->set_userdata('t_photo', $result[0]->user_photo);
                $this->session->set_userdata('type', $result[0]->user_type);
            }
            if ($result[0]->user_type == 'drona') {
                $this->session->set_userdata('admin', $result[0]->email);
                $this->session->set_userdata('name', $result[0]->name);
                $this->session->set_userdata('user_id', $result[0]->user_id);
                $this->session->set_userdata('user_code', $result[0]->user_code);
                $this->session->set_userdata('user_photo', $result[0]->user_photo);
                $this->session->set_userdata('type', $result[0]->user_type);
            }
            if ($result[0]->user_type == 'account') {
                $this->session->set_userdata('account', $result[0]->email);
                $this->session->set_userdata('name', $result[0]->name);
                $this->session->set_userdata('user_id', $result[0]->user_id);
                $this->session->set_userdata('user_code', $result[0]->user_code);
                $this->session->set_userdata('user_photo', $result[0]->user_photo);
                $this->session->set_userdata('type', $result[0]->user_type);
            }
            return $result[0]->user_type;
        } else {
            return 0;
        }
    }

    public function get_state() {
        $query = $this->db->query("SELECT * FROM state");
        return $query->result();
    }

    public function get_city_list($id) {
        $query = $this->db->query("SELECT * FROM cities WHERE state='" . $id . "'");
        return $query->result();
    }

    public function get_city($val) {
        $query = $this->db->query("SELECT * FROM cities WHERE state='" . $val['state'] . "'");
        $result = $query->result();
        return json_encode($result);
    }

    public function save_client() {
        $logo = 'default.png';

        $path = './assets/upload/client/';
        $img_name = $_FILES['logo']['name'];
        $tmp_name = $_FILES['logo']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $this->image_resize($file_name, $path, $path, '250');
                $logo = $file_name;
            }
        }

        $data = array('client_name' => $_POST['fname'],
            'company_id' => $_POST['company_id'],
            'industry' => $_POST['industry'],
            'logo' => $logo,
            'address' => $_POST['address'],
            'person1' => $_POST['name1'],
            'email1' => $_POST['email1'],
            'phone1' => $_POST['mobile1'],
            'phone2' => $_POST['mobile2'],
            'email2' => $_POST['email2'],
            'person2' => $_POST['name2'],
            'payment_credit_days' => $_POST['payment'],
            'invoice_type' => $_POST['invoice_type'],
            'debit_note' => $_POST['debit_note'],
            'create_date' => $this->full_date
        );

        $this->db->insert('clients', $data);

        return $this->db->insert_id();
    }

    public function import_gst() {
        $targetPath = './assets/upload/client/' . $_FILES['gstFile']['name'];
        move_uploaded_file($_FILES['gstFile']['tmp_name'], $targetPath);

        $Reader = new SpreadsheetReader($targetPath);
        $sheetCount = count($Reader->sheets());
        for ($i = 0; $i < $sheetCount; $i++) {
            $Reader->ChangeSheet($i);
            $rCount = 0;
            foreach ($Reader as $Row) {
                if ($rCount > 0 && !empty($Row[0])) {
                    $data = array('client_id' => $_POST['cid'],
                        'client_address' => $Row[0],
                        'state' => $Row[1],
                        'state_code' => $Row[2],
                        'gst_number' => $Row[3]);

                    $this->db->insert('client_gst_numbers', $data);
                }
                $rCount++;
            }
        }
        return 1;
    }

    public function addClient_gst() {
        $data = array('client_id' => $_POST['ccid'],
            'client_address' => $_POST['gstaddress'],
            'state' => $_POST['state'],
            'state_code' => $_POST['statecode'],
            'gst_number' => $_POST['gst']);

        $this->db->insert('client_gst_numbers', $data);
        return 1;
    }

    public function get_client() {
        $query = $this->db->query("SELECT clients.*, companies.company_name FROM clients, companies where companies.company_id = clients.company_id");
        return $query->result();
    }

    public function remove_client($val) {
        $this->db->query("DELETE FROM clients WHERE client_id='" . $val['cid'] . "'");
    }

    public function get_client_gst($id) {
        if (empty($id)) {
            $query = $this->db->query("SELECT c.client_name,g.* FROM client_gst_numbers g,clients c WHERE  c.client_id=g.client_id");
        } else {
            $query = $this->db->query("SELECT c.client_name,g.* FROM client_gst_numbers g,clients c WHERE g.client_id='" . $id . "' AND c.client_id=g.client_id");
        }

        return $query->result();
    }

    public function get_singleClientGst($id) {
        $query = $this->db->query("SELECT c.client_name,g.* FROM client_gst_numbers g,clients c WHERE g.client_id='" . $id . "' AND c.client_id=g.client_id");
        return $query->result();
    }

    public function get_single_client($id) {
        $query = $this->db->query("SELECT c.*,cm.company_id FROM clients c, companies cm WHERE c.client_id='" . $id . "' AND cm.company_id=c.company_id");
        return $query->result();
    }

    public function get_single_company($id) {
        $query = $this->db->query("SELECT * FROM companies  WHERE company_id='" . $id . "'");
        return $query->result();
    }

    public function update_client() {
        $logo = $_POST['cimg'];

        $path = './assets/upload/client/';
        $img_name = $_FILES['logo']['name'];
        $tmp_name = $_FILES['logo']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $this->image_resize($file_name, $path, $path, '250');
                $logo = $file_name;
            }
        }

        $data = array('client_name' => $_POST['fname'],
            'company_id' => $_POST['company_id'],
            'industry' => $_POST['industry'],
            'logo' => $logo,
            'address' => $_POST['address'],
            'person1' => $_POST['name1'],
            'email1' => $_POST['email1'],
            'phone1' => $_POST['mobile1'],
            'phone2' => $_POST['mobile2'],
            'email2' => $_POST['email2'],
            'person2' => $_POST['name2'],
            'payment_credit_days' => $_POST['payment'],
            'invoice_type' => $_POST['invoice_type'],
            'debit_note' => $_POST['debit_note']
        );

        $this->db->where('client_id', $_POST['cid']);
        $this->db->update('clients', $data);

        return 1;
    }

    public function save_pm() {
        $check = $this->db->query("SELECT email FROM application_users WHERE email='" . $_POST['email'] . "'");
        if ($check->num_rows()) {
            return 'error';
        }
        $photo = 'no-image.jpg';
        $user_code = md5(uniqid(rand()));

        $path = './assets/upload/pm/';
        $img_name = $_FILES['photo']['name'];
        $tmp_name = $_FILES['photo']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $this->image_resize($file_name, $path, $path, '250');
                $photo = $file_name;
            }
        }

        $data = array('password' => md5($_POST['password']),
            'user_code' => $user_code,
            'name' => $_POST['fname'],
            'user_type' => $_POST['user_type'],
            'email' => $_POST['email'],
            'contact_number' => $_POST['mobile'],
            'user_photo' => $photo,
            'is_active' => '1',
            'admin_approved' => '1',
            'create_date' => $this->full_date);

        $this->db->insert('application_users', $data);
        $msg = array('email' => $_POST['email'], 'password' => $_POST['password'], 'name' => $_POST['fname'], 'user_type' => $_POST['user_type']);
        new_user_mail($_POST['email'], $msg);
        return $user_code;
    }

    public function get_pm() {
        $query = $this->db->query("SELECT user_id,user_code,name,user_type,email,contact_number,user_photo,is_active,admin_approved,create_date FROM application_users WHERE user_type in ( 'project manager','business manager', 'content manager' , 'training support' , 'admin support','drona') ");
        return $query->result();
    }

    public function get_basicpm() {
        $query = $this->db->query("SELECT user_id,user_code,name FROM application_users WHERE user_type in ( 'project manager') ");
        return $query->result();
    }

    public function get_single_pm($id) {
        $query = $this->db->query("SELECT user_id,name,user_type,email,contact_number,user_photo,is_active,admin_approved,create_date FROM application_users WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function update_pm() {
        if ($_POST['c_email'] != $_POST['email']) {
            $check = $this->db->query("SELECT email FROM application_users WHERE email='" . $_POST['email'] . "'");
            if ($check->num_rows()) {
                return 'error';
            }
        }
        $photo = $_POST['cimg'];

        $path = './assets/upload/pm/';
        $img_name = $_FILES['photo']['name'];
        $tmp_name = $_FILES['photo']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $this->image_resize($file_name, $path, $path, '250');
                $photo = $file_name;
            }
        }

        $data = array(
            'name' => $_POST['fname'],
            'user_type' => $_POST['user_type'],
            'email' => $_POST['email'],
            'contact_number' => $_POST['mobile'],
            'user_photo' => $photo,
            'is_active' => '1',
            'admin_approved' => '1');

        $this->db->where('user_id', $_POST['uid']);
        $this->db->update('application_users', $data);

        return 1;
    }

    public function assign_pm($val) {
        $data = array('client_id' => $val['cid'],
            'pm_id' => $val['pm'],
            'bm_id' => $val['bm'],
            'start_date' => $this->full_date,
            'status' => 1);

        $check = $this->db->query("SELECT * FROM client_projectmanagers WHERE client_id='" . $val['cid'] . "'");
        if ($check->num_rows()) {
            $this->db->where('client_id', $val['cid']);
            $this->db->update('client_projectmanagers', $data);
        } else {
            $this->db->insert('client_projectmanagers', $data);
        }

        $this->db->query("UPDATE clients SET status='1' WHERE client_id='" . $val['cid'] . "'");
    }

    public function remove_user($val) {
        $this->db->query("DELETE FROM application_users WHERE user_id='" . $val['uid'] . "'");
    }

    public function check_client_assign($id) {
        $query = $this->db->query("SELECT pm.user_id,pm.user_code,pm.name,u.name as bname,u.user_code as buser_code FROM client_projectmanagers cp INNER JOIN application_users u ON cp.bm_id=u.user_code, application_users pm WHERE cp.client_id='" . $id . "' AND pm.user_code=cp.pm_id");
        return $query->result();
    }

    public function get_pm_client_list($val) {
        $query = $this->db->query("SELECT c.client_name FROM client_projectmanagers cp, clients c WHERE cp.pm_id='" . $val['pm'] . "' AND c.client_id=cp.client_id");
        $result = $query->result();
        echo json_encode($result);
    }

    public function get_client_assign_pending() {
        $query = $this->db->query("SELECT client_id,client_name FROM clients WHERE status='0'");
        return $query->result();
    }

    public function save_trainer() {

        $check = $this->db->query("SELECT email FROM application_users WHERE email='" . $_POST['email'] . "'");
        if ($check->num_rows()) {
            return 'error';
        }
        $photo = 'no-image.jpg';
        $user_code = md5(uniqid(rand()));
        $unencrypt = substr($_POST['mobile'], -6);
        $password = md5($unencrypt);

        $data = array('user_code' => $user_code,
            'user_type' => 'trainer',
            'password' => $password,
            'app_password' => $password,
            'name' => $_POST['pname'],
            'address' => $_POST['address1'],
            'city' => $_POST['city'],
            'state' => $_POST['state'],
            'country' => $_POST['country'],
            'contact_number' => $_POST['mobile'],
            'contact_number_landline' => $_POST['landline'],
            'user_dob' => $_POST['dob'],
            'user_gender' => $_POST['gender'],
            'email' => $_POST['email'],
            'skypeID' => $_POST['skype'],
            'create_date' => $this->full_date,
            'is_active' => '1',
            'admin_approved' => '1',
            'user_rating' => $_POST['rate'],
            'admin_comments' => $_POST['comments'],
            'user_photo' => $photo,
        );

        $this->db->insert('application_users', $data);
        $msg = array('email' => $_POST['email'], 'password' => $unencrypt, 'name' => $_POST['pname']);
        new_trainer_mail($_POST['email'], $msg);
        return 1;
    }

    public function update_trainer() {
        if ($_POST['c_email'] != $_POST['email']) {
            $check = $this->db->query("SELECT email FROM application_users WHERE email='" . $_POST['email'] . "'");
            if ($check->num_rows()) {
                return 'error';
            }
        }

        $photo = $_POST['cimg'];
        $cv = $_POST['c_cv'];

        $path = './assets/upload/trainer/';
        $img_name = $_FILES['photo']['name'];
        $tmp_name = $_FILES['photo']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $this->image_resize($file_name, $path, $path, '250');
                $photo = $file_name;
            }
        }

        $img_name = $_FILES['cv']['name'];
        $tmp_name = $_FILES['cv']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $cv = $file_name;
            }
        }
        $occupation = "";
        if (isset($_POST["occupation"])) {
            $occupation = $_POST["occupation"];
        }
        $specification = "";
        if (isset($_POST["specification"])) {
            $specification = implode(',', $_POST['specification']);
        }
        if (!empty($_POST['industry']))
            $industry = implode(',', $_POST['industry']);
        else
            $industry = "";

        if ($_POST['fees'] == "")
            $fees = 0;
        else
            $fees = $_POST['fees'];

        $data = array(
            'name' => $_POST['pname'],
            'address' => $_POST['address1'],
            'city' => $_POST['city'],
            'state' => $_POST['state'],
            'country' => $_POST['country'],
            'contact_number' => $_POST['mobile'],
            'contact_number_landline' => $_POST['landline'],
            'user_dob' => $_POST['dob'],
            'user_gender' => $_POST['gender'],
            'email' => $_POST['email'],
            'skypeID' => $_POST['skype'],
            'user_photo' => $photo,
            'user_cv' => $cv,
            'is_active' => '1',
            'freelancer' => $occupation,
            'trainer_area' => $specification,
            'industry' => $industry,
            'expected_fee_per_day' => $fees,
            'trainer_credit' => $_POST['trainer_credit'],
            'admin_approved' => '1',
        );

        $this->db->where('user_code', $_POST['uid']);
        $this->db->update('application_users', $data);

        if (!empty($_POST['password'])) {
            $pass = md5($_POST['password']);
            $this->db->query("UPDATE application_users SET password='" . $pass . "' WHERE user_code='" . $_POST['uid'] . "'");
        }

        return 1;
    }

    public function get_trainer() {
        $p = 0;
        if (isset($_GET['page']) && $this->session->userdata('TRAINERQUERY')) {
            $p = $_GET['page'];
            $queryString = $this->session->userdata('TRAINERQUERY');
        } else {
            $queryString = "FROM application_users WHERE user_type='trainer' AND is_active='1'";
            $this->session->set_userdata('TRAINERQUERY', $queryString);
        }
        $lmt = "LIMIT 20 OFFSET 0";
        if ($p > 1) {
            $p = ($p - 1) * 20;
            $lmt = "LIMIT 20 OFFSET " . $p;
        }
        $query = $this->db->query("SELECT user_id,user_code,name,trainer_area,email,industry,contact_number,state,city,create_date,is_active,admin_approved,user_photo $queryString ORDER BY create_date DESC $lmt");
        return $query->result();
    }

    public function getTrainerCount() {
        $queryString = $this->session->userdata('TRAINERQUERY');
        $query = $this->db->query("SELECT COUNT(user_id) as total $queryString");
        return $query->result();
    }

    public function get_inactive_trainer() {
        $query = $this->db->query("SELECT user_id,user_code,name,trainer_area,email,contact_number,state,city,create_date,is_active,admin_approved,user_photo FROM application_users WHERE user_type='trainer' AND is_active='0' AND admin_approved='1' ORDER BY create_date DESC");
        return $query->result();
    }

    public function get_pending_trainer() {
        $query = $this->db->query("SELECT user_id,user_code,name,trainer_area,email,contact_number,state,city,create_date,is_active,admin_approved,user_photo FROM application_users WHERE user_type='trainer' AND admin_approved='0' ORDER BY create_date DESC");
        return $query->result();
    }

    public function get_program_title($val) {
        $query = $this->db->query("SELECT program FROM program_titles WHERE area='" . $val['area'] . "'");
        $result = $query->result();
        echo json_encode($result);
    }

    public function get_single_trainer($id) {
        $query = $this->db->query("SELECT * FROM application_users WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function get_trainer_workExperiance($id) {
        $query = $this->db->query("SELECT * FROM trainer_work_experiance WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function trainer_education($id) {
        $query = $this->db->query("SELECT * FROM trainer_education WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function trainer_certifications($id) {
        $query = $this->db->query("SELECT * FROM trainer_certifications WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function trainer_bankDetails($id) {
        $query = $this->db->query("SELECT * FROM trainer_bank WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function trainer_make_approve($val) {
        $this->db->query("UPDATE application_users SET is_active='1', admin_approved='1',user_rating='" . $val['rate'] . "',admin_comments='" . $val['comm'] . "' WHERE user_code='" . $val['tid'] . "'");
        $query = $this->db->query("SELECT email, name,contact_number FROM application_users WHERE user_code='" . $val['tid'] . "'");
        $result = $query->result();
        $data = array('name' => $result[0]->name, 'email' => $result[0]->email);
        //trainer_approval_mail($result[0]->email, $data);
        $text = "Your Wagons Learning account is now active.We have emailed credentials to access member portal. Call us on 020-65606055 for any help.";
        send_sms($result[0]->contact_number, $text);
    }

    public function get_basic_trainer($id) {
        $query = $this->db->query("SELECT name,email,contact_number,user_code FROM application_users WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function get_trainer_engagement($id) {
        $query = $this->db->query("SELECT eg.*, p.project_title,p.location_of_training, p.training_start_date FROM training_engagements eg, training_projects p WHERE eg.user_code='" . $id . "' AND p.project_id=eg.project_id ORDER BY p.training_start_date DESC");
        return $query->result();
    }

    public function add_trainer_work() {
        $data = array('user_code' => $_POST['user'],
            'work_designation' => $_POST['designation'],
            'work_function' => '',
            'work_industry' => implode('/', $_POST['industry']),
            'work_period_from' => $_POST['pfrom'],
            'work_orgn' => $_POST['org'],
            'up_date' => $this->full_date,
            'work_period_to' => $_POST['pto']);

        $this->db->insert('trainer_work_experiance', $data);

        return 1;
    }

    public function update_trainer_work() {
        $data = array(
            'work_designation' => $_POST['designation'],
            'work_function' => '',
            'work_industry' => implode('/', $_POST['industry']),
            'work_period_from' => $_POST['pfrom'],
            'work_orgn' => $_POST['org'],
            'up_date' => $this->full_date,
            'work_period_to' => $_POST['pto']);

        $this->db->where('id', $_POST['id']);
        $this->db->update('trainer_work_experiance', $data);

        return 1;
    }

    public function save_bankDetails() {

        $path = './assets/upload/pan/';
        $img_name = $_FILES['pan_image']['name'];
        $tmp_name = $_FILES['pan_image']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $data = array('user_code' => $_POST['user'],
            'account_name' => $_POST['aname'],
            'bank_name' => $_POST['bname'],
            'bank_account' => $_POST['anumber'],
            'bank_ifsc' => $_POST['ifsc'],
            'bank_branch' => $_POST['branch'],
            'pan_no' => $_POST['pan_no'],
            'pan_image' => $file
        );


        $check = $this->db->query("SELECT * FROM trainer_bank WHERE user_code='" . $_POST['user'] . "'");
        if ($check->num_rows()) {
            $this->db->where('user_code', $_POST['user']);
            $this->db->update('trainer_bank', $data);
        } else {
            $this->db->insert('trainer_bank', $data);
        }

        return 1;
    }

    public function add_trainer_experience() {
        $data = array('user_code' => $_POST['user'],
            'area_of_training' => $_POST['training_area'],
            'program_title' => $_POST['program'],
            'company' => $_POST['company'],
            'industry' => implode('/', $_POST['industry']),
            'up_date' => $this->full_date);

        $this->db->insert('trainer_training_experiance', $data);

        return 1;
    }

    public function get_trainer_experience($id) {
        $query = $this->db->query("SELECT * FROM trainer_training_experiance WHERE user_code='" . $id . "'");
        return $query->result();
    }

    public function get_single_trainer_work($code, $id) {
        $query = $this->db->query("SELECT * FROM trainer_work_experiance WHERE user_code='" . $code . "' AND id='" . $id . "'");
        return $query->result();
    }

    public function add_trainer_education() {
        $edu = explode(',', $_POST['qualificatioin']);
        $data = array('user_code' => $_POST['user'],
            'qualification' => $edu[0],
            'discipline' => $edu[1],
            'university' => $_POST['university'],
            'year_of_passing' => $_POST['year'],
            'up_date' => $this->full_date);

        $this->db->insert('trainer_education', $data);

        return 1;
    }

    public function add_trainer_certification() {
        $data = array('user_code' => $_POST['user'],
            'certification_in' => $_POST['title'],
            'certification_level' => $_POST['level'],
            'awarded_by' => $_POST['award'],
            'valid_upto' => $_POST['valid'],
            'up_date' => $this->full_date);

        $this->db->insert('trainer_certifications', $data);

        return 1;
    }

    public function get_single_program($id) {
        $query = $this->db->query("SELECT * FROM training_projects p, clients c WHERE p.project_id='" . $id . "' AND c.client_id=p.client_id");
        return $query->result();
    }

    public function get_basic_programs() {
        $today = date('Y-m-d');

        $filter = "";
        $filter1 = "";
        if (isset($_POST['pclient'])) {
            if (isset($_POST['title'])) {
                if (!empty($_POST['title'])) {
                    $filter .= " AND p.project_title LIKE '%" . $_POST['title'] . "%' ";
                }
                if (!empty($_POST['location'])) {
                    $filter .= " AND p.location_of_training LIKE '%" . $_POST['location'] . "%' ";
                }
            }
            if ($_POST['pclient'] != 'All') {
                $filter .= " AND p.client_id='" . $_POST['pclient'] . "'";
            }

            if (!empty($_POST['sdate'])) {
                $filter .=" AND DATE(p.training_start_date)='" . $_POST['sdate'] . "' ";
            }
        }


        $p = 0;
        if (isset($_GET['page']) && $this->session->userdata('UPQUERY')) {
            $p = $_GET['page'];
            $queryString = $this->session->userdata('UPQUERY');
        } else {
            $queryString = "FROM training_projects p, clients c WHERE p.training_start_date>='" . $today . "' AND p.is_active!='0' $filter AND c.client_id=p.client_id ";
            $this->session->set_userdata('UPQUERY', $queryString);
        }
        $lmt = "LIMIT 20 OFFSET 0";
        if ($p > 1) {
            $p = ($p - 1) * 20;
            $lmt = "LIMIT 20 OFFSET " . $p;
        }
        $query = $this->db->query("SELECT p.project_id,p.user_code,p.project_title,p.location_of_training,p.training_start_date,p.training_end_date,p.is_active,p.trainer_engage_flag,p.create_date,c.client_id,c.client_name, p.half_day $queryString ORDER BY p.training_start_date ASC $lmt");
        return $query->result();
    }

    public function getUpCount() {
        $queryString = $this->session->userdata('UPQUERY');
        $query = $this->db->query("SELECT COUNT(p.project_id) as total $queryString");
        return $query->result();
    }

    public function get_basic_past_programs() {
        $today = date('Y-m-d');
        $filter = "";
        $filter1 = " LEFT JOIN program_trainers t ON p.project_id=t.project_id ";
        if (isset($_POST['pclient'])) {
            if (isset($_POST['title'])) {
                if (!empty($_POST['title'])) {
                    $filter .= " AND p.project_title LIKE '%" . $_POST['title'] . "%' ";
                }
                if (!empty($_POST['location'])) {
                    $filter .= " AND p.location_of_training LIKE '%" . $_POST['location'] . "%' ";
                }
            }
            if ($_POST['pclient'] != 'All') {
                $filter .= " AND p.client_id='" . $_POST['pclient'] . "'";
            }

            if (!empty($_POST['date'])) {
                $filter .=" AND DATE(p.training_start_date)='" . $_POST['date'] . "' ";
            }
            if (isset($_POST['tinvoice'])) {

                if ($_POST['pm'] != 'All') {
                    $filter .= " AND p.user_code='" . $_POST['pm'] . "'";
                }
                if ($_POST['cinvoice'] != 'All') {
                    $filter .= " AND p.client_invoice_raised='" . $_POST['cinvoice'] . "'";
                }
                if ($_POST['tinvoice'] != 'All') {
                    $filter1 = " INNER JOIN program_trainers t ON p.project_id=t.project_id ";
                    if ($_POST['tinvoice'] == '0') {
                        $filter1 .= " AND t.trainer_invoice_flag='0' ";
                    }
                    if ($_POST['tinvoice'] == '1') {
                        $filter1 .= " AND t.trainer_invoice_flag='1' AND ( t.trainer_invoice_action IS NULL || t.trainer_invoice_action='0')";
                    }
                    if ($_POST['tinvoice'] == '2') {
                        $filter1 .= " AND t.trainer_invoice_flag='1' AND ( t.trainer_invoice_action='1' || t.trainer_invoice_action='2')";
                    }
                    if ($_POST['tinvoice'] == '3') {
                        $filter1 .= " AND t.trainer_invoice_action='1' AND t.trainer_paid_status='0' ";
                    }
                    if ($_POST['tinvoice'] == '4') {
                        $filter1 .= " AND t.trainer_invoice_action='1' AND t.trainer_paid_status='1' ";
                    }
                }
            }
        }

        $p = 0;
        if (isset($_GET['page']) && $this->session->userdata('PASTQUERY')) {
            $p = $_GET['page'];
            $queryString = $this->session->userdata('PASTQUERY');
        } else {
            $queryString = "FROM training_projects p  $filter1, clients c WHERE p.training_start_date<'" . $today . "' $filter AND p.is_active='1' AND c.client_id=p.client_id";
            $this->session->set_userdata('PASTQUERY', $queryString);
        }
        $lmt = "LIMIT 20 OFFSET 0";
        if ($p > 1) {
            $p = ($p - 1) * 20;
            $lmt = "LIMIT 20 OFFSET " . $p;
        }

        $query = $this->db->query("SELECT p.project_id,p.client_id,p.user_code,p.project_title,p.location_of_training,p.training_start_date,p.training_end_date,p.is_active,p.create_date,p.project_type,c.client_id,c.client_name,t.trainer_invoice_flag,t.trainer_id,t.trainer_paid_status,t.trainer_invoice_action, p.client_invoice_raised, p.half_day $queryString  ORDER BY p.project_id DESC $lmt");

        return $query->result();
    }

    public function getPastCount() {
        $queryString = $this->session->userdata('PASTQUERY');
        $query = $this->db->query("SELECT COUNT(p.project_id) as total $queryString");
        return $query->result();
    }

    public function accept_request($val) {
        if (isset($val['assign'])) {
            $data1 = array('project_id' => $val['pid'],
                'user_code' => $val['uid'],
                'status' => '1',
                'admin_approved' => '1',
                'approval_date' => $this->full_date,
                'apply_date' => $this->full_date);

            $this->db->insert('training_engagements', $data1);
            $val['rid'] = $this->db->insert_id();
        }
        $this->db->query("UPDATE training_engagements SET status='1', admin_approved='1', approval_date='" . $this->full_date . "' WHERE id='" . $val['rid'] . "'");
        $this->db->query("UPDATE training_projects SET trainer_engage_flag='1' WHERE project_id='" . $val['pid'] . "'");

        $query = $this->db->query("SELECT client_charges FROM training_projects WHERE project_id='" . $val['pid'] . "'");
        $result = $query->result();
        $client_amt = $result[0]->client_charges;


        $data = array('project_id' => $val['pid'],
            'trainer_id' => $val['uid'],
            'training_date_from' => $val['from'],
            'trainer_date_to' => $val['to'],
            'amount' => $val['amt'],
            'in_amount' => $client_amt);

        $this->db->insert('program_trainers', $data);
        $ptid = $this->db->insert_id();
        //$desc = mysql_real_escape_string($val['trainer_brief']);

        if ($val['from'] != $val['to']) {
            $date1 = $val['from'];
            $date2 = $val['to'];
            $period = new DatePeriod(
                    new DateTime($date1), new DateInterval('P1D'), new DateTime($date2)
            );

            foreach ($period as $dval) {
                $date = $dval->format('Y-m-d');
                $dayofweek = date('w', strtotime($date));
                if ($dayofweek != '0') {
                    $data = array('project_id' => $val['pid'],
                        'trainer_id' => $val['uid'],
                        'trainer_brief' => $val['trainer_brief'],
                        'ttt_required' => $val['tr'],
                        'program_date' => $date);

                    $this->db->insert('trainer_assigned', $data);
                }
            }
            $data = array('project_id' => $val['pid'],
                'trainer_id' => $val['uid'],
                'trainer_brief' => $val['trainer_brief'],
                'ttt_required' => $val['tr'],
                'program_date' => $date2);

            $this->db->insert('trainer_assigned', $data);
        } else {

            $data = array('project_id' => $val['pid'],
                'trainer_id' => $val['uid'],
                'trainer_brief' => $val['trainer_brief'],
                'ttt_required' => $val['tr'],
                'program_date' => $val['from']);

            $this->db->insert('trainer_assigned', $data);
        }

        $trainer = $this->get_basic_trainer($val['uid']);

        $tMobile = $trainer[0]->contact_number;
        $tCode = $trainer[0]->user_code;

        $aPass = substr($tMobile, -4);

        $this->db->query("UPDATE application_users SET app_password='" . $aPass . "' WHERE user_code='" . $tCode . "'");

        $program = $this->get_basic_program_detail($val['pid']);


        $msg = array('pname' => $program[0]->project_title, 'pdate' => $program[0]->training_start_date, 'ploc' => $program[0]->location_of_training, 'email' => $trainer[0]->email, 'name' => $trainer[0]->name, 'status' => 'Approved');

        $note = array('user' => $trainer[0]->user_code,
            'project' => $program[0]->project_id,
            'task' => 0,
            'type' => 'Accept Request',
            'due_date' => $ptid,
            'text' => "Your Engagement Request for " . $program[0]->project_title . " was Accepted and You have been selected to conduct the program on " . date_formate_short($program[0]->training_start_date) . ".");
        $this->push_notification($note);

        $pmQuery = $this->db->query("SELECT u.name,u.contact_number FROM training_projects p,application_users u WHERE p.project_id='" . $program[0]->project_id . "' AND u.user_code=p.user_code");
        $pmResult = $pmQuery->result();
        $dates = $val['from'];
        if ($val['from'] != $val['to']) {
            $dates = $val['from'] . " to " . $val['to'];
        }
        $ebody = "Your request for program engagment is approved. <br /> 3.	Login to your trainer portal regularly till the completion of the program to get regular updates. <br/> 4.	Any update / alterations to the training program in the system will be notified by email kindly ensure you are connected to your emails all the time.";
        $ebody .="<br/><br/><b>Terms & Conditions</b><br/>";
        $ebody . "1. In case the trainer needs to submit physical forms like attendance sheet ,feedback forms , assessments etc and/or petrol bills along with their physical invoice- Their payment period of 45 days would start from the date we receive their physical invoices along with required support documents & /or required forms in our Pune Office.";
        $ebody . "2. In case there is no reimbursement & /or any formats to be submitted  and only invoice needs to be updated in CRM-  45th day would be counted from the date the complete and correct invoice is uploaded on the CRM.";
        $ebody . "3. Also, please note that for fuel claims, the petrol/diesel/CNG bills needs to be sent to Pune office for reimbursement. This bills should of any prior dates of the training program.";
        $eData = array('title' => 'Program engagment request approved',
            'body' => $ebody,
            'project' => $program[0]->project_title,
            'date' => $dates,
            'fees' => $val['amt'],
            'trainer' => $trainer[0]->user_code,
            'pid' => $ptid,
            'pm' => $pmResult[0]->name,
            'mobile' => $pmResult[0]->contact_number,
            'location' => $program[0]->location_of_training);

        mail_to_trainer($trainer[0]->email, $eData);
        // mail_to_trainer('anil@eneblur.com', $eData);

        $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $program[0]->project_id . "' AND notification_type='Trainer Engage'");

        $this->db->query("UPDATE training_engagements SET status='0' WHERE project_id='" . $val['pid'] . "' AND id!='" . $val['rid'] . "'");
        $query = $this->db->query("SELECT u.email FROM training_engagements e,application_users u WHERE e.project_id='" . $val['pid'] . "' AND e.id!='" . $val['rid'] . "' AND u.user_code=e.user_code");
        $result = $query->result();
        if (!empty($result)) {
            foreach ($result as $rsData) {
                $eData = array('title' => 'Program request is rejected',
                    'body' => 'Your request for following program is rejected.',
                    'project' => $program[0]->project_title,
                    'date' => $program[0]->training_start_date,
                    'location' => $program[0]->location_of_training);

                mail_to_trainer($rsData->email, $eData);
            }
        }
    }

    public function get_basic_program_detail($id) {
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date, p.location_of_training FROM training_projects p WHERE p.project_id='" . $id . "'");
        return $query->result();
    }

    public function reject_request($val) {
        $this->db->query("UPDATE training_engagements SET status='0', reject_date='" . $this->full_date . "' WHERE id='" . $val['rid'] . "'");
    }

    public function get_assigned_trainers($id) {
        $query = $this->db->query("SELECT u.user_code,u.name FROM program_trainers p, application_users u WHERE p.project_id='" . $id . "' AND u.user_code=p.trainer_id");
        return $query->result();
    }

    public function get_progrom_date() {
        $month = date('m');
        $query = $this->db->query("SELECT project_id,project_title,training_start_date,training_duration,trainer_engage_flag,excl_sun,excl_sat, location_of_training, c.client_name FROM training_projects, clients c WHERE MONTH(training_start_date)='" . $month . "' AND is_active!='0' AND c.client_id=training_projects.client_id");
        return $query->result();
    }

    public function save_trainer_calendar() {
        $data = array('trainer_id' => $_POST['date_user'],
            'tc_title' => $_POST['title'],
            'from_date' => $_POST['from'],
            'to_date' => $_POST['to']);

        $this->db->insert('trainer_calendar', $data);

        return 'Calendar';
    }
    
      public function save_trainer_tds_certificate() {
        $path = './assets/upload/tdscertificate/';
        $config['allowed_types'] = 'gif|jpg|jpeg|png|doc|docx|pdf';
        $img_name = $_FILES['file']['name'];
        $tmp_name = $_FILES['file']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }
         $data = array('trainer_id' => $_POST['trainer_id'],
                        //'tt_id' => $_POST['tt_id'],
                      'email' => $_POST['email'],
                       'tt_year' => $_POST['year'],
                       'tt_quater' => $_POST['quater'],
                       'tds_file' => $file
                );
        $this->db->insert('trainer_tds', $data);
        
        $data= array('email' => $_POST['email'], 'tt_quater' => $_POST['quater'], 'tt_year' => $_POST['year']);
        tds_certificate_mail($_POST['email'], $data);
        return 1;
        
    }
    
    public function get_all_tds_certificate($id){
        $query = $this->db->query("SELECT * FROM trainer_tds WHERE trainer_id='" . $id .  "'");
        return $query->result();
    }
    public function remove_tds_certificate($id) {
    $this->db->query("DELETE FROM trainer_tds WHERE tt_id='" . $_POST['id'] . "'");
    }
    public function get_trainer_calendor($id) {
        $query = $this->db->query("SELECT * FROM trainer_calendar WHERE trainer_id='" . $id . "'");
        return $query->result();
    }

    public function remove_work_experience($val) {
        $this->db->query("DELETE FROM trainer_work_experiance WHERE id='" . $val['rid'] . "'");
    }

    public function remove_training_experience($val) {
        $this->db->query("DELETE FROM trainer_training_experiance WHERE id='" . $val['rid'] . "'");
    }

    public function remove_education($val) {
        $this->db->query("DELETE FROM trainer_education WHERE id='" . $val['rid'] . "'");
    }

    public function remove_certification($val) {
        $this->db->query("DELETE FROM trainer_certifications WHERE id='" . $val['rid'] . "'");
    }

    public function search_trainer($val) {
        $filter = '';

        if ($val['state'] != '') {
            $filter .= " AND state='" . $val['state'] . "' ";
        }
        if ($val['city'] != '') {
            $filter .= " AND city='" . $val['city'] . "' ";
        }
        if ($val['industry'] != '') {
            $filter .= " AND industry LIKE'%" . $val['industry'] . "%' ";
        }
        if ($val['age'] != '') {
            $age_data = explode('-', $val['age']);
            $filter .= " AND (date_format(now(),'%Y') - date_format(user_dob,'%Y')) BETWEEN '" . $age_data[0] . "' and '" . $age_data[1] . "' ";
        }
        if ($val['exp']) {
            $exp = explode('-', $val['exp']);
            $filter .= " AND total_experiance_training BETWEEN '" . $exp[0] . "' and '" . $exp[1] . "' ";
        }
        if ($val['name'] != '') {
            $filter .= " AND name LIKE'%" . $val['name'] . "%' ";
        }
        if ($val['keyword'] != '') {
            $filter .= " AND cv_content LIKE'%" . $val['keyword'] . "%' ";
        }

        $this->session->set_userdata('search_filter', $filter);

        $query = $this->db->query("SELECT name,company_name,city,total_experiance_training,date_format(now(),'%Y') - date_format(user_dob,'%Y') as age, user_code FROM application_users WHERE user_type='trainer' AND is_active='1' " . $filter . " ORDER BY name ");
        $result = $query->result();
        echo json_encode($result);
    }

    public function save_content() {
        $path = './assets/upload/content/';
        $img_name = $_FILES['content']['name'];
        $tmp_name = $_FILES['content']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $project = '0';
        $client = '0';



        $data = array('filename' => $_POST['title'],
            'file_path' => $file,
            'project_id' => $project,
            'client_id' => $client,
            'upload_by' => $_POST['content_user'],
            'upload_date' => $this->cdate);

        $this->db->insert('training_content', $data);

        return 1;
    }

    public function get_all_content() {
        $query = $this->db->query("SELECT c.*, u.name,u.user_type,u.user_code FROM training_content c,application_users u WHERE u.user_code=c.upload_by ORDER BY c.id");
        return $query->result();
    }

    public function remove_content($val) {
        $this->db->query("DELETE FROM training_content WHERE id='" . $val['rid'] . "'");
    }

    public function forgot_password($val) {
        $query = $this->db->query("SELECT user_code,email FROM application_users WHERE email='" . $val['email'] . "'");

        if ($query->num_rows()) {
            $result = $query->result();
            $link = 'admin/update_password/' . $result[0]->user_code . '/' . custome_encode($result[0]->email);
            forgot_password_mail($val['email'], $link);
            return 1;
        }
        return 0;
    }

    public function update_password($val) {
        $query = $this->db->query("SELECT user_code,email FROM application_users WHERE email='" . $val['username'] . "' AND user_code='" . $val['user_code'] . "'");
        //return $this->db->last_query();
        if ($query->num_rows()) {
            $pass = md5($val['password']);
            $this->db->query("UPDATE application_users SET password='" . $pass . "' WHERE user_code='" . $val['user_code'] . "'");
            return 1;
        }
        return 0;
    }

    public function get_all_payment() {
        $pay = "";
        $date = "";
        $user = "";
        $project = "";
        if (isset($_POST['status'])) {
            if ($_POST['status'] != 'All') {
                $pay = " AND pt.trainer_paid_status='" . $_POST['status'] . "' ";
            }

            if ($_POST['trainer'] != '') {
                $user = " AND u.name LIKE '%" . $_POST['trainer'] . "%' ";
            }

            if ($_POST['title'] != '') {
                $project = " AND p.project_title LIKE '%" . $_POST['title'] . "%' ";
            }
        }

        if (isset($_POST['sdate']) && isset($_POST['tdate'])) {
            if (!empty($_POST['sdate']) && !empty($_POST['tdate'])) {
                $date = " AND DATE(pt.training_date_from)>= '" . $_POST['sdate'] . "' AND DATE(pt.training_date_from)<= '" . $_POST['tdate'] . "' ";
            }
        }

        $p = 0;
        if (isset($_GET['page']) && $this->session->userdata('TINVOICE')) {
            $p = $_GET['page'];
            $queryString = $this->session->userdata('TINVOICE');
        } else {
            $queryString = "FROM program_trainers pt INNER JOIN application_users u ON  pt.trainer_id=u.user_code $user, training_projects p INNER JOIN clients c ON p.client_id=c.client_id WHERE pt.trainer_invoice_flag='1' AND pt.trainer_invoice_action='1' $pay $date AND p.project_id=pt.project_id $project";
            $this->session->set_userdata('TINVOICE', $queryString);
        }
        $lmt = "LIMIT 20 OFFSET 0";
        if ($p > 1) {
            $p = ($p - 1) * 20;
            $lmt = "LIMIT 20 OFFSET " . $p;
        }

        $query = $this->db->query("SELECT pt.*,p.project_id,p.project_title,p.location_of_training,c.client_name,u.user_code,u.name $queryString ORDER BY p.training_start_date DESC $lmt");
        return $query->result();
    }

    public function getTInvoiceCount() {
        $queryString = $this->session->userdata('TINVOICE');
        $query = $this->db->query("SELECT COUNT(pt.id) as total $queryString");
        return $query->result();
    }

    public function get_PM_payment() {
        $query = $this->db->query("SELECT pt.*,p.project_id,p.project_title, p.training_start_date, p.location_of_training, u.user_code,u.name,u.trainer_credit, clients.client_name FROM program_trainers pt INNER JOIN training_projects p ON pt.project_id=p.project_id INNER JOIN application_users u ON  pt.trainer_id=u.user_code, clients WHERE clients.client_id = p.client_id AND pt.trainer_invoice_flag='1' ORDER BY p.training_start_date DESC");
        return $query->result();
    }

    //Admin processing trainer Payment, Inform Trainer and PM
    public function update_payment() {
        $data = array('trainer_paid_status' => '1',
            'trainer_payment_date' => $_POST['date']);
        $this->db->where('id', $_POST['pid']);
        $this->db->update('program_trainers', $data);
        $file = "";
        $path = './assets/upload/content/';
        $img_name = $_FILES['advice']['name'];
        $tmp_name = $_FILES['advice']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $data = array('p_id' => $_POST['pid'],
            'tp_amt' => $_POST['amt'],
            'tp_pay_date' => $_POST['date'],
            'tp_pay_id' => $_POST['paymentid'],
            'tp_advice' => $file,
            'tp_date' => $this->cdate);

        $this->db->insert('trainer_payment', $data);

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training, c.client_name FROM training_projects p, clients c  WHERE p.project_id='" . $_POST['project_id'] . "' AND c.client_id=p.client_id");
        $project = $query->result();

        $query2 = $this->db->query("SELECT u.name,u.email,u.user_code FROM program_trainers p, application_users u WHERE p.id='" . $_POST['pid'] . "' AND u.user_code=p.trainer_id");
        $result = $query2->result();
        $eUid = $result[0]->user_code;
        $email = '';
        foreach ($result as $da) {
            $email .=$da->email . ',';
        }
        $email = rtrim($email, ',');

        $query3 = $this->db->query("SELECT u.email,u.user_code FROM application_users u, training_projects p WHERE p.project_id='" . $_POST['project_id'] . "' AND u.user_code=p.user_code");
        $result = $query3->result();
        $pm_email = $result[0]->email;
        $pmCode = $result[0]->user_code;

        $email .=',' . $pm_email;

        $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $project[0]->project_id . "' AND notification_type='TS Confirm Invoice'");

        $data = array('project' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'location' => $project[0]->location_of_training,
            'company' => $project[0]->client_name,
            'status' => 'InvoicePaid');

        $note = array('user' => $eUid,
            'project' => $project[0]->project_id,
            'task' => 0,
            'type' => 'Admin Paid to trainer',
            'due_date' => '',
            'text' => "Payment has been processed against Your Invoice by Accounts Dept.");
        $this->push_notification($note);

        $note = array('user' => $pmCode,
            'project' => $project[0]->project_id,
            'task' => 0,
            'type' => 'Admin Paid to trainer',
            'due_date' => '',
            'text' => "Payment has been processed against Trainer Invoice by Accounts Dept.");
        $this->push_notification($note);

        trainer_invoice_paid($email, $data);
    }

    public function change_password($val) {
        $old = md5($val['old_pass']);
        $check = $this->db->query("SELECT user_code FROM application_users WHERE password='" . $old . "' AND user_code='" . $val['uid'] . "'");
        if ($check->num_rows()) {
            $new = md5($val['new_pass']);
            $check = $this->db->query("UPDATE  application_users SET password='" . $new . "' WHERE  user_code='" . $val['uid'] . "'");
            return 1;
        }

        return 0;
    }

    public function get_request_details($id) {
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training,p.training_duration, c.client_name,c.email1,c.phone1,u.email,u.name,u.company_name,u.city, u.user_code FROM training_engagements te INNER JOIN application_users u ON te.user_code=u.user_code, training_projects p , clients c  WHERE te.id='" . $id . "' AND p.project_id=te.project_id AND c.client_id=p.client_id");
        return $query->result();
    }

    public function save_review() {
        if ($_POST['user_type'] == 'BDM') {
            $this->db->query("UPDATE training_engagements SET bdm_comments='" . $_POST['comment'] . "',status='3' WHERE id='" . $_POST['id'] . "'");
        }
        if ($_POST['user_type'] == 'Client') {
            $this->db->query("UPDATE training_engagements SET client_comments='" . $_POST['comment'] . "',status='4' WHERE id='" . $_POST['id'] . "'");
        }
        return 1;
    }

    public function getCheckin($pid, $tid) {
        $query = $this->db->query("SELECT * FROM trainer_assigned WHERE project_id='" . $pid . "' AND trainer_id='" . $tid . "'");
        //return $this->db->last_query();
        return $query->result();
    }

    public function get_basic_client() {
        $query = $this->db->query("SELECT client_id,client_name FROM clients ORDER BY client_name");
        return $query->result();
    }

    public function get_basic_project($id) {
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,p.training_end_date,p.training_duration,p.half_day,SUM(f.in_amount) as total_fees,p.venue, p.project_type, p.duration_display FROM training_projects p, program_trainers f WHERE p.project_id='" . $id . "' AND f.project_id=p.project_id");
        return $query->result();
    }

    public function get_project_debit($id) {
        $query = $this->db->query("SELECT * FROM debit_note WHERE project='" . $id . "'");
        return $query->result();
    }

    public function get_project_expense($id) {
        $query = $this->db->query("SELECT * FROM program_expenses WHERE project_id='" . $id . "'");
        return $query->result();
    }

    public function get_project_expenseForInvoie($id) {
        $query = $this->db->query("SELECT * FROM program_expenses WHERE project_id='" . $id . "' AND absorb='N' AND in_amount!='0' ");
        return $query->result();
    }

    public function get_client_basicProject($id) {
        $query = $this->db->query("SELECT p.project_id,p.project_title,p.location_of_training,p.training_start_date,p.training_duration FROM training_projects p WHERE p.client_id='" . $id . "' ORDER BY p.training_start_date DESC");
        return $query->result();
    }

    public function creat_pInvoice() {
        $this->db->query("UPDATE training_projects SET client_invoice_raised='Y',client_invoice_date='" . $this->cdate . "' WHERE project_id='" . $_POST['project'] . "'");
        $query = $this->db->query("SELECT cg.*,c.person1,c.email1,c.debit_note,(SELECT client_charges FROM training_projects WHERE project_id='" . $_POST['project'] . "') as charge, p.contact_person,p.contact_email FROM client_gst_numbers cg,clients c,training_projects p WHERE cg.id='" . $_POST['gst'] . "' AND c.client_id=cg.client_id AND p.client_id=c.client_id AND p.project_id='" . $_POST['project'] . "'");
        if ($query->num_rows()) {
            $result = $query->result();
            $indebit = 'Y';
            if ($result[0]->debit_note == 'Yes') {
                $indebit = 'N';
            }
            $data = array('ci_client' => $_POST['cid'],
                'ci_type' => 'Program',
                'ci_project' => $_POST['project'],
                'ci_company' => $_POST['cm'],
                'ci_gst_address' => $result[0]->client_address,
                'ci_state' => $result[0]->state,
                'ci_state_code' => $result[0]->state_code,
                'ci_gst' => $result[0]->gst_number,
                'ci_poNumber' => $_POST['poNum'],
                'ci_refNumber' => $_POST['refNum'],
                'ci_name' => $result[0]->contact_person,
                'ci_email' => $result[0]->contact_email,
                'ci_desc' => 'Professional Fees for training program',
                'ci_charge' => $result[0]->charge,
                'ci_date' => $this->full_date,
                'include_debitnote' => $indebit,
                'ci_status' => 0);

            $this->db->insert('client_invoice', $data);
            $id = $this->db->insert_id();

            if (date('m') <= 3) {
                $fYear = (date('y') - 1) . '-' . date('y');
            } else {
                $fYear = date('y') . '-' . (date('y') + 1);
            }

            $nquery = $this->db->query("SELECT invoice_no FROM companies WHERE company_id='" . $_POST['cm'] . "'");
            $nresult = $nquery->result();

            $invoice = 'WLE/' . $fYear . '/' . str_pad($nresult[0]->invoice_no, 3, "0", STR_PAD_LEFT);
            $inFrmt = 'WLE/' . $fYear . '/';
            if ($_POST['cm'] == '1') {
                $invoice = 'MAH/' . $fYear . '/' . str_pad($nresult[0]->invoice_no, 4, "0", STR_PAD_LEFT);
                $inFrmt = 'MAH/' . $fYear . '/';
            }
            $this->db->query("UPDATE companies SET invoice_no=invoice_no+1 WHERE company_id='" . $_POST['cm'] . "'");
            $this->db->query("UPDATE client_invoice SET ci_no_frmt='" . $inFrmt . "', ci_number='" . $invoice . "' WHERE ci_id='" . $id . "'");

            return $id;
        }
        return 0;
    }

    public function creat_mInvoice() {
        $year = date('Y',  strtotime($_POST['tdate']));
        $month = date('m',  strtotime($_POST['tdate']));
        $this->db->query("UPDATE training_projects SET client_invoice_raised='Y',client_invoice_date='" . $this->cdate . "' WHERE client_id='" . $_POST['cid'] . "' AND DATE(training_start_date)>='" . $_POST['fdate'] . "' AND DATE(training_start_date)<='" . $_POST['tdate'] . "'");
        $query = $this->db->query("SELECT cg.*,c.person1,c.email1, (SELECT MAX(client_charges) FROM training_projects WHERE is_active='1' AND client_id='" . $_POST['cid'] . "' AND DATE(training_start_date)>='" . $_POST['fdate'] . "' AND DATE(training_start_date)<='" . $_POST['tdate'] . "' AND is_active='1') as charge,(SELECT SUM(training_duration) FROM training_projects WHERE is_active='1' AND client_id='" . $_POST['cid'] . "' AND DATE(training_start_date)>='" . $_POST['fdate'] . "' AND DATE(training_start_date)<='" . $_POST['tdate'] . "') as days FROM client_gst_numbers cg,clients c WHERE cg.id='" . $_POST['gst'] . "' AND c.client_id=cg.client_id");
        if ($query->num_rows()) {
            $result = $query->result();
            $charge = $result[0]->charge*$result[0]->days;
            
            $data = array('ci_client' => $_POST['cid'],
                'ci_type' => 'Consolidated',
                'ci_year' => $year,
                'ci_month' => $month,
                'ci_company' => $_POST['cm'],
                'ci_gst_address' => $result[0]->client_address,
                'ci_state' => $result[0]->state,
                'ci_state_code' => $result[0]->state_code,
                'ci_gst' => $result[0]->gst_number,
                'ci_poNumber' => $_POST['poNum'],
                'ci_refNumber' => $_POST['refNum'],
                'ci_name' => $result[0]->person1,
                'ci_email' => $result[0]->email1,
                'ci_desc' => 'Professional Fees for training program',
                'ci_charge' => $charge,
                'ci_days' => $result[0]->days,
                'ci_date' => $this->full_date,
                'ci_status' => 0);

            $this->db->insert('client_invoice', $data);
            $id = $this->db->insert_id();

            if (date('m') <= 3) {
                $fYear = (date('y') - 1) . '-' . date('y');
            } else {
                $fYear = date('y') . '-' . (date('y') + 1);
            }

            $nquery = $this->db->query("SELECT invoice_no FROM companies WHERE company_id='" . $_POST['cm'] . "'");
            $nresult = $nquery->result();

            $invoice = 'WLE/' . $fYear . '/' . str_pad($nresult[0]->invoice_no, 3, "0", STR_PAD_LEFT);
            $inFrmt = 'WLE/' . $fYear . '/';
            if ($_POST['cm'] == '1') {
                $invoice = 'MAH/' . $fYear . '/' . str_pad($nresult[0]->invoice_no, 4, "0", STR_PAD_LEFT);
                $inFrmt = 'MAH/' . $fYear . '/';
            }
            $this->db->query("UPDATE companies SET invoice_no=invoice_no+1 WHERE company_id='" . $_POST['cm'] . "'");
            $this->db->query("UPDATE client_invoice SET ci_no_frmt='" . $inFrmt . "', ci_number='" . $invoice . "' WHERE ci_id='" . $id . "'");
            return $id;
        }
        return 0;
    }

    public function creat_dInvoice() {
        $query = $this->db->query("SELECT cg.*,c.person1,c.email1, '0' AS charge,'1' AS days FROM client_gst_numbers cg,clients c WHERE cg.id='" . $_POST['gst'] . "' AND c.client_id=cg.client_id");
        if ($query->num_rows()) {
            $result = $query->result();
            $data = array('ci_client' => $_POST['client'],
                'ci_type' => 'Program',
                'ci_year' => '',
                'ci_month' => '',
                'ci_project' => $_POST['dproject'],
                'ci_company' => $_POST['company'],
                'ci_gst_address' => $result[0]->client_address,
                'ci_state' => $result[0]->state,
                'ci_state_code' => $result[0]->state_code,
                'ci_gst' => $result[0]->gst_number,
                'ci_poNumber' => $_POST['poNum'],
                'ci_refNumber' => $_POST['refNum'],
                'ci_name' => $result[0]->person1,
                'ci_email' => $result[0]->email1,
                'ci_desc' => 'Debit Note',
                'ci_charge' => $result[0]->charge,
                'ci_days' => $result[0]->days,
                'ci_date' => $this->full_date,
                'ci_status' => 0);

            $this->db->insert('client_invoice', $data);
            $id = $this->db->insert_id();

            for ($i = 1; $i < 4; $i++) {
                $data = array('project_id' => $_POST['dproject'],
                    'expense_type' => 'item' . $i,
                    'amount' => '100',
                    'in_amount' => '100',
                    'absorb' => 'N',
                    'added_by' => 'Admin',
                    'added_date' => $this->full_date);

                $this->db->insert('program_expenses', $data);
            }

            if (date('m') <= 3) {
                $fYear = (date('y') - 1) . '-' . date('y');
            } else {
                $fYear = date('y') . '-' . (date('y') + 1);
            }


            //$invoice = 'WLE/' . $fYear . '/' . str_pad($nresult[0]->invoice_no, 3, "0", STR_PAD_LEFT);
            $inFrmt = 'WLE/' . $fYear . '/';
            if ($_POST['company'] == '1') {
                //$invoice = 'MAH/' . $fYear . '/' . str_pad($nresult[0]->invoice_no, 4, "0", STR_PAD_LEFT);
                $inFrmt = 'MAH/' . $fYear . '/';
            }
            //$this->db->query("UPDATE companies SET debit_no=debit_no+1 WHERE company_id='" . $_POST['cm'] . "'");
            $this->db->query("UPDATE client_invoice SET ci_no_frmt='" . $inFrmt . "', ci_number='NA' WHERE ci_id='" . $id . "'");
            return $id;
        }
        return 0;
    }

    public function get_client_invoice($id) {
        $query = $this->db->query("SELECT * FROM client_invoice WHERE ci_id='" . $id . "'");
        return $query->result();
    }

    public function get_month_project($cid, $year, $month) {
        $query = $this->db->query("SELECT project_id,project_title FROM training_projects WHERE client_id='" . $cid . "' AND YEAR(training_start_date)='" . $year . "' AND MONTH(training_start_date)='" . $month . "'");
        return $query->result();
    }

    public function get_allclient_invoice() {
        $filter = "";
        $title = "";
        if (isset($_POST['pclient'])) {
            if ($_POST['pclient'] != 'All') {
                $filter .="  ci.ci_client='" . $_POST['pclient'] . "' AND ";
            }
            if ($_POST['invoice'] != '') {
                $filter .="  ci.ci_number='" . $_POST['invoice'] . "' AND ";
            }
            if ($_POST['status'] != 'All') {
                if ($_POST['status'] == '2') {
                    $filter .=" ci.ci_debit_id!='0' AND ci.ci_debit_status='0' AND ";
                } else {
                    $filter .="  ci.ci_pay_status='" . $_POST['status'] . "' AND ";
                }
            }
            if ($_POST['title'] != '') {
                $title .=" AND p.project_title LIKE '%" . $_POST['title'] . "%' ";
            }
        }



        $p = 0;
        if (isset($_GET['page']) && $this->session->userdata('CINVOICE')) {
            $p = $_GET['page'];
            $queryString = $this->session->userdata('CINVOICE');
        } else {
            $queryString = "FROM client_invoice ci LEFT JOIN training_projects p ON ci.ci_project=p.project_id $title, clients c WHERE $filter c.client_id=ci.ci_client";
            $this->session->set_userdata('CINVOICE', $queryString);
        }
        $lmt = "LIMIT 20 OFFSET 0";
        if ($p > 1) {
            $p = ($p - 1) * 20;
            $lmt = "LIMIT 20 OFFSET " . $p;
        }

        $query = $this->db->query("SELECT ci.*,c.client_id,c.client_name,c.payment_credit_days,p.project_title,p.training_start_date,p.training_duration,p.client_payment_receieved,p.client_debit_received $queryString ORDER BY ci.ci_id DESC $lmt");
        return $query->result();
    }

    public function getCInvoiceCount() {
        $queryString = $this->session->userdata('CINVOICE');
        $query = $this->db->query("SELECT COUNT(ci.ci_id) as total $queryString");
        return $query->result();
    }

    public function get_cinvoicefor_export() {
        //$query = $this->db->query("SELECT ci.*,c.client_id,c.client_name,p.project_title,p.training_start_date,p.training_end_date,p.training_duration,p.location_of_training,cm.company_name,cm.gst_state_code FROM client_invoice ci LEFT JOIN training_projects p ON ci.ci_project=p.project_id, clients c,companies cm WHERE ci.ci_status='1' AND c.client_id=ci.ci_client AND cm.company_id=c.company_id ORDER BY ci.ci_id DESC");
        $clientfilter = "";
        $monthfilter1 = "";
        $monthfilter2 = "";
        if (isset($_POST['pclient'])) {
            if ($_POST['pclient'] != 'All') {
                $clientfilter = " AND c.client_id='" . $_POST['pclient'] . "'";
            }
            if ($_POST['company'] != 'All') {
                $clientfilter = " AND c.company_id='" . $_POST['company'] . "'";
            }
            if (isset($_POST['month'])) {
                if ($_POST['month'][0] != 'All') {
                    $monthfilter1 .=" AND ( ";
                    foreach ($_POST['month'] as $mdata) {
                        $monthfilter1 .=" MONTH(ci.ci_date)='" . $mdata . "' ||";
                    }
                    $monthfilter1 = chop($monthfilter1, "||");
                    $monthfilter1 .=" ) ";

                    $monthfilter2 .=" AND ( ";
                    foreach ($_POST['month'] as $mdata) {
                        $monthfilter2 .=" MONTH(di.di_date)='" . $mdata . "' ||";
                    }
                    $monthfilter2 = chop($monthfilter2, "||");
                    $monthfilter2 .=" ) ";
                }
            }
            if ($_POST['year'] != 'All') {
                $monthfilter1 .=" AND YEAR(ci.ci_date)='" . $_POST['year'] . "'";
                $monthfilter2 .=" AND YEAR(di.di_date)='" . $_POST['year'] . "'";
            }
        }
        if (isset($_POST['bdm'])) {
            $query = $this->db->query("SELECT 'Fees' AS intype,ci.ci_type AS type,ci.ci_number AS number,ci.ci_gst AS gst,ci.ci_gst_address AS gst_address,ci.ci_month AS month,ci.ci_year AS year,ci.ci_date AS date,ci.ci_amt AS amt,ci.ci_gstamt AS gstamt,ci.ci_state_code AS statecode,ci.ci_no_frmt AS nofrmt,ci.ci_debit_id AS debitid,ci.ci_damt AS damt,ci.ci_dgstamt AS dgstamt,ci.ci_pay_status AS pay_status,ci.ci_debit_status AS debit_status,cm.company_name AS company,c.client_name AS clientname,p.project_title AS project,p.training_start_date AS sdate,p.training_end_date AS edate,p.training_duration AS duration,p.location_of_training AS location,cm.gst_state_code AS gststatecode FROM client_invoice ci LEFT JOIN training_projects p ON ci.ci_project=p.project_id, clients c, client_projectmanagers cp,companies cm WHERE cp.bm_id='" . $_POST['bdm'] . "' AND ci.ci_client=cp.client_id AND ci.ci_number!='NA' AND ci.ci_status='1'    $monthfilter1 AND c.client_id=ci.ci_client $clientfilter AND cm.company_id=c.company_id UNION ALL SELECT 'Debit' AS intype,ci.ci_type AS type,ci.ci_number AS number,ci.ci_gst AS gst,ci.ci_gst_address AS gst_address,ci.ci_month AS month,ci.ci_year AS year,ci.ci_date AS date,ci.ci_amt AS amt,ci.ci_gstamt AS gstamt,ci.ci_state_code AS statecode,ci.ci_no_frmt AS nofrmt,ci.ci_debit_id AS debitid,ci.ci_damt AS damt,ci.ci_dgstamt AS dgstamt,ci.ci_pay_status AS pay_status,ci.ci_debit_status AS debit_status,cm.company_name AS company,c.client_name AS clientname,p.project_title AS project,p.training_start_date AS sdate,p.training_end_date AS edate,p.training_duration AS duration,p.location_of_training AS location,cm.gst_state_code AS gststatecode FROM client_invoice ci LEFT JOIN training_projects p ON ci.ci_project=p.project_id, clients c, client_projectmanagers cp,companies cm WHERE cp.bm_id='" . $_POST['bdm'] . "' AND ci.ci_client=cp.client_id AND ci.ci_status='1' AND ci.ci_debit_id!='0'  $monthfilter1 AND c.client_id=ci.ci_client $clientfilter AND cm.company_id=c.company_id UNION ALL SELECT 'Drona Fees' AS intype,'drona' AS type, di.di_number AS number,di.di_gst AS gst,di.di_address AS gst_address,di.di_month AS month,di.di_year AS year,di.di_date AS date,di.di_subtotal AS amt,di.di_gstamt AS gstamt,di.di_state_code AS statecode,null AS nofrmt,di.di_debitNumber AS debitid,di.di_debit_subtotal AS damt,di.di_debit_gstamt AS dgstamt,di.di_pay_status AS pay_status,di.di_debit_status AS debit_status,cm.company_name AS company,c.client_name AS clientname,di.di_project AS project,null AS sdate,null AS edate,null AS duration,di.di_location AS location,cm.gst_state_code AS gststatecode FROM drona_invoice di INNER JOIN companies cm ON di.di_company=cm.company_id, clients c, client_projectmanagers cp WHERE cp.bm_id='" . $_POST['bdm'] . "' AND di.di_client=cp.client_id AND di.di_status='1' $monthfilter2 AND c.client_id=di.di_client $clientfilter ORDER BY date");
        } else {
            $query = $this->db->query("SELECT 'Fees' AS intype,ci.ci_type AS type,ci.ci_number AS number,ci.ci_gst AS gst,ci.ci_gst_address AS gst_address,ci.ci_month AS month,ci.ci_year AS year,ci.ci_date AS date,ci.ci_amt AS amt,ci.ci_gstamt AS gstamt,ci.ci_state_code AS statecode,ci.ci_no_frmt AS nofrmt,ci.ci_debit_id AS debitid,ci.ci_damt AS damt,ci.ci_dgstamt AS dgstamt,ci.ci_pay_status AS pay_status,ci.ci_debit_status AS debit_status,cm.company_name AS company,c.client_name AS clientname,p.project_title AS project,p.training_start_date AS sdate,p.training_end_date AS edate,p.training_duration AS duration,p.location_of_training AS location,cm.gst_state_code AS gststatecode FROM client_invoice ci LEFT JOIN training_projects p ON ci.ci_project=p.project_id, clients c,companies cm WHERE ci.ci_number!='NA' AND ci.ci_status='1'    $monthfilter1 AND c.client_id=ci.ci_client $clientfilter AND cm.company_id=c.company_id UNION ALL SELECT 'Debit' AS intype,ci.ci_type AS type,ci.ci_number AS number,ci.ci_gst AS gst,ci.ci_gst_address AS gst_address,ci.ci_month AS month,ci.ci_year AS year,ci.ci_date AS date,ci.ci_amt AS amt,ci.ci_gstamt AS gstamt,ci.ci_state_code AS statecode,ci.ci_no_frmt AS nofrmt,ci.ci_debit_id AS debitid,ci.ci_damt AS damt,ci.ci_dgstamt AS dgstamt,ci.ci_pay_status AS pay_status,ci.ci_debit_status AS debit_status,cm.company_name AS company,c.client_name AS clientname,p.project_title AS project,p.training_start_date AS sdate,p.training_end_date AS edate,p.training_duration AS duration,p.location_of_training AS location,cm.gst_state_code AS gststatecode FROM client_invoice ci LEFT JOIN training_projects p ON ci.ci_project=p.project_id, clients c,companies cm WHERE ci.ci_status='1' AND ci.ci_debit_id!='0'  $monthfilter1 AND c.client_id=ci.ci_client $clientfilter AND cm.company_id=c.company_id UNION ALL SELECT 'Drona Fees' AS intype,'drona' AS type, di.di_number AS number,di.di_gst AS gst,di.di_address AS gst_address,di.di_month AS month,di.di_year AS year,di.di_date AS date,di.di_subtotal AS amt,di.di_gstamt AS gstamt,di.di_state_code AS statecode,null AS nofrmt,'' AS debitid,di.di_debit_subtotal AS damt,di.di_debit_gstamt AS dgstamt,di.di_pay_status AS pay_status,di.di_debit_status AS debit_status,cm.company_name AS company,c.client_name AS clientname,di.di_project AS project,null AS sdate,null AS edate,null AS duration,di.di_location AS location,cm.gst_state_code AS gststatecode FROM drona_invoice di INNER JOIN companies cm ON di.di_company=cm.company_id, clients c WHERE di.di_status='1' AND di.di_number!='' $monthfilter2 AND c.client_id=di.di_client $clientfilter UNION ALL SELECT 'Drona Fees' AS intype,'drona' AS type, di.di_number AS number,di.di_gst AS gst,di.di_address AS gst_address,di.di_month AS month,di.di_year AS year,di.di_date AS date,di.di_subtotal AS amt,di.di_gstamt AS gstamt,di.di_state_code AS statecode,null AS nofrmt,di.di_debitNumber AS debitid,di.di_debit_subtotal AS damt,di.di_debit_gstamt AS dgstamt,di.di_pay_status AS pay_status,di.di_debit_status AS debit_status,cm.company_name AS company,c.client_name AS clientname,di.di_project AS project,null AS sdate,null AS edate,null AS duration,di.di_location AS location,cm.gst_state_code AS gststatecode FROM drona_invoice di INNER JOIN companies cm ON di.di_company=cm.company_id, clients c WHERE di.di_status='1' AND di.di_debitNumber!='' $monthfilter2 AND c.client_id=di.di_client $clientfilter ORDER BY date");
            //return $this->db->last_query();
        }
        return $query->result();
    }

    public function update_client_invoice() {
        $data = array('ci_gst_address' => $_POST['gstAddress'],
            'ci_gst' => $_POST['gst'],
            'ci_sac' => $_POST['sac'],
            'ci_name' => $_POST['name'],
            'ci_state' => $_POST['statename'],
            'ci_email' => $_POST['email'],
            'ci_poNumber' => $_POST['poNum'],
            'ci_refNumber' => $_POST['refNum'],
            'ci_project' => $_POST['pname'],
            'ci_charge' => $_POST['fees'],
            'ci_desc' => $_POST['desc'],
            'include_debitnote' => $_POST['indebit'],
            'include_gst' => $_POST['ingst'],
            'ci_date' => $_POST['date']);

        $this->db->where('ci_id', $_POST['inid']);
        $this->db->update('client_invoice', $data);
        return 1;
    }

    //Admin confirms client Invoice Email Admin & PM
    public function confirm_client_invoice($val) {
        $mv = 0;
        if ($val['debit'] == 'N' && $val['exp'] == '1' && $val['did'] == '0') {
            $mquery = $this->db->query("SELECT debit_no FROM companies  WHERE company_id='" . $val['cm'] . "'");
            $mresult = $mquery->result();
            $mv = $mresult[0]->debit_no;
            $this->db->query("UPDATE companies SET debit_no=debit_no+1 WHERE company_id='" . $val['cm'] . "'");
        }

        $updata = array('ci_status' => '1',
            'ci_amt' => $val['amt'],
            'ci_gstamt' => $val['gst'],
            'ci_debit_id' => $mv,
            'ci_damt' => $val['damt'],
            'ci_dgstamt' => $val['dgst']);

        $this->db->where('ci_id', $val['cid']);
        $this->db->update('client_invoice', $updata);

        //$this->db->query("UPDATE client_invoice SET ci_status='1', ci_amt='" . $val['amt'] . "',ci_debit_id='" . $mv . "' WHERE ci_id='" . $val['cid'] . "'");
        if ($val['amt'] != '0') {
            $query = $this->db->query("SELECT ci.ci_number, ci.ci_type, c.client_name, c.client_id FROM client_invoice ci, clients c WHERE ci.ci_id='" . $val['cid'] . "' AND c.client_id=ci.ci_client");
            $ci = $query->result();
            if ($ci[0]->ci_type != 'Consolidated') {
                $query3 = $this->db->query("SELECT project_title FROM training_projects WHERE project_id='" . $val['pid'] . "'");
                $result3 = $query3->result();
                $pName = $result3[0]->project_title;
            } else {
                $pName = 'Multiple';
            }

            $client_id = $ci[0]->client_id;

            $query2 = $this->db->query("SELECT u.name, u.email,u.user_code FROM `client_projectmanagers` c, application_users u where u.user_code = c.pm_id AND c.client_id='" . $client_id . "'");
            $result = $query2->result();
            $eUid = $result[0]->user_code;
            $email = "accounts@wagonslearning.com," . $result[0]->email;
            $pm_name = $result[0]->name;


            $data = array('inv_no' => $ci[0]->ci_number,
                'inv_date' => $this->full_date,
                'company' => $ci[0]->client_name,
                'pm' => $result[0]->name,
                'status' => 'ClientInvoiceGenerated');

            $note = array('user' => $eUid,
                'project' => $val['pid'],
                'task' => 0,
                'type' => 'Client Invoice',
                'due_date' => '',
                'text' => "Invoice has been generated to client for the program : " . $pName);
            $this->push_notification($note);

            project_mail_admin($email, $data);
        }
    }

    public function get_program_expense($id) {
        $query = $this->db->query("SELECT * FROM program_expenses WHERE project_id='" . $id . "'");
        return $query->result();
    }

    public function update_expense($val) {
        $data = json_decode($val['updetail']);
        $fees = json_decode($val['fee']);
        foreach ($data as $dd) {
            $this->db->query("UPDATE program_expenses SET expense_type='" . $dd->details . "', in_amount='" . $dd->amt . "' WHERE id='" . $dd->id . "'");
        }
//        foreach($fees as $ff){
//            $this->db->query("UPDATE program_trainers SET in_amount='" . $ff->amt . "' WHERE id='" . $ff->id . "'");
//        }
    }

    public function get_trainer_fees($id) {
        $query = $this->db->query("SELECT f.id,f.amount,f.in_amount,u.name FROM program_trainers f,application_users u WHERE f.project_id='" . $id . "' AND u.user_code=f.trainer_id");
        return $query->result();
    }

    public function update_fees($val) {
        $this->db->query("UPDATE program_trainers SET in_amount='" . $val['amt'] . "' WHERE id='" . $val['fid'] . "'");
    }

    public function absotb_status($val) {
        $this->db->query("UPDATE program_expenses SET absorb='" . $val['status'] . "' WHERE id='" . $val['eid'] . "'");
    }

//    public function get_client_gst($id) {
//        $query = $this->db->query("SELECT * FROM client_gst_numbers WHERE client_id='" . $id . "'");
//        return $query->result();
//    }

    public function get_basic_company() {
        $query = $this->db->query("SELECT * FROM companies");
        return $query->result();
    }

    public function get_client_location($val) {
        $query = $this->db->query("SELECT * FROM client_gst_numbers WHERE client_id='" . $val['cid'] . "'");
        $result = $query->result();
        ?>
        <span class="input-group-addon" id="sizing-addon1">GST Location <sup>*</sup></span>
        <select class="form-control" name="gst" required>
            <option value=""> - select - </option>
            <?php
            if (!empty($result)) {
                foreach ($result as $r_data) {
                    ?>
                    <option value="<?php echo $r_data->id; ?>"><?php echo $r_data->state; ?></option>
                    <?php
                }
            }
            ?>
        </select>
        <?php
    }

    public function get_single_gst($id) {
        $query = $this->db->query("SELECT * FROM client_gst_numbers WHERE id='" . $id . "'");
        return $query->result();
    }

    public function update_gst() {
        $data = array('state' => $_POST['state'],
            'state_code' => $_POST['statecode'],
            'client_address' => $_POST['address'],
            'gst_number' => $_POST['gst']);

        $this->db->where('id', $_POST['gid']);
        $this->db->update('client_gst_numbers', $data);

        return 1;
    }

    public function save_edit_request() {
        $data = array('ci_id' => $_POST['cid'],
            'ci_number' => $_POST['inumber'],
            'er_email' => $_POST['email'],
            'er_desc' => $_POST['reason'],
            'er_date' => $this->full_date,
            'er_status' => 0);

        $this->db->insert("invoice_edit_request", $data);
        $id = $this->db->insert_id();
        $intype = '';
        if (isset($_POST['intype'])) {
            $intype = 'Drona';
        }
        $eData = array('eid' => $id,
            'id' => $_POST['cid'],
            'client' => $_POST['client'],
            'project' => $_POST['project'],
            'intype' => $intype,
            'pdate' => $_POST['pdate']);

        edit_request_mail($_POST['email'], $eData);
    }

    public function save_invoice_copy() {
        $file = "";
        $path = './assets/upload/client/';
        $img_name = $_FILES['img']['name'];
        $tmp_name = $_FILES['img']['tmp_name'];
        if (!empty($img_name[0])) {
            $count = count($img_name);
            for ($i = 0; $i < $count; $i++) {
                if ($file_name = image_upload($path, $img_name[$i], $tmp_name[$i])) {
                    $file .= $file_name . ',';
                }
            }
        }

        $file = chop($file, ',');

        $this->db->query("UPDATE client_invoice SET ci_img='" . $file . "' WHERE ci_id='" . $_POST['upCid'] . "'");
    }

    public function get_allEdit_request($id) {
        $query = $this->db->query("SELECT * FROM invoice_edit_request WHERE ci_number='" . $id . "' ORDER BY er_id DESC");
        return $query->result();
    }

    public function get_edit_request($id) {
        $query = $this->db->query("SELECT * FROM invoice_edit_request WHERE er_id='" . $id . "'");
        return $query->result();
    }

    public function accept_edit_request($val) {
        if ($val['status'] == '1') {
            if (isset($val['drona'])) {
                $this->db->query("UPDATE drona_invoice SET di_status='0' WHERE di_id='" . $val['cid'] . "'");
            } else {
                $this->db->query("UPDATE client_invoice SET ci_status='0' WHERE ci_id='" . $val['cid'] . "'");
            }
        }

        $this->db->query("UPDATE invoice_edit_request SET er_status='" . $val['status'] . "', er_status_date='" . $this->full_date . "' WHERE er_id='" . $val['eid'] . "'");
        if ($val['status'] == '1') {
            $eData = array('client' => $val['client'],
                'project' => $val['project'],
                'pdate' => $val['pdate']);

            edit_requestAccept_mail('account@wagonslearning.com', $eData);
        }
    }

    public function save_client_payment() {

        $payadvice = 1;
        if (isset($_POST['payadvice'])) {
            $payadvice = 0;
        }

        if ($_POST['intype'] == '3') {
            if ($_POST['payType'] == '1') {
                $this->db->query("UPDATE drona_invoice SET di_pay_status='1' WHERE di_id='" . $_POST['cid'] . "'");
            } else {
                $this->db->query("UPDATE drona_invoice SET di_debit_status='1' WHERE di_id='" . $_POST['cid'] . "'");
            }
        }

        $data = array('ci_id' => $_POST['cid'],
            'cp_amt' => $_POST['amt'],
            'cp_date' => $_POST['date'],
            'cp_pay_id' => $_POST['paymentid'],
            'cp_drona' => $_POST['intype'],
            'cp_type' => $_POST['payType']);

        $this->db->insert('client_payment', $data);

        //Message PM about the client payment

        if ($_POST['intype'] == '1') {
            if ($_POST['payType'] == '1') {
                $this->db->query("UPDATE client_invoice SET ci_pay_status='1',ci_pay_advice='" . $payadvice . "' WHERE ci_id='" . $_POST['cid'] . "'");
                $this->db->query("UPDATE training_projects SET client_payment_receieved='Y' WHERE project_id='" . $_POST['upPid'] . "'");
            } else {
                $this->db->query("UPDATE client_invoice SET ci_debit_status='1',ci_pay_advice='" . $payadvice . "' WHERE ci_id='" . $_POST['cid'] . "'");
                $this->db->query("UPDATE training_projects SET client_debit_received='Y' WHERE project_id='" . $_POST['upPid'] . "'");
            }
        }
        if ($_POST['intype'] == '2') {
            $this->db->query("UPDATE client_invoice SET ci_pay_status='1',ci_pay_advice='" . $payadvice . "' WHERE ci_id='" . $_POST['cid'] . "'");
            $this->db->query("UPDATE training_projects SET client_payment_receieved='Y' WHERE client_id='" . $_POST['client'] . "' AND MONTH(training_start_date)='" . $_POST['month'] . "' AND  YEAR(training_start_date)='" . $_POST['year'] . "'");
        }
        if ($_POST['intype'] == '1' || $_POST['intype'] == '2') {
            $query = $this->db->query("SELECT ci_number, client_name, clients.client_id FROM `client_invoice`, clients where clients.client_id=ci_client AND ci_id='" . $_POST['cid'] . "'");
            $ci = $query->result();

            $client_id = $ci[0]->client_id;

            $query2 = $this->db->query("SELECT u.name, u.email,u.user_code FROM `client_projectmanagers` c, application_users u where u.user_code = c.pm_id AND c.client_id='" . $client_id . "'");
            $result = $query2->result();
            if (!empty($result)) {
                $email = "accounts@wagonslearning.com," . $result[0]->email;
                if ($_POST['intype'] == '1') {
                    $note = array('user' => $result[0]->user_code,
                        'project' => $_POST['upPid'],
                        'task' => 0,
                        'type' => 'Client Payment',
                        'due_date' => '',
                        'text' => "Client payment is received ");
                    $this->push_notification($note);
                }
            } else {
                $email = "accounts@wagonslearning.com,";
            }


            $data = array('inv_no' => $ci[0]->ci_number,
                'pay_date' => $_POST['date'] . ' 00:00:00',
                'company' => $ci[0]->client_name,
                'amt' => $_POST['amt'],
                'pm' => '',
                'status' => 'ClientPaymentConfirmed');

            project_mail_admin($email, $data);
        }
    }

    public function updatePayAdvice($val) {
        $this->db->query("UPDATE client_invoice SET ci_pay_advice='" . $val['status'] . "' WHERE ci_id='" . $val['cid'] . "'");
    }

    public function remove_client_payment($val) {
        $query = $this->db->query("SELECT * FROM client_payment WHERE cp_id='" . $val['rid'] . "'");
        $result = $query->result();
        if ($result[0]->cp_drona == '3') {
            if ($result[0]->cp_type == '1') {
                $this->db->query("UPDATE drona_invoice SET di_pay_status='0' WHERE di_id='" . $result[0]->ci_id . "'");
            } else {
                $this->db->query("UPDATE drona_invoice SET di_debit_status='0' WHERE di_id='" . $result[0]->ci_id . "'");
            }
        } else {
            $query2 = $this->db->query("SELECT ci_id,ci_client,ci_year,ci_month,ci_project FROM client_invoice WHERE ci_id='" . $result[0]->ci_id . "'");
            $result2 = $query2->result();
            if ($result[0]->cp_drona == '1') {
                if ($result[0]->cp_type == '1') {
                    $this->db->query("UPDATE client_invoice SET ci_pay_status='0' WHERE ci_id='" . $result2[0]->ci_id . "'");
                    $this->db->query("UPDATE training_projects SET client_payment_receieved='N' WHERE project_id='" . $result2[0]->ci_project . "'");
                } else {
                    $this->db->query("UPDATE client_invoice SET ci_debit_status='0' WHERE ci_id='" . $result2[0]->ci_id . "'");
                    $this->db->query("UPDATE training_projects SET client_debit_received='N' WHERE project_id='" . $result2[0]->ci_project . "'");
                }
            }
            if ($result[0]->cp_drona == '2') {
                $this->db->query("UPDATE client_invoice SET ci_pay_status='0' WHERE ci_id='" . $result2[0]->ci_id . "'");
                $this->db->query("UPDATE training_projects SET client_payment_receieved='N' WHERE client_id='" . $result2[0]->ci_client . "' AND MONTH(training_start_date)='" . $result2[0]->ci_month . "' AND  YEAR(training_start_date)='" . $result2[0]->ci_year . "'");
            }
        }
        $this->db->query("DELETE FROM client_payment WHERE cp_id='" . $val['rid'] . "'");
    }

    public function get_client_payment($id, $type) {
        if ($type != '0') {
            $query = $this->db->query("SELECT * FROM client_payment WHERE ci_id='" . $id . "' AND cp_drona='" . $type . "'");
        } else {
            $query = $this->db->query("SELECT * FROM client_payment WHERE ci_id='" . $id . "'");
        }
        return $query->result();
    }

    public function remove_client_gst($val) {
        $this->db->query("DELETE FROM client_gst_numbers WHERE id='" . $val['cid'] . "'");
    }

    public function push_notification($val) {
        $data = array('user_id' => $val['user'],
            'project_id' => $val['project'],
            'notification_text' => $val['text'],
            'todo_task' => $val['task'],
            'notification_type' => $val['type'],
            'due_date' => $val['due_date'],
            'notification_date' => $this->full_date);

        $this->db->insert('notifications', $data);
    }

    public function get_notification($id) {
        $query = $this->db->query("SELECT n.*,p.project_title FROM notifications n, training_projects p WHERE n.user_id='" . $id . "' AND p.project_id=n.project_id ORDER BY n.id DESC");
        return $query->result();
    }

    public function get_feedbackGroup() {
        $query = $this->db->query("SELECT fq_group FROM feedback_question GROUP BY fq_group ORDER BY fq_id");
        return $query->result();
    }

    public function get_fdQuestions($id) {
        $query = $this->db->query("SELECT * FROM feedback_question WHERE fq_group='" . $id . "' ORDER BY fq_id");
        return $query->result();
    }

    public function save_feedback() {
        // $query = $this->db->query("SELECT fa_name FROM feedback_ans WHERE project_id='" . $_POST['project'] . "' AND fa_name='" . $_POST['pname'] . "'");
        //  if (!$query->num_rows()) {
        $query1 = $this->db->query("SELECT * FROM feedback_question");
        $result1 = $query1->result();
        if (!empty($result1)) {
            foreach ($result1 as $rsdata) {
                $ans = "";
                if (isset($_POST['ansR' . $rsdata->fq_id])) {
                    $ans = $_POST['ansR' . $rsdata->fq_id];
                }
                $data = array('project_id' => $_POST['project'],
                    'trainer_id' => '',
                    'fq_id' => $rsdata->fq_id,
                    'fa_name' => $_POST['pname'],
                    'fa_ans' => $ans);

                $this->db->insert('feedback_ans', $data);
            }
        }
        return 1;
        // }
        //return 2;
    }

    public function get_avgAns($id, $val, $pid) {
        $query = $this->db->query("SELECT COUNT(fa_ans) as total FROM feedback_ans WHERE project_id='" . $pid . "' AND  fq_id='" . $id . "' AND fa_ans='" . $val . "'");
        $result = $query->result();
        return $result[0]->total;
    }

    public function getWeekCount() {
        $query = $this->db->query("SELECT COUNT(project_id) as total FROM training_projects WHERE WEEKOFYEAR(training_start_date)=WEEKOFYEAR(NOW()) AND YEAR(training_start_date)=YEAR(NOW()) AND is_active='1'");
        $result = $query->result();
        return $result[0]->total;
    }

    public function getMonthCount() {
        $query = $this->db->query("SELECT COUNT(project_id) as total FROM training_projects WHERE MONTH(training_start_date)=MONTH(NOW()) AND YEAR(training_start_date)=YEAR(NOW()) AND is_active='1'");
        $result = $query->result();
        return $result[0]->total;
    }

    public function getTotalCount() {
        $query = $this->db->query("SELECT COUNT(project_id) as total FROM training_projects WHERE is_active='1'");
        $result = $query->result();
        return $result[0]->total;
    }

    public function save_drona_invoice() {
        $query = $this->db->query("SELECT c.client_name,g.* FROM client_gst_numbers g INNER JOIN clients c ON g.client_id=c.client_id WHERE g.id='" . $_POST['gst'] . "'");
        $result = $query->result();
        if (!empty($result)) {
            $query1 = $this->db->query("SELECT invoice_no FROM companies WHERE company_id='" . $_POST['company'] . "'");
            $result1 = $query1->result();
        }

        if (date('m') <= 3) {
            $fYear = (date('y') - 1) . '-' . date('y');
        } else {
            $fYear = date('y') . '-' . (date('y') + 1);
        }

        $inFrmt = 'WLE/' . $fYear . '/';
        $invoiceNumber = '';
        if ($_POST['company'] == '1') {
            $inFrmt = 'MAH/' . $fYear . '/';
        }

        $onlyDebit = 1;
        if (!isset($_POST['onlydebit'])) {
            $invoiceNumber = $inFrmt . str_pad($result1[0]->invoice_no, 3, "0", STR_PAD_LEFT);
            $onlyDebit = 0;
        }
        $address = $result[0]->client_name . '<br/>' . $result[0]->client_address;
        $data = array('di_number' => $invoiceNumber,
            'di_client' => $_POST['client'],
            'di_address' => $address,
            'di_state' => $result[0]->state,
            'di_state_code' => $result[0]->state_code,
            'di_gst' => $result[0]->gst_number,
            'di_company' => $_POST['company'],
            'di_type' => $_POST['drona'],
            'di_year' => $_POST['year'],
            'di_month' => $_POST['month'],
            'di_date' => $this->full_date,
            'di_vendor_code' => $_POST['vcode'],
            'di_department' => $_POST['department'],
            'di_location' => $_POST['location'],
            'di_project' => $_POST['project'],
            'di_name' => $_POST['name'],
            'di_email' => $_POST['email'],
            'di_poNumber' => $_POST['poNum'],
            'di_sesNumber' => $_POST['sesNum'],
            'di_onlyDebit' => $onlyDebit);

        $this->db->insert('drona_invoice', $data);
        $id = $this->db->insert_id();
        if ($onlyDebit == 0) {
            $this->db->query("UPDATE companies SET invoice_no=invoice_no+1 WHERE company_id='" . $_POST['company'] . "'");
        }
        return $id;
    }

    public function get_singleDrona_invoice($id) {
        $query = $this->db->query("SELECT d.*,c.client_name FROM drona_invoice d INNER JOIN clients c ON d.di_client=c.client_id WHERE di_id='" . $id . "'");
        return $query->result();
    }

    public function get_singleCompany($id) {
        $query = $this->db->query("SELECT * FROM companies WHERE company_id='" . $id . "'");
        return $query->result();
    }

    public function save_drona_expense() {
        $debitNumber = '';
        if ($_POST['exptype'] == '0' && empty($_POST['debit_number'])) {
            $query = $this->db->query("SELECT debit_no FROM companies WHERE company_id='" . $_POST['company'] . "'");
            $result = $query->result();

            if (date('m') <= 3) {
                $fYear = (date('y') - 1) . '-' . date('y');
            } else {
                $fYear = date('y') . '-' . (date('y') + 1);
            }

            $inFrmt = 'WLE/' . $fYear . '/';
            $invoiceNumber = '';
            if ($_POST['company'] == '1') {
                $inFrmt = 'MAH/' . $fYear . '/';
            }

            $inNumber = explode('/', $_POST['in_number']);
            $debitNumber = $inFrmt . 'D/' . str_pad($result[0]->debit_no, 3, "0", STR_PAD_LEFT);

            $this->db->query("UPDATE companies SET debit_no=debit_no+1 WHERE company_id='" . $_POST['company'] . "'");
            $this->db->query("UPDATE drona_invoice SET di_debitNumber='" . $debitNumber . "' WHERE di_id='" . $_POST['in_id'] . "'");
        }
        $data = array('di_id' => $_POST['in_id'],
            'dd_number' => $debitNumber,
            'dd_desc' => $_POST['desc'],
            'dd_amount' => $_POST['amount'],
            'dd_date' => $this->full_date,
            'dd_status' => $_POST['exptype']);

        $this->db->insert('drona_debitnote', $data);
    }

    public function get_drona_expense($id) {
        $query = $this->db->query("SELECT * FROM drona_debitnote WHERE di_id='" . $id . "'");
        return $query->result();
    }

    public function get_client_gstLocation($id) {
        $query = $this->db->query("SELECT id,state FROM client_gst_numbers WHERE client_id='" . $id . "'");
        return $query->result();
    }

    public function update_drona_invoice() {
        $query = $this->db->query("SELECT c.client_name,g.* FROM client_gst_numbers g INNER JOIN clients c ON g.client_id=c.client_id WHERE g.id='" . $_POST['gst'] . "'");
        $result = $query->result();

        $onlyDebit = 1;
        if ($_POST['onlydebit'] == '0') {
            if ($_POST['drona'] != 'Project Vijay') {
                $onlyDebit = 0;
            }
        }
        $address = $result[0]->client_name . '<br/>' . $result[0]->client_address;
        $data = array('di_client' => $_POST['client'],
            'di_address' => $address,
            'di_state' => $result[0]->state,
            'di_state_code' => $result[0]->state_code,
            'di_gst' => $result[0]->gst_number,
            'di_type' => $_POST['drona'],
            'di_year' => $_POST['year'],
            'di_month' => $_POST['month'],
            'di_date' => $_POST['idate'],
			'di_number' => $_POST['inumber'],
            'di_vendor_code' => $_POST['vcode'],
            'di_department' => $_POST['department'],
            'di_location' => $_POST['location'],
            'di_project' => $_POST['project'],
            'di_name' => $_POST['name'],
            'di_email' => $_POST['email'],
            'di_poNumber' => $_POST['poNum'],
            'di_sac' => $_POST['sac'],
            'di_sesNumber' => $_POST['sesNum'],
            'di_onlyDebit' => $onlyDebit);

        $this->db->where('di_id', $_POST['inid']);
        $this->db->update('drona_invoice', $data);
    }

    public function get_drona_invoice() {

        $filter = "";

        if (isset($_POST['month'])) {
            if ($_POST['month'] != 'All') {
                $filter .= " AND d.di_month='" . $_POST['month'] . "' ";
            }
            if ($_POST['year'] != 'All') {
                $filter .= " AND d.di_year='" . $_POST['year'] . "' ";
            }
            if ($_POST['pclient'] != 'All') {
                $filter .= " AND d.di_client='" . $_POST['pclient'] . "' ";
            }
            if ($_POST['title'] != '') {
                $filter .= " AND d.di_type LIKE '%" . $_POST['title'] . "%' ";
            }
            if ($_POST['invoice'] != '') {
                $filter .= " AND ( d.di_number='" . $_POST['invoice'] . "' || d.di_debitNumber='" . $_POST['invoice'] . "') ";
            }
            if ($_POST['status'] != 'All') {
                $filter .=" AND d.di_pay_status='" . $_POST['status'] . "' ";
            }
        }

        $p = 0;
        if (isset($_GET['page']) && $this->session->userdata('DRONAQUERY')) {
            $p = $_GET['page'];
            $queryString = $this->session->userdata('DRONAQUERY');
        } else {
            $queryString = "FROM drona_invoice d INNER JOIN clients c ON d.di_client=c.client_id WHERE d.di_id!='' $filter";
            $this->session->set_userdata('DRONAQUERY', $queryString);
        }
        $lmt = "LIMIT 20 OFFSET 0";
        if ($p > 1) {
            $p = ($p - 1) * 20;
            $lmt = "LIMIT 20 OFFSET " . $p;
        }

        $query = $this->db->query("SELECT d.di_id,d.di_number,d.di_month,d.di_year,d.di_type,d.di_project,d.di_date,d.di_debitNumber,d.di_onlyDebit,d.di_status,d.di_pay_status,d.di_debit_status,c.client_name, (CASE WHEN d.di_onlyDebit='0' THEN (SELECT SUM(dd.dd_amount) FROM drona_debitnote dd WHERE dd.di_id=d.di_id AND dd.dd_status='1') ELSE (SELECT SUM(dd.dd_amount) FROM drona_debitnote dd WHERE dd.di_id=d.di_id AND dd.dd_status='0') END) as amount $queryString ORDER BY d.di_id DESC $lmt");
        return $query->result();
    }

    public function getDronaCount() {
        $queryString = $this->session->userdata('DRONAQUERY');
        $query = $this->db->query("SELECT COUNT(d.di_id) as total $queryString");
        return $query->result();
    }

    public function remove_drona_expense($val) {
        $this->db->query("DELETE FROM drona_debitnote WHERE dd_id='" . $val['eid'] . "'");
    }

    public function confirm_drona_invoice($val) {
        $this->db->query("UPDATE drona_invoice SET di_status='1',di_subtotal='" . $val['insub'] . "',di_gstamt='" . $val['ingst'] . "',di_debit_subtotal='" . $val['debitsub'] . "',di_debit_gstamt='" . $val['debitgst'] . "',di_amount='" . $val['inamt'] . "',di_debit_amount='" . $val['debitamt'] . "' WHERE di_id='" . $val['did'] . "'");
    }

    public function save_dinvoice_copy() {
        $file = "";
        $path = './assets/upload/client/';
        $img_name = $_FILES['img']['name'];
        $tmp_name = $_FILES['img']['tmp_name'];
        if (!empty($img_name[0])) {
            $count = count($img_name);
            for ($i = 0; $i < $count; $i++) {
                if ($file_name = image_upload($path, $img_name[$i], $tmp_name[$i])) {
                    $file .= $file_name . ',';
                }
            }
        }
        $file = chop($file, ',');
        $this->db->query("UPDATE drona_invoice SET di_img='" . $file . "' WHERE di_id='" . $_POST['upCid'] . "'");
    }

    public function send_feedback($val) {
        $id = onlyStringEncode($val['id']);
        send_feedback_mail($val['email'], array('id' => $id));
    }

    public function get_singleclient_invoice($id) {
        $query = $this->db->query("SELECT c.ci_number,c.ci_date,c.ci_amt,c.ci_damt,c.ci_gstamt,c.ci_dgstamt,c.ci_debit_id,c.ci_no_frmt,c.ci_status,cm.company_name FROM client_invoice c INNER JOIN  companies cm ON c.ci_company=cm.company_id WHERE c.ci_project='" . $id . "'");
        return $query->result();
    }

    public function get_consInvoiceExport($cid, $y, $m) {
        $filter = "";
        if (!empty($m)) {
            $filter .= " AND ci_month='" . $m . "' ";
        }
        if (!empty($y)) {
            $filter .= " AND ci_year='" . $y . "' ";
        }

        $query = $this->db->query("SELECT * FROM client_invoice WHERE ci_client='" . $cid . "' $filter");
        //return $this->db->last_query();
        return $query->result();
    }

    public function get_client_upPrograms($id, $bdm) {
        $today = date('Y-m-d');

        $filter = "";
        $filter1 = "";
        if (isset($_POST['pclient'])) {
            if (isset($_POST['title'])) {
                if (!empty($_POST['title'])) {
                    $filter .= " AND p.project_title LIKE '%" . $_POST['title'] . "%' ";
                }
                if (!empty($_POST['location'])) {
                    $filter .= " AND p.location_of_training LIKE '%" . $_POST['location'] . "%' ";
                }
            }

            if (!empty($_POST['sdate'])) {
                $filter .=" AND DATE(p.training_start_date)='" . $_POST['sdate'] . "' ";
            }
        }


        $p = 0;
        if (isset($_GET['page']) && $this->session->userdata('CUPQUERY')) {
            $p = $_GET['page'];
            $queryString = $this->session->userdata('CUPQUERY');
        } else {
            if (empty($id)) {
                $queryString = "FROM training_projects p, clients c, client_projectmanagers cp WHERE cp.bm_id='" . $bdm . "' AND p.client_id=cp.client_id AND p.training_start_date>='" . $today . "' AND p.is_active='1'  $filter AND c.client_id=p.client_id ";
            } else {
                $queryString = "FROM training_projects p, clients c WHERE p.training_start_date>='" . $today . "' AND p.is_active!='0' AND p.client_id='" . $id . "' $filter AND c.client_id=p.client_id ";
            }
            $this->session->set_userdata('CUPQUERY', $queryString);
        }
        $lmt = "LIMIT 20 OFFSET 0";
        if ($p > 1) {
            $p = ($p - 1) * 20;
            $lmt = "LIMIT 20 OFFSET " . $p;
        }
        $query = $this->db->query("SELECT p.project_id,p.user_code,p.project_title,p.location_of_training,p.training_start_date,p.training_end_date,p.is_active,p.trainer_engage_flag,p.create_date,c.client_id,c.client_name, p.half_day $queryString ORDER BY p.training_start_date ASC $lmt");
        return $query->result();
    }

    public function getClientUpCount() {
        $queryString = $this->session->userdata('CUPQUERY');
        $query = $this->db->query("SELECT COUNT(p.project_id) as total $queryString");
        return $query->result();
    }

    //directior dashboard
    public function getTotalProgramSummary($cid = NULL) {
        $cy = date('Y');
        $cm = date('m');

        if ($cm < 4) {
            $y1 = $cy - 1;
            $y2 = $cy;
        } else {
            $y1 = $cy;
            $y2 = $cy + 1;
        }

        $clientFilter = "";
        if (!empty($cid)) {
            $clientFilter = "client_id='" . $cid . "' AND ";
        }

        $query = $this->db->query("SELECT COUNT(project_id) AS total,DATE_FORMAT(training_start_date, '%M %Y') AS mon FROM training_projects WHERE $clientFilter is_active='1' AND YEAR(training_start_date)='" . $y1 . "' AND MONTH(training_start_date)>='04' GROUP BY MONTH(training_start_date) UNION ALL SELECT COUNT(project_id) AS total,DATE_FORMAT(training_start_date, '%M %Y') AS mon FROM training_projects WHERE $clientFilter is_active='1' AND YEAR(training_start_date)='" . $y2 . "' AND MONTH(training_start_date)<'04' GROUP BY MONTH(training_start_date)");
        return $query->result();
    }

    public function getTotalExpenseSummary($cid = NULL) {
        $cy = date('Y');
        $cm = date('m');

        if ($cm < 4) {
            $y1 = $cy - 1;
            $y2 = $cy;
        } else {
            $y1 = $cy;
            $y2 = $cy + 1;
        }

        $clientFilter = "";
        if (!empty($cid)) {
            $clientFilter = "p.client_id='" . $cid . "' AND ";
        }

        $query = $this->db->query("SELECT SUM(pe.amount) AS amt,DATE_FORMAT(p.training_start_date,'%M-%Y') AS mon FROM program_expenses pe,training_projects p WHERE $clientFilter p.is_active='1' AND YEAR(p.training_start_date)='" . $y1 . "' AND MONTH(p.training_start_date)>='04' AND pe.project_id=p.project_id GROUP BY MONTH(p.training_start_date) UNION ALL SELECT SUM(pe.amount) AS amt,DATE_FORMAT(p.training_start_date,'%M-%Y') AS mon FROM program_expenses pe,training_projects p WHERE $clientFilter p.is_active='1' AND YEAR(p.training_start_date)='" . $y2 . "' AND MONTH(p.training_start_date)<'04' AND pe.project_id=p.project_id GROUP BY MONTH(p.training_start_date)");
        return $query->result();
    }

    public function getTotalFeesSummary($cid = NULL) {
        $cy = date('Y');
        $cm = date('m');

        if ($cm < 4) {
            $y1 = $cy - 1;
            $y2 = $cy;
        } else {
            $y1 = $cy;
            $y2 = $cy + 1;
        }

        $clientFilter = "";
        if (!empty($cid)) {
            $clientFilter = "p.client_id='" . $cid . "' AND ";
        }

        $query = $this->db->query("SELECT SUM(pt.amount*(DATEDIFF(pt.trainer_date_to,pt.training_date_from)+1)) AS amt, DATE_FORMAT(p.training_start_date,'%M-%Y') AS mon FROM program_trainers pt, training_projects p WHERE $clientFilter p.is_active='1' AND YEAR(p.training_start_date)='" . $y1 . "' AND MONTH(p.training_start_date)>='04' AND pt.project_id=p.project_id GROUP BY MONTH(p.training_start_date) UNION ALL SELECT SUM(pt.amount*(DATEDIFF(pt.trainer_date_to,pt.training_date_from)+1)) AS amt, DATE_FORMAT(p.training_start_date,'%M-%Y') AS mon FROM program_trainers pt, training_projects p WHERE $clientFilter p.is_active='1' AND YEAR(p.training_start_date)='" . $y2 . "' AND MONTH(p.training_start_date)<'04' AND pt.project_id=p.project_id GROUP BY MONTH(p.training_start_date)");
        return $query->result();
    }

    public function getIncomeSummary1($cid = NULL) {
        $cy = date('Y');
        $cm = date('m');

        if ($cm < 4) {
            $y1 = $cy - 1;
            $y2 = $cy;
        } else {
            $y1 = $cy;
            $y2 = $cy + 1;
        }

        $clientFilter = "";
        if (!empty($cid)) {
            $clientFilter = "p.client_id='" . $cid . "' AND ";
        }

        $query = $this->db->query("SELECT SUM(ci.ci_amt+ci.ci_gstamt+ci.ci_damt+ci.ci_dgstamt) AS amt,DATE_FORMAT(p.training_start_date,'%M-%Y') AS mon FROM client_invoice ci,training_projects p WHERE $clientFilter YEAR(p.training_start_date)='" . $y1 . "' AND MONTH(p.training_start_date)>='04'  AND ci.ci_project=p.project_id GROUP BY MONTH(p.training_start_date) UNION ALL SELECT SUM(ci.ci_amt+ci.ci_gstamt+ci.ci_damt+ci.ci_dgstamt) AS amt,DATE_FORMAT(p.training_start_date,'%M-%Y') AS mon FROM client_invoice ci,training_projects p WHERE $clientFilter YEAR(p.training_start_date)='" . $y2 . "' AND MONTH(p.training_start_date)<'04'  AND ci.ci_project=p.project_id GROUP BY MONTH(p.training_start_date)");
        return $query->result();
    }

    public function getIncomeSummary2($cid = NULL) {
        $cy = date('Y');
        $cm = date('m');

        if ($cm < 4) {
            $y1 = $cy - 1;
            $y2 = $cy;
        } else {
            $y1 = $cy;
            $y2 = $cy + 1;
        }

        $clientFilter = "";
        if (!empty($cid)) {
            $clientFilter = "ci.ci_client='" . $cid . "' AND ";
        }

        $query = $this->db->query("SELECT SUM(ci.ci_amt+ci.ci_gstamt+ci.ci_damt+ci.ci_dgstamt) AS amt, MONTHNAME(STR_TO_DATE(ci.ci_month, '%m')) AS mon, ci.ci_year AS yr FROM client_invoice ci WHERE $clientFilter ci.ci_year='" . $y1 . "' AND ci.ci_month>='04' GROUP BY ci.ci_month UNION ALL SELECT SUM(ci.ci_amt+ci.ci_gstamt+ci.ci_damt+ci.ci_dgstamt) AS amt, MONTHNAME(STR_TO_DATE(ci.ci_month, '%m')) AS mon, ci.ci_year AS yr FROM client_invoice ci WHERE $clientFilter ci.ci_year='" . $y2 . "' AND ci.ci_month<'04' GROUP BY ci.ci_month");
        return $query->result();
    }

    public function getIncomeSummary3($cid = NULL) {
        $cy = date('Y');
        $cm = date('m');

        if ($cm < 4) {
            $y1 = $cy - 1;
            $y2 = $cy;
        } else {
            $y1 = $cy;
            $y2 = $cy + 1;
        }

        $clientFilter = "";
        if (!empty($cid)) {
            $clientFilter = "ci.ci_client='" . $cid . "' AND ";
        }

        $query = $this->db->query("SELECT SUM(ci.ci_amt+ci.ci_gstamt+ci.ci_damt+ci.ci_dgstamt) AS amt,DATE_FORMAT(ci.ci_date,'%M-%Y') AS mon FROM client_invoice ci WHERE $clientFilter ci.ci_number='NA' AND YEAR(ci.ci_date)='" . $y1 . "' AND MONTH(ci.ci_date)>='04' GROUP BY MONTH(ci.ci_date) UNION ALL SELECT SUM(ci.ci_amt+ci.ci_gstamt+ci.ci_damt+ci.ci_dgstamt) AS amt,DATE_FORMAT(ci.ci_date,'%M-%Y') AS mon FROM client_invoice ci WHERE $clientFilter ci.ci_number='NA' AND YEAR(ci.ci_date)='" . $y2 . "' AND MONTH(ci.ci_date)<'04' GROUP BY MONTH(ci.ci_date)");
        return $query->result();
    }

    public function getClientProgramSummary() {
        $cy = date('Y');
        $cm = date('m');

        if ($cm < 4) {
            $y1 = $cy - 1;
            $y2 = $cy;
        } else {
            $y1 = $cy;
            $y2 = $cy + 1;
        }

        $date1 = $y1 . '-04-01';
        $date2 = $y2 . '-03-31';
        $query = $this->db->query("SELECT round(COUNT(p.project_id)/(SELECT COUNT(project_id) FROM training_projects WHERE DATE(training_start_date)>='" . $date1 . "' AND DATE(training_start_date)<='" . $date2 . "' AND is_active='1'),2)*100 AS total,c.client_name FROM training_projects p,clients c WHERE DATE(p.training_start_date)>='" . $date1 . "' AND DATE(p.training_start_date)<='" . $date2 . "' AND p.is_active='1' AND c.client_id=p.client_id GROUP BY p.client_id ORDER BY total DESC LIMIT 5");
        return $query->result();
    }

    public function getDRTInvoiceTotal($cid = NULL) {
        $clientFilter = "";
        if (!empty($cid)) {
            $clientFilter = " WHERE c.ci_client='" . $cid . "'";
        }
        $query = $this->db->query("SELECT SUM(c.ci_amt) AS amt,SUM(c.ci_gstamt) AS gst,SUM(c.ci_dgstamt) AS dgst,SUM(c.ci_damt) AS damt FROM client_invoice c $clientFilter");
        return $query->result();
    }

    public function getDRTTrainerAmount($cid = NULL) {
        $clientFilter = "";
        if (!empty($cid)) {
            $clientFilter = "p.client_id='" . $cid . "' AND ";
        }
        $query = $this->db->query("SELECT SUM(pt.amount*(DATEDIFF(pt.trainer_date_to,pt.training_date_from)+1)) AS amt FROM program_trainers pt,training_projects p WHERE $clientFilter p.is_active='1'  AND pt.project_id=p.project_id AND pt.trainer_invoice_flag='1'");
        return $query->result();
    }

    public function getDRTTrainerDebit($cid = NULL) {
        $clientFilter = "";
        if (!empty($cid)) {
            $clientFilter = "p.client_id='" . $cid . "' AND ";
        }
        $query = $this->db->query("SELECT SUM(d.debit_amt) AS amt FROM debit_note d,training_projects p WHERE $clientFilter p.is_active='1'  AND d.project=p.project_id AND d.debit_status='1'");
        return $query->result();
    }

    public function getDRTPayPending($cid = NULL) {
        $clientFilter1 = "";
        if (!empty($cid)) {
            $clientFilter1 = "ci.ci_client='" . $cid . "' AND ";
        }
        $clientFilter2 = "";
        if (!empty($cid)) {
            $clientFilter2 = "di.di_client='" . $cid . "' AND ";
        }
        $query = $this->db->query("SELECT * FROM (SELECT ci.ci_number AS num,c.client_name AS cname,ci.ci_amt+ci.ci_gstamt AS amt,ci.ci_date AS date FROM client_invoice ci INNER JOIN clients c ON c.client_id=ci.ci_client WHERE $clientFilter1 ci.ci_number!='NA' AND ci.ci_status='1' AND ci.ci_pay_status='0' UNION ALL SELECT CONCAT_WS('D/',ci.ci_no_frmt,ci.ci_debit_id) AS num,c.client_name AS cname,ci.ci_damt+ci.ci_dgstamt AS amt,ci.ci_date AS date FROM client_invoice ci INNER JOIN clients c ON c.client_id=ci.ci_client WHERE $clientFilter1 ci.ci_status='1' AND ci.ci_debit_id!='0' AND ci.ci_debit_status='0' UNION ALL SELECT di.di_number AS num,c.client_name AS cname,di.di_amount AS amt,di.di_date AS date FROM drona_invoice di INNER JOIN clients c ON di.di_client=c.client_id WHERE $clientFilter2 di.di_number!='' AND di.di_status='1' AND di.di_pay_status='0' UNION ALL SELECT di.di_debitNumber AS num,c.client_name AS cname,di.di_debit_amount AS amt,di.di_date AS date FROM drona_invoice di INNER JOIN clients c ON di.di_client=c.client_id WHERE $clientFilter2 di.di_debitNumber!='' AND di.di_status='1' AND di.di_debit_status='0') result ORDER BY date DESC");
        return $query->result();
    }

    public function updatePrint_status($val) {
        $this->db->query("UPDATE program_trainers SET invoice_print='" . $val['status'] . "' WHERE project_id='" . $val['pid'] . "' AND trainer_id='" . $val['tid'] . "'");
    }

    public function save_courier() {

        $pType = $_POST['pType'];

        $data = array('cr_project' => $_POST['project'],
            'cr_type' => $pType,
            'cr_cname' => $_POST['cname'],
            'cr_ship_date' => $_POST['cr_ship_date'],
            'cr_cid' => $_POST['cid'],
            'cr_amt' => $_POST['camt'],
            'comments' => $_POST['comments'],
            'cr_date' => $this->cdate);

        $this->db->insert('courier', $data);
        $rfid = $this->db->insert_id();

        $data1 = array('project_id' => $_POST['project'],
            'ref_id' => $rfid,
            'expense_type' => $pType,
            'notes' => $_POST['comments'],
            'amount' => $_POST['camt'],
            'in_amount' => $_POST['camt'],
            'added_by' => 'Account',
            'added_date' => $this->full_date);

        $this->db->insert('program_expenses', $data1);

        if ($_POST['pType'] == "Training Props") {
            $this->db->query("UPDATE program_props SET delivery_status='1' WHERE project_id='" . $_POST['project'] . "'");
        }

        //SEND MAIL TO TRAINER AND PM ONLY IF SHipment was for PROPS
        $projectId = '';
        if ($_POST['pType'] == "Training Props" || $_POST['pType'] == "Misc. Charges") {

            $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,c.client_name FROM training_projects p, clients c  WHERE p.project_id='" . $_POST['project'] . "' AND c.client_id=p.client_id");
            $project = $query->result();

            $query2 = $this->db->query("SELECT u.name,u.email,u.user_code FROM program_trainers p, application_users u WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.trainer_id");
            $result = $query2->result();
            $email = '';
            $eUid = array();
            foreach ($result as $da) {
                $email .=$da->email . ',';
                array_push($eUid, $da->user_code);
            }
            $email = rtrim($email, ',');

            $query3 = $this->db->query("SELECT u.email,u.user_code FROM application_users u, training_projects p WHERE p.project_id='" . $_POST['project'] . "' AND u.user_code=p.user_code");
            $result = $query3->result();
            $pm_email = $result[0]->email;
            array_push($eUid, $result[0]->user_code);

            $email .=',' . $pm_email;
            if ($pType == "Training Props") {

                $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $project[0]->project_id . "' AND notification_type='PM Approve Props'");

                $data = array('project' => $project[0]->project_title,
                    'start_date' => $project[0]->training_start_date,
                    'company' => $project[0]->client_name,
                    'status' => 'PropsShipped');
                foreach ($eUid as $uid) {
                    $note = array('user' => $uid,
                        'project' => $_POST['project'],
                        'task' => 0,
                        'type' => 'AS Props Ship',
                        'due_date' => '',
                        'text' => "Admin Support has Shipped Props Requiested for the program  " . $project[0]->project_title);
                    $this->push_notification($note);
                }
                //return $eUid;
                props_status_mail($email, $data);
            }
            if ($pType == "Misc. Charges") {
                $note = array('user' => $result[0]->user_code,
                    'project' => $_POST['project'],
                    'task' => 0,
                    'type' => 'AS Misc Update',
                    'due_date' => '',
                    'text' => "Admin Support update Misc Courier Details for  " . $project[0]->project_title);
                $this->push_notification($note);
            }
            $projectId = $project[0]->project_id;
        }
        if ($pType == 'Participant Handbook') {
            $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $projectId . "' AND notification_type='AS Content'");
            return 'Courier';
        } else {
            return 'Props';
        }
    }

    public function makeBulkPayment() {
        $sel = json_decode($_POST['sel']);
        foreach ($sel as $ss) {
            $tid = $ss->tid;
            $pid = $ss->pid;
            $query = $this->db->query("SELECT pt.id,pt.amount,pt.total_bill,pt.trainer_invoice_file,pt.trainer_invoice_gst,p.training_duration FROM program_trainers pt INNER JOIN training_projects p ON pt.project_id=p.project_id WHERE pt.project_id='" . $pid . "' AND pt.trainer_id='" . $tid . "'");
            $result = $query->result();
            $gst = 0;
            if (empty($result[0]->trainer_invoice_file)) {
                $professional = round($result[0]->amount * $result[0]->training_duration, 2);
            } else {
                $professional = round($result[0]->total_bill / 118 * 100, 2);
                $gst = round(0.18 * $professional, 2);
            }

            $tds = $professional * .10;
            $debit = 0;
            $query1 = $this->db->query("SELECT * FROM debit_note WHERE project='" . $pid . "' AND trainer='" . $tid . "' AND debit_status='1'");
            $result1 = $query1->result();
            if (!empty($result1)) {
                foreach ($result1 as $rs) {
                    $debit +=$rs->debit_amt;
                }
            }

            $pTotal = $professional - $tds + $gst + $debit;

            $val = array('pid' => $result[0]->id, 'project_id' => $pid, 'amt' => $pTotal, 'duration' => $professional);
            //return $val;
            $this->update_bulkpayment($val);
        }
    }

    public function update_bulkpayment($val) {
        $data = array('trainer_paid_status' => '1',
            'trainer_payment_date' => $_POST['date']);
        $this->db->where('id', $val['pid']);
        $this->db->update('program_trainers', $data);
        $file = "";
        $path = './assets/upload/content/';
        $img_name = $_FILES['advice']['name'];
        $tmp_name = $_FILES['advice']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $file = $file_name;
            }
        }

        $data = array('p_id' => $val['pid'],
            'tp_amt' => $val['amt'],
            'tp_pay_date' => $_POST['date'],
            'tp_pay_id' => $_POST['paymentid'],
            'tp_advice' => $file,
            'tp_date' => $this->cdate);

        $this->db->insert('trainer_payment', $data);

        $query = $this->db->query("SELECT p.project_id,p.project_title,p.training_start_date,p.location_of_training, c.client_name FROM training_projects p, clients c  WHERE p.project_id='" . $val['project_id'] . "' AND c.client_id=p.client_id");
        $project = $query->result();

        $query2 = $this->db->query("SELECT u.name,u.email,u.user_code FROM program_trainers p, application_users u WHERE p.id='" . $val['pid'] . "' AND u.user_code=p.trainer_id");
        $result = $query2->result();
        $eUid = $result[0]->user_code;
        $email = '';
        foreach ($result as $da) {
            $email .=$da->email . ',';
        }
        $email = rtrim($email, ',');

        $query3 = $this->db->query("SELECT u.email,u.user_code FROM application_users u, training_projects p WHERE p.project_id='" . $val['project_id'] . "' AND u.user_code=p.user_code");
        $result = $query3->result();
        $pm_email = $result[0]->email;
        $pmCode = $result[0]->user_code;

        $email .=',' . $pm_email;

        $this->db->query("UPDATE notifications SET todo_task='2' WHERE project_id='" . $project[0]->project_id . "' AND notification_type='TS Confirm Invoice'");

        $data = array('project' => $project[0]->project_title,
            'start_date' => $project[0]->training_start_date,
            'location' => $project[0]->location_of_training,
            'company' => $project[0]->client_name,
            'status' => 'InvoicePaid');

        $note = array('user' => $eUid,
            'project' => $project[0]->project_id,
            'task' => 0,
            'type' => 'Admin Paid to trainer',
            'due_date' => '',
            'text' => "Payment has been processed against Your Invoice by Accounts Dept.");
        $this->push_notification($note);

        $note = array('user' => $pmCode,
            'project' => $project[0]->project_id,
            'task' => 0,
            'type' => 'Admin Paid to trainer',
            'due_date' => '',
            'text' => "Payment has been processed against Trainer Invoice by Accounts Dept.");
        $this->push_notification($note);

        trainer_invoice_paid($email, $data);
    }

    public function cancelCInvoice($val) {
        $this->db->query("UPDATE client_invoice SET ci_status='2' WHERE ci_id='" . $val['cid'] . "'");
        $this->db->query("UPDATE training_projects SET is_active='0' WHERE project_id='" . $val['pid'] . "'");
    }

    public function cancelDInvoice($val) {
        $this->db->query("UPDATE drona_invoice SET di_status='2' WHERE di_id='" . $val['cid'] . "'");
    }

    public function updateUserProfile() {
        $photo = $_POST['cimg'];

        $path = './assets/upload/pm/';
        $img_name = $_FILES['photo']['name'];
        $tmp_name = $_FILES['photo']['tmp_name'];
        if (!empty($img_name)) {
            if ($file_name = image_upload($path, $img_name, $tmp_name)) {
                $this->image_resize($file_name, $path, $path, '250');
                $photo = $file_name;
            }
        }

        $data = array(
            'name' => $_POST['fname'],
            'contact_number' => $_POST['contact'],
            'user_photo' => $photo);

        $this->db->where('user_id', $_POST['uid']);
        $this->db->update('application_users', $data);

        return 1;
    }

}
