<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class home_model extends CI_Model {

    public $cdate = null;
    public $full_date = null;

    public function __construct() {
        $datestring = "%Y-%m-%d";
        $datestring1 = "%Y-%m-%d %h:%i:%s";
        $time = time();
        $this->cdate = mdate($datestring, $time);
        $this->full_date = mdate($datestring1, $time);
    }

    public function image_resize($file_name, $path, $newpath, $newWidth) {
        $img_size = getimagesize($path . $file_name);
        $newHeight = round(($newWidth / $img_size[0]) * $img_size[1]);
        $config['image_library'] = 'gd2';
        $config['source_image'] = $path . $file_name;
        $config['new_image'] = $newpath . $file_name;
        $config['maintain_ratio'] = TRUE;
        $config['width'] = $newWidth;
        $config['height'] = $newHeight;
        $this->image_lib->initialize($config);
        $this->image_lib->resize();
    }

    public function crop_image($val) {

        $image_config['image_library'] = 'gd2';
        $image_config['source_image'] = $val['src_path'];
        $image_config['new_image'] = $val['target_path'];
        $image_config['quality'] = "100%";
        $image_config['maintain_ratio'] = FALSE;
        $image_config['width'] = $val['width'];
        $image_config['height'] = $val['height'];
        $image_config['x_axis'] = $val['pos_x'];
        $image_config['y_axis'] = $val['pos_y'];
        $this->image_lib->clear();
        $this->image_lib->initialize($image_config);
        if (!$this->image_lib->crop()) {
            return 0;
        } else {
            return 1;
        }
    }

    public function get_degree() {
        $query = $this->db->query("SELECT * FROM category");
        return $query->result();
    }

    public function get_year_list($id) {
        $query = $this->db->query("SELECT y_id FROM products WHERE c_id='" . $id . "' GROUP BY y_id");
        return $query->result();
    }

    public function get_subject_list($y, $c) {
        $query = $this->db->query("SELECT sc_id FROM products WHERE c_id='" . $c . "' AND y_id='" . $y . "' GROUP BY sc_id");
        return $query->result();
    }

    public function get_authour_list($y, $c, $s) {
        $query = $this->db->query("SELECT pd_authour FROM products WHERE c_id='" . $c . "' AND y_id='" . $y . "' AND sc_id='" . $s . "' GROUP BY pd_authour");
        return $query->result();
    }

    public function get_yearBook_limit($c, $y, $l) {
        $query = $this->db->query("SELECT * FROM products p,product_img pi WHERE p.c_id='" . $c . "' AND p.y_id='" . $y . "' AND p.pd_status='1' AND pi.pd_id=p.pd_id ORDER BY RAND() LIMIT $l");
        return $query->result();
    }

    public function get_subjectBook_limit($c, $y, $s, $l) {
        $query = $this->db->query("SELECT * FROM products p,product_img pi WHERE p.c_id='" . $c . "' AND p.y_id='" . $y . "' AND p.sc_id='" . $s . "' AND p.pd_status='1' AND pi.pd_id=p.pd_id ORDER BY RAND() LIMIT $l");
        return $query->result();
    }

    public function get_subject_book($c, $y, $s) {
        $query = $this->db->query("SELECT * FROM products p,product_img pi WHERE p.c_id='" . $c . "' AND p.y_id='" . $y . "' AND p.sc_id='" . $s . "' AND p.pd_status='1' AND pi.pd_id=p.pd_id");
        return $query->result();
    }

    public function get_total_degreeBook($id) {
        $query = $this->db->query("SELECT COUNT(pd_id) as total_book FROM products WHERE c_id='" . $id . "' AND pd_status='1'");
        $result = $query->result();
        return $result[0]->total_book;
    }

    public function get_total_yearBook($c, $y) {
        $query = $this->db->query("SELECT COUNT(pd_id) as total_book FROM products WHERE c_id='" . $c . "' AND y_id='" . $y . "' AND pd_status='1'");
        $result = $query->result();
        return $result[0]->total_book;
    }

    public function get_total_subjectBook($c, $y, $s) {
        $query = $this->db->query("SELECT COUNT(pd_id) as total_book FROM products WHERE c_id='" . $c . "' AND y_id='" . $y . "' AND sc_id='" . $s . "' AND pd_status='1'");
        $result = $query->result();
        return $result[0]->total_book;
    }

    public function add_to_cart($val) {
        if (!$this->session->userdata('cart')) {
            $cart = array();
            $cart[$val['pid']] = $val['qnt'];
            $this->session->set_userdata('cart', $cart);
        } else {
            $cart = $this->session->userdata('cart');
            if ($val['qnt'] == 0) {
                unset($cart[$val['pid']]);
            } else {
                $cart[$val['pid']] = $val['qnt'];
            }
            $this->session->set_userdata('cart', $cart);
        }

        $count = count($this->session->userdata('cart'));
        return $count;
    }

    public function get_book($id) {
        $query = $this->db->query("SELECT * FROM products WHERE pd_id='" . $id . "' AND pd_status='1'");
        return $query->result();
    }

    public function get_book_img($id) {
        $query = $this->db->query("SELECT * FROM product_img WHERE pd_id='" . $id . "' ORDER BY pi_status DESC");
        return $query->result();
    }

    public function get_single_product($id) {
        $query = $this->db->query("SELECT * FROM products p,product_img pi  WHERE p.pd_id='" . $id . "' AND pi.pd_id=p.pd_id GROUP BY pi.pd_id");
        return $query->result();
    }

    public function get_authour($id) {
        $query = $this->db->query("SELECT * FROM author WHERE at_name='" . $id . "'");
        return $query->result();
    }

    public function get_product_basic($id) {
        $query = $this->db->query("SELECT p.pd_id,p.pd_name,p.c_id,p.y_id,p.sc_id,p.pd_authour,p.pd_rate,p.pd_ship,pi.pd_img FROM products p, product_img pi WHERE p.pd_id='" . $id . "' AND pi.pd_id=p.pd_id");
        return $query->result();
    }

    public function save_user() {
        $check = $this->db->query("SELECT u_contact FROM user WHERE u_contact='" . $_POST['mob'] . "'");
        if ($check->num_rows()) {
            return 2;
        }

        $data = array('u_name' => $_POST['fname'],
            'u_contact' => $_POST['mob'],
            'u_email' => $_POST['email'],
            'u_password' => $_POST['pass'],
            'u_date' => $this->cdate,
            'u_last_login' => $this->cdate);

        $this->db->insert('user', $data);

        $this->session->set_userdata('user', $_POST['mob']);
        $this->session->set_userdata('u_name', $_POST['fname']);
        return 1;
    }

    public function user_login() {

        $this->db->where('u_contact', $_POST['mobile']);
        $this->db->where('u_password', $_POST['pass']);
        $query = $this->db->get('user');

        if ($query->num_rows()) {
            $result = $query->result();
            $this->session->set_userdata('u_name', $result[0]->u_name);
            $this->session->set_userdata('user', $result[0]->u_contact);
            $this->session->set_userdata('u_contact', $result[0]->u_contact);
            return 3;
        } else {
            return 4;
        }
    }

    public function get_user($id) {
        $query = $this->db->query("SELECT * FROM user  WHERE u_contact='" . $id . "' ");
        return $query->result();
    }

    public function get_shipping_address($id) {
        $query = $this->db->query("SELECT * FROM shipping_address WHERE u_id='" . $id . "'");
        return $query->result();
    }

    public function update_address() {
        $data = array('u_id' => $this->session->userdata('user'),
            'sh_name' => $_POST['fname'],
            'sh_city' => $_POST['city'],
            'sh_pincode' => $_POST['pincode'],
            'sh_address' => $_POST['address'],
            'sh_contact' => $_POST['mobile'],
            'sh_landmark' => $_POST['landmark'],
            'sh_email' => $_POST['email']);

        $check = $this->db->query("SELECT u_id FROM shipping_address WHERE u_id='" . $this->session->userdata('user') . "'");
        if ($check->num_rows()) {
            $this->db->where('u_id', $this->session->userdata('user'));
            $this->db->update('shipping_address', $data);
        } else {
            $this->db->insert('shipping_address', $data);
        }
    }

    public function get_order_id() {
        $this->db->query("INSERT INTO ref_no VALUES('','oid')");
        return $this->db->insert_id();
    }

    public function order_cod() {

        $cart = $this->session->userdata('cart');
        $product = array();
        $no = 0;
        $mrp = 0;
        $ship = 0;
        $item = 0;
        foreach ($cart as $key => $value) {
            $result = $this->get_single_product($key);
            $detail = array();
            $rate = $result[0]->pd_rate;
            $mrp +=$rate * $value;
            $item += $value;
            $ship += $result[0]->pd_ship * $value;

            $detail['id'] = $result[0]->pd_id;
            $detail['name'] = $result[0]->pd_name;
            $detail['cat'] = $result[0]->c_id;
            $detail['sub'] = $result[0]->sc_id;
            $detail['year'] = $result[0]->y_id;
            $detail['authour'] = $result[0]->pd_authour;
            $detail['del'] = $result[0]->pd_publisher . ',' . $result[0]->pd_edition . ',' . $result[0]->pd_isbn;
            $detail['img'] = $result[0]->pd_img;
            $detail['mrp'] = $result[0]->pd_mrp;
            $detail['rate'] = $rate;
            $detail['ship'] = $result[0]->pd_ship * $value;
            $detail['item'] = $value;
            $detail['sub_total'] = ($rate * $value) + ($result[0]->pd_ship * $value);
            $product[$no] = $detail;
            $no++;
        }

        $tax = 0;
        $total_amt = $mrp + $ship + $tax;

        $data = array('o_id' => $_POST['oid'],
            'u_id' => $this->session->userdata('user'),
            'o_date' => $this->full_date,
            'o_total_mrp' => $mrp,
            'o_total_ship' => $ship,
            'o_total_tax' => $tax,
            'o_total_item' => $item,
            'o_total_amt' => $total_amt,
            'o_pay_type' => 'COD',
            'o_confirm_status' => 0,
            'o_status' => 1
        );

        $this->db->insert('product_order', $data);

        $o_id = $_POST['oid'];

        foreach ($product as $pdata) {

            $data1 = array('o_id' => $o_id,
                'pd_id' => $pdata['id'],
                'pd_name' => $pdata['name'],
                'pd_img' => $pdata['img'],
                'pd_cat' => $pdata['cat'],
                'pd_sub' => $pdata['sub'],
                'pd_year' => $pdata['year'],
                'pd_authour' => $pdata['authour'],
                'pd_details' => $pdata['del'],
                'pd_mrp' => $pdata['mrp'],
                'pd_offer' => '',
                'pd_rate' => $pdata['rate'],
                'pd_ship' => $pdata['ship'],
                'pd_qnt' => $pdata['item'],
                'pd_total_amt' => $pdata['sub_total']
            );

            $this->db->insert('order_details', $data1);
        }

        $address = $this->get_shipping_address($this->session->userdata('user'));

        $dd = array('o_id' => $o_id,
            'dd_name' => $address[0]->sh_name,
            'dd_email' => $address[0]->sh_email,
            'dd_city' => $address[0]->sh_city,
            'dd_contact' => $address[0]->sh_contact,
            'dd_address' => $address[0]->sh_address,
            'dd_pincode' => $address[0]->sh_pincode,
            'dd_area' => $address[0]->sh_landmark,
            'dd_status' => 0
        );

        $this->db->insert('delivery_details', $dd);

        $data2 = array('u_id' => $this->session->userdata('user'),
            'o_id' => $o_id,
            'py_amt' => $total_amt,
            'py_type' => 'COD',
            'py_method' => '',
            'py_cur' => 'INR',
            'py_tx' => '',
            'py_payment_id' => '',
            'py_date' => $this->cdate,
            'py_status' => 0,
        );

        $this->db->insert('payment', $data2);

        $this->session->unset_userdata('cart');

        // new_order_mail($this->get_order($o_id));

        return $o_id;
    }

    public function update_account() {
        if ($_POST['cu'] != $_POST['mobile']) {
            $check = $this->db->query("SELECT u_contact FROM user WHERE u_contact='" . $_POST['mob'] . "'");
            if ($check->num_rows()) {
                return 2;
            }
        }

        $data = array('u_name' => $_POST['uname'],
            'u_contact' => $_POST['mobile'],
            'u_email' => $_POST['email'],
            'u_date' => $this->cdate,
            'u_last_login' => $this->cdate);

        $this->db->where('u_contact', $_POST['cu']);
        $this->db->update('user', $data);

        $this->session->set_userdata('user', $_POST['mobile']);
        return 1;
    }
    
    public function update_password($val)
    {
        $check = $this->db->query("SELECT u_contact FROM user WHERE u_contact='" . $val['user'] . "' AND u_password='" . $val['old_pass'] . "'");
        if (!$check->num_rows()) {
                return 0;
            }
            
            $data = array('u_password'=>$val['new_pass']);
            $this->db->where('u_contact',$val['user']);
            $this->db->update('user',$data);
            
            return 1;
            
    }
    
    public function get_order_list($id)
    {
        $query = $this->db->query("SELECT * FROM product_order WHERE u_id='".$id."' ORDER BY o_id DESC");
        return $query->result();
    }
    
    public function get_order($u,$id)
    {
        $query = $this->db->query("SELECT * FROM product_order o, delivery_details d WHERE o.u_id='".$u."' AND o.o_id='".$id."' AND d.o_id=o.o_id");
        return $query->result();
    }
    
    public function get_payment_details($id)
    {
        $query = $this->db->query("SELECT * FROM payment WHERE o_id='".$id."'");
        return $query->result();
    }
    
    public function get_order_details($id)
    {
        $query = $this->db->query("SELECT * FROM order_details WHERE o_id='".$id."'");
        return $query->result();
    }
    
    public function cancel_order()
    {
        $this->db->query("UPDATE product_order SET o_cancel='1' WHERE o_id='".$_POST['oid']."'");
        $query = $this->db->query("SELECT * FROM payment WHERE o_id='".$_POST['oid']."'");
        $result = $query->result();
        
        $data = array('o_id'=>$_POST['oid'],
            'co_amt'=>$result[0]->py_amt,
            'co_method'=>$result[0]->py_type,
            'co_payment_id'=>$result[0]->py_payment_id,
            'co_reason'=>$_POST['reason'],
            'co_date'=>$this->full_date,
            'co_type'=>'Customer',
            'co_pay_status'=>$result[0]->py_status);
        
        $this->db->insert('cancel_order',$data);
        
        $this->db->query("DELETE FROM payment WHERE o_id='".$_POST['oid']."'");
    }
    
    public function get_cancel_order($id=NULL)
    {
        if(!empty($id))
        {
            $query = $this->db->query("SELECT * FROM cancel_order WHERE o_id='".$id."'");
        }
        else
        {
            $query = $this->db->query("SELECT c.*,u.u_name,u.u_contact FROM cancel_order c, product_order o, user u WHERE o.o_id=c.o_id AND u.u_contact=o.u_id");
        }
        
        return $query->result();
    }
    
    public function get_top_selling($l)
    {
        $query = $this->db->query("SELECT *, SUM(od.pd_qnt) FROM order_details od, products p, product_img pi WHERE p.pd_id=od.pd_id AND pi.pd_id=p.pd_id GROUP BY od.pd_id ORDER BY SUM(od.pd_qnt) DESC LIMIT $l");
        return $query->result();
    }
    
    public function get_new_books($l)
    {
        $query = $this->db->query("SELECT * FROM products p, product_img pi WHERE p.pd_feature='1' AND pi.pd_id=p.pd_id  ORDER BY RAND() LIMIT $l");
        return $query->result();
    }
    
    public function search_suggestion($id) {
        $data = array();
        $data[] = array('name'=>$id,'findin'=>'','cat'=>'');
        $query = $this->db->query("SELECT pd_id,pd_name,c_id,y_id FROM products WHERE pd_name LIKE '%" . $id . "%' AND pd_status='1' GROUP BY pd_id");
        if ($query->num_rows()) {
            $result = $query->result();
            foreach ($result as $rs_data) {
                $val = array();
                $val['name'] = $rs_data->pd_name;
                $val['findin'] = 'in';
                $val['cat'] = $rs_data->y_id.', '.$rs_data->c_id;
                $data[] = $val;
            }
        }
        
        $query2 = $this->db->query("SELECT at_name FROM author WHERE at_name LIKE '%" . $id . "%' GROUP BY at_name");
        if($query2->num_rows())
        {
            $result2 = $query2->result();
            foreach ($result2 as $rs_data) {
                $val = array();
                $val['name'] = $rs_data->at_name;
                $val['findin'] = 'in';
                $val['cat'] = 'Authour';
                $data[] = $val;
            }
        }
        
        return json_encode($data);
    }
    
    public function get_search($id)
    {
        $query = $this->db->query("SELECT * FROM products p, product_img pi WHERE (p.pd_name='" . $id . "' ||  p.pd_name LIKE '%" . $id . "%') AND p.pd_status='1' AND pi.pd_id=p.pd_id GROUP BY p.pd_id");
//        return $this->db->last_query();
        return $query->result();
    }
    
    public function get_authour_search($id)
    {
        $query = $this->db->query("SELECT * FROM products p, product_img pi WHERE p.pd_authour='" . $id . "' AND p.pd_status='1' AND pi.pd_id=p.pd_id GROUP BY p.pd_id");
//        return $this->db->last_query();
        return $query->result();
    }

}
