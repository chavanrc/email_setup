$(document).ready(function () {
    var activeList = '';
    var search_box = '';
    var data_src = '';
    $.fn.custome_auto = function (param) {
        search_box = this;
        var pos = search_box.offset();
        var h = search_box.height();
        var w = search_box.width();
        var hh = 40 + parseInt(pos.top);

        $("body").append('<ul class="cust_auto"></ul>');
        $('.cust_auto').css('position', 'absolute');
        $('.cust_auto').css('left', pos.left);
        $('.cust_auto').css('top', hh);
        $('.cust_auto').css('min-width', w);

        $.get(param.sourceUrl, function (data) {
            data_src = data;
        }, "json");

        search_box.keyup(function (e) {
            var search_value = search_box.val();
            if (search_value != '')
            {
                $('.cust_auto').show();
                if (e.which != 38 && e.which != 40 && e.which != 13)
                {
                    activeList = '';
                    $('.cust_auto li').remove();
                    var i = 0;
                    var incr = 0;
                    for (i = 0; i < data_src.length; i++) {
                        var str = data_src[i].name.toLowerCase();
                        var patt = new RegExp(search_value);
                        if (patt.test(str))
                        {
                            incr = incr + 1;
                            $('.cust_auto').append('<li id="cust_' + incr + '">' + data_src[i].name + ' <span>' + data_src[i].findin + '</span> <i>' + data_src[i].cat + '</i></li>');
                        }
                    }
                }
            }
            else
            {
                $('.cust_auto').hide();
            }
        });
    };

    $(document).on("click", ".cust_auto li", function () {
        var va = $(this).text();
        search_box.val(va);
        $.cookie("area", va,{ path: '/' });
        $('.cust_auto').hide();
    });

    $(window).keyup(function (e) {
        if (e.which === 40) {
            if (activeList != '')
            {
                $('.cust_auto li').removeClass('cust_auto_background');
                $('#' + activeList + '').next('li').addClass('cust_auto_background');
                activeList = $('#' + activeList + '').next().attr("id");
            }
            else {
                $('#cust_1').addClass('cust_auto_background');
                activeList = 'cust_1';
            }
        }
        if (e.which === 38) {
            if (activeList != '')
            {
                $('.cust_auto li').removeClass('cust_auto_background');
                $('#' + activeList + '').prev('li').addClass('cust_auto_background');
                activeList = $('#' + activeList + '').prev().attr("id");
            }
        }
        if (e.which === 13) {
            if (activeList != '')
            {
                var selectedValue = $('#' + activeList + '').text();
                search_box.val(selectedValue);
                $.cookie("area", selectedValue,{ path: '/' });
                $('.cust_auto').hide();
            }
        }
    });
});